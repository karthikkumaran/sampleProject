<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\LaravelSubscriptions\Plan;
use Carbon\Carbon;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = [
            'id' => 1,
            'slug' => "user-trial",
            'name' => '{"en":"User trial"}',
            'description' => '{"en":"Basic trial plan"}',
            'is_active' => 1,
            'price' => 0.00,
            'signup_fee' => 0.00,
            'currency' => "INR",
            'trial_period' => 1,
            'trial_interval' => "day",
            'invoice_period' => 6,
            'invoice_interval' => "day",
            'grace_period' => 0,
            'grace_interval' => "day",
            'sort_order' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ];
        Plan::insert($plans);
    }
}
