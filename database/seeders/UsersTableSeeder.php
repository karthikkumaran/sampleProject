<?php

namespace Database\Seeders;

use App\User;
use App\UserInfo;
use App\CompanyInfo;
use App\Team;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        Team::create(array("name"=>"Admin"));
        $users = [
            [
                'id'                 => 1,
                'name'               => 'Admin',
                'email'              => 'admin@admin.com',
                'password'           => '$2y$10$2oD9cF7MBquzP5nbRIQz4uYYiL2jbuIYPQEtDmu3L0Yfig1rQeoTK',
                'remember_token'     => null,
                'approved'           => 1,
                'verified'           => 1,
                'verified_at'        => '2020-03-01 14:39:07',
                'verification_token' => '',
                'team_id' => 1,
            ],
        ];

        User::insert($users);        
        UserInfo::create(array('user_id' => 1,'preview_link' => "preview_admin", 'public_link' => "public_admin"));
        CompanyInfo::create(array('user_id' => 1));
    }
}
