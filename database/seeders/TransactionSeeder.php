<?php

namespace Database\Seeders;

use App\Permission;
use Illuminate\Database\Seeder;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [

            'id'    => '141',
            'title' => 'show_transactions',
        ];
        Permission::insert($permissions);
    }
}
