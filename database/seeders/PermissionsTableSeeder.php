<?php

namespace Database\Seeders;

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => '1',
                'title' => 'user_management_access',
            ],
            [
                'id'    => '2',
                'title' => 'permission_create',
            ],
            [
                'id'    => '3',
                'title' => 'permission_edit',
            ],
            [
                'id'    => '4',
                'title' => 'permission_show',
            ],
            [
                'id'    => '5',
                'title' => 'permission_delete',
            ],
            [
                'id'    => '6',
                'title' => 'permission_access',
            ],
            [
                'id'    => '7',
                'title' => 'role_create',
            ],
            [
                'id'    => '8',
                'title' => 'role_edit',
            ],
            [
                'id'    => '9',
                'title' => 'role_show',
            ],
            [
                'id'    => '10',
                'title' => 'role_delete',
            ],
            [
                'id'    => '11',
                'title' => 'role_access',
            ],
            [
                'id'    => '12',
                'title' => 'user_create',
            ],
            [
                'id'    => '13',
                'title' => 'user_edit',
            ],
            [
                'id'    => '14',
                'title' => 'user_show',
            ],
            [
                'id'    => '15',
                'title' => 'user_delete',
            ],
            [
                'id'    => '16',
                'title' => 'user_access',
            ],
            [
                'id'    => '17',
                'title' => 'audit_log_show',
            ],
            [
                'id'    => '18',
                'title' => 'audit_log_access',
            ],
            [
                'id'    => '19',
                'title' => 'product_management_access',
            ],
            [
                'id'    => '20',
                'title' => 'product_category_create',
            ],
            [
                'id'    => '21',
                'title' => 'product_category_edit',
            ],
            [
                'id'    => '22',
                'title' => 'product_category_show',
            ],
            [
                'id'    => '23',
                'title' => 'product_category_delete',
            ],
            [
                'id'    => '24',
                'title' => 'product_category_access',
            ],
            [
                'id'    => '25',
                'title' => 'product_tag_create',
            ],
            [
                'id'    => '26',
                'title' => 'product_tag_edit',
            ],
            [
                'id'    => '27',
                'title' => 'product_tag_show',
            ],
            [
                'id'    => '28',
                'title' => 'product_tag_delete',
            ],
            [
                'id'    => '29',
                'title' => 'product_tag_access',
            ],
            [
                'id'    => '30',
                'title' => 'product_create',
            ],
            [
                'id'    => '31',
                'title' => 'product_edit',
            ],
            [
                'id'    => '32',
                'title' => 'product_show',
            ],
            [
                'id'    => '33',
                'title' => 'product_delete',
            ],
            [
                'id'    => '34',
                'title' => 'product_access',
            ],
            [
                'id'    => '35',
                'title' => 'faq_management_access',
            ],
            [
                'id'    => '36',
                'title' => 'faq_category_create',
            ],
            [
                'id'    => '37',
                'title' => 'faq_category_edit',
            ],
            [
                'id'    => '38',
                'title' => 'faq_category_show',
            ],
            [
                'id'    => '39',
                'title' => 'faq_category_delete',
            ],
            [
                'id'    => '40',
                'title' => 'faq_category_access',
            ],
            [
                'id'    => '41',
                'title' => 'faq_question_create',
            ],
            [
                'id'    => '42',
                'title' => 'faq_question_edit',
            ],
            [
                'id'    => '43',
                'title' => 'faq_question_show',
            ],
            [
                'id'    => '44',
                'title' => 'faq_question_delete',
            ],
            [
                'id'    => '45',
                'title' => 'faq_question_access',
            ],
            [
                'id'    => '46',
                'title' => 'content_management_access',
            ],
            [
                'id'    => '47',
                'title' => 'content_category_create',
            ],
            [
                'id'    => '48',
                'title' => 'content_category_edit',
            ],
            [
                'id'    => '49',
                'title' => 'content_category_show',
            ],
            [
                'id'    => '50',
                'title' => 'content_category_delete',
            ],
            [
                'id'    => '51',
                'title' => 'content_category_access',
            ],
            [
                'id'    => '52',
                'title' => 'content_tag_create',
            ],
            [
                'id'    => '53',
                'title' => 'content_tag_edit',
            ],
            [
                'id'    => '54',
                'title' => 'content_tag_show',
            ],
            [
                'id'    => '55',
                'title' => 'content_tag_delete',
            ],
            [
                'id'    => '56',
                'title' => 'content_tag_access',
            ],
            [
                'id'    => '57',
                'title' => 'content_page_create',
            ],
            [
                'id'    => '58',
                'title' => 'content_page_edit',
            ],
            [
                'id'    => '59',
                'title' => 'content_page_show',
            ],
            [
                'id'    => '60',
                'title' => 'content_page_delete',
            ],
            [
                'id'    => '61',
                'title' => 'content_page_access',
            ],
            [
                'id'    => '62',
                'title' => 'user_alert_create',
            ],
            [
                'id'    => '63',
                'title' => 'user_alert_show',
            ],
            [
                'id'    => '64',
                'title' => 'user_alert_delete',
            ],
            [
                'id'    => '65',
                'title' => 'user_alert_access',
            ],
            [
                'id'    => '66',
                'title' => 'team_create',
            ],
            [
                'id'    => '67',
                'title' => 'team_edit',
            ],
            [
                'id'    => '68',
                'title' => 'team_show',
            ],
            [
                'id'    => '69',
                'title' => 'team_delete',
            ],
            [
                'id'    => '70',
                'title' => 'team_access',
            ],
            [
                'id'    => '71',
                'title' => 'arobject_create',
            ],
            [
                'id'    => '72',
                'title' => 'arobject_edit',
            ],
            [
                'id'    => '73',
                'title' => 'arobject_show',
            ],
            [
                'id'    => '74',
                'title' => 'arobject_delete',
            ],
            [
                'id'    => '75',
                'title' => 'arobject_access',
            ],
            [
                'id'    => '76',
                'title' => 'video_create',
            ],
            [
                'id'    => '77',
                'title' => 'video_edit',
            ],
            [
                'id'    => '78',
                'title' => 'video_show',
            ],
            [
                'id'    => '79',
                'title' => 'video_delete',
            ],
            [
                'id'    => '80',
                'title' => 'video_access',
            ],
            [
                'id'    => '81',
                'title' => 'campaign_create',
            ],
            [
                'id'    => '82',
                'title' => 'campaign_edit',
            ],
            [
                'id'    => '83',
                'title' => 'campaign_show',
            ],
            [
                'id'    => '84',
                'title' => 'campaign_delete',
            ],
            [
                'id'    => '85',
                'title' => 'campaign_access',
            ],
            [
                'id'    => '86',
                'title' => 'entitycard_create',
            ],
            [
                'id'    => '87',
                'title' => 'entitycard_edit',
            ],
            [
                'id'    => '88',
                'title' => 'entitycard_show',
            ],
            [
                'id'    => '89',
                'title' => 'entitycard_delete',
            ],
            [
                'id'    => '90',
                'title' => 'entitycard_access',
            ],
            [
                'id'    => '91',
                'title' => 'asset_create',
            ],
            [
                'id'    => '92',
                'title' => 'asset_edit',
            ],
            [
                'id'    => '93',
                'title' => 'asset_show',
            ],
            [
                'id'    => '94',
                'title' => 'asset_delete',
            ],
            [
                'id'    => '95',
                'title' => 'asset_access',
            ],
            [
                'id'    => '96',
                'title' => 'currency_create',
            ],
            [
                'id'    => '97',
                'title' => 'currency_edit',
            ],
            [
                'id'    => '98',
                'title' => 'currency_show',
            ],
            [
                'id'    => '99',
                'title' => 'currency_delete',
            ],
            [
                'id'    => '100',
                'title' => 'currency_access',
            ],
            [
                'id'    => '101',
                'title' => 'company_info_create',
            ],
            [
                'id'    => '102',
                'title' => 'company_info_edit',
            ],
            [
                'id'    => '103',
                'title' => 'company_info_show',
            ],
            [
                'id'    => '104',
                'title' => 'company_info_delete',
            ],
            [
                'id'    => '105',
                'title' => 'company_info_access',
            ],
            [
                'id'    => '106',
                'title' => 'country_create',
            ],
            [
                'id'    => '107',
                'title' => 'country_edit',
            ],
            [
                'id'    => '108',
                'title' => 'country_show',
            ],
            [
                'id'    => '109',
                'title' => 'country_delete',
            ],
            [
                'id'    => '110',
                'title' => 'country_access',
            ],
            [
                'id'    => '111',
                'title' => 'state_create',
            ],
            [
                'id'    => '112',
                'title' => 'state_edit',
            ],
            [
                'id'    => '113',
                'title' => 'state_show',
            ],
            [
                'id'    => '114',
                'title' => 'state_delete',
            ],
            [
                'id'    => '115',
                'title' => 'state_access',
            ],
            [
                'id'    => '116',
                'title' => 'city_create',
            ],
            [
                'id'    => '117',
                'title' => 'city_edit',
            ],
            [
                'id'    => '118',
                'title' => 'city_show',
            ],
            [
                'id'    => '119',
                'title' => 'city_delete',
            ],
            [
                'id'    => '120',
                'title' => 'city_access',
            ],
            [
                'id'    => '121',
                'title' => 'user_info_create',
            ],
            [
                'id'    => '122',
                'title' => 'user_info_edit',
            ],
            [
                'id'    => '123',
                'title' => 'user_info_show',
            ],
            [
                'id'    => '124',
                'title' => 'user_info_delete',
            ],
            [
                'id'    => '125',
                'title' => 'user_info_access',
            ],
            [
                'id'    => '126',
                'title' => 'order_create',
            ],
            [
                'id'    => '127',
                'title' => 'order_edit',
            ],
            [
                'id'    => '128',
                'title' => 'order_show',
            ],
            [
                'id'    => '129',
                'title' => 'order_delete',
            ],
            [
                'id'    => '130',
                'title' => 'order_access',
            ],
            [
                'id'    => '131',
                'title' => 'impersonate_user',
            ],
            [
                'id'    => '132',
                'title' => 'product_attribute_access',
            ],
            [
                'id'    => '133',
                'title' => 'product_attribute_create',
            ],
            [
                'id'    => '134',
                'title' => 'product_attribute_edit',
            ],
            [
                'id'    => '135',
                'title' => 'product_attribute_delete',
            ],
            [
                'id'    => '136',
                'title' => 'product_attribute_show',
            ],
            [
                'id'    => '137',
                'title' => 'show_document',
            ],
            [
                'id'    => '138',
                'title' => 'custom_domain',
            ],
            [
                'id'    => '139',
                'title' => 'custom_script',
            ],
            // [
            //     'id'    => '140',
            //     'title' => 'tryon_custom_script',
            // ],
        ];

        Permission::insert($permissions);
    }
}
