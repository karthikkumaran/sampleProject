<?php

namespace Database\Seeders;

use App\CrmStatus;
use Illuminate\Database\Seeder;

class CrmStatusTableSeeder extends Seeder
{
    public function run()
    {
        $crmStatuses = [
            [
                'id'         => '1',
                'name'       => 'Lead',
                'created_at' => '2020-03-01 11:56:00',
                'updated_at' => '2020-03-01 11:56:00',
            ],
            [
                'id'         => '2',
                'name'       => 'Customer',
                'created_at' => '2020-03-01 11:56:00',
                'updated_at' => '2020-03-01 11:56:00',
            ],
            [
                'id'         => '3',
                'name'       => 'Partner',
                'created_at' => '2020-03-01 11:56:00',
                'updated_at' => '2020-03-01 11:56:00',
            ],
        ];

        CrmStatus::insert($crmStatuses);
    }
}
