<?php

namespace Database\Seeders;

use App\LaravelSubscriptions\PlanSubscriptionFeatures;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PlanSubscriptionFeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $features = [
            [
                'id'          => 1,
                'slug'        => "catalogs",
                'name'        => "catalogs",
                'description' => "Restriction on the number of catalogs",
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'          => 2,
                'slug'        => "products",
                'name'        => "products",
                'description' => "Restriction on the number of products",
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'          => 3,
                'slug'        => "stories",
                'name'        => "stories",
                'description' => "Products in a catalog can be viewed in stories like in whatsapp status",
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'          => 4,
                'slug'        => "analytics",
                'name'        => "analytics",
                'description' => "User can view analytics for his store,orders,customer etc",
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'          => 5,
                'slug'        => "orders",
                'name'        => "orders",
                'description' => "Restriction on the number of orders",
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'          => 6,
                'slug'        => "customers",
                'name'        => "customers",
                'description' => "Restriction on the number of customers",
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'          => 7,
                'slug'        => "grace-period",
                'name'        => "grace period",
                'description' => "Extend the subscription for a particular user",
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'          => 8,
                'slug'        => "pos",
                'name'        => "pos",
                'description' => "Point of sales feature for the admin to place order for customers",
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'          => 9,
                'slug'        => "inventory",
                'name'        => "inventory",
                'description' => "Manage product inventory/stock",
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'          => 10,
                'slug'        => "ar-object",
                'name'        => "ar_object",
                'description' => "Upload try on images for products",
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'          => 11,
                'slug'        => "3d-object",
                'name'        => "3d_object",
                'description' => "Upload 3D objects for products",
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'          => 12,
                'slug'        => "video-catalog",
                'name'        => "Video catalog",
                'description' => "Restriction on the number of Video catalogs",
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ];
        PlanSubscriptionFeatures::insert($features);
    }
}
