<?php

namespace Database\Seeders;

use App\Permission;
use Illuminate\Database\Seeder;

class AnalyticsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [

            'id'    => '142',
            'title' => 'show_analytics',
        ];
        Permission::insert($permissions);
    }
}
