<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\LaravelSubscriptions\PlanFeature;
use Carbon\Carbon;


class PlanFeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plan_features = [
            [
                'plan_id' => 1,
                'feature_id' => 1,
                'value' => 5,
                'resettable_period' => 0,
                'resettable_interval' => "month",
                'sort_order' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'plan_id' => 1,
                'feature_id' => 2,
                'value' => 50,
                'resettable_period' => 0,
                'resettable_interval' => "month",
                'sort_order' => 2,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
        ];
        PlanFeature::insert($plan_features);
    }
}
