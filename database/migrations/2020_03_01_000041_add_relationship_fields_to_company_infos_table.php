<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToCompanyInfosTable extends Migration
{
    public function up()
    {
        Schema::table('company_infos', function (Blueprint $table) {
            $table->unsignedInteger('team_id')->nullable();
            $table->foreign('team_id', 'team_fk_1077362')->references('id')->on('teams');
            $table->unsignedInteger('country_id')->nullable();
            $table->foreign('country_id', 'country_fk_1077405')->references('id')->on('countries');
            $table->unsignedInteger('state_id')->nullable();
            $table->foreign('state_id', 'state_fk_1077406')->references('id')->on('states');
            $table->unsignedInteger('city_id')->nullable();
            $table->foreign('city_id', 'city_fk_1077407')->references('id')->on('cities');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id', 'users_fk_1077434')->references('id')->on('users');
        });
    }
}
