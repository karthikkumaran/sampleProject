<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTermsToPlanInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plan_invoices', function (Blueprint $table) {
            //
            $table->string('terms')->nullable()->after('inv_date');
            $table->dateTime('due_date')->nullable()->after('terms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plan_invoices', function (Blueprint $table) {
            //
            $table->dropColumn('terms');
            $table->dropColumn('due_date');
        });
    }
}
