<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToEntitycardsTable extends Migration
{
    public function up()
    {
        Schema::table('entitycards', function (Blueprint $table) {
            $table->unsignedInteger('campaign_id');
            $table->foreign('campaign_id', 'campagin_fk_1077158')->references('id')->on('campaigns');
            $table->unsignedInteger('product_id')->nullable();
            $table->foreign('product_id', 'product_fk_1077159')->references('id')->on('products');
            $table->unsignedInteger('video_id')->nullable();
            $table->foreign('video_id', 'video_fk_1077160')->references('id')->on('videos');
            $table->unsignedInteger('team_id')->nullable();
            $table->foreign('team_id', 'team_fk_1077165')->references('id')->on('teams');
            $table->unsignedInteger('asset_id')->nullable();
            $table->foreign('asset_id', 'asset_fk_1077208')->references('id')->on('assets');
        });
    }
}
