<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_variants', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id')->nullable();
            $table->foreign('product_id')->references('id')->on('products');
            $table->longtext('variant_data')->nullable();
            $table->string('name')->nullable();
            $table->longText('description')->nullable();
            $table->decimal('price', 15, 2)->nullable();
            $table->string('sku')->nullable();
            $table->string('discount')->nullable();
            $table->string('stock')->nullable();
            $table->boolean('out_of_stock')->nullable();
            $table->boolean('status')->default(1);
            $table->boolean('downgradeable')->nullable();
            $table->string('url')->nullable();
            $table->integer('featured');
            $table->integer('new');
            $table->string('units')->nullable();
            $table->string('minimum_quantity')->nullable();
            $table->unsignedInteger('team_id')->nullable();
            $table->foreign('team_id')->references('id')->on('teams');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_variants');
    }
}
