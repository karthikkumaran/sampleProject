<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersProductsTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders_products', function ($table) {

            $table->string('name')->nullable()->after('id');
            $table->string('sku')->nullable()->after('name');
            $table->integer('qty')->nullable()->after('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders_products', function ($table) {

            $table->dropColumn('name');
            $table->dropColumn('sku');
            $table->dropColumn('qty');
        });
    }
}
