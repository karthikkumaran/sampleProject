<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignsTable extends Migration
{
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('is_start');
            $table->string('type');
            $table->string('is_active')->nullable();
            $table->string('subdomain')->nullable();
            $table->string('customdomain')->nullable();
            $table->string('preview_link')->nullable();
            $table->string('public_link')->nullable();
            $table->longtext('meta_data')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
