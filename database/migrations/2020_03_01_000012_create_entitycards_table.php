<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntitycardsTable extends Migration
{
    public function up()
    {
        Schema::create('entitycards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('meta_data')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
