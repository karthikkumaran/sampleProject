<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status');
            $table->longtext('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->unsignedInteger('campaign_id');
            $table->foreign('campaign_id', 'campaign_fk_1077995')->references('id')->on('campaigns');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id', 'user_fk_1077997')->references('id')->on('users');
            $table->unsignedInteger('team_id');
            $table->foreign('team_id', 'team_fk_1077991')->references('id')->on('teams');
        });
    } 

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
