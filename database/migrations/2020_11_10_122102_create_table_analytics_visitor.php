<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableAnalyticsVisitor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analytics_visitor', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip_address');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('country_name');
            $table->{$this->jsonable()}('client_ips')->nullable();
            $table->boolean('is_from_trusted_proxy')->default(0);
            $table->string('state_name')->nullable();
            $table->string('postal_code')->nullable();
            $table->string('timezone')->nullable();
            $table->string('city')->nullable();

            $table->unsignedInteger('team_id')->nullable(false);
            $table->foreign('team_id')->references('id')->on('teams');
            // Indexes
            // $table->unique(['client_ip', 'latitude', 'longitude']);
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analytics_visitor');
    }

    protected function jsonable(): string
    {
        $driverName = DB::connection()->getPdo()->getAttribute(PDO::ATTR_DRIVER_NAME);
        $dbVersion = DB::connection()->getPdo()->getAttribute(PDO::ATTR_SERVER_VERSION);
        $isOldVersion = version_compare($dbVersion, '5.7.8', 'lt');

        return $driverName === 'mysql' && $isOldVersion ? 'text' : 'json';
    }
}
