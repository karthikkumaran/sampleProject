<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToArobjectsTable extends Migration
{
    public function up()
    {
        Schema::table('arobjects', function (Blueprint $table) {
            $table->unsignedInteger('product_id');
            $table->foreign('product_id', 'product_fk_1076974')->references('id')->on('products');
            $table->unsignedInteger('team_id')->nullable();
            $table->foreign('team_id', 'team_fk_1076980')->references('id')->on('teams');
        });
    }
}
