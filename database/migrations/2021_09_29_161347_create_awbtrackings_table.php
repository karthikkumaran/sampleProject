<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAwbtrackingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('awbtrackings', function (Blueprint $table) {
            $table->id();
            $table->string('awb_no')->nullable();
            $table->string('carrier_name')->nullable();
            $table->unsignedInteger('awb_id')->nullable();
            $table->unsignedInteger('child_awb_id')->nullable();
            $table->unsignedInteger('shippingorderid')->nullable();
            $table->string('return')->nullable();
            $table->string('eventstatus')->nullable();
            $table->longtext('remark')->nullable();
            $table->string('location')->nullable();
            $table->string('eventtime')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('awbtrackings');
        // 
        $table->dropColumn('awb_no');
        $table->dropColumn('carrier_name');
        $table->dropColumn('awb_id');
        $table->dropColumn('child_awb_id');
        $table->dropColumn('shippingorderid');
        $table->dropColumn('return');
        $table->dropColumn('eventstatus');
        $table->dropColumn('remark');
        $table->dropColumn('location');
        $table->dropColumn('eventtime');
    }
}
