<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToEnquiryproductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('enquiry_product', function ($table) {

            $table->string('name')->nullable()->after('id');
            $table->string('sku')->nullable()->after('name');
            $table->integer('qty')->nullable()->after('price');
            $table->longtext('meta_data')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('enquiry_product', function ($table) {

            $table->dropColumn('name');
            $table->dropColumn('sku');
            $table->dropColumn('qty');
            $table->dropColumn('meta_data');
        });
    }
}
