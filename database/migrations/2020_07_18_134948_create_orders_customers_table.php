<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fname')->nullable();
            $table->string('lname')->nullable();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('city')->nullable();
            $table->string('pincode')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->string('email')->nullable();
            $table->string('mobileno')->nullable();
            $table->string('amobileno')->nullable();
            $table->string('addresstype')->nullable();
            $table->longtext('meta_data')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('orders_customers', function (Blueprint $table) {
            $table->unsignedInteger('order_id');
            $table->foreign('order_id', 'order_fk_1077897')->references('id')->on('orders');
            $table->unsignedInteger('campaign_id');
            $table->foreign('campaign_id', 'campaign_fk_1077997')->references('id')->on('campaigns');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id', 'user_fk_1077891')->references('id')->on('users');
            $table->unsignedInteger('team_id');
            $table->foreign('team_id', 'team_fk_1077891')->references('id')->on('teams');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_customers');
    }
}
