<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveSlugFromPlanFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(config('rinvex.subscriptions.tables.plan_features'), function (Blueprint $table) {
            $table->dropForeign(['plan_id']);
            $table->dropUnique(['plan_id', 'slug']);
            $table->dropColumn('slug');
            $table->foreign('plan_id')->references('id')->on(config('rinvex.subscriptions.tables.plans'))
                  ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(config('rinvex.subscriptions.tables.plan_features'), function (Blueprint $table) {
            $table->string('slug');
            $table->unique(['slug']);
        });
    }
}
