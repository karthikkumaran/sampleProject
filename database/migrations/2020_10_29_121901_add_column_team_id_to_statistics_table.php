<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTeamIdToStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(config('rinvex.statistics.tables.routes'), function (Blueprint $table) {
            $table->unsignedInteger('team_id')->nullable();
            $table->foreign('team_id')->references('id')->on('teams');
        });

        Schema::table(config('rinvex.statistics.tables.paths'), function (Blueprint $table) {
            $table->unsignedInteger('team_id')->nullable();
            $table->foreign('team_id')->references('id')->on('teams');
        });

        Schema::table(config('rinvex.statistics.tables.devices'), function (Blueprint $table) {
            $table->unsignedInteger('team_id')->nullable();
            $table->foreign('team_id')->references('id')->on('teams');
        });

        Schema::table(config('rinvex.statistics.tables.agents'), function (Blueprint $table) {
            $table->unsignedInteger('team_id')->nullable();
            $table->foreign('team_id')->references('id')->on('teams');
        });

        Schema::table(config('rinvex.statistics.tables.platforms'), function (Blueprint $table) {
            $table->unsignedInteger('team_id')->nullable();
            $table->foreign('team_id')->references('id')->on('teams');
        });

        Schema::table(config('rinvex.statistics.tables.geoips'), function (Blueprint $table) {
            $table->unsignedInteger('team_id')->nullable();
            $table->foreign('team_id')->references('id')->on('teams');
        });

        Schema::table(config('rinvex.statistics.tables.requests'), function (Blueprint $table) {
            $table->unsignedInteger('team_id')->nullable();
            $table->foreign('team_id')->references('id')->on('teams');
        });

        Schema::table(config('rinvex.statistics.tables.data'), function (Blueprint $table) {
            $table->unsignedInteger('team_id')->nullable();
            $table->foreign('team_id')->references('id')->on('teams');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(config('rinvex.statistics.tables.routes'), function (Blueprint $table) {
            $table->dropForeign('statistics_routes_team_id_foreign');
            $table->dropColumn('team_id');
        });

        Schema::table(config('rinvex.statistics.tables.paths'), function (Blueprint $table) {
            $table->dropForeign('statistics_paths_team_id_foreign');
            $table->dropColumn('team_id');
        });

        Schema::table(config('rinvex.statistics.tables.devices'), function (Blueprint $table) {
            $table->dropForeign('statistics_devices_team_id_foreign');
            $table->dropColumn('team_id');
        });

        Schema::table(config('rinvex.statistics.tables.agents'), function (Blueprint $table) {
            $table->dropForeign('statistics_agents_team_id_foreign');
            $table->dropColumn('team_id');
        });

        Schema::table(config('rinvex.statistics.tables.platforms'), function (Blueprint $table) {
            $table->dropForeign('statistics_platforms_team_id_foreign');
            $table->dropColumn('team_id');
        });

        Schema::table(config('rinvex.statistics.tables.geoips'), function (Blueprint $table) {
            $table->dropForeign('statistics_geoips_team_id_foreign');
            $table->dropColumn('team_id');
        });

        Schema::table(config('rinvex.statistics.tables.requests'), function (Blueprint $table) {
            $table->dropForeign('statistics_requests_team_id_foreign');
            $table->dropColumn('team_id');
        });

        Schema::table(config('rinvex.statistics.tables.data'), function (Blueprint $table) {
            $table->dropForeign('statistics_data_team_id_foreign');
            $table->dropColumn('team_id');
        });
    }
}
