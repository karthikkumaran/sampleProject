<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFeatureIdToPlanFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(config('rinvex.subscriptions.tables.plan_features'), function (Blueprint $table) {
            $table->integer('feature_id')->unsigned()->after('plan_id');

            $table->foreign('feature_id')->references('id')->on('plan_subscription_features')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(config('rinvex.subscriptions.tables.plan_features'), function (Blueprint $table) {
            $table->dropForeign(['feature_id']);
            $table->dropColumn('feature_id');
        });
    }
}
