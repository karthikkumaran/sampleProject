<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserInfosTable extends Migration
{
    public function up()
    {
        Schema::create('user_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('gender')->nullable();
            $table->string('designation')->nullable();
            $table->date('dob')->nullable();
            $table->string('subdomain')->nullable();
            $table->string('customdomain')->nullable(); 
            $table->string('preview_link')->nullable();
            $table->string('public_link')->nullable();
            $table->longtext('meta_data')->nullable(); 
            $table->string('is_start')->default('0');
            $table->string('is_active')->default('1');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
