<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToProductCategoriesTable extends Migration
{
    public function up()
    {
        Schema::table('product_categories', function (Blueprint $table) {
            $table->unsignedInteger('team_id')->nullable();
            $table->foreign('team_id', 'team_fk_1076652')->references('id')->on('teams');
            $table->unsignedInteger('product_category_id')->nullable();
            $table->foreign('product_category_id', 'category_fk_1077998')->references('id')->on('product_categories');
        });
    }
}
