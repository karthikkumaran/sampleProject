<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnCustomersignToOrderscustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders_customers', function (Blueprint $table) {
            $table->string('customer_sign')->nullable()->after('meta_data');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders_customers', function (Blueprint $table) {
            $table->dropColumn('terms');
            //
        });
    }
}
