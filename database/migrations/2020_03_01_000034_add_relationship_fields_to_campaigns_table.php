<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToCampaignsTable extends Migration
{
    public function up()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->foreign('user_id', 'user_fk_1077065')->references('id')->on('users');
            $table->unsignedInteger('team_id')->nullable(); 
            $table->foreign('team_id', 'team_fk_1077065')->references('id')->on('teams');
        });
    }
}
