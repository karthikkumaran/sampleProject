<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanSubscriptionFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_subscription_features', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->string('name');
            $table->string('description')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->unique(['slug']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_subscription_features');
    }

    // /**
    //  * Get jsonable column data type.
    //  *
    //  * @return string
    //  */
    // protected function jsonable(): string
    // {
    //     $driverName = DB::connection()->getPdo()->getAttribute(PDO::ATTR_DRIVER_NAME);
    //     $dbVersion = DB::connection()->getPdo()->getAttribute(PDO::ATTR_SERVER_VERSION);
    //     $isOldVersion = version_compare($dbVersion, '5.7.8', 'lt');

    //     return $driverName === 'mysql' && $isOldVersion ? 'text' : 'json';
    // }
}
