<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan_invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('subscription_payment_id');
            $table->foreign('subscription_payment_id')->references('id')->on('subscription_payments'); 
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');  
            $table->string('plan_name');
            $table->dateTime('plan_start')->nullable();
            $table->dateTime('plan_expiry')->nullable();
            $table->dateTime('inv_date')->nullable();         
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plan_invoices');
    }
}
