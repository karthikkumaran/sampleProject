<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersProductsTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * @return void
     */
    public function up()
    {
        Schema::create('orders_products', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('price', 15, 2)->nullable();
            $table->decimal('discount', 15, 2)->nullable();
            $table->longtext('notes')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('orders_products', function (Blueprint $table) {
            $table->unsignedInteger('order_id');
            $table->foreign('order_id', 'order_fk_1077896')->references('id')->on('orders');
            $table->unsignedInteger('product_id');
            $table->foreign('product_id', 'product_fk_1077996')->references('id')->on('products');
            $table->unsignedInteger('campaign_id');
            $table->foreign('campaign_id', 'campaign_fk_1077996')->references('id')->on('campaigns');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id', 'user_fk_1077998')->references('id')->on('users');
            $table->unsignedInteger('team_id');
            $table->foreign('team_id', 'team_fk_1077992')->references('id')->on('teams');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_products');
    }
}
