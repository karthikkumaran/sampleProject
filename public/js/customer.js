var loginModal='';
var registerModal = '';
var forgotModal = '';
var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> loading...';



var loginButton= $('.loginButton').on("click",function(){
            loginModal = $('#loginModal')
            loginModal.modal('show')
    })

var registerButton = $('.registerButton').on("click",function(){
        registerModal = $('#registerModal')
        registerModal.modal('show')

    })

var forgotButton =  document.getElementById('forgot_password')
if (forgotButton) {
    forgotButton.onclick = function(){
        loginModal.modal('hide')
        forgotModal = $('#forgotPasswordModal')
        forgotModal.modal('show')
    }
}


var registerLink = document.getElementById('registerLink');
if (registerLink) {
    registerLink.onclick = function () {
        loginModal.modal('hide')
        registerButton.trigger('click')
    }
}

var loginLink = document.getElementById('loginLink');
if (loginLink) {
    loginLink.onclick = function () {
        registerModal.modal('hide')
        if(forgotModal)
            forgotModal.modal('hide')
        loginButton.trigger('click')
    }
}

var F_loginLink = document.getElementById('forgotPwModal_loginLink');
if (F_loginLink) {
    F_loginLink.onclick = function () {
        forgotModal.modal('hide')
        loginButton.trigger('click')
    }
}

password = document.getElementById('rpassword');
cpassword = document.getElementById("cpassword");

if (password) {
    password.oninput = function () {
        if (cpassword.value != '') {
            if (cpassword.value != password.value) {
                $('#signup').prop('disabled', true);
                $("#CheckPasswordMatch").html("Password does not match !").css("color", "red");
            } else {
                $("#CheckPasswordMatch").html(" ");
                $('#signup').prop('disabled', false);
            }
        }
    }
}


if (cpassword) {
    cpassword.oninput = function () {
        if (password.value != cpassword.value) {
            $('#signup').prop('disabled', true);
            $("#CheckPasswordMatch").html("Password does not match !").css("color", "red");
        }
        else {
            $("#CheckPasswordMatch").html(" ");
            $('#signup').prop('disabled', false);
        }
    }
}

    
//login
var loginform = document.getElementById('loginform');
if (loginform) {
    loginform.onsubmit = function (e) {
        e.preventDefault();
        $('#login').attr('disabled', true);
        $('#login').html(loadingText);

        var loginForm = document.forms.loginform;
        var formData = new FormData(loginForm);

        data = {};
        for (var pair of formData.entries()) {
            data[pair[0]] = pair[1];
        }

        $.ajax({
            url: "/loginCustomer",
            type: "post",
            data: data,
        }).done(function (data) {
            if (data.msg == "success") {
                // window.location.reload();
                window.location.href = data.homeLink;
            }

            else if (data.msg == "error") {
                $('#login').attr('disabled', false);
                $('#login').html('LOG IN');

                showAlertMessage('login', ' Invalid email id or password!','danger')
            }

        }).fail(function (jqXHR, ajaxOptions, thrownError) {
        
        });

    }
}


//register
var registerform = document.getElementById('registerform');

if (registerform) {
    registerform.onsubmit = function (e) {
        e.preventDefault();
        $('#signup').prop('disabled', true);
        $('#signup').html(loadingText);


        var regForm = document.forms.registerform;
        var formData = new FormData(regForm);

        data = {};
        for (var pair of formData.entries()) {
            data[pair[0]] = pair[1];
        }
    
        $.ajax({
            url: "/registerCustomer",
            type: "post",
            data: data,
        }).done(function (data) {
            if (data.msg == "success") {
                window.location.href = data.homeLink;
            }
            else if (data.msg == "error") {
                $('#signup').prop('disabled', false);
                $('#signup').html('Sign Up');
                showAlertMessage('register','Email id already exist!','danger' )
            }

        }).fail(function (jqXHR, ajaxOptions, thrownError) {
        
        });
    
    }
}


//forgotPasswordForm
var forgotPasswordform = document.getElementById('forgotPasswordform');

if (forgotPasswordform) {
    forgotPasswordform.onsubmit = function (e) {
        e.preventDefault();
        $('#resetPassword').prop('disabled', true);
        $('#resetPassword').html(loadingText);

        var forgotPWform = document.forms.forgotPasswordform;
        var formData = new FormData(forgotPWform);

        data = {};
        for (var pair of formData.entries()) {
            data[pair[0]] = pair[1];
        }
    
        $.ajax({
            url: "/forget-password",
            type: "post",
            data: data,
        }).done(function (data) {
                showAlertMessage('forgotPassword',data.message,data.class )
            if (data.class == "success") {
                $('#resetPassword').hide();
            } else {
                $('#resetPassword').prop('disabled', false);
                $('#resetPassword').html('Recover Password');

            }
        }).fail(function (jqXHR, ajaxOptions, thrownError) {
        
        });
    
    }
}

function showAlertMessage(class_name,msg,type)
{
    $('#' + class_name + '-flash-message').append("<div class='mx-1 my-1 alert alert-"+type+" alert-dismissible fade show' role='alert'><a href = '#' class= 'close' data-dismiss='alert' aria-label='close' >&times;</a> "+msg+"</div> ");
    if (class_name == 'login') {
        $('#loginform').trigger("reset");
    }

    if (class_name == 'register') {
        $('#remail').val(' '); 
    }
    

}


