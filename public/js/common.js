function currencyFormatter(num, digits) {

    // console.log(num);
    if (num === undefined || num == null || num == 0)
        return formatMoney(0);
    var si = [
        { value: 1, symbol: "" },
        { value: 1E3, symbol: "K" },
        { value: 1E6, symbol: "M" },
        { value: 1E9, symbol: "G" },
        { value: 1E12, symbol: "T" },
        { value: 1E15, symbol: "P" },
        { value: 1E18, symbol: "E" }
    ];
    var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
    var i;
    for (i = si.length - 1; i > 0; i--) {
        if (num >= si[i].value) {
            break;
        }
    }
    return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol;
}

function formatMoney(number, decPlaces, decSep, thouSep) {
    decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
        decSep = typeof decSep === "undefined" ? "." : decSep;
    thouSep = typeof thouSep === "undefined" ? "," : thouSep;
    var sign = number < 0 ? "-" : "";
    var i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
    var j = (j = i.length) > 3 ? j % 3 : 0;

    return sign +
        (j ? i.substr(0, j) + thouSep : "") +
        i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
        (decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
}

function hasNull(target) {
    for (var member in target) {
        if (target[member] == null)
            return true;
    }
    return false;
}

function getSubscriptionFeatureUsage(featurename) {

    console.log("test subscription"+ featurename);
    $('.loading1').show();
    $.ajax({
        url: '/admin/campaign/usage/',
        type: "get",
        data: {
            'featurename': featurename
        },
        datatype: "html"
    }).done(function(data) {
        alertUsageLimitReached(data, featurename);
        $('.loading1').hide();
    }).fail(function(jqXHR, ajaxOptions, thrownError) {
        $('.loading1').hide();
    });
}

function alertUsageLimitReached(data, featurename) {
    if (featurename != null) {
        switch (data['alert']) {
            case 'limit_reached':
                $(".subscription_content_" + featurename).empty().html(data["alert_msg"]);
                // $(".modal-footer").empty();
                break;
            case 'limit_almost_reached':
                $(".subscription_alert_msg_" + featurename).html(data["alert_msg"]);
                break;
        }
    } else {
        $(".subscription_alert_msg").html(data["alert_msg"]);
    }
}

function createNewCatalog() {
    // getSubscriptionFeatureUsage('catalogs');
    $("#createNewCatalog").modal('show');
}

function openInNewTab(url) {
    var win = window.open(url, '_blank');
    win.focus();
}

var plan_id = "";
var subscription_url = "";

function page_redirection(data) {
    $.ajax({
        url: subscription_url,
        type: "POST",
        data: data,
        success: function(response) {
            window.location = response;
        }
    });
}

function razorpayPayment(response) {
    var options = {
        "key": response['razorpayId'], // Enter the Key ID generated from the Dashboard
        "amount": response['amount'], // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
        "currency": response['currency'],
        "name": response['name'],
        "description": response['description'],
        "order_id": response['orderId'], //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
        "handler": function(response) {
            data = "rzp_paymentid=" + response.razorpay_payment_id + "&rzp_orderid=" + response.razorpay_order_id + "&rzp_signature=" + response.razorpay_signature + "&status=success" + '&plan_id=' + plan_id;
            page_redirection(data);
        },
        "prefill": {
            "name": response['name'],
            "email": response['email'],
            "contact": response['contactNumber']
        },
        "theme": {
            "color": "#528FF0"
        }
    };

    var rzp1 = new Razorpay(options);
    rzp1.open();
    rzp1.on('payment.failed', function(response) {
        data = "status=failure" + "&rzp_paymentid=" + response.metadata.payment_id + "&rzp_orderid=" + response.metadata.order_id + "&response=" + response;
        page_redirection(data);
    });
}

function subscriptionPaymentService(plan_id, subscription_url) {
    plan_id = plan_id;
    subscription_url = subscription_url;
    $.ajax({
        url: "/admin/subscription/getpaymentdetails",
        type: "get",
        data: {
            'plan_id': plan_id
        },
        datatype: "html"
    }).done(function(data) {
        razorpayPayment(data);
        $('.loading1').hide();
    }).fail(function(jqXHR, ajaxOptions, thrownError) {
        $('.loading1').hide();
    });
}
//PWA Installation
var deferredPrompt;
var installSource;

$(document).ready(function()  {
    
window.addEventListener('beforeinstallprompt', (e) => {

    $("#aboutModal").modal('show');
    console.log('before');
	deferredPrompt = e;
	installSource = 'nativeInstallCard';
    console.log('prompt');
	e.userChoice.then(function (choiceResult) {
		if (choiceResult.outcome === 'accepted') {
			deferredPrompt = null;
		}

		ga('send', {
			hitType: 'event',
			eventCategory: 'pwa-install',
			eventAction: 'native-installation-card-prompted',
			eventLabel: installSource,
			eventValue: choiceResult.outcome === 'accepted' ? 1 : 0
		});
	});
})
function getPWADisplayMode() {
    const isStandalone = window.matchMedia('(display-mode: standalone)').matches;

    if (document.referrer.startsWith('android-app://')) {
        return 'twa';
    } else if (navigator.standalone || isStandalone) {
        return 'standalone';
    }

    return 'browser';

}
console.log(getPWADisplayMode());
if (getPWADisplayMode() === 'standalone') {
    console.log('hide');
    $("#aboutModal").modal('hide');
}
window.addEventListener('appinstalled', () => {
    console.log('appinstall');
	deferredPrompt = null;
    $("#aboutModal").modal('hide');

	const source = installSource || 'browser';

	ga('send', 'event', 'pwa-install', 'installed', source);
});


window.matchMedia('(display-mode: standalone)').addEventListener('change', ({ matches }) => {
    if (matches) {
        console.log('hide');
        $("#aboutModal").modal('hide');
    } else {
        $("#aboutModal").modal('show');
    }
});



const installApp = document.getElementById('installApp');

installApp.addEventListener('click', async () => {
	installSource = 'customInstallationButton';

	if (deferredPrompt !== null) {
		deferredPrompt.prompt();
		const { outcome } = await deferredPrompt.userChoice;
		if (outcome === 'accepted') {
			deferredPrompt = null;
		}

		ga('send', {
			hitType: 'event',
			eventCategory: 'pwa-install',
			eventAction: 'custom-installation-button-clicked',
			eventLabel: installSource,
			eventValue: outcome === 'accepted' ? 1 : 0
		});
	} else {
		showToast('SimpliSell is already installed.')
	}
})
});




