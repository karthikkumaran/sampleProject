var cartBtn = $(".cart"),
    cartItemCount = $(".cart-item-count");
var count = 0;
var cart = [];

// Constructor
function Item(catalogid, id, name, price, discounted_price, catagory, img, qty) {
    this.catalogid = catalogid;
    this.id = id;
    this.name = name;
    this.price = price;
    this.discounted_price = discounted_price;
    this.catagory = catagory;
    this.img = img;
    this.qty = qty;
}

// Save cart
function saveCart() {
    // localStorage.setItem('shoppingCart', JSON.stringify(cart));
    // console.log("save cart", JSON.stringify(cart).price);
    // console.log("save cart", cart);
    const now = new Date();
    var time = now.getTime() + 1000 * 60;
    // console.log(cart, time);
    localStorage.setItem('shoppingCart', JSON.stringify(cart));
    localStorage.setItem('expiretime', JSON.stringify(time));
    // console.log(localStorage);
}

// Load cart
function loadCart() {
    cart = JSON.parse(localStorage.getItem('shoppingCart'));
    var $this = cartBtn,
        addToCart = $this.find(".add-to-cart"),
        viewInCart = $this.find(".view-in-cart");

    cartItemCount = count = cart.length;
    if (cart.length > 0) {
        $(".cart-item-count").html(count);
        //console.log("count added");
        for (var i = 0; i < cartBtn.length; i++) {
            addToCart = $(cartBtn[i]).find(".add-to-cart");
            viewInCart = $(cartBtn[i]).find(".view-in-cart");

            var pid = addToCart.data("id");
            let obj = cart.find(o => o.id === pid);
            //console.log("obj", obj);
            //console.log("obj cart", cart);
            //console.log("obj id", pid);
            if (obj != undefined) {
                //addToCart.addClass("d-none");
                //viewInCart.addClass("d-inline-block");
                addToCart.removeClass("d-inline-block");
                addToCart.addClass("d-none");
                viewInCart.removeClass("d-none");
                viewInCart.addClass("d-inline-block");
            } else {
                addToCart.removeClass("d-none");
                addToCart.addClass("d-inline-block");
                viewInCart.removeClass("d-inline-block");
                viewInCart.addClass("d-none");


            }
        }
    } else {
        $(".cart-item-count").html("");
    }
}



// =============================
// Public methods and propeties
// =============================
var obj = {};

// Add to cart
obj.addItemToCart = function(catalogid, id, name, price, discounted_price, catagory, img, qty) {
        for (var item in cart) {
            if (cart[item].id === id) {
                cart[item].qty++;
                saveCart();
                return;
            }
        }
        var item = new Item(catalogid, id, name, price, discounted_price, catagory, img, qty);
        cart.push(item);
        // console.log("added item");
        saveCart();
    }
    // Set count from item
obj.setCountForItem = function(id, qty) {
    for (var i in cart) {
        if (cart[i].id === id) {
            cart[i].qty = qty;
            break;
        }
    }
    saveCart();
};
// Remove item from cart
obj.removeItemFromCart = function(id) {
    for (var item in cart) {
        if (cart[item].id === id) {
            cart[item].qty--;
            if (cart[item].qty === 0) {
                cart.splice(item, 1);
            }
            break;
        }
    }
    // console.log("removed item");
    saveCart();

}

// Remove all items from cart
obj.removeItemFromCartAll = function(name) {
    for (var item in cart) {
        if (cart[item].name === name) {
            cart.splice(item, 1);
            break;
        }
    }
    saveCart();
}

if (localStorage.getItem("shoppingCart") != "[]") {
    const now = new Date();
    var now_time = now.getTime();
    var expire_time = localStorage.getItem("expiretime");
    // console.log(now_time);
    // console.log(expire_time);
    if (now_time > expire_time) {
        // console.log("Exceeded");
        obj.removeItemFromCartAll();
        obj.totalCart();
    } else {
        // console.log(" NOt Exceeded");
        loadCart();
    }

} else {
    // console.log("Empty");
}

// Clear cart
obj.clearCart = function() {
    cart = [];
    saveCart();
}

// Count cart 
obj.totalCount = function() {
    var totalCount = 0;
    for (var item in cart) {
        totalCount += cart[item].qty;
    }
    return totalCount;
}

// Total cart
obj.totalCart = function() {
    var totalCart = 0;
    for (var item in cart) {
        totalCart += cart[item].price * cart[item].qty;
    }
    return Number(totalCart.toFixed(2));
}

//   function removeCart(id){
//       if(cart.length > 0){
//           cart.splice(cart.index(id),1);
//       }
//   }
// For View in cart
cartBtn.on("click", function() {
    //console.log("triggered");
    var $this = $(this),
        addToCart = $this.find(".add-to-cart"),
        viewInCart = $this.find(".view-in-cart");

    var catalogid = addToCart.data('catalogid');
    var id = addToCart.data('id');
    var name = addToCart.data('name');
    var price = Number(addToCart.data('price'));
    var discounted_price = Number(addToCart.data('discounted_price'));
    // console.log(addToCart.data('discounted_price'));
    var catagory = addToCart.data('catagory');
    var img = addToCart.data('img');
    // cart.push(addToCart.data('id'));
    // saveCart();
    if (addToCart.is(':visible')) {
        addToCart.removeClass("d-inline-block");
        addToCart.addClass("d-none");
        viewInCart.removeClass("d-none");
        viewInCart.addClass("d-inline-block");

        track_events('add_to_cart', id, slink);


        count++;
        // console.log(count);
        $(".cart-item-count").html(count);
        obj.addItemToCart(catalogid, id, name, price, discounted_price, catagory, img, 1);
    } else if (viewInCart.is(':visible')) {
        addToCart.removeClass("d-none");
        addToCart.addClass("d-inline-block");
        viewInCart.removeClass("d-inline-block");
        viewInCart.addClass("d-none");

        track_events('remove_product', id, slink);

        count--;
        // console.log(count);
        if (count > 0) {
            $(".cart-item-count").html(count);
        } else {
            $(".cart-item-count").html("");
        }
        obj.removeItemFromCart(id);
    }
});