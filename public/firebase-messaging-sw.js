 /*
 Give the service worker access to Firebase Messaging.
 Note that you can only use Firebase Messaging here, other Firebase libraries are not available in the service worker.
 */
 importScripts('https://www.gstatic.com/firebasejs/8.3.1/firebase-app.js');
 importScripts('https://www.gstatic.com/firebasejs/8.3.1/firebase-messaging.js');
   
 /*
 Initialize the Firebase app in the service worker by passing in the messagingSenderId.
 * New configuration for app@pulseservice.com
 */
 self.addEventListener('notificationclick', function (event) {
    console.log('SW notification click event')
    const url = event.notification.data.FCM_MSG.notification.click_action;
    event.waitUntil(
      clients.matchAll({type: 'window'}).then( windowClients => {
          for (var i = 0; i < windowClients.length; i++) {
              var client = windowClients[i];
              // If so, just focus it.
              if (client.url === url && 'focus' in client) {
                  return client.focus();
              }
          }
          // If not, then open the target URL in a new window/tab.
          if (clients.openWindow) {
              return clients.openWindow(url);
          }
      })
  );
  })
 firebase.initializeApp({
      apiKey: "AIzaSyCuIegfFzl7QxmU5jNaBKFZHV-Pdv6TPIs",
     authDomain: "simplisell.firebaseapp.com",
      projectId: "simplisell",
     storageBucket: "simplisell.appspot.com",
      messagingSenderId: "198993584705",
      appId: "1:198993584705:web:fcc57013d86757ec465cc0"

     /* Testing */
    //  apiKey: "AIzaSyBMjEBbwSe5EEj768x3xN8Em9FBNpk_BvY",
    //  authDomain: "test-firebase-c9465.firebaseapp.com",
    //  projectId: "test-firebase-c9465",
    //  storageBucket: "test-firebase-c9465.appspot.com",
    //  messagingSenderId: "542966593622",
    //  appId: "1:542966593622:web:b6871ff3dea450ffbae3f7",
    //  measurementId: "G-EKG4ZZ27BJ"
     });
  
 /*
 Retrieve an instance of Firebase Messaging so that it can handle background messages.
 */
 const messaging = firebase.messaging(); 
 messaging.setBackgroundMessageHandler(function(payload) {
     console.log(
         "[firebase-messaging-sw.js] Received background message ",
         payload,
     );
     /* Customize notification here */
     const notificationTitle = "Background Message Title";
     const notificationOptions = {
         body: "Background Message body.",
         icon: "/itwonders-web-logo.png",
     };
  
     return self.registration.showNotification(
         notificationTitle,
         notificationOptions,
     );
     
 });