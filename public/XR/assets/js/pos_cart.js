var cartBtn = $(".cart"),
    cartItemCount = $(".cart-item-count");
var count = 0;
var cart = [];

// Constructor
function Item(catalogid, id, name, description, price, discount, discounted_price, catagory, stock, img, qty, min_qty, max_qty,variant_id,product_id,sku) {
    this.catalogid = catalogid;
    this.id = id;
    this.name = name;
    this.description = description;
    this.price = price;
    this.discount = discount;
    this.discounted_price = discounted_price;
    this.catagory = catagory;
    this.stock = stock;
    this.img = img;
    this.qty = qty;
    this.min_qty = min_qty;
    this.max_qty = max_qty;
    this.variant_id=variant_id
    this.product_id=product_id;
    this.sku=sku;
}

// Save cart
function saveCart() {
    const now = new Date();
    var time = now.getTime() + 1000 * 60 * 60 * 2;
    if (customer) {
        customer_cart = 'posCart_' + customer;
        localStorage.setItem(customer_cart, JSON.stringify(cart));
        localStorage.setItem('expiretime', JSON.stringify(time));
    } else {
        localStorage.setItem('posCart', JSON.stringify(cart));
        localStorage.setItem('expiretime', JSON.stringify(time));
    }
}

// =============================
// Public methods and propeties
// =============================
var obj = {};

// Add to cart
obj.addItemToCart = function(catalogid, id, name, description, price, discount, discounted_price, catagory, stock, img, qty, min_qty, max_qty,variant_id,product_id,sku) {
    for (var item in cart) {
        if (cart[item].sku===sku ) {
            cart[item].qty++;
            saveCart();
            return;
        }
    }
    var item = new Item(catalogid, id, name, description, price, discount, discounted_price, catagory, stock, img, qty,min_qty, max_qty,variant_id,product_id,sku);
    cart.push(item);
    saveCart();
}

// Set count from item
obj.setCountForItem = function(sku, qty) {
    for (var i in cart) {
        if (cart[i].sku === sku) {
            cart[i].qty = qty;
            // console.log(cart[i].qty);
              break;
        }
    }
    saveCart();
};

// Remove item from cart
obj.removeItemFromCart = function(sku) {
    for (var item in cart) {
        // if (cart[item].id === id) {
        //     // cart[item].qty--;
        //     // if (cart[item].qty === 0) {
        //     cart.splice(item, 1);
        //     // }
        //     break;
        // }
           if (cart[item].sku === sku) {
            // cart[item].qty--;
            // if (cart[item].qty === 0) {
                cart.splice(item, 1);
            // }
            break;
        }
    }
    saveCart();
}

// Remove all items from cart
obj.removeItemFromCartAll = function(name) {
    for (var item in cart) {
        if (cart[item].name === name) {
            cart.splice(item, 1);
            break;
        }
    }
    saveCart();
}

// Clear cart
obj.clearCart = function() {
    cart = [];
    saveCart();
}

// Count cart
obj.totalCount = function() {
    var totalCount = 0;
    for (var item in cart) {
        totalCount += cart[item].qty;
    }
    return totalCount;
}

// Total cart
obj.totalCart = function() {
    var totalCart = 0;
    for (var item in cart) {
        if (cart[item].discounted_price != 0) {
            // console.log(cart[item].id, cart[item].discounted_price)
            totalCart += cart[item].discounted_price * cart[item].qty;
            // console.log(totalCart);
        } else {
            // console.log(cart[item].id, cart[item].price)
            totalCart += cart[item].price * cart[item].qty;
            // console.log(totalCart);
        }
    }
    return Number(totalCart.toFixed(2));
}

obj.totalDiscount = function() {
    var totalDiscount = 0;
    for (var item in cart) {
        if (cart[item].discount != 0) {
            totalDiscount += cart[item].discount;
        }
    }
    return Number(totalDiscount.toFixed(2));
}

// For View in cart
cartBtn.on("click", function() {
    addProductToCart($(this));
});

$(".cart .quantity-counter").on("click", function(e) {
    e.stopPropagation();
});

function addProductToCart($this) {
    // console.log($this);
    // alert("hi");
    var addToCart = $this.find(".add-to-cart"),
        viewInCart = $this.find(".remove-from-cart");
    var catalogid = addToCart.data('catalogid');
    var product_id=addToCart.data("product_id");
    var variant_id=addToCart.data("variant_id");
    var  sku =addToCart.data("sku");
    var id = addToCart.data('id');
    var name = addToCart.data('name');
    var description = addToCart.data('description');
    var price = Number(addToCart.data('price'));
    var discount = Number(addToCart.data('discount'));
    var discounted_price = Number(addToCart.data('discounted_price'));
    var catagory = addToCart.data('catagory');
    var stock = addToCart.data('stock');
    var img = addToCart.data('img');
    var qty = Number(addToCart.data("quantity"));
    var min_qty = Number(addToCart.data("minimum_quantity"));
    var max_qty = Number(addToCart.data("maximum_quantity"));
    var totalCart = 0;
    if (addToCart.is(':visible')) {
        addToCart.removeClass("d-inline-block");
        addToCart.addClass("d-none");
        viewInCart.removeClass("d-none");
        viewInCart.addClass("d-inline-block");
        viewInCart.addClass("text-white");
        viewInCart.parent().addClass("bg-danger");
        count++;
        $(".cart-item-count").html(count);
        obj.addItemToCart(catalogid, id, name, description, price, discount, discounted_price, catagory, stock, img, qty, min_qty, max_qty,variant_id,product_id,sku);
        // obj.addItemToCart(catalogid, id, name, price, weight, height, width, length, discounted_price, catagory, img, qty, min_qty,variant_id,product_id,sku,max_qty)
        toastr.success('Product added to cart.');
        loadCart();
    } else if (viewInCart.is(':visible')) {
        // alert("byee");
        addToCart.removeClass("d-none");
        addToCart.addClass("d-inline-block");
        viewInCart.removeClass("d-inline-block");
        viewInCart.parent().removeClass("bg-danger");
        viewInCart.addClass("d-none");
        count--;
        if (count > 0) {
            $(".cart-item-count").html(count);
        } else {
            $(".cart-item-count").html("");
        }
        obj.removeItemFromCart(sku);
        toastr.success('Product removed from cart.');
        loadCart();
    }
}
