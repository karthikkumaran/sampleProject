var cartBtn = $(".cart"),
    cartItemCount = $(".cart-item-count");
var count = 0;
var cart = [];

// Constructor
function Item(catalogid, id, name, price, weight, height, width,length, discounted_price, catagory, img, qty,min_qty,variant_id,product_id,sku,max_qty) {
    this.catalogid = catalogid;
    this.id = id;
    this.name = name;
    this.price = price;
    this.weight = weight;
    this.length = length;
    this.width = width;
    this.height = height;
    this.discounted_price = discounted_price;
    this.catagory = catagory;
    this.img = img;
    this.qty = qty;
    this.min_qty = min_qty;
    this.variant_id=variant_id
    this.product_id=product_id;
    this.sku=sku;
    this.max_qty=max_qty;
}

// Save cart
function saveCart() {
    // localStorage.setItem('shoppingCart', JSON.stringify(cart));
    // console.log("save cart", JSON.stringify(cart).price);
    // console.log("save cart", cart);
    // console.log("bye");
    const now = new Date();
    var time = now.getTime() + 1000 * 60 * 60 * 2;

    localStorage.setItem("shoppingCart", JSON.stringify(cart));
    localStorage.setItem("expiretime", JSON.stringify(time));

}

// Load cart
function loadCart() {
    // alert("hi");
    cart = JSON.parse(localStorage.getItem("shoppingCart"));
    var $this = cartBtn,
        addToCart = $this.find(".add-to-cart"),
        viewInCart = $this.find(".view-in-cart");
    cartItemCount = count = cart.length;
    if (cart.length > 0) {
        $(".cart-item-count").html(count);
        for (var i = 0; i < cartBtn.length; i++) {
            addToCart = $(cartBtn[i]).find(".add-to-cart");
            viewInCart = $(cartBtn[i]).find(".view-in-cart");
            var pid = addToCart.data("sku");
            let obj = cart.find((o) => o.sku === pid);
            // console.log("obj cart",cart);
            if (obj != undefined) {
                addToCart.addClass("d-none");
                // $(cartBtn[i]).css('background-color', 'black');
                // console.log($(cartBtn[i]).attr('style','display'));
                $(cartBtn[i]).addClass("btn-warning");
                viewInCart.addClass("d-inline-block");
                viewInCart.removeClass("d-none");
            }
        }
    } else {
        $(".cart-item-count").html("");
    }
}

// =============================
// Public methods and propeties
// =============================
var obj = {};

// Add to cart
obj.addItemToCart = function(catalogid, id, name, price, weight,  height, width, length, discounted_price, catagory, img, qty, min_qty,variant_id,product_id,sku,max_qty) {

    for (var item in cart)  {
        if (cart[item].sku===sku ) {
            cart[item].qty++;
            saveCart();
            return;
        }
    }
    var item = new Item(catalogid, id, name, price,weight,  height, width, length, discounted_price, catagory, img, qty, min_qty,variant_id,product_id,sku,max_qty);
    cart.push(item);
    saveCart();
    //  cart.push(item);
    // console.log(item);
}

// Set count from item
obj.setCountForItem = function(sku, qty) {
    // alert(sku);
    for (var i in cart) {
        if (cart[i].sku === sku) {
            cart[i].qty = qty;
            // console.log(cart[i].qty);
              break;
        }
    }
    saveCart();

};
// Remove item from cart
obj.removeItemFromCart = function(sku) {
    for (var item in cart) {
        if (cart[item].sku === sku) {
            cart[item].qty--;
            if (cart[item].qty === 0) {
                cart.splice(item, 1);
            }
            break;
        }
    }
    saveCart();
}

// Remove all items from cart
obj.removeItemFromCartAll = function(name) {
    for (var item in cart) {
        if (cart[item].name === name) {
            cart.splice(item, 1);
            break;
        }
    }
    saveCart();
}

// Clear cart
obj.clearCart = function() {
    cart = [];
    saveCart();
}

// Count cart
obj.totalCount = function() {
    var totalCount = 0;
    for (var item in cart) {
        totalCount += cart[item].qty;
    }
    return totalCount;
}

// Total cart
obj.totalCart = function() {
    var totalCart = 0;
    for (var item in cart) {
        if (cart[item].discounted_price != 0) {
            // console.log(cart[item].id, cart[item].discounted_price)
            totalCart += cart[item].discounted_price * cart[item].qty;
            // console.log(totalCart);
        } else {
            // console.log(cart[item].id, cart[item].price)
            totalCart += cart[item].price * cart[item].qty;
            // console.log(totalCart);
        }
    }
    return Number(totalCart.toFixed(2));
}

if (localStorage.getItem("shoppingCart") != null) {
    const now = new Date();
    var now_time = now.getTime();
    var expire_time = localStorage.getItem("expiretime");
    // console.log(now_time);
    // console.log(expire_time);
    if (now_time > expire_time) {
        // console.log("Exceeded");
        obj.removeItemFromCartAll();
        obj.totalCart();
    } else {
        // console.log(" NOt Exceeded");
        loadCart();
    }
}
//   function removeCart(id){
//       if(cart.length > 0){
//           cart.splice(cart.index(id),1);
//       }
//   }
// For View in cart
cartBtn.on("click", function() {
    // console.log("Added to acrt");
    addProductToCart($(this));
});

$(".cart .quantity-counter").on("click", function(e) {
    e.stopPropagation();
});

function addProductToCart($this) {

    var addToCart = $this.find(".add-to-cart"),
        viewInCart = $this.find(".view-in-cart");
    var product_id=addToCart.data("product_id");
    var variant_id=addToCart.data("variant_id");
    var  sku=addToCart.data("sku");
    var catalogid = addToCart.data("catalogid");
    var id = addToCart.data("id");
    var name = addToCart.data("name");
    var price = Number(addToCart.data("price"));
    var discounted_price = Number(addToCart.data("discounted_price"));
    var weight = Number(addToCart.data("productweight"));
    var height = Number(addToCart.data("height"));
    var width = Number(addToCart.data("width"));
    var length = Number(addToCart.data("length"));
    var qty = Number(addToCart.data("quantity"));
    var min_qty = Number(addToCart.data("minimum_quantity"));
    var max_qty = Number(addToCart.data("maximum_quantity"));
    // alert(max_qty);
    // console.log(addToCart.data('discounted_price'));
    var catagory = addToCart.data("catagory");
    var img = addToCart.data("img");
    // cart.push(addToCart.data('id'));
    // saveCart();
    if (addToCart.is(":visible")) {
        addToCart.removeClass("d-inline-block");
        addToCart.addClass("d-none");
        // viewInCart.parent().setAttribute('style', '');
        // console.log(viewInCart);
        viewInCart.removeClass("d-none");
        viewInCart.addClass("d-inline-block");
        viewInCart.parent().removeClass("btn-primary");
        viewInCart.parent().addClass("btn-warning");
        url = window.location.pathname;
        slink = url == "/" ? window.location.hostname : url.slice(url.lastIndexOf("/") + 1);
        track_events("add_to_cart", product_id, slink);
        $this.addClass("btn-warning");
        count++;
        $(".cart-item-count").html(count);
      obj.addItemToCart(catalogid, id, name, price, weight, height, width, length, discounted_price, catagory, img, qty, min_qty,variant_id,product_id,sku,max_qty);
    //    console.log(catalogid, id, name, price, weight, height, width, length, discounted_price, catagory, img, qty, min_qty,product_id,variant_id,sku,max_qty);
       toastr.success("Product added to cart.");
    } else {
        // var href= viewInCart.attr('href');
        // window.location.href = href;
    }
}

var addProductToCartMirrar360 = function(data) {

    console.log(data);
    console.log("Add to cart simplisell called")
    // var addToCart = $this.find(".add-to-cart"),
    //     viewInCart = $this.find(".view-in-cart");

    try{
        var catalogid =data.catalogid;
        var id = data.id;
        var name = data.name;
        var price = data.price;
        var discounted_price = data.discounted_price;
        var weight = data.weight;
        var height = data.height;
        var width = data.width;
        var length = data.length;
        var qty = data.qty;
        var min_qty = data.min_qty;
        // console.log(addToCart.data('discounted_price'));
        var catagory = data.catagory;
        var img = data.img;
        // cart.push(addToCart.data('id'));
        // saveCart();
        // if (addToCart.is(":visible")) {
        //     addToCart.removeClass("d-inline-block");
        //     addToCart.addClass("d-none");

        //     // viewInCart.parent().setAttribute('style', '');
        //     // console.log(viewInCart);
        //     viewInCart.removeClass("d-none");
        //     viewInCart.addClass("d-inline-block");
        //     viewInCart.parent().removeClass("btn-primary");
        //     viewInCart.parent().addClass("btn-warning");
            url = window.location.pathname;
            slink = url == "/" ? window.location.hostname : url.slice(url.lastIndexOf("/") + 1);
        //     track_events("add_to_cart", id, slink);

        //     $this.addClass("bg-warning");
            count++;
        //     $(".cart-item-count").html(count);
            obj.addItemToCart(catalogid, id, name, price, weight, height, width, length, discounted_price, catagory, img, qty, min_qty);
        //     toastr.success("Product added to cart.");
        // } else {
        //     // var href= viewInCart.attr('href');
        //     // window.location.href = href;
        // }
    }catch(e){
        console.log(e)
    }
}

window.addProductToCartMirrar360 = addProductToCartMirrar360;
