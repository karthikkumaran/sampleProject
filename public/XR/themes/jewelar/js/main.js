
    function productList() {
      var x = document.getElementById("product_list");
      
      if (x.style.display === "none") {
        x.style.display = "inline-block";
        if($("#details").hasClass('show')) { 
          $("#product_title_btn").removeClass('text-truncate');
        $("#product_back_btn").show();
        $("#product_detail_btn").hide();
      } else {
        $("#product_title_btn").addClass('text-truncate');
        $("#product_back_btn").hide();
        $("#product_detail_btn").show();  
      }  
      } else {
        x.style.display = "none";
        $("#product_title_btn").addClass('text-truncate');
        $("#product_back_btn").hide();
        $("#product_detail_btn").show();  
      }
      
      
    }
    
    function productDetails() {
      var y = document.getElementById("product_list");
      if (y.style.display === "none") {
        y.style.display = "inline-block";
      }
      var x = document.getElementById("product_details");
      if (x.style.display === "none") {
        x.style.display = "block";
        $("#product_title_btn").addClass('text-truncate');
        $("#product_back_btn").hide();
        $("#product_detail_btn").show();
      } else {
        x.style.display = "none";
        $("#product_title_btn").removeClass('text-truncate');
        $("#product_back_btn").show();
        $("#product_detail_btn").hide();
      }
    }
    
    /* swipe function */


    var gesture = {
      x: [],
      y: [],
      match: ''
    },
    countImg= 0,
    currentImg,
    prevImg,
    tolerance = 100;
    swipediv = document.getElementById("myswipe");
    swipediv.addEventListener('touchstart',function(e){
  e.preventDefault()
  for (i=0;i<e.touches.length;i++){
    gesture.x.push(e.touches[i].clientX)
    gesture.y.push(e.touches[i].clientY)
  }
});
swipediv.addEventListener('touchmove',function(e){
  e.preventDefault()
  for (var i=0; i<e.touches.length; i++) {
    gesture.x.push(e.touches[i].clientX)
    gesture.y.push(e.touches[i].clientY)
  }
});
swipediv.addEventListener('touchend',function(e){
     var xTravel = gesture.x[gesture.x.length-1] - gesture.x[0],
      yTravel = gesture.y[gesture.y.length-1] - gesture.y[0];
  if (xTravel<tolerance && xTravel>-tolerance && yTravel<-tolerance){
    gesture.match = 'Swiped Up'
  }
  if (xTravel<tolerance && xTravel>-tolerance && yTravel>tolerance){
    gesture.match = 'Swiped Down'
  }
  if (yTravel<tolerance && yTravel>-tolerance && xTravel<-tolerance){
    gesture.match = 'Swiped Left'
    if (countImg == $('.tryonimg').length) {

    } else {
      countImg = countImg + 1;
      currentImg = $('.tryonimg').eq(countImg);
      prevImg = $('.tryonimg').eq(countImg - 1);
      console.log("next",currentImg);
      $(currentImg).click();
    }
  }
  if (yTravel<tolerance && yTravel>-tolerance && xTravel>tolerance){
    gesture.match = 'Swiped Right'
    
    if (countImg==0) {	
			// countImg=$('.tryonimg').length-1;
      // prevImg = $('.tryonimg').eq(0);
      // currentImg = $('.tryonimg').eq(countImg);
		}
		else {
			countImg=countImg-1;
			//set current and previous images
			currentImg = $('.tryonimg').eq(countImg);
      prevImg = $('.tryonimg').eq(countImg+1);
      $(currentImg).click();
      console.log("previous",currentImg);
    }
  }
  // if (gesture.match!==''){
  //   output.innerHTML = gesture.match
  //   output.style.opacity = 1
  // }
  // setTimeout(function(){
  //   output.style.opacity = 0
  // },500)
  gesture.x = []
  gesture.y = []
  gesture.match = xTravel = yTravel = ''
  
})

    /* end swipe function */