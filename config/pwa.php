<?php

return [
    'base_domain'                    => '127.0.0.1:8000',
    'prefix'                => '/pwa',
    'icons_path'                     => storage_path('/../vendor/codexshaper/laravel-pwa/resources/icons'),
    'scope'                                => '.',
];
