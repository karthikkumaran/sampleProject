# FCM Push Notification(FIREBASE):

1.create Firebase project

* Enter Project Name:

  ![1654589381641.png](image/notification/1654589381641.png)

2.Register Your App in created firebase project

    ![1654589573007.png](image/notification/1654589573007.png)

* create firebase-messaging-sw.js file in public and paste the generated code from registered app like below code.

importScripts('https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.3.2/firebase-messaging.js');

firebase.initializeApp({
    apiKey: "XXXXXXXXXXXXXXXXXXXXXXXXX",
    projectId: "XXXXXXXX",
    messagingSenderId: "XXXXXXXXX",
    appId: "1:XXXXXXXX:web:XXXXXXXXXXX"
}); // These fields are automatically generated after created the project

const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function({data:{title,body,icon}}) {
    return self.registration.showNotification(title,{body,icon});
});

3. Open console page and open project settings and open Cloud messaging.

   ![1654590182564.png](image/notification/1654590182564.png)
4. Copy the server Key and paste it in .env file as FCM_SECRET_KEY.



# **TELEGRAM NOTIFICATION:**

1. Create telegram account.
2. After created, In search bar Enter "BotFather" and search.
3. Click on FatherBot and open chat.
4. In chat box enter "/newbot" . By this command you can create your own bot.(Name Should be Unique)
5. After create bot in chat box enter "/token". It needs your bot's name. After that, It shows you your bot's token.
6. Copy the code and Bot name and Paste in .env file as TELEGRAM_BOT_TOKEN and TELEGRAM_BOT_NAME.
7. After paste it, Open cmd in project root, to listen usages enter Keyword "php artisan telebot:polling".
