<?php

namespace App;

use App\Http\Controllers\Traits\StatisticsTrait;
use App\Traits\Auditable;
use App\Traits\MultiTenantModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class ordersProduct extends Model implements HasMedia
{
    use SoftDeletes, MultiTenantModelTrait, HasMediaTrait, Auditable,StatisticsTrait;

    public $table = 'orders_products';

    protected $with = ['product'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'team_id',
        'user_id',
        'campaign_id',
        'order_id',
        'product_id',
        'price',
        'qty',
        'sku',
        'name',
        'discount',
        'meta_data',
        'notes',
        'created_at',
        'updated_at',
        'deleted_at',
        'product_variant_id'
    ];

    public function order()
    {
        return $this->belongsTo(order::class, 'order_id');
    }

    public function customer()
    {
        return $this->belongsTo(ordersCustomer::class, 'order_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function productVariant()
    {
        return $this->belongsTo(ProductVariant::class, 'product_variant_id');
    }

    public function campaign()
    {
        return $this->belongsTo(Campaign::class, 'campaign_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }
    //
}
