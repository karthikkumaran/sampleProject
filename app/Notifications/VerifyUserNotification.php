<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Seshac\Otp\Otp;

class VerifyUserNotification extends Notification
{
    use Queueable;

    private $user = null;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        $otp =  Otp::setValidity(1440)
        ->setLength(6)
        ->setMaximumOtpsAllowed(10)
        ->setOnlyDigits(true)
        ->setUseSameToken(false)
        ->generate($this->user->email);
        
        $this->user->verification_token = $otp->token;
        $this->user->save();
        
        return (new MailMessage)
            ->view('emails.user_verify', ['otp_token' => $otp->token]);
            // ->line(trans('global.verifyYourUser'))
            // // ->action(trans('global.clickHereToVerify'), route('userVerification', $this->user->verification_token))
            // ->line($otp->token)
            // ->line("Your OTP is valid for 1 day.")
            // ->line(trans('global.thankYouForUsingOurApplication'))
            // ->line(trans('global.contact_support'));
    }

    public function toArray($notifiable)
    {
        return [];
    }
}
