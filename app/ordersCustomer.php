<?php

namespace App;

use App\Traits\Auditable;
use App\Traits\MultiTenantModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use App\Http\Controllers\Traits\StatisticsTrait;


class ordersCustomer extends Model implements HasMedia
{
    use SoftDeletes, MultiTenantModelTrait, HasMediaTrait, Auditable,StatisticsTrait;
    public $table = 'orders_customers';
 
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'customer_id',
        'order_id',
        'team_id',
        'user_id',
        'campaign_id',
        'fname',
        'lname',
        'address1',
        'address2',
        'city',
        'pincode',
        'state',
        'country',
        'email',
        'country_code',
        'mobileno',
        'amobileno',
        'addresstype',
        'meta_data',
        'customer_sign',
        'delivery_note',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function order()
    {
        return $this->belongsTo(orders::class, 'order_id');
    }

    public function orderProducts()
    {
        return $this->hasMany(ordersProduct::class, 'order_id','order_id');
    }

    public function campaign()
    {
        return $this->belongsTo(Campaign::class, 'campaign_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }
    //
}