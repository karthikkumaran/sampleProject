<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class customer_address extends Model
{
    use HasFactory;
    public $table = 'customer_addresses';

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    protected $fillable = [
        'name',
        'mobileno',
        'amobileno',
        'email',
        'address1',
        'address2',
        'city',
        'pincode',
        'state',
        'addresstype',
        'customer_id'
    ];
}
