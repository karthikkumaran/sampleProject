<?php

namespace App\Http\Controllers\Api\V2\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Campaign;
use App\Customer;
use App\Models\OrderStatus;
use App\Models\OrderStatusRemarks;
use App\ordersCustomer;
use App\UserInfo;
use App\orders;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;
use URL;

class CustomerApiController extends Controller
{
    public function getDetail(Request $request)
    {
        $user_id = auth()->user()->id;
        $Customer = Customer::whereId($user_id)->first();
        if(!empty($Customer))
            return (["status" => "Success", "message" => "Customer Details", "data" => $Customer]);
        else
            return (["message" => "No Customer so far"]);
    }

    public function updateProfile(Request $request)
    {
        $user_id=auth()->user()->id;
        $Customer = Customer::find($user_id);
        
        if(!empty($Customer)) {
            $Customer->update($request->only(['email','country_code','mobile','name']));
            return (["status" => "Success", "message" => "Customer Details Updated", "data" => $Customer]);
        }
        else
            return (["message" => "No Customer so far"]);
    }

    public function updateAddress(Request $request)
    {
        $user_id=auth()->user()->id;
        $Customer=Customer::find($user_id);       

        if(!empty($Customer)){
            $meta_data=json_decode($Customer->meta_data,true);
            $meta_data['address']=$request->only('address1','address2','city','pincode','state','country');
            $Customer->update(['meta_data'=>json_encode($meta_data)]);
            return (["status" => "Success", "message" => "Customer Details Updated", "data" => $Customer]);
        }
        else
            return (["message" => "No Customer so far"]);
    }
}