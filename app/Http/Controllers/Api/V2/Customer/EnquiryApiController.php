<?php

namespace App\Http\Controllers\Api\V2\Customer;

use App\Campaign;
use App\Customer;
use App\enquiry;
use App\enquiryCustomer;
use App\enquiryProduct;
use App\Entitycard;
use App\Http\Controllers\Controller;
use App\Product;
use App\UserInfo;
use Illuminate\Http\Request;
use Gate;
use Auth;
use Symfony\Component\HttpFoundation\Response;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;

class EnquiryApiController extends Controller
{

    public function userSettings()
    {
        $user_id = auth()->user()->id;
        $userSettings = UserInfo::where("user_id", $user_id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);
        return $user_meta_data;
    }

    public function store(Request $request)
    {
        $request['catalogs'] = "";
        $request['products'] = "";
        $products_quantity_array = [];
        $product_notes_array = [];


        foreach($request->checkout_products as $key => $checkout_product) {
            $request['catalogs'] = $request['catalogs'] == "" ? $request['catalogs'] . $checkout_product['catalog_link'] : $request['catalogs'] . "," . $checkout_product['catalog_link'];
            $request['products'] = $request['products'] == "" ? $request['products'] . $checkout_product['id'] : $request['products'] . "," . $checkout_product['id'];
            $products_quantity_array = $products_quantity_array + array( $checkout_product['id'] => $checkout_product['qty']);
            $product_notes_array = $product_notes_array + array( $checkout_product['id'] => $checkout_product['notes']);
            // $request->catalogs = $request->catalogs == "" ? $request->catalogs . $checkout_product : "," . $request->catalogs . $checkout_product;
        }

        $request['products_qty'] = $products_quantity_array;
        $request['product_notes'] = $product_notes_array;

        

        //return $request;

        $order_ID = ''; //temprarory value for order_ID instead of removing order_ID in notification.   
        $campaign = '';
        // dd(auth()->user()->roles);
        // if(isset(auth()->user()->team_id))
        $products = Product::whereIn('id', explode(",", $request->products))->get()->groupBy('id')->toArray();

        //return response(["products" => explode(",", $request->catalogs)]);

        if(!empty($products)) {
            $public_link = $request->key;
            $catalogs = array_unique(explode(",", $request->catalogs));
            $product_ids = explode(",", $request->products);
            $product_qty = $request->products_qty;
            $product_notes = $request->product_notes;
            $enquiry_notes = $request->order_notes ?? null;
            $cmeta_data = array();
            $pmeta_data = array();
            $entity_email = array();
            $currency = $request->currency;
            $shareableLink = ShareableLink::where('uuid', $public_link)->first();
            $enquiry_campaign = $shareableLink->shareable;
            $enquiry_campaign_id = $enquiry_campaign->id;
            $enquiry_campaign = Campaign::where("id", $enquiry_campaign_id)->first();

            $enquiry_id = enquiry::where('team_id', $enquiry_campaign->team->id)->count() + 1;

            $enquiry = enquiry::create(array(
                "status" => 0,
                'notes' => $enquiry_notes, 'campaign_id' => $enquiry_campaign_id,
                'user_id' => $enquiry_campaign->user->id, 'team_id' => $enquiry_campaign->team->id,
                'enquiry_id' => $enquiry_id
            ));

            $order_total_price = 0;
            $order_total_discount = 0;
            foreach ($catalogs as $catalog) {

                $shareableLink = ShareableLink::where('uuid', $catalog)->first();
                if (!empty($shareableLink)) {
                    $campaign = $shareableLink->shareable;
                    $campaign_id = $campaign->id;
                    $campaign = Campaign::where("id", $campaign_id)->first();
                    $campaign_name = $campaign->name;
                    $pmeta_data['campaign_name'] = $campaign_name;
                    $campaign_meta_data = json_decode($campaign->meta_data, true);
                    $userSettings = UserInfo::where("user_id", $campaign->user->id)->first();
                    $meta_data = json_decode($userSettings->meta_data, true);
                    $product_total_price = 0;
                    $product_total_discount = 0;
                    $EntityCard = Entitycard::where('campaign_id', $campaign_id)->whereIn('product_id', $product_ids)->get();
                    array_push($entity_email, $EntityCard);

                    foreach ($EntityCard as $key => $card) {
                        $product = $card->product;
                        $product_category = $product->categories ? $product->categories : " ";
                        // dd(count($product->categories));
                        $pmeta_data['category'] = $product_category;
                        $product_stock = $product->stock;
                        if ($product_stock != null) {
                            $product_new_stock = $product_stock - $product_qty[$product->id];
                            Product::where("id", $product->id)->update(array("stock" => $product_new_stock));
                        }

                        $enquiryProduct = enquiryProduct::create(array(
                            "enquiry_id" => $enquiry->id,
                            "product_id" => $product->id, "price" => $product->price,
                            "qty" => $product_qty[$product->id], "sku" => $product->sku, "name" => $product->name,
                            "discount" => $product->discount, "notes" => array_key_exists($product->id, $product_notes) ? $product_notes[$product->id] : null,
                            "meta_data" => json_encode($pmeta_data),
                            'campaign_id' =>  $enquiry_campaign_id,
                            'user_id' => $enquiry_campaign->user->id,
                            'team_id' => $enquiry_campaign->team->id
                        ));
                        if ($product->discount) {
                            $discount_price = $product->price * ($product->discount / 100);
                            $discounted_price = $product->price - $discount_price;
                            $product_total_price += $product_qty[$product->id] * $discounted_price;
                        } else {
                            $product_total_price += $product_qty[$product->id] * $product->price;
                        }
                        $product_total_discount += $product->discount;
                    }
                    $order_total_discount += $product_total_discount;
                    $order_total_price += $product_total_price;
                }
            }

            $cmeta_data['product_total_price'] = $order_total_price;
            $cmeta_data['product_total_discount'] = $order_total_discount;
            $cmeta_data['currency'] = $currency;

            $customerData = array(
                "fname" => $request->fname,
                "email" => $request->email,
                "address1" => $request->apt_number,
                "address2" => $request->landmark,
                "city" => $request->city,
                "pincode" => $request->pincode,
                "state" => $request->state,
                "country" => "india",
                "mobileno" => $request->mnumber,
                "amobileno" => $request->amnumber,
                "addresstype" => $request->addresstype,
                'enquiry_id' => $enquiry->id,
                'meta_data' => json_encode($cmeta_data),
                'campaign_id' => $enquiry_campaign_id,
                'user_id' => $enquiry_campaign->user->id,
                'team_id' => $enquiry_campaign->team->id
            );
            enquiryCustomer::create($customerData);

            return response(['enquiry_id' => $enquiry->id, 'key' => $public_link]);
        } else {
            return ['status' => "error"];
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($enquiry_id)
    {
        //abort_if(Gate::denies('order_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $enquiry = enquiryCustomer::where("enquiry_id", $enquiry_id)->with(['enquiryProducts', 'enquiry'])->get();
        $userSettings = UserInfo::where("user_id", auth()->user()->id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);
        return (compact('enquiry', 'user_meta_data'));
    }
}
