<?php

namespace App\Http\Controllers\Api\V2\Customer;

use App\Campaign;
use App\Entitycard;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\ordersResource;
use App\orders;
use App\Models\OrderStatus;
use App\Models\OrderStatusRemarks;
use App\ordersCustomer;
use App\ordersProduct;
use App\Product;
use App\Transaction;
use App\UserInfo;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Spatie\MediaLibrary\Models\Media;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Models\promocode_user;
use App\Telegram_Bot_Subscriber;
use Carbon\Carbon;
use WeStacks\TeleBot\TeleBot;
use Gabievi\Promocodes\Models\Promocode;
use Gabievi\Promocodes\Facades\Promocodes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OrdersApiController extends Controller
{

    public function index()
    {
        /**
         * @OA\GET(
         *     path="/api/v1/user/orders",
         *     tags={"Orders"},
         *     summary="Show all Orders Placed",
         *     description="API to get the list of Orders Placed",
         *     security={
         *       {"passport": {}}
         *     },
         *     @OA\Response(
         *         response=403,
         *         description="Content access Denied"
         *     ),
         *     @OA\Response(
         *         response=200,
         *         description="Permission Granted"
         *     ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         **/
        // abort_if(Gate::denies('order_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $team_id=auth()->user()->store_owner_id;
        $user_id=auth()->user()->customer_id;
        // dd($team_id,$user_id);
        $orders = ordersCustomer::with(['order' => function($q) {
            $q->with('transactions')->whereNotNull('transaction_id');
        }, 'orderProducts'])->where([['team_id',$team_id],['customer_id',$user_id]])->orderBy('order_id','desc')->get();
        // $orders =  orders::where("user_id", Auth::User()->id)->with(['orderproduct', 'customer'])->get();
        if (count($orders) == 0) {
            return (["message" => "No Orders so far"]);
        }
        return $orders;
    }

    public function calculateTotalAmount($products_ids, $product_qty, $products)
    {
        //return [ 'product' => $products, 'qty' => $products_qty];
        $total_amount = 0;
        $total_discount = 0;
         
        foreach ($products_ids as $key => $value) {
            //return $value;
            $product_price = $products[$value][0]['price'];
            $product_discount = $products[$value][0]['discount'];
            $product_order_price = $product_price;
            $product_order_discount = $product_discount;
            if ($product_discount != null) {
                $product_order_price = $product_price - ($product_price * ($product_discount / 100));
                $product_order_discount =  $product_discount;
            }
            $temp_total = $product_order_price * (int)$product_qty[$value];
            $total_amount += $temp_total;
            $total_discount += $product_order_discount;
        }

        return ['total_amount' => $total_amount, 'total_discount' => $total_discount];
    }

    public function store(Request $request)
    {
        $request['catalogs'] = "";
        $request['products'] = "";
        $products_quantity_array = [];
        $product_notes_array = [];


        foreach($request->checkout_products as $key => $checkout_product) {
            $request['catalogs'] = $request['catalogs'] == "" ? $request['catalogs'] . $checkout_product['catalog_link'] : $request['catalogs'] . "," . $checkout_product['catalog_link'];
            $request['products'] = $request['products'] == "" ? $request['products'] . $checkout_product['id'] : $request['products'] . "," . $checkout_product['id'];
            $products_quantity_array = $products_quantity_array + array( $checkout_product['id'] => $checkout_product['qty']);
            $product_notes_array = $product_notes_array + array( $checkout_product['id'] => $checkout_product['notes']);
            // $request->catalogs = $request->catalogs == "" ? $request->catalogs . $checkout_product : "," . $request->catalogs . $checkout_product;
        }

        $request['products_qty'] = $products_quantity_array;
        $request['product_notes'] = $product_notes_array;

        

        //return $request;

        $order_ID = ''; //temprarory value for order_ID instead of removing order_ID in notification.   
        $campaign = '';
        // dd(auth()->user()->roles);
        // if(isset(auth()->user()->team_id))
        $products = Product::whereIn('id', explode(",", $request->products))->get()->groupBy('id')->toArray();

        //return ["request" => $request, "products" => $products];

        //return $products;
        // else
        //     $products = Product::whereIn('id', array_keys($request->products_qty))->where("team_id",auth()->user()->store_owner_id)->get()->groupBy('id')->toArray();

        // dd($products,auth()->user());
        if(!empty($products)) {
            $total = $this->calculateTotalAmount(explode(",", $request->products),$request['products_qty'], $products);

            //return $total;

            $public_link = $request->key;
            $catalogs = array_unique(explode(",", $request->catalogs));
            $product_ids = explode(",", $request->products);
            $product_qty = $request->products_qty;
            $product_notes = $request->product_notes;
            $order_notes = $request->order_notes ?? null;
            $cmeta_data = array();
            $pmeta_data = array();
            $entity_email = array();
            $currency = $request->currency;
            $shareableLink = ShareableLink::where('uuid', $public_link)->first();
            $order_campaign = $shareableLink->shareable;
            $order_campaign_id = $order_campaign->id;
            $order_campaign = Campaign::where("id", $order_campaign_id)->first();

    
            $order = orders::create(array(
                "status" => 0,
                'notes' => $order_notes,
                'campaign_id' => $order_campaign_id,
                'user_id' => $order_campaign->user->id,
                'team_id' => $order_campaign->team->id,
                'order_mode' => "Online-Storefront"
            ));
            
                $orderstatuses=OrderStatus::create(array(
                    'order_id' => $order->id,
                    'status'=> 0,
                    'changed_by'=> isset(auth()->user()->id) ? auth()->user()->id :null,          
                ));
                $orderstatusremark= OrderStatusRemarks::create(array(
                    'order_id' => $order->id,
                    'status'=> 0,
                    'remarks'=> 'Your Order is Received',
                    'showremarks'=>true
                ));
            foreach ($catalogs as $catalog) {

                $shareableLink = ShareableLink::where('uuid', $catalog)->first();
                if (!empty($shareableLink)) {
                    $campaign = $shareableLink->shareable;
                    $campaign_id = $campaign->id;
                    $campaign = Campaign::where("id", $campaign_id)->first();
                    $campaign_name = $campaign->name;
                    $pmeta_data['campaign_name'] = $campaign_name;
                    $campaign_meta_data = json_decode($campaign->meta_data, true);
                    $userSettings = UserInfo::where("user_id", $campaign->user->id)->first();
                    $meta_data = json_decode($userSettings->meta_data, true);
                    // $product_total_price = 0;
                    // $product_total_discount = 0;
                    $EntityCard = Entitycard::where('campaign_id', $campaign_id)->whereIn('product_id', $product_ids)->get();
                    array_push($entity_email, $EntityCard);

                    foreach ($EntityCard as $key => $card) {
                        $product = $card->product;
                        $product_category = $product->categories ? $product->categories : " ";
                        $pmeta_data['category'] = $product_category;
                        $product_stock = $product->stock;
                        if ($product_stock != null) {
                            $product_new_stock = $product_stock - $product_qty[$product->id];
                            Product::where("id", $product->id)->update(array("stock" => $product_new_stock));
                        }

                        //return ['val'=> array_keys($products_quantity_array), 'pro' => $product->id];

                        $orderProduct = ordersProduct::create(array(
                            "order_id" => $order->id,
                            "product_id" => $product->id, "price" => $product->price,
                            "qty" => $product_qty[$product->id], "sku" => $product->sku, "name" => $product->name,
                            "discount" => $product->discount, "notes" => array_key_exists($product->id, $product_notes) ? $product_notes[$product->id] : null, "meta_data" => json_encode($pmeta_data), 'campaign_id' => $campaign_id, 'user_id' => $campaign->user->id, 'team_id' => $campaign->team->id
                        ));
                    }
                }
            }

            $cmeta_data['product_total_price'] = $total['total_amount'];
            $cmeta_data['coupon_applied'] = false;

            //apply promocode
            if($request->coupon_code_applied=='true')
            {
                promocode_user::create(
                    ['promocode_id'=>$request->coupon_id,
                    'customer_id'=>auth()->guard('customer')->user()->id,
                    'used_at'=>Carbon::now()]);
                $cmeta_data['coupon_applied'] = true;
                $cmeta_data['coupon_id'] = $request->coupon_id;
                $cmeta_data['coupon_code'] = $request->couponCode;
                $cmeta_data['coupon_discount_amount'] = $request->coupon_discount_amount;
                $cmeta_data['product_total_price']= $cmeta_data['product_total_price'] - $request->coupon_discount_amount;
            }
    
            $cmeta_data['product_total_discount'] = $total['total_discount'];
            $cmeta_data['currency'] = $currency;
            $cmeta_data['shipping_charges'] = $request->shipping_charges;
            $cmeta_data['tax_amount'] = $request->tax_amount;
            $cmeta_data['tax_percentage'] = $request->tax_percentage;
            $extra_charges_names = explode(",", $request->extra_charges_names);
            $extra_charges_values = explode(",", $request->extra_charges_values);
            $cmeta_data['extra_charges_names'] = $extra_charges_names;
            $cmeta_data['extra_charges_values'] = $extra_charges_values;

            $customerData = array(
                "fname" => $request->fname,
                "email" => $request->email,
                "address1" => $request->apt_number,
                "address2" => $request->landmark,
                "city" => $request->city,
                "pincode" => $request->pincode,
                "state" => $request->state,
                "country" => "india",
                "country_code" => $request->country_code,
                "mobileno" => $request->mnumber,
                "amobileno" => $request->amnumber,
                "addresstype" => $request->addresstype,
                'order_id' => $order->id,
                'meta_data' => json_encode($cmeta_data),
                'campaign_id' => $order_campaign_id,
                'user_id' => $order_campaign->user->id,
                'team_id' => $order_campaign->team->id,
                'customer_id' => auth()->user()->customer_id,
            );
            
            $ordersCustomer = ordersCustomer::create($customerData);

            if (empty($meta_data['settings']['emailid']))
                $emailFrom = $order_campaign->user->email;
            else
                $emailFrom = $meta_data['settings']['emailid'];

            if (empty($meta_data['settings']['emailfromname']))
                $emailFromName = $order_campaign->user->name;
            else
                $emailFromName = $meta_data['settings']['emailfromname'];

            if (!empty($meta_data['storefront']['storename']))
                $emailSubject = $meta_data['storefront']['storename'] . " - Your order is received";
            else
                $emailSubject = "Your order is received";

            //return $product_qty;

            $data['order']=true;
            $data['userSettings'] = $userSettings;
            $data['product_qty'] = $product_qty;
            $data['product_notes'] = $product_notes;
            $data['order_notes'] = $order_notes;
            $data['customerData'] = $ordersCustomer;
            $data['meta_data'] = $meta_data;
            $data['campaign'] = $order_campaign;
            $data['entity'] = $entity_email;
            $data['toMail'] = $request->email;
            $data['toBcc'] = $emailFrom;
            $data['toName'] = $request->fname;
            $data['subject'] = $emailSubject;
            $data['fromMail'] = $emailFrom;
            $data['fromName'] = $emailFromName;
            $data['mnumber'] = $request->mnumber;
            $data['address'] = $request->apt_number . ",<br>" . $request->landmark . ",<br>" . $request->city . " - " . $request->pincode . ",<br>" . $request->state;
            $response = app('App\Http\Controllers\Admin\MailController')->orderplace_email($data, $order);


            $notification_request=new Request();
            $notification_request->setMethod('POST');
            $notification_request->request->add(
                array("user_id"=>$campaign->user->id,
                "title"=>'Order id: #'.$order_ID,
                "body"=>'Your have received an order from '.$request->fname.'. Order Number is #'.$order_ID.' Login to your SimpliSell account to process the order.')
            );

            if(!isset($meta_data['notification_settings']['order_notification']) ){
                $notification=app('App\Http\Controllers\Admin\NotificationController')->sendNotification($notification_request);
            }else{
                if( $meta_data['notification_settings']['order_notification'] != "off"){
                    $notification=app('App\Http\Controllers\Admin\NotificationController')->sendNotification($notification_request);
                }
            }

            $bot = new TeleBot(env('TELEGRAM_BOT_TOKEN'));
            $telegram_bot_subscribers = Telegram_Bot_Subscriber::where("user_id", $order_campaign->user->id)->get();

            if(!empty($telegram_bot_subscribers)){

                $telegram_text = "Your store has received a new order #". $order_ID ."\n"
                . "<b><a href='". route('admin.orders.show',$order->id) . "'>Click here to view the order.</a></b>\n";
                
                
                foreach ($telegram_bot_subscribers as $subscriber) {
                    $bot->sendMessage([
                        'chat_id' => $subscriber->telegram_chat_id,
                        'parse_mode' => 'HTML',
                        'text' => $telegram_text,
                    ]);
                }
            }


            if ($response->{'original'}['order_id']) {
                $order_id = $response->{'original'}['order_id'];
                return ['order_id' => $order_id];
            }
        } else {
            return ['status' => "error"];
        }
     
    }

    public function store_transaction(Request $request)
    {
        $request['catalogs'] = "";
        $request['products'] = "";
        $products_quantity_array = [];
        $product_notes_array = [];


        foreach($request->checkout_products as $key => $checkout_product) {
            $request['catalogs'] = $request['catalogs'] == "" ? $request['catalogs'] . $checkout_product['catalog_link'] : $request['catalogs'] . "," . $checkout_product['catalog_link'];
            $request['products'] = $request['products'] == "" ? $request['products'] . $checkout_product['id'] : $request['products'] . "," . $checkout_product['id'];
            $products_quantity_array = $products_quantity_array + array( $checkout_product['id'] => $checkout_product['qty']);
            $product_notes_array = $product_notes_array + array( $checkout_product['id'] => $checkout_product['notes']);
            // $request->catalogs = $request->catalogs == "" ? $request->catalogs . $checkout_product : "," . $request->catalogs . $checkout_product;
        }

        $request['products_qty'] = $products_quantity_array;
        $request['product_notes'] = $product_notes_array;

        $products = Product::whereIn('id', explode(",", $request['products']))->get()->groupBy('id')->toArray();
        //return $products;
        $total = $this->calculateTotalAmount(explode(",", $request['products']), $request['products_qty'], $products);

        //return $total;

        $request['COD_total'] = $total;
        if($request->coupon_code_applied=='true')
        {
            $total['total_amount'] = (int)$total['total_amount'] - $request->coupon_discount_amount;
        }
        if(isset($request->shipping_charges))
        {
            $total['total_amount'] += $request->shipping_charges;
        }
        if(isset($request->tax_percentage))
        {
            $total['total_amount'] += $request->tax_amount;
        }
        $extra_charges_names = explode(",", $request->extra_charges_names);
        $extra_charges_values = explode(",", $request->extra_charges_values);
        if(isset($extra_charges_values))
        {
            foreach($extra_charges_values as $key)
            {
                $total['total_amount'] += (int)$key;
            }
        }
        $request['COD_total'] = $total;

        $transaction_details = app('App\Http\Controllers\Admin\PaymentResponseController')->payment($request);

        $shareableLink = ShareableLink::where('uuid', $request->key)->first();
        $order_campaign = $shareableLink->shareable;

        $transaction = Transaction::create([
            'payment_mode' => $request->payment_method,
            'payment_method' => $transaction_details['method'],
            'order_id' => $transaction_details['order_id'],
            'transaction_id' => $transaction_details['trans_id'],
            'invoice_id' => '',
            'status' => 'success',
            'amount' => $transaction_details['amount'],
            'team_id' => $order_campaign->team->id,
            'meta_data' => serialize($transaction_details),
        ]);

        // $order_ref_id = orders::where('team_id',$order_campaign->team->id)->whereNotNull('transaction_id')->count()+1;
        $ref_id = orders::where('team_id',$order_campaign->team->id)->whereNotNull('transaction_id')->count();
        $ref_last_id = orders::select('order_ref_id')->where('team_id',$order_campaign->team->id)->whereNotNull('transaction_id')->latest()->first();
        // return $order_campaign->team->id;
        if(!empty($ref_last_id))
            $ref_prev_id = $ref_last_id->order_ref_id;
        if(!empty($ref_prev_id)) {
            $order_ref_id = ($ref_id == $ref_prev_id)?($ref_id + 1):($ref_prev_id + 1);
        }
        else {
            $order_ref_id = ($ref_id + 1);
        }   

        Orders::find($request->store_order_id)->update(['transaction_id' => $transaction->id, 'order_ref_id' => $order_ref_id]);
        
        if ($request->input('photo', false)) {
            $transaction->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $transaction->id]);
        }



        return ['status' => "Success", 'key' => $request->key, 'transaction_id' => $transaction_details['trans_id'] , 'order_ref_id' => $order_ref_id ];
    }

    public function show($id)
    {
        /**
         * @OA\GET(
         *     path="/api/v1/user/orders/{id}",
         *     tags={"Orders"},
         *     summary="Show a specific Order by ID",
         *     description="API to show a specific Order",
         *     security={
         *       {"passport": {}}
         *     },
         *      @OA\Parameter(
         *          name="id",
         *          description="Order ID",
         *          required=true,
         *          in="path",
         *          @OA\Schema(
         *              type="integer"
         *          )
         *      ),
         *     @OA\Response(
         *         response=403,
         *         description="Content access Denied"
         *     ),
         *     @OA\Response(
         *         response=200,
         *         description="Permission Granted"
         *     ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         *      @OA\Response(
         *          response=404,
         *          description="Invalid Category ID"
         *    ),
         * )
         **/
        // abort_if(Gate::denies('order_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

                
        $team_id=auth()->user()->store_owner_id;
        $user_id=auth()->user()->customer_id;
        $ordersCustomer = ordersCustomer::with(['order', 'orderProducts'])->where([['team_id',$team_id],['customer_id',$user_id],['order_id', $id]])->orderBy('order_id','desc')->first();
        $orderstatus = OrderStatus::where('order_id', $id)->get();  
        $orderstatusremark = OrderStatusRemarks::where('order_id',$id)->get(); 
        if (empty($ordersCustomer)) {
            return (["message" => "No Orders so far"]);
        }
        $data["ordersCustomer"] = $ordersCustomer;
        $data["orderstatus"] = $orderstatus;
        $data["orderstatusremark"] = $orderstatusremark;
        return $data;
    }

    public function update(Request $request, $id)
    {
        /**
         * @OA\POST(
         *     path="/api/v1/user/orders/{id}",
         *     tags={"Orders"},
         *     summary="Update a specific order by ID",
         *     description="Update a specific Order",
         *     security={
         *       {"passport": {}}
         *     },
         *      @OA\Parameter(
         *          name="id",
         *          description="Order ID",
         *          required=true,
         *          in="path",
         *          @OA\Schema(
         *              type="integer"
         *          )
         *      ),
         *   @OA\RequestBody(
         *       required = true,
         *       @OA\MediaType(
         *           mediaType="multipart/form-data",
         *           @OA\Schema( 
         *               @OA\Property(
         *                  property="status",
         *                  type="integer",
         *                  description="Options: 0 - Ordered, 1 - Approved, 2 - Rejected, 3 - Cancelled"
         *               ),
         *               @OA\Property(
         *                  property="notes",
         *                  type="string",
         *                  description="Customer notes on the Order"
         *               ),
         *           ),
         *       ),
         *   ),
         *     @OA\Response(
         *         response=201,
         *         description="Product Category created Successfully"
         *      ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         **/
        abort_if(Gate::denies('order_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $order = orders::find($id)->where('user_id', Auth::User()->id)->get();
        if ($order == []) {
            return ['message' => 'No Such Order to update'];
        }
        if (!$request->status && !$request->notes) {
            return response(['message' => 'Request body missing required parameters. Hint: Check if the request json contains status and notes fields'], 422);
        }
        if (!($request->status >= 0 && $request->status < 4)) {
            return response(['message' => 'Satus has to be one of the following: Options 0 - Ordered, 1 - Approved, 2 - Rejected, 3 - Cancelled'], 422);
        }
        $order->update($request->all());

        return new ordersResource($order);
    }

    public function destroy($id)
    {
        /**
         * @OA\DELETE(
         *     path="/api/v1/user/orders/{id}",
         *     tags={"Orders"},
         *     summary="Delete a specific order Order",
         *     description="Deleting a specific order",
         *     security={
         *       {"passport": {}}
         *     },
         *      @OA\Parameter(
         *          name="id",
         *          description="Order ID",
         *          required=true,
         *          in="path",
         *          @OA\Schema(
         *              type="integer"
         *          )
         *      ),
         *     @OA\Response(
         *         response=200,
         *         description="Product Catalog Deleted Successfully"
         *      ),
         *      @OA\Response(
         *          response=404,
         *          description="Invalid Category ID"
         *      ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         **/
        $order = orders::find($id);
        if ($order && $order->user_id == Auth::User()->id) {

            abort_if(Gate::denies('order_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

            $order->delete();

            return response(['message' => 'Product Catalog Deleted Successfully'], 200);
        }
        return response(['message' => 'No Such Order to Delete']);
    }


    public function get_order_details(Request $request){
        $order_ref_id = $request->order_ref_id;
        $order = false;
        if($order_ref_id){
            $order = Orders::where("order_ref_id", $order_ref_id)->first();
        }
        if($order){
            $order_id = $order->id;
        }
        else{
            return response(["msg" => "Invalid Order Reference ID"]);
        }    

        $orders = ordersCustomer::where('order_id', $order_id)
            ->with(['order' => function($q) {
                $q->with('transactions')->whereNotNull('transaction_id');
            }, 'orderProducts'])
            ->get();

        return response($orders);

    }

}
