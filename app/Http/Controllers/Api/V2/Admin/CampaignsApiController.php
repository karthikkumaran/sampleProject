<?php

namespace App\Http\Controllers\Api\V2\Admin;

use App\Campaign;
use App\Entitycard;
use App\Product;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCampaignRequest;
use App\Http\Requests\UpdateCampaignRequest;
use App\Http\Resources\Admin\CampaignResource;
use App\Http\Requests\StoreEntitycardRequest;
use App\Http\Requests\UpdateEntitycardRequest;
use App\Http\Resources\Admin\EntitycardResource;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Http\Resources\Admin\ProductResource;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CampaignsApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('campaign_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new CampaignResource(Campaign::with(['team'])->get()->load('campaignEntitycards'));
    }

    public function store(StoreCampaignRequest $request)
    {
        $campaign = Campaign::create($request->all());

        return (new CampaignResource($campaign))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Campaign $campaign)
    {
        abort_if(Gate::denies('campaign_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new CampaignResource($campaign->load(['team']));
    }

    public function update(UpdateCampaignRequest $request, Campaign $campaign)
    {
        $campaign->update($request->all());

        return (new CampaignResource($campaign))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Campaign $campaign)
    {
        abort_if(Gate::denies('campaign_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $campaign->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function index_sync()
    {
        // abort_if(Gate::denies('entitycard_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $campaigns = new CampaignResource(Campaign::with(['team'])->get());
        $entitycards = new EntitycardResource(Entitycard::with(['campagin', 'product', 'video', 'asset', 'team'])->get());
        $products = new ProductResource(Product::with(['categories', 'tags', 'team'])->get());

        // return response([ 'campaigns' => $campaigns, 'entitycards' => $entitycards, 'products' => $products]);
        return response([ ['campaigns' => $campaigns], ['entitycards' => $entitycards], ['products' => $products]]);
        // return response(array( 'campaigns' => $campaigns, 'entitycards' => $entitycards, 'products' => $products));

    }
}
