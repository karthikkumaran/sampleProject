<?php

namespace App\Http\Controllers\Api\V2\Admin;

use App\Models\Device;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Webpatser\Uuid\Uuid;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Traits\ApiResponser;

class DevicesApiController extends Controller
{
    use ApiResponser;

    public function checkDevice(Request $request)
    {        
        $deviceData = Device::where("key",$request->device_key);
        $device = $deviceData->first();
        $isDevice = false;
        if(!empty($device)) {
            if($device->status == 1) {
                
                if(!empty($device->device_name) && !empty($device->device_id)) {                
                    if(($device->device_name == $request->device_name) && ($device->device_id == $request->device_id)) {
                        $isDevice = true;
                    } else {
                        $isDevice = false;
                    }
                } else {
                    $isDevice = false;
                }

                if(empty($device->device_id) && empty($device->device_name)) {
                    $isDevice = true;
                }
                if($isDevice) {
                    $deviceData->update($request->only(['device_name','device_id','meta_data']));
                    $now = Carbon::now();
                    $startDate = $device->start_date;
                    $endDate = $device->end_date;
                
                    if (!$now->between($startDate, $endDate)) {
                        return $this->success([], 'Device is Expired', 'expired', Response::HTTP_UNPROCESSABLE_ENTITY);
                    }
                    $user = User::find($device->user_id);
                    $accessToken = $user->createToken('accessToken')->plainTextToken;
                    return $this->success(['accessToken' => $accessToken, 'startDate' => $startDate, 'endDate' => $endDate], 'Approved', 'success', Response::HTTP_OK);
                }
                else {
                    return $this->success([], 'Device is Not Allowed', 'not_allowed', Response::HTTP_UNPROCESSABLE_ENTITY);
                }

            }
            if($device->status == 0)
                return $this->success([], 'Device is deactivated', 'deactivated', Response::HTTP_UNPROCESSABLE_ENTITY);
            else
                return $this->success([], 'Device is blocked', 'blocked', Response::HTTP_UNPROCESSABLE_ENTITY);
            
        }
        else
            return $this->error('No device key so far', Response::HTTP_NOT_FOUND);
    }
    
}
