<?php

namespace App\Http\Controllers\Api\V2\Admin;

use App\Arobject;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreArobjectRequest;
use App\Http\Requests\UpdateArobjectRequest;
use App\Http\Resources\Admin\ArobjectResource;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ArobjectApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('arobject_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ArobjectResource(Arobject::with(['product', 'team'])->get());
    }

    public function store(StoreArobjectRequest $request)
    {
        $arobject = Arobject::create($request->all());

        if ($request->input('object', false)) {
            $arobject->addMedia(storage_path('tmp/uploads/' . $request->input('object')))->toMediaCollection('object');
        }

        return (new ArobjectResource($arobject))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Arobject $arobject)
    {
        abort_if(Gate::denies('arobject_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ArobjectResource($arobject->load(['product', 'team']));
    }

    public function update(UpdateArobjectRequest $request, Arobject $arobject)
    {
        $arobject->update($request->all());

        if ($request->input('object', false)) {
            if (!$arobject->object || $request->input('object') !== $arobject->object->file_name) {
                $arobject->addMedia(storage_path('tmp/uploads/' . $request->input('object')))->toMediaCollection('object');
            }
        } elseif ($arobject->object) {
            $arobject->object->delete();
        }

        return (new ArobjectResource($arobject))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Arobject $arobject)
    {
        abort_if(Gate::denies('arobject_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $arobject->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
