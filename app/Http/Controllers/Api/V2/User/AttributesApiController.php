<?php

namespace App\Http\Controllers\Api\V2\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\ProductResource;
use Gate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\AttributesMetadata;
use Yajra\DataTables\Facades\DataTables;
use Rinvex\Attributes\Models\Attribute;
use Symfony\Component\HttpFoundation\Response;

class AttributesApiController extends Controller
{
    public function index()
    {
        /** @OA\Get(
         * path="/api/v1/user/attributes",
         * summary="Show all the Attributes",
         * description="show the attributes",
         * tags={"Product-attributes"},
         * security={
         *       {"passport": {}}
         *     },
         *     @OA\Response(
         *         response=403,
         *         description="Content access Denied"
         *     ),
         *     @OA\Response(
         *         response=200,
         *         description="Permission Granted"
         *     ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         */
        abort_if(Gate::denies('product_attribute_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return AttributesMetadata::where("team_id", Auth::user()->team_id)->get();
    }


    public function store(Request $request)
    {
        /**
         * @OA\POST(
         * path="/api/v1/user/attributes",
         *     summary="Create a new attribute",
         *     description="API to create the  attributes",
         *     security={
         *       {"passport": {}}
         *     },
         * operationId="authRegister",
         * tags={"Product-attributes"},
         * @OA\RequestBody(
         *    required=true,
         *    description="Pass user attribute update",
         *    @OA\JsonContent(
         *       required={"slug", "label", "type", "value", "group"},
         *       @OA\Property(property="slug", type="list", format="strings", example="ameer"),
         *       @OA\Property(property="label", type="string", format="strings", example="e-kart"),
         *       @OA\Property(property="type", type="string", format="strings", example="checkbox"),
         *       @OA\Property(property="value", type="string", format="strings", example="tv fridge AC"),
         *       @OA\Property(property="group", type="string", format="strings", example="Shipment"),
         *    ),
         * ),
         *     @OA\Response(
         *         response=403,
         *         description="Content access Denied"
         *     ),
         *     @OA\Response(
         *         response=200,
         *         description="Permission Granted"
         *     ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         */

        Attribute::typeMap([
            'varchar' => Varchar::class,
        ]);

        app('rinvex.attributes.entities')->push(Product::class);

        // $product = Product::find(1);
        // $product->fill(['colour' =Attr1 > "red"])->save();
        $label = $request->label;
        $type = $request->type;
        $values = $request->value;
        $attr_group_name = $request->group;
        $attribute_id = [];
        $attribute_slug = [];

        foreach ($label as $data) {
            $attribute = app('rinvex.attributes.attribute')->create([
                'slug' => $data,
                'type' => "varchar",
                'name' => $data,
                'is_required' => 1,
                'entities' => ['App\Product']
            ]);

            $attribute_id[] = $attribute->id;
            $attribute_slug[] = $attribute->slug;
        }


        for ($i = 0; $i < count($label); $i++) {
            AttributesMetadata::create([
                'slug' => $attribute_slug[$i],
                'label' => $label[$i],
                'type' => $type[$i],
                'values' => $values[$i],
                'attribute_id' => $attribute_id[$i],
                'attribute_group_name' => $attr_group_name,
            ]);
        }
        // return redirect()->route('admin.product-attributes.index');

        // $attribute = Attribute::create($request->all());
        // // $attribute->attribute()->sync($request->input('attribute', []));
        // // $attribute->entity()->sync($request->input('entity', []));
        $attribute->save();

        return (new ProductResource($attribute))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show($id)
    {
        /**
         * @OA\GET(
         *     path="/api/v1/user/attributes/{id}",
         *     tags={"Product-attributes"},
         *     summary="Show a specific attribute by ID",
         *     description="API to get the attribute",
         *     security={
         *       {"passport": {}}
         *     },
         *      @OA\Parameter(
         *          name="id",
         *          description="attribute id",
         *          required=true,
         *          in="path",
         *          @OA\Schema(
         *              type="integer"
         *          )
         *      ),
         *     @OA\Response(
         *         response=201,
         *         description="Product Category created Successfully"
         *      ),
         *     @OA\Response(
         *         response=403,
         *         description="Content access Denied"
         *     ),
         *    @OA\Response(
         *         response=422,
         *         description="Missing Required Parameters"
         *     ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         */ {
            abort_if(Gate::denies('product_attribute_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

            return AttributesMetadata::where("team_id", Auth::user()->team_id)->where("id", $id)->get();
        }
    }


    public function update(Request $request, $id)
    {
        /**
         * @OA\POST(
         * path="/api/v1/user/attributes/{id}",
         *     summary="Update a specific attribute by ID",
         *     description="API to update the  attributes",
         *     security={
         *       {"passport": {}}
         *     },
         *      @OA\Parameter(
         *          name="id",
         *          description="attribute id",
         *          required=true,
         *          in="path",
         *          @OA\Schema(
         *              type="integer"
         *          )
         *      ),
         * operationId="authRegister",
         * tags={"Product-attributes"},
         * @OA\RequestBody(
         *    required=true,
         *    description="Pass user attribute update",
         *    @OA\JsonContent(
         *       required={"slug", "label", "type", "value", "group"},
         *       @OA\Property(property="slug", type="string", format="strings", example="e-kart"),
         *       @OA\Property(property="label", type="string", format="strings", example="e-kart"),
         *       @OA\Property(property="type", type="string", format="strings", example="checkbox"),
         *       @OA\Property(property="value", type="string", format="strings", example="tv fridge AC"),
         *       @OA\Property(property="group", type="string", format="strings", example="Shipment"),
         *    ),
         * ),
         * @OA\Response(
         *    response=201,
         *    description="Created successfully",
         *    response=405,
         *    description="you are not authorized",    
         *     ),
         *     @OA\Response(
         *         response=201,
         *         description="Product Category created Successfully"
         *      ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         */


        // Attribute::typeMap([
        //     'varchar' => Varchar::class,
        // ]);

        // app('rinvex.attributes.entities')->push(Product::class);

        // // $product = Product::find(1);
        // // $product->fill(['colour' =Attr1 > "red"])->save();
        // $label = $request->label;
        // $type = $request->type;
        // $values = $request->value;
        // $attr_group_name = $request->group;
        // $attribute_id = [];
        // $attribute_slug = [];

        // foreach ($label as $data) {
        //     $attribute = app('rinvex.attributes.attribute')->create([
        //         'slug' => $data,
        //         'type' => "varchar",
        //         'name' => $data,
        //         'is_required' => 1,
        //         'entities' => ['App\Product']
        //     ]);

        //     $attribute_id[] = $attribute->id;
        //     $attribute_slug[] = $attribute->slug;
        // }


        // for ($i = 0; $i < count($label); $i++) {
        //     AttributesMetadata::create([
        //         'slug' => $attribute_slug[$i],
        //         'label' => $label[$i],
        //         'type' => $type[$i],
        //         'values' => $values[$i],
        //         'attribute_id' => $attribute_id[$i],
        //         'attribute_group_name' => $attr_group_name,
        //     ]);
        // }
        // // return redirect()->route('admin.product-attributes.index');

        // // $attribute = Attribute::create($request->all());
        // // // $attribute->attribute()->sync($request->input('attribute', []));
        // // // $attribute->entity()->sync($request->input('entity', []));
        // $attribute->save();
        $attribute = AttributesMetadata::find($id);
        $attribute->label = $request->label;
        $attribute->type = $request->type;
        $attribute->values = $request->value;
        $attribute->attribute_group_name = $request->group;
        $attribute->save();
        $id = $attribute->attribute_id;
        $attributes = Attribute::find($id);
        $attributes->name = $request->label;
        $attributes->type = $request->type;
        $attributes->group = $request->group;
        $attributes->save();
        // $attribute->attribute_metadata()->sync($request->input('attribute_metadata', []));
        // $product->tags()->sync($request->input('tags', []));
        // $product->save();

        // $attribute->update($request->all());



        return (new ProductResource($attribute));
        // ->response()
        // ->setStatusCode(Response::HTTP_ACCEPTED);
    }


    public function destroy($id)
    {
        /**
         * @OA\Delete(
         *     path="/api/v1/user/attributes/{id}",
         *     tags={"Product-attributes"},
         *     summary="Delete a specific attribute by ID",
         *     description="API to get the delete attributes",
         *     security={
         *       {"passport": {}}
         *     },
         *      @OA\Parameter(
         *          name="id",
         *          description="attribute id",
         *          required=true,
         *          in="path",
         *          @OA\Schema(
         *              type="integer"
         *          )
         *      ),
         * @OA\Response(
         *    response=201,
         *    description="Created successfully",
         *    response=405,
         *    description="you are not authorized",    
         *     ),
         *     @OA\Response(
         *         response=200,
         *         description="Product Catalog Deleted Successfully"
         *      ),
         *      @OA\Response(
         *          response=404,
         *          description="Invalid Category ID"
         *      ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         **/
        abort_if(Gate::denies('product_attribute_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $attribute = AttributesMetadata::where('id', $id)->firstOrFail();
        if ($attribute->delete()) {
            return ['result' => "success", "msg" => "Attribute is Deleted successfully"];
        }
        return response(null, Response::HTTP_NO_CONTENT);
    }
}
