<?php

namespace App\Http\Controllers\Api\V2\User;

use App\Campaign;
use App\Entitycard;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\ordersResource;
use App\orders;
use App\ordersCustomer;
use App\ordersProduct;
use App\Product;
use App\Transaction;
use App\UserInfo;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class OrdersApiController extends Controller
{

    public function index()
    {
        /**
         * @OA\GET(
         *     path="/api/v1/user/orders",
         *     tags={"Orders"},
         *     summary="Show all Orders Placed",
         *     description="API to get the list of Orders Placed",
         *     security={
         *       {"passport": {}}
         *     },
         *     @OA\Response(
         *         response=403,
         *         description="Content access Denied"
         *     ),
         *     @OA\Response(
         *         response=200,
         *         description="Permission Granted"
         *     ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         **/
        abort_if(Gate::denies('order_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $orders =  orders::where("user_id", Auth::User()->id)->with(['orderproduct', 'customer'])->get();
        if ($orders == []) {
            return (["message" => "No Orders so far"]);
        }
        return $orders;
    }

    public function calculateTotalAmount($products_qty, $products)
    {
        $total_amount = 0;
        $total_discount = 0;
        foreach ($products_qty as $key => $value) {
            $product_price = $products[$key][0]['price'];
            $product_discount = $products[$key][0]['discount'];
            $product_order_price = $product_price;
            $product_order_discount = $product_discount;
            if ($product_discount != null) {
                $product_order_price = $product_price - ($product_price * ($product_discount / 100));
                $product_order_discount =  $product_discount;
            }
            $temp_total = $product_order_price * (int)$value;
            $total_amount += $temp_total;
            $total_discount += $product_order_discount;
        }

        return ['total_amount' => $total_amount, 'total_discount' => $total_discount];
    }

    public function store(Request $request)
    {
        $campaign = '';
        $products = Product::whereIn('id', array_keys($request->products_qty))->get()->groupBy('id')->toArray();
        $total = $this->calculateTotalAmount($request->products_qty, $products);
        // $request['COD_total'] = $total;

        // $transaction_details = app('App\Http\Controllers\Admin\PaymentResponseController')->payment($request);

        // if ($transaction_details['flag']) {
        $public_link = $request->key;
        $catalogs = explode(",", $request->catalogs);
        $product_ids = explode(",", $request->products);
        $product_qty = $request->products_qty;
        $product_notes = $request->product_notes;
        $order_notes = $request->order_notes ?? null;
        $cmeta_data = array();
        $pmeta_data = array();
        $entity_email = array();
        $currency = $request->currency;
        $shareableLink = ShareableLink::where('uuid', $public_link)->first();
        $order_campaign = $shareableLink->shareable;
        $order_campaign_id = $order_campaign->id;
        $order_campaign = Campaign::where("id", $order_campaign_id)->first();

        // $transaction = Transaction::create([
        //     'payment_mode' => $request->payment_method, 'payment_method' => $transaction_details['method'],
        //     'order_id' => $transaction_details['order_id'],
        //     'transaction_id' => $transaction_details['trans_id'],
        //     'invoice_id' => '',
        //     'status' => 'success',
        //     'amount' => $transaction_details['amount'],
        //     'team_id' => $order_campaign->team->id,
        //     'meta_data' => serialize($transaction_details),
        // ]);

        // if ($request->input('photo', false)) {
        //     $transaction->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
        // }

        // if ($media = $request->input('ck-media', false)) {
        //     Media::whereIn('id', $media)->update(['model_id' => $transaction->id]);
        // }


        $order_ID = orders::where('team_id', $order_campaign->team->id)->count() + 1;

        $order = orders::create(array(
            "status" => 0,
            'notes' => $order_notes,
            'campaign_id' => $order_campaign_id,
            'user_id' => $order_campaign->user->id,
            'team_id' => $order_campaign->team->id,
            'order_mode' => "Online-Storefront",
            'order_id' => $order_ID
        ));

        // $order_total_price = 0;
        // $order_total_discount = 0;
        foreach ($catalogs as $catalog) {
            $shareableLink = ShareableLink::where('uuid', $catalog)->first();
            if (!empty($shareableLink)) {
                $campaign = $shareableLink->shareable;
                $campaign_id = $campaign->id;
                $campaign = Campaign::where("id", $campaign_id)->first();
                $campaign_name = $campaign->name;
                $pmeta_data['campaign_name'] = $campaign_name;
                $campaign_meta_data = json_decode($campaign->meta_data, true);
                $userSettings = UserInfo::where("user_id", $campaign->user->id)->first();
                $meta_data = json_decode($userSettings->meta_data, true);
                // $product_total_price = 0;
                // $product_total_discount = 0;
                $EntityCard = Entitycard::where('campaign_id', $campaign_id)->whereIn('product_id', $product_ids)->get();
                array_push($entity_email, $EntityCard);

                foreach ($EntityCard as $key => $card) {
                    $product = $card->product;
                    $product_category = $product->categories ? $product->categories : " ";
                    // dd(count($product->categories));
                    $pmeta_data['category'] = $product_category;
                    $product_stock = $product->stock;
                    if ($product_stock != null) {
                        $product_new_stock = $product_stock - $product_qty[$product->id];
                        Product::where("id", $product->id)->update(array("stock" => $product_new_stock));
                    }

                    $orderProduct = ordersProduct::create(array(
                        "order_id" => $order->id,
                        "product_id" => $product->id, "price" => $product->price,
                        "qty" => $product_qty[$product->id], "sku" => $product->sku, "name" => $product->name,
                        "discount" => $product->discount, "notes" => array_key_exists($product->id, $product_notes) ? $product_notes[$product->id] : null, "meta_data" => json_encode($pmeta_data), 'campaign_id' => $campaign_id, 'user_id' => $campaign->user->id, 'team_id' => $campaign->team->id
                    ));
                    // if ($product->discount) {
                    //     $discount_price = $product->price * ($product->discount / 100);
                    //     $discounted_price = $product->price - $discount_price;
                    //     $product_total_price += $product_qty[$product->id] * $discounted_price;
                    // } else {
                    //     $product_total_price += $product_qty[$product->id] * $product->price;
                    // }
                    // $product_total_discount += $product->discount;
                }
                // $order_total_discount += $product_total_discount;
                // $order_total_price += $product_total_price;
            }
        }

        // $cmeta_data['product_total_price'] = $order_total_price;
        // $cmeta_data['product_total_discount'] = $order_total_discount;
        $cmeta_data['product_total_price'] = $total['total_amount'];
        $cmeta_data['product_total_discount'] = $total['total_discount'];
        $cmeta_data['currency'] = $currency;

        $customerData = array(
            "fname" => $request->fname,
            "email" => $request->email,
            "address1" => $request->apt_number,
            "address2" => $request->landmark,
            "city" => $request->city,
            "pincode" => $request->pincode,
            "state" => $request->state,
            "country" => "india",
            "mobileno" => $request->mnumber,
            "amobileno" => $request->amnumber,
            "addresstype" => $request->addresstype,
            'order_id' => $order->id,
            'meta_data' => json_encode($cmeta_data),
            'campaign_id' => $order_campaign_id,
            'user_id' => $order_campaign->user->id,
            'team_id' => $order_campaign->team->id,
            'customer_id' => auth()->guard('customer')->check() ? auth()->guard('customer')->user()->customer_id : '',
        );

        $ordersCustomer = ordersCustomer::create($customerData);

        if (empty($meta_data['settings']['emailid']))
            $emailFrom = $order_campaign->user->email;
        else
            $emailFrom = $meta_data['settings']['emailid'];

        if (empty($meta_data['settings']['emailfromname']))
            $emailFromName = $order_campaign->user->name;
        else
            $emailFromName = $meta_data['settings']['emailfromname'];

        if (!empty($meta_data['storefront']['storename']))
            $emailSubject = $meta_data['storefront']['storename'] . " - Your order is received";
        else
            $emailSubject = "Your order is received";

        $data['userSettings'] = $userSettings;
        $data['product_qty'] = $product_qty;
        $data['product_notes'] = $product_notes;
        $data['order_notes'] = $order_notes;
        $data['customerData'] = $ordersCustomer;
        $data['meta_data'] = $meta_data;
        $data['campaign'] = $order_campaign;
        $data['entity'] = $entity_email;
        $data['toMail'] = $request->email;
        $data['toBcc'] = $emailFrom;
        $data['toName'] = $request->fname;
        $data['subject'] = $emailSubject;
        $data['fromMail'] = $emailFrom;
        $data['fromName'] = $emailFromName;
        $data['mnumber'] = $request->mnumber;
        $data['address'] = $request->apt_number . ",<br>" . $request->landmark . ",<br>" . $request->city . " - " . $request->pincode . ",<br>" . $request->state;
        $response = app('App\Http\Controllers\Admin\MailController')->orderplace_email($data, $order);


        $notification_request = new Request();
        $notification_request->setMethod('POST');
        $notification_request->request->add(
            array(
                "user_id" => $campaign->user->id,
                "title" => 'Order id: #' . $order_ID,
                "body" => 'Your have received an order from ' . $request->fname . '. Order Number is #' . $order_ID . ' Login to your SimpliSell account to process the order.'
            )
        );

        if (!isset($meta_data['notification_settings']['order_notification'])) {
            $notification = app('App\Http\Controllers\Admin\NotificationController')->sendNotification($notification_request);
        } else {
            if ($meta_data['notification_settings']['order_notification'] != "off") {
                $notification = app('App\Http\Controllers\Admin\NotificationController')->sendNotification($notification_request);
            }
        }

        if ($response->{'original'}['order_id']) {
            $order_id = $response->{'original'}['order_id'];
            // dd($campaign, $userSettings);
            return ['order_id' => $order_id];
        }
    }

    public function store_transaction(Request $request)
    {
        // dd($request->all());
        $products = Product::whereIn('id', array_keys($request->products_qty))->get()->groupBy('id')->toArray();
        $total = $this->calculateTotalAmount($request->products_qty, $products);

        $request['COD_total'] = $total;

        $transaction_details = app('App\Http\Controllers\Admin\PaymentResponseController')->payment($request);

        $shareableLink = ShareableLink::where('uuid', $request->key)->first();
        $order_campaign = $shareableLink->shareable;
        $transaction = Transaction::create([
            'payment_mode' => $request->payment_method, 'payment_method' => $transaction_details['method'],
            'order_id' => $transaction_details['order_id'],
            'transaction_id' => $transaction_details['trans_id'],
            'invoice_id' => '',
            'status' => 'success',
            'amount' => $transaction_details['amount'],
            'team_id' => $order_campaign->team->id,
            'meta_data' => serialize($transaction_details),
        ]);

        Orders::find($request->store_order_id)->update(['transaction_id' => $transaction->id]);
        if ($request->input('photo', false)) {
            $transaction->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $transaction->id]);
        }
        return ['key' => $request->key, 'order_id' => $request->store_order_id];
    }

    public function show($id)
    {
        /**
         * @OA\GET(
         *     path="/api/v1/user/orders/{id}",
         *     tags={"Orders"},
         *     summary="Show a specific Order by ID",
         *     description="API to show a specific Order",
         *     security={
         *       {"passport": {}}
         *     },
         *      @OA\Parameter(
         *          name="id",
         *          description="Order ID",
         *          required=true,
         *          in="path",
         *          @OA\Schema(
         *              type="integer"
         *          )
         *      ),
         *     @OA\Response(
         *         response=403,
         *         description="Content access Denied"
         *     ),
         *     @OA\Response(
         *         response=200,
         *         description="Permission Granted"
         *     ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         *      @OA\Response(
         *          response=404,
         *          description="Invalid Category ID"
         *    ),
         * )
         **/
        abort_if(Gate::denies('order_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $order = orders::find($id);
        if ($order && $order->user_id == Auth::User()->id) {

            return new ordersResource($order->load(['team', 'product']));
        }

        return response(['message' => 'No Such Order found'], 404);
    }

    public function update(Request $request, $id)
    {
        /**
         * @OA\POST(
         *     path="/api/v1/user/orders/{id}",
         *     tags={"Orders"},
         *     summary="Update a specific order by ID",
         *     description="Update a specific Order",
         *     security={
         *       {"passport": {}}
         *     },
         *      @OA\Parameter(
         *          name="id",
         *          description="Order ID",
         *          required=true,
         *          in="path",
         *          @OA\Schema(
         *              type="integer"
         *          )
         *      ),
         *   @OA\RequestBody(
         *       required = true,
         *       @OA\MediaType(
         *           mediaType="multipart/form-data",
         *           @OA\Schema( 
         *               @OA\Property(
         *                  property="status",
         *                  type="integer",
         *                  description="Options: 0 - Ordered, 1 - Approved, 2 - Rejected, 3 - Cancelled"
         *               ),
         *               @OA\Property(
         *                  property="notes",
         *                  type="string",
         *                  description="Customer notes on the Order"
         *               ),
         *           ),
         *       ),
         *   ),
         *     @OA\Response(
         *         response=201,
         *         description="Product Category created Successfully"
         *      ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         **/
        abort_if(Gate::denies('order_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $order = orders::find($id)->where('user_id', Auth::User()->id)->get();
        if ($order == []) {
            return ['message' => 'No Such Order to update'];
        }
        if (!$request->status && !$request->notes) {
            return response(['message' => 'Request body missing required parameters. Hint: Check if the request json contains status and notes fields'], 422);
        }
        if (!($request->status >= 0 && $request->status < 4)) {
            return response(['message' => 'Satus has to be one of the following: Options 0 - Ordered, 1 - Approved, 2 - Rejected, 3 - Cancelled'], 422);
        }
        $order->update($request->all());

        return new ordersResource($order);
    }

    public function destroy($id)
    {
        /**
         * @OA\DELETE(
         *     path="/api/v1/user/orders/{id}",
         *     tags={"Orders"},
         *     summary="Delete a specific order Order",
         *     description="Deleting a specific order",
         *     security={
         *       {"passport": {}}
         *     },
         *      @OA\Parameter(
         *          name="id",
         *          description="Order ID",
         *          required=true,
         *          in="path",
         *          @OA\Schema(
         *              type="integer"
         *          )
         *      ),
         *     @OA\Response(
         *         response=200,
         *         description="Product Catalog Deleted Successfully"
         *      ),
         *      @OA\Response(
         *          response=404,
         *          description="Invalid Category ID"
         *      ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         **/
        $order = orders::find($id);
        if ($order && $order->user_id == Auth::User()->id) {

            abort_if(Gate::denies('order_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

            $order->delete();

            return response(['message' => 'Product Catalog Deleted Successfully'], 200);
        }
        return response(['message' => 'No Such Order to Delete']);
    }
}
