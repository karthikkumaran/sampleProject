<?php

namespace App\Http\Controllers\Api\V2\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Gate;
use Auth;
use App\UserInfo;

class StoreInfoApiController extends Controller
{
    public function index()
    {
        $user_id = auth()->user()->id;
        $userSettings = UserInfo::where("user_id", $user_id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);
        return compact('userSettings','user_meta_data');
    }
}
