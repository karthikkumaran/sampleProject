<?php

namespace App\Http\Controllers\Api\V2\User;

use App\Arobject;
use App\Campaign;
use App\Entitycard;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCampaignRequest;
use App\Http\Requests\UpdateCampaignRequest;
use App\Http\Resources\Admin\CampaignResource;
use App\Http\Resources\Admin\EntitycardResource;
use App\Product;
use Gate;
use Auth;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CampaignsApiController extends Controller
{
    public function index()
    {
        /** @OA\Get(
         * path="/api/v1/user/campaigns",
         * summary="Show all the campaigns",
         * description="show the campaigns",
         * tags={"Catalog"},
         * security={
         *       {"passport": {}}
         *     },
         *     @OA\Response(
         *         response=403,
         *         description="Content access Denied"
         *     ),
         *     @OA\Response(
         *         response=200,
         *         description="Permission Granted"
         *     ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         */
        abort_if(Gate::denies('campaign_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $campaigns = Campaign::select('id', 'name', 'is_start', 'type', 'is_active', 'subdomain', 'customdomain', 'preview_link', 'public_link', 'meta_data', 'is_private', 'is_added_home', 'downgradeable')
            ->paginate(10)
            ->load(['campaignEntitycards' => function ($query) {
                $query->limit(-1);
            }]);
        if ($campaigns->isEmpty()) {
            return response(['message' => 'No Data Found'], 404);
        }
        return $campaigns;
        // foreach ($campaigns as $campaign) {
        //     $entityCardProducts = array();
        //     $entityCards = $campaign->campaignEntitycards()->get();
        //     foreach ($entityCards as $entityCard) {
        //         $product = $entityCard->product()->first();
        //         array_push($entityCardProducts, $product->get(
        //             [
        //                 'id', 'name', 'description', 'price', 'sku', 'discount', 'stock',
        //                 'out_of_stock', 'status', 'downgradeable', 'url'
        //             ]
        //         ));
        //     }
        //     array_push($entity_array, [
        //         'campaign' => $campaign->get(
        //             [
        //                 'id', 'name', 'is_start', 'type', 'is_active', 'subdomain', 'customdomain', 'preview_link',
        //                 'public_link', 'meta_data', 'is_private', 'is_added_home', 'downgradeable'
        //             ]
        //         )[0], 'products' => $entityCardProducts
        //     ]);
        // }
    }

    public function EntityCardProduct(Request $request)
    {
        /**
         * @OA\GET(
         *     path="/api/v1/user/campaigns/products/{id}",
         *     tags={"Catalog"},
         *     summary="show all the products present inside a Catalog",
         *     description="API to get products in a Catalog",
         *     security={
         *       {"passport": {}}
         *     },
         *      @OA\Parameter(
         *          name="id",
         *          description="Catalog id",
         *          required=true,
         *          in="path",
         *          @OA\Schema(
         *              type="integer"
         *          )
         *      ),
         *     @OA\Response(
         *         response=403,
         *         description="Content access Denied"
         *     ),
         *     @OA\Response(
         *         response=200,
         *         description="Permission Granted"
         *     ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         *      @OA\Response(
         *          response=404,
         *          description="Invalid Category ID"
         *    ),
         * )
         **/
        abort_if(Gate::denies('product_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $sort = ['id', 'desc'];
        if(isset($request->limit) && !empty($request->limit)) {
            $page = $request->limit;
        } else {
            $page = 10;
        }
        $campaign_id = $request->campaign_id;
        if (isset($request->q) && !empty($request->q)) {
            $q = $request->q;
            $is_search = true;
        } else {
            $is_search = false;
        }

        if(isset($request->category_id) && !empty($request->category_id)) {
            $category_id = $request->category_id;
        }

        if($is_search == true) {
            if(isset($category_id)) {
                $data = Entitycard::with(['product', 'arobject'])->where("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($q) {
                    $query->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                })->whereHas('product',function($query2) use ($category_id) {
                    $query2->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1)->where('catagory_id', $category_id);
                })->orderBy($sort[0], $sort[1])->paginate($page);
            } else {
                $data = Entitycard::with(['product', 'arobject'])->where("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($q) {
                    $query->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                })->whereHas('product',function($query2) {
                    $query2->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy($sort[0], $sort[1])->paginate($page);
            }
        } else if(isset($category_id)) {
            $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($category_id) {
                $query->where('catagory_id', $category_id);
            })->whereHas('product', function($query) {
                $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy($sort[0], $sort[1])->paginate($page);
        } else {
            $data = Entitycard::with(['product', 'arobject'])->where("campaign_id", $campaign_id)->whereHas('product',function($query2) {
                $query2->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy($sort[0], $sort[1])->paginate($page);
        }
        if ($data->isEmpty()) {
            return response(['message' => 'No Data Found'], 404);
        }
        return response($data, 200);
    }

    
    public function catalogueByCard(Request $request){
        return new CampaignResource(Campaign::with(['team','campaignEntitycards'])->where('id',$request->id)->first());
    }

    public function getCardById(Request $request) {
        return new CampaignResource(Entitycard::with(['team','product'])->where('id',$request->id)->first());
    }

    public function store(StoreCampaignRequest $request)
    {
        /**
         * @OA\Post(
         * path="/api/v1/user/campaigns",
         * summary="Create a new Campaign",
         * description="API to create campaigns",
         * operationId="authRegister",
         * tags={"Catalog"},
         * security={
         *       {"passport": {}}
         *     },
         * @OA\RequestBody(
         *    required=true,
         *    description="Pass campaign data",
         *    @OA\JsonContent(
         *       required={"name", "is_start", "type"},
         *       @OA\Property(property="name", type="string", format="strings", example="sports"),
         *       @OA\Property(property="is_start", type="int", format="int", example=1),
         *       @OA\Property(property="is_private", type="int", format="int", example=0),
         *       @OA\Property(property="type", type="int", format="int", example=3),
         *       @OA\Property(property="is_active", type="int", format="int", example=1),
         *    ),
         * ),
         *     @OA\Response(
         *         response=201,
         *         description="Product Category created Successfully"
         *      ),
         *     @OA\Response(
         *         response=403,
         *         description="Content access Denied"
         *     ),
         *    @OA\Response(
         *         response=422,
         *         description="Missing Required Parameters"
         *     ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         */
        $user = Auth::user();
        $team = $user->team;
        $name = $request->name;
        $is_start = $request->is_start;
        $is_private = $request->is_private;
        $is_added_home = $request->is_added_home;
        $is_expired = $request->is_expired;
        $campaign = Campaign::create(array_merge($request->all(), ['name' => $name, 'is_start' => $is_start, 'is_active' => 1, 'user_id' => auth()->user()->id, 'team_id' => $team->id, 'is_private' => $is_private, 'is_added_home' => $is_added_home]));

        $preview_link = ShareableLink::buildFor($campaign);
        $publish_link = ShareableLink::buildFor($campaign);
        $preview_link->setActive();
        if ($is_start == 1) {
            $publish_link->setActive();
        }
        if (!empty($is_private)) {
            if (!empty($request->private_password)) {
                $publish_link->setPassword($request->private_password);
            }
        }
        if (!empty($is_expired)) {
            if (!empty($request->expired_date)) {
                $publish_link->setExpirationDate(Carbon::parse($request->expired_date));
            }
        }
        //$preview_link->setPassword('muthu');
        // $campaign->update(array_merge($request->all(),array('preview_link'=> $preview_link->build()->uuid, 'public_link'=> $publish_link->build()->uuid)));
        $campaign->preview_link = $preview_link->build()->uuid;
        $campaign->public_link = $publish_link->build()->uuid;
        $campaign->save();
        // $campaign = Campaign::create($request->all());

        return (new CampaignResource($campaign))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show($id)
    {
        /**
         * @OA\GET(
         *     path="/api/v1/user/campaigns/{id}",
         *     tags={"Catalog"},
         *     summary="show a specific campaign by ID",
         *     description="API to get campaigns",
         *     security={
         *       {"passport": {}}
         *     },
         *      @OA\Parameter(
         *          name="id",
         *          description="campaigns id",
         *          required=true,
         *          in="path",
         *          @OA\Schema(
         *              type="integer"
         *          )
         *      ),
         *     @OA\Response(
         *         response=403,
         *         description="Content access Denied"
         *     ),
         *     @OA\Response(
         *         response=200,
         *         description="Permission Granted"
         *     ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         *      @OA\Response(
         *          response=404,
         *          description="Invalid Category ID"
         *    ),
         * )
         **/
        abort_if(Gate::denies('campaign_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $campaign = Campaign::find($id);
        if ($campaign && $campaign->user_id == Auth::User()->id) {
            $entityCardProducts = array();
            $entityCards = $campaign->campaignEntitycards()->get();
            foreach ($entityCards as $entityCard) {
                $product = $entityCard->product()->get();
                array_push($entityCardProducts, $product);
            }
            return ['campaign' => $campaign, 'product' => $entityCardProducts];
        }

        return response(['message' => 'No Data found'], 404);
    }

    public function update(UpdateCampaignRequest $request, Campaign $campaign)
    {
        /**
         * @OA\Put(
         * path="/api/v1/user/campaigns/{id}",
         *     summary="Update a specific campaign by ID",
         *     description="API to update the campaigns",
         *     security={
         *       {"passport": {}}
         *     },
         *      @OA\Parameter(
         *          name="id",
         *          description="campaign id",
         *          required=true,
         *          in="path",
         *          @OA\Schema(
         *              type="integer"
         *          )
         *      ),
         * operationId="authRegister",
         * tags={"Catalog"},
         * @OA\RequestBody(
         *    required=true,
         *    description="API to update the campaign",
         *    @OA\JsonContent(
         *       required={"name", "is_start", "type"},
         *       @OA\Property(property="name", type="string", format="strings", example="sports"),
         *       @OA\Property(property="is_start", type="int", format="int", example=1),
         *       @OA\Property(property="is_private", type="int", format="int", example=0),
         *       @OA\Property(property="type", type="int", format="int", example=3),
         *       @OA\Property(property="is_active", type="int", format="int", example=1),
         *    ),
         * ),
         *     @OA\Response(
         *         response=201,
         *         description="Product Category created Successfully"
         *    ),
         *      @OA\Response(
         *          response=404,
         *          description="Invalid Category ID"
         *    ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         *    @OA\Response(
         *         response=422,
         *         description="Missing Required Parameters"
         *     ),
         * )
         */
        $campaign->update($request->all());
        return ($campaign);
        $user = Auth::user();
        $team = $user->team;
        $name = $request->name;
        $is_start = $request->is_start;
        $is_private = $request->is_private;
        $is_added_home = $request->is_added_home;
        $is_expired = $request->is_expired;
        // $campaign = Campaign::create(array_merge($request->all(), ['name' => $name, 'is_start' => $is_start,'is_active' => 1,'user_id' => auth()->user()->id,'team_id' => $team->id,'is_private' => $is_private,'is_added_home' => $is_added_home]));

        $preview_link = ShareableLink::buildFor($campaign);
        $publish_link = ShareableLink::buildFor($campaign);
        $preview_link->setActive();
        if ($is_start == 1) {
            $publish_link->setActive();
        }
        if (!empty($is_private)) {
            if (!empty($request->private_password)) {
                $publish_link->setPassword($request->private_password);
            }
        }
        if (!empty($is_expired)) {
            if (!empty($request->expired_date)) {
                $publish_link->setExpirationDate(Carbon::parse($request->expired_date));
            }
        }
        //$preview_link->setPassword('muthu');
        // $campaign->update(array_merge($request->all(),array('preview_link'=> $preview_link->build()->uuid, 'public_link'=> $publish_link->build()->uuid)));
        $campaign->preview_link = $preview_link->build()->uuid;
        $campaign->public_link = $publish_link->build()->uuid;
        $campaign->save();


        return ($campaign);
    }

    public function destroy($id)
    {
        /**
         * @OA\Delete(
         *     path="/api/v1/user/campaigns/{id}",
         *     tags={"Catalog"},
         *     summary="Delete a specific campaign by ID",
         *     description="API to get the delete campaign",
         *     security={
         *       {"passport": {}}
         *     },
         *      @OA\Parameter(
         *          name="id",
         *          description="campaign id",
         *          required=true,
         *          in="path",
         *          @OA\Schema(
         *              type="integer"
         *          )
         *      ),
         *     @OA\Response(
         *         response=200,
         *         description="Product Catalog Deleted Successfully"
         *      ),
         *      @OA\Response(
         *          response=404,
         *          description="Invalid Category ID"
         *      ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         **/
        $campaign = Campaign::find($id);
        if ($campaign && $campaign->user_id == Auth::User()->team_id) {
            abort_if(Gate::denies('campaign_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

            $campaign->delete();
            return response(['message' => 'Catalog Deleted Successfully'], 200);
        }

        return response(['message' => 'No Data Found'], 404);
    }
}
