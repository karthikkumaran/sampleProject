<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\CompanyInfo;
use App\Http\Controllers\Controller;
use App\Team;
use App\User;
use App\UserInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;
use Symfony\Component\HttpFoundation\Response;


class AuthApiController extends Controller
{

    public function register(Request $request)
    {

        /**
         * @OA\Post(
         * path="/api/v1/register",
         * summary="Register",
         * description="Register using username, email, password",
         * operationId="authRegister",
         * tags={"auth"},
         * @OA\RequestBody(
         *    required=true,
         *    description="Pass user credentials",
         *    @OA\JsonContent(
         *       required={"name","email","password"},
         *       @OA\Property(property="name", type="string", format="text", example="user"),
         *       @OA\Property(property="email", type="string", format="email", example="user@user.com"),
         *       @OA\Property(property="password", type="string", format="password", example="password"),
         *       @OA\Property(property="password_confirmation", type="string", format="password_confirmation", example="password"),
         *    ),
         * ),
         * @OA\Response(
         *    response=422,
         *    description="Wrong credentials response",
         *    @OA\JsonContent(
         *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
         *        )
         * ),
         * @OA\Response(
         *    response=403,
         *    description="Your account is yet to be approved",
         *    @OA\JsonContent(
         *       @OA\Property(property="message", type="string", example="Already Registered, admin approval required")
         *        )
         *      ),
         * @OA\Response(
         *    response=400,
         *    description="Your email not verified",
         *    @OA\JsonContent(
         *       @OA\Property(property="message", type="string", example="Already Registered, Email verification required")
         *        )
         *      )
         * )
         * )
         */
        if(!$request->email || !$request->name || !$request->password || !$request->password_confirmation){
            return response(['message' => 'Required parameters Missing: Hint check if all the required parameters are passed - Required parameters -> [email, name, password, password_confirmation]'], 422);
        };
        $loginData = $request->validate([
            'email' => 'email|required',
            'password' => 'required'
        ]);
        if(Auth::attempt($loginData)){
            if (!Auth::attempt($loginData)) {
                return response(['message' => 'Invalid Credentials'], 422);
            }
            if (!Auth::user()->verified) {
                return response(['Message' => 'Already Registered - Email Verification required'], 400);
            } elseif (!Auth::user()->approved) {
                return response(['Message' => 'Already Registered - Your account is under approval'], 403);
            }
            return response(['message'=>'User already Registered - Try Login'], 403);
        }
        $request->validate([
            'name' => 'required|max:55',
            'email' => 'email|required|unique:users',
            'password' => 'required|confirmed'
        ]);

        if (empty($request->team_id)) {
            $teams = Team::create(array("name" => $request->email));
            $teamid = $teams->id;
        } else {
            $teamid = $request->team_id;
        }
        $request['team_id'] = $teamid;
        $user = User::create($request->all());  

        $userInfo = UserInfo::create(array('user_id' => $user->id));

        $preview_link = ShareableLink::buildFor($userInfo);
        $publish_link = ShareableLink::buildFor($userInfo);
        $preview_link->setActive();

        $userInfo->update(array('id' => $userInfo->id, 'preview_link' => $preview_link->build()->uuid, 'public_link' => $publish_link->build()->uuid));
        $companyInfo = CompanyInfo::create(array('user_id' => $user->id));

        return response([ 'user' => $user, 'Message' => 'A verification mail will be sent to your registered mail-id']);
    }

    public function login(Request $request)
    {

        /**
         * @OA\Post(
         * path="/api/v1/login",
         * summary="Login",
         * description="Login using email, password",
         * operationId="authLogin",
         * tags={"auth"},
         * @OA\RequestBody(
         *    required=true,
         *    description="Pass user credentials",
         *    @OA\JsonContent(
         *       required={"email","password"},
         *       @OA\Property(property="email", type="string", format="email", example="user@user.com"),
         *       @OA\Property(property="password", type="string", format="password", example="password"),
         *    ),
         * ),
         * @OA\Response(
         *    response=422,
         *    description="Wrong credentials response",
         *    @OA\JsonContent(
         *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
         *       )
         *      ),
         * @OA\Response(
         *    response=403,
         *    description="Your account is yet to be approved",
         *    @OA\JsonContent(
         *       @OA\Property(property="message", type="string", example="Sorry, admin approval required")
         *        )
         *      ),
         * @OA\Response(
         *    response=400,
         *    description="Your email not verified",
         *    @OA\JsonContent(
         *       @OA\Property(property="message", type="string", example="Sorry, Email verification required")
         *        )
         *      )
         * )
         */
        $loginData = $request->validate([
            'email' => 'email|required',
            'password' => 'required'
        ]);
        

        if (!Auth::attempt($loginData)) {
            return response(['message' => 'Invalid Credentials'],422);
        }

        if(!Auth::user()->verified){
            return response(['Message' => 'Email Verification required'],400);
        }elseif(!Auth::user()->approved){
            return response(['Message' => 'Your account is under approval'],403);
        }

        // $accessToken = Auth::user()->createToken('authToken')->accessToken;
        $accessToken = Auth::user()->createToken('authToken')->plainTextToken;

        // return $accessToken;
        return response(['user' => Auth::user()->id, 'access_token' => $accessToken],200);

    }
}
