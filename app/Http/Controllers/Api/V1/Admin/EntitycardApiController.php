<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Entitycard;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreEntitycardRequest;
use App\Http\Requests\UpdateEntitycardRequest;
use App\Http\Resources\Admin\EntitycardResource;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class EntitycardApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('entitycard_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new EntitycardResource(Entitycard::with(['campagin', 'product', 'video', 'asset', 'team'])->get());
    }

    public function store(StoreEntitycardRequest $request)
    {
        $entitycard = Entitycard::create($request->all());

        return (new EntitycardResource($entitycard))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(Entitycard $entitycard)
    {
        abort_if(Gate::denies('entitycard_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new EntitycardResource($entitycard->load(['campagin', 'product', 'video', 'asset', 'team']));
    }

    public function update(UpdateEntitycardRequest $request, Entitycard $entitycard)
    {
        $entitycard->update($request->all());

        return (new EntitycardResource($entitycard))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(Entitycard $entitycard)
    {
        abort_if(Gate::denies('entitycard_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $entitycard->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
