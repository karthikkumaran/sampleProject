<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Campaign;
use App\Entitycard;
use App\Product;
use App\Arobject;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCampaignRequest;
use App\Http\Requests\UpdateCampaignRequest;
use App\Http\Resources\Admin\CampaignResource;
use App\Http\Requests\StoreEntitycardRequest;
use App\Http\Requests\UpdateEntitycardRequest;
use App\Http\Resources\Admin\EntitycardResource;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Http\Resources\Admin\ProductResource;
use App\Http\Resources\Admin\ArobjectResource;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Traits\ApiResponser;


class SyncApiController extends Controller
{
    use ApiResponser;

    public function index()
    {
        // abort_if(Gate::denies('entitycard_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $campaigns = new CampaignResource(Campaign::with(['team'])->get());
        $entitycards = new EntitycardResource(Entitycard::with(['team'])->get());
        $products = new ProductResource(Product::with(['categories', 'tags', 'team','arobject'])->get());
        $arobject = new ArobjectResource(Arobject::with(['team'])->get());


        return $this->success([ 'campaigns' => $campaigns, 'entitycards' => $entitycards, 'products' => $products], 'getting', 'success', Response::HTTP_OK);


        // $campaigns = Campaign::select('id', 'name', 'is_start', 'type', 'is_active', 'subdomain', 'customdomain', 'preview_link', 'public_link', 'meta_data', 'is_private', 'is_added_home', 'downgradeable')
        //     ->get()
        //     ->load(['campaignEntitycards']);
        // if ($campaigns->isEmpty()) {
        //     return response(['message' => 'No Data Found'], Response::HTTP_NOT_FOUND);
        // }
        // return $this->success($campaigns, 'getting', 'success', Response::HTTP_OK);

        // return $campaigns;

    } 
}
