<?php

namespace App\Http\Controllers\Api\V1\Frontend;

use App\Models\Device;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Webpatser\Uuid\Uuid;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Traits\ApiResponser;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;
use App\UserInfo;
use App\Product;
use App\ProductCategory;
use App\Entitycard;

class ProductListApiController extends Controller
{
    use ApiResponser;

    public function index(Request $request)
    {
        $shareableLink = ShareableLink::where('uuid', $request->link)->first();
        if(empty($shareableLink)){
            return response(['status' => "failed", 'message' => 'No Data Found'], 200);
        }
        
            $campaign = $shareableLink->shareable;
            if(!empty($campaign)) {
                if($campaign->table=="campaigns") {
                    $campaign_id = $campaign->id;
                    $team_id = $campaign->team_id;
                }
                else 
                    return response(['status' => "failed", 'message' => 'No Data Found'], 200);
            } else 
            return response(['status' => "failed", 'message' => 'No Data Found'], 200);
            

        
        $sort = ['id', 'desc'];
        if(isset($request->limit) && !empty($request->limit)) {
            $page = $request->limit;
        } else {
            $page = 10;
        }
        // $campaign_id = $request->campaign_id;
        if (isset($request->q) && !empty($request->q)) {
            $q = $request->q;
            $is_search = true;
        } else {
            $is_search = false;
        }

        if(isset($request->category_id) && !empty($request->category_id)) {
            $category_id = $request->category_id;
        }

        if($is_search == true) {
            if(isset($category_id)) {
                $data = Entitycard::with(['product', 'arobject'])->where("campaign_id", $campaign_id)->where("team_id", $team_id)->whereHas('product', function ($query) use ($q) {
                    $query->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                })->whereHas('product',function($query2) use ($category_id) {
                    $query2->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1)->where('catagory_id', $category_id);
                })->orderBy($sort[0], $sort[1])->paginate($page);
            } else {
                $data = Entitycard::with(['product', 'arobject'])->where("campaign_id", $campaign_id)->where("team_id", $team_id)->whereHas('product', function ($query) use ($q) {
                    $query->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                })->whereHas('product',function($query2) {
                    $query2->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy($sort[0], $sort[1])->paginate($page);
            }
        } else if(isset($category_id)) {
            $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->where("team_id", $team_id)->whereHas('product', function ($query) use ($category_id) {
                $query->where('catagory_id', $category_id);
            })->whereHas('product', function($query) {
                $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy($sort[0], $sort[1])->paginate($page);
        } else {
            $data = Entitycard::with(['product', 'arobject'])->where("campaign_id", $campaign_id)->where("team_id", $team_id)->whereHas('product',function($query2) {
                $query2->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy($sort[0], $sort[1])->paginate($page);
        }
        if ($data->isEmpty()) {
            return response(['status' => "failed", 'message' => 'No Data Found'], 200);
        }
        $dataO = $data;
        $data = $data->map(function($item, $index) {
            return [
              "id" => $item["product"]["id"],
              "name" => $item["product"]["name"],
              "sku" => $item["product"]["sku"],
              "description" => $item["product"]["description"],
              "price" => $item["product"]["price"],
              "imgUrls" => $item["product"]["photo"]->pluck('fullUrl'),
              "thumbUrls" => $item["product"]["photo"]->pluck('thumbnailFullUrl'),
            //   "tryonImg" =>  $item["arobject"]->pluck('position') ,$item["arobject"]->pluck('object')->pluck('fullUrl')
            "tryonImg" =>  $item["arobject"]->map(function($item1, $index1) { 
                if($item1["position"] == "jwl-nck")
                return ["neck" => $item1["object"]["fullUrl"] ];
                if($item1["position"] == "jwl-ear-l" || $item1["position"] == "jwl-ear-r")
                return ["ear" => $item1["object"]["fullUrl"] ];
                if($item1["position"] == "watch")
                return ["watch" => $item1["object"]["fullUrl"] ];
                if($item1["position"] == "ring")
                return ["ring" => $item1["object"]["fullUrl"] ];
                if($item1["position"] == "apparel")
                return ["apparel" => $item1["object"]["fullUrl"] ];
            })
            ];
           });
        //    dd($dataO);
            $data1["data"] = $data;
            $data1["page"] = array("current_page" => $dataO->currentPage(), "last_page" => $dataO->lastPage(), "total" => $dataO->total()); 
            // $data1[] = array("current_page" => $dataO->currentPage(), "last_page" => $dataO->lastPage(), "total" => $dataO->total()); 
            
        return response(['status' => "success", 'message' => 'Data Founded', 'data' => $data1], 200);

    } 
    
}
