<?php

namespace App\Http\Controllers\Api\V1\Customer;

use App\CompanyInfo;
use App\Customer;
use App\Http\Controllers\Controller;
use App\Team;
use App\User;
use App\UserInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;
use Illuminate\Support\Facades\Validator;

class CustomerAuthApiController extends Controller
{
    // public function register(Request $request)
    // {

    //     /**
    //      * @OA\Post(
    //      * path="/api/v1/register",
    //      * summary="Register",
    //      * description="Register using username, email, password",
    //      * operationId="authRegister",
    //      * tags={"auth"},
    //      * @OA\RequestBody(
    //      *    required=true,
    //      *    description="Pass user credentials",
    //      *    @OA\JsonContent(
    //      *       required={"name","email","password"},
    //      *       @OA\Property(property="name", type="string", format="text", example="user"),
    //      *       @OA\Property(property="email", type="string", format="email", example="user@user.com"),
    //      *       @OA\Property(property="password", type="string", format="password", example="password"),
    //      *       @OA\Property(property="password_confirmation", type="string", format="password_confirmation", example="password"),
    //      *    ),
    //      * ),
    //      * @OA\Response(
    //      *    response=422,
    //      *    description="Wrong credentials response",
    //      *    @OA\JsonContent(
    //      *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
    //      *        )
    //      * ),
    //      * @OA\Response(
    //      *    response=403,
    //      *    description="Your account is yet to be approved",
    //      *    @OA\JsonContent(
    //      *       @OA\Property(property="message", type="string", example="Already Registered, admin approval required")
    //      *        )
    //      *      ),
    //      * @OA\Response(
    //      *    response=400,
    //      *    description="Your email not verified",
    //      *    @OA\JsonContent(
    //      *       @OA\Property(property="message", type="string", example="Already Registered, Email verification required")
    //      *        )
    //      *      )
    //      * )
    //      * )
    //      */
    //     if (!$request->email || !$request->name || !$request->password || !$request->password_confirmation || !$request->mobile) {
    //         return response(['message' => 'Required parameters Missing: Hint check if all the required parameters are passed - Required parameters -> [email, name, password, password_confirmation]', 'request' => $request], 422);
    //     };
    //     $loginData = $request->validate([
    //         'email' => 'email|required',
    //         'password' => 'required'
    //     ]);
    //     if (Auth::guard('customer')->attempt($loginData)) {
    //         // if (!Auth::guard('customer')->user()->verified) {
    //         //     return response(['Message' => 'Already Registered - Email Verification required'], 400);
    //         // } elseif (!Auth::user()->approved) {
    //         //     return response(['Message' => 'Already Registered - Your account is under approval'], 403);
    //         // }
    //         return response(['message' => 'User already Registered - Try Login'], 403);
    //     }
    //     $request->validate([
    //         'name' => 'required|max:55',
    //         'email' => 'email|required|unique:users',
    //         'password' => 'required|confirmed',
    //         'mobile' => 'required'
    //     ]);
    //     $customer = Customer::create($request->all());
    //     return ['result' => $customer];
    // }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'mobile' => ['required', 'string', 'max:13'],
            'password' => 'required|string|min:6',
            'cpassword' => 'required|string|min:6',
        ]);
    }

    protected function createCustomer(Request $request)
    {
        $this->validator($request->all())->validate();

        $store_owner_id = $this->findStoreOwnerId($request->slink);
        if(!empty($store_owner_id)) {
            $customer = Customer::where([
                ['email', $request['email']],
                ['store_owner_id', $store_owner_id]
            ])->first();

            if (empty($customer)) {
                $customer_id = Customer::where('store_owner_id', $store_owner_id)->count();
                Customer::create([
                    'name' => $request['name'],
                    'email' => $request['email'],
                    'mobile' => $request['mobile'],
                    'password' => Hash::make($request['password']),
                    'approved' => 1,
                    'store_owner_id' => $store_owner_id,
                    'customer_id' => $customer_id + 1,
                ]); 
            } else {
                return ['status' => 'error'];
            }
        } else {
            return ['status' => 'error', 'msg' => 'Invaild Store ID'];
        }

        if (Auth::guard('customer')->attempt(['email' => $request->email, 'password' => $request->password])) {
            $accessToken = Auth::guard('customer')->user()->createToken('accessToken')->plainTextToken;

            return ['msg' => 'success', 'accessToken' => $accessToken]; 
        }
    }

    protected function findStoreOwnerId($slink)
    {
        $shareableLink = ShareableLink::where('uuid', $slink)->first();
        if ($shareableLink == null) {

            $shareable = UserInfo::where('customdomain', $slink)->first();

            if ($shareable == null) {
                $shareable = UserInfo::where('subdomain', $slink)->first();
            }
        } else {
            $shareable = $shareableLink->shareable;
        }
        if(isset($shareable->user)) {
            $team_id = $shareable->user->team_id;
        } else {
            $team_id = null;
        }
        return $team_id;
    }

    public function customerLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('customer')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
            $accessToken = Auth::guard('customer')->user()->createToken('accessToken')->plainTextToken;
            return ['msg' => 'success', 'accessToken' => $accessToken];
        }
        return response(['message' => 'Invalid Credentials'], 401);
    }
}
