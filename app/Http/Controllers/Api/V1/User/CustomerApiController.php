<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\ProductResource;
use App\ordersCustomer;
use App\ordersProduct;
use Gate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class CustomerApiController extends Controller
{
    public function index()
    {
        /**
         * @OA\GET(
         *     path="/api/v1/user/customers",
         *     tags={"Customers"},
         *     summary="Show all the Customers",
         *     description="API to get the list of Customers",
         *     security={
         *       {"passport": {}}
         *     },
         *     @OA\Response(
         *         response=403,
         *         description="Content access Denied"
         *     ),
         *     @OA\Response(
         *         response=200,
         *         description="Permission Granted"
         *     ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         **/
        abort_if(Gate::denies('order_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $customer = ordersCustomer::where("user_id", Auth::user()->id)->get();
        if ($customer) {
            return $customer;
        }
    }

    public function show($id)
    {
        /**
         * @OA\GET(
         *     path="/api/v1/user/customers/{id}",
         *     tags={"Customers"},
         *     summary="Show a specific Customer by ID",
         *     description="API to get a specific customers",
         *     security={
         *       {"passport": {}}
         *     },
         *      @OA\Parameter(
         *          name="id",
         *          description="customer id",
         *          required=true,
         *          in="path",
         *          @OA\Schema(
         *              type="integer"
         *          )
         *      ),
         *     @OA\Response(
         *         response=403,
         *         description="Content access Denied"
         *     ),
         *     @OA\Response(
         *         response=200,
         *         description="Permission Granted"
         * )
         * )
         **/
        abort_if(Gate::denies('order_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $orderCustomer = ordersCustomer::find($id);

        if ($orderCustomer && $orderCustomer->user_id == Auth::User()->id) {

            return ordersCustomer::where("team_id", Auth::user()->team_id)->where("id", $id)->get();
        }

        return response(['message' => 'No such Customer found']);
    }


    public function update(Request $request, $id)
    {
        /**
         * @OA\POST(
         *     path="/api/v1/user/customers/{id}",
         *     tags={"Customers"},
         *     summary="Update a specific Customer by ID",
         *     description="Update a specific Customer",
         *     security={
         *       {"passport": {}}
         *     },
         *      @OA\Parameter(
         *          name="id",
         *          description="Customer id",
         *          required=true,
         *          in="path",
         *          @OA\Schema(
         *              type="integer"
         *          )
         *      ),
         *   @OA\RequestBody(
         *       required = true,
         *       @OA\MediaType(
         *           mediaType="multipart/form-data",
         *           @OA\Schema( 
         *               @OA\Property(
         *                  property="fname",
         *                  type="string",
         *                  description="Enter the First Name of customer"
         *               ),
         *               @OA\Property(
         *                  property="lname",
         *                  type="string",
         *                  description="Enter the Last Name of customer"
         *               ),
         *               @OA\Property(
         *                  property="address1",
         *                  type="string",
         *                  description="Flat, House No, Street:"
         *               ),
         *               @OA\Property(
         *                  property="address2",
         *                  type="string",
         *                  description="Landmark e.g. near apollo hospital:"
         *               ),
         *               @OA\Property(
         *                  property="city",
         *                  type="string",
         *                  description="Enter the city"
         *               ),
         *               @OA\Property(
         *                  property="pincode",
         *                  type="integer",
         *                  description="Enter the pin code"
         *               ),
         *               @OA\Property(
         *                  property="state",
         *                  type="string",
         *                  description="State"
         *               ),
         *               @OA\Property(
         *                  property="country",
         *                  type="string",
         *                  description="Country"
         *               ),
         *               @OA\Property(
         *                  property="email",
         *                  type="email",
         *                  description="Enter your mail ID"
         *               ),
         *               @OA\Property(
         *                  property="mobileno",
         *                  type="integer",
         *                  description="Enter Primary Mobile Number"
         *               ),
         *               @OA\Property(
         *                  property="amobileno",
         *                  type="integer",
         *                  description="Enter Alternate Mobile Number"
         *               ),
         *               @OA\Property(
         *                  property="addresstype",
         *                  type="string",
         *                  description="Home/Work"
         *               ),
         *           ),
         *       ),
         *   ),
         *     @OA\Response(
         *         response=201,
         *         description="Product Category created Successfully"
         *      ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         **/
        abort_if(Gate::denies('order_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $customer = ordersCustomer::find($id);
        if ($customer && $customer->user_id == Auth::User()->id) {
            $customer->update($request->all());
            return $customer;
        }
        return response(["Message" => "No such Customer Found"]);
    }



    public function destroy($id)
    {
        /**
         * @OA\Delete(
         *     path="/api/v1/user/customers/{id}",
         *     tags={"Customers"},
         *     summary="Delete a Specific Customer by ID",
         *     description="API to delete a specific customer",
         *     security={
         *       {"passport": {}}
         *     },
         *      @OA\Parameter(
         *          name="id",
         *          description="Customer id",
         *          required=true,
         *          in="path",
         *          @OA\Schema(
         *              type="integer"
         *          )
         *      ),
         *     @OA\Response(
         *         response=403,
         *         description="Content access Denied"
         *     ),
         *     @OA\Response(
         *         response=200,
         *         description="Permission Granted"
         * )
         * )
         **/
        abort_if(Gate::denies('order_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $customer = ordersCustomer::find($id);
        if ($customer && $customer->user_id == Auth::User()->id) {
            $customer->delete();
            return ["message" => "Customer is Deleted successfully"];
        }
        return ["Message" => "No Such Customer Found"];
    }
}
