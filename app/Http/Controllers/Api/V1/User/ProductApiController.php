<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Http\Resources\Admin\ProductResource;
use App\Product;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class ProductApiController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        /** @OA\Get(
         * path="/api/v1/user/products",
         * summary="Show all Products",
         * description="show the order",
         * tags={"Products"},
         * security={
         *       {"passport": {}}
         *     },
         *     @OA\Response(
         *         response=403,
         *         description="Content access Denied"
         *     ),
         *     @OA\Response(
         *         response=200,
         *         description="Permission Granted"
         *     ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         */
        abort_if(Gate::denies('product_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $product = Product::with(['categories', 'tags', 'arobject'])->paginate(10);
        if ($product->isEmpty()) {
            return response(['message' => 'No Data Found'], 404);
        }
        return new ProductResource($product);
    }

    public function store(Request $request)
    {
        /**
         * @OA\Post(
         * path="/api/v1/user/products",
         * summary="Create a new product",
         * description="Enter the Name, Description, Price, Categories, Tags",
         * operationId="authRegister",
         * tags={"Products"},
         * security={
         *       {"passport": {}}
         *     },
         * @OA\RequestBody(
         *    required=true,
         *    description="Pass user order",
         * @OA\MediaType(
         *           mediaType="multipart/form-data",
         *           @OA\Schema( 
         *               @OA\Property(
         *                  property="name",
         *                  type="string",
         *               ),
         *               @OA\Property(
         *                  property="description",
         *                  type="string",
         *               ),
         *              @OA\Property(
         *                  property="price",
         *                  type="string",
         *               ),
         *              @OA\Property(
         *                  property="sku",
         *                  type="string",
         *               ),
         *              @OA\Property(
         *                  property="discount",
         *                  type="string",
         *               ),
         *              
         *              @OA\Property(
         *                  property="stock",
         *                  type="string",
         *               ),
         *               @OA\Property(
         *                  property="photo",
         *                  type="string",
         *                  format="binary"
         *               ),
         *           ),
         *       ),
         *    @OA\JsonContent(
         *       required={"name","Description","Price","Categories","Tags"},
         *       @OA\Property(property="name", type="string", format="strings", example="cricket set"),
         *       @OA\Property(property="Description", type="string", format="strings", example="All the cricket equipments belongs to A1 quality"),
         *       @OA\Property(property="Price", type="string", format="strings", example="200"),
         *       @OA\Property(property="Categories", type="string", format="strings", example="Sports Equipments"),
         *       @OA\Property(property="Tags", type="string", example="Sports"),
         *       @OA\Property(property="persistent", type="boolean", example="true"),
         *    ),
         * ),
         *     @OA\Response(
         *         response=201,
         *         description="Product Category created Successfully"
         *      ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         */
        $product = Product::create($request->all());
        $product->categories()->sync($request->input('categories', []));
        $product->tags()->sync($request->input('tags', []));
        $product->save();

        if ($request->photo) {
            $product->addMediaFromRequest('photo')->toMediaCollection('photo');
        }
        // if($product->store())
        // {
        //     return ['Result'=>"Product added"];
        // }

        return (new ProductResource($product->load(['team', 'media'])));
    }

    public function show($id)
    {
        /** @OA\Get(
         * path="/api/v1/user/products/{id}",
         * summary="show a specific product by ID",
         * description="Show a specific product by ID",
         * tags={"Products"},
         *      @OA\Parameter(
         *          name="id",
         *          description="product id",
         *          required=true,
         *          in="path",
         *          @OA\Schema(
         *              type="integer"
         *          )
         *      ),
         * security={
         *       {"passport": {}}
         *     },
         *     @OA\Response(
         *         response=200,
         *         description="Product Catalog Deleted Successfully"
         *      ),
         *      @OA\Response(
         *          response=404,
         *          description="Invalid Category ID"
         *      ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         */
        abort_if(Gate::denies('product_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $product = Product::find($id);
        if (!$product) {
            return response(["message" => "No such Product found"]);
        }
        return new ProductResource($product->load(['categories', 'tags', 'arobject']));
    }


    public function update(Request $request, $id)
    {
        /**
         * @OA\POST(
         * path="/api/v1/user/products/{id}",
         * summary="Update a specific product by ID",
         * description="order with name, Description, Price, Categories",
         * tags={"Products"},
         * security={
         *       {"passport": {}}
         *     },
         *      @OA\Parameter(
         *          name="id",
         *          description="product id",
         *          required=true,
         *          in="path",
         *          @OA\Schema(
         *              type="integer"
         *          )
         *      ),
         * @OA\RequestBody(
         *    required=true,
         *    description="Pass user  update",
         * @OA\MediaType(
         *           mediaType="multipart/form-data",
         *           @OA\Schema( 
         *               @OA\Property(
         *                  property="name",
         *                  type="string",
         *               ),
         *               @OA\Property(
         *                  property="description",
         *                  type="string",
         *               ),
         *              @OA\Property(
         *                  property="price",
         *                  type="string",
         *               ),
         *              @OA\Property(
         *                  property="sku",
         *                  type="string",
         *               ),
         *              @OA\Property(
         *                  property="discount",
         *                  type="string",
         *               ),
         *              
         *              @OA\Property(
         *                  property="stock",
         *                  type="string",
         *               ),
         *               @OA\Property(
         *                  property="photo",
         *                  type="string",
         *                  format="binary"
         *               ),
         *           ),
         *       ),
         * ),
         * @OA\Response(
         *    response=201,
         *    description="Updated successfully", 
         *     ),
         *      @OA\Response(
         *          response=404,
         *          description="Invalid product ID"
         *    ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         *    @OA\Response(
         *         response=422,
         *         description="Missing Required Parameters"
         *     ),
         * )
         **/
        $product = Product::find($id);
        if (!$product) {
            return response(["Message" => "No such Product found"]);
        }
        $product->categories()->sync($request->input('categories', []));
        $product->tags()->sync($request->input('tags', []));
        // $product->save();

        $product->update($request->all());

        // if ($request->input('photo', false)) {
        //     if (!$product->photo || $request->input('photo') !== $product->photo->file_name) {
        //         $product->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
        //     }
        // } 
        // elseif ($product->photo) {
        //     $product->photo->delete();
        // }

        return (new ProductResource($product));
        // ->response()
        // ->setStatusCode(Response::HTTP_ACCEPTED);
    }


    public function destroy($id)
    {
        /**
         * @OA\Delete(
         * path="/api/v1/user/products/{id}",
         * summary="Delete a specific product by ID",
         * description="Delete the Product",
         * operationId="authRegister",
         * tags={"Products"},
         * security={
         *       {"passport": {}}
         *     },
         *      @OA\Parameter(
         *          name="id",
         *          description="product id",
         *          required=true,
         *          in="path",
         *          @OA\Schema(
         *              type="integer"
         *          )
         *      ),
         *     @OA\Response(
         *         response=200,
         *         description="Product Catalog Deleted Successfully"
         *      ),
         *      @OA\Response(
         *          response=404,
         *          description="Invalid Category ID"
         *      ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         **/
        abort_if(Gate::denies('product_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $product = Product::find($id);

        if (!$product) {
            return response(["Message" => "No such Product found"]);
        }

        $product->delete();

        return response(["Message" => "Product Deleted Successfully"]);
    }
}
