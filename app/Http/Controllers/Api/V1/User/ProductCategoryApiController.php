<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\StoreProductCategoryRequest;
use App\Http\Requests\UpdateProductCategoryRequest;
use App\Http\Resources\Admin\ProductCategoryResource;
use App\ProductCategory;
use Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class ProductCategoryApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /**
         * @OA\GET(
         *     path="/api/v1/user/product-categories",
         *     tags={"Product Category"},
         *     summary="Show all Category of the products",
         *     description="API to get the list of Categories of product",
         *     security={
         *       {"passport": {}}
         *     },
         *     @OA\Response(
         *         response=403,
         *         description="Content access Denied"
         *     ),
         *     @OA\Response(
         *         response=200,
         *         description="Permission Granted"
         *     ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         **/
        abort_if(Gate::denies('product_category_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $sort = ['id', 'desc'];
        if(isset($request->limit) && !empty($request->limit)) {
            $page = $request->limit;
        } else {
            $page = 10;
        }
        $product_category = ProductCategory::with(['childrenCategories', 'parent'])
            ->select(['id', 'name', 'description'])->orderBy($sort[0], $sort[1])->paginate($page);
        return new ProductCategoryResource($product_category);
    }
    public function store(Request $request)
    {
        /**
         * @OA\POST(
         *     path="/api/v1/user/product-categories",
         *     tags={"Product Category"},
         *     summary="Creating new Category of the products",
         *     description="Create a new product category and store",
         *     security={
         *       {"passport": {}}
         *     },
         *   @OA\RequestBody(
         *       required = true,
         *       @OA\MediaType(
         *           mediaType="multipart/form-data",
         *           @OA\Schema( 
         *               @OA\Property(
         *                  property="name",
         *                  description="Name of your category",
         *                  type="string",
         *               ),
         *               @OA\Property(
         *                  property="description",
         *                  description="Describe your category",
         *                  type="string",
         *               ),
         *               @OA\Property(
         *                  property="photo",
         *                  type="string",
         *                  description="Provide a Display Image for your category",
         *                  format="binary"
         *               ),
         *               @OA\Property(
         *                  property="product_category_id",
         *                  description="If you want this category to be the child category enter the parent category ID here",
         *                  type="integer",
         *               ),
         *           ),
         *       ),
         *   ),
         *     @OA\Response(
         *         response=201,
         *         description="Product Category created Successfully"
         *      ),
         *     @OA\Response(
         *         response=403,
         *         description="Content access Denied"
         *     ),
         *    @OA\Response(
         *         response=422,
         *         description="Missing Required Parameters"
         *     ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         **/
        abort_if(Gate::denies('product_category_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        if (!$request->name && !$request->description) {
            return response(['message' => 'Request body missing required parameters. Hint: Check if the request json contains name and description fields'], 422);
        }

        $productCategory = ProductCategory::create($request->all());

        if ($request->photo) {
            $productCategory->addMediaFromRequest('photo')->toMediaCollection('photo');
        }

        return (new ProductCategoryResource($productCategory->load(['team', 'media'])));
    }

    public function show($id)
    {
        /**
         * @OA\GET(
         *     path="/api/v1/user/product-categories/{id}",
         *     tags={"Product Category"},
         *     summary="Show a specific Category of the products",
         *     description="API to get the list of Categories of product",
         *     security={
         *       {"passport": {}}
         *     },
         *      @OA\Parameter(
         *          name="id",
         *          description="category id",
         *          required=true,
         *          in="path",
         *          @OA\Schema(
         *              type="integer"
         *          )
         *      ),
         *     @OA\Response(
         *         response=403,
         *         description="Content access Denied"
         *     ),
         *     @OA\Response(
         *         response=200,
         *         description="Permission Granted"
         *    ),
         *      @OA\Response(
         *          response=404,
         *          description="Invalid Category ID"
         *      ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         **/
        abort_if(Gate::denies('product_category_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $productCategory = ProductCategory::find($id);

        if ($productCategory && Auth::User()->team_id == $productCategory->team_id) {
            return new ProductCategoryResource($productCategory->load(['team']));
        }

        return response(['message' => 'No Such Category found'], 404);
    }

    public function update(Request $request, $id)
    {

        /**
         * @OA\POST(
         *     path="/api/v1/user/product-categories/{id}",
         *     tags={"Product Category"},
         *     summary="Update a Category of the products",
         *     description="Update a category",
         *     security={
         *       {"passport": {}}
         *     },
         *      @OA\Parameter(
         *          name="id",
         *          description="category id",
         *          required=true,
         *          in="path",
         *          @OA\Schema(
         *              type="integer"
         *          )
         *      ),
         *   @OA\RequestBody(
         *       required = true,
         *       @OA\MediaType(
         *           mediaType="multipart/form-data",
         *           @OA\Schema( 
         *               @OA\Property(
         *                  property="name",
         *                  description="Name of your category",
         *                  type="string",
         *               ),
         *               @OA\Property(
         *                  property="description",
         *                  description="Describe your category",
         *                  type="string",
         *               ),
         *               @OA\Property(
         *                  property="photo",
         *                  type="string",
         *                  description="Provide a Display Image for your category",
         *                  format="binary"
         *               ),
         *               @OA\Property(
         *                  property="product_category_id",
         *                  description="If you want this category to be the child category enter the parent category ID here",
         *                  type="integer",
         *               ),
         *           ),
         *       ),
         *   ),
         *     @OA\Response(
         *         response=201,
         *         description="Product Category created Successfully"
         *    ),
         *      @OA\Response(
         *          response=404,
         *          description="Invalid Category ID"
         *    ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         *    @OA\Response(
         *         response=422,
         *         description="Missing Required Parameters"
         *     ),
         * )
         **/
        abort_if(Gate::denies('product_category_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $productCategory = ProductCategory::find($id);

        if ($productCategory && Auth::User()->team_id == $productCategory->team_id) {

            $productCategory->update($request->all());

            if ($request->file('photo', false)) {
                $productCategory->addMediaFromRequest('photo')->toMediaCollection('photo');
            }
            return (new ProductCategoryResource($productCategory));
        }
        return response(['message' => 'No Such Category to Update'], 404);
    }

    public function destroy($id)
    {
        /**
         * @OA\DELETE(
         *     path="/api/v1/user/product-categories/{id}",
         *     tags={"Product Category"},
         *     summary="Delete a Category of the Products",
         *     description="Deleting a category in the container",
         *     security={
         *       {"passport": {}}
         *     },
         *      @OA\Parameter(
         *          name="id",
         *          description="category id",
         *          required=true,
         *          in="path",
         *          @OA\Schema(
         *              type="integer"
         *          )
         *      ),
         *     @OA\Response(
         *         response=200,
         *         description="Product Category Deleted Successfully"
         *      ),
         *      @OA\Response(
         *          response=404,
         *          description="Invalid Category ID"
         *      ),
         *    @OA\Response(
         *         response=500,
         *         description="Authorization Required"
         *     ),
         * )
         **/
        abort_if(Gate::denies('product_category_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $productCategory = ProductCategory::find($id);
        if ($productCategory && Auth::User()->team_id == $productCategory->team_id) {

            $temp = $productCategory->load(['team', 'media']);
            $productCategory->delete();
            return response(["Deleted Category" => $temp, 'message' => 'Product Category Deleted Successfully'], 200);
        }

        return response(['message' => 'No Such Category to Delete'], 404);
    }
}
