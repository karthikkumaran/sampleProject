<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Http\Request;
use App\Campaign;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyVideoRequest;
use App\Http\Requests\StoreProductCardRequest;
use App\Http\Requests\StoreVideoRequest;
use App\Http\Requests\UpdateVideoRequest;
use App\Http\Requests\StoreProductRequest;
use Illuminate\Support\Facades\Schema;
use App\Video;
use Gate;
use App\Entitycard;
use App\AttributesMetadata;
use App\ordersCustomer;
use App\Varchar;
use App\UserInfo;
use App\Product;
use App\ProductCategory;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;
use \Jenssegers\Agent\Agent;
use App\User;
use App\LaravelSubscriptions\Plan;
use App\LaravelSubscriptions\PlanFeature;
use App\LaravelSubscriptions\PlanSubscription;
use App\LaravelSubscriptions\PlanSubscriptionUsage;
use App\LaravelSubscriptions\PlanSubscriptionFeatures;
use App\LaravelSubscriptions\PlanSubscribedFeatures;

trait AddEntityCardTrait
{
    public function InstaTry($campaign)
    {

        $products = Product::all();
        $addedCards = $campaign->campaginEntitycards;
        return view('admin.campaigns.addproductcard.instatry', ['campaign' => $campaign, 'products' => $products, 'addedCards' => $addedCards]);
    }
    public function TryAlong($campaign)
    {
        $video = Video::where('campaignid_id', $campaign->id)->first();
        $campId = $campaign->id;
        if (!empty($video)) {
            $videoURL = $video->video_file->getUrl();
            $videoID = $video->id;
            $videoName = $video->title;
            $products = Product::all();
            $addedCards = $campaign->campaginEntitycards;
            return view('admin.campaigns.addproductcard.tryalong', ['campaign' => $campaign, 'videoURL' => $videoURL, 'videoID' => $videoID, 'videoName' => $videoName, 'products' => $products, 'addedCards' => $addedCards]);
        } else {
            return view('admin.campaigns.addproductcard.tryalong', ['campaign' => $campaign]);
        }
    }

    public function TryAlongVideoStore(StoreVideoRequest $request)
    {
        $video = Video::create($request->all());

        if ($request->input('video_file', false)) {
            $video->addMedia(storage_path('tmp/uploads/' . $request->input('video_file')))->toMediaCollection('video_file');
        }

        return redirect()->route('admin.campaigns.addProductCard', $request->campaignid_id);
    }

    public function TryAlongStoreProductCard(Request $request)
    {
        // $productCard = ProductCard::create($request->all());
        $productCard = Entitycard::create(array_merge($request->all(), array("meta_data" => '{ "start_time": ' . $request->start_time . ', "end_time": ' . $request->end_time . ' }')));
        return redirect()->back();
        // return redirect()->route('admin.product-cards.index');
    }

    public function createEntityCard(Request $request)
    {
        $product_ids = $request->product_id;
        $catagory_ids = $request->catagory_id;
        $campaign_id = $request->campaign_id;
        if($request->videoid == "undefined"){
            $video_id = null; 
        }
        else{
            $video_id =  $request->videoid;
        }
        // $entitycard = Entitycard::where('campaign_id', $campaign_id)->where('team_id',auth()->user()->id)->first();
        // if(!empty($entitycard->video_id)){
        //     $video_id =  $entitycard->video_id;
        // }
        $i = 0;
        foreach ($product_ids as $product_id) {
            $catagory_id = $catagory_ids[$i];
            // echo "catagory ".$catagory_id;
            if (empty($catagory_id)) {
                Entitycard::create(array('product_id' => $product_id, 'campaign_id' => $campaign_id, 'meta_data' => '{}','video_id'=>$video_id));
            } else {
                Entitycard::create(array('product_id' => $product_id, 'campaign_id' => $campaign_id, 'catagory_id' => $catagory_id, 'meta_data' => '{}','video_id'=>$video_id));
            }
            $i++;
        }
        return response()->json(["status" => true, "message" => "Card Added successfully."]);
        // echo '<script>localStorage.setItem("selectedProducts","");</script>';
        // return redirect()->back()->with('message', trans('Added Card'));
        // $productCard = ProductCard::create($request->all());
    }

    public function createEntityCardFromProducts(Request $request)
    {
        $product_ids = $request->product_id;
        $catagory_ids = $request->catagory_id;
        $campaign_ids = $request->catalogs_selected;
        $catagory_id = null;
        foreach ($campaign_ids as $campaign_id) {
            $campaign_products = Entitycard::where('campaign_id', $campaign_id)->get();
            $campaign_product_ids = array();
            foreach ($campaign_products as $campaign_product) {
                array_push($campaign_product_ids, $campaign_product->product_id);
            }
            $i = 0;
            foreach ($product_ids as $product_id) {
                if(!in_array($product_id, $campaign_product_ids)){
                    $catagory_id = $catagory_ids[$i];
                    if (empty($catagory_id)) {
                        Entitycard::create(array('product_id' => $product_id, 'campaign_id' => $campaign_id, 'meta_data' => '{}'));
                    } else {
                        Entitycard::create(array('product_id' => $product_id, 'campaign_id' => $campaign_id, 'catagory_id' => $catagory_id, 'meta_data' => '{}'));
                    }
                    $i++;
                }
            }
        }
        
        return response()->json(["status" => true, "message" => "Card Added successfully."]);
    }

    public function previewAR($campaign)
    {
        // if(!empty($campaign)){
        //     if($campaign->type == 0){
        //         $video = Video::where('campaignid_id', $campaign->id)->first();
        //         $videoURL=$video->video_file->getUrl();
        //         $videoID=$video->id;
        //         $videoName=$video->title;
        //         $products = Product::all();
        //         $addedCards = $campaign->campaginEntitycards;
        //         return view('admin.campaigns.previewAR.tryalong',['videoURL' => $videoURL,'videoID'=>$videoID,'videoName'=>$videoName,'products'=>$products,'addedCards'=>$addedCards]);
        //     } 
        //     elseif($campaign->type == 1){
        //         $products = Product::all();
        //         $addedCards = $campaign->campaginEntitycards;
        //         return view('admin.campaigns.previewAR.instatry',['products'=>$products,'addedCards'=>$addedCards]);
        //     }elseif($campaign->type == 3){
        //         $meta_data = json_decode($campaign->meta_data, true);
        //         $data = EntityCard::where('campaign_id',$campaign->id)->orderBy('id','desc')->get();
        //         return view('templates.view.catalog',compact('campaign','meta_data','data'));
        //     }

        // }

        // return;
        $campaign_meta_data = json_decode($campaign->meta_data, true);
        $userSettings = UserInfo::where("user_id", $campaign->user->id)->first();
        $meta_data = json_decode($userSettings->meta_data, true);
        $access_token = $campaign->user->createToken('AuthToken')->plainTextToken;

        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

        $data = Entitycard::where('campaign_id', $campaign->id)->orderBy('id', 'desc')->get();
        $initialProduct = Entitycard::with('product')->where('campaign_id', $campaign->id)->where('product_id',$_GET['sku'])->first();
        if(!empty($meta_data['tryon_settings'])) {
            if($meta_data['tryon_settings']['tryon_version'] == '4.0.0' || $meta_data['tryon_settings']['tryon_version'] == '4.0.2' ){
                return view('templates.arview.tryonProductV4', compact('campaign', 'userSettings', 'meta_data', 'data','initialProduct', 'access_token'));
            } else if($meta_data['tryon_settings']['tryon_version'] == '4.0.3'){
                if($meta_data['tryon_settings']['tryon_mode'] == "BIG-SCREEN"){
                    return view('templates.arview.tryonv4_03.tryonProductV4BigScreen', compact('campaign', 'userSettings', 'meta_data', 'data','initialProduct', 'access_token'));
                }else{
                    return view('templates.arview.tryonv4_03.tryonProductV4', compact('campaign', 'userSettings', 'meta_data', 'data','initialProduct', 'access_token'));
                }
            } 
        }

        return view('templates.arview.tryonProduct', compact('campaign', 'userSettings', 'meta_data', 'data','initialProduct', 'access_token'));
        
    }

    public function previewVAR($campaign)
    {
        $campaign_meta_data = json_decode($campaign->meta_data, true);
        $userSettings = UserInfo::where("user_id", $campaign->user->id)->first();
        $meta_data = json_decode($userSettings->meta_data, true);

        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

        $data = Entitycard::where('campaign_id', $campaign->id)->where('product_id', $campaign->sku)->first();
        // return view('themes.' . $theme . '.pages.viewAR', compact('campaign', 'userSettings', 'meta_data', 'data'));
        $product = $data->product;
        $product_script_meta_data =  json_decode($product->scripts->meta_data ?? '',true);
        
        if(!empty($product_script_meta_data["custom_theme"]) && $product_script_meta_data["custom_theme"] == 1){
            return view('templates.arview.viewARCustomTheme', compact('campaign', 'userSettings', 'meta_data', 'data'));
        }

        return view('templates.arview.viewAR', compact('campaign', 'userSettings', 'meta_data', 'data'));
    }

    public function previewVVR($campaign)
    {
        $campaign_meta_data = json_decode($campaign->meta_data, true);
        $userSettings = UserInfo::where("user_id", $campaign->user->id)->first();
        $meta_data = json_decode($userSettings->meta_data, true);
        $vr_store_url = $meta_data["vr-store-url"]["vr-store-url"];

        // $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

        // $data = Entitycard::where('campaign_id', $campaign->id)->where('product_id', $campaign->sku)->first();
        // return view('themes.' . $theme . '.pages.viewAR', compact('campaign', 'userSettings', 'meta_data', 'data'));
        // $product = $data->product;
        // $product_script_meta_data =  json_decode($product->scripts->meta_data ?? '',true);
        
        // if(!empty($product_script_meta_data["custom_theme"]) && $product_script_meta_data["custom_theme"] == 1){
        //      return view('templates.arview.viewARCustomTheme', compact('campaign', 'userSettings', 'meta_data', 'data'));
        //  }

        return view('templates.arview.viewVR', compact('vr_store_url', 'campaign', 'userSettings', 'meta_data'));
    }

    public function previewVARembbed($campaign)
    {
        $campaign_meta_data = json_decode($campaign->meta_data, true);
        $userSettings = UserInfo::where("user_id", $campaign->user->id)->first();
        $meta_data = json_decode($userSettings->meta_data, true);

        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

        $data = Entitycard::where('campaign_id', $campaign->id)->where('product_id', $campaign->sku)->first();
        // return view('themes.' . $theme . '.pages.viewAR', compact('campaign', 'userSettings', 'meta_data', 'data'));

        $product = $data->product;
        $product_script_meta_data =  json_decode($product->scripts->meta_data ?? '',true);
        
        if(!empty($product_script_meta_data["custom_theme"]) && $product_script_meta_data["custom_theme"] == 1){
            return view('templates.arview.viewARembbedCustomTheme', compact('campaign', 'userSettings', 'meta_data', 'data'));
        }

        return view('templates.arview.viewARembbed', compact('campaign', 'userSettings', 'meta_data', 'data'));
    }

    public function preview($campaign,$video_id)
    {
        if (!empty($campaign)) {
            if ($campaign->type == 0 ) {
                $video = Video::where('campaignid_id', $campaign->id)->first();
                $videoURL = $video->video_file->getUrl();
                $videoID = $video->id;
                $videoName = $video->title;
                $products = Product::all();
                $addedCards = $campaign->campaginEntitycards;
                return view('admin.campaigns.previewAR.tryalong', ['videoURL' => $videoURL, 'videoID' => $videoID, 'videoName' => $videoName, 'products' => $products, 'addedCards' => $addedCards]);
            } elseif ($campaign->type == 1) {
                $products = Product::all();
                $addedCards = $campaign->campaginEntitycards;
                return view('admin.campaigns.previewAR.instatry', ['products' => $products, 'addedCards' => $addedCards]);
            } elseif ($campaign->type == 3 || $campaign->type == 2) {
                $campaign_meta_data = json_decode($campaign->meta_data, true);
                $userSettings = UserInfo::where("user_id", $campaign->user->id)->first();
                $meta_data = json_decode($userSettings->meta_data, true);

                $data = Entitycard::where('campaign_id', $campaign->id)->whereHas('product', function ($query) {
                    return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy('id', 'desc')->get();
                $video_data = Entitycard::where('campaign_id', $campaign->id)->whereNotNull('video_id')->whereNull('product_id')->get();
                $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

                $categories = ProductCategory::leftJoin('entitycards', function ($join) {
                    $join->on('product_categories.id', '=', 'entitycards.catagory_id');
                })->where("entitycards.campaign_id", $campaign->id)
                    ->whereNotNull('entitycards.catagory_id')
                    ->whereNull('entitycards.deleted_at')
                    ->groupBy('product_categories.id')
                    ->orderBy('product_categories.id', 'desc')
                    ->get();
                $attrribute_array = array();
                $product_ids = array();
                $content_array = array();
                foreach ($data as $entity_card) {
                    array_push($product_ids, $entity_card->product_id);
                }
                $values = varchar::whereIn('entity_id', $product_ids)->get();
                foreach ($values as $value => $card) {
                    $vals = explode(",", $card->content);
                    if(count($vals) > 1){
                        foreach ($vals as $val){
                            array_push($content_array, $val);
                        }
                    }
                    else{
                        array_push($content_array, $vals[0]);
                    }
                    array_push($attrribute_array, $card->attribute_id);
                }
                $content_array = array_unique($content_array);
                $attributes = AttributesMetadata::whereIn('attribute_id', $attrribute_array)->get()->groupBy('attribute_group_name');
                return view('themes.' . $theme . '.pages.catalog', compact('campaign', 'userSettings', 'meta_data', 'data', 'categories','attributes','values','content_array','video_data','video_id'));
            }
        }

        return;
    }

    public function previewShowVideo($campaign,$video_id)
    {
        if (!empty($campaign)) {
            if ($campaign->type == 0 ) {
                $video = Video::where('campaignid_id', $campaign->id)->first();
                $videoURL = $video->video_file->getUrl();
                $videoID = $video->id;
                $videoName = $video->title;
                $products = Product::all();
                $addedCards = $campaign->campaginEntitycards;
                return view('admin.campaigns.previewAR.tryalong', ['videoURL' => $videoURL, 'videoID' => $videoID, 'videoName' => $videoName, 'products' => $products, 'addedCards' => $addedCards]);
            } elseif ($campaign->type == 1) {
                $products = Product::all();
                $addedCards = $campaign->campaginEntitycards;
                return view('admin.campaigns.previewAR.instatry', ['products' => $products, 'addedCards' => $addedCards]);
            } elseif ($campaign->type == 3 || $campaign->type == 2) {
                $campaign_meta_data = json_decode($campaign->meta_data, true);
                $userSettings = UserInfo::where("user_id", $campaign->user->id)->first();
                $meta_data = json_decode($userSettings->meta_data, true);

                $data = Entitycard::where('campaign_id', $campaign->id)->whereHas('product', function ($query) {
                    return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy('id', 'desc')->get();
                // $video_data = Entitycard::where('campaign_id', $campaign->id)->whereNotNull('video_id')->whereNull('product_id')->get();
                $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');
                $video_data = Entitycard::where('campaign_id', $campaign->id)->whereNotNull('video_id')->whereNull('product_id')->get();

                $categories = ProductCategory::leftJoin('entitycards', function ($join) {
                    $join->on('product_categories.id', '=', 'entitycards.catagory_id');
                })->where("entitycards.campaign_id", $campaign->id)
                    ->whereNotNull('entitycards.catagory_id')
                    ->whereNull('entitycards.deleted_at')
                    ->groupBy('product_categories.id')
                    ->orderBy('product_categories.id', 'desc')
                    ->get();
                $attrribute_array = array();
                $product_ids = array();
                $content_array = array();
                foreach ($data as $entity_card) {
                    array_push($product_ids, $entity_card->product_id);
                }
                $values = varchar::whereIn('entity_id', $product_ids)->get();
                foreach ($values as $value => $card) {
                    $vals = explode(",", $card->content);
                    if(count($vals) > 1){
                        foreach ($vals as $val){
                            array_push($content_array, $val);
                        }
                    }
                    else{
                        array_push($content_array, $vals[0]);
                    }
                    array_push($attrribute_array, $card->attribute_id);
                }
                $content_array = array_unique($content_array);
                $attributes = AttributesMetadata::whereIn('attribute_id', $attrribute_array)->get()->groupBy('attribute_group_name');
                return view('themes.' . $theme . '.pages.videos', compact('campaign', 'userSettings', 'meta_data', 'data', 'categories','attributes','values','content_array','video_data','video_id'));
            }
        }

        return;

    }

    public function previewVideo($campaign)
    {
        if (!empty($campaign)) {
            if ($campaign->type == 0 ) {
                $video = Video::where('campaignid_id', $campaign->id)->first();
                $videoURL = $video->video_file->getUrl();
                $videoID = $video->id;
                $videoName = $video->title;
                $products = Product::all();
                $addedCards = $campaign->campaginEntitycards;
                return view('admin.campaigns.previewAR.tryalong', ['videoURL' => $videoURL, 'videoID' => $videoID, 'videoName' => $videoName, 'products' => $products, 'addedCards' => $addedCards]);
            } elseif ($campaign->type == 1) {
                $products = Product::all();
                $addedCards = $campaign->campaginEntitycards;
                return view('admin.campaigns.previewAR.instatry', ['products' => $products, 'addedCards' => $addedCards]);
            } elseif ($campaign->type == 3 || $campaign->type == 2) {
                $campaign_meta_data = json_decode($campaign->meta_data, true);
                $userSettings = UserInfo::where("user_id", $campaign->user->id)->first();
                $meta_data = json_decode($userSettings->meta_data, true);

                $data = Entitycard::where('campaign_id', $campaign->id)->whereHas('product', function ($query) {
                    return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy('id', 'desc')->get();

                $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

                $categories = ProductCategory::leftJoin('entitycards', function ($join) {
                    $join->on('product_categories.id', '=', 'entitycards.catagory_id');
                })->where("entitycards.campaign_id", $campaign->id)
                    ->whereNotNull('entitycards.catagory_id')
                    ->whereNull('entitycards.deleted_at')
                    ->groupBy('product_categories.id')
                    ->orderBy('product_categories.id', 'desc')
                    ->get();
                $attrribute_array = array();
                $product_ids = array();
                $content_array = array();
                foreach ($data as $entity_card) {
                    array_push($product_ids, $entity_card->product_id);
                }
                $values = varchar::whereIn('entity_id', $product_ids)->get();
                foreach ($values as $value => $card) {
                    $vals = explode(",", $card->content);
                    if(count($vals) > 1){
                        foreach ($vals as $val){
                            array_push($content_array, $val);
                        }
                    }
                    else{
                        array_push($content_array, $vals[0]);
                    }
                    array_push($attrribute_array, $card->attribute_id);
                }
                $content_array = array_unique($content_array);
                $attributes = AttributesMetadata::whereIn('attribute_id', $attrribute_array)->get()->groupBy('attribute_group_name');
                return view('themes.' . $theme . '.pages.video_product', compact('campaign', 'userSettings', 'meta_data', 'data', 'categories','attributes','values','content_array'));
            }
        }

        return;
    }

    public function storeFront_old($userSettings)
    {
        dd('hellooo');
        $catalog = Campaign::where("user_id", $userSettings->user_id)->where("is_start", 1)->get();
        $meta_data = json_decode($userSettings->meta_data, true);
        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');
        $categories = ProductCategory::all();
        if (count($catalog) > 1) {
            return view('templates.view.store', compact('catalog', 'userSettings', 'meta_data'));
        } else {
            $campaign = $catalog[0];
            $data = Entitycard::where('campaign_id', $campaign->id)->orderBy('id', 'desc')->get();

            // return view('templates.view.catalog',compact('campaign','userSettings','meta_data','data'));
            return view('themes.' . $theme . '.pages.catalog', compact('campaign', 'userSettings', 'meta_data', 'data', 'categories'));
        }
    }

    public function storeFront($catalog, $userSettings)
    {
        // $catalog = Campaign::where("user_id",$userSettings->user_id)->where("is_start",1)->get();
        // return view('templates.view.store',compact('catalog','userSettings'));

        $user = $userSettings->user;
        if ($user->isSubscriptionExpired() == true) {
            $grace_period = $user->getGracePeriod();
            if ($grace_period == false) {
                return redirect()->route('store_subscription_expired');
            }
        }
        $meta_data = json_decode($userSettings->meta_data, true);
        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

        if (count($catalog) > 1) {
            return view('themes.' . $theme . '.pages.store', compact('catalog', 'userSettings', 'meta_data'));
        } else if (count($catalog) > 0) {
            $campaign = $catalog[0];
            // $categories = ProductCategory::leftJoin('entitycards', function ($join) {
            //     $join->on('product_categories.id', '=', 'entitycards.catagory_id');
            // })->where("entitycards.campaign_id", $campaign->id)
            //     ->whereNotNull('entitycards.catagory_id')
            //     ->whereNull('entitycards.deleted_at')
            //     ->groupBy('product_categories.id')
            //     ->orderBy('product_categories.id', 'desc')
            //     ->get();
            
            $categories = ProductCategory::leftJoin('entitycards', function ($join) {
                $join->on('product_categories.id', '=', 'entitycards.catagory_id');
            })->where("entitycards.campaign_id", $campaign->id)
                ->whereNotNull('entitycards.catagory_id')
                ->whereNull('entitycards.deleted_at')
                ->groupBy('product_categories.id')
                ->orderBy('product_categories.id', 'desc')
                ->get();
            $attrribute_array = array();
            $product_ids = array();
            $content_array = array();

            $data = Entitycard::where('campaign_id', $campaign->id)->whereHas('product', function ($query) {
                return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy('id', 'desc')->get();

            foreach ($data as $entity_card) {
                array_push($product_ids, $entity_card->product_id);
            }
            $values = varchar::whereIn('entity_id', $product_ids)->get();
            foreach ($values as $value => $card) {
                $vals = explode(",", $card->content);
                if(count($vals) > 1){
                    foreach ($vals as $val){
                        array_push($content_array, $val);
                    }
                }
                else{
                    array_push($content_array, $vals[0]);
                }
                array_push($attrribute_array, $card->attribute_id);
            }
            $content_array = array_unique($content_array);
            $attributes = AttributesMetadata::whereIn('attribute_id', $attrribute_array)->get()->groupBy('attribute_group_name');
            $card = Entitycard::where('campaign_id', $campaign->id)->whereNotNull('video_id')->whereNull('product_id')->orderBy('id', 'desc')->first();
            if(!empty($card))
            {
                return view('themes.' . $theme . '.pages.store', compact('catalog', 'userSettings', 'meta_data'));   
            }
            else{
                
                if($theme == 'rustic-theme'){
                    return view('themes.' . $theme . '.pages.store', compact('campaign', 'userSettings', 'meta_data'));   
                }
                return view('themes.' . $theme . '.pages.catalog', compact('campaign', 'userSettings', 'meta_data', 'data', 'categories','values','attributes','content_array'));
            }
        } else {
            return view('templates.errors.404');
        }
    }

    public function storeFrontVideo($catalog, $userSettings)
    {
        // $catalog = Campaign::where("user_id",$userSettings->user_id)->where("is_start",1)->get();
        // return view('templates.view.store',compact('catalog','userSettings'));

        $user = $userSettings->user;
        if ($user->isSubscriptionExpired() == true) {
            $grace_period = $user->getGracePeriod();
            if ($grace_period == false) {
                return redirect()->route('store_subscription_expired');
            }
        }
        $meta_data = json_decode($userSettings->meta_data, true);
        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

        if (count($catalog) > 1) {
            return view('themes.' . $theme . '.pages.videostore', compact('catalog', 'userSettings', 'meta_data'));
        } else if (count($catalog) > 0) {
            $campaign = $catalog[0];
            // $categories = ProductCategory::leftJoin('entitycards', function ($join) {
            //     $join->on('product_categories.id', '=', 'entitycards.catagory_id');
            // })->where("entitycards.campaign_id", $campaign->id)
            //     ->whereNotNull('entitycards.catagory_id')
            //     ->whereNull('entitycards.deleted_at')
            //     ->groupBy('product_categories.id')
            //     ->orderBy('product_categories.id', 'desc')
            //     ->get();
            
            $categories = ProductCategory::leftJoin('entitycards', function ($join) {
                $join->on('product_categories.id', '=', 'entitycards.catagory_id');
            })->where("entitycards.campaign_id", $campaign->id)
                ->whereNotNull('entitycards.catagory_id')
                ->whereNull('entitycards.deleted_at')
                ->groupBy('product_categories.id')
                ->orderBy('product_categories.id', 'desc')
                ->get();
            $attrribute_array = array();
            $product_ids = array();
            $content_array = array();

            $data = Entitycard::where('campaign_id', $campaign->id)->whereHas('product', function ($query) {
                return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy('id', 'desc')->get();

            foreach ($data as $entity_card) {
                array_push($product_ids, $entity_card->product_id);
            }
            $values = varchar::whereIn('entity_id', $product_ids)->get();
            foreach ($values as $value => $card) {
                $vals = explode(",", $card->content);
                if(count($vals) > 1){
                    foreach ($vals as $val){
                        array_push($content_array, $val);
                    }
                }
                else{
                    array_push($content_array, $vals[0]);
                }
                array_push($attrribute_array, $card->attribute_id);
            }
            $content_array = array_unique($content_array);
            $attributes = AttributesMetadata::whereIn('attribute_id', $attrribute_array)->get()->groupBy('attribute_group_name');
            $card = Entitycard::where('campaign_id', $campaign->id)->whereNotNull('video_id')->whereNull('product_id')->orderBy('id', 'desc')->first();
            if(!empty($card))
            {
                return view('themes.' . $theme . '.pages.videostore', compact('catalog', 'userSettings', 'meta_data'));   
            }
            else{
                return view('themes.' . $theme . '.pages.videostore', compact('catalog', 'userSettings', 'meta_data'));
            }
        } else {
            return view('templates.errors.404');
        }
    }

    public function catalogProductDetails($campaign, $product)
    {
        if (($product->downgradeable == NULL) || ($product->downgradeable != 1)) {
            $attrribute_array = array();
            $campaign_meta_data = json_decode($campaign->meta_data, true);
            $userSettings = UserInfo::where("user_id", $campaign->user->id)->first();
            $meta_data = json_decode($userSettings->meta_data, true);
            $EntityCard = Entitycard::where('campaign_id', $campaign->id)->where('product_id', $product->id)->orderBy('id', 'desc')->first();
            $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');
            $categories = ProductCategory::all();
            $values = varchar::where('entity_id', $product->id)->pluck('content', 'attribute_id');
            foreach ($values as $value => $card) {
                array_push($attrribute_array, $value);
            }
            $attributes = AttributesMetadata::whereIn('attribute_id', $attrribute_array)->get()->groupBy('attribute_group_name');
            // return view('templates.view.catalogDetails',compact('campaign','userSettings','meta_data','product'));
            return view('themes.' . $theme . '.pages.catalogDetails', compact('campaign', 'userSettings', 'meta_data', 'product', 'attributes', 'values', 'EntityCard'));
        } else {
            return view('errors.404');
        }
    }

    //checkout page navigaion function
    public function checkout($campaign)
    {
        $campaign_meta_data = json_decode($campaign->meta_data, true);
        $userSettings = UserInfo::where("user_id", $campaign->user->id)->first();
        $meta_data = json_decode($userSettings->meta_data, true);
        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');
        $categories = ProductCategory::all();

        $customer = null;

        //prefilling the address for logged in customers
        if(auth()->guard('customer')->check())
        {
            $customer=auth()->guard('customer')->user()->toArray();
            $address=json_decode($customer['meta_data'],true);
            if(isset($address['address']))
            {
                $customer=array_merge($address['address'],$customer);
                $customer['address']=True;
                $customer['amobileno']='';
                $customer['addresstype']='';
            }
            else{
                $address=ordersCustomer::select('address1','address2','city','pincode','state','country','amobileno','addresstype')->where([['customer_id',$customer['customer_id']], ['team_id',$customer['store_owner_id']]])
                ->orwhere([['email',$customer['email']],['mobileno',$customer['mobile']]])->first();
                $customer['address']=False;

                if(!empty($address))
                {
                    $customer['address']=True;
                    $address=$address->toArray();
                    $customer=array_merge($address,$customer);
                }

                
            }
        }
        if($campaign->is_start == 1) {
            $shoppableLink = route('mystore.publishedVideo',$userSettings->public_link);  
        }else{
            $shoppableLink = route('mystore.publishedVideo',$userSettings->preview_link);
        }
        
        return view('themes.' . $theme . '.pages.checkout', compact('campaign', 'userSettings', 'meta_data','customer','shoppableLink'));
    }

    public function stories($campaign)
    {
        $canViewStories = false;
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $campaign->user->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $campaign->user->id)->first();
        }
        if(!empty($subscription)){
            $subscribed_package = $subscription->slug;
            $user = User::where('id', $campaign->user->id)->first();
            $canViewStories = $user->subscription($subscribed_package)->canUseFeature('stories');
        }
        if($canViewStories){
            $campaign_meta_data = json_decode($campaign->meta_data, true);
            $userSettings = UserInfo::where("user_id", $campaign->user->id)->first();
            $meta_data = json_decode($userSettings->meta_data, true);
    
            // $data = Entitycard::where('campaign_id', $campaign->id)->orderBy('id', 'desc')->get();
            $data = Entitycard::with('product')->where("campaign_id", $campaign->id)->whereHas('product', function ($query) {
                return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy('id', 'desc')->get();
            $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');
            $categories = ProductCategory::leftJoin('entitycards', function ($join) {
                $join->on('product_categories.id', '=', 'entitycards.catagory_id');
            })->where("entitycards.campaign_id", $campaign->id)
                ->whereNotNull('entitycards.catagory_id')
                ->whereNull('entitycards.deleted_at')
                ->groupBy('product_categories.id')
                ->orderBy('product_categories.id', 'desc')
                ->get();
            //   dd($categories[0]);
            return view('themes.' . $theme . '.pages.stories', compact('campaign', 'userSettings', 'meta_data', 'data', 'categories'));
        }
        else {
            return view('errors.404');
        }
    }

    public function viewAllProducts($campaign)
    {
        if (!empty($campaign)) {
            $campaign_meta_data = json_decode($campaign->meta_data, true);
            $userSettings = UserInfo::where("user_id", $campaign->user->id)->first();
            $meta_data = json_decode($userSettings->meta_data, true);
            $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');
            $data = Entitycard::where('campaign_id', $campaign->id)->whereHas('product', function ($query) {
                return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy('id', 'desc')->get();

            return view('themes.' . $theme . '.pages.shop-grid', compact('campaign', 'userSettings', 'meta_data', 'data'));
        }
        return;
    }

    public function productViewByCategory($campaign, $category)
    {
        if (!empty($campaign)) {
            $campaign_meta_data = json_decode($campaign->meta_data, true);
            $userSettings = UserInfo::where("user_id", $campaign->user->id)->first();
            $meta_data = json_decode($userSettings->meta_data, true);
            $data = Entitycard::where('campaign_id', $campaign->id)->where('catagory_id', $category->id)->whereHas('product', function ($query) {
                return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy('id', 'desc')->get();
            $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');
            return view('themes.' . $theme . '.pages.category', compact('campaign', 'category', 'userSettings', 'meta_data', 'data'));
        }
        return;
    }

    public function publicSharing($link)
    {
        if ($link->subdomain == "www" && $link->domain == "simplisell")
            return Redirect::to(env('APP_URL', 'localhost'));

        // if (env('SITE_URL') == $link->domain . "." . $link->tld)
        //     return view('landing.index');

        if (!empty($link->subdomain) && !empty($link->domain) && !empty($link->tld)) {
            if($link->subdomain == "www")
                $cdomain = $link->domain . "." . $link->tld;
            else
                $cdomain = $link->subdomain . "." . $link->domain . "." . $link->tld;
            $data = Campaign::where('customdomain', $cdomain)->first();
        }

        if (empty($link->subdomain) && !empty($link->domain) && !empty($link->tld)) {
            $cdomain = $link->domain . "." . $link->tld;
            $data = Campaign::where('customdomain', $cdomain)->first();
        }

        if (empty($data)) {
            if (!empty($link->subdomain))
                $data = Campaign::where('subdomain', $link->subdomain)->first();
        }

        $campaign = "";
        // dd($data);

        if (!empty($data)) {
            if ($data->is_start == 1) {
                $shareableLink = ShareableLink::where('uuid', $data->public_link)->first();
                if (!empty($shareableLink)) {
                    $campaign = $shareableLink->shareable;
                    return $campaign;
                }
            }
        }

        if (!empty($link->shareable_link->shareable) && empty($link->subdomain)) {
            $campaign = Campaign::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();
            // $campaign = $link->shareable_link->shareable;
        } else if (!empty($link->shareable_link->shareable) && !empty($link->subdomain)) {
            $cdomain = $link->subdomain . "." . $link->domain . "." . $link->tld;
            $campaign = Campaign::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();
            $userSettings = UserInfo::where("user_id", $campaign->user_id)->where("customdomain", $cdomain)->where('is_start', 1)->first();
            if (empty($userSettings))
                $userSettings = UserInfo::where("user_id", $campaign->user_id)->where("subdomain", $link->subdomain)->where('is_start', 1)->first();
            if (empty($userSettings))
                return null;
            // return view('errors.404');
            // $campaign = $link->shareable_link->shareable;
        } 

        return $campaign;
    }

    public function storeFrontLogin($catalog, $userSettings)
    {
        $user = $userSettings->user;
        if ($user->isSubscriptionExpired() == true) {
            $grace_period = $user->getGracePeriod();
            if ($grace_period == false) {
                return redirect()->route('store_subscription_expired');
            }
        }
        $meta_data = json_decode($userSettings->meta_data, true);
        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

        return view('themes.' . $theme . '.partials.customer_login', compact('userSettings', 'meta_data'));   
    }

    public function storeFrontRegister($catalog, $userSettings)
    {
        $user = $userSettings->user;
        if ($user->isSubscriptionExpired() == true) {
            $grace_period = $user->getGracePeriod();
            if ($grace_period == false) {
                return redirect()->route('store_subscription_expired');
            }
        }
        $meta_data = json_decode($userSettings->meta_data, true);
        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

        return view('themes.' . $theme . '.partials.customer_register', compact('userSettings', 'meta_data'));   
    }

    public function storeFrontForgotPassword($catalog, $userSettings)
    {
        $user = $userSettings->user;
        if ($user->isSubscriptionExpired() == true) {
            $grace_period = $user->getGracePeriod();
            if ($grace_period == false) {
                return redirect()->route('store_subscription_expired');
            }
        }
        $meta_data = json_decode($userSettings->meta_data, true);
        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

        return view('themes.' . $theme . '.partials.customer_forgot_password', compact('userSettings', 'meta_data'));   
    }
}
