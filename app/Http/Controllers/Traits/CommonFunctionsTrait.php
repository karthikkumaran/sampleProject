<?php

namespace App\Http\Controllers\Traits;
use Illuminate\Http\Request;
use App\Campaign;
use App\Http\Controllers\Controller;
use Gate;
use Auth;
use Session;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use App\LaravelSubscriptions\Plan;
use App\LaravelSubscriptions\PlanFeature;
use App\LaravelSubscriptions\PlanSubscription;
use App\LaravelSubscriptions\PlanSubscriptionUsage;
use App\LaravelSubscriptions\PlanSubscriptionFeatures;
use App\LaravelSubscriptions\PlanSubscribedFeatures;

trait CommonFunctionsTrait{

    public function getSubscription(){
        $user = Auth::user();
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $user->id)->first();
        }
        $subscribed_package = "";
        if(!empty($subscription)){
            $subscribed_package = $subscription->slug;
        }
        
        return compact('user', 'subscription', 'subscribed_package');
    }

    public function getSubscriptionFeatureUsage(Request $request)
    {
                $data = $this->getSubscription();
        // dd($data['user'],$data['subscribed_package'],$request->featurename);
        // print_r($data['user']->subscription($data['subscribed_package'])->getFeatureUsage($request->featurename));
        // echo $data['subscribed_package'];
        // print_r($request);
        // echo $request->featurename;
        // exit;
        
        if(!empty($data['subscription'])){
            $usage = $data['user']->subscription($data['subscribed_package'])->getFeatureUsage($request->featurename);
            $usage_limit = $data['user']->subscription($data['subscribed_package'])->getFeatureValue($request->featurename);
            
            $usage_remaining = $usage_limit - $usage;
            Session::flash('subscription_usage_remaining',$usage_remaining);
            $alert = false;
            $alert_msg="";
            if($usage >= $usage_limit){
                $alert = "limit_reached";
            }
            else if($usage >= 0.8*$usage_limit){
                $alert = "limit_almost_reached";
            }
            if ($request->ajax()) {
                return $this->alertUsageLimitReached($alert,'json');
            }
            else{
                return $this->alertUsageLimitReached($alert,'session');
            }
        }
    }

    public function alertUsageLimitReached($type,$returntype){ 
        
        $alert = $type;
        $alert_msg = "";
        switch ($type) {
            case 'limit_reached':
                $alert_msg = "You have created maximum number of catalog allowed in this plan. If you like to create additional catalogs please upgrade your plan.";
                break;
            case 'limit_almost_reached':
                $alert_msg = "You have used more than 80% of your plan. Please update your plan at the earliest.";
                break;
            case 'limit_exceeded':
                $alert_msg = "The number of products you tried to upload exceeds your remaining usage limit. Please update your plan at the earliest.";
                break;
        }
        $alert_dom = '<h6 class="alert alert-danger alert-validation-msg" role="alert">
                        <p><i class="feather icon-info mr-1 align-middle"></i>'.$alert_msg.'</p>
                        <p class="text-center"><a href="'. route('admin.subscriptions.index') . '" class="btn btn-info btn-xs">Upgrade plan</a></p></h6>';
                
        if($returntype == "json"){
            return response()->json(['alert' => $alert,'alert_msg' =>$alert_dom]);
        }
        else if($returntype == "session"){
            if($type == "limit_reached"){
                Session::flash('subscription_exhausted',$alert_msg);
            }
            else if($type == "limit_exceeded"){
                Session::flash('subscription_usage_exceeded',$alert_msg);
            }
            Session::flash('subscription_alert_message',$alert_msg);
            
            return;
        }
    }

    public function getFeatureUsageRemaining(Request $request){
        
        $data = $this->getSubscription();
        $usage_remaining = $data['user']->subscription($data['subscribed_package'])->getFeatureRemainings($request->featurename);
        return $usage_remaining;
    }

    public function newUserTrialSubscription($user){
        
        $plan = Plan::where('slug', "user-trial")->first();
        $user->newSubscription($plan->name, $plan);
        $user->subscription_status = 1;
        $user->save();
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $user->id)->first();
        }
        $now = Carbon::now();
        $features = $plan->features;
        $subscribed_features = [];
        foreach ($features as $feature) {
            $subscribed_features[] = [
                'user_id' => $user->id,
                'subscription_id' => $subscription->id,
                'plan_id' => $plan->id,
                'feature_id' => $feature->feature_id,
                'value' => $feature->value,
                'resettable_period' => $feature->resettable_period,
                'resettable_interval' => $feature->resettable_interval,
                'is_approved' => 1,
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }
        PlanSubscribedFeatures::insert($subscribed_features);
        Session::forget('subscription_check');
        $no_subscription = false;
        $trial_subscription = true;
        $current_time = Carbon::now();
        $trial_period = $current_time->diff($subscription->ends_at);
        $trial_duration = $trial_period->d;
        $subscription_expiry_alert = false;
        $grace_period = false;
        $subscription_grace_period_alert = false;
        $subscription_expired = false;
        $subscription_start_alert = false;
        Session::forget('subscription_check');
        Session::push('subscription_check', ['no_subscription' => $no_subscription, 'trial_subscription' => $trial_subscription, 'trial_duration' => $trial_duration,'subscription_expiry_alert' => $subscription_expiry_alert, 'subscription_expired' => $subscription_expired, 'grace_period' => $grace_period, 'subscription_start_alert' => $subscription_start_alert]);
        $this->setSubscribedFeaturesSession($user);
    }
    
    public function setSubscriptionCheckSession($user) {
        if ($user->getIsAdminAttribute() == false) {
            $no_subscription = false;
            $trial_subscription = false;
            $trial_duration = false;
            $subscription_expiry_alert = false;
            $grace_period = false;
            $subscription_grace_period_alert = false;
            $subscription_expired = false;
            $subscription_start_alert = false;
            $user_trial = $user->isOnUserTrial();
            if($user->subscription_status != 1){ //Subscription_status -> 1 // means user is subscribed to a plan or on trial.
                // if($user->isFirstTimeUser() != true){    
                                   
                    $no_subscription = true;
                // }
            }
            else{
                if(!empty($user_trial)){
                                        
                    $trial_subscription = true;
                    if($user_trial->invert == 1){
                        $trial_duration = -1;
                    }
                    else{
                    
                        $trial_duration = $user_trial->d;
                    }
                }
                else{
                    
                    $expiry_duration = $user->getSubscriptionExpiryDuration();
                    if($expiry_duration->invert == 0 && $expiry_duration->y == 0 && $expiry_duration->m == 0){
                        if($expiry_duration->d <= intval(env('SUBSCRIPTION_EXPIRATION_ALERT'))){
                            $subscription_expiry_alert = $expiry_duration->d;
                        }
                    }

                    $start_duration = $user->getSubscriptionStartDuration();
                    $trial_ends_duration = $user->getSubscriptionTrialDuration();

                    if($trial_ends_duration->invert == 1 && $start_duration->invert == 0){
                            $subscription_start_alert = 1;
                    }

                }

                if($user->isSubscriptionExpired() == true){

                    $subscription_expired =true;
                    $grace_period = $user->getGracePeriod();
                    if($grace_period != false){
                        if($grace_period->d == 0){
                            $grace_period = 1;
                        }
                        else{
                            $grace_period = $grace_period->d;
                        }
                    }
                }
                else{
                    
                    $this->setSubscribedFeaturesSession($user);
                }
            }
           
            Session::forget('subscription_check');
            Session::push('subscription_check', ['no_subscription' => $no_subscription, 'trial_subscription' => $trial_subscription, 'trial_duration' => $trial_duration,'subscription_expiry_alert' => $subscription_expiry_alert, 'subscription_expired' => $subscription_expired, 'grace_period' => $grace_period, 'subscription_start_alert' => $subscription_start_alert]);
        }
        return;
    }
    
    public function setSubscribedFeaturesSession($user){
        
        Session::forget('subscribed_features');
        $subscription_features = array();
        $plan_subscribed_features = PlanSubscribedFeatures::where('user_id', $user->id)->get();                   
        foreach($plan_subscribed_features as $feature){
            $feature_details = $feature->getFeatureDetails();
            $subscription_features[$feature_details->name] = true;
        }
       
        Session::push('subscribed_features', $subscription_features);
        return;
    }
    
}