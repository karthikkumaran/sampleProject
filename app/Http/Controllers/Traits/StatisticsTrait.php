<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Support\Carbon;
use App\Models\Statistics\Events;

trait StatisticsTrait
{

    public function scopeLast7days($query)
    {
        $sdate = Carbon::tomorrow()->subdays(7);
        $edate = Carbon::tomorrow()->format('Y-m-d');
      
        $stat = $query->selectRaw('DATE(created_at) as periodname')->whereBetween('created_at', [$sdate,$edate]);
        return $stat;
    }

    public function scopeLast30days($query)
    {
        $stat = $query->selectRaw('DATE(created_at) as periodname')->where('created_at', '>=', Carbon::tomorrow()->subDays(30)->format('Y-m-d'));
        return $stat;
    }

    public function scopeToday($query)
    {
        $stat = $query->selectRaw('HOUR(created_at) as periodname')->whereDate('created_at', Carbon::today()->toDateString());
        return $stat;
    }

    public function scopeYesterday($query)
    {
        $stat = $query->selectRaw('HOUR(created_at) as periodname')
            ->whereDate('created_at', date("Y-m-d", strtotime('-1 days')));
        return $stat;
    }

    public function scopeThismonth($query)
    {
        $stat = $query->selectRaw('DATE(created_at) as periodname')->whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'));
        return $stat;
    }

    public function scopeMonthly($query)
    {
        $stat = $query->selectRaw("MONTHNAME(created_at) as periodname")->whereYear('created_at', date('Y'));
        return $stat;
    }

    public function scopeYearly($query)
    {
        $stat = $query->selectRaw("YEAR(created_at) as periodname");
        return $stat;
    }
}
