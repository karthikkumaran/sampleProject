<?php

namespace App\Http\Controllers\Traits;

use App\Http\Controllers\Admin\ProductController;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use SpreadsheetReader;
use SpreadsheetReader_CSV;
use SpreadsheetReader_XLS;
use SpreadsheetReader_XLSX;

trait CsvImportTrait
{
    public function processCsvImport(Request $request)
    {
        try {
            $filename = $request->input('filename', false);
            $path     = storage_path('app/csv_import/' . $filename);

            $hasHeader = $request->input('hasHeader', false);

            $fields = $request->input('fields', false);
            $fields = array_flip(array_filter($fields));
             
            $modelName = $request->input('modelName', false);
            $model     = "App\\" . $modelName;

            $reader = new SpreadsheetReader($path);
            $insert = [];

            foreach ($reader as $key => $row) {
                if ($hasHeader && $key == 0) {
                    continue;
                }

                $tmp = [];

                foreach ($fields as $header => $k) {
                    if (isset($row[$k])) {
                        $tmp[$header] = $row[$k];
                    }
                }

                if (count($tmp) > 0) {
                    $insert[] = $tmp;
                }
            }

            $for_insert = array_chunk($insert, 100);
            $insertion_items = array();
            foreach ($for_insert as $insert_item){
                
                foreach ($insert_item as $item)
                {
                    $item['team_id'] = auth()->user()->team_id;
                    if($item['sku'])
                    {
                        $keyDigit = 4;
                        
                        $total = Product::where('sku',$item['sku'])->where('team_id',$item['team_id'])->get();
                        if(count($total) > 0)
                        {
                            $sku = strtoupper(str::random($keyDigit));
                            $item['sku'] = $sku;
                        }
                        else
                        {
                            $item['sku'] = $item['sku'];
                        }
                    }
                    array_push($insertion_items,$item);
                    
                }
            }
            $model::insert($insertion_items);

            $rows  = count($insert);
            $table = Str::plural($modelName);

            File::delete($path);

            session()->flash('message', trans('global.app_imported_rows_to_table', ['rows' => $rows, 'table' => $table]));

            return redirect($request->input("redirect"));
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    public function parseCsvImport(Request $request)
    {
        
        $file = $request->file('csv_file');
        $request->validate([
            'csv_file' => 'mimes:csv,txt,xlsx,xls',
        ]);
        
        $path      = $file->path();
        $hasHeader = $request->input('header', false) ? true : false;
        
        if($file->getClientMimeType() == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        {
            $reader  = new SpreadsheetReader_XLSX($path);
        }
        else if($file->getClientMimeType() == 'application/vnd.ms-excel')
        {
            $reader  = new SpreadsheetReader_XLS($path);
        }
        else
        {
            $reader  = new SpreadsheetReader($path);
        }
        $headers = $reader->current();
        $lines   = [];
        $lines[] = $reader->next();
        $lines[] = $reader->next();
        if($file->getClientMimeType() == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        {
            $filename = Str::random(10) . '.xlsx';

        }
        else if($file->getClientMimeType() == 'application/vnd.ms-excel')
        {
            $filename = Str::random(10) . '.xls';
        }
        else
        {
            $filename = Str::random(10) . '.csv';
        }
       
            
        // $file->storeAs('csv_import', $filename);
        Storage::disk('local')->put('csv_import/'.$filename, file_get_contents($file));

        $modelName     = $request->input('model', false);
        $fullModelName = "App\\" . $modelName;
        //print_r($lines);
        $model     = new $fullModelName();
        $fillables = $model->getFillable();
        
        $redirect = url()->previous();
        
        $routeName = 'admin.' . strtolower(Str::plural(Str::kebab($modelName))) . '.processCsvImport';
        
        return view('csvImport.parseInput', compact('headers', 'filename', 'fillables', 'hasHeader', 'modelName', 'lines', 'redirect', 'routeName'));
    }
}
