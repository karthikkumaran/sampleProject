<?php declare(strict_types=1);

namespace App\Http\Controllers\Shareable;

use Illuminate\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;

use App\UserInfo;

class ShareableLinkPasswordController
{
    public function show(ShareableLink $link): View
    {
        $campaign = $link->shareable;
        $userSettings = UserInfo::where("user_id", $campaign->user->id)->first();
        $meta_data = json_decode($userSettings->meta_data, true);
        // return view('shareable-model::password', ['link' => $shareableLink]);
        return view('shareable.password', compact('link','campaign','userSettings','meta_data'));
    }

    public function store(Request $request, ShareableLink $shareableLink): RedirectResponse
    {
        if (password_verify($request->get('password'), $shareableLink->password)) {
            session([$shareableLink->uuid => true]);
        }

        return redirect(config('shareable-model.base_url').'/'.$shareableLink->uuid);
        // return redirect($shareableLink->url);
        // return redirect()->back();
    }
}