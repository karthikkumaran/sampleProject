<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ordersCustomer;
use App\ordersProduct;
use App\UserInfo;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Gate;
use DB;
use App\orders;

class CustomersController extends Controller
{
    //new index function  for list of all customers
    public function index(Request $request)
    {
        return view("templates.admin.customer.index");
    }

    //new function for cutomer order details
    public function CustomerOrderDetails($id)
    {
        abort_if(Gate::denies('order_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return view('templates.admin.customer.show',['id'=>$id]);
    }

    //new function for cutomer details
    public function CustomerInfo($id)
    {
        abort_if(Gate::denies('order_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return view("templates.admin.customer.orders",['id'=>$id] );
    }

    public function customerdetails($id)
    {
        abort_if(Gate::denies('order_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $customer_total_price = 0;
        $userSettings = UserInfo::where("user_id", auth()->user()->id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);
        $email = ordersCustomer::where("id", $id)->pluck("email");
        $customer = ordersCustomer::select('*', DB::raw('count(*) as total'))->where("email", $email)->get();
        $orders = ordersCustomer::select('*')->where("email", $email);
        $customer_order_data = $orders->paginate();

        foreach ($customer_order_data as $order) {
            $order_meta_data = json_decode($order->meta_data, true);
            $customer_total_price += $order_meta_data['product_total_price'];
        }
        return view("templates.admin.customer.orders", compact("customer", "customer_total_price", "user_meta_data"));
    }

    public function displayorderlist(Request $request, $id = "")
    {
        $sort = ['id', 'desc'];
        $page = 10;


        // dd($id);
        abort_if(Gate::denies('order_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // dd($request);
        if ($request->ajax()) {

            $q = $request->q;
            $email = ordersCustomer::where("id", $request->customerid)->pluck("email");
            $orders = ordersCustomer::select('*')->where("email", $email);
            $userSettings = UserInfo::where("user_id", auth()->user()->id)->first();
            $user_meta_data = json_decode($userSettings->meta_data, true);
            if (isset($request->s) && !empty($request->s)) {
                $sort = explode(',', $request->s);
            }
            if (isset($request->q) && !empty($request->q)) {

                // dd($request->q);
                $data = $orders->where("order_id", "LIKE", '%' . $q . '%')->paginate($page);
            } else {


                $data = $orders->paginate($page);
            }

            // dd($data);
            return view("cards.orders", compact("data", "user_meta_data"));
        }
        // dd($customer);
        // $customer = ordersCustomer::select('*', DB::raw('count(*) as total'))->where("email", $email)->get();
        // return view("templates.admin.customer.orders",compact("customer"));
        // return false;
    }

    public function displayorder($orderid)
    {
        // dd($orders);
        abort_if(Gate::denies('order_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        // $orders->load('team');
        // $order = orders::where("id", $orders)->first();

        $transaction = Orders::find($orderid)->transactions()->get();
        $transaction = count($transaction) == 0 ? null : $transaction[0];
        $customer = ordersCustomer::where("order_id", $orderid)->first();
        $userSettings = UserInfo::where("user_id", auth()->user()->id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);
        // dd($orders);
        return view('templates.admin.customer.show', compact( 'customer', 'user_meta_data', 'transaction'));
    }

    public function customerajaxData(Request $request)
    {
        $sort = ['id', 'desc'];
        $page = 10;
        $customers = ordersCustomer::select('*', DB::raw('count(*) as total'));
        $userSettings = UserInfo::where("user_id", auth()->user()->id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);
        // dd($request->ajax());
        if ($request->ajax()) {

            if (isset($request->s) && !empty($request->s)) {

                $sort = explode(',', $request->s);
            }

            if (isset($request->q) && !empty($request->q)) {
                $q = $request->q;
                $data = $customers
                    ->orwhere('fname', 'Like', '%' . $q . '%')
                    ->orWhere('lname', 'LIKE', '%' . $q . '%')
                    ->orWhere('email', 'LIKE', '%' . $q . '%')
                    ->orWhere('mobileno', 'LIKE', '%' . $q . '%')
                    ->groupBy('email', 'mobileno','customer_id')->paginate($page);
                    
            } else {
                $data = $customers->groupBy('email', 'mobileno','customer_id')->paginate($page);
            }

            return view("cards.customers", compact('data', 'user_meta_data'));
        }
        // $data = $customer->paginate($request->page);
        // return view("cards.customers", compact('data'));
        $customercount = $customers->count();
        return view("templates.admin.customer.index", compact('customercount'));
    }

}
