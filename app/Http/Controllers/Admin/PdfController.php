<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use Gate;

use App\orders;
use App\ordersProduct;
use App\ordersCustomer;
use App\Campaign;
use App\Entitycard;
use App\UserInfo;
use App\Product;
use PDF;
use App\SubscriptionPayment;
use App\Models\PlanInvoice;
use App\LaravelSubscriptions\plan;


class PdfController extends Controller
{
  public function export_catalog_pdf(Request $request)
  {
    ini_set('max_execution_time', 300); //300 seconds = 5 minutes
    ini_set('memory_limit', '3000M'); //This might be too large, but depends on the data set
    // Send data to the view using loadView function of PDF facade
    $data["campaign"] = $campaign = Campaign::where('public_link', $request->shareable_link->uuid)->where('is_start', 1)->first();

    $data["campaign_meta_data"] = json_decode($campaign->meta_data, true);
    $data["userSettings"] = $userSettings = UserInfo::where("user_id", $campaign->user->id)->first();
    $data["meta_data"] = json_decode($userSettings->meta_data, true);
    $data["data"] = Entitycard::where('campaign_id', $campaign->id)->orderBy('id', 'desc')->get();

    $pdf = PDF::loadView('pdf.catalogPdf', $data);
    // return $pdf->stream();
    // return view('pdf.catalogPdf',$data);

    // Finally, you can download the file using download function
    return $pdf->download($data["meta_data"]["storefront"]["storename"] . '- ' . $data["campaign"]["name"] . '.pdf');
  }

  public function export_order_pdf(Request $request)
  {

    abort_if(Gate::denies('order_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
    ini_set('max_execution_time', 300); //300 seconds = 5 minutes
    ini_set('memory_limit', '3000M'); //This might be too large, but depends on the data set
    // $order = orders::where("id", $request->orders)->first();
    // $products = ordersProduct::where("order_id", $order->id)->get();
    $customer = ordersCustomer::where("order_id", $request->orders)->first();
    $userSettings = UserInfo::where("user_id", auth()->user()->id)->first();
    $user_meta_data = json_decode($userSettings->meta_data, true);

    $transaction_id = Orders::find($request->orders)->transactions()->get();

    $transaction = count($transaction_id) > 0 ? $transaction_id->toArray()[0] : null;

    $data['order'] = $customer->order;
    $data['products'] = $customer->orderProducts;
    $data['customer'] = $customer;
    $data['userSettings'] = $userSettings;
    $data['transaction'] = $transaction;
    // dd($order->customer);

    $data['meta_data'] = $user_meta_data;
      if(!isset($data['meta_data']["order_template"]["order_template"]) || $data['meta_data']["order_template"]["order_template"]=="default_template")
      {
        $pdf = PDF::loadView('pdf.orderDefaultPdf', $data);
      }
      else
      {
        $pdf = PDF::loadView('pdf.orderModernPdf', $data);
      }
    // return $pdf->stream();
    // return view('pdf.invoicePdf',$data);
    // Finally, you can download the file using download function
    return $pdf->download($data["meta_data"]["storefront"]["storename"] . ' - Order #' . $customer->order->order_ref_id . '.pdf');
  }

  public function export_label_pdf(Request $request)
  {
    // dd('hai');

    abort_if(Gate::denies('order_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
    ini_set('max_execution_time', 300); //300 seconds = 5 minutes
    ini_set('memory_limit', '3000M'); //This might be too large, but depends on the data set
    // $order = orders::where("id", $request->orders)->first();
    // $products = ordersProduct::where("order_id", $order->id)->get();
    $customer = ordersCustomer::where("order_id", $request->orders)->first();
    $userSettings = UserInfo::where("user_id", auth()->user()->id)->first();
    $user_meta_data = json_decode($userSettings->meta_data, true);

    $transaction_id = Orders::find($request->orders)->transactions()->get();

    $transaction = count($transaction_id) > 0 ? $transaction_id->toArray()[0] : null;

    $data['order'] = $customer->order;
    $data['products'] = $customer->orderProducts;
    $data['customer'] = $customer;
    $data['userSettings'] = $userSettings;
    $data['transaction'] = $transaction;
    // dd($order->customer);

    $data['meta_data'] = $user_meta_data;
    $pdf = PDF::loadView('pdf.labelPdf', $data);

    // return $pdf->stream();
    // return view('pdf.invoicePdf',$data);
    // Finally, you can download the file using download function
    return $pdf->download($data["meta_data"]["storefront"]["storename"] . ' - label #' . $customer->order->order_ref_id . '.pdf');
  }

  public function export_invoice_pdf(Request $request)
  {
    $customer = ordersCustomer::where("order_id", $request->orders)->first();
    $userSettings = UserInfo::where("user_id", auth()->user()->id)->first();

    $user_meta_data = json_decode($userSettings->meta_data, true);

    $transaction_id = Orders::find($request->orders)->transactions()->get();

    $transaction = count($transaction_id) > 0 ? $transaction_id->toArray()[0] : null;

    $data['order'] = $customer->order;
    $data['products'] = $customer->orderProducts;
    $data['customer'] = $customer;
    $data['userSettings'] = $userSettings;
    $data['transaction'] = $transaction;
    // dd($order->customer);

    $data['meta_data'] = $user_meta_data;
    if(!isset($data['meta_data']["invoice_template"]["invoice_template"]) || $data['meta_data']["invoice_template"]["invoice_template"]=="default_template")
    {
      $pdf = PDF::loadView('pdf.invoiceDefaultPdf', $data);
    }
    elseif($data['meta_data']["invoice_template"]["invoice_template"]=="modern_template"){
      $pdf = PDF::loadView('pdf.invoiceModernPdf', $data);
    }


    return $pdf->download($data["meta_data"]["storefront"]["storename"] . ' - invoice #' . $customer->order->order_ref_id . '.pdf');

  }

  public function customer_export_order_pdf(Request $request)
  {
    ini_set('max_execution_time', 300); //300 seconds = 5 minutes
    ini_set('memory_limit', '3000M'); //This might be too large, but depends on the data set

    $customer = ordersCustomer::where('order_id', (int)$request->orders)->first();
    // dd($customer,$request->orders);
    $user_id = $customer->user_id;
    $userSettings = UserInfo::where("user_id", $user_id)->first();
    $user_meta_data = json_decode($userSettings->meta_data, true);

    $transaction_id = Orders::find($request->orders)->transactions()->get();

    $transaction = count($transaction_id) > 0 ? $transaction_id->toArray()[0] : null;

    $data['order'] = $customer->order;
    $data['products'] = $customer->orderProducts;
    $data['customer'] = $customer;
    $data['userSettings'] = $userSettings;
    $data['meta_data'] = $user_meta_data;
    $data['transaction_id'] = $transaction;

    // $pdf = PDF::loadView('pdf.orderPdf', $data);
    if(!isset($data['meta_data']["order_template"]["order_template"]) || $data['meta_data']["order_template"]["order_template"]=="default_template")
      {
        $pdf = PDF::loadView('pdf.orderDefaultPdf', $data);
      }
      else
      {
        $pdf = PDF::loadView('pdf.orderModernPdf', $data);
      }

    // return view('pdf.invoicePdf',$data);


    return $pdf->download($data["meta_data"]["storefront"]["storename"] . ' - Order #' . $customer->order->order_ref_id . '.pdf');
  }

  public function export_user_payment_pdf(Request $request)
  {
    ini_set('max_execution_time', 300); //300 seconds = 5 minutes
    ini_set('memory_limit', '3000M'); //This might be too large, but depends on the data set
    $user_payment = SubscriptionPayment::where('id', $request->payment_id)->first();
    $auth_user = auth()->user();
    $user_id = $user_payment->user_id;
    $planinvoice = PlanInvoice::where('subscription_payment_id',$user_payment->id)->where('user_id',$user_payment->user_id)->first();
    $userSettings = UserInfo::where("user_id", $user_id)->first();
    $user_meta_data = json_decode($userSettings->meta_data, true);
    // $plan = plan::where('slug',$user_payment->plan_name)->first();
    $plan = plan::where('name','like','%'.$user_payment->plan_name.'%')->first();
    $data['planinvoice'] = $planinvoice;
    $data['user_meta_data'] = $user_meta_data;
    $data['user_payment'] = $user_payment;
    $data['plan'] = $plan;
    $pdf = PDF::loadView('pdf.userPaymentPdf', $data);

    return $pdf->download('My Payment #' . $user_payment->id . '.pdf');
  }

  public function print_user_payment_pdf(Request $request)
  {
    ini_set('max_execution_time', 300); //300 seconds = 5 minutes
    ini_set('memory_limit', '3000M'); //This might be too large, but depends on the data set
    $user_payment = SubscriptionPayment::where('id', $request->payment_id)->first();
    $auth_user = auth()->user();
    $user_id = $user_payment->user_id;
    $planinvoice = PlanInvoice::where('subscription_payment_id',$user_payment->id)->where('user_id',$user_payment->user_id)->first();
    $userSettings = UserInfo::where("user_id", $user_id)->first();
    $user_meta_data = json_decode($userSettings->meta_data, true);
    // $plan = plan::where('slug',$user_payment->plan_name)->first();
    $plan = plan::where('name','like','%'.$user_payment->plan_name.'%')->first();
    $data['user_meta_data'] = $user_meta_data;
    $data['planinvoice'] = $planinvoice;
    $data['user_payment'] = $user_payment;
    $data['plan'] = $plan;
    $pdf = PDF::loadView('pdf.userPaymentPdf', $data);

    return $pdf->stream();
  }

  public function print_pos_invoice_pdf(Request $request)
  {
    ini_set('max_execution_time', 300); //300 seconds = 5 minutes
    ini_set('memory_limit', '3000M'); //This might be too large, but depends on the data set

    $customer = ordersCustomer::where("order_id", $request->orders)->first();
    $user_id = auth()->user()->id;
    $userSettings = UserInfo::where("user_id", $user_id)->first();
    $user_meta_data = json_decode($userSettings->meta_data, true);

    $transaction_id = Orders::find($request->orders)->transactions()->get();

    $transaction = count($transaction_id) > 0 ? $transaction_id->toArray()[0] : null;

    $data['order'] = $customer->order;
    $data['products'] = $customer->orderProducts;
    $data['customer'] = $customer;
    $data['userSettings'] = $userSettings;
    $data['meta_data'] = $user_meta_data;
    $data['transaction_id'] = $transaction;

    $pdf = PDF::loadView('pdf.posInvoicePdf', $data);

    // return $pdf->download('My Payment #'.$customer->order->id.'.pdf');
    return $pdf->stream();
  }
}
