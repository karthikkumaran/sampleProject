<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\UserInfo;
use App\Product;
use App\Variant;
use App\ProductVariant;
use App\ProductCategory;
use App\Entitycard;
use Gate;
use QrCode;
use App\User;
use App\orders;
use App\enquiry;
use App\Varchar;
use App\Campaign;
use Carbon\Carbon;
use App\AttributesMetadata;
use Illuminate\Http\Request;
use App\Models\Statistics\Events;
use App\LaravelSubscriptions\Plan;
use Illuminate\Support\HtmlString;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Redirect;
use App\LaravelSubscriptions\PlanFeature;
use App\Http\Requests\StoreCampaignRequest;
use App\Http\Requests\UpdateCampaignRequest;
use App\LaravelSubscriptions\PlanSubscription;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\MassDestroyCampaignRequest;
use App\Http\Controllers\Traits\AddEntityCardTrait;
use App\LaravelSubscriptions\PlanSubscriptionUsage;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\LaravelSubscriptions\PlanSubscribedFeatures;
use App\Http\Controllers\Traits\CommonFunctionsTrait;
use App\LaravelSubscriptions\PlanSubscriptionFeatures;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;

class CampaignsController extends Controller
{
    use AddEntityCardTrait;
    use MediaUploadingTrait;
    use CommonFunctionsTrait;


    public function index(Request $request)
    {
        abort_if(Gate::denies('campaign_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // if ($request->ajax()) {
        //     $query = Campaign::with(['team'])->select(sprintf('%s.*', (new Campaign)->table));
        //     $table = Datatables::of($query);

        //     $table->addColumn('placeholder', '&nbsp;');
        //     $table->addColumn('actions', '&nbsp;');

        //     $table->editColumn('actions', function ($row) {
        //         $viewGate      = 'campaign_show';
        //         $editGate      = 'campaign_edit';
        //         $deleteGate    = 'campaign_delete';
        //         $crudRoutePart = 'campaigns';

        //         return view('partials.datatablesActions', compact(
        //             'viewGate',
        //             'editGate',
        //             'deleteGate',
        //             'crudRoutePart',
        //             'row'
        //         ));
        //     });

        //     $table->editColumn('id', function ($row) {
        //         return $row->id ? $row->id : "";
        //     });
        //     $table->editColumn('name', function ($row) {
        //         return $row->name ? $row->name : "";
        //     });
        //     $table->editColumn('is_start', function ($row) {
        //         return $row->is_start ? Campaign::IS_START_SELECT[$row->is_start] : '';
        //     });
        //     $table->editColumn('type', function ($row) {
        //         return $row->type ? Campaign::TYPE_SELECT[$row->type] : '';
        //     });
        //     $table->editColumn('is_active', function ($row) {
        //         return $row->is_active ? Campaign::IS_ACTIVE_SELECT[$row->is_active] : '';
        //     });
        //     $table->editColumn('preview_link', function ($row) {
        //         return $row->preview_link ? $row->preview_link : "";
        //     });
        //     $table->editColumn('public_link', function ($row) {
        //         return $row->public_link ? $row->public_link : "";
        //     });

        //     $table->rawColumns(['actions', 'placeholder']);

        //     return $table->make(true);
        // }

        // return view('admin.campaigns.index');

        // $campaign_views = Events::where('name', 'catalog')->groupBy('campaign_id')
        //     ->selectRaw('campaign_id,count(campaign_id) as views')->orderBy('campaign_id', 'desc')->paginate(9);

        $campaign = Campaign::where('type', '3')->orderBy('is_start', 'desc')->orderBy('id', 'desc')->paginate(9);

        foreach ($campaign as $key => $value) {
            $campaign[$key]['views'] = Events::where('name', 'catalog')->where('campaign_id', $value->id)->count();
        }
        // dd($campaign->toArray(), $campaign_views->toArray());
        $user = Auth::user();
        if (!$user->getIsAdminAttribute()) {
            if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')) {
                $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
            } else {
                $subscription = PlanSubscription::where('user_id', $user->id)->first();
            }
            if (!empty($subscription)) {
                $allowed_catalogs = $user->subscription($subscription->slug)->getFeatureValue('catalogs');
            } else {
                $allowed_catalogs = "true";
            }
        } else {
            $allowed_catalogs = "true";
        }
        $this->getSubscriptionFeatureUsage(new Request(["featurename" => "catalogs"]));
        return view('admin.campaigns.index', ['campaignLists' => $campaign, 'allowed_catalogs' => $allowed_catalogs]);
    }

    public function videoindex(Request $request)
    {
        abort_if(Gate::denies('campaign_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');



        $campaign = Campaign::where('type', '2')->orderBy('is_start', 'desc')->orderBy('id', 'desc')->paginate(9);

        foreach ($campaign as $key => $value) {
            $campaign[$key]['views'] = Events::where('name', 'catalog')->where('campaign_id', $value->id)->count();
        }
        // dd($campaign->toArray(), $campaign_views->toArray());
        $user = Auth::user();
        if (!$user->getIsAdminAttribute()) {
            if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')) {
                $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
            } else {
                $subscription = PlanSubscription::where('user_id', $user->id)->first();
            }
            if (!empty($subscription)) {
                $allowed_catalogs = $user->subscription($subscription->slug)->getFeatureValue('catalogs');
            } else {
                $allowed_catalogs = "true";
            }
        } else {
            $allowed_catalogs = "true";
        }
        $this->getSubscriptionFeatureUsage(new Request(["featurename" => "catalogs"]));
        return view('admin.campaigns.videoindex', ['campaignLists' => $campaign, 'allowed_catalogs' => $allowed_catalogs]);
    }


    public function storeIndex(Request $request)
    {
        abort_if(Gate::denies('campaign_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if (auth()->user()->getIsAdminAttribute()) {
            return redirect('/admin/adminDashboard');
        }

        $user = auth()->user();
        if ($user->email != $user->team->name) {
            $user = User::where('email', $user->team->name)->first();
        }

        $user_id = $user->id;

        if ($user->isFirstTimeUser()) {
            if ($user->is_on_trial == 1) {
                $trial_plan = Plan::where('slug', 'user-trial')->first();
            } else {
                if ($user->subscription_status != 1) { //Subscription_status -> 1 // means user is subscribed to a plan or on trial.
                    $packages = Plan::get();

                    return view('admin.subscriptions.first_time_user_subscription', compact('packages'));
                }
                $trial_plan = null;
            }

            return view('admin.campaigns.store_onboarding', compact('user', 'trial_plan'));
        }

        $campaignLists = Campaign::where("is_added_home", "1")->orderBy('id', 'desc')->paginate(9);
        $userSettings = UserInfo::where("user_id", $user_id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);
        // $subscription = PlanSubscription::where('user_id', $user_id)->first();
        // if(!empty($subscription)){
        //     if($subscription->slug == "user-trial"){
        //         $current_time = Carbon::now();
        //         $trial_period = $current_time->diff($subscription->ends_at);
        //     }
        //     else{

        //     }
        // }
        // else{

        // }
        $data['catalogs_count'] = Campaign::count();
        $data['orders_count'] = orders::count();
        $data['products_count'] = Product::count();
        foreach ($campaignLists as $key => $value) {
            $campaignLists[$key]['views'] = Events::where('name', 'catalog')->where('campaign_id', $value->id)->count();
        }
        return view('admin.campaigns.store_index', compact('campaignLists', 'userSettings', 'user_meta_data', 'data'));
    }

    public function create()
    {
        abort_if(Gate::denies('campaign_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $products = Product::all();
        return view('admin.campaigns.create', compact('products'));
    }

    public function CampCreate(Request $request)
    {
        $user = Auth::user();
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')) {
            $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
        } else {
            $subscription = PlanSubscription::where('user_id', $user->id)->first();
        }
        $canCreateCampaign = true;
        $catalog_type = "catalogs";
        if ($request->type == 2) {
            $catalog_type = "Video catalog";
        }

        if (!empty($subscription)) {
            $subscribed_package = $subscription->slug;
            $canCreateCampaign = $user->subscription($subscribed_package)->canUseFeature($catalog_type);
        }

        if ($canCreateCampaign) {
            $team = $user->team;
            $name = $request->name;
            $is_start = $request->is_start;
            $is_private = $request->is_private;
            $is_added_home = $request->is_added_home;
            $is_expired = $request->is_expired;
            $campaign = Campaign::create(array_merge($request->all(), ['name' => $name, 'is_start' => $is_start, 'is_active' => 1, 'user_id' => auth()->user()->id, 'team_id' => $team->id, 'is_private' => $is_private, 'is_added_home' => $is_added_home]));

            $preview_link = ShareableLink::buildFor($campaign);
            $publish_link = ShareableLink::buildFor($campaign);
            $preview_link->setActive();
            if ($is_start == 1) {
                $publish_link->setActive();
            }
            if (!empty($is_private)) {
                if (!empty($request->private_password)) {
                    $publish_link->setPassword($request->private_password);
                }
            }
            if (!empty($is_expired)) {
                if (!empty($request->expired_date)) {
                    $publish_link->setExpirationDate(Carbon::parse($request->expired_date));
                }
            }
            //$preview_link->setPassword('muthu');
            // $campaign->update(array_merge($request->all(),array('preview_link'=> $preview_link->build()->uuid, 'public_link'=> $publish_link->build()->uuid)));
            $campaign->preview_link = $preview_link->build()->uuid;
            $campaign->public_link = $publish_link->build()->uuid;
            $campaign->save();
            if (!empty($subscription)) {
                $user->subscription($subscribed_package)->recordFeatureUsage($catalog_type, 1);
                $this->getSubscriptionFeatureUsage(new Request(["featurename" => "products"]));
            }
            return $campaign->id;
        } else {
            $alert = "limit_reached";
            return $this->alertUsageLimitReached($alert, 'json');
            // return redirect()->back()->with('alert_message','Password change successfully.');
        }
    }

    public function store(StoreCampaignRequest $request)
    {
        $campaign = Campaign::create($request->all());

        return redirect()->route('admin.campaigns.index');
    }

    public function edit(Campaign $campaign)
    {
        abort_if(Gate::denies('campaign_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $campaign->load('team');
        // dd($campaign->public_shareable);
        $userSettings = UserInfo::where("user_id", $campaign->user->id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);
        // $all_catalogs = Campaign::all();
        $published_catalogs = Campaign::where("is_start", 1)->count();
        $catalogs_publishable = "true";
        $product_enable = "true";
        $remaining_products = "false";
        $downgraded = false;
        // $published_catalogs = 0;
        $enabled_products = 0;
        $allowed_catalogs = "true";
        $user = Auth::user();
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')) {
            $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
        } else {
            $subscription = PlanSubscription::where('user_id', $user->id)->first();
        }
        if (!empty($subscription)) {
            $allowed_catalogs = $user->subscription($subscription->slug)->getFeatureValue('catalogs');
            if ($published_catalogs >= $allowed_catalogs) {
                $catalogs_publishable = "false";
            }
        }
        switch ($campaign->type) {
            case 2:
                $categories = ProductCategory::rightJoin('product_product_category', function ($join) {
                    $join->on('product_categories.id', '=', 'product_product_category.product_category_id');
                })->groupBy('product_categories.id')->orderBy('product_categories.id', 'desc')->get();

                if (!empty($subscription)) {
                    $this->getSubscriptionFeatureUsage(new Request(["featurename" => "products"]));
                    $all_products = Product::all();
                    foreach ($all_products as $product) {
                        if ($product->downgradeable == 1) {
                            $downgraded = true;
                        } else {
                            $enabled_products++;
                        }
                    }
                    if ($downgraded == true) {
                        $allowed_products = $user->subscription($subscription->slug)->getFeatureValue('products');
                        if ($enabled_products >= $allowed_products) {
                            $product_enable = "false";
                        }
                        $remaining_products = $allowed_products - $enabled_products;
                    }
                }
                $isvideo = true;
                return view('templates.admin.video_catalog', compact('userSettings', 'user_meta_data', 'campaign', 'categories', 'catalogs_publishable', 'product_enable', 'allowed_catalogs', 'remaining_products', 'isvideo'));
                break;
            case 3:
                $categories = ProductCategory::rightJoin('product_product_category', function ($join) {
                    $join->on('product_categories.id', '=', 'product_product_category.product_category_id');
                })->groupBy('product_categories.id')->orderBy('product_categories.id', 'desc')->get();

                if (!empty($subscription)) {
                    $this->getSubscriptionFeatureUsage(new Request(["featurename" => "products"]));
                    $all_products = Product::all();
                    foreach ($all_products as $product) {
                        if ($product->downgradeable == 1) {
                            $downgraded = true;
                        } else {
                            $enabled_products++;
                        }
                    }
                    if ($downgraded == true) {
                        $allowed_products = $user->subscription($subscription->slug)->getFeatureValue('products');
                        if ($enabled_products >= $allowed_products) {
                            $product_enable = "false";
                        }
                        $remaining_products = $allowed_products - $enabled_products;
                    }
                }
                $isvideo = false;
                return view('templates.admin.catalog', compact('userSettings', 'user_meta_data', 'campaign', 'categories', 'catalogs_publishable', 'product_enable', 'allowed_catalogs', 'remaining_products', 'isvideo'));
                break;
            default:
                break;
        }

        // return view('admin.campaigns.edit', compact('campaign'));
    }


    public function ajaxProductData(Request $request)
    {
        // print_r($request);
        // die;
        $sort = ['id', 'desc'];
        $page = 20;
        $is_search = false;
        $is_filter = false;
        $category_filter = "";
        // $attributes_filter = "";
        $price_filter_low = "";
        $price_filter_high = "";
        $cat_id = array();
        $category = array();
        // $attributes = array();
        $filters = json_decode($request->filter, true);
        if ($request->ajax()) {
            $preview = $request->preview;
            $theme = $request->theme;
            $video_id = $request->video_id;
            $shareableLink = ShareableLink::where('uuid', $request->shareable_link)->first();
            $campaign = $shareableLink->shareable;
            if ($shareableLink->shareable->table == "campaigns") {
                $campaign_id = $shareableLink->shareable->id;
            } else {
                $user_id = $shareableLink->shareable->user_id;
                $campaign_id = Campaign::where('user_id', $user_id)->first()->id;
            }
            // $campaign_id = $campaign->id;
            $campaign = Campaign::where('id', $campaign_id)->first();
            // dd($shareableLink,$campaign_id,$campaign);
            // $video_data = Entitycard::with('product')->where("campaign_id", $campaign_id)->where('video_id',$video_id)->whereNull('product_id')->get();
            $userSettings = UserInfo::where("user_id", $campaign->user_id)->first();
            $meta_data = json_decode($userSettings->meta_data, true);
            if ($preview == 1) {
                $campaign->is_start = false;
            }
            if (isset($request->s) && !empty($request->s)) {
                $sort = explode(',', $request->s);
            }
            if (isset($request->q) && !empty($request->q)) {
                $q = $request->q;
                $is_search = true;
            }
            if (isset($request->filter) && !empty($filters)) {
                $is_filter = true;
                foreach ($filters as $card) {
                    if (in_array("category", array_values($card)) && $card['type'] == "category") {
                        $category_filter = $card['value'];
                        if ($category_filter == "all") {
                            $cat_id = "";
                        } else {
                            $cat = explode(",", $category_filter);
                            $category = ProductCategory::whereIn('name', $cat)->get('id');
                            foreach ($category as $catg) {
                                array_push($cat_id, $catg->id);
                            }
                        }
                    } else {
                        $category_filter = "all";
                    }
                    if ($card['type'] == "price") {
                        $price_range = explode(",", $card['value']);
                        $price_filter_low = $price_range[0];
                        if (count($price_range) == 2) {
                            $price_filter_high = $price_range[1];
                        }
                    }
                    // if (in_array("attributes", array_values($card)) && $card['type'] == "attributes") {
                    //     $filter_attributes = $card['value'];
                    //     if ($filter_attributes == "all") {
                    //         // $attributes = "";
                    //     } else {
                    //         foreach ($filter_attributes as $attribute){
                    //             array_push($attributes, [$attribute["attribute_id"] => $attribute["attribute_value"]]);
                    //         }
                    //     }
                    // } else {
                    //     $attributes_filter = "all";
                    // }
                }
            }
            if (($is_search == true) && ($is_filter == false)) {
                $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->where('video_id', $video_id)->whereHas('product', function ($query) use ($q) {
                    $query->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                })->whereHas('product', function ($query2) {
                    $query2->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy($sort[0], $sort[1])->paginate($page);
            } else if (($is_search == false) && ($is_filter == true)) {
                if (($price_filter_low != "all") && ($category_filter != "all")) {
                    if ($price_filter_high == "") {
                        $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->where('video_id', $video_id)->whereHas('product', function ($query) use ($price_filter_low, $cat_id) {
                            $query->where('products.price', '>=', $price_filter_low)->whereIn('catagory_id', $cat_id);
                        })->whereHas('product', function ($query) {
                            $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    } else {
                        $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->where('video_id', $video_id)->whereHas('product', function ($query) use ($price_filter_low, $price_filter_high, $cat_id) {
                            $query->where('products.price', '>=', $price_filter_low)->where('products.price', '<=', $price_filter_high)->whereIn('catagory_id', $cat_id);
                        })->whereHas('product', function ($query) {
                            $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    }
                } else if (($price_filter_low != "all") && ($category_filter == "all")) {
                    if ($price_filter_high == "") {
                        $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->where('video_id', $video_id)->whereHas('product', function ($query) use ($price_filter_low, $price_filter_high) {
                            $query->where('products.price', '>=', $price_filter_low);
                        })->whereHas('product', function ($query) {
                            $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    } else {
                        $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->where('video_id', $video_id)->whereHas('product', function ($query) use ($price_filter_low, $price_filter_high) {
                            $query->where('products.price', '>=', $price_filter_low)->where('products.price', '<=', $price_filter_high);
                        })->whereHas('product', function ($query) {
                            $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    }
                } else if (($price_filter_low == "all") && ($category_filter != "all")) {
                    $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->where('video_id', $video_id)->whereHas('product', function ($query) use ($cat_id) {
                        $query->whereIn('catagory_id', $cat_id);
                    })->whereHas('product', function ($query) {
                        $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                    })->orderBy($sort[0], $sort[1])->paginate($page);
                } else {
                    $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->where('video_id', $video_id)->whereHas('product', function ($query) {
                        return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                    })->orderBy($sort[0], $sort[1])->paginate($page);
                }
            } else if (($is_search == true) && ($is_filter == true)) {
                if (($price_filter_low != "all") && ($category_filter != "all")) {
                    if ($price_filter_high == "") {
                        // dd("Searching","Filtering","Both Search and Filter, Both Filters only min ");
                        $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->where('video_id', $video_id)->whereHas('product', function ($query) use ($price_filter_low, $cat_id, $q) {
                            $query->where('products.price', '>=', $price_filter_low)->where('catagory_id', $cat_id)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                        })->whereHas('product', function ($query) {
                            $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    } else {
                        // dd("Searching","Filtering","Both Search and Filter, Both Filters ");
                        $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->where('video_id', $video_id)->whereHas('product', function ($query) use ($price_filter_low, $price_filter_high, $cat_id, $q) {
                            $query->where('products.price', '>=', $price_filter_low)->where('products.price', '<=', $price_filter_high)->where('catagory_id', $cat_id)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                        })->whereHas('product', function ($query) {
                            $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    }
                } else if (($price_filter_low != "all") && ($category_filter == "all")) {
                    if ($price_filter_high == "") {
                        // dd("Searching","Filtering","Both Search and Filter, Only price Filter only min ");
                        $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->where('video_id', $video_id)->whereHas('product', function ($query) use ($price_filter_low, $price_filter_high, $q) {
                            $query->where('products.price', '>=', $price_filter_low)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                        })->whereHas('product', function ($query) {
                            $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    } else {
                        // dd("Searching","Filtering","Both Search and Filter, Only price Filter ");
                        $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->where('video_id', $video_id)->whereHas('product', function ($query) use ($price_filter_low, $price_filter_high, $q) {
                            $query->where('products.price', '>=', $price_filter_low)->where('products.price', '<=', $price_filter_high)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                        })->whereHas('product', function ($query) {
                            $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    }
                } else if (($price_filter_low == "all") && ($category_filter != "all")) {
                    // dd("Searching","Filtering","Both Search and Filter, Only category Filter ");
                    $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->where('video_id', $video_id)->whereHas('product', function ($query) use ($cat_id, $q) {
                        $query->where('catagory_id', 'LIKE', $cat_id)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                    })->whereHas('product', function ($query) {
                        $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                    })->orderBy($sort[0], $sort[1])->paginate($page);
                } else {
                    // dd("Searching","Filtering","Both Search and Filter, both all filters");
                    $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->where('video_id', $video_id)->whereHas('product', function ($query) use ($q) {
                        $query->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                    })->whereHas('product', function ($query) {
                        $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                    })->orderBy($sort[0], $sort[1])->paginate($page);
                }
            } else {
                $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->where('video_id', $video_id)->whereHas('product', function ($query) {
                    return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy($sort[0], $sort[1])->paginate($page);
            }

            if ($theme == "modern-theme") {
                if ($request->view == "grid") {
                    return view('themes.' . $theme . '.cards.modern_grid_view', compact('data', 'campaign', 'meta_data', 'userSettings', 'is_search', 'is_filter', 'video_id'));
                } else if ($request->view == "list") {
                    return view('themes.' . $theme . '.cards.modern_list_view', compact('data', 'campaign', 'meta_data', 'userSettings', 'is_search', 'is_filter', 'video_id'));
                }
                return view('themes.' . $theme . '.cards.catalog_products', compact('data', 'campaign', 'meta_data', 'userSettings', 'is_search', 'is_filter', 'video_id'));
            } else {
                return view('themes.' . $theme . '.cards.catalog_products', compact('data', 'campaign', 'meta_data', 'userSettings', 'is_search', 'is_filter', 'video_id'));
            }
        }
        return false;
    }

    public function ajaxVideoProductData(Request $request)
    {
        $sort = ['id', 'desc'];
        $page = 20;
        $is_search = false;
        $is_filter = false;
        $category_filter = "";
        // $attributes_filter = "";
        $price_filter_low = "";
        $price_filter_high = "";
        $cat_id = array();
        $category = array();
        // $attributes = array();
        $filters = json_decode($request->filter, true);
        if ($request->ajax()) {
            $preview = $request->preview;
            $theme = $request->theme;
            $video_id = $request->video_id;
            $shareableLink = ShareableLink::where('uuid', $request->shareable_link)->first();
            $campaign = $shareableLink->shareable;
            if ($shareableLink->shareable->table == "campaigns") {
                $campaign_id = $shareableLink->shareable->id;
            } else {
                $user_id = $shareableLink->shareable->user_id;
                $campaign_id = Campaign::where('user_id', $user_id)->first()->id;
            }
            // $campaign_id = $campaign->id;
            $campaign = Campaign::where('id', $campaign_id)->first();
            // dd($shareableLink,$campaign_id,$campaign);
            // $video_data = Entitycard::with('product')->where("campaign_id", $campaign_id)->where('video_id',$video_id)->whereNull('product_id')->get();
            $userSettings = UserInfo::where("user_id", $campaign->user_id)->first();
            $meta_data = json_decode($userSettings->meta_data, true);
            if ($preview == 1) {
                $campaign->is_start = false;
            }


            $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->where('video_id', $video_id)->whereHas('product', function ($query) {
                return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy($sort[0], $sort[1])->paginate($page);


            if ($theme == "modern-theme") {
                if ($request->view == "grid") {
                    return view('themes.' . $theme . '.cards.modern_grid_view', compact('data', 'campaign', 'meta_data', 'userSettings', 'is_search', 'is_filter', 'video_id'));
                } else if ($request->view == "list") {
                    return view('themes.' . $theme . '.cards.modern_list_view', compact('data', 'campaign', 'meta_data', 'userSettings', 'is_search', 'is_filter', 'video_id'));
                }
                return view('themes.' . $theme . '.cards.catalog_videoproductdata', compact('data', 'campaign', 'meta_data', 'userSettings', 'is_search', 'is_filter', 'video_id'));
            } else {
                return view('themes.' . $theme . '.cards.catalog_videoproductdata', compact('data', 'campaign', 'meta_data', 'userSettings', 'is_search', 'is_filter', 'video_id'));
            }
        }
        return false;
    }

    public function ajaxRelatedVideo(Request $request)
    {

        $sort = ['id', 'desc'];
        $page = 20;
        $filters = json_decode($request->filter, true);
        if ($request->ajax()) {
            $preview = $request->preview;
            $theme = $request->theme;
            $video_id = $request->video_id;
            $shareableLink = ShareableLink::where('uuid', $request->shareable_link)->first();
            $campaign = $shareableLink->shareable;
            if ($shareableLink->shareable->table == "campaigns") {
                $campaign_id = $shareableLink->shareable->id;
            } else {
                $user_id = $shareableLink->shareable->user_id;
                $campaign_id = Campaign::where('user_id', $user_id)->first()->id;
            }
            // $campaign_id = $campaign->id;
            $campaign = Campaign::where('id', $campaign_id)->first();
            // dd($shareableLink,$campaign_id,$campaign);
            $video_data = Entitycard::with('product')->where("campaign_id", $campaign_id)->whereNull('product_id')->get();
            $userSettings = UserInfo::where("user_id", $campaign->user_id)->first();
            $meta_data = json_decode($userSettings->meta_data, true);
            $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->whereHas('product', function ($query) {
                return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy($sort[0], $sort[1])->paginate($page);
            return view('themes.' . $theme . '.cards.catalog_videoproducts', compact('campaign', 'data', 'video_data', 'video_id'));
        }
        return false;
    }

    public function ajaxRelatedVideoData(Request $request)
    {

        $sort = ['id', 'desc'];
        $page = 20;
        $filters = json_decode($request->filter, true);
        if ($request->ajax()) {
            $preview = $request->preview;
            $theme = $request->theme;
            $video_id = $request->video_id;
            $shareableLink = ShareableLink::where('uuid', $request->shareable_link)->first();
            $campaign = $shareableLink->shareable;
            if ($shareableLink->shareable->table == "campaigns") {
                $campaign_id = $shareableLink->shareable->id;
            } else {
                $user_id = $shareableLink->shareable->user_id;
                $campaign_id = Campaign::where('user_id', $user_id)->first()->id;
            }
            // $campaign_id = $campaign->id;
            $campaign = Campaign::where('id', $campaign_id)->first();
            // dd($shareableLink,$campaign_id,$campaign);
            $video_data = Entitycard::with('product')->where("campaign_id", $campaign_id)->whereNull('product_id')->get();
            $userSettings = UserInfo::where("user_id", $campaign->user_id)->first();
            $meta_data = json_decode($userSettings->meta_data, true);
            $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->whereHas('product', function ($query) {
                return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy($sort[0], $sort[1])->paginate($page);
            return view('themes.' . $theme . '.cards.catalog_Relatedvideodata', compact('campaign', 'data', 'video_data', 'video_id'));
        }
        return false;
    }

    public function ajaxVideoData(Request $request)
    {
        $sort = ['id', 'desc'];
        $page = 20;
        $is_search = false;
        $is_filter = false;
        $category_filter = "";
        // $attributes_filter = "";
        $price_filter_low = "";
        $price_filter_high = "";
        $cat_id = array();
        $category = array();
        // $attributes = array();
        $filters = json_decode($request->filter, true);
        if ($request->ajax()) {
            $preview = $request->preview;
            $theme = $request->theme;
            $shareableLink = ShareableLink::where('uuid', $request->shareable_link)->first();
            $campaign = $shareableLink->shareable;
            if ($shareableLink->shareable->table == "campaigns") {
                $campaign_id = $shareableLink->shareable->id;
            } else {
                $user_id = $shareableLink->shareable->user_id;
                $campaign_id = Campaign::where('user_id', $user_id)->first()->id;
            }
            // $campaign_id = $campaign->id;
            $video_card = Entitycard::where("campaign_id", $campaign_id)->whereNull('product_id')->whereNotNull('video_id')->get();
            $campaign = Campaign::where('id', $campaign_id)->first();
            // dd($shareableLink,$campaign_id,$campaign);
            $userSettings = UserInfo::where("user_id", $campaign->user_id)->first();
            $meta_data = json_decode($userSettings->meta_data, true);
            if ($preview == 1) {
                $campaign->is_start = false;
            }
            if (isset($request->s) && !empty($request->s)) {
                $sort = explode(',', $request->s);
            }
            if (isset($request->q) && !empty($request->q)) {
                $q = $request->q;
                $is_search = true;
            }
            if (isset($request->filter) && !empty($filters)) {
                $is_filter = true;
                foreach ($filters as $card) {
                    if (in_array("category", array_values($card)) && $card['type'] == "category") {
                        $category_filter = $card['value'];
                        if ($category_filter == "all") {
                            $cat_id = "";
                        } else {
                            $cat = explode(",", $category_filter);
                            $category = ProductCategory::whereIn('name', $cat)->get('id');
                            foreach ($category as $catg) {
                                array_push($cat_id, $catg->id);
                            }
                        }
                    } else {
                        $category_filter = "all";
                    }
                    if ($card['type'] == "price") {
                        $price_range = explode(",", $card['value']);
                        $price_filter_low = $price_range[0];
                        if (count($price_range) == 2) {
                            $price_filter_high = $price_range[1];
                        }
                    }
                    // if (in_array("attributes", array_values($card)) && $card['type'] == "attributes") {
                    //     $filter_attributes = $card['value'];
                    //     if ($filter_attributes == "all") {
                    //         // $attributes = "";
                    //     } else {
                    //         foreach ($filter_attributes as $attribute){
                    //             array_push($attributes, [$attribute["attribute_id"] => $attribute["attribute_value"]]);
                    //         }
                    //     }
                    // } else {
                    //     $attributes_filter = "all";
                    // }
                }
            }
            if (($is_search == true) && ($is_filter == false)) {
                $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($q) {
                    $query->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                })->whereHas('product', function ($query2) {
                    $query2->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy($sort[0], $sort[1])->paginate($page);
            } else if (($is_search == false) && ($is_filter == true)) {
                if (($price_filter_low != "all") && ($category_filter != "all")) {
                    if ($price_filter_high == "") {
                        $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($price_filter_low, $cat_id) {
                            $query->where('products.price', '>=', $price_filter_low)->whereIn('catagory_id', $cat_id);
                        })->whereHas('product', function ($query) {
                            $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    } else {
                        $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($price_filter_low, $price_filter_high, $cat_id) {
                            $query->where('products.price', '>=', $price_filter_low)->where('products.price', '<=', $price_filter_high)->whereIn('catagory_id', $cat_id);
                        })->whereHas('product', function ($query) {
                            $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    }
                } else if (($price_filter_low != "all") && ($category_filter == "all")) {
                    if ($price_filter_high == "") {
                        $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($price_filter_low, $price_filter_high) {
                            $query->where('products.price', '>=', $price_filter_low);
                        })->whereHas('product', function ($query) {
                            $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    } else {
                        $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($price_filter_low, $price_filter_high) {
                            $query->where('products.price', '>=', $price_filter_low)->where('products.price', '<=', $price_filter_high);
                        })->whereHas('product', function ($query) {
                            $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    }
                } else if (($price_filter_low == "all") && ($category_filter != "all")) {
                    $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($cat_id) {
                        $query->whereIn('catagory_id', $cat_id);
                    })->whereHas('product', function ($query) {
                        $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                    })->orderBy($sort[0], $sort[1])->paginate($page);
                } else {
                    $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->whereHas('product', function ($query) {
                        return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                    })->orderBy($sort[0], $sort[1])->paginate($page);
                }
            } else if (($is_search == true) && ($is_filter == true)) {
                if (($price_filter_low != "all") && ($category_filter != "all")) {
                    if ($price_filter_high == "") {
                        // dd("Searching","Filtering","Both Search and Filter, Both Filters only min ");
                        $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($price_filter_low, $cat_id, $q) {
                            $query->where('products.price', '>=', $price_filter_low)->where('catagory_id', $cat_id)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                        })->whereHas('product', function ($query) {
                            $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    } else {
                        // dd("Searching","Filtering","Both Search and Filter, Both Filters ");
                        $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($price_filter_low, $price_filter_high, $cat_id, $q) {
                            $query->where('products.price', '>=', $price_filter_low)->where('products.price', '<=', $price_filter_high)->where('catagory_id', $cat_id)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                        })->whereHas('product', function ($query) {
                            $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    }
                } else if (($price_filter_low != "all") && ($category_filter == "all")) {
                    if ($price_filter_high == "") {
                        // dd("Searching","Filtering","Both Search and Filter, Only price Filter only min ");
                        $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($price_filter_low, $price_filter_high, $q) {
                            $query->where('products.price', '>=', $price_filter_low)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                        })->whereHas('product', function ($query) {
                            $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    } else {
                        // dd("Searching","Filtering","Both Search and Filter, Only price Filter ");
                        $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($price_filter_low, $price_filter_high, $q) {
                            $query->where('products.price', '>=', $price_filter_low)->where('products.price', '<=', $price_filter_high)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                        })->whereHas('product', function ($query) {
                            $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    }
                } else if (($price_filter_low == "all") && ($category_filter != "all")) {
                    // dd("Searching","Filtering","Both Search and Filter, Only category Filter ");
                    $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($cat_id, $q) {
                        $query->where('catagory_id', 'LIKE', $cat_id)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                    })->whereHas('product', function ($query) {
                        $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                    })->orderBy($sort[0], $sort[1])->paginate($page);
                } else {
                    // dd("Searching","Filtering","Both Search and Filter, both all filters");
                    $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($q) {
                        $query->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                    })->whereHas('product', function ($query) {
                        $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                    })->orderBy($sort[0], $sort[1])->paginate($page);
                }
            } else {
                $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->whereHas('product', function ($query) {
                    return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy($sort[0], $sort[1])->paginate($page);
            }
            if ($theme == "modern-theme") {
                if ($request->view == "grid") {
                    return view('themes.' . $theme . '.cards.modern_grid_view', compact('data', 'campaign', 'meta_data', 'userSettings', 'is_search', 'is_filter'));
                } else if ($request->view == "list") {
                    return view('themes.' . $theme . '.cards.modern_list_view', compact('data', 'campaign', 'meta_data', 'userSettings', 'is_search', 'is_filter'));
                }
                return view('themes.' . $theme . '.cards.catalog_videos', compact('data', 'campaign', 'meta_data', 'userSettings', 'is_search', 'is_filter', 'video_card'));
            } else {
                return view('themes.' . $theme . '.cards.catalog_videos', compact('data', 'campaign', 'meta_data', 'userSettings', 'is_search', 'is_filter', 'video_card'));
            }
        }
        return false;
    }

    public function ajaxCategoryProductData(Request $request)
    {
        $sort = ['id', 'desc'];
        $page = 20;
        $is_search = false;
        $is_filter = false;
        if ($request->ajax()) {
            $category_id = $request->category_id;
            $preview = $request->preview;
            $theme = $request->theme;
            $shareableLink = ShareableLink::where('uuid', $request->shareable_link)->first();
            $campaign = $shareableLink->shareable;
            if ($shareableLink->shareable->table == "campaigns") {
                $campaign_id = $shareableLink->shareable->id;
            } else {
                $user_id = $shareableLink->shareable->user_id;
                $campaign_id = Campaign::where('user_id', $user_id)->first()->id;
            }
            $campaign = Campaign::where('id', $campaign_id)->first();
            $userSettings = UserInfo::where("user_id", $campaign->user_id)->first();
            $meta_data = json_decode($userSettings->meta_data, true);
            if ($preview == 1) {
                $campaign->is_start = false;
            }

            $data = Entitycard::with('product')->where("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($category_id) {
                $query->where('catagory_id', $category_id);
            })->whereHas('product', function ($query) {
                $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy($sort[0], $sort[1])->paginate($page);

            if ($theme == "modern-theme") {
                if ($request->view == "grid") {
                    return view('themes.' . $theme . '.cards.modern_grid_view', compact('data', 'campaign', 'meta_data', 'userSettings', 'is_search', 'is_filter'));
                } else if ($request->view == "list") {
                    return view('themes.' . $theme . '.cards.modern_list_view', compact('data', 'campaign', 'meta_data', 'userSettings', 'is_search', 'is_filter'));
                }
                return view('themes.' . $theme . '.cards.catalog_products', compact('data', 'campaign', 'meta_data', 'userSettings', 'is_search', 'is_filter'));
            } else {
                return view('themes.' . $theme . '.cards.catalog_products', compact('data', 'campaign', 'meta_data', 'userSettings', 'is_search', 'is_filter'));
            }
        }
        return false;
    }

    public function ajaxProductDetails(Request $request)
    {
        if ($request->ajax()) {
            $attrribute_array = array();
            $entitycard_id = $request->card_id;
            $video_id = $request->video_id;
            if ($request->campaign_id == "undefined") {
                $shareableLink = ShareableLink::where('uuid', $request->shareable_link)->first();
                $campaign = $shareableLink->shareable;
                if ($shareableLink->shareable->table == "campaigns") {
                    $campaign_id = $shareableLink->shareable->id;
                } else {
                    $user_id = $shareableLink->shareable->user_id;
                    $campaign_id = Campaign::where('user_id', $user_id)->first()->id;
                }
            } else {
                $campaign_id = $request->campaign_id;
            }
            $campaign = Campaign::where('id', $campaign_id)->first();
            $userSettings = UserInfo::where("user_id", $campaign->user_id)->first();
            $meta_data = json_decode($userSettings->meta_data, true);
            $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');
            $categories = ProductCategory::all();
            $preview = $request->preview;
            if ($preview == 1) {
                $campaign->is_start = false;
            }
            if (empty($video_id)) {
                $EntityCard = Entitycard::where('campaign_id', $campaign->id)->where('id', $entitycard_id)->whereHas('product', function ($query) {
                    return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy('id', 'desc')->first();
                $EntityCard_next = Entitycard::where('campaign_id', $campaign->id)->where('id', '<', $entitycard_id)->whereHas('product', function ($query) {
                    return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy('id', 'desc')->first();
                $EntityCard_previous = Entitycard::where('campaign_id', $campaign->id)->where('id', '>', $entitycard_id)->whereHas('product', function ($query) {
                    return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy('id', 'asc')->first();
            } else {
                $EntityCard = Entitycard::where('campaign_id', $campaign->id)->where('id', $entitycard_id)->where('video_id', $video_id)->whereHas('product', function ($query) {
                    return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy('id', 'desc')->first();
                $EntityCard_next = Entitycard::where('campaign_id', $campaign->id)->where('id', '<', $entitycard_id)->where('video_id', $video_id)->whereHas('product', function ($query) {
                    return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy('id', 'desc')->first();
                $EntityCard_previous = Entitycard::where('campaign_id', $campaign->id)->where('id', '>', $entitycard_id)->where('video_id', $video_id)->whereHas('product', function ($query) {
                    return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy('id', 'asc')->first();
            }
            if (!empty($EntityCard)) {
                $product = $EntityCard->product;
                $values = varchar::where('entity_id', $product->id)->pluck('content', 'attribute_id');
                foreach ($values as $value => $card) {
                    array_push($attrribute_array, $value);
                }
                $attributes = AttributesMetadata::whereIn('attribute_id', $attrribute_array)->get()->groupBy('attribute_group_name');
                $variant = '';
                $variants = '';
                $parentproduct = '';
                $q = '';
                if (!empty($product->has_variants)) {
                    $variant = ProductVariant::where('product_id', $product->id)->where('downgradeable', 0)->get();
                    $product = ProductVariant::where('product_id', $product->id)->where('downgradeable', 0)->first();
                    $parentproduct = Product::where('id', $product->product_id)->first();
                    $variants =  Variant::all();
                    $q = $product->id;
                }
                // print_r($variant);
                // die;
                return view('themes.' . $theme . '.cards.product_details', compact('campaign', 'userSettings', 'meta_data', 'product', 'EntityCard_next', 'EntityCard_previous', 'attributes', 'values', 'video_id', 'variant', 'q', 'variants', 'entitycard_id', 'parentproduct'));
            } else {
                return false;
            }
        }
        return false;
    }

    public function ajaxProductVariantDetails(Request $request)
    {
        if ($request->ajax()) {
            $attrribute_array = array();
            $entitycard_id = $request->card_id;
            $video_id = $request->video_id;
            if ($request->campaign_id == "undefined") {
                $shareableLink = ShareableLink::where('uuid', $request->shareable_link)->first();
                $campaign = $shareableLink->shareable;
                if ($shareableLink->shareable->table == "campaigns") {
                    $campaign_id = $shareableLink->shareable->id;
                } else {
                    $user_id = $shareableLink->shareable->user_id;
                    $campaign_id = Campaign::where('user_id', $user_id)->first()->id;
                }
            } else {
                $campaign_id = $request->campaign_id;
            }
            $campaign = Campaign::where('id', $campaign_id)->first();
            $userSettings = UserInfo::where("user_id", $campaign->user_id)->first();
            $meta_data = json_decode($userSettings->meta_data, true);
            $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');
            $categories = ProductCategory::all();
            $preview = $request->preview;
            if ($preview == 1) {
                $campaign->is_start = false;
            }
            $EntityCard = Entitycard::where('campaign_id', $campaign->id)->where('id', $entitycard_id)->whereHas('product', function ($query) {
                return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy('id', 'desc')->first();
            $EntityCard_next = Entitycard::where('campaign_id', $campaign->id)->where('id', '<', $entitycard_id)->whereHas('product', function ($query) {
                return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy('id', 'desc')->first();
            $EntityCard_previous = Entitycard::where('campaign_id', $campaign->id)->where('id', '>', $entitycard_id)->whereHas('product', function ($query) {
                return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy('id', 'asc')->first();
            if (!empty($EntityCard)) {
                $product = $EntityCard->product;
                $values = varchar::where('entity_id', $product->id)->pluck('content', 'attribute_id');
                foreach ($values as $value => $card) {
                    array_push($attrribute_array, $value);
                }
                $attributes = AttributesMetadata::whereIn('attribute_id', $attrribute_array)->get()->groupBy('attribute_group_name');
                $variant = '';
                $variants = '';
                $q = '';
                $parentproduct = '';
                if (!empty($request->productvariantid)) {
                    $variant = ProductVariant::where('product_id', $product->id)->where('downgradeable', 0)->get();
                    $product = ProductVariant::where('id', $request->productvariantid)->where('product_id', $product->id)->where('downgradeable', 0)->first();
                    $parentproduct = Product::where('id', $product->product_id)->first();
                    $variants =  Variant::all();
                    $q = $product->id;
                }
                return view('themes.' . $theme . '.cards.product_details', compact('campaign', 'userSettings', 'meta_data', 'product', 'EntityCard_next', 'EntityCard_previous', 'attributes', 'values', 'video_id', 'variant', 'q', 'variants', 'entitycard_id', 'parentproduct'));
            } else {
                return false;
            }
        }
        return false;
    }

    public function ajaxCombinationalProductVariantDetails(Request $request)
    {
        if ($request->ajax()) {
            $attrribute_array = array();
            $entitycard_id = $request->card_id;
            $video_id = $request->video_id;
            if ($request->campaign_id == "undefined") {
                $shareableLink = ShareableLink::where('uuid', $request->shareable_link)->first();
                $campaign = $shareableLink->shareable;
                if ($shareableLink->shareable->table == "campaigns") {
                    $campaign_id = $shareableLink->shareable->id;
                } else {
                    $user_id = $shareableLink->shareable->user_id;
                    $campaign_id = Campaign::where('user_id', $user_id)->first()->id;
                }
            } else {
                $campaign_id = $request->campaign_id;
            }
            $campaign = Campaign::where('id', $campaign_id)->first();
            $userSettings = UserInfo::where("user_id", $campaign->user_id)->first();
            $meta_data = json_decode($userSettings->meta_data, true);
            $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');
            $categories = ProductCategory::all();
            $preview = $request->preview;
            if ($preview == 1) {
                $campaign->is_start = false;
            }
            $EntityCard = Entitycard::where('campaign_id', $campaign->id)->where('id', $entitycard_id)->whereHas('product', function ($query) {
                return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy('id', 'desc')->first();
            $EntityCard_next = Entitycard::where('campaign_id', $campaign->id)->where('id', '<', $entitycard_id)->whereHas('product', function ($query) {
                return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy('id', 'desc')->first();
            $EntityCard_previous = Entitycard::where('campaign_id', $campaign->id)->where('id', '>', $entitycard_id)->whereHas('product', function ($query) {
                return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy('id', 'asc')->first();
            if (!empty($EntityCard)) {
                $product = $EntityCard->product;
                $values = varchar::where('entity_id', $product->id)->pluck('content', 'attribute_id');
                foreach ($values as $value => $card) {
                    array_push($attrribute_array, $value);
                }
                $attributes = AttributesMetadata::whereIn('attribute_id', $attrribute_array)->get()->groupBy('attribute_group_name');
                $variant = '';
                $variants = '';
                $q = '';
                $parentproduct = '';
                if (!empty($request->value)) {
                    $previousproduct = ProductVariant::find($request->previousproductid);
                    $variantdata = json_decode($previousproduct->variant_data);
                    $var = array();
                    foreach ($variantdata as $key => $val) {
                        if ($val->type == $request->key) {
                            $var[$key]['type'] = $val->type;
                            $var[$key]['value'] = $request->value;
                        } else {
                            $var[$key]['type'] = $val->type;
                            $var[$key]['value'] = $val->value;
                        }
                    }
                    $data = json_encode($var);
                    $variantproduct = ProductVariant::where('product_id', $previousproduct->product_id)->where('variant_data', $data)->where('downgradeable', 0)->first();
                    if (empty($variantproduct)) {
                        $variantproduct = ProductVariant::where('product_id', $previousproduct->product_id)->where('downgradeable', 0)->get();
                        foreach ($variantproduct as $key => $val) {
                            $variantdata = json_decode($val->variant_data);
                            foreach ($variantdata as  $var) {
                                if ($var->value == $request->value) {
                                    break 2;
                                }
                            }
                        }
                        $var = array();
                        foreach ($variantdata as $key => $val) {
                            $var[$key]['type'] = $val->type;
                            $var[$key]['value'] = $val->value;
                        }
                        $data = json_encode($var);
                        $variantproduct = ProductVariant::where('product_id', $previousproduct->product_id)->where('variant_data', $data)->where('downgradeable', 0)->first();
                        $product = $variantproduct;
                        $variant = ProductVariant::where('product_id', $product->product_id)->where('downgradeable', 0)->get();
                        $variants =  Variant::all();
                        $q = $product->id;
                    } else {
                        $product = $variantproduct;
                        $variant = ProductVariant::where('product_id', $product->product_id)->where('downgradeable', 0)->get();
                        $variants =  Variant::all();
                        $q = $product->id;
                    }
                    $parentproduct = Product::where('id', $product->product_id)->first();
                }

                return view('themes.' . $theme . '.cards.product_details', compact('campaign', 'userSettings', 'meta_data', 'product', 'EntityCard_next', 'EntityCard_previous', 'attributes', 'values', 'video_id', 'variant', 'q', 'variants', 'entitycard_id', 'parentproduct'));
            } else {
                return false;
            }
        }
        return false;
    }

    public function ajaxEntityData(Request $request)
    {
        $sort = ['id', 'desc'];
        $page = 20;
        $is_search = false;
        if ($request->ajax()) {
            // $categories = ProductCategory::all()->pluck('name', 'id');
            $campaign = Campaign::where('id', $request->campaign_id)->first();

            if (isset($request->s) && !empty($request->s)) {
                $sort = explode(',', $request->s);
            }

            if (isset($request->q) && !empty($request->q)) {
                $q = $request->q;
                $is_search = true;
                // $data = Entitycard::with('product')->where("campaign_id",$campaign_id)->where('name', 'LIKE', '%' . $q . '%' )->orWhere('sku', 'LIKE', '%' . $q . '%' )
                // ->orderBy($sort[0],$sort[1])->paginate($page);
                $data = Entitycard::with('product')->where("campaign_id", $request->campaign_id)->whereHas('product', function ($query) use ($q) {
                    return $query->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                })->orderBy($sort[0], $sort[1])->paginate($page);
            } else {
                $data = Entitycard::with('product')->where("campaign_id", $request->campaign_id)->orderBy($sort[0], $sort[1])->paginate($page);
            }
            foreach ($data as $key => $value) {
                $data[$key]['views'] = Events::where('name', 'product details')->where('product_id', $value->product_id)->count();
            }
            $userSettings = UserInfo::where("user_id", auth()->user()->id)->first();
            $user_meta_data = json_decode($userSettings->meta_data, true);
            $user = Auth::user();
            if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')) {
                $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
            } else {
                $subscription = PlanSubscription::where('user_id', $user->id)->first();
            }
            $product_enable = "true";
            // $enabled_products = 0;
            $enabled_products = Product::where('downgradeable', '!=', 1)->count();
            if (!empty($subscription)) {
                // foreach ($all_products as $product){
                //     if($product->downgradeable == 1){

                //     }
                //     else{
                //         $enabled_products++;
                //     }
                // }
                $allowed_products = $user->subscription($subscription->slug)->getFeatureValue('products');
                if ($enabled_products >= $allowed_products) {
                    $product_enable = "false";
                }
                $this->getSubscriptionFeatureUsage(new Request(["featurename" => "products"]));
            }

            return view('cards.entitycard_grid', compact('data', 'userSettings', 'campaign', 'user_meta_data', 'is_search', 'product_enable'));
        }
        return false;
    }

    public function ajaxVideoEntityData(Request $request)
    {
        $sort = ['id', 'desc'];
        $page = 20;
        $is_search = false;
        if ($request->ajax()) {
            // $categories = ProductCategory::all()->pluck('name', 'id');
            $campaign = Campaign::where('id', $request->campaign_id)->first();

            if (isset($request->s) && !empty($request->s)) {
                $sort = explode(',', $request->s);
            }

            if (isset($request->q) && !empty($request->q)) {
                $q = $request->q;
                $is_search = true;
                // $data = Entitycard::with('product')->where("campaign_id",$campaign_id)->where('name', 'LIKE', '%' . $q . '%' )->orWhere('sku', 'LIKE', '%' . $q . '%' )
                // ->orderBy($sort[0],$sort[1])->paginate($page);
                $data = Entitycard::with('video')->where("campaign_id", $request->campaign_id)->whereNull('product_id')->whereHas('video', function ($query) use ($q) {
                    return $query->where('title', 'LIKE', '%' . $q . '%');
                })->orderBy($sort[0], $sort[1])->paginate($page);
            } else {
                $data = Entitycard::with('video')->where("campaign_id", $request->campaign_id)->whereNull('product_id')->orderBy($sort[0], $sort[1])->paginate($page);
            }
            foreach ($data as $key => $value) {
                $data[$key]['views'] = Events::where('name', 'product details')->where('product_id', $value->product_id)->count();
            }
            $userSettings = UserInfo::where("user_id", auth()->user()->id)->first();
            $user_meta_data = json_decode($userSettings->meta_data, true);
            $user = Auth::user();
            if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')) {
                $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
            } else {
                $subscription = PlanSubscription::where('user_id', $user->id)->first();
            }
            $product_enable = "true";
            $enabled_products = 0;
            // $enabled_products = Product::where('downgradeable', '!=' , 1)->count();
            if (!empty($subscription)) {
                // foreach ($all_products as $product){
                //     if($product->downgradeable == 1){

                //     }
                //     else{
                //         $enabled_products++;
                //     }
                // }
                $allowed_products = $user->subscription($subscription->slug)->getFeatureValue('products');
                if ($enabled_products >= $allowed_products) {
                    $product_enable = "false";
                }
                $this->getSubscriptionFeatureUsage(new Request(["featurename" => "products"]));
            }

            return view('cards.video_entitycard_grid', compact('data', 'userSettings', 'campaign', 'user_meta_data', 'is_search', 'product_enable'));
        }
        return false;
    }

    public function ajaxVideoProductEntityData(Request $request)
    {
        $video_id = $request->video_id;
        $sort = ['id', 'desc'];
        $page = 20;
        $is_search = false;
        if ($request->ajax()) {
            // $categories = ProductCategory::all()->pluck('name', 'id');
            $campaign = Campaign::where('id', $request->campaign_id)->first();

            if (isset($request->s) && !empty($request->s)) {
                $sort = explode(',', $request->s);
            }

            if (isset($request->q) && !empty($request->q)) {
                $q = $request->q;
                $is_search = true;
                // $data = Entitycard::with('product')->where("campaign_id",$campaign_id)->where('name', 'LIKE', '%' . $q . '%' )->orWhere('sku', 'LIKE', '%' . $q . '%' )
                // ->orderBy($sort[0],$sort[1])->paginate($page);
                $data = Entitycard::with('video')->where("campaign_id", $request->campaign_id)->where('video_id', $video_id)->whereNotNull('product_id')->whereHas('video', function ($query) use ($q) {
                    return $query->where('title', 'LIKE', '%' . $q . '%');
                })->orderBy($sort[0], $sort[1])->paginate($page);
            } else {
                $data = Entitycard::with('video')->where("campaign_id", $request->campaign_id)->where('video_id', $video_id)->whereNotNull('product_id')->orderBy($sort[0], $sort[1])->paginate($page);
            }
            foreach ($data as $key => $value) {
                $data[$key]['views'] = Events::where('name', 'product details')->where('product_id', $value->product_id)->count();
            }
            $userSettings = UserInfo::where("user_id", auth()->user()->id)->first();
            $user_meta_data = json_decode($userSettings->meta_data, true);
            $user = Auth::user();
            if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')) {
                $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
            } else {
                $subscription = PlanSubscription::where('user_id', $user->id)->first();
            }
            $product_enable = "true";
            $enabled_products = 0;
            // $enabled_products = Product::where('downgradeable', '!=' , 1)->count();
            if (!empty($subscription)) {
                // foreach ($all_products as $product){
                //     if($product->downgradeable == 1){

                //     }
                //     else{
                //         $enabled_products++;
                //     }
                // }
                $allowed_products = $user->subscription($subscription->slug)->getFeatureValue('products');
                if ($enabled_products >= $allowed_products) {
                    $product_enable = "false";
                }
                $this->getSubscriptionFeatureUsage(new Request(["featurename" => "products"]));
            }

            return view('cards.video_product_entitycard_grid', compact('data', 'userSettings', 'campaign', 'user_meta_data', 'is_search', 'product_enable', 'video_id'));
        }
        return false;
    }


    public function update(UpdateCampaignRequest $request, Campaign $campaign)
    {
        $campaign->update($request->all());

        return redirect()->route('admin.campaigns.index');
    }

    public function updateCampaign(Request $request)
    {
        Campaign::find($request->pk)->update([$request->name => $request->value]);

        return response()->json(['success' => 'done']);
    }

    public function updateCatalogSettings(Request $request)
    {
        // Campaign::find($request->pk)->update($request->all());
        $campaign = Campaign::where('id', $request->id);
        if (empty($campaign->first()))
            return response()->json([], 403);

        $campaign1 = $campaign->first();
        $shareableLink = ShareableLink::where('uuid', $campaign1->public_link)->first();
        if (count($campaign->first()->campaignEntitycards) > 0 || $campaign->first()->is_start == 1) {
            $campaign->update(array('is_start' => $request->is_start));
            $shareableLink->update(array('active' => $request->is_start));

            if (!empty($campaign1->downgradeable) || ($campaign1->downgradeable == "0")) {
                if (!empty($request->downgradeable) || $request->downgradeable == 0 || $request->downgradeable == 1) {
                    $campaign->update(array('downgradeable' => $request->downgradeable));
                }
            }

            if (!empty($request->is_private) || $request->is_private == 0) {
                $campaign->update(array('is_private' => $request->is_private));
                if ($request->is_private == 0) {
                    $shareableLink->update(array('password' => null));
                }
                if (!empty($request->private_password)) {
                    $shareableLink->update(array('password' => bcrypt($request->private_password)));
                }
            } else {
                $shareableLink->update(array('password' => null));
            }

            if (!empty($request->expired_date)) {
                $shareableLink->update(array('expires_at' => $request->expired_date));
            } else {
                $shareableLink->update(array('expires_at' => null));
            }

            if (!empty($request->is_added_home) || $request->is_added_home == 0) {
                $campaign->update(array('is_added_home' => $request->is_added_home));
            }
        }
        return response()->json(['success' => 'done']);
    }

    public function publish(Request $request)
    {
        $campaign = Campaign::where('id', $request->id);
        if (count($campaign->first()->campaignEntitycards) > 0 || $campaign->first()->is_start == 1) {
            $campaign->update(array('id' => $request->id, 'is_start' => $request->status));
            $campaign = $campaign->first();
            $shareableLink = ShareableLink::where('uuid', $campaign->public_link)->first();
            $shareableLink->update(array('active' => $request->status));
            return redirect()->route('admin.campaigns.index');
        } else {
            return response()->json(['msg' => "Doesn't have any product in the Catalog", 'status' => "failure"], 403);
        }
    }

    public function show(Campaign $campaign)
    {
        abort_if(Gate::denies('campaign_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $campaign->load('team', 'campaginEntitycards');

        return view('admin.campaigns.show', compact('campaign'));
    }

    public function destroy(Campaign $campaign)
    {
        abort_if(Gate::denies('campaign_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $campaign->delete();
        $user = Auth::user();
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')) {
            $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
        } else {
            $subscription = PlanSubscription::where('user_id', $user->id)->first();
        }
        $campaigns_usage = PlanSubscriptionUsage::where(['subscription_id' => $subscription->id, 'feature_id' => 1])->first();
        $campaigns = Campaign::where('team_id', $user->team_id)->get();
        $available_campaigns = $campaigns->count();
        $campaigns_used = $campaigns_usage->used;
        if ($available_campaigns < $campaigns_used) {
            if (!empty($campaigns_usage)) {
                $campaign_count = $campaigns_usage->used;
                $campaigns_usage->used = $campaign_count - 1;
                $campaigns_usage->save();
            }
        }

        return back();
    }

    public function massDestroy(MassDestroyCampaignRequest $request)
    {
        Campaign::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function previewQrCode(Request $request)
    {
        // dd($request->qrstring);
        // Create a basic QR code
        $userSettings = UserInfo::where('user_id', auth()->user()->id)->first();
        // $meta_data = json_decode($campaign->meta_data, true);
        $shareableLink = ShareableLink::where('uuid', $request->qrstring)->first();
        if (!empty($shareableLink)) {
            // $str = url('/').''.config('shareable-model.base_url').'/'.$request->qrstring;
            if (empty($userSettings->subdomain)) {
                $str = url('/') . '/p/' . $request->qrstring;
                $qrCode = QrCode::format('png')->size(500)->generate($str);
            } else {
                $str = $userSettings->subdomain . "." . env('SITE_URL') . "/" . $request->qrstring;
                $qrCode = QrCode::format('png')->size(500)->generate($str);
            }
        } else {
            $qrCode = QrCode::format('png')->size(500)->generate("https://" . $request->qrstring);
        }

        return $qrCode;
    }

    static public function publicLink($linkstring, $type, $id = "", $qrCode = false)
    {
        $shareableLink = ShareableLink::where('uuid', $linkstring)->first();
        if (isset(auth()->user()->id)) {
            $userSettings = UserInfo::where('user_id', auth()->user()->id)->first();
        } else {
            $campaign = $shareableLink->shareable;

            if ($campaign->table == "user_infos")
                $userSettings = $shareableLink->shareable;
            if ($campaign->table == "campaigns") {
                $campaign_id = $campaign->id;
                $campaign = Campaign::where("id", $campaign_id)->first();
                $userSettings = UserInfo::where("user_id", $campaign->user->id)->first();
            }
            // dd($campaign,$campaign_st,$userSettings);
        }

        $publiclink = "";
        if (!empty($shareableLink)) {

            if (empty($userSettings->customdomain)) {
                if (empty($userSettings->subdomain)) {
                    $publiclink = url('/');
                } else {
                    $publiclink = "https://" . $userSettings->subdomain . "." . env('SITE_URL');
                }
            } else {
                $publiclink = "https://" . $userSettings->customdomain;
            }
        }

        if ($type == "catalog")
            $publiclink .= '/p/' . $linkstring;
        elseif ($type == "video_catalog")
            $publiclink .= '/sv/' . $linkstring;
        else if ($type == "stories")
            $publiclink .= '/stories/' . $linkstring;
        else if ($type == "product")
            $publiclink .= '/d/' . $id . '/' . $linkstring;
        else if ($type == "videoproduct")
            $publiclink .= '/vd/' . $linkstring . '?video-id=' . $id;
        else if ($type == "store" && empty($userSettings->subdomain))
            $publiclink .= '/s/' . $linkstring;
        else if ($type == "tryon")
            $publiclink .= '/t/' . $linkstring . '?sku=' . $id;
        else if ($type == "viewar")
            $publiclink .= '/v/' . $linkstring . '?sku=' . $id;
        else if ($type == "viewvr")
            $publiclink .= '/vr/' . $linkstring;


        if ($qrCode) {
            return "data:image/png;base64," . base64_encode(QrCode::format('png')->size(500)->generate($publiclink));
        }
        return $publiclink;
    }

    public function addEntity(Request $request)
    {
        $campaign = Campaign::where('id', request('id'))->first();
        if (!empty($campaign)) {
            if ($campaign->campaign_type == 0)
                return $this->TryAlong($campaign);
            elseif ($campaign->campaign_type == 1)
                return $this->InstaTry($campaign);
        }
    }

    public function sharePreview(ShareableLink $link)
    {
        $campaign = $link->shareable;
        $campaign->is_start = false;
        return $this->preview($campaign);
    }

    public function sharePreviewTryOn(ShareableLink $link)
    {
        $campaign = $link->shareable;
        return $this->previewAR($campaign);
    }
    public function sharePreviewViewAR(ShareableLink $link)
    {
        $campaign = $link->shareable;
        return $this->previewVAR($campaign);
    }
    public function sharePreviewViewVR(ShareableLink $link)
    {
        $campaign = $link->shareable;
        return $this->previewVVR($campaign);
    }
    public function sharePreviewCatalogDetails(Request $link)
    {
        $product_id = $link->product_id;
        $campaign = $link->shareable_link->shareable;
        $EntityCard = Entitycard::where('campaign_id', $campaign->id)->where('product_id', $product_id)->orderBy('id', 'desc')->first();
        if (!empty($EntityCard)) {
            $product = $EntityCard->product;
            return $this->catalogProductDetails($campaign, $product);
        }
    }

    public function sharePublic(Request $link)
    {
        // dd("storeFront");
        $campaign = $this->publicSharing($link);
        $video_id = "";
        if ($link->has('video-id')) {
            $video_id = $link->input('video-id');
        }
        // if ($link->subdomain == "www")
        //     return Redirect::to(env('APP_URL', 'localhost'));
        // // if(env('SITE_URL') == $link->domain.".".$link->tld)
        // //     return Redirect::to("login");
        // if (!empty($link->subdomain) && !empty($link->domain) && !empty($link->tld)) {
        //     $cdomain = $link->subdomain . "." . $link->domain . "." . $link->tld;
        //     $data = Campaign::where('customdomain', $cdomain)->first();
        // }
        // if (empty($link->subdomain) && !empty($link->domain) && !empty($link->tld)) {
        //     $cdomain = $link->domain . "." . $link->tld;
        //     $data = Campaign::where('customdomain', $cdomain)->first();
        // }
        // if (empty($data))
        //     if (!empty($link->subdomain))
        //         $data = Campaign::where('subdomain', $link->subdomain)->first();
        // $campaign = "";
        // // dd($data);
        // if (!empty($data)) {
        //     if ($data->is_start == 1) {
        //         $shareableLink = ShareableLink::where('uuid', $data->public_link)->first();
        //         if (!empty($shareableLink)) {
        //             $campaign = $shareableLink->shareable;
        //             return $this->preview($campaign);
        //         }
        //     }
        // }

        // if (!empty($link->shareable_link->shareable) && empty($link->subdomain)) {
        //     $campaign = Campaign::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();
        //     // $campaign = $link->shareable_link->shareable;
        // } else if (!empty($link->shareable_link->shareable) && !empty($link->subdomain)) {
        //     $campaign = Campaign::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();
        //     $userSettings = UserInfo::where("user_id", $campaign->user_id)->where("subdomain", $link->subdomain)->where('is_start', 1)->first();
        //     if (empty($userSettings))
        //         return view('errors.404');
        //     // $campaign = $link->shareable_link->shareable;
        // }

        if (!empty($campaign)) {
            return $this->preview($campaign, $video_id);
        }

        /* company level subdomain end */
        return view('errors.404');
    }
    public function sharePublicVideo(Request $link)
    {
        $campaign = $this->publicSharing($link);
        if (!empty($campaign)) {
            return $this->previewVideo($campaign);
        }

        return view('errors.404');
    }

    public function shareShowVideo(Request $link)
    {
        $campaign = $this->publicSharing($link);
        $video_id = "";
        if ($link->has('video-id')) {
            $video_id = $link->input('video-id');
        }
        if (!empty($campaign)) {
            return $this->previewShowVideo($campaign, $video_id);
        }

        return view('errors.404');
    }


    public function sharePublicCam(Request $link)
    {
        $campaign = $this->publicSharing($link);

        if (!empty($campaign)) {
            return $this->previewAR($campaign);
        }

        /* company level subdomain end */
        return view('errors.404');
    }


    public function sharePublicCamViewAR(Request $link)
    {

        $campaign = $this->publicSharing($link);
        if (!empty($campaign)) {
            $campaign->sku = $link->sku;
            return $this->previewVAR($campaign);
        }

        /* company level subdomain end */
        return view('errors.404');
    }

    public function sharePublicCamViewVR(Request $link)
    {
        $campaign = $this->publicSharing($link);
        if (!empty($campaign)) {
            $campaign->sku = $link->sku;
            return $this->previewVVR($campaign);
        }

        /* company level subdomain end */
        return view('errors.404');
    }


    public function sharePublicAllProducts(Request $link)
    {
        $campaign = $this->publicSharing($link);
        // if ($link->subdomain == "www")
        //     return Redirect::to(env('APP_URL', 'localhost'));

        // if (!empty($link->subdomain))
        //     $data = Campaign::where('subdomain', $link->subdomain)->first();
        // $campaign = "";

        // // dd($data);
        // if (!empty($data)) {
        //     if ($data->is_start == 1) {
        //         $shareableLink = ShareableLink::where('uuid', $data->public_link)->first();
        //         if (!empty($shareableLink)) {
        //             $campaign = $shareableLink->shareable;
        //             return $this->viewAllProducts($campaign);
        //         }
        //     }
        // }

        // if (!empty($link->shareable_link->shareable) && empty($link->subdomain)) {
        //     $campaign = Campaign::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();
        //     // $campaign = $link->shareable_link->shareable;
        // } else if (!empty($link->shareable_link->shareable) && !empty($link->subdomain)) {
        //     $campaign = Campaign::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();
        //     $userSettings = UserInfo::where("user_id", $campaign->user_id)->where("subdomain", $link->subdomain)->where('is_start', 1)->first();
        //     if (empty($userSettings))
        //         return view('errors.404');
        //     // $campaign = $link->shareable_link->shareable;
        // }

        if (!empty($campaign)) {
            return $this->viewAllProducts($campaign);
        }

        /* company level subdomain end */
        return view('errors.404');
    }


    public function sharePublicCatalogDetails(Request $link)
    {
        // dd("details");
        $campaign = $this->publicSharing($link);
        $product_id = $link->product_id;
        // if ($link->subdomain == "www")
        //     return Redirect::to(env('APP_URL', 'localhost'));

        // if (!empty($link->subdomain))
        //     $data = Campaign::where('subdomain', $link->subdomain)->first();
        // $campaign = "";

        // if (!empty($data)) {
        //     if ($data->is_start == 1) {
        //         $shareableLink = ShareableLink::where('uuid', $data->public_link)->first();
        //         if (!empty($shareableLink)) {
        //             $campaign = $shareableLink->shareable;
        //             $EntityCard = Entitycard::where('campaign_id', $campaign->id)->where('product_id', $product_id)->orderBy('id', 'desc')->first();
        //             if (!empty($EntityCard)) {
        //                 $product = $EntityCard->product;
        //                 return $this->catalogProductDetails($campaign, $product);
        //             }
        //         }
        //     }
        // }

        // if (!empty($link->shareable_link->shareable) && empty($link->subdomain)) {
        //     $campaign = Campaign::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();
        // } else if (!empty($link->shareable_link->shareable) && !empty($link->subdomain)) {
        //     $campaign = Campaign::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();
        //     $userSettings = UserInfo::where("user_id", $campaign->user_id)->where("subdomain", $link->subdomain)->where('is_start', 1)->first();
        //     if (empty($userSettings))
        //         return view('errors.404');
        // }

        if (!empty($campaign)) {
            $EntityCard = Entitycard::where('campaign_id', $campaign->id)->where('product_id', $product_id)->orderBy('id', 'desc')->first();
            if (!empty($EntityCard)) {
                $product = $EntityCard->product;
                return $this->catalogProductDetails($campaign, $product);
            }
        }

        return view('errors.404');
    }

    //sharePublicCheckout
    public function sharePublicCheckout(Request $link)
    {
        // dd("checkout");
        // $product_id = $link->product_id;
        $campaign = $this->publicSharing($link);
        // if ($link->subdomain == "www")
        //     return Redirect::to(env('APP_URL', 'localhost'));

        // if (!empty($link->subdomain))
        //     $data = Campaign::where('subdomain', $link->subdomain)->first();
        // $campaign = "";

        // if (!empty($data)) {
        //     if ($data->is_start == 1) {
        //         $shareableLink = ShareableLink::where('uuid', $data->public_link)->first();
        //         if (!empty($shareableLink)) {
        //             $campaign = $shareableLink->shareable;
        //             return $this->checkout($campaign);
        //         }
        //     }
        // }

        // if (!empty($link->shareable_link->shareable) && empty($link->subdomain)) {
        //     $campaign = Campaign::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();
        //     if (!empty($campaign)) {
        //         return $this->checkout($campaign);
        //     }
        // } else if (!empty($link->shareable_link->shareable)  && !empty($link->subdomain)) {
        //     $campaign = Campaign::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();
        //     $userSettings = UserInfo::where("user_id", $campaign->user_id)->where("subdomain", $link->subdomain)->where('is_start', 1)->first();
        //     if (empty($userSettings))
        //         return view('errors.404');
        if (!empty($campaign)) {
            return $this->checkout($campaign);
        }
        // }

        return view('errors.404');
    }

    public function sharePublicStories(Request $link)
    {
        // dd("stories");
        $campaign = $this->publicSharing($link);
        // if ($link->subdomain == "www")
        //     return Redirect::to(env('APP_URL', 'localhost'));
        // // if(env('SITE_URL') == $link->domain.".".$link->tld)
        // //     return Redirect::to("login");
        // if (!empty($link->subdomain) && !empty($link->domain) && !empty($link->tld)) {
        //     $cdomain = $link->subdomain . "." . $link->domain . "." . $link->tld;
        //     $data = Campaign::where('customdomain', $cdomain)->first();
        // }
        // if (empty($link->subdomain) && !empty($link->domain) && !empty($link->tld)) {
        //     $cdomain = $link->domain . "." . $link->tld;
        //     $data = Campaign::where('customdomain', $cdomain)->first();
        // }
        // if (empty($data))
        //     if (!empty($link->subdomain))
        //         $data = Campaign::where('subdomain', $link->subdomain)->first();
        // $campaign = "";
        // // dd($data);
        // if (!empty($data)) {
        //     if ($data->is_start == 1) {
        //         $shareableLink = ShareableLink::where('uuid', $data->public_link)->first();
        //         if (!empty($shareableLink)) {
        //             $campaign = $shareableLink->shareable;
        //             return $this->stories($campaign);
        //         }
        //     }
        // }

        // if (!empty($link->shareable_link->shareable) && empty($link->subdomain)) {
        //     $campaign = Campaign::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();
        //     // $campaign = $link->shareable_link->shareable;
        // } else if (!empty($link->shareable_link->shareable) && !empty($link->subdomain)) {
        //     $campaign = Campaign::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();
        //     $userSettings = UserInfo::where("user_id", $campaign->user_id)->where("subdomain", $link->subdomain)->where('is_start', 1)->first();
        //     if (empty($userSettings))
        //         return view('errors.404');
        //     // $campaign = $link->shareable_link->shareable;
        // }

        if (!empty($campaign)) {
            return $this->stories($campaign);
        }

        /* company level subdomain end */
        return view('errors.404');
    }

    public function sharePublicProductByCategory(Request $link)
    {
        $category_id = $link->category_id;
        // dd("categories");
        $campaign = $this->publicSharing($link);
        // if ($link->subdomain == "www")
        //     return Redirect::to(env('APP_URL', 'localhost'));

        // if (!empty($link->subdomain))
        //     $data = Campaign::where('subdomain', $link->subdomain)->first();
        // $campaign = "";

        // if (!empty($data)) {
        //     if ($data->is_start == 1) {
        //         $shareableLink = ShareableLink::where('uuid', $data->public_link)->first();
        //         if (!empty($shareableLink)) {
        //             $campaign = $shareableLink->shareable;
        //             $category = ProductCategory::where('id', $category_id)->first();
        //             if (!empty($category)) {
        //                 return $this->productViewByCategory($campaign, $category);
        //             }
        //         }
        //     }
        // }

        // if (!empty($link->shareable_link->shareable) && empty($link->subdomain)) {
        //     $campaign = Campaign::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();
        //     // $campaign = $link->shareable_link->shareable;
        // } else if (!empty($link->shareable_link->shareable) && !empty($link->subdomain)) {
        //     $campaign = Campaign::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();
        //     $userSettings = UserInfo::where("user_id", $campaign->user_id)->where("subdomain", $link->subdomain)->where('is_start', 1)->first();
        //     if (empty($userSettings))
        //         return view('errors.404');
        //     // $campaign = $link->shareable_link->shareable;
        // }


        if (!empty($campaign)) {
            $category = ProductCategory::where('id', $category_id)->first();
            if (!empty($category)) {
                return $this->productViewByCategory($campaign, $category);
            }
        }


        return view('errors.404');
    }

    public function paymentConfirmation(Request $link)
    {
        $campaign = Campaign::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();
        $userSettings = UserInfo::where("user_id", $campaign->user_id)->where('is_start', 1)->first();
        $meta_data = json_decode($userSettings->meta_data, true);
        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

        if (isset($link->order_id)) {
            $order = Orders::find($link->order_id);
            // $order_id = $orders->order_id;
            $transaction_id = $order->transactions()->get()->toArray()[0]['transaction_id'];
            return view('themes.' . $theme . '.pages.paymentConfirmation', compact('campaign', 'userSettings', 'order', 'transaction_id', 'meta_data'));
        } else {
            $enquiry = enquiry::find($link->enquiry_id);
            $enquiry_id = $enquiry->enquiry_id;
            return view('themes.' . $theme . '.pages.paymentConfirmation', compact('campaign', 'userSettings', 'enquiry_id', 'meta_data'));
        }
    }

    public function Payment(Request $link)
    {
        $campaign = Campaign::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();
        $userSettings = UserInfo::where("user_id", $campaign->user_id)->where('is_start', 1)->first();

        $link['payment_method'] = "UPI";
        $link['campaign'] = $campaign;
        $link['userSettings'] = $userSettings;

        return app('App\Http\Controllers\Admin\PaymentController')->payment($link);
    }


    public function ajaxCatalogShareData(Request $request)
    {
        $campaign = Campaign::where('public_link', $request->key)->where('is_start', 1)->first();
        $EntityCard = Entitycard::where('campaign_id', $campaign->id)->whereHas('product', function ($query) {
            return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
        })->orderBy('id', 'desc')->get();
        $twitter_facebook_text = '';
        $whatsapp_text = '';

        $len = 0;

        foreach ($EntityCard as $key => $card) {
            if ($key > 5)
                break;
            $product = $card->product;
            // $temp1 = "\n Product Name : " . $product->name . "\n Price : " . $product->price;
            // $temp2 = "\n Product Name : " . $product->name . "\n Price : " . $product->price;

            if (empty($product->discount)) {
                $temp1 = "\n Product Name : " . $product->name . "\n Price : " . $product->price . "\n--------------------";
                $temp2 = "\n *" . $product->name . "* \n " . $product->price . "\n--------------------";
            } else {
                $temp1 = "\n Product Name : " . $product->name . "\n Price : " . $product->price . "\n Discounted Price : " .  ($product->price - ($product->price * ($product->discount / 100))) . "\n--------------------";
                $temp2 = "\n *" . $product->name . "* \n ~" . $product->price . "~ \n" .  ($product->price - ($product->price * ($product->discount / 100))) . "\n--------------------";
            }
            // $temp = ["name" => $product->name, "price" => $product->price, "discount" => $product->discount * $product->price];

            $twitter_facebook_text = $twitter_facebook_text . $temp1;
            $whatsapp_text = $whatsapp_text . $temp2;
        }

        // dd($twitter_facebook_text, $whatsapp_text);
        return ['data' => [$twitter_facebook_text . "\n For more Details", $whatsapp_text . "\n For more Details"]];
    }

    public function storeAboutUs(Request $request)
    {
        $shareableLink = ShareableLink::where('uuid', $request->shareable_link->uuid)->first();
        $shareable = $shareableLink->shareable;
        $user = $shareable->user;
        $userSettings = UserInfo::where("user_id", $user->id)->first();
        $meta_data = json_decode($userSettings->meta_data, true);
        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

        return view('themes.' . $theme . '.pages.about_us', compact('user', 'meta_data', 'userSettings'));
    }
    public function storeFaq(Request $request){
        $shareableLink = ShareableLink::where('uuid', $request->shareable_link->uuid)->first();
        $shareable = $shareableLink->shareable;
        $user = $shareable->user;
        $userSettings = UserInfo::where("user_id", $user->id)->first();
        $meta_data = json_decode($userSettings->meta_data, true);
        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

        return view('themes.' . $theme . '.pages.faq', compact('user', 'meta_data', 'userSettings'));
    }

    public function storeOrigin(Request $request){
        $shareableLink = ShareableLink::where('uuid', $request->shareable_link->uuid)->first();
        $shareable = $shareableLink->shareable;
        $user = $shareable->user;
        $userSettings = UserInfo::where("user_id", $user->id)->first();
        $meta_data = json_decode($userSettings->meta_data, true);
        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

        return view('themes.' . $theme . '.pages.product_origin', compact('user', 'meta_data', 'userSettings'));
    }

    public function storePrivacyPolicy(Request $request)
    {
        $shareableLink = ShareableLink::where('uuid', $request->shareable_link->uuid)->first();
        $shareable = $shareableLink->shareable;
        $user = $shareable->user;
        $userSettings = UserInfo::where("user_id", $user->id)->first();
        $meta_data = json_decode($userSettings->meta_data, true);
        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

        return view('themes.' . $theme . '.pages.privacy_policy', compact('user', 'meta_data', 'userSettings'));
    }

    public function storeTermsAndConditions(Request $request)
    {
        $shareableLink = ShareableLink::where('uuid', $request->shareable_link->uuid)->first();
        $shareable = $shareableLink->shareable;
        $user = $shareable->user;
        $userSettings = UserInfo::where("user_id", $user->id)->first();
        $meta_data = json_decode($userSettings->meta_data, true);
        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

        return view('themes.' . $theme . '.pages.terms_and_conditions', compact('user', 'meta_data', 'userSettings'));
    }

    public function storeRefundAndReturnPolicy(Request $request)
    {
        $shareableLink = ShareableLink::where('uuid', $request->shareable_link->uuid)->first();
        $shareable = $shareableLink->shareable;
        $user = $shareable->user;
        $userSettings = UserInfo::where("user_id", $user->id)->first();
        $meta_data = json_decode($userSettings->meta_data, true);
        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

        return view('themes.' . $theme . '.pages.refund_and_return_policy', compact('user', 'meta_data', 'userSettings'));
    }

    public function storeContactUs(Request $request)
    {
        $shareableLink = ShareableLink::where('uuid', $request->shareable_link->uuid)->first();
        $shareable = $shareableLink->shareable;
        $user = $shareable->user;
        $userSettings = UserInfo::where("user_id", $user->id)->first();
        $meta_data = json_decode($userSettings->meta_data, true);
        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

        return view('themes.' . $theme . '.pages.contact_us', compact('user', 'meta_data', 'userSettings'));
    }

    public function storeShippingPolicy(Request $request)
    {
        $shareableLink = ShareableLink::where('uuid', $request->shareable_link->uuid)->first();
        $shareable = $shareableLink->shareable;
        $user = $shareable->user;
        $userSettings = UserInfo::where("user_id", $user->id)->first();
        $meta_data = json_decode($userSettings->meta_data, true);
        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

        return view('themes.' . $theme . '.pages.shipping_policy', compact('user', 'meta_data', 'userSettings'));
    }
    public function ajaxPosProductDetails(Request $request)
    {
        if ($request->ajax()) {
            $attrribute_array = array();
            $entitycard_id = $request->card_id;
            $video_id = $request->video_id;
            $campaign_id = $request->campaign_id;
            $campaign = Campaign::where('id', $campaign_id)->first();
            $userSettings = UserInfo::where("user_id", $campaign->user_id)->first();
            $meta_data = json_decode($userSettings->meta_data, true);
            $categories = ProductCategory::all();
            $preview = $request->preview;
            if ($preview == 1) {
                $campaign->is_start = false;
            }
            if (empty($video_id)) {
                $EntityCard = Entitycard::where('campaign_id', $campaign->id)->where('id', $entitycard_id)->whereHas('product', function ($query) {
                    return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy('id', 'desc')->first();
                $EntityCard_next = Entitycard::where('campaign_id', $campaign->id)->where('id', '<', $entitycard_id)->whereHas('product', function ($query) {
                    return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy('id', 'desc')->first();
                $EntityCard_previous = Entitycard::where('campaign_id', $campaign->id)->where('id', '>', $entitycard_id)->whereHas('product', function ($query) {
                    return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy('id', 'asc')->first();
            } else {
                $EntityCard = Entitycard::where('campaign_id', $campaign->id)->where('id', $entitycard_id)->where('video_id', $video_id)->whereHas('product', function ($query) {
                    return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy('id', 'desc')->first();
                $EntityCard_next = Entitycard::where('campaign_id', $campaign->id)->where('id', '<', $entitycard_id)->where('video_id', $video_id)->whereHas('product', function ($query) {
                    return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy('id', 'desc')->first();
                $EntityCard_previous = Entitycard::where('campaign_id', $campaign->id)->where('id', '>', $entitycard_id)->where('video_id', $video_id)->whereHas('product', function ($query) {
                    return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy('id', 'asc')->first();
            }
            if (!empty($EntityCard)) {
                $product = $EntityCard->product;
                $values = varchar::where('entity_id', $product->id)->pluck('content', 'attribute_id');
                foreach ($values as $value => $card) {
                    array_push($attrribute_array, $value);
                }
                $attributes = AttributesMetadata::whereIn('attribute_id', $attrribute_array)->get()->groupBy('attribute_group_name');
                $variant = '';
                $variants = '';
                $parentproduct = '';
                $q = '';
                if (!empty($product->has_variants)) {
                    $variant = ProductVariant::where('product_id', $product->id)->where('downgradeable', 0)->get();
                    $product = ProductVariant::where('product_id', $product->id)->where('downgradeable', 0)->first();
                    $parentproduct = Product::where('id', $product->product_id)->first();
                    $variants =  Variant::all();
                    $q = $product->id;
                }
                return view('.cards.pos_product_detail', compact('campaign', 'userSettings', 'meta_data', 'product', 'EntityCard_next', 'EntityCard_previous', 'attributes', 'values', 'video_id', 'variant', 'q', 'variants', 'entitycard_id', 'parentproduct'));
            } else {
                return false;
            }
        }
        return false;
    }
    public function ajaxPosProductVariantDetails(Request $request)
    {
        if ($request->ajax()) {
            $attrribute_array = array();
            $entitycard_id = $request->card_id;
            $video_id = $request->video_id;
            $campaign_id = $request->campaign_id;
            $campaign = Campaign::where('id', $campaign_id)->first();
            $userSettings = UserInfo::where("user_id", $campaign->user_id)->first();
            $meta_data = json_decode($userSettings->meta_data, true);
            $categories = ProductCategory::all();
            $preview = $request->preview;
            if ($preview == 1) {
                $campaign->is_start = false;
            }
            $EntityCard = Entitycard::where('campaign_id', $campaign->id)->where('id', $entitycard_id)->whereHas('product', function ($query) {
                return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy('id', 'desc')->first();
            $EntityCard_next = Entitycard::where('campaign_id', $campaign->id)->where('id', '<', $entitycard_id)->whereHas('product', function ($query) {
                return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy('id', 'desc')->first();
            $EntityCard_previous = Entitycard::where('campaign_id', $campaign->id)->where('id', '>', $entitycard_id)->whereHas('product', function ($query) {
                return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy('id', 'asc')->first();
            if (!empty($EntityCard)) {
                $product = $EntityCard->product;
                $values = varchar::where('entity_id', $product->id)->pluck('content', 'attribute_id');
                foreach ($values as $value => $card) {
                    array_push($attrribute_array, $value);
                }
                $attributes = AttributesMetadata::whereIn('attribute_id', $attrribute_array)->get()->groupBy('attribute_group_name');
                $variant = '';
                $variants = '';
                $q = '';
                $parentproduct = '';
                if (!empty($request->productvariantid)) {
                    $variant = ProductVariant::where('product_id', $product->id)->where('downgradeable', 0)->get();
                    $product = ProductVariant::where('id', $request->productvariantid)->where('product_id', $product->id)->where('downgradeable', 0)->first();
                    $parentproduct = Product::where('id', $product->product_id)->first();
                    $variants =  Variant::all();
                    $q = $product->id;
                }
                return view('.cards.pos_product_detail', compact('campaign', 'userSettings', 'meta_data', 'product', 'EntityCard_next', 'EntityCard_previous', 'attributes', 'values', 'video_id', 'variant', 'q', 'variants', 'entitycard_id', 'parentproduct'));
            } else {
                return false;
            }
        }
        return false;
    }

    public function ajaxPosCombinationalProductVariantDetails(Request $request)
    {
        if ($request->ajax()) {
            $attrribute_array = array();
            $entitycard_id = $request->card_id;
            $video_id = $request->video_id;
            $campaign_id = $request->campaign_id;
            $campaign = Campaign::where('id', $campaign_id)->first();
            $userSettings = UserInfo::where("user_id", $campaign->user_id)->first();
            $meta_data = json_decode($userSettings->meta_data, true);
            $categories = ProductCategory::all();
            $preview = $request->preview;
            if ($preview == 1) {
                $campaign->is_start = false;
            }
            $EntityCard = Entitycard::where('campaign_id', $campaign->id)->where('id', $entitycard_id)->whereHas('product', function ($query) {
                return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy('id', 'desc')->first();
            $EntityCard_next = Entitycard::where('campaign_id', $campaign->id)->where('id', '<', $entitycard_id)->whereHas('product', function ($query) {
                return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy('id', 'desc')->first();
            $EntityCard_previous = Entitycard::where('campaign_id', $campaign->id)->where('id', '>', $entitycard_id)->whereHas('product', function ($query) {
                return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
            })->orderBy('id', 'asc')->first();
            if (!empty($EntityCard)) {
                $product = $EntityCard->product;
                $values = varchar::where('entity_id', $product->id)->pluck('content', 'attribute_id');
                foreach ($values as $value => $card) {
                    array_push($attrribute_array, $value);
                }
                $attributes = AttributesMetadata::whereIn('attribute_id', $attrribute_array)->get()->groupBy('attribute_group_name');
                $variant = '';
                $variants = '';
                $q = '';
                $parentproduct = '';
                if (!empty($request->value)) {
                    $previousproduct = ProductVariant::find($request->previousproductid);
                    $variantdata = json_decode($previousproduct->variant_data);
                    $var = array();
                    foreach ($variantdata as $key => $val) {
                        if ($val->type == $request->key) {
                            $var[$key]['type'] = $val->type;
                            $var[$key]['value'] = $request->value;
                        } else {
                            $var[$key]['type'] = $val->type;
                            $var[$key]['value'] = $val->value;
                        }
                    }
                    $data = json_encode($var);
                    $variantproduct = ProductVariant::where('product_id', $previousproduct->product_id)->where('variant_data', $data)->where('downgradeable', 0)->first();
                    if (empty($variantproduct)) {
                        $variantproduct = ProductVariant::where('product_id', $previousproduct->product_id)->where('downgradeable', 0)->get();
                        foreach ($variantproduct as $key => $val) {
                            $variantdata = json_decode($val->variant_data);
                            foreach ($variantdata as  $var) {
                                if ($var->value == $request->value) {
                                    break 2;
                                }
                            }
                        }
                        $var = array();
                        foreach ($variantdata as $key => $val) {
                            $var[$key]['type'] = $val->type;
                            $var[$key]['value'] = $val->value;
                        }
                        $data = json_encode($var);
                        $variantproduct = ProductVariant::where('product_id', $previousproduct->product_id)->where('variant_data', $data)->where('downgradeable', 0)->first();
                        $product = $variantproduct;
                        $variant = ProductVariant::where('product_id', $product->product_id)->where('downgradeable', 0)->get();
                        $variants =  Variant::all();
                        $q = $product->id;
                    } else {
                        $product = $variantproduct;
                        $variant = ProductVariant::where('product_id', $product->product_id)->where('downgradeable', 0)->get();
                        $variants =  Variant::all();
                        $q = $product->id;
                    }
                    $parentproduct = Product::where('id', $product->product_id)->first();
                }

                return view('.cards.pos_product_detail', compact('campaign', 'userSettings', 'meta_data', 'product', 'EntityCard_next', 'EntityCard_previous', 'attributes', 'values', 'video_id', 'variant', 'q', 'variants', 'entitycard_id', 'parentproduct'));
            } else {
                return false;
            }
        }
        return false;
    }
}
