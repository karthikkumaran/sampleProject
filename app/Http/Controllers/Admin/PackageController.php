<?php

namespace App\Http\Controllers\Admin;

use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Schema;
use App\LaravelSubscriptions\Plan;
use App\LaravelSubscriptions\PlanFeature;
use App\LaravelSubscriptions\PlanSubscription;
use App\LaravelSubscriptions\PlanSubscriptionUsage;
use App\LaravelSubscriptions\PlanSubscriptionFeatures;
use App\LaravelSubscriptions\PlanSubscribedFeatures;
use Carbon\Carbon;
use App\User;
use App\Product;
use App\Campaign;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!empty(auth()->user()) && auth()->user()->getIsAdminAttribute()){
            $packages = Plan::get();
            
            return view('admin.packages.index', compact('packages')); 
        }
        else{
            return view('errors.404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!empty(auth()->user()) && auth()->user()->getIsAdminAttribute()){
            return view('admin.packages.create');
        }
        else{
            return view('errors.404');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!empty(auth()->user()) && auth()->user()->getIsAdminAttribute()){
            // dd($request->all());
            $name = $request->name;
            $description = $request->description;
            $price = $request->price;
            $discount = $request->discount;
            $signup_fee = $request->signup_fee;
            $invoice_period = $request->invoice_period;
            $invoice_interval = $request->invoice_interval;
            $trial_period = $request->trial_period;
            $trial_interval = $request->trial_interval;
            $grace_period = $request->grace_period;
            $grace_interval = $request->grace_interval;
            $sort_order = $request->sort_order;
            $currency = $request->currency;
            
            $plan = Plan::create([
                'name' => $name,
                'description' => $description,
                'price' => $price,
                'signup_fee' => $signup_fee,
                'invoice_period' => $invoice_period,
                'invoice_interval' => $invoice_interval,
                'trial_period' => $trial_period,
                'trial_interval' => $trial_interval,
                'grace_period' => $grace_period ?? 0,
                'grace_interval' => $grace_interval ?? "day",
                'sort_order' => $sort_order,
                'currency' => $currency,
                'discount'=> $discount,
            ]);
        
            $feature_id = $request->feature_id;
            $feature_value = $request->feature_value;
            $feature_sort_order = $request->feature_sort_order;

            $plan_features = [];
            $feature_existing_array = [];
            for ($i = 0; $i < count($feature_id); $i++) {
                if(in_array($feature_id[$i], $feature_existing_array)){
                    continue;
                }
                else{
                    array_push($feature_existing_array, $feature_id[$i]);
                    array_push($plan_features, new PlanFeature([
                        'feature_id' => $feature_id[$i],
                        'value' => $feature_value[$i],
                        'sort_order' => 1
                    ]));
                }
            }
            $plan->features()->saveMany($plan_features);

            return redirect()->route('admin.packages.index');
        }
        else{
            return view('errors.404');
        }
    }

    public function addFeature($id,$plan_id)
    {
        if(!empty(auth()->user()) && auth()->user()->getIsAdminAttribute()){
            $plan_subscription_features = PlanSubscriptionFeatures::all();
            $added_features = [];
            $plan_features = PlanFeature::where('plan_id', $plan_id)->get();
            foreach($plan_features as $feature){
                array_push($added_features,$feature->feature_id);
            }
        
            return view('admin.packages.featuresection', compact('id','plan_subscription_features','added_features'));
        }
        else{
            return view('errors.404');
        }
    }

    public function addPlanFeature($id)
    {
        if(!empty(auth()->user()) && auth()->user()->getIsAdminAttribute()){
            $plan_subscription_features = PlanSubscriptionFeatures::all();
            $added_features = [];

            return view('admin.packages.featuresection', compact('id','plan_subscription_features','added_features'));
        }
        else{
            return view('errors.404');
        }
    }

    public function removeFeature(Request $request, $id)
    {
        if(!empty(auth()->user()) && auth()->user()->getIsAdminAttribute()){
            if(!$request->has('flag') && PlanSubscription::where('plan_id', '=', $request->plan_id)->exists()){
                return response()->json(['status' => "warning"]);
            }
            else {
                PlanFeature::where(['plan_id' => $request->plan_id, 'feature_id' => $id])->forceDelete();            
            
                return response()->json(['status' => "success"]);
            }

            return response()->json(['errors' => trans('global.cantDelete')], 424);
        }
        else{
            return view('errors.404');
        }
    }   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     //
    // }

    // /**
    //  * Show the form for editing the specified resource.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    public function edit($id)
    {
        if(!empty(auth()->user()) && auth()->user()->getIsAdminAttribute()){
            $package = Plan::find($id);
            return view('admin.packages.edit',compact('package')); 
        }
        else{
            return view('errors.404');
        }
    }

    // /**
    //  * Update the specified resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    public function update(Request $request, $id)
    {
        if(!empty(auth()->user()) && auth()->user()->getIsAdminAttribute()){
            $package = Plan::find($id);
            $package->name = $request->name;
            $package->description = $request->description;
            $package->price = $request->price;
            $package->signup_fee = $request->signup_fee;
            $package->invoice_period = $request->invoice_period;
            $package->invoice_interval = $request->invoice_interval;
            $package->trial_period = $request->trial_period;
            $package->trial_interval = $request->trial_interval;
            $package->grace_period = $request->grace_period;
            $package->grace_interval = $request->grace_interval;
            $package->sort_order = $request->sort_order;
            $package->currency = $request->currency;
            $package->discount = $request->discount;

            $package->save();
        
            $features = $package->features;
            $feature_id = $request->feature_id;
            $feature_value = $request->feature_value;
            for ($i = 0; $i < count($feature_id); $i++) {
                $feature = PlanFeature::where(['plan_id' => $package->id,'feature_id' => $feature_id[$i]])->first();
                if($feature){
                    if($feature_value[$i] < $feature->value){
                    
                    }
                    else{
                        $subscribed_features = PlanSubscribedFeatures::where(['plan_id' => $package->id, 'feature_id' => $feature_id[$i]])->get();
                        foreach($subscribed_features as $subscribed_feature){
                            $subscribed_feature->value = $feature_value[$i];
                            $subscribed_feature->save();
                        }
                    }
                    $feature->value = $feature_value[$i];
                    $feature->save();
                }
                else{
                    $plan_subscribed_features = new PlanSubscribedFeatures;
                    $plan_subscribed_users = PlanSubscribedFeatures::where('plan_id',$package->id)->groupBy('user_id')->get();
                
                    $package->features()->saveMany([
                        new PlanFeature([
                            'feature_id' => $feature_id[$i],
                            'value' => $feature_value[$i],
                            'sort_order' => 1
                        ]),
                    ]);
                
                    foreach($plan_subscribed_users as $user){
                        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
                            $subscription = PlanSubscription::where('subscriber_id', $user->user_id)->first();
                        }
                        else{
                            $subscription = PlanSubscription::where('user_id', $user->user_id)->first();
                        }
                        if(!empty($subscription)){
                            $plan_subscribed_features->create([
                                'user_id' => $user->user_id,
                                'subscription_id' => $subscription->id,
                                'plan_id' => $package->id,
                                'feature_id' => $feature_id[$i],
                                'value' => $feature_value[$i],
                                'is_approved' => 1,
                            ]);
                        }
                    }
                }
            }

            return redirect()->route('admin.packages.index');
        }
        else{
            return view('errors.404');
        }
    }

    // /**
    //  * Remove the specified resource from storage.
    //  *
    //  * @param  int  $id
    //  * @return \Illuminate\Http\Response
    //  */
    // public function destroy($id)
    // {
    //     //
    // }
}
