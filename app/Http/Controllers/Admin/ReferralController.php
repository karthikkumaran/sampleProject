<?php

namespace App\Http\Controllers\Admin;
// use Famdirksen\LaravelReferral\Models\ReferralAccount;
// use App\Models\ReferralAccount;
// use App\Models\Referral;/
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\LaravelSubscriptions\PlanSubscription;

class ReferralController extends Controller
{
    public function index()
    {
        // $referralData = new ReferralAccount();
        // dd(auth()->user()->referralAccounts());
        // dd(auth()->user()->getReferralLink(""));
        // dd($referralData->getReferralToken());
        // $url = $referralData->getReferralLink(); 
        // dd($url);       
        
        // $user=User::where('id', auth()->user()->id)->first();


        $user=auth()->user();
        $link = $user->getReferralLink();
        
        $referred = User::where('referred_by',$user->affiliate_id)->get();
        $plan_id = [];
        foreach($referred as $key => $value)
        {
         $subscribe = PlanSubscription::where('subscriber_id',$value->id)->first();
         array_push($plan_id,$subscribe);  
        }
        // dd($plan_id);
        
        // $referralAccount = ReferralAccount::where('object_id',$user->id)->first();
        // if(empty( $referralAccount))
        // {
        //     $refer=$user->makeReferralAccount($user->email);
        //     $link = url('/?r='.$refer->getReferralToken());
        // }
        // else{       
        //     $link = url('/?r='.$referralAccount->getReferralToken());
        // }

        // echo $link;
        // $data = {'link'=>$link}
        // dd($user->token);
        // dd($user->getReferralLink());
        // dd($user->remember_token);
        // dd($user->makeReferralAccount($user->email));        
        // dd($user->referralAccounts()->getReferralLink());
        // dd($referralAccount->token);
        // dd($link);
        return view('admin.referral.index', compact("link","referred","plan_id"));
        // return view('admin.referral.index');
        // return view('admin.refferal.index', ['link' => 'link']);
        // return view('admin.refferal.index')->with(link,$link)

        // return view::make('admin.refferal.index') -> with('link', $link);
    }
}