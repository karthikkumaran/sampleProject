<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Schema;
use App\LaravelSubscriptions\Plan;
use App\LaravelSubscriptions\PlanFeature;
use App\LaravelSubscriptions\PlanSubscription;
use App\LaravelSubscriptions\PlanSubscriptionUsage;
use App\LaravelSubscriptions\PlanSubscriptionFeatures;
use App\LaravelSubscriptions\PlanSubscribedFeatures;

use App\User;
use App\Product;
use App\Campaign;

class PackageAddOnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!empty(auth()->user()) && auth()->user()->getIsAdminAttribute()){
        if ($request->ajax()) {
            $query = PlanSubscription::where('deleted_at', null)->get();
            $table = Datatables::of($query);
            $table->addColumn('user_name', '&nbsp;');
            $table->addColumn('plan', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $editGate      = 'user_edit';
                // $deleteGate    = 'product_attribute_delete';
                $crudRoutePart = 'package_add_ons';

                return view('partials.SubscribedUsersDatatablesActions', compact(
                    'editGate',
                    // 'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('user_name', function ($row) {
                if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
                    $user = User::where('id', $row->subscriber_id)->first();
                }
                else{
                    $user = User::where('id', $row->user_id)->first();
                }
                
                if(!empty($user)){
                    return $user->name ? $user->name : "";
                }
                return "";
            });
            $table->editColumn('plan', function ($row) {
                if(!empty($row->plan)){
                    return $row->plan ? $row->plan->name : "";
                }
                return "";
            });
            
            return $table->make(true);
        }

            return view('admin.package_add_on.index');
        }
        else{
            return view('errors.404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function addFeatureAddOn($id,$user_id)
    {
        if(!empty(auth()->user()) && auth()->user()->getIsAdminAttribute()){
        $plan_subscription_features = PlanSubscriptionFeatures::all();
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $user_id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $user_id)->first();
        }
        $plan_subscribed_features = PlanSubscribedFeatures::where(['user_id' => $user_id,'plan_id' => $subscription->plan_id])->get('feature_id');
        $subscribed_features = [];
        foreach($plan_subscribed_features as $feature){
            array_push($subscribed_features,$feature->feature_id);
        }
        
            return view('admin.package_add_on.feature_add_on_section', compact('id','plan_subscription_features','subscribed_features'));
        }
        else{
            return view('errors.404');
        }
    }

    public function removeAddOnFeature(Request $request, $id){
        if(!empty(auth()->user()) && auth()->user()->getIsAdminAttribute()){
        if(!$request->has('flag') && PlanSubscribedFeatures::where(['user_id' => $request->user_id, 'plan_id' => $request->plan_id,'feature_id' => $id])->exists()){
            return response()->json(['status' => "warning"]);
        }
        else {
            PlanSubscribedFeatures::where(['user_id' => $request->user_id,'plan_id' => $request->plan_id, 'feature_id' => $id])->forceDelete();
            
            return response()->json(['status' => "success"]);
        }

            return response()->json(['errors' => trans('global.cantDelete')], 424);
        }
        else{
            return view('errors.404');
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!empty(auth()->user()) && auth()->user()->getIsAdminAttribute()){
        $user = User::where('id', $id)->first();
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $user->id)->first();
        }
        $package  = Plan::find($subscription->plan_id);
        $subscribed_features = PlanSubscribedFeatures::where(['user_id' => $user->id,'plan_id' => $subscription->plan_id])->get();
            return view('admin.package_add_on.edit', compact('user','subscription','package','subscribed_features')); 
        }
        else{
            return view('errors.404');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user_id)
    {
        if(!empty(auth()->user()) && auth()->user()->getIsAdminAttribute()){
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $user_id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $user_id)->first();
        }
        $user = User::where("id", $user_id)->first();
        $feature_id = $request->feature_id;
        $feature_add_on = $request->feature_add_on;
        $feature_value = $request->feature_value;
        for ($i = 0; $i < count($feature_id); $i++) {
            $subscribed_feature = PlanSubscribedFeatures::where(['user_id' => $user_id,'plan_id' => $subscription->plan_id,'feature_id' => $feature_id[$i]])->first();
            if($subscribed_feature){
                if(!empty($feature_add_on[$i])){
                    $subscribed_feature->add_on_value = $feature_add_on[$i];
                    $subscribed_feature->save();
                }
                if($subscribed_feature->feature_id == 1){
                    $campaigns = Campaign::where('user_id', $user->id)->orderBy('is_start', 'desc')->orderBy('id', 'desc')->get();
                    $available_catalogs = $campaigns->count();
                    if ($available_catalogs > 0) {
                        $campaign_count = 0;
                        $catalogs_usage_count = 0;
                        $campaign_value = $user->subscription($subscription->slug)->getFeatureValue('catalogs');
                        if ($available_catalogs > $campaign_value) {
                            foreach ($campaigns as $campaign) {
                                $campaign_count = $campaign_count + 1;
                                if ($campaign_count > $campaign_value) {
                                    $campaign->is_start = 0;
                                    $campaign->is_added_home = 0;
                                    $campaign->downgradeable = 1;
                                        $campaign->save();
                                } else {
                                    $catalogs_usage_count = $campaign_count;
                                    $campaign->downgradeable = 0;
                                    $campaign->is_start = 1;
                                    $campaign->is_added_home = 1;
                                    $campaign->save();
                                }
                            }
                        } else {
                            $catalogs_usage_count = $available_catalogs;
                        }
                        $catalogs_usage = PlanSubscriptionUsage::where(['subscription_id' => $subscription->id, 'feature_id' => 1])->first();
                        if (!empty($catalogs_usage)) {
                            $catalogs_usage->used = $catalogs_usage_count;
                            $catalogs_usage->save();
                        }
                    }
                }
                else if($subscribed_feature->feature_id == 2){
                    $products = Product::where('team_id', $user->team_id)->get();
                    $available_products = $products->count();
                    if ($available_products > 0) {
                        $product_count = 0;
                        $products_usage_count = 0;
                        $product_value = $user->subscription($subscription->slug)->getFeatureValue('products');
                        if ($available_products > $product_value) {
                            foreach ($products as $product) {
                                $product_count = $product_count + 1;
                                if ($product_count > $product_value) {
                                    $product->downgradeable = 1;
                                    $product->save();
                                } else {
                                    $products_usage_count = $product_count;
                                    $product->downgradeable = 0;
                                    $product->save();
                                }
                            }
                        } else {
                            $products_usage_count = $available_products;
                        }
    
                        $products_usage = PlanSubscriptionUsage::where(['subscription_id' => $subscription->id, 'feature_id' => 2])->first();
                        if (!empty($products_usage)) {
                            $products_usage->used = $products_usage_count;
                            $products_usage->save();
                        }
                    }
                }
            }
            else{
                for ($j = 0; $j < count($feature_value); $j++) {
                    $subscribed_feature = PlanSubscribedFeatures::where(['user_id' => $user_id,'plan_id' => $subscription->plan_id,'feature_id' => $feature_id[$i]])->first();
                    if(empty($subscribed_feature)){
                        $plan_subscribed_features = new PlanSubscribedFeatures;
                        $plan_subscribed_features->create([
                            'user_id' => $user_id,
                            'subscription_id' => $subscription->id,
                            'plan_id' => $subscription->plan_id,
                            'feature_id' => $feature_id[$i],
                            'value' => $feature_value[$j],
                            'is_add_on' => 1,
                            'is_approved' => 1,
                        ]);
                    }
                }
            }
        }
        if($request->grace_period_interval != ""){
            $grace_period = PlanSubscribedFeatures::where(['user_id' => $user_id,'plan_id' => $subscription->plan_id,'feature_id' => 7])->first();
            if(empty($grace_period)){
                $plan_subscribed_features = new PlanSubscribedFeatures;
                $plan_subscribed_features->create([
                    'user_id' => $user_id,
                    'subscription_id' => $subscription->id,
                    'plan_id' => $subscription->plan_id,
                    'feature_id' => 7,
                    'value' => $request->grace_period_interval,
                    'add_on_value' => $request->grace_period_duration,
                    'is_add_on' => 1,
                    'is_approved' => 1,
                ]);
            }
            else{
                $grace_period->value = $request->grace_period_interval;
                $grace_period->add_on_value = $request->grace_period_duration;
                $grace_period->save();
            }
        }
        
            return redirect()->route('admin.users.index');
        }
        else{
            return view('errors.404');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
