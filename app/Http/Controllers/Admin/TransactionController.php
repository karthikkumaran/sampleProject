<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\UserInfo;
use App\Transaction;
use App\ordersCustomer;
use App\Orders;
use Gate;
use Symfony\Component\HttpFoundation\Response;


class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_if(Gate::denies('show_transactions'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $page = 10;
        $transactions = Transaction::orderBy('id', 'desc')->paginate($page);
        $user_id = auth()->user()->id;
        $userSettings = UserInfo::where("user_id", $user_id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);
        return view('templates.admin.transactions.index', compact('user_meta_data','transactions'));
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort_if(Gate::denies('show_transactions'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $transaction=Transaction::find($id);
        $order=Orders::where('transaction_id',$id)->first();
        $customer = ordersCustomer::where("order_id",$order->id)->first();
        $userSettings = UserInfo::where("user_id", auth()->user()->id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);
        return view('templates.admin.transactions.transactions', compact('transaction', 'user_meta_data','customer'));
    }



    public function ajaxData(Request $request)
    {
        abort_if(Gate::denies('show_transactions'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $page = 10;
        $userSettings = UserInfo::where("user_id", auth()->user()->id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);

        if ($request->ajax()) {

            $q = $request->q;
            $products = [];

            if (isset($request->s) && !empty($request->s)) {
                $sort = explode(',', $request->s);
            }
            if (isset($request->q) && !empty($request->q)) {

                $transactions = Transaction::Where('order_id', 'LIKE', $q . '%')
                    ->orwhere('payment_mode', "LIKE", '%' . $q . "%")
                    ->orwhere('payment_method', "LIKE", '%' . $q . "%")
                    ->orwhere('transaction_id', "LIKE", '%' . $q . "%")
                    // ->orwhere('invoice_id', "LIKE", '%' . $q . "%")
                    ->orwhere('amount', "LIKE", '%' . $q . "%")
                    ->orwhere('status', "LIKE", '%' . $q . "%")
                    ->orwhere('created_at', "LIKE", '%' . $q . "%")
                    ->orderBy('id', 'desc')
                    ->paginate($page);

            } else {
                $transactions = Transaction::orderBy('id', 'desc')->paginate($page);
            }
            
            return view("cards.transactions_list", compact('transactions', 'user_meta_data'));
        }
    }

}
