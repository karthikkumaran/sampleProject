<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use Illuminate\Http\Request;
use App\Product;
use App\Variant;
use App\ProductVariant;
use App\ProductCategory;
use App\ProductTag;
use App\AttributesMetadata;
use App\Arobject;
use App\Varchar;
use Auth;

use Spatie\MediaLibrary\Models\Media;
use App\Http\Controllers\Traits\CommonFunctionsTrait;
use Illuminate\Support\Facades\Schema;
use App\LaravelSubscriptions\Plan;
use App\LaravelSubscriptions\PlanFeature;
use App\LaravelSubscriptions\PlanSubscription;
use App\LaravelSubscriptions\PlanSubscriptionUsage;
use App\LaravelSubscriptions\PlanSubscriptionFeatures;
use App\LaravelSubscriptions\PlanSubscribedFeatures;

class ProductVariantsController extends Controller
{
    use MediaUploadingTrait;
    use CsvImportTrait;
    use CommonFunctionsTrait;
    public function index(Request $request, $product_id)
    {
        $product = Product::where("id", $product_id)->first();
        return view('admin.product_variants.index', compact('product'));
    }

    public function addVariant($variant_id)
    {
        $variants = Variant::where("team_id", auth()->user()->team_id)->get();
        return view('admin.product_variants.variants_section', compact('variant_id','variants'));
    }

    public function show($variant_id)
    {
        $productvariants = ProductVariant::where('id',$variant_id)->first();
        return view('admin.product_variants.show', compact('productvariants'));
    }

    public function edit($variant_id)
    {
        $productvariants = ProductVariant::where('id',$variant_id)->first();
        // $categories = ProductCategory::whereNull('product_category_id')
        //     ->with('childrenCategories')
        //     ->get();

        $attributes = AttributesMetadata::get()->groupBy('attribute_group_name');

        $values = varchar::where('entity_id', $productvariants->id)->pluck('content', 'attribute_id');

        // $tags = ProductTag::all()->pluck('name', 'id');
        $productvariants->load('team');
        $product = $productvariants;

        return view('cards.productvariant_edit', compact('product', 'attributes', 'values'));
        // return view('admin.product_variants.edit', compact('productvariants'));
    }

    public function update(request $request)
    {
    //  echo  $request;
    //  die;
        $productvariants = ProductVariant::where('id',$request->id)->first();
        $user = Auth::user();
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $user->id)->first();
        }
        if (!empty($subscription)) {
            $subscribed_package = $subscription->slug;
        }
        $canUseInventory = $user->subscription($subscribed_package)->canUseFeature('inventory');
        if($canUseInventory){
            $productvariants->out_of_stock = $request->input('out_of_stock') ? true : false;
            $stock_qty = $request->input('varstock_qty');
            if ($stock_qty == null) {
                $request->merge(['varstock' => null]);
            }
            else{
            $productvariants->stock = $request->input('varstock');
            }
        }
        else{
            $request->merge(['out_of_stock' => $productvariants->out_of_stock]);
            $request->merge(['varstock' => $productvariants->stock]);
            $request->merge(['varstock_qty' => null]);
        }
        $productvariants->featured = $request->input('featured') ? true : false;
        $productvariants->new = $request->input('new') ? true : false;
        $units=$request->input('units');
        $productvariants->downgradeable = ($request->input('downgradeable') == 1) ? true : false;
        $discount_rate = $request->input('vardiscount_rate');
        $minimum_quantity = $request->input('minimum_quantity');
        $productvariants->description = $request->input('vardescription');
        // $productvariants->quantity = $request->input('quantity');

        // Attribute::typeMap([
        //     'varchar' => Varchar::class,
        // ]);

        // foreach ($request->all() as $key => $value) {
        //     $request->merge([
        //         $key => is_array($value) ? implode(',', $value) : $value
        //     ]);
        // }
        if ($discount_rate == null) {
            $request->merge(['discount' => null]);
        }else{
            $productvariants->discount = $request->input('vardiscount');
        }

        if ($request->has("attribute") && $request->attribute == 1) {
            foreach ($productvariants->attributes() as $attribute) {
                $attr = $attribute->slug;
                if ($request->has($attr)) {
                    $request->merge([
                        $attr => is_array($request->$attr) ? implode(',', $request->$attr) : $request->$attr
                    ]);
                } else {
                    $productvariants->$attr = null;
                    $productvariants->save();
                }
            }
        } else {
            $values = varchar::where('entity_id', $productvariants->id);
            if (!empty($values->get())) {
                $data = $values->pluck('content')->toArray();
                foreach ($request->all() as $key => $value) {
                    if (is_array($value) && $key != "categories" && $key != "photo" && $key != "docs") {
                        $request->request->remove($key);
                    } elseif (in_array($value, $data)) {
                        $request->request->remove($key);
                    }
                }
                $values->delete();
            }
        }

        if ($discount_rate == null) {
            $request->merge(['discount' => null]);
        }
        if($units==null){
            $request->merge(['units'=>null]);
        }

        $productvariants->update($request->all());


        // $productvariants->tags()->sync($request->input('tags', []));

        // if (count($product->photo) > 0) {
        //     foreach ($product->photo as $media) {
        //         if (!in_array($media->file_name, $request->input('photo', []))) {
        //             $media->delete();
        //         }
        //     }
        // }

            $media = $productvariants->photo->pluck('file_name')->toArray();

        foreach ($request->input('photo', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $productvariants->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('photo');
            }
        }

        $media_docs = $productvariants->docs->pluck('file_name')->toArray();
        foreach ($request->input('docs', []) as $file) {
            if (count($media_docs) === 0 || !in_array($file, $media_docs)) {
                $productvariants->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('docs');
            }
        }

        $canUploadAR = $user->subscription($subscribed_package)->canUseFeature('ar_object');
        if($canUploadAR){
            if ($request->input('try_on_object', false)) {
                $arobject = Arobject::Create(array('ar_type' => $request->try_on_type, 'position' => $request->position, 'product_id' => $productvariants->id));

                if ($request->input('try_on_object', false)) {
                    if (!$arobject->object) {
                        $arobject->addMedia(storage_path('tmp/uploads/' . $request->input('try_on_object')))->toMediaCollection('object');
                    }
                    else{
                        if(isset($arobject->object->file_name)){
                            if ( $request->input('try_on_object') !== $arobject->object->file_name) {
                                $arobject->addMedia(storage_path('tmp/uploads/' . $request->input('try_on_object')))->toMediaCollection('object');
                            }
                        }
                    }

                }
            }
        }

        $canUpload3D = $user->subscription($subscribed_package)->canUseFeature('3d_object');
        if($canUpload3D){
            if ($request->input('object_3d', false)) {
                $arobject = Arobject::Create(array('ar_type' => $request->type_3d, 'product_id' => $productvariants->id));

                if ($request->input('object_3d', false)) {
                    if (!$arobject->object_3d) {
                        $arobject->addMedia(storage_path('tmp/uploads/' . $request->input('object_3d')))->toMediaCollection('object_3d');
                    }else{
                        if(isset($arobject->object_3d->file_name)){
                            if( $request->input('object_3d') !== $arobject->object_3d->file_name){
                                $arobject->addMedia(storage_path('tmp/uploads/' . $request->input('object_3d')))->toMediaCollection('object_3d');
                            }
                        }
                    }
                }
            }

            if ($request->input('object_3d_ios', false)) {
                $arobject = Arobject::Create(array('ar_type' => $request->type_3d, 'product_id' => $productvariants->id));

                if ($request->input('object_3d_ios', false)) {
                    if (!$arobject->object_3d_ios) {
                        $arobject->addMedia(storage_path('tmp/uploads/' . $request->input('object_3d_ios')))->toMediaCollection('object_3d_ios');
                    }else{
                        if(isset($arobject->object_3d_ios->file_name)){
                            if( $request->input('object_3d_ios') !== $arobject->object_3d_ios->file_name){
                                $arobject->addMedia(storage_path('tmp/uploads/' . $request->input('object_3d_ios')))->toMediaCollection('object_3d_ios');
                            }
                        }
                    }
                }
            }
        }

        // return redirect()->route('admin.products.index');
        // return redirect()->back();
        return true;

        // if($request->downgradeable != 0){
        //     $downgradeable = 1;
        //     $status = 0;
        // }
        // else{
        //     $status = 1;
        //     $downgradeable = null;
        // }
        // $productvariants->update([
        //     'name' => $request->product_name,
        //     'description' => $request->description,
        //     'sku' => $request->sku,
        //     'price' => $request->price,
        //     'discount'=> $request->discount,
        //     'out_of_stock' => $request->out_of_stock,
        //     'status' => $status,
        //     'downgradeable' => $downgradeable,
        //     'featured' => $request->featured,
        //     'new'=> $request->new,
        //     'units' => $request->units,
        //     'minimum_quantity' => $request->minimum_quantity
        // ]);
        // return true;

    }

    public function delete($variant_id)
    {
        $productvariants = ProductVariant::where('id',$variant_id)->first();

        $productvariants->delete();

        return back();

    }

    public function storeCKEditorImages(Request $request)
    {

        $model         = new ProductVariant();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media', 'public');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }

    public function removeImg(Request $request)
    {
        $mediaId = $request->id;
        // if ($mediaId instanceof Media) {
        //     $mediaId = $mediaId->getKey();
        // }
        // $media = $this->media->find($mediaId);
        // if (! $media) {
        //     throw MediaCannotBeDeleted::doesNotBelongToModel($mediaId, $this);
        // }
        // $media->delete();
        $media = Media::find($request->input('id'));
        // dd($media);
        $model = ProductVariant::find($media->model_id);
        $model->deleteMedia($media->id);
        return response()->json(['success' => 'done']);
        //     Model::whereHas('media', function ($query) use($media_id){
        //         $query->whereId($media_id);
        //    })->first()->deleteMedia($media_id);
    }
}

