<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;
use Razorpay\Api\Api;
use App\UserInfo;
use App\Product;
use Illuminate\Support\Str;

class PaymentResponseController extends Controller
{

    public function user_meta_data($key)
    {
        $public_link = $key;
        $shareableLink = ShareableLink::where('uuid', $public_link)->first();
        $user_id = $shareableLink->shareable->user_id;
        $userSettings = UserInfo::select('meta_data')->where("user_id", $user_id)->first();
        $meta_data = json_decode($userSettings->meta_data, true);
        return $meta_data;
    }

    public function payment(Request $request)
    {
        if ($request->payment_method == "Razorpay") {
            $transaction_details = $this->Razorpay_payment($request);
        }

        if ($request->payment_method == "Paypal") {
            $transaction_details = $this->Paypal_payment($request);
        }

        if ($request->payment_method == "Cash on delivery") {
            $transaction_details = $this->cashOnDelivery_payment($request);
        }

        if ($request->payment_method == "UPI") {
            $transaction_details = $this->upi_payment($request);
        }

        return $transaction_details;
    }

    public function Razorpay_payment(Request $request)
    {

        $meta_data = $this->user_meta_data($request->key);


        $api = new Api($meta_data['payment_settings'][$request->payment_method]['rp-key-id'], $meta_data['payment_settings'][$request->payment_method]['rp-secret-key']);

        try {
            $attributes  = array("razorpay_signature"  => $request->rzp_signature,  "razorpay_payment_id"  => $request->rzp_paymentid,  "razorpay_order_id" => $request->rzp_orderid);
            $order  = $api->utility->verifyPaymentSignature($attributes);
            $flag = true;
        } catch (\Exception $e) {
            $flag = false;
        }

        $payment  = $api->payment->fetch($request->rzp_paymentid);
        $transaction_details = $payment;
        $transaction_details['trans_id'] =  $transaction_details['id'];
        $transaction_details['amount'] = $transaction_details['amount'] / 100;
        $transaction_details['flag'] = $flag;
        return $transaction_details;
    }

    public function Paypal_payment(Request $request)
    {
        dd($request);
    }

    public function cashOnDelivery_payment(Request $request)
    {
        $transaction_details = ['flag' => true, 'method' => '', 'order_id' => 'order_' . Str::random(10), 'trans_id' => 'cash_' . Str::random(10), 'amount' => $request['COD_total']['total_amount'], 'meta_data' => ''];
        return $transaction_details;
    }

    public function upi_payment(Request $request)
    {
        // dd($request->all());
        $transaction_details = ['flag' => true, 'method' => '', 'order_id' => 'order_' . Str::random(10), 'trans_id' => $request['transaction_id'], 'amount' => $request['COD_total']['total_amount'], 'meta_data' => ''];
        return $transaction_details;
    }
}
