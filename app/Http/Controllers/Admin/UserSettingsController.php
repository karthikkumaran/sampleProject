<?php

namespace App\Http\Controllers\Admin;

use App\Campaign;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyCampaignRequest;
use App\Http\Requests\StoreCampaignRequest;
use App\Http\Requests\UpdateCampaignRequest;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

use Illuminate\Support\Facades\Redirect;

use Carbon\Carbon;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;
use QrCode;

use Session;
use URL;

use App\User;
use App\UserInfo;
use App\CompanyInfo;
use App\Product;
use App\ProductCategory;
use App\Entitycard;
use App\Http\Controllers\Traits\AddEntityCardTrait;
use Illuminate\Support\Facades\Schema;
use App\LaravelSubscriptions\Plan;
use App\LaravelSubscriptions\PlanSubscription;
use App\LaravelSubscriptions\PlanSubscriptionUsage;
use App\LaravelSubscriptions\PlanSubscriptionFeatures;
use App\LaravelSubscriptions\PlanSubscribedFeatures;
use App\Http\Controllers\Traits\CommonFunctionsTrait;
use App\Otp_Verification;
use App\Telegram_Bot_Subscriber;
use Seshac\Otp\Otp;
use App\Mail\NewUserDetailsEmail;
use Illuminate\Support\Facades\Mail;

class UserSettingsController extends Controller
{
    use AddEntityCardTrait;
    use MediaUploadingTrait;
    use CommonFunctionsTrait;

    public function index(Request $request)
    {
        $user = auth()->user();
        $user_id = $user->id;
        $campaign = UserInfo::where('user_id', $user_id)->first();
        $meta_data = json_decode($campaign->meta_data, true);
        $path = '../resources/views/themes';
        $dir = new \DirectoryIterator($path);
        $identifier = $user->email . '_telegram';
        $generated_otp = \DB::table('otps')->where('identifier', $identifier)->first();
        if(!empty($generated_otp)){
            $expires = Otp::expiredAt($identifier);
            $expiry_time = $expires->expired_at;
            $current_time = Carbon::now();
            $interval = $current_time->diff($expiry_time);
            if($interval->invert == 1){
                $otp_verification = Otp_Verification::where('otp_reference_id', $generated_otp->id)->forceDelete();
                \DB::table('otps')->where('identifier', $identifier)->delete();
                $telegram_otp = null;
            }
            else{
                $telegram_otp = $generated_otp->token;
            }
        }
        else{
            $telegram_otp = null;
        }
        $bot_accounts = Telegram_Bot_Subscriber::where('user_id', $user_id)->get();

        return view('templates.admin.user_setting', compact('campaign', 'meta_data', 'dir', 'telegram_otp', 'bot_accounts'));
    }

    public function set_domain(Request $request)
    {
            $request->validate([
    
                'whatsapp' =>  'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10',
                    'contactnumber' =>  'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:10',    
                ]);
    
    
        $user = auth()->user();
        $user_info = UserInfo::where('user_id', $user->id);
        $meta_data = json_decode($user_info->first()->meta_data, true);

        $meta_data['storefront']['storename'] = $request->storename;
        $meta_data['settings']['emailid'] = $request->emailid;
        $meta_data['settings']['emailfromname'] = $request->emailfromname;
        $meta_data['settings']['contactnumber_country_code'] = $request->contactnumber_country_code;
        $meta_data['settings']['contactnumber'] = $request->contactnumber;
        $meta_data['settings']['whatsapp_country_code'] = $request->whatsapp_country_code;
        $meta_data['settings']['whatsapp'] = $request->whatsapp;
        $meta_data['themes']['name'] = env('DEFAULT_STOREFRONT_THEME','default-theme');
        //enable notification by default
        $meta_data['notification_settings']['order_notification']="on";
        //allow guest user by default
        $meta_data['checkout_settings']['guest_user']="on";
        $meta_data['payment_settings']['payment_method'][] = 'Enquiry';
        $meta_data['payment_settings']['Enquiry']['desc-text'] = 'Enquiry description text will be appearing here';
        $subdomain = $request->subdomain;
        if (!empty($subdomain)) {
            $user_info_subdomain = UserInfo::where('subdomain', $subdomain)->first();
            if (empty($user_info_subdomain))
                $data['subdomain'] = $subdomain;
            else if ($user_info->first()->id != $user_info_subdomain->id) {
                Session::flash('error', 'subdomain already taken!');
                return redirect()->route('admin.campaigns.storeIndex');
            }
            // $meta_data['settings']['subdomain'] = $subdomain.'.'.env('SITE_URL');
            $meta_data['settings']['subdomain'] = $subdomain;
        } else {
            Session::flash('error', 'Invalid subdomain!');
            return redirect()->route('admin.campaigns.storeIndex');
        }
        // dd($meta_data['settings']['subdomain']);
        $data['meta_data'] = json_encode($meta_data);
        // dd($request->all(),$meta_data,$data);
        $user_info = $user_info->update($data);
        $this->storePublish(new Request(["id" => $user->id, 'status' => 1]));

        if ($user->is_on_trial == 1) {
            $this->newUserTrialSubscription($user);

            return redirect()->route('admin.campaigns.storeIndex');
        }

        // else{
        //     return redirect()->route('admin.subscriptions.index')->with('no_subscription','Please subscribe to continue.');
        // }
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $user->id)->first();
        }
        $data["user"] = $user;
        $data["user_meta_data"] = $meta_data;
        $data["user_subscription"] = $subscription;

        Mail::send(new NewUserDetailsEmail($data));
        return redirect()->route('admin.campaigns.storeIndex');
    }

    public function update(Request $request)
    {
        $user_info = UserInfo::where('user_id', auth()->user()->id);
        $meta_data = json_decode($user_info->first()->meta_data, true);

        $splash = $request->only('brand_name', 'bg_color', 'brand_type', 'brand_color', 'loader_type', 'loader_color');
        if (!empty($splash)) {
            $meta_data['splash'] = $splash;

            if ($request->input('splashlogo', false)) {
                if (!$user_info->first()->splashlogo || $request->input('splashlogo') !== $user_info->first()->splashlogo->file_name) {
                    $user_info->first()->addMedia(storage_path('tmp/uploads/' . $request->input('splashlogo')))->toMediaCollection('splashlogo');
                }
            } elseif ($user_info->first()->splashlogo) {
                $user_info->first()->splashlogo->delete();
            }
        }

        $storefront = $request->only('storename', 'bannerimg', 'storelogo','logo_type');
        if (!empty($storefront)) {


            $meta_data['storefront'] = $storefront;


            // $media_store_logo = $user_info->first()->storelogo->file_name != null ? $user_info->first()->storelogo->filename : null;

            if ($request->input('storelogo', false)) {
                // if ($user_info->first()->storelogo && $user_info->first()->storelogo->file_name != $request->input('storelogo')) {
                    if (!$user_info->first()->storelogo || $request->input('storelogo') !== $user_info->first()->storelogo->file_name) {
                    $user_info->first()->addMedia(storage_path('tmp/uploads/' . $request->input('storelogo')))->toMediaCollection('storelogo');
                }
            } elseif ($user_info->first()->storelogo) {
                $user_info->first()->storelogo->delete();
            }


            $media_banner = $user_info->first()->bannerimg->pluck('file_name')->toArray();

            // dd($media_store_logo,$request->input('storelogo'),$media_banner,$request->input('bannerimg'));

            foreach ($request->input('bannerimg', []) as $file) {
                if (count($media_banner) === 0 || !in_array($file, $media_banner)) {
                    $user_info->first()->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('bannerimg');
                }
            }

            // foreach ($request->input('bannerimg',[]) as $file) {
            //     $user_info->first()->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('bannerimg');
            // }

            if ($media = $request->input('ck-media', false)) {
                Media::whereIn('id', $media)->update(['model_id' => $user_info->first()->id]);
            }

            // } else {
            if (empty($request->input('bannerimg', []))) {
                foreach ($user_info->first()->bannerimg as $camp) {
                    $camp->delete();
                }
            }

            // }
            $meta_data['storefront'] = $storefront;
        }

        $notification_settings=$request->only('order_notification');
        if (!empty($notification_settings)) {
            if(isset($notification_settings['order_notification']))
                $meta_data['notification_settings']=$notification_settings;
        }
        $notification_settings=$request->only('order_notification');
        if (!empty($notification_settings)) {
            if(isset($notification_settings['order_notification']))
                $meta_data['notification_settings']=$notification_settings;
        }
        $themes = $request->only('name','theme_background_color','theme_text_color','theme_body_color','theme_mobNav_color','theme_footer_color','product_details_display');
        if (!empty($themes)) {
            $theme_name = $themes["name"];
            $default_color_path = "../resources/views/themes/" . $theme_name . "/partials/default_color.php";
            if(file_exists($default_color_path)){

            }else{
                $themes = $request->only('name','product_details_display');
            }

            if (!isset($themes['product_details_display'])) {
                $themes['product_details_display'] = "modal_popup";
            }

            $meta_data['themes'] = $themes;
        }

        $announcement_settings = $request->only('announcement_text', 'announcement_text_color', 'announcement_background_color');
        if (!empty($announcement_settings)) {
            $meta_data['announcement_settings'] = $announcement_settings;
        }

        $payment_methods = $request->only('enquiry', 'razorpay', 'cod', 'cod-text', 'upi', 'upi-name', 'upi-id', 'upi-text', 'rp-text', 'rp-key-id', 'rp-secret-key', 'rp-webhook-secret-key', 'paypal', 'paypal-text', 'paypal-secret', 'paypal-client-id', 'paypal-env', 'stripe',  'stripe-text', 'stripe-secret', 'stripe-webhook-secret-key', 'stripe-client-id');
        if (!empty($payment_methods)) {

            // $payment_settings=[];
            if (isset($payment_methods['enquiry'])) {
                $payment_settings['payment_method'][] = 'Enquiry';
                $payment_settings['Enquiry']['desc-text'] = 'Enquiry description text will be appearing here';
            }

            if(isset($payment_methods['cod'])){
                $payment_settings['payment_method'][] = 'Cash on delivery';
                $payment_settings['Cash on delivery']['desc-text'] = $payment_methods['cod-text'];
            }

            if(isset($payment_methods['upi'])){
                $payment_settings['payment_method'][] ='UPI';
                $payment_settings['UPI']['upi-id'] = $payment_methods['upi-id'];
                $payment_settings['UPI']['upi-name'] = $payment_methods['upi-name'];
                $payment_settings['UPI']['desc-text'] = $payment_methods['upi-text'];
            }

            if(isset($payment_methods['razorpay'])){
                $payment_settings['payment_method'][] = 'Razorpay';
                $payment_settings['Razorpay']['desc-text'] = $payment_methods['rp-text'];
                $payment_settings['Razorpay']['rp-key-id'] = $payment_methods['rp-key-id'];
                $payment_settings['Razorpay']['rp-secret-key'] = $payment_methods['rp-secret-key'];
                $payment_settings['Razorpay']['rp-webhook-secret-key'] = $payment_methods['rp-webhook-secret-key'] ?? null;
            }


                // $payment_settings['payment_method'][] =isset($payment_methods['paypal'])? 'Paypal':'';
                // $payment_settings['Paypal']['desc-text'] = $payment_methods['paypal-text'];
                // $payment_settings['Paypal']['paypal-client-id'] = $payment_methods['paypal-client-id'];
                // $payment_settings['Paypal']['paypal-secret'] = $payment_methods['paypal-secret'];
                // $payment_settings['Paypal']['paypal-env'] = $payment_methods['paypal-env'];

                // $payment_settings['payment_method'][] =isset($payment_methods['stripe'])? 'Stripe':'';
                // $payment_settings['Stripe']['desc-text'] = $payment_methods['stripe-text'];
                // $payment_settings['Stripe']['stripe-client-id'] = $payment_methods['stripe-client-id'];
                // $payment_settings['Stripe']['stripe-secret'] = $payment_methods['stripe-secret'];
                // $payment_settings['Stripe']['stripe-webhook-secret-key'] = $payment_methods['stripe-webhook-secret-key'];

                // remove the empty elements ('')
                $payment_settings['payment_method']=array_filter($payment_settings['payment_method']);

            $meta_data['payment_settings'] = $payment_settings;
            // dd($meta_data);
        }
        // $storefront_shop_tv = $request->input('storefront_shop_tv');
        //     if(empty($storefront_shop_tv)){
        //         $settings['storefront_shop_tv'] = "0";
        //     }
        $shipping_methods=$request->only('shipping','public-key-id','sp-app-id','sp-sell-id','sp-private-key','sp-address-id','sp-continuous',
        'shyplite_length','shyplite_width','shyplite_height','shyplite_weight');
            if(!empty($shipping_methods)){
                if (isset($shipping_methods['shipping']) && $shipping_methods['public-key-id']) {
                    $shipping_settings['shipping_method'][] = 'shipping';

                }
                else{
                    $shipping_settings['shipping_method'][] ='';
                }

                $shipping_settings['shipping_method'][] = isset($payment_methods['shipping']) ? 'shipping' : '';

                $shipping_settings['shipping']['public-key-id'] = $shipping_methods['public-key-id'];
                $shipping_settings['shipping']['sp-app-id'] = $shipping_methods['sp-app-id'];
                $shipping_settings['shipping']['sp-sell-id'] = $shipping_methods['sp-sell-id'];
                $shipping_settings['shipping']['sp-private-key'] = $shipping_methods['sp-private-key'];
                $shipping_settings['shipping']['sp-address-id'] = $shipping_methods['sp-address-id'];
                $shipping_settings['shipping']['sp-continuous'] = $shipping_methods['sp-continuous'];
                $shipping_settings['shipping']['shyplite_length'] = $shipping_methods['shyplite_length'];
                $shipping_settings['shipping']['shyplite_width'] = $shipping_methods['shyplite_width'];
                $shipping_settings['shipping']['shyplite_height'] = $shipping_methods['shyplite_height'];
                $shipping_settings['shipping']['shyplite_weight'] = $shipping_methods['shyplite_weight'];

                $shipping_settings['shipping_method']=array_filter($shipping_settings['shipping_method']);
                $meta_data['shipping_settings'] = $shipping_settings;
            }



        $footer_settings=$request->only('facebook_id','whatsapp_num','google_link','twitter_id','youtube_link','instagram_link','pininterest_link','address1','address2','city','state','country','pincode');
        if(!empty($footer_settings)) {
            if(isset($footer_settings['address1']))
                $footer_settings['address'] = TRUE;
            $meta_data['footer_settings'] = $footer_settings;
        }

        $checkout_settings=$request->only('guest_user','order_confirmation_title','order_confirmation_description','display_coupons','tax_settings','minimum_checkout_value','delivery_charge','create_extra_charges','create_extra_charges');
        if(!empty($checkout_settings)){
            //allow guest user
            if(empty($checkout_settings['guest_user']))
                $checkout_settings['guest_user']='off';
            if(empty($checkout_settings['display_coupons']))
                $checkout_settings['display_coupons']='off';
            if(empty($checkout_settings['create_extra_charges'])){
                $checkout_settings['create_extra_charges']='0';
            }
            else{
                $checkout_settings['extra_charges_details']['extra_charge_type'] = $request->input('extra_charge_type');
                $checkout_settings['extra_charges_details']['extra_charge_name'] = $request->input('extra_charge_name');
                if($request->input('extra_charge_type') =="percentage"){
                    $checkout_settings['extra_charges_details']['extra_charge_percentage'] = $request->input('extra_charge_percentage');
                }
                else if($request->input('extra_charge_type') =="flat_charge"){
                    $checkout_settings['extra_charges_details']['extra_charge_amount'] = $request->input('extra_charge_amount');
                }
            }

            if($checkout_settings['delivery_charge'] == "charge_per_order") {
                $checkout_settings['delivery_charge_settings']['delivery_charge_per_order'] = $request->input('delivery_charge_per_order');
                $checkout_settings['delivery_charge_settings']['free_delivery_above'] = $request->input('free_delivery_above');
            }
            else if($checkout_settings['delivery_charge'] == "charge_per_product") {
                $checkout_settings['delivery_charge_settings']['delivery_charge_per_product'] = $request->input('delivery_charge_per_product');
                $checkout_settings['delivery_charge_settings']['free_delivery_above'] = $request->input('free_delivery_above');
            }


            if(empty($checkout_settings['tax_settings'])) {
                $checkout_settings['tax_settings'] = "0";
            }
            $checkout_settings['tax_setting_details']['pan_number'] = $request->input('pan_number');
            $checkout_settings['tax_setting_details']['gst_number'] = $request->input('gst_number');
            $checkout_settings['tax_setting_details']['gst_percentage'] = $request->input('gst_percentage');

            $extra_charge_id = $request->input('extra_charge_id');
            $extra_charge_name = $request->input('extra_charge_name');
            $extra_charge_percentage = $request->input('extra_charge_percentage');
            $extra_charge_amount = $request->input('extra_charge_amount');
            $extra_charge_enabled = $request->input('extra_charge_enabled');
            $extra_charges = [];
            if(!empty($extra_charge_id)){
                for($i = 0; $i < count($extra_charge_id); $i++) {
                    $extra_charge = [];
                    $extra_charge['extra_charge_id'] = $i;
                    $extra_charge['extra_charge_enabled'] = $request->input('extra_charge_enabled_'.$extra_charge_id[$i]) ?? "0";
                    $extra_charge['extra_charge_name'] = $extra_charge_name[$i];
                    $extra_charge['extra_charge_type'] = $request->input('extra_charge_type_'.$extra_charge_id[$i]);
                    $extra_charge['extra_charge_percentage'] = $extra_charge_percentage[$i] ?? null;
                    $extra_charge['extra_charge_amount'] = $extra_charge_amount[$i] ?? null;
                    array_push($extra_charges, $extra_charge);
                }
            }
            $checkout_settings['extra_charges'] = $extra_charges;

            $meta_data['checkout_settings'] = $checkout_settings;
        }

        $invoice_settings = $request->only('invoice_signature','show_signature_in_order_summary');
        if (!empty($invoice_settings)) {
            if(!isset($content_settings['show_signature_in_order_summary'])) {
                $content_settings['show_signature_in_order_summary'] = "0";
            }

            if ($request->input('invoice_signature', false)) {
                if (!$user_info->first()->invoice_signature || $request->input('invoice_signature') !== $user_info->first()->invoice_signature->file_name) {
                    $user_info->first()->addMedia(storage_path('tmp/uploads/' . $request->input('invoice_signature')))->toMediaCollection('invoice_signature');
                }
            } elseif ($user_info->first()->invoice_signature) {
                $user_info->first()->invoice_signature->delete();
            }

            $meta_data['invoice_settings'] = $invoice_settings;
        }

        $invoice_template = $request->only('invoice_template');
        if(!empty($invoice_template)) {
            $meta_data['invoice_template'] = $invoice_template;
        }
        // print_r( $request->only('display_customer_sign'));
        // exit;
        if($request->only('display_customer_sign'))
        {
             $meta_data['pos_setting']['display_customer_sign']=$request->only('display_customer_sign')['display_customer_sign'];
        }
        else{
            $meta_data['pos_setting']['display_customer_sign']='off';
        }

        if($request->only('seo_title')){
            $meta_data['seo_title'] = $request->only('seo_title');
        }
        else{
            $meta_data['seo_title'] = '';
        }

        if($request->only('seo_description')){
            $meta_data['seo_description'] = $request->only('seo_description');
        }
        else{
            $meta_data['seo_description'] = '';
        }

        if($request->only('seo_keyword')){
            $meta_data['seo_keyword'] = $request->only('seo_keyword');
        }
        else{
            $meta_data['seo_keyword'] = '';
        }

        $analytic_settings = $request->only('google_analytics_id','google_analytics_gtm_id');
        if($request->only('google_analytics_id') && $request->only('google_analytics_gtm_id')){
            $meta_data['analytic_settings'] = $analytic_settings;
        }

        $custom_script_settings = $request->only('custom_script_header','custom_script_footer');
        if(!empty($custom_script_settings)) {
            $meta_data['custom_script_settings'] = $custom_script_settings;
        }

        $tryon_settings = $request->only('tryon_key','tryon_secretKey','tryon_mode','tryon_product_detail_mode','tryon_ui_product_list_title','tryon_floating_mode','tryon_version','storetryonlogo', 'tryon_primary_color', 'tryon_secondary_color', 'tryon_tertiary_color');
        if(!empty($tryon_settings)){
            $meta_data['tryon_settings'] = $tryon_settings;
            if ($request->input('storetryonlogo', false)) {
                // if ($user_info->first()->storelogo && $user_info->first()->storelogo->file_name != $request->input('storelogo')) {
                    if (!$user_info->first()->storetryonlogo || $request->input('storetryonlogo') !== $user_info->first()->storetryonlogo->file_name) {
                    $user_info->first()->addMedia(storage_path('tmp/uploads/' . $request->input('storetryonlogo')))->toMediaCollection('storetryonlogo');
                }
            } elseif ($user_info->first()->storetryonlogo) {
                $user_info->first()->storetryonlogo->delete();
            }
        }



        $order_template = $request->only('order_template');
        if(!empty($order_template)){
            $meta_data['order_template'] = $order_template;
        }

        $vr_store = $request->only('vr-store-url');
        if(!empty($vr_store)){
            $meta_data['vr-store-url'] = $vr_store;
        }


        $settings = $request->only('private_store','subdomain', 'customdomain', 'whatsapp_country_code', 'whatsapp', 'enquiry_text', 'emailid', 'emailfromname', 'contactnumber_country_code', 'contactnumber', 'currency', 'currency_selection','storefront_show_inventory','pos_show_inventory','storefront_show_sku','pos_show_sku','storefront_shop_tv');
        if (!empty($settings)) {
            $storefront_show_sku = $request->input('storefront_show_sku');
            if (empty($storefront_show_sku)) {
                $settings['storefront_show_sku'] = "0";
            }
            $pos_show_sku = $request->input('pos_show_sku');
            if (empty($pos_show_sku)) {
                $settings['pos_show_sku'] = "0";
            }

            $currency = $request->input('currency');
            $currency_selection = $request->input('currency_selection');
            if (!empty($currency) && !empty($currency_selection)) {
                // if($currency_selection == "currency_symbol")
                    $settings['currency_symbol'] = User::CURRENCY_SYMBOLS[$currency];
            }

            $storefront_show_inventory = $request->input('storefront_show_inventory');
            if (empty($storefront_show_inventory)) {
                $settings['storefront_show_inventory'] = "0";
            }
            $pos_show_inventory = $request->input('pos_show_inventory');
            if (empty($pos_show_inventory)) {
                $settings['pos_show_inventory'] = "0";
            }
            $storefront_shop_tv = $request->input('storefront_shop_tv');
            if(empty($storefront_shop_tv)){
                $settings['storefront_shop_tv'] = "0";
            }

            if (!empty($settings['subdomain'])) {
                $user_info_subdomain = UserInfo::where('subdomain', $settings['subdomain'])->first();
                if (empty($user_info_subdomain))
                    $data['subdomain'] = $settings['subdomain'];
                else if ($user_info->first()->id != $user_info_subdomain->id) {
                    $data['subdomain'] = $meta_data['settings']['subdomain'];
                    $settings['subdomain'] = $meta_data['settings']['subdomain'];
                    Session::flash('error', 'subdomain already taken!');
                }
            } else {
                $data['subdomain'] = "";
            }

            //private store
            $shareableLink = ShareableLink::where('uuid', $user_info->first()->public_link)->first();
            if(empty($settings['private_store'])){
                $settings['private_store']='off';
                $user_info->update(['is_private'=>'0']);
                $shareableLink->update(array('password' => null));
            }else{
                $user_info->update(['is_private'=>'1']);
                $shareableLink->update(array('password' => bcrypt($request->secret_password)));
            }

            if (!Gate::denies('custom_domain')) {
                if (!empty($settings['customdomain'])) {
                    $user_info_customdomain = UserInfo::where('customdomain', $settings['customdomain'])->first();
                    if (empty($user_info_customdomain))
                        $data['customdomain'] = $settings['customdomain'];
                    else if ($user_info->first()->id != $user_info_customdomain->id) {
                        $data['customdomain'] = $meta_data['settings']['customdomain'];
                        $settings['customdomain'] = $meta_data['settings']['customdomain'];
                        Session::flash('error', 'custom domain already taken!');
                    }
                } else {
                    $data['customdomain'] = "";
                }
            } else {
                $data['customdomain'] = $meta_data['settings']['customdomain'] ?? '';
                $settings['customdomain'] = $meta_data['settings']['customdomain'] ?? '';
            }

            $meta_data['settings'] = $settings;
        }

        $content_settings = $request->only('about_us','privacy_policy','terms_and_conditions','refund_and_return_policy','contact_us','shipping_policy','show_about_us','show_privacy_policy','show_terms_and_conditions','show_refund_and_return_policy','show_contact_us','show_shipping_policy');
        if(!empty($content_settings)){
            if(!isset($content_settings['show_about_us'])) {
                $content_settings['show_about_us'] = "0";
            }
            if (!isset($content_settings['show_privacy_policy'])) {
                $content_settings['show_privacy_policy'] = "0";
            }
            if (!isset($content_settings['show_terms_and_conditions'])) {
                $content_settings['show_terms_and_conditions'] = "0";
            }
            if (!isset($content_settings['show_refund_and_return_policy'])) {
                $content_settings['show_refund_and_return_policy'] = "0";
            }
            if (!isset($content_settings['show_contact_us'])) {
                $content_settings['show_contact_us'] = "0";
            }
            if (!isset($content_settings['show_shipping_policy'])) {
                $content_settings['show_shipping_policy'] = "0";
            }
            $meta_data['content_settings'] = $content_settings;
        }

        // $data = $request->only('_token');
        $data['meta_data'] = json_encode($meta_data);
        $user_info = $user_info->update($data);
        // print_r($request->toArray());
        // exit;
        return redirect()->to(URL::previous() . "#" . $request->hash);
        // return redirect()->back();
    }

    public function saveToken(Request $request)
    {
        User::where('id',$request['data'])->first()->update(['device_token'=>$request['token']]);
        return response()->json(['Token updated Successfully']);
    }
    // public function update(Request $request) {
    //     $user_info = UserInfo::where('user_id', auth()->user()->id);
    //     $meta_data = json_decode($user_info->first()->meta_data,true);

    //     $splash = $request->only('brand_name','bg_color','brand_type','brand_color','loader_type','loader_color');
    //     if(!empty($splash)) {
    //         $meta_data['splash'] = $splash;

    //         if ($request->input('splashlogo', false)) {
    //             if (!$user_info->first()->splashlogo || $request->input('splashlogo') !== $user_info->first()->splashlogo->file_name) {
    //                 $user_info->first()->addMedia(storage_path('tmp/uploads/' . $request->input('splashlogo')))->toMediaCollection('splashlogo');
    //             }
    //         } elseif ($user_info->first()->splashlogo) {
    //             $user_info->first()->splashlogo->delete();
    //         }

    //     }

    //     $storefront = $request->only('storename','bannerimg');
    //     if(!empty($storefront)) {
    //         $meta_data['storefront'] = $storefront;

    //         // if ($request->input('bannerimg', false)) {
    //             // if (!$user_info->first()->bannerimg || $request->input('bannerimg') !== $user_info->first()->bannerimg->file_name) {
    //             //     $user_info->first()->addMedia(storage_path('tmp/uploads/' . $request->input('bannerimg')))->toMediaCollection('bannerimg');
    //             // }
    //             foreach ($request->input('bannerimg', []) as $file) {
    //                 $user_info->first()->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('bannerimg');
    //             }
    //             if ($media = $request->input('ck-media', false)) {
    //                 Media::whereIn('id', $media)->update(['model_id' => $user_info->first()->id]);
    //             }
    //         // } else {
    //             if(empty($request->input('bannerimg', []))) {
    //                 foreach ($user_info->first()->bannerimg as $camp) {
    //                         $camp->delete();
    //                 }
    //             }

    //         // }
    //         $meta_data['storefront'] = $storefront;
    //     }

    //     $themes = $request->only('name');
    //     if(!empty($themes)){
    //         $meta_data['themes'] = $themes;
    //     }

    //     $settings = $request->only('subdomain','customdomain','whatsapp','enquiry_text','emailid','emailfromname','contactnumber','order_confirmation_title','order_confirmation_description','currency','currency_selection');
    //     if(!empty($settings)) {
    //         if(!empty($settings['subdomain'])) {
    //             $user_info_subdomain = Campaign::where('subdomain',$settings['subdomain'])->first();
    //             if(empty($user_info_subdomain))
    //                 $data['subdomain'] = $settings['subdomain'];
    //             else if($user_info->first()->id != $user_info_subdomain->id){
    //                 $data['subdomain'] = $meta_data['settings']['subdomain'];
    //                 $settings['subdomain'] = $meta_data['settings']['subdomain'];
    //                 Session::flash('error', 'subdomain already taken!');
    //             }

    //         } else {
    //             $data['subdomain'] = "";
    //         }

    //         if(app('impersonate')->isImpersonating()){
    //             if(!empty($settings['customdomain'])) {
    //                 $user_info_customdomain = Campaign::where('customdomain',$settings['customdomain'])->first();
    //                 if(empty($user_info_customdomain))
    //                     $data['customdomain'] = $settings['customdomain'];
    //                 else if($user_info->first()->id != $user_info_customdomain->id){
    //                     $data['customdomain'] = $meta_data['settings']['customdomain'];
    //                     $settings['customdomain'] = $meta_data['settings']['customdomain'];
    //                     Session::flash('error', 'custom domain already taken!');
    //                 }

    //             } else {
    //                 $data['customdomain'] = "";
    //             }
    //         }
    //         else{
    //             $data['customdomain'] = $meta_data['settings']['customdomain'];
    //             $settings['customdomain'] = $meta_data['settings']['customdomain'];
    //         }

    //         $meta_data['settings'] = $settings;
    //     }

    //     // $data = $request->only('_token');
    //     $data['meta_data'] = json_encode($meta_data);

    //     $user_info = $user_info->update($data);

    //     return redirect()->to(URL::previous() . "#".$request->hash);
    //     // return redirect()->back();
    // }

    public function storePublish(Request $request)
    {
        $userInfo = UserInfo::where('user_id', $request->id);
        $userInfo->update(array('id' => $request->id, 'is_start' => $request->status));
        $userInfo = $userInfo->first();
        $shareableLink = ShareableLink::where('uuid', $userInfo->public_link)->first();
        $shareableLink->update(array('active' => $request->status));
        return redirect()->back();
    }

    public function storePreview(ShareableLink $link)
    {
        $userSettings = $link->shareable;
        $userSettings->is_start = false;
        $campaign = Campaign::where("user_id", $userSettings->user_id)->where("is_start", 1)->get();
        return $this->storeFront($campaign, $userSettings);
    }

    public function storePublic(Request $link)
    {
        if ($link->subdomain == "www" && $link->domain == "simplisell")
            return Redirect::to(env('APP_URL', 'localhost'));
        if (env('SITE_URL') == $link->domain . "." . $link->tld)
            return view('landing.index');
        if (!empty($link->subdomain) && !empty($link->domain) && !empty($link->tld)) {
            if($link->subdomain == "www")
                $cdomain = $link->domain . "." . $link->tld;
            else
                $cdomain = $link->subdomain . "." . $link->domain . "." . $link->tld;
            // $data = Campaign::where('customdomain', $cdomain)->first();
            $data = UserInfo::where('customdomain', $cdomain)->first();
        }
        if (empty($link->subdomain) && !empty($link->domain) && !empty($link->tld)) {
            $cdomain = $link->domain . "." . $link->tld;
            $data = UserInfo::where('customdomain', $cdomain)->first();
        }
        if (empty($data))
            if (!empty($link->subdomain))
                $data = UserInfo::where('subdomain', $link->subdomain)->first();
        $campaign = "";
        if (!empty($data)) {
            if ($data->is_start == 1) {
                $shareableLink = ShareableLink::where('uuid', $data->public_link)->first();
                // dd($data,$link->subdomain,$link->domain,$link->tld,$shareableLink);

                if (!empty($shareableLink)) {
                    $userSettings = $shareableLink->shareable;
                    // dd($shareableLink,$userSettings);
                    $campaign = Campaign::where("user_id", $userSettings->user_id)->where("type","!=",2)->where("is_start", 1)->where("is_added_home", 1)->orderBy('updated_at','desc')->get();
                    return $this->storeFront($campaign, $userSettings);
                }
            }
        }

        if (!empty($link->shareable_link->shareable) && empty($link->subdomain)) {
            $userSettings = UserInfo::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();
            // Campaign::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();
            // dd($userInfo);

            $campaign = Campaign::where("user_id", $userSettings->user_id)->where("type","!=",2)->where("is_start", 1)->where("is_added_home", 1)->orderBy('updated_at','desc')->get();
        }

        if (!empty($campaign)) {
            return $this->storeFront($campaign, $userSettings);
        }

        /* company level subdomain end */
        // return view('landing.index');
        return view('errors.404');
    }

    public function storePublicVideo(Request $link)
    {
        if ($link->subdomain == "www" && $link->domain == "simplisell")
            return Redirect::to(env('APP_URL', 'localhost'));
        if (env('SITE_URL') == $link->domain . "." . $link->tld)
            return view('landing.index');
        if (!empty($link->subdomain) && !empty($link->domain) && !empty($link->tld)) {
            if($link->subdomain == "www")
                $cdomain = $link->domain . "." . $link->tld;
            else
                $cdomain = $link->subdomain . "." . $link->domain . "." . $link->tld;
            // $data = Campaign::where('customdomain', $cdomain)->first();
            $data = UserInfo::where('customdomain', $cdomain)->first();
        }
        if (empty($link->subdomain) && !empty($link->domain) && !empty($link->tld)) {
            $cdomain = $link->domain . "." . $link->tld;
            $data = UserInfo::where('customdomain', $cdomain)->first();
        }
        if (empty($data))
            if (!empty($link->subdomain))
                $data = UserInfo::where('subdomain', $link->subdomain)->first();
        $campaign = "";
        if (!empty($data)) {
            if ($data->is_start == 1) {
                $shareableLink = ShareableLink::where('uuid', $data->public_link)->first();
                // dd($data,$link->subdomain,$link->domain,$link->tld,$shareableLink);

                if (!empty($shareableLink)) {
                    $userSettings = $shareableLink->shareable;
                    // dd($shareableLink,$userSettings);
                    $campaign = Campaign::where("user_id", $userSettings->user_id)->where("is_start", 1)->where("type",2)->where("is_added_home", 1)->orderBy('updated_at','desc')->get();
                    return $this->storeFront($campaign, $userSettings);
                }
            }
        }

        if (!empty($link->shareable_link->shareable) && empty($link->subdomain)) {
            $userSettings = UserInfo::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();
            // Campaign::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();
            // dd($userInfo);

            $campaign = Campaign::where("user_id", $userSettings->user_id)->where("type",2)->where("is_start", 1)->where("is_added_home", 1)->orderBy('updated_at','desc')->get();
        }

        if (!empty($campaign)) {
            return $this->storeFrontVideo($campaign, $userSettings);
        }

        /* company level subdomain end */
        return view('errors.404');
    }

    public function storePublic_old(ShareableLink $link)
    {
        $userInfo = userInfo::where("public_link", $link->uuid)->first();
        if (!empty($userInfo))
            return $this->storeFront($userInfo);
        else
            return view('errors.404');
    }

    public function removebanerImg(Request $request)
    {
        $mediaId = $request->id;
        $media = Media::find($request->input('id'));
        $model = UserInfo::find($media->model_id);
        $model->deleteMedia($media->id);
        return response()->json(['success' => 'done']);
    }

    public function createExtraCharge($extra_charge_id){
        $user = auth()->user();
        $user_info = UserInfo::where('user_id', $user->id)->first();
        $meta_data = json_decode($user_info->meta_data, true);

        return view('templates.admin.extra_charge', compact('extra_charge_id','meta_data'));
    }

    public function storeLogin(Request $link)
    {
        if ($link->subdomain == "www" && $link->domain == "simplisell")
            return Redirect::to(env('APP_URL', 'localhost'));
        if (env('SITE_URL') == $link->domain . "." . $link->tld)
            return view('landing.index');
        if (!empty($link->subdomain) && !empty($link->domain) && !empty($link->tld)) {
            if($link->subdomain == "www")
                $cdomain = $link->domain . "." . $link->tld;
            else
                $cdomain = $link->subdomain . "." . $link->domain . "." . $link->tld;
            // $data = Campaign::where('customdomain', $cdomain)->first();
            $data = UserInfo::where('customdomain', $cdomain)->first();
        }
        if (empty($link->subdomain) && !empty($link->domain) && !empty($link->tld)) {
            $cdomain = $link->domain . "." . $link->tld;
            $data = UserInfo::where('customdomain', $cdomain)->first();
        }
        if (empty($data))
            if (!empty($link->subdomain))
                $data = UserInfo::where('subdomain', $link->subdomain)->first();
        $campaign = "";
        if (!empty($data)) {
            if ($data->is_start == 1) {
                $shareableLink = ShareableLink::where('uuid', $data->public_link)->first();
                // dd($data,$link->subdomain,$link->domain,$link->tld,$shareableLink);

                if (!empty($shareableLink)) {
                    $userSettings = $shareableLink->shareable;
                    // dd($shareableLink,$userSettings);
                    $campaign = Campaign::where("user_id", $userSettings->user_id)->where("type","!=",2)->where("is_start", 1)->where("is_added_home", 1)->orderBy('updated_at','desc')->get();
                    return $this->storeFrontLogin($campaign, $userSettings);
                }
            }
        }

        if (!empty($link->shareable_link->shareable) && empty($link->subdomain)) {
            $userSettings = UserInfo::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();

            $campaign = Campaign::where("user_id", $userSettings->user_id)->where("type","!=",2)->where("is_start", 1)->where("is_added_home", 1)->orderBy('updated_at','desc')->get();
        }

        if (!empty($campaign)) {
            return $this->storeFrontLogin($campaign, $userSettings);
        }

        return view('errors.404');
    }

    public function storeRegister(Request $link)
    {
        if ($link->subdomain == "www" && $link->domain == "simplisell")
            return Redirect::to(env('APP_URL', 'localhost'));
        if (env('SITE_URL') == $link->domain . "." . $link->tld)
            return view('landing.index');
        if (!empty($link->subdomain) && !empty($link->domain) && !empty($link->tld)) {
            if($link->subdomain == "www")
                $cdomain = $link->domain . "." . $link->tld;
            else
                $cdomain = $link->subdomain . "." . $link->domain . "." . $link->tld;
            // $data = Campaign::where('customdomain', $cdomain)->first();
            $data = UserInfo::where('customdomain', $cdomain)->first();
        }
        if (empty($link->subdomain) && !empty($link->domain) && !empty($link->tld)) {
            $cdomain = $link->domain . "." . $link->tld;
            $data = UserInfo::where('customdomain', $cdomain)->first();
        }
        if (empty($data))
            if (!empty($link->subdomain))
                $data = UserInfo::where('subdomain', $link->subdomain)->first();
        $campaign = "";
        if (!empty($data)) {
            if ($data->is_start == 1) {
                $shareableLink = ShareableLink::where('uuid', $data->public_link)->first();
                // dd($data,$link->subdomain,$link->domain,$link->tld,$shareableLink);

                if (!empty($shareableLink)) {
                    $userSettings = $shareableLink->shareable;
                    // dd($shareableLink,$userSettings);
                    $campaign = Campaign::where("user_id", $userSettings->user_id)->where("type","!=",2)->where("is_start", 1)->where("is_added_home", 1)->orderBy('updated_at','desc')->get();
                    return $this->storeFrontLogin($campaign, $userSettings);
                }
            }
        }

        if (!empty($link->shareable_link->shareable) && empty($link->subdomain)) {
            $userSettings = UserInfo::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();

            $campaign = Campaign::where("user_id", $userSettings->user_id)->where("type","!=",2)->where("is_start", 1)->where("is_added_home", 1)->orderBy('updated_at','desc')->get();
        }

        if (!empty($campaign)) {
            return $this->storeFrontRegister($campaign, $userSettings);
        }

        return view('errors.404');
    }
    
    public function storeforgotpassword(Request $link)
    {
        if ($link->subdomain == "www" && $link->domain == "simplisell")
            return Redirect::to(env('APP_URL', 'localhost'));
        if (env('SITE_URL') == $link->domain . "." . $link->tld)
            return view('landing.index');
        if (!empty($link->subdomain) && !empty($link->domain) && !empty($link->tld)) {
            if($link->subdomain == "www")
                $cdomain = $link->domain . "." . $link->tld;
            else
                $cdomain = $link->subdomain . "." . $link->domain . "." . $link->tld;
            // $data = Campaign::where('customdomain', $cdomain)->first();
            $data = UserInfo::where('customdomain', $cdomain)->first();
        }
        if (empty($link->subdomain) && !empty($link->domain) && !empty($link->tld)) {
            $cdomain = $link->domain . "." . $link->tld;
            $data = UserInfo::where('customdomain', $cdomain)->first();
        }
        if (empty($data))
            if (!empty($link->subdomain))
                $data = UserInfo::where('subdomain', $link->subdomain)->first();
        $campaign = "";
        if (!empty($data)) {
            if ($data->is_start == 1) {
                $shareableLink = ShareableLink::where('uuid', $data->public_link)->first();
                // dd($data,$link->subdomain,$link->domain,$link->tld,$shareableLink);

                if (!empty($shareableLink)) {
                    $userSettings = $shareableLink->shareable;
                    // dd($shareableLink,$userSettings);
                    $campaign = Campaign::where("user_id", $userSettings->user_id)->where("type","!=",2)->where("is_start", 1)->where("is_added_home", 1)->orderBy('updated_at','desc')->get();
                    return $this->storeFrontLogin($campaign, $userSettings);
                }
            }
        }

        if (!empty($link->shareable_link->shareable) && empty($link->subdomain)) {
            $userSettings = UserInfo::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();

            $campaign = Campaign::where("user_id", $userSettings->user_id)->where("type","!=",2)->where("is_start", 1)->where("is_added_home", 1)->orderBy('updated_at','desc')->get();
        }

        if (!empty($campaign)) {
            return $this->storeFrontForgotPassword($campaign, $userSettings);
        }

        return view('errors.404');
    }
}
