<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class NotificationController extends Controller
{
    public function index()
    {
        return view('test');
    }

    public function saveToken(Request $request)
    {
        $user=auth()->user()->update(['device_token'=>$request->token]);
        return response()->json(['token saved successfully.']);
    }
  
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function sendNotification_test(Request $request)
    {
        $firebaseToken = auth()->user()->device_token;
          
        // $message = array('title' => $data["title"], 'content'   =>  $data["body"]);

        $SERVER_API_KEY = env('FCM_SECRET_KEY');

        // $data = array("registration_ids" => $firebaseToken,"data" => array("title" => $request->title,"content" => $request->body));

        $data = array("to" => $firebaseToken,"priority" => 10, "webpush"=> array("headers"=>array ("Urgency"=> "high")) ,"notification" => array( "title" => $request["title"], "body" =>$request["body"],"icon" => "favicon.png", "click_action" => "https://simplisell.co"));

        $dataString = json_encode($data);
    
        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];
    
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
               
        $response = curl_exec($ch);
  
        return $response;

    }

    public function sendNotification(Request $request)
    {  
        $firebaseToken = User::where('id',$request->user_id)->first()->device_token;
          
        $SERVER_API_KEY = env('FCM_SECRET_KEY');

        $data = array("to" => $firebaseToken,"priority" => 10, "webpush"=> array("headers"=>array ("Urgency"=> "high")) ,"notification" => array( "title" => $request["title"], "body" =>$request["body"],"icon" => "favicon.png", "click_action" => "https://simplisell.co"));

        // $data = array("to" => $firebaseToken, "notification" => array( "title" => $request->title, "body" => $request->body,"icon" => "favicon.png", "click_action" => "https://simplisell.co"));

        $dataString = json_encode($data);
     
        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];
    
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
               
        $response = curl_exec($ch);
  
        return $response;
    }
}
