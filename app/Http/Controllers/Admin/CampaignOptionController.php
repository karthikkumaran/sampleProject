<?php

namespace App\Http\Controllers\Admin;

use App\Campaign;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyCampaignRequest;
use App\Http\Requests\StoreCampaignRequest;
use App\Http\Requests\UpdateCampaignRequest;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

use Carbon\Carbon;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;
use QrCode;

use Session;
use URL;

use App\User;
use App\CompanyInfo;
use App\Product;
use App\ProductCategory;
use App\Entitycard;
use App\Http\Controllers\Traits\AddEntityCardTrait;

class CampaignOptionController extends Controller
{
    use MediaUploadingTrait;

    
    public function index(Request $request)
    {
        $campaign = Campaign::where('id', $request->campaign_id)->first();
        $meta_data = json_decode($campaign->meta_data, true);
        // dd($campaign->getFirstMediaUrl('splashlogo'));
        return view('templates.admin.campaign_options',compact('campaign', 'meta_data'));
    }

    public function update(Request $request)
    {
        $campaign->update($request->all());

        return redirect()->route('admin.campaigns.index');
    }

   
    public function options(Request $request) {
        $campagin = Campagin::where('id', $request->campaign_id)->first();
        return view('templates.admin.campaign_options',compact('campagin'));
    }

    public function optionsUpdate(Request $request){
        // dd($request->all());
        $campaign = Campaign::where('id', $request->id);

        // $meta_data = (array)json_decode($campaign->first()->meta_data);
        $meta_data = json_decode($campaign->first()->meta_data,true);

        // $splash = $meta_data['splash'];

        $splash = $request->only('brand_name','bg_color','brand_type','brand_color','loader_type','loader_color');
        if(!empty($splash)) {
            $meta_data['splash'] = $splash;
            // if ($request->input('splashlogo', false)) {
            //     $campaign->first()->addMedia(storage_path('tmp/uploads/' . $request->input('splashlogo')))->toMediaCollection('splashlogo');
            // }
    
            // if ($media = $request->input('ck-media', false)) {
            //     Media::whereIn('id', $media)->update(['model_id' => $campaign->id]);
            // }
            if ($request->input('splashlogo', false)) {
                if (!$campaign->first()->splashlogo || $request->input('splashlogo') !== $campaign->first()->splashlogo->file_name) {
                    $campaign->first()->addMedia(storage_path('tmp/uploads/' . $request->input('splashlogo')))->toMediaCollection('splashlogo');
                }
            } elseif ($campaign->first()->splashlogo) {
                $campaign->first()->splashlogo->delete();
            }

        }

        $storefront = $request->only('bannerimg');
        if(!empty($storefront)) {
            $meta_data['storefront'] = $storefront;
            
            // if ($request->input('bannerimg', false)) {
                // if (!$campaign->first()->bannerimg || $request->input('bannerimg') !== $campaign->first()->bannerimg->file_name) {
                //     $campaign->first()->addMedia(storage_path('tmp/uploads/' . $request->input('bannerimg')))->toMediaCollection('bannerimg');
                // }
                foreach ($request->input('bannerimg', []) as $file) {
                    $campaign->first()->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('bannerimg');
                }
                if ($media = $request->input('ck-media', false)) {
                    Media::whereIn('id', $media)->update(['model_id' => $campaign->first()->id]);
                }
            // } else {
                if(empty($request->input('bannerimg', []))) {
                    foreach ($campaign->get() as $camp) {
                        $camp->bannerimg->delete();
                    }
                }
            
            // }

        }

        $settings = $request->only('subdomain','customdomain','whatsapp','emailid','emailfromname','contactnumber');
        if(!empty($settings)) {
            if(!empty($settings['subdomain'])) {
                $campaign_subdomain = Campaign::where('subdomain',$settings['subdomain'])->first();
                if(empty($campaign_subdomain))
                    $data['subdomain'] = $settings['subdomain'];
                else if($campaign->first()->id != $campaign_subdomain->id){
                    $data['subdomain'] = $meta_data['settings']['subdomain'];
                    $settings['subdomain'] = $meta_data['settings']['subdomain'];
                    Session::flash('error', 'subdomain already taken!');
                }
                    
            } else {
                $data['subdomain'] = "";
            }
            if(!empty($settings['customdomain'])) {
                $campaign_customdomain = Campaign::where('customdomain',$settings['customdomain'])->first();
                if(empty($campaign_customdomain))
                    $data['customdomain'] = $settings['customdomain'];
                else if($campaign->first()->id != $campaign_customdomain->id){
                    $data['customdomain'] = $meta_data['settings']['customdomain'];
                    $settings['customdomain'] = $meta_data['settings']['customdomain'];
                    Session::flash('error', 'custom domain already taken!');
                }
                    
            } else {
                $data['customdomain'] = "";
            }
            $meta_data['settings'] = $settings;
        }   

        // $data = $request->only('_token');
        $data['meta_data'] = json_encode($meta_data);

        $campaign = $campaign->update($data);

        // $arobject->update($request->all());

        // if ($request->input('object', false)) {
        //     if (!$arobject->object || $request->input('object') !== $arobject->object->file_name) {
        //         $arobject->addMedia(storage_path('tmp/uploads/' . $request->input('object')))->toMediaCollection('object');
        //     }
        // } elseif ($arobject->object) {
        //     $arobject->object->delete();
        // }

        return redirect()->to(URL::previous() . "#".$request->hash);
        // return redirect()->back();
    }
}
