<?php

namespace App\Http\Controllers\Admin\Analytics;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Statistics\Path;
use App\Models\Statistics\Events;
use Yajra\DataTables\Facades\DataTables;
use App\Campaign;
use App\Models\Statistics\UniqueVistor;
use App\ordersCustomer;
use App\orders;
use App\ordersProduct;
use App\Product;
use Carbon\Carbon;
use App\UserInfo;


class StatisticsController extends Controller
{

    public function index(Request $request)
    {
        // $labels = ["Agent", "Device", "Pages", "Platform", "Request", "Route", "Geoip", "Events"];
        $labels = ["Events", "Pages", "Unique_Visitors"];
        return view('admin.statistics.index', compact('labels'));
    }

    public function getTableName(Request $request)
    {
        if ($request->ajax()) {

            $title = "";

            if ("Pages" == $request->label) {

                $title = "Page";
            } else if ("Events" == $request->label) {

                $title = "Event";
            } else if ("Unique_Visitors" == $request->label) {

                $title = "Unique Visitor";
            }
            $label = $request->label;
            return view('admin.statistics.stats_table', compact('label', 'title'));
        }
    }

    public function StatsData(Request $request)
    {

        if ($request->ajax()) {
            $data = null;
            $dataColumns = [];
            $team_id = auth()->user()->team_id;

            if ("customers" == $request->label) {

                $q = $request->q;

                $customers = ordersCustomer::select('fname', 'lname', 'email', 'mobileno')->orwhere('fname', 'Like', '%' . $q . '%')
                    ->orWhere('lname', 'LIKE', '%' . $q . '%')
                    ->orWhere('email', 'LIKE', '%' . $q . '%')
                    ->orWhere('mobileno', 'LIKE', '%' . $q . '%')
                    ->groupBy('email', 'mobileno')->get();

                return $customers;
            }

            if ("Pages" == $request->label) {

                $data = Path::where('team_id', $team_id)->get();
                $table = Datatables::of($data);
                $table->removeColumn('method');
                $table->removeColumn('parameters');
                $table->removeColumn('locale');

                $table->editColumn('parameters', function ($row) {
                    return is_array($row->parameters) ? implode(', ', array_map(function ($v, $k) {
                        return sprintf("%s='%s'", $k, $v);
                    }, $row->parameters, array_keys($row->parameters))) : $row->parameters;
                });

                $table->editColumn('path', function ($row) {
                    if ($row->path[0] == "s")
                        $pagaName = "Store Front";
                    elseif ($row->path[0] == "c")
                        $pagaName = "Checkout Page";
                    elseif ($row->path[0] == "p")
                        $pagaName = "Catalog Page";
                    elseif ($row->path[0] == "d")
                        $pagaName =  "Product Details Page";
                    return $pagaName;
                });

                $table->addColumn('S No', ' ', 0);
                $dataColumns = ["S.No", "Host", "Page Name", "Count"];
            } else if ("Events" == $request->label) {

                $data = Events::where('team_id', $team_id)->groupBy("event_name", "product_id", "campaign_id")
                    ->select("event_name", "product_id", "campaign_id")
                    ->selectRaw('count(event_name) as count')->get();

                // dd($data);
                $table = Datatables::of($data);
                $table->removeColumn('created_at');
                $table->removeColumn('updated_at');

                $table->editColumn('event_name', function ($row) {
                    $name = preg_replace('/[\d_]+/', ' ', $row->event_name);
                    return ucwords($name);
                });

                $table->editColumn('product_id', function ($row) {
                    return $row->product_id ? Product::find($row->product_id)->name : "";
                });

                $table->editColumn('campaign_id', function ($row) {
                    return $row->campaign_id ? Campaign::find($row->campaign_id)->name : "";
                });

                $table->addColumn('S No', ' ', 0);
                $dataColumns = ["S.No", "Event Name", "Product Name", "Catalog Name", "Count"];
            } else if ("Unique_Visitors" == $request->label) {
                $data = UniqueVistor::where('team_id', $team_id)->groupBy("ip_address")
                    ->select("ip_address", "country_name", "state_name", "city", "postal_code", "timezone")
                    ->selectRaw('count(ip_address) as count')->get();

                // dd($data->toArray());
                $table = Datatables::of($data);
                $table->removeColumn('created_at');
                $table->removeColumn('updated_at');

                $table->addColumn('S No', ' ', 0);
                $dataColumns = ["S.No", "Ip Address", "Country", "State", "City", "Postal Code", "Timezone", "Count"];
            }

            $table->removeColumn('team_id');
            $table->removeColumn('id');

            return $table->with('dataColumns', $dataColumns)->make(true);
        }
    }

    public function statsChart(Request $request)
    {
        $team_id = auth()->user()->team_id;
        $period = null;
        $temp_array = [];
        $months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        $days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
        $hours = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'];
        $bgcolours = [
            'rgba(22, 212, 88, 0.7)',
            'rgba(54, 162, 235, 0.7)',
            'rgba(255, 206, 86, 0.7)',
            'rgba(75, 192, 192, 0.7)',
            'rgba(153, 102, 255, 0.7)',
            'rgba(255, 159, 64, 0.7)',
            'rgba(224, 2, 03 ,0.7)',
            'rgba(24, 212, 203 ,0.7)',
            'rgba(225, 12, 203 ,0.7)',
            'rgba(2, 2, 203 ,0.7)',
        ];

        $bordercolours = [
            'rgb(22, 212, 88)',
            'rgb(54, 162, 235)',
            'rgb(255, 206, 86)',
            'rgb(75, 192, 192)',
            'rgb(153, 102, 255)',
            'rgb(255, 159, 64)',
            'rgb(224, 2, 03 )',
            'rgb(24, 212, 203 )',
            'rgb(225, 12, 203 )',
            'rgb(2, 2, 203 )',
        ];
        $datum = null;
        $title = '';


        if ($request->ajax()) {
            $stat = null;
            if ($request->label == "Events") {
                $stat = Events::where('team_id', $team_id)->select('name');
            }
            if ($request->label == "Unique_Visitors") {
                $stat = UniqueVistor::where('team_id', $team_id);
            }

            if ($request->flag == 'true' && $request->month_year == "month") {

                $title = "Month wise " . $request->label . " and count";
                $x_axis = 'Months';
                $temp_array = [0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 0, 0];
                $period = $months;

                $datum = $stat
                    ->selectRaw("MONTHNAME(created_at) as periodname")
                    ->whereYear('created_at', date('Y'));
            } elseif ($request->flag == 'true' && $request->month_year == "year") {

                $title = "Year wise " . $request->label . " and count";
                $x_axis = 'Year';
                $period = [];

                $datum = $stat->selectRaw("YEAR(created_at) as periodname");
            } elseif ($request->flag == 'true' && ($request->sdate || $request->edate)) {


                $sdate = $request->sdate ? Carbon::parse($request->sdate)->subDay(1) : Carbon::now()->subDay(1)->toDateString();
                $edate = $request->edate ? Carbon::parse($request->edate)->addDay(1) : Carbon::now()->addDay(1)->toDateString();

                $title = "Events from " . $request->sdate . " to " . $edate;
                $x_axis = 'Date';
                $period = [];

                $datum = $stat
                    ->selectRaw('DATE(created_at) as periodname')
                    ->whereBetween('created_at', [$sdate, $edate]);
            } else if ($request->flag == "true" && $request->current == "today") {

                $title = "Today's " . $request->label . " and count";
                $x_axis = 'Hour';
                $temp_array = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                $period = $hours;


                $datum = $stat
                    ->selectRaw('HOUR(created_at) as periodname')
                    ->whereDate('created_at', Carbon::today()->toDateString());
            } elseif ($request->flag = "false" || ($request->flag == "true" && $request->current == "last_7_days")) {
                // last week data

                ///
                $sdate = Carbon::tomorrow()->subdays(7);
                $edate = Carbon::tomorrow()->format('Y-m-d');
                $temp_array = [0, 0, 0, 0, 0, 0, 0];
                $period = [];

                while ($sdate->lte($edate)) {
                    $period[] = $sdate->toDateString();
                    $sdate->addDay();
                }

                $datum = $stat
                    ->selectRaw('DATE(created_at) as periodname')
                    ->whereBetween('created_at', [$sdate->subDays(8)->toDateString(), $edate]);

                ///
                $title = "Last 7 day's " . $request->label . " and count";
                $x_axis = 'Date';
            } else if ($request->flag == "true" && $request->current == "last_30_days") {

                $title = "Last 30 days's " . $request->label . " and count";
                $x_axis = 'Date';
                $period = [];



                $datum = $stat
                    ->selectRaw('DATE(created_at) as periodname')
                    ->whereMonth('created_at', '>=', Carbon::now()->subDays(30));
            } else if ($request->flag == "true" && $request->current == "thismonth") {

                $title = "This month's " . $request->label . " and count";
                $x_axis = 'Date';
                $period = [];


                $datum = $stat
                    ->selectRaw('DATE(created_at) as periodname')
                    ->whereMonth('created_at', date('m'))
                    ->whereYear('created_at', date('Y'));
            } else if ($request->flag == "true" && $request->current == "yesterday") {
                $title = "Yesterday's " . $request->label . " and count";
                $x_axis = 'Hour';
                $period = $hours;
                $temp_array = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];


                $datum = $stat
                    ->selectRaw('HOUR(created_at) as periodname')
                    ->whereDate('created_at', date("Y-m-d", strtotime('-1 days')));
            }

            $label = [];
            $data = [];
            $y_axis = "Count";

            if ($request->label == "Events") {
                $datum = $datum->groupBy(['periodname', 'name'])->selectRaw('count( name) as count')
                    ->get()->groupBy('periodname');
                // dd($datum->toArray());
            } elseif ($request->label == "Unique_Visitors") {
                $datum = $datum->groupBy('periodname')->selectRaw('count(distinct(ip_address)) as count')->get()->groupBy('periodname');
            }

            if ($request->month_year == "year" || $request->current == "last_30_days" || ($request->sdate || $request->edate) || $request->current == "thismonth") {

                $period = array_keys($datum->toArray());
                $temp_array = array_fill(0, count($period), 0);
            }

            foreach ($datum as $key => $values) {
                foreach ($values as $value) {
                    $index = array_search($value->periodname, $period);
                    if ($request->label == "Events") {
                        if (in_array($value->name, $label)) {
                            $data[$value->name][$index] = $value->count;
                        } else {
                            array_push($label, $value->name);
                            $temp = $temp_array;
                            $temp[$index] = $value->count;
                            $data[$value->name] = $temp;
                        }
                    } elseif ($request->label == "Unique_Visitors") {
                        $temp_array[$index] = $value["count"];
                        $total_price[$index] = $value["total_price"];
                    }
                }
            }

            if ($request->label == "Unique_Visitors")
                $data = $temp_array;



            return [
                "uniquevisitor" => $request->label == "Unique_Visitors" ? $this->ShowUniqueVisitor() : null,
                "axes" => [$x_axis, $y_axis],
                "title" => $title,
                "labels" => $period,
                "data" => $data,
                "label" => $label,
                'bgcolour' => array_slice($bgcolours, 0, count($label)),
                'bordercolour' => array_slice($bordercolours, 0, count($label))
            ];
        }
    }

    public function ShowUniqueVisitor()
    {
        $team_id = auth()->user()->team_id;
        $visitor_count = UniqueVistor::where('team_id', $team_id)->distinct('ip_address')->count();
        return $visitor_count ? $visitor_count : 0;
    }

    //biz-insights analytics main-function
    public function salesStat(Request $request, $sales = null)
    {

        $team_id = auth()->user()->team_id;
        $months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        $hours = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'];
        $total_price = [];
        $stat_count = null;
        $temp_array = null;
        $default = false;
        $dashboard = false;

        if ($sales) {

            if ($sales == "dashboard") {
                $user_id = auth()->user()->id;
                $currency_settings = UserInfo::where("user_id", $user_id)->select('meta_data->settings->currency as currency', 'meta_data->settings->currency_selection as currency_selection')->first();
                return view('admin.statistics.dashboard.index', compact('currency_settings'));
            }
            return view('admin.statistics.sales', ["stat" => $sales]);
        }

        if ($request->ajax()) {

            $datum = null;
            $stat = null;
            if ($request->label == "catalog-views" || $request->label =="videocatalog-views"|| $request->label  == "product-views" || $request->label  == "store-views" || $request->label == 'story-views') {
                $dashboard = $request->label  == "store-views" || $request->label  == "story-views" ? true : false;
                $stat = Events::where('team_id', $team_id)->select('name');
            } else if ($request->label == "sales" || $request->label == "dsales" || $request->label == "dorders" || $request->label == "orders" || $request->label == "discounts" || $request->label == "customers" || $request->label == "avg-order-value") {
                $dashboard = $request->label == "discounts" ||  $request->label == "dsales" || $request->label == "dorders" || $request->label == "customers" || $request->label == "avg-order-value" ? true : false;
                // $stat = ordersCustomer::Join('orders', function ($join) {
                    // $join->on('orders.id', '=', 'orders_customers.order_id');
                // })->join('transactions',function ($join){
                    // $join->on('orders.transaction_id', '=', 'transactions.id');})
                    // ->select('orders_customers.*')->where('orders_customers.team_id', $team_id);

                $stat = ordersCustomer::where('team_id', $team_id);
                // dd($stat);
                $stat_sales = ordersCustomer::Join('orders', function ($join) {
                    $join->on('orders.id', '=', 'orders_customers.order_id');
                    })->join('transactions',function ($join){
                    $join->on('orders.transaction_id', '=', 'transactions.id');
                });

            } else if ($request->label == "items-sold") {
                $dashboard = true;
                $stat = ordersProduct::where('team_id', $team_id);
            } else if ($request->label == 'hour-order' || $request->label == 'day-order') {
                $data = $this->ordersStat($request->label);
                return $data;
            }

            $request->label = $request->label == "dsales" || $request->label == "dorders" ? preg_replace('/d/', '', $request->label, 1) : $request->label;
            if ($request->flag == 'true' && ($request->month_year == "month" || $request->current == "monthly")) {

                $title = "Month wise " . $request->label . " and count";
                $x_axis = 'Month';
                $temp_array = [0, 0, 0, 0,  0, 0, 0, 0, 0, 0, 0, 0];
                $period = $months;
                if ($request->label == "sales" || $request->label == "dsales" || $request->label == "dorders" || $request->label == "orders" || $request->label == "discounts" || $request->label == "customers" || $request->label == "avg-order-value")
                {
            $stat_sales = $stat_sales->whereYear('orders_customers.created_at', '=', date('Y'))->whereMonth('orders_customers.created_at', '=', date('m'));
                $datanum_dup = $stat->get();
                // dd($datanum_dup);
                $order_not_transaction = array();
                foreach ($datanum_dup as $key => $values) {
                                    
                    $isOrder = Orders::where('id',$values->order_id )->whereNotNull('transaction_id')->first();     
                    if(isset($isOrder)){

                        array_push($order_not_transaction, $values->order_id);

                    }
                }
                $stat = $stat->whereIn('order_id', $order_not_transaction);
                }
                $datum = $stat->Monthly();
                $stat_count = $this->getStatCount($dashboard, 'monthly');
            } elseif ($request->flag == 'true' && ($request->month_year == "year" || $request->current == "yearly")) {

                $default = true;
                $title = "Year wise " . $request->label . " and count";
                $x_axis = 'Year';
                $period = [];
                if ($request->label == "sales" || $request->label == "dsales" || $request->label == "dorders" || $request->label == "orders" || $request->label == "discounts" || $request->label == "customers" || $request->label == "avg-order-value")
                {
                $stat_sales = $stat_sales->whereYear('orders_customers.created_at', '=', date('Y'));

                $datanum_dup = $stat->get();
                // dd($datanum_dup);
                $order_not_transaction = array();
                foreach ($datanum_dup as $key => $values) {
                                    
                    $isOrder = Orders::where('id',$values->order_id )->whereNotNull('transaction_id')->first();     
                    if(isset($isOrder)){

                        array_push($order_not_transaction, $values->order_id);

                    }
                }
                $stat = $stat->whereIn('order_id', $order_not_transaction);
                }
                $datum = $stat->Yearly();
                $stat_count = $this->getStatCount($dashboard, 'yearly');
            } elseif ($request->flag == 'true' && ($request->sdate || $request->edate)) {


                $sdate = $request->sdate ? Carbon::parse($request->sdate)->subDay(1) : Carbon::now()->subDay(1)->toDateString();
                $edate = $request->edate ? Carbon::parse($request->edate)->addDay(1) : Carbon::now()->addDay(1)->toDateString();
                $period = [];

                while ($sdate->lte($edate)) {
                    $period[] = $sdate->toDateString();
                    $sdate->addDay();
                }

                $title = ucwords($request->label) . " from " . $request->sdate . " to " . $edate;
                $x_axis = 'Date';

                $datum = $stat
                    ->selectRaw('DATE(created_at) as periodname')
                    ->whereBetween('created_at', [$sdate, $edate]);

                $stat_count = $this->getStatCount($dashboard, [$request->sdate, $request->edate]);
            } else if ($request->flag == "true" && $request->current == "today") {

                $title = "Today's " . $request->label . " and count";
                $x_axis = 'Hours';
                $period = $hours;
                $temp_array = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
                if ($request->label == "sales" || $request->label == "dsales" || $request->label == "dorders" || $request->label == "orders" || $request->label == "discounts" || $request->label == "customers" || $request->label == "avg-order-value")
                {
                    $df = strtotime('today');
                    $start_day =  date("Y-m-d", $df);
                    $dt = strtotime('tomorrow');
                    $end_day = date("Y-m-d", $dt);
                    $stat_sales = $stat_sales->whereBetween('orders_customers.created_at', [$start_day, $end_day]);
                }
                if ($request->label == "sales" || $request->label == "dsales" || $request->label == "dorders" || $request->label == "orders" || $request->label == "discounts" || $request->label == "customers" || $request->label == "avg-order-value")
                {
                    $datanum_dup = $stat->get();
                    // dd($datanum_dup);
                    $order_not_transaction = array();
                    foreach ($datanum_dup as $key => $values) {
                                        
                        $isOrder = Orders::where('id',$values->order_id )->whereNotNull('transaction_id')->first();     
                        if(isset($isOrder)){

                            array_push($order_not_transaction, $values->order_id);

                        }
                    }
                    $stat = $stat->whereIn('order_id', $order_not_transaction);
                }
                $datum = $stat->Today();

                $stat_count = $this->getStatCount($dashboard, "today");
            } elseif ($request->flag == "false" || ($request->flag == "true" && $request->current == "last_7_days")) {
                // last week data
                if ($request->label == "sales" || $request->label == "dsales" || $request->label == "dorders" || $request->label == "orders" || $request->label == "discounts" || $request->label == "customers" || $request->label == "avg-order-value")
                {
                    $datanum_dup = $stat->get();
                    // dd($datanum_dup);
                    $order_not_transaction = array();
                    foreach ($datanum_dup as $key => $values) {
                                        
                        // $isOrder = ordersCustomer::Join('orders', function ($join) {
                        //     $join->on('orders.id', '=', 'orders_customers.order_id');
                        // })->join('transactions',function ($join){
                        //     $join->on('orders.transaction_id', '=', 'transactions.id');
                        // })->select('orders_customers.*')->where('orders_customers.order_id',$values->order_id)
                        // ->first();
                        $isOrder = Orders::join('transactions',function ($join){
                            $join->on('orders.transaction_id', '=', 'transactions.id');
                        })->where('orders.id',$values->order_id )->first();     
                        if(isset($isOrder)){
                            array_push($order_not_transaction, $values->order_id);

                        }
                    }

                    $stat = $stat->whereIn('order_id', $order_not_transaction);
                }
                // $datanum_dup1 = $stat->get();
                // dd(count($datanum_dup1));

                $sdate = Carbon::tomorrow()->subdays(7);
                $edate = Carbon::tomorrow()->format('Y-m-d');
                $temp_array = [0, 0, 0, 0, 0, 0, 0];
                $period = [];

                while ($sdate->lte($edate)) {
                    $period[] = $sdate->toDateString();
                    $sdate->addDay();
                }


                $title = "Last 7 day's " . $request->label . " and count";
                $x_axis = 'Date';
                if ($request->label == "sales" || $request->label == "dsales" || $request->label == "dorders" || $request->label == "orders" || $request->label == "discounts" || $request->label == "customers" || $request->label == "avg-order-value")
                {
                    $df = strtotime('-7 days');
                    $start_week =  date("Y-m-d", $df);
                    $dt = strtotime('tomorrow');
                    $end_week = date("Y-m-d", $dt); 
                    $stat_sales = $stat_sales->whereBetween('orders_customers.created_at', [$start_week, $end_week]);
                }
                $datum = $stat->Last7days();
                // dd($datum->count());
                                
                // $date = Carbon::now()->subDays(7);
                // $datum = $stat->where('orders_customers.created_at', '>=', $date);

                $stat_count = $this->getStatCount($dashboard, "last_7_days");
            } else if ($request->flag == "true" && $request->current == "last_30_days") {

                $title = "Last 30 days's " . $request->label . " and count";
                $x_axis = 'Date';
                $temp_array = array_fill(0, 30, 0);
                $period = [];


                $sdate = Carbon::tomorrow()->subdays(30);
                $edate = Carbon::tomorrow()->format('Y-m-d');

                while ($sdate->lte($edate)) {
                    $period[] = $sdate->toDateString();
                    $sdate->addDay();
                }
                if ($request->label == "sales" || $request->label == "dsales" || $request->label == "dorders" || $request->label == "orders" || $request->label == "discounts" || $request->label == "customers" || $request->label == "avg-order-value")
                {
                    $df = strtotime('-30 days');
                    $start_month =  date("Y-m-d", $df);
                    $dt = strtotime('tomorrow');
                    $end_month = date("Y-m-d", $dt);
                    $stat_sales = $stat_sales->whereBetween('orders_customers.created_at', [$start_month, $end_month]);
                }
                if ($request->label == "sales" || $request->label == "dsales" || $request->label == "dorders" || $request->label == "orders" || $request->label == "discounts" || $request->label == "customers" || $request->label == "avg-order-value")
                {
                $datanum_dup = $stat->get();
                // dd($datanum_dup);
                $order_not_transaction = array();
                foreach ($datanum_dup as $key => $values) {
                                    
                    $isOrder = Orders::where('id',$values->order_id )->whereNotNull('transaction_id')->first();     
                    if(isset($isOrder)){

                        array_push($order_not_transaction, $values->order_id);

                    }
                }
                $stat = $stat->whereIn('order_id', $order_not_transaction);
                }
                $datum = $stat->Last30days();
                $stat_count = $this->getStatCount($dashboard, "last_30_days");
            } else if ($request->flag == "true" && $request->current == "thismonth") {

                $title = "This month's " . $request->label . " and count";
                $x_axis = 'Date';
                $period = [];

                $month = Carbon::now();
                $start = Carbon::parse($month)->startOfMonth();
                $end = Carbon::parse($month)->endOfMonth();

                while ($start->lte($end)) {
                    $period[] = $start->toDateString();
                    $start->addDay();
                }

                $temp_array = array_fill(0, count($period), 0);
                if ($request->label == "sales" || $request->label == "dsales" || $request->label == "dorders" || $request->label == "orders" || $request->label == "discounts" || $request->label == "customers" || $request->label == "avg-order-value")
                {
                $datanum_dup = $stat->get();
                // dd($datanum_dup);
                $order_not_transaction = array();
                foreach ($datanum_dup as $key => $values) {
                                    
                    $isOrder = Orders::where('id',$values->order_id )->whereNotNull('transaction_id')->first();     
                    if(isset($isOrder)){

                        array_push($order_not_transaction, $values->order_id);

                    }
                }
                $stat = $stat->whereIn('order_id', $order_not_transaction);
                }
                if ($request->label == "sales" || $request->label == "dsales" || $request->label == "dorders" || $request->label == "orders" || $request->label == "discounts" || $request->label == "customers" || $request->label == "avg-order-value")
                {
                $stat_sales = $stat_sales->whereMonth('orders_customers.created_at', '=', date('m'));
                }
                $datum = $stat->Thismonth();

                $stat_count = $this->getStatCount($dashboard, "thismonth");
            } else if ($request->flag == "true" && $request->current == "yesterday") {
                $title = "Yesterday's " . $request->label . " and count";
                $x_axis = 'Hour';
                $period = $hours;
                $temp_array = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

                if ($request->label == "sales" || $request->label == "dsales" || $request->label == "dorders" || $request->label == "orders" || $request->label == "discounts" || $request->label == "customers" || $request->label == "avg-order-value")
                {
                $df = strtotime('yesterday');
                $start_day =  date("Y-m-d", $df);
                $dt = strtotime('today');
                $end_day = date("Y-m-d", $dt);
                $stat_sales = $stat_sales->whereBetween('orders_customers.created_at', [$start_day, $end_day]);
                }
                if ($request->label == "sales" || $request->label == "dsales" || $request->label == "dorders" || $request->label == "orders" || $request->label == "discounts" || $request->label == "customers" || $request->label == "avg-order-value")
                {
                $datanum_dup = $stat->get();
                // dd($datanum_dup);
                $order_not_transaction = array();
                foreach ($datanum_dup as $key => $values) {
                                    
                    $isOrder = Orders::where('id',$values->order_id )->whereNotNull('transaction_id')->first();     
                    if(isset($isOrder)){

                        array_push($order_not_transaction, $values->order_id);

                    }
                }
                $stat = $stat->whereIn('order_id', $order_not_transaction);
                }
                $datum = $stat->Yesterday();

                $stat_count = $this->getStatCount($dashboard, "yesterday");
            }

            $data = [];
            $y_axis = "Count";
            $datum2 = [];
            // $stat_sale = $stat_sales->get();
            // dd($stat_sale);
            if ($request->label == "customers") {

                $datum = $datum
                    ->groupBy(['email', 'mobileno', 'periodname'])
                    ->selectRaw("sum(json_unquote(json_extract(`meta_data`, '$." . "product_total_price" . "'))) as total_amt ")
                    ->get()->groupBy('periodname');
                // dd($datum);

                if (!empty($datum)) {
                    foreach ($datum as $key => $values) {
                        $temp = 0;
                        // $count = 0;
                        foreach ($values as $value) {
                            $temp = $temp + $value->total_amt;
                            // $count = $count + $value->count;
                        }
                        $datum2[$key] = array(array('count' => count($datum[$key]), 'total_amt' => $temp));
                    }
                    $datum = $datum2;
                }
            } elseif ($request->label == "orders") {
                
                // transaction remove logic 
                
                $datum = $datum
                    ->groupBy('periodname')
                    ->selectRaw("count(*) as count ,sum(json_unquote(json_extract(`meta_data`, '$." . "product_total_price" . "'))) as total_amt ")
                    ->get()
                    ->groupBy('periodname');
                // dd($datum->toArray());
            } elseif ($request->label == "sales" || $request->label == "avg-order-value" || $request->label == "discounts") {
                $operation = 'sum';
                $column = "total_price";
                if ($request->label == "avg-order-value") {
                    $operation = 'avg';
                    $y_axis = "Average order value";
                } else if ($request->label == "discounts") {
                    $column = "product_total_discount";
                    $y_axis = "Discount amount";
                } elseif ($request->label == "sales") {
                    $y_axis = "Amount";
                }
               
            
                
                $datum = $datum->groupBy('periodname')
                    ->selectRaw($operation . "(json_unquote(json_extract(`meta_data`, '$." . $column . "'))) as total_amt ")
                    ->get()->groupBy('periodname');
            } elseif ($request->label == "catalog-views" || $request->label =="videocatalog-views" || $request->label  == "product-views" || $request->label  == "store-views" || $request->label  == "story-views") {

                // $view_datum = clone $datum;
                if ($request->label == "catalog-views")
                    $name = 'catalog';
                elseif($request->label =="videocatalog-views")
                $name = 'video catalog';
                elseif ($request->label == "product-views")
                    $name = 'product details';
                elseif ($request->label == "store-views")
                    $name = 'store';
                elseif ($request->label == "story-views")
                    $name = 'story view';

                $datum = $datum->where('name', $name);

                $datum = $datum->groupBy('periodname')
                    ->selectRaw("count(*) as count")
                    ->get()->groupBy('periodname');
            } elseif ($request->label == "items-sold") {
                $datum = $datum->groupBy('periodname')->selectRaw('sum(qty) as count')->get()->groupBy('periodname');
            }

            if ($default) {
                if ($request->label == "customers")
                    $period = array_keys($datum);
                else
                    $period = array_keys($datum->toArray());
                $temp_array = array_fill(0, count($period), 0);
                $default = false;
            }

            // dd($temp_array,$datum,$period);
            foreach ($datum as $key => $values) {
                $index = array_search($key, $period);
                foreach ($values as $value) {
                    if ($request->label == "sales" || $request->label == "discounts" || $request->label == "avg-order-value")
                        $temp_array[$index] = $value["total_amt"];
                    else {
                        $temp_array[$index] = $value["count"];
                        $total_price[$index] = $value["total_amt"];
                    }
                }
                // dd($datum, $period, $temp_array);
            }
            
            $data = $temp_array;
            // dd($data);
            return [
                "metrics" => $stat_count,
                "icustomer" =>  $request->label,
                "axes" => [$x_axis, $y_axis],
                "title" => $title,
                "labels" => $period,
                "total_price" => $total_price,
                "data" => $data,
                "label" => '',
                'bgcolour' => array_fill(0, count($period), 'rgba(54, 162, 235, 0.7)'),
                'bordercolour' => array_fill(0, count($period), 'rgb(54, 162, 235)'),
            ];
        }
    }

    public function customerStat(Request $request)
    {
        if ($request->ajax()) {
            $customer = json_decode($request->q);
            $datum = null;
            $y_axis = "";
            $filtername = '';

            if ($request->filter == "amount") {
                $datum = ordersCustomer::select('fname', 'meta_data->product_total_price as total_price', 'created_at as periodname')
                    ->where([['email', $customer->email], ['mobileno', $customer->mobileno]])->orderBy('created_at', 'desc')->get();

                $filtername = 'total_price';
                $y_axis = "Amount";
            } else if ($request->filter == "no_of_products") {

                $filtername = 'no_of_products';
                $y_axis = "No. of Products";

                $datum = ordersCustomer::select('fname', 'order_id', 'created_at as periodname')
                    ->where([['email', $customer->email], ['mobileno', $customer->mobileno]])->with('orderProducts')->orderBy('created_at', 'desc')->get();

                foreach ($datum as $key => $data) {
                    $datum[$key]['no_of_products'] = $data->orderProducts->count();
                }
            } else if ($request->filter == "no_of_orders") {

                $filtername = 'no_of_orders';
                $y_axis = "No. of Orders";

                $datum = ordersCustomer::select('fname', 'order_id')->selectRaw('date(created_at) as periodname')
                    ->where([['email', $customer->email], ['mobileno', $customer->mobileno]])->with('order')
                    ->groupBy('periodname')->selectRaw('count(order_id) as no_of_orders')
                    ->orderBy('created_at', 'desc')->get();
            }

            if ($datum) {
                // dd($datum->toArray(),$filtername);
                $period = array_keys($datum->groupBy('periodname')->toArray());
                $label = [];
                $data = [];
                $x_axis = "Date";
                $title = ucfirst($datum[0]->fname) . "'s order details";
                $total_price = 0;

                foreach ($datum as $key => $values) {
                    $index = array_search($values->periodname, $period);
                    $data[$index] = $values->$filtername;
                    if ($request->filter == "amount")
                        $total_price = $total_price + (int)$values->total_price;
                }
            }

            return [
                "icustomer" => "filter",
                "uniquevisitor" => null,
                "axes" => [$x_axis, $y_axis],
                "title" => $title,
                "labels" => $period,
                "total_price" =>  null,
                "data" => $data,
                "label" => $customer->email,
                'bgcolour' => array_fill(0, count($period), 'rgba(54, 162, 235, 0.7)'),
                'bordercolour' => array_fill(0, count($period), 'rgb(54, 162, 235)'),
            ];
        }
    }

    public function ordersStat($label)
    {
        $team_id = auth()->user()->team_id;
        $stat = ordersCustomer::where('team_id', $team_id);
        $temp_order = null;
        $temp_revenue = null;
        $period = [];

        if ($label == "hour-order") {

            $stat = $stat->selectRaw("Hour(created_at) as periodname , id, json_unquote(json_extract(`meta_data`, '$.product_total_price'))")
                ->where('created_at', '>=', Carbon::now()->subDay()->toDateTimeString());

            $period = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23'];
            $temp_order = array_fill(0, count($period), 0);
            $temp_revenue = array_fill(0, count($period), 0);
        } elseif ($label == "day-order") {

            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);

            $stat = $stat->selectRaw(" DAYNAME(created_at) as periodname , id, json_unquote(json_extract(`meta_data`, '$.product_total_price'))")
                ->whereBetween('created_at', [$start_week, $end_week]);

            $period = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
            $temp_order = array_fill(0, count($period), 0);
            $temp_revenue = array_fill(0, count($period), 0);
        }

        $stat = $stat->groupBy('periodname')
            ->selectRaw("sum(json_unquote(json_extract(`meta_data`, '$.product_total_price'))) as total_price, count(id) as count")->get()->groupBy('periodname');

        $data = [];

        foreach ($stat as $key => $values) {
            $index = array_search($key, $period);
            foreach ($values as $value) {
                $temp_revenue[$index] = $value['total_price'];
                $temp_order[$index] = $value["count"];
            }
        }

        array_push($data, $temp_order);
        array_push($data, $temp_revenue);

        return [
            "chart" => $label,
            "axes" => ['Orders count', 'Revenue'],
            "labels" => $period,
            "data" => $data,
            "label" => ['Orders count', 'Revenue'],
            'bgcolour' => ['rgba(46,204,113,0.7 )', 'rgba(54, 162, 235, 0.7)'],
            'bordercolour' => ['rgb(46,204,113)', 'rgb(54, 162, 235)'],
        ];
    }

    //biz-insights analytics count
    public function getStatCount($flag, $filter)
    {

        // dd($flag, $filter);
        $team_id = auth()->user()->team_id;

        $stat_event = Events::where('team_id', $team_id)->select('name');
        $stat_sales = ordersCustomer::Join('orders', function ($join) {
            $join->on('orders.id', '=', 'orders_customers.order_id');
        })->join('transactions',function ($join){
            $join->on('orders.transaction_id', '=', 'transactions.id');
        })->where('orders_customers.team_id', $team_id);
        $stat_product = ordersProduct::where('team_id', $team_id);


        if (!is_array($filter) && $filter == "monthly") {

            $stat_event = $stat_event->Monthly();
            $stat_sales = $stat_sales->whereYear('orders_customers.created_at', '=', date('Y'));
            if ($flag)
                $stat_product = $stat_product->Monthly();
        } elseif (!is_array($filter) && $filter == "yearly") {

            $stat_event = $stat_event->Yearly();
            $stat_sales = $stat_sales->whereYear('orders_customers.created_at', '=', date('Y'));

            if ($flag)
                $stat_product = $stat_product->Yearly();
        } elseif (is_array($filter)) {


            $sdate = $filter[0] ? Carbon::parse($filter[0])->subDay(1) : Carbon::now()->subDay(1)->toDateString();
            $edate = $filter[1] ? Carbon::parse($filter[1])->addDay(1) : Carbon::now()->addDay(1)->toDateString();
            $stat_event = $stat_event->selectRaw('DATE(created_at) as periodname')->whereBetween('created_at', [$sdate, $edate]);
            $stat_sales = $stat_sales->selectRaw('DATE(created_at) as periodname')->whereBetween('created_at', [$sdate, $edate]);
            if ($flag)
                $stat_product = $stat_product->selectRaw('DATE(created_at) as periodname')->whereBetween('created_at', [$sdate, $edate]);
        } else if (!is_array($filter) && $filter == "today") {

            $stat_event = $stat_event->Today();
            $df = strtotime('today');
            $start_day =  date("Y-m-d", $df);
            $dt = strtotime('tomorrow');
            $end_day = date("Y-m-d", $dt);
            $stat_sales = $stat_sales->whereBetween('orders_customers.created_at', [$start_day, $end_day]);
            if ($flag)
                $stat_product = $stat_product->Today();
        } elseif (!is_array($filter) && $filter == "last_7_days") {
            $stat_event = $stat_event->Last7days();
            $df = strtotime('-7 days');
            $start_week =  date("Y-m-d", $df);
            $dt = strtotime('tomorrow');
            $end_week = date("Y-m-d", $dt); 
            $stat_sales = $stat_sales->whereBetween('orders_customers.created_at', [$start_week, $end_week]);

            if ($flag)
                $stat_product = $stat_product->Last7days();

        } else if (!is_array($filter) && $filter == "last_30_days") {

            $stat_event = $stat_event->Last30days();
            $df = strtotime('-30 days');
            $start_month =  date("Y-m-d", $df);
           $dt = strtotime('tomorrow');
           $end_month = date("Y-m-d", $dt);
            $stat_sales = $stat_sales->whereBetween('orders_customers.created_at', [$start_month, $end_month]);

            if ($flag)
                $stat_product = $stat_product->Last30days();

        } else if (!is_array($filter) && $filter == "thismonth") {

            $stat_event = $stat_event->Thismonth();
            $stat_sales = $stat_sales->whereMonth('orders_customers.created_at', '=', date('m'));

            if ($flag)
                $stat_product = $stat_product->Thismonth();

        } else if (!is_array($filter) && $filter == "yesterday") {

            $stat_event = $stat_event->Yesterday();
            $df = strtotime('yesterday');
            $start_day =  date("Y-m-d", $df);
            $dt = strtotime('today');
            $end_day = date("Y-m-d", $dt);
            $stat_sales = $stat_sales->whereBetween('orders_customers.created_at', [$start_day, $end_day]);

            if ($flag)
                $stat_product = $stat_product->Yesterday();
                
        }

        $prod_view = null;
        $catalog_view = null;
        $store_view = null;
        $total_sales = null;
        $orders_count = null;
        $total_discount = null;
        $no_of_customers = null;
        $no_of_Items = null;
        $story_view = null;

        $table_customer = null;
        $table_product = null;


        //sales

        //orders
        $temp_stat_sales = clone $stat_sales;
        $sales = $temp_stat_sales->selectRaw("count(*) as count ,sum(amount) as total_price ")->get();
        $total_sales = $sales->toArray()[0]["total_price"];
        $orders_count = $sales->toArray()[0]["count"];



        if ($flag) {

            //store views
            $views = $stat_event->groupBy('name')->selectRaw('count(*) as count')->get()->groupBy('name')->toArray();
            $store_view = array_key_exists('store', $views) ? $views["store"][0]['count'] : 0;
            $story_view = array_key_exists('story view', $views) ? $views["story view"][0]['count'] : 0;

            //discount
            $temp_stat_sales = clone $stat_sales;
            // $sales = $temp_stat_sales->selectRaw("sum(json_unquote(json_extract(`orders_customers.meta_data`, '$." . "product_total_discount" . "'))) as total_discount ")->get();
            // $sales = $temp_stat_sales->selectRaw("sum(amount) as total_discount ")->get();

            // $total_discount = $sales->toArray()[0]["total_discount"];

            //no of customers 
            $temp_stat_customer = clone $stat_sales;
            $temp_stat_customer = $temp_stat_customer->groupBy(['email', 'mobileno']);
            $no_of_customers = $temp_stat_customer->get()->count();

            //no of items sold
            $no_of_Items = clone $stat_product;
            $no_of_Items = $no_of_Items->selectRaw('sum(qty) as total_qty')->get();
            $no_of_Items = $no_of_Items->toArray()[0]['total_qty'];

            //conversion

            //top selling product
            $top_product = $stat_product->select('price', 'product_id', 'qty', 'name', 'sku')
                ->groupBy('product_id')
                ->selectRaw('sum(price) as Revenue, sum(qty) as Orders , name , sku')
                ->orderBy('Orders', 'desc')->limit(5)->get();

            $table_product = DataTables::of($top_product);
            $table_product->addColumn('Product Info', '&nbsp;', 0);
            $table_product->editColumn('Product Info', function ($row) {
                return $row->name . '-' . $row->sku;
            });
            $table_product->removeColumn(['name', 'sku', 'product_id', 'qty', 'price', 'product']);

            $table_product = $table_product->make(true);

            // dd($table_product);



            // top purchasing customers
            $top_customers = $stat_sales->select('fname', 'lname', 'email', 'mobileno')->selectRaw("amount as Revenue")
                ->groupBy(['email', 'mobileno'])->selectRaw('count(*) as Orders')
                ->orderBy('Revenue', 'desc')->limit(5)->get();

            $table_customer = Datatables::of($top_customers);
            $table_customer->addColumn('Customer Info', '&nbsp;', 0);

            $table_customer->editColumn('Customer Info', function ($row) {
                return $row->fname . ' ' . $row->lname . ' - ' . $row->mobileno . ' - ' . $row->email;
            });
            $table_customer->removeColumn(['fname', 'lname', 'email', 'mobileno']);

            $table_customer = $table_customer->make(true);
            // dd($table_customer);
        } else {
            //product views
            //catalog views
            //story view

            $views = $stat_event->groupBy('name')->selectRaw('count(*) as count')->get()->groupBy('name')->toArray();
            $prod_view = array_key_exists('product details', $views) ? $views["product details"][0]['count'] : 0;
            $catalog_view = array_key_exists('catalog', $views) ? $views["catalog"][0]['count'] : 0;
            $videocatalog_view = array_key_exists('video catalog', $views) ? $views["video catalog"][0]['count'] : 0;
        }


        if (!$flag) {
            return [
                "totalSales" => $total_sales,
                "ordersCount" => $orders_count,
                "catalogView" =>  $catalog_view,
                'prodView' => $prod_view,
                "videocatalogview"=> $videocatalog_view,
            ];
        } else {
            return [
                "totalSales" => $total_sales,
                "ordersCount" => $orders_count,
                'storeView' => $store_view,
                'totalDiscount' => $total_discount,
                'no_of_Customers' => $no_of_customers,
                'no_of_Items' => $no_of_Items ?? 0,
                'top_customers' => $table_customer,
                'top_products' => $table_product,
                'story_view' => $story_view,
            ];
        }
    }
}
