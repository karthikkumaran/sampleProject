<?php

namespace App\Http\Controllers\Admin\Analytics;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;
use App\Campaign;
use App\Models\Statistics\Events;
use App\UserInfo;

class AnalyticsController extends Controller
{
    public function EventsAjax(Request $request)
    {
        // return($request->all());
        if ($request->ajax()) {

            $shareableLink = ShareableLink::where('uuid', $request->slink)->first();
            $shareable = '';
            $campaign_id = "";

            if ($shareableLink == null) {

                $subdomain = substr($request->slink, 0, strpos($request->slink, '.'));
                $subdomain = str_replace("www.", '', $subdomain);
                $subdomain = str_replace("www", '', $subdomain);
                $shareable = UserInfo::where('customdomain', 'LIKE', '%' . $subdomain . '%')->first();

                if ($shareable == null) {
                    $shareable = UserInfo::where('subdomain', 'LIKE', '%' . $subdomain . '%')->first();
                }
            } else {
                $shareable = $shareableLink->shareable;
            }
            // dd($shareable);
            $team_id = $shareable->user->team_id;

            // dd($team_id);
            if ($shareable->table == "campaigns") {

                $campaign_id = $shareable->id;
            } else {
                $user_id = $shareable->user_id;
                $campaign_id = Campaign::where('user_id', $user_id)->first();
                if(!empty($campaign_id))
                    $campaign_id = $campaign_id->id;
            }

            $event_name = "";
            $name = "";

            if ($request->event == "checkout_page" || $request->event == "confirm_order" ||  $request->event == "catalog" || $request->event == "story_view") {
                $event_name = $request->event . '_' . $campaign_id;
            } else if ($request->event == "store") {
                $event_name = $request->event . '_' . $team_id;
            } else {
                $event_name = $request->event . '_' . $campaign_id . '_' . $request->product_id;
                // $campaign_id = null;
            }
            $name = preg_replace('/[\d_]+/', ' ', $request->event);

            // dd($name,$campaign_id);
            $event = [
                'name' => $name,
                'event_name' => $event_name,
                'team_id' => $team_id,
                'campaign_id' => $campaign_id,
                'product_id' =>  $request->product_id == "null" ? NULL : $request->product_id,
            ];

            $track_event = Events::Create($event);

            // dd($track_event);
        }
    }
}
