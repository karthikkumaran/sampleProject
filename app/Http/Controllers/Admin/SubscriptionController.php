<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\LaravelSubscriptions\Plan;
use App\LaravelSubscriptions\PlanFeature;
use App\LaravelSubscriptions\PlanSubscription;
use App\LaravelSubscriptions\PlanSubscriptionUsage;
use App\LaravelSubscriptions\PlanSubscriptionFeatures;
use App\LaravelSubscriptions\PlanSubscribedFeatures;
use App\Http\Controllers\Traits\CommonFunctionsTrait;
use App\Jobs\DowngradeSubscriptionJob;
use Carbon\Carbon;
use App\User;
use Session;
use App\Product;
use App\Campaign;
use App\UserInfo;
use Razorpay\Api\Api;
use Illuminate\Support\Str;
use App\SubscriptionPayment;
use App\Models\PlanInvoice;
use Rinvex\Subscriptions\Services\Period;
use Illuminate\Support\Facades\Schema;

class SubscriptionController extends Controller
{
    use CommonFunctionsTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $packages = Plan::get();
        $user = auth()->user();
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $user->id)->first();
        }

        return view('admin.subscriptions.index', compact('packages', 'subscription'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Plan $plan)
    {
        $user = auth()->user();
        $plan = Plan::find($request->plan_id);
        $api = new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));
        $payment_metadata  = $api->payment->fetch($request->rzp_paymentid);
        if ($request->status == "success") {
            try {
                $attributes  = array("razorpay_signature"  => $request->rzp_signature,  "razorpay_payment_id"  => $request->rzp_paymentid,  "razorpay_order_id" => $request->rzp_orderid);
                $order = $api->utility->verifyPaymentSignature($attributes);
                if ($order == null) {
                    $flag = true;
                } else {
                    $flag = false;
                }
            } catch (\Exception $e) {
                $flag = false;
            }

            if ($flag == true) {
                if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
                    $trial_subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
                }
                else{
                    $trial_subscription = PlanSubscription::where('user_id', $user->id)->first();
                }
                if (!empty($trial_subscription)) {
                    $trial_subscription->forceDelete();
                }
                $user->newSubscription($plan->name, $plan);
                $user->subscription_status = 1;
                $user->save();
                if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
                    $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
                }
                else{
                    $subscription = PlanSubscription::where('user_id', $user->id)->first();
                }
                $now = Carbon::now();
                $features = $plan->features;
                $subscribed_features = [];
                foreach ($features as $feature) {
                    $subscribed_features[] = [
                        'user_id' => $user->id,
                        'plan_id' => $plan->id,
                        'subscription_id' => $subscription->id,
                        'feature_id' => $feature->feature_id,
                        'value' => $feature->value,
                        'resettable_period' => $feature->resettable_period,
                        'resettable_interval' => $feature->resettable_interval,
                        'is_approved' => 1,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ];
                }
                PlanSubscribedFeatures::insert($subscribed_features);

                $campaigns = Campaign::where('user_id', $user->id)->orderBy('is_start', 'desc')->orderBy('id', 'desc')->get();
                $campaigns_count = $campaigns->count();
                if ($campaigns_count > 0) {
                    PlanSubscriptionUsage::create([
                        'subscription_id' => $subscription->id,
                        'feature_id' => 1,
                        'used' => $campaigns_count,
                    ]);
                }

                $products = Product::where('team_id', $user->team_id)->get();
                $products_count = $products->count();
                if ($products_count > 0) {
                    PlanSubscriptionUsage::create([
                        'subscription_id' => $subscription->id,
                        'feature_id' => 2,
                        'used' => $products_count,
                    ]);
                }
                if($plan->discount != 0 && $plan->discount < $plan->price)
                {
                $planDiscount = $plan->price - $plan->discount;
                }
                else{
                    $planDiscount = $plan->price;  
                }

                $subscription_payment = SubscriptionPayment::create([
                    'user_id' => $user->id,
                    'plan_name' => $plan->name,
                    'plan_price' => $planDiscount,
                    'plan_start' => $user->subscription($subscription->slug)->starts_at,
                    'plan_expiry' => $user->subscription($subscription->slug)->ends_at,
                    'payment_id' => $request->rzp_paymentid,
                    'payment_mode' => "razor_pay",
                    'payment_method' => $payment_metadata['method'],
                    'status' => $request->status,
                    'meta_data' => serialize($payment_metadata),
                ]);
                $now = carbon::now()->format('Y-m-d H:i:s');
                $inv_id = PlanInvoice::whereNotNull('inv_num')->count();
                    if(!empty($inv_id)){
                        $inv_num = $inv_id + 1;
                    }
                    else{
                        $inv_num = 1;
                    }
                $plan_invoice = PlanInvoice::create([
                    'subscription_payment_id' => $subscription_payment->id,
                    'user_id' => $user->id,
                    'plan_name' => $plan->name,
                    'plan_start' => $user->subscription($subscription->slug)->starts_at,
                    'plan_expiry' => $user->subscription($subscription->slug)->ends_at,
                    'inv_date' => $now,
                    'inv_num' => $inv_num
                ]);
                Session::flash('payment_successful', 'success');
                
                Session::forget('subscription_check');
                Session::push('subscription_check', ['no_subscription' => false, 'trial_subscription' => false, 'trial_duration' => false,'subscription_expiry_alert' => false, 'subscription_expired' => false, 'grace_period' => false]);
                $this->setSubscribedFeaturesSession($user);

                if ($user->isFirstTimeUser()) {
                    return route('admin.campaigns.storeIndex');
                }

                return route('admin.subscription.myPlan');
            } else {
                // return redirect()->route('admin.subscriptions.index');
            }
        } else {
            $subscription_payment = SubscriptionPayment::create([
                'user_id' => $user->id,
                'plan_name' => $plan->name,
                'plan_price' => $planDiscount,
                'payment_id' => $request->rzp_paymentid,
                'payment_mode' => "razor_pay",
                'payment_method' => $payment_metadata['method'],
                'status' => $request->status,
                'meta_data' => serialize($payment_metadata),
            ]);
            $now = carbon::now()->format('Y-m-d H:i:s');
            $inv_id = PlanInvoice::whereNotNull('inv_num')->count();
                    if(!empty($inv_id)){
                        $inv_num = $inv_id + 1;
                    }
                    else{
                        $inv_num = 1;
                    }
            $plan_invoice = PlanInvoice::create([
                'subscription_payment_id' => $subscription_payment->id,
                'user_id' => $user->id,
                'plan_name' => $plan->name,
                'inv_date' => $now,
                'inv_num' => $inv_num
            ]);
            // return redirect()->route('admin.subscriptions.index');
        }
    }

    public function upgradePlan(Request $request)
    {
        $user = auth()->user();
        $plan = Plan::find($request->plan_id);
        $api = new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));
        $payment_metadata  = $api->payment->fetch($request->rzp_paymentid);
        if ($request->status == "success") {
            try {
                $attributes  = array("razorpay_signature"  => $request->rzp_signature,  "razorpay_payment_id"  => $request->rzp_paymentid,  "razorpay_order_id" => $request->rzp_orderid);
                $order = $api->utility->verifyPaymentSignature($attributes);
                if ($order == null) {
                    $flag = true;
                } else {
                    $flag = false;
                }
            } catch (\Exception $e) {
                $flag = false;
            }
            if ($flag == true) {
                if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
                    $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
                }
                else{
                    $subscription = PlanSubscription::where('user_id', $user->id)->first();
                }
                $features = $plan->features;
                foreach ($features as $feature) {
                    $plan_subscribed_feature = PlanSubscribedFeatures::where(['user_id' => $user->id, 'plan_id' => $subscription->plan_id, 'feature_id' => $feature->feature_id])->first();
                    if ($plan_subscribed_feature) {
                        $plan_subscribed_feature->plan_id = $plan->id;
                        $plan_subscribed_feature->value = $feature->value;
                        $plan_subscribed_feature->is_add_on = 0;
                        $plan_subscribed_feature->add_on_value = null;
                        $plan_subscribed_feature->save();
                    } else {
                        $plan_subscribed_features = new PlanSubscribedFeatures;
                        $plan_subscribed_features->create([
                            'user_id' => $user->id,
                            'subscription_id' => $subscription->id,
                            'plan_id' => $plan->id,
                            'feature_id' => $feature->feature_id,
                            'value' => $feature->value,
                            'is_approved' => 1,
                        ]);
                    }
                }
                $subscription->changePlan($plan);
                $campaigns = Campaign::where('user_id', $user->id)->orderBy('is_start', 'desc')->orderBy('id', 'desc')->get();
                $available_catalogs = $campaigns->count();
                if ($available_catalogs > 0) {
                    $campaign_count = 0;
                    $catalogs_usage_count = 0;
                    $campaign_value = $user->subscription($subscription->slug)->getFeatureValue('catalogs');
                    if ($available_catalogs > $campaign_value) {
                        foreach ($campaigns as $campaign) {
                            $campaign_count = $campaign_count + 1;
                            if ($campaign_count > $campaign_value) {
                                $campaign->is_start = 0;
                                $campaign->is_added_home = 0;
                                $campaign->downgradeable = 1;
                                $campaign->save();
                            } else {
                                $catalogs_usage_count = $campaign_count;
                                $campaign->is_start = 1;
                                $campaign->is_added_home = 1;
                                $campaign->downgradeable = 0;
                                $campaign->save();
                            }
                        }
                    } else {
                        $catalogs_usage_count = $available_catalogs;
                    }

                    $catalogs_usage = PlanSubscriptionUsage::where(['subscription_id' => $subscription->id, 'feature_id' => 1])->first();
                    if (!empty($catalogs_usage)) {
                        $catalogs_usage->used = $catalogs_usage_count;
                        $catalogs_usage->save();
                    }
                }

                $products = Product::where('team_id', $user->team_id)->get();
                $available_products = $products->count();
                if ($available_products > 0) {
                    $product_count = 0;
                    $products_usage_count = 0;
                    $product_value = $user->subscription($subscription->slug)->getFeatureValue('products');
                    if ($available_products > $product_value) {
                        foreach ($products as $product) {
                            $product_count = $product_count + 1;
                            if ($product_count > $product_value) {
                                $product->downgradeable = 1;
                                $product->save();
                            } else {
                                $products_usage_count = $product_count;
                                $product->downgradeable = 0;
                                $product->save();
                            }
                        }
                    } else {
                        foreach ($products as $product) {
                            $product->downgradeable = null;
                            $product->save();
                        }
                        $products_usage_count = $available_products;
                    }

                    $products_usage = PlanSubscriptionUsage::where(['subscription_id' => $subscription->id, 'feature_id' => 2])->first();
                    if (!empty($products_usage)) {
                        $products_usage->used = $products_usage_count;
                        $products_usage->save();
                    }
                }
                if($plan->discount != 0 && $plan->discount < $plan->price)
                {
                $planDiscount = $plan->price - $plan->discount;
                }
                else{
                    $planDiscount = $plan->price;  
                }    
                $subscription_payment = SubscriptionPayment::create([
                    'user_id' => $user->id,
                    'plan_name' => $plan->name,
                    'plan_price' => $planDiscount,
                    'plan_start' => $user->subscription($subscription->slug)->starts_at,
                    'plan_expiry' => $user->subscription($subscription->slug)->ends_at,
                    'payment_id' => $request->rzp_paymentid,
                    'payment_mode' => "razor_pay",
                    'payment_method' => $payment_metadata['method'],
                    'status' => $request->status,
                    'meta_data' => serialize($payment_metadata),
                ]);
                $now = carbon::now()->format('Y-m-d H:i:s');
                $inv_id = PlanInvoice::whereNotNull('inv_num')->count();
                    if(!empty($inv_id)){
                        $inv_num = $inv_id + 1;
                    }
                    else{
                        $inv_num = 1;
                    }
                $plan_invoice = PlanInvoice::create([
                    'subscription_payment_id' => $subscription_payment->id,
                    'user_id' => $user->id,
                    'plan_name' => $plan->name,
                    'plan_start' => $user->subscription($subscription->slug)->starts_at,
                    'plan_expiry' => $user->subscription($subscription->slug)->ends_at,
                    'inv_date' => $now,
                    'inv_num' => $inv_num
                ]);

                Session::flash('payment_successful', 'success');
                Session::forget('subscription_check');
                Session::push('subscription_check', ['no_subscription' => false, 'trial_subscription' => false, 'trial_duration' => false,'subscription_expiry_alert' => false, 'subscription_expired' => false, 'grace_period' => false]);
                $this->setSubscribedFeaturesSession($user);
                
                return route('admin.subscription.myPlan');
            } else {
                // return redirect()->route('admin.subscriptions.index');
            }
        } else {
            $subscription_payment = SubscriptionPayment::create([
                'user_id' => $user->id,
                'plan_name' => $plan->name,
                'plan_price' => $planDiscount,
                'payment_id' => $request->rzp_paymentid,
                'payment_mode' => "razor_pay",
                'payment_method' => $payment_metadata['method'],
                'status' => $request->status,
                'meta_data' => serialize($payment_metadata),
            ]);
            $now = carbon::now()->format('Y-m-d H:i:s');
            $inv_id = PlanInvoice::whereNotNull('inv_num')->count();
                    if(!empty($inv_id)){
                        $inv_num = $inv_id + 1;
                    }
                    else{
                        $inv_num = 1;
                    }
            $plan_invoice = PlanInvoice::create([
                'subscription_payment_id' => $subscription_payment->id,
                'user_id' => $user->id,
                'plan_name' => $plan->name,
                'inv_date' => $now,
                'inv_num' => $inv_num
            ]);
            // return redirect()->route('admin.subscriptions.index');
        }
    }

    public function downgradePlan(Request $request)
    {
        $user = auth()->user();
        $plan = Plan::find($request->plan_id);
        $api = new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));
        $payment_metadata  = $api->payment->fetch($request->rzp_paymentid);
        try {
            $attributes  = array("razorpay_signature"  => $request->rzp_signature,  "razorpay_payment_id"  => $request->rzp_paymentid,  "razorpay_order_id" => $request->rzp_orderid);
            $order  = $api->utility->verifyPaymentSignature($attributes);
            $flag = true;
        } catch (\Exception $e) {
            $flag = false;
        }

        if ($request->status == "success") {
            if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
                $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
            }
            else{
                $subscription = PlanSubscription::where('user_id', $user->id)->first();
            }

            $data['user'] = $user;
            $data['plan'] = $plan;
            $data['subscription'] = $subscription;

            $subscription_end_time = $subscription->ends_at;
            $current_time = Carbon::now();
            $interval = $current_time->diff($subscription_end_time);
            DowngradeSubscriptionJob::dispatch($data)->onQueue('downgradeable')->delay($interval);
            $trial_start = Carbon::now()->add($interval);
            $trial = new Period($plan->trial_interval, $plan->trial_period, $trial_start);
            $period = new Period($plan->invoice_interval, $plan->invoice_period, $trial->getEndDate());

            if($plan->discount != 0 && $plan->discount < $plan->price)
            {
                $planDiscount = $plan->price - $plan->discount;
             }
            else{
                    $planDiscount = $plan->price;  
                }

            $subscription_payment = SubscriptionPayment::create([
                'user_id' => $user->id,
                'plan_name' => $plan->name,
                'plan_price' => $planDiscount,
                'plan_start' => $period->getStartDate(),
                'plan_expiry' => $period->getEndDate(),
                'payment_id' => $request->rzp_paymentid,
                'payment_mode' => "razor_pay",
                'payment_method' => $payment_metadata['method'],
                'status' => $request->status,
                'meta_data' => serialize($payment_metadata),
            ]);
            $now = carbon::now()->format('Y-m-d H:i:s');
            $inv_id = PlanInvoice::whereNotNull('inv_num')->count();
                    if(!empty($inv_id)){
                        $inv_num = $inv_id + 1;
                    }
                    else{
                        $inv_num = 1;
                    }
            $plan_invoice = PlanInvoice::create([
                'subscription_payment_id' => $subscription_payment->id,
                'user_id' => $user->id,
                'plan_name' => $plan->name,
                'plan_start' => $period->getStartDate(),
                'plan_expiry' => $period->getEndDate(),
                'inv_date' => $now,
                'inv_num' => $inv_num
            ]);

            return route('admin.subscriptions.index');
        } else {
            $subscription_payment = SubscriptionPayment::create([
                'user_id' => $user->id,
                'plan_name' => $plan->name,
                'plan_price' => $planDiscount,
                'payment_id' => $request->rzp_paymentid,
                'payment_mode' => "razor_pay",
                'payment_method' => $payment_metadata['method'],
                'status' => $request->status,
                'meta_data' => serialize($payment_metadata),
            ]);
            $now = carbon::now()->format('Y-m-d H:i:s');
            $inv_id = PlanInvoice::whereNotNull('inv_num')->count();
                    if(!empty($inv_id)){
                        $inv_num = $inv_id + 1;
                    }
                    else{
                        $inv_num = 1;
                    }
            $plan_invoice = PlanInvoice::create([
                'subscription_payment_id' => $subscription_payment->id,
                'user_id' => $user->id,
                'plan_name' => $plan->name,
                'inv_date' => $now,
                'inv_num' => $inv_num
            ]);
            return redirect()->route('admin.subscriptions.index');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($subscription, $id)
    {
        $plan = Plan::find($id);
        $subscription = $subscription;

        return view('admin.subscriptions.show', compact('plan', 'subscription'));
    }

    public function newUserShow($plan_id)
    {
        $plan = Plan::find($plan_id);

        return view('admin.subscriptions.new_user_show', compact('plan'));
    }

    public function getPaymentDetails(Request $request)
    {
        $user = auth()->user();
        $userSettings = UserInfo::where("user_id", $user->id)->first();
        $meta_data = json_decode($userSettings->meta_data, true);
        $plan = Plan::find($request->plan_id);
        // dd($plan);
        if($plan->discount != 0 && $plan->discount < $plan->price)
        {
        $planDiscount = $plan->price - $plan->discount;
        }
        else{
            $planDiscount = $plan->price;  
        }   
        $api = new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));
        $receipt_id = Str::random(20);        
        $order  = $api->order->create(array('receipt' => $receipt_id, 'amount' => (int)$planDiscount * 100, 'currency' => $plan->currency)); // Creates order
        $response = [
            'orderId' => $order['id'],
            'razorpayId' => env('RAZORPAY_KEY'),
            'amount' => $planDiscount * 100,
            'name' => $plan->name,
            'currency' => $plan->currency,
            'email' => $meta_data['settings']['emailid'] ?? $user->email,
            'contactNumber' => $meta_data['settings']['contactnumber'] ?? $user->mobile,
            'description' =>  $plan->description,
        ];

        return $response;
    }

    public function myPlan()
    {
        $user = auth()->user();
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            
            $myplan = PlanSubscription::where('subscriber_id', $user->id)->first();
        }
        else{
            
            $myplan = PlanSubscription::where('user_id', $user->id)->first();
        }
        
        $package = Plan::find($myplan->plan_id);


        return view('admin.subscriptions.myplan', compact('myplan', 'package'));
    }

    public function renewSubscription()
    {
        $user = auth()->user();
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $mysubscription = PlanSubscription::where('subscriber_id', $user->id)->first();
        }
        else{
            $mysubscription = PlanSubscription::where('user_id', $user->id)->first();
        }
        $user->subscription($mysubscription->slug)->renew();

        Session::forget('subscription_check');
        Session::push('subscription_check', ['no_subscription' => false, 'trial_subscription' => false, 'trial_duration' => false,'subscription_expiry_alert' => false, 'subscription_expired' => false, 'grace_period' => false]);
        $this->setSubscribedFeaturesSession($user);

        return redirect()->back()->with('message', 'Thank you for renewing your subscription. You can now continue using our product without interruptions.');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
