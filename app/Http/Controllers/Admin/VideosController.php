<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyVideoRequest;
use App\Http\Requests\StoreVideoRequest;
use App\Http\Requests\UpdateVideoRequest;
use App\Video;
use App\Entitycard;
use App\ProductCategory;
use App\Models\Statistics\Events;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;


class VideosController extends Controller
{
    use MediaUploadingTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('video_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Video::with(['team'])->select(sprintf('%s.*', (new Video)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'video_show';
                $editGate      = 'video_edit';
                $deleteGate    = 'video_delete';
                $crudRoutePart = 'videos';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('title', function ($row) {
                return $row->title ? $row->title : "";
            });
            $table->editColumn('description', function ($row) {
                return $row->description ? $row->description : "";
            });
            $table->editColumn('file', function ($row) {
                return $row->file ? '<a href="' . $row->file->getUrl() . '" target="_blank">' . trans('global.downloadFile') . '</a>' : '';
            });
            $table->editColumn('video_url', function ($row) {
                return $row->video_url ? $row->video_url : "";
            });

            $table->rawColumns(['actions', 'placeholder', 'file']);

            return $table->make(true);
        }

        return view('admin.videos.index');
    }

    public function create()
    {
        abort_if(Gate::denies('video_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.videos.create');
    }

    public function store(StoreVideoRequest $request)
    {
        $video = Video::create($request->all());
        if ($request->input('thumbnail', false)) {
            if (!$video->thumbnail || $request->input('thumbnail') !== $video->thumbnail->file_name) {
                $video->addMedia(storage_path('tmp/uploads/' . $request->input('thumbnail')))->toMediaCollection('thumbnail');
            }
        } 
        if(!empty($video) && isset($request->campaign_id))
            Entitycard::create(array('video_id' => $video->id, 'campaign_id' => $request->campaign_id, 'meta_data' => '{}'));

        // if ($request->input('file', false)) {
        //     $video->addMedia(storage_path('tmp/uploads/' . $request->input('file')))->toMediaCollection('file');
        // }

        // if ($media = $request->input('ck-media', false)) {
        //     Media::whereIn('id', $media)->update(['model_id' => $video->id]);
        // }

        // return redirect()->route('admin.videos.index');
        return response()->json(["status" => true, "message" => "Card Added successfully.","video_id" => $video->id,'campaign_id' => $request->campaign_id]);
    }

    public function edit(Video $video)
    {
        abort_if(Gate::denies('video_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $video->load('team','videoEntitycards');
        $campaign = $video->videoEntitycards->campagin;
        $categories = ProductCategory::whereNull('product_category_id')
            ->with('childrenCategories')
            ->get();
            $currency = "₹";

        return view('admin.videos.edit', compact('video','campaign','categories','currency'));
    }

    public function updatevideoproduct()
    {
        
        return view('admin.videos.video_product');
    }

    public function updatevideoname(Request $request)
    {
        Video::find($request->pk)->update(['title' => $request->value]);

        return response()->json(['success' => 'done']);
    }

    public function update(UpdateVideoRequest $request, Video $video)
    {
        $video->update($request->all());

        if ($request->input('thumbnail', false)) {
            if (!$video->thumbnail || $request->input('thumbnail') !== $video->thumbnail->file_name) {
                $video->addMedia(storage_path('tmp/uploads/' . $request->input('thumbnail')))->toMediaCollection('thumbnail');
            }
        } 
        // elseif ($video->thumbnail) {
        //     $video->thumbnail->delete();
        // }
        return redirect()->back();
        // return true;
        // return redirect()->route('admin.videos.index');
        // return redirect()->route('admin.videos.edit')->with(compact($video->id));
    }

    public function removethumbnail(Request $request)
    {
        $mediaId = $request->id;
        $media = Media::find($request->input('id'));
        $model = Video::find($media->model_id);
        // dd($model);

        $model->deleteMedia($media->id);
        // $media->delete();
        return response()->json(['success' => 'done']);
    }


    public function show(Video $video)
    {
        abort_if(Gate::denies('video_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $video->load('team', 'videoEntitycards');

        return view('admin.videos.show', compact('video'));
    }

    public function destroy(Video $video)
    {
        abort_if(Gate::denies('video_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $video->delete();

        return back();
    }

    public function massDestroy(MassDestroyVideoRequest $request)
    {
        Video::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('video_create') && Gate::denies('video_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Video();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media', 'public');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
