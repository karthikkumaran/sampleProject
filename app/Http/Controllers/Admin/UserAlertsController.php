<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use WeStacks\TeleBot\TeleBot;
use App\Telegram_Bot_Subscriber;
use App\Mail\NotificationEmail;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\MassDestroyUserAlertRequest;
use App\Http\Requests\StoreUserAlertRequest;
use App\User;
use App\UserAlert;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class UserAlertsController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('user_alert_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = UserAlert::with(['users'])->select(sprintf('%s.*', (new UserAlert)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'user_alert_show';
                $editGate      = 'user_alert_edit';
                $deleteGate    = 'user_alert_delete';
                $crudRoutePart = 'user-alerts';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('alert_text', function ($row) {
                return $row->alert_text ? $row->alert_text : "";
            });
            $table->editColumn('alert_link', function ($row) {
                return $row->alert_link ? $row->alert_link : "";
            });
            $table->editColumn('user', function ($row) {
                $labels = [];

                foreach ($row->users as $user) {
                    $labels[] = sprintf('<span class="label label-info label-many">%s</span>', $user->name);
                }

                return implode(' ', $labels);
            });

            $table->rawColumns(['actions', 'placeholder', 'user']);

            return $table->make(true);
        }

        return view('admin.userAlerts.index');
    }

    public function create()
    {
        abort_if(Gate::denies('user_alert_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::all()->pluck('name', 'id');

        return view('admin.userAlerts.create', compact('users'));
    }

    public function store(StoreUserAlertRequest $request)
    {
        // dd($request); 
        $message="";
        $userAlert = UserAlert::create($request->all());
        $userAlert->users()->sync($request->input('users', []));
        if($request->alert_pushnotify == true && !empty($request->title) && !empty($request->body))
        {
            
            foreach($request->users as $users)
            {
                $firebaseToken = User::find($users);
                $SERVER_API_KEY = env('FCM_SECRET_KEY');
                
               
                $data = array("to" => $firebaseToken->device_token,"priority"=>'high', "notification" => array( "title" => $request->title,"body" => strip_tags($request->body,''),"icon" => "favicon.png", "click_action" => "https://simplisell.co"));
                    
                $dataString = json_encode($data);
                
                $headers = [
                    'Authorization: key='.$SERVER_API_KEY,
                    'Content-Type: application/json',
                ];
            
                $ch = curl_init();
                curl_setopt_array($ch,array(
                    CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $dataString,
                    CURLOPT_HTTPHEADER => $headers
                ));
                
        
                curl_exec($ch);
                
            }
        }
        if($request->alert_email == true && !empty($request->email_subject) && !empty($request->email_message))
        {
            foreach($request->users as $key){
                $users = User::where('id',$key)->first();
                $data = array();
                $data['users'] = $users;
                $data['subject'] = $request->email_subject;
                $data['message'] = $request->email_message;
                // dd($data);

                Mail::send(new NotificationEmail($data));
                $message = "Notification send Successfully";
            }
        }
        if($request->alert_telegram == true && !empty($request->telegram_message))
        {
            foreach($request->users as $key){
                $users = User::where('id',$key)->first();
                $bot = new TeleBot(env('TELEGRAM_BOT_TOKEN'));
                $telegram_bot_subscribers = Telegram_Bot_Subscriber::where("user_id", $users->id)->get();

                if(!empty($telegram_bot_subscribers)){

                $telegram_text = strip_tags($request->telegram_message,'');
                
                
                    foreach ($telegram_bot_subscribers as $subscriber) {
                        $bot->sendMessage([
                            'chat_id' => $subscriber->telegram_chat_id,
                            'parse_mode' => 'HTML',
                            'text' => $telegram_text,
                        ]);
                    }
                }
            }
        }
        if(!empty($message)){
            return redirect()->route('admin.user-alerts.index')->with('message',$message);
        }
        else{
            return redirect()->route('admin.user-alerts.index'); 
        }
    }

    public function show(UserAlert $userAlert)
    {
        abort_if(Gate::denies('user_alert_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $userAlert->load('users');

        return view('admin.userAlerts.show', compact('userAlert'));
    }

    public function destroy(UserAlert $userAlert)
    {
        abort_if(Gate::denies('user_alert_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $userAlert->delete();

        return back();
    }

    public function massDestroy(MassDestroyUserAlertRequest $request)
    {
        UserAlert::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function read(Request $request)
    {
        $alerts = \Auth::user()->userUserAlerts()->where('read', false)->get();

        foreach ($alerts as $alert) {
            $pivot       = $alert->pivot;
            $pivot->read = true;
            $pivot->save();
        }
    }
}
