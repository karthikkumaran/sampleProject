<?php

namespace App\Http\Controllers\Admin;

use App\Capture;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;

use Webpatser\Uuid\Uuid;
use Carbon\Carbon;

use Illuminate\Support\Facades\Redirect;

use App\Entitycard;
use App\Campaign;
use App\UserInfo;

class CapturesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // cfe4bc2a-62eb-4ab5-aab5-3fb57cbbfdde
        $capture = Capture::where('id',23)->first();
        // dd($capture->photo->getUrl());
        
        // dd($capture->photo->hasGeneratedConversion('thumb'));

        //base 64
//         $path   = $capture->photo->getPath();
// $type   = pathinfo($path, PATHINFO_EXTENSION);
// $data   = file_get_contents($path);
// $base64 = 'data:application/' . $type . ';base64,' . base64_encode($data);
// return response()->json(['type' => $type, 'file' => $base64]);
        
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $capture = false;
        // $user = Auth::user();

        // $user_id = $user->id;
        
        $uuid = (string)Uuid::generate(4);
        $campaign_id = $request->cid;
        $product_id = $request->pid;
        $campaign = Campaign::where('id', $campaign_id)->first();
        // var_dump($uuid,$campaign_id,$product_id,$campaign,$request->photo);die;
        if(!empty($request->photo) && !empty($campaign)){
            $team_id = $campaign->team->id;
            $user_id = $campaign->user->id;

            $photo = $request->photo;
            $capture = Capture::create(array("user_id"=>$user_id,"team_id"=>$team_id,"campaign_id"=>$campaign_id,"product_id"=>$product_id,"uuid"=>$uuid));
            $current_timestamp = Carbon::now()->timestamp."-".Uuid::generate(5,'captures', Uuid::NS_DNS)->string;
            $capture->addMediaFromBase64($photo)->usingFileName($current_timestamp.".png")->toMediaCollection('photo');
        }
         
        return response()->json(route('captures.share',$capture->uuid),200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    } 

    public function share(Request $request) {

        if($request->subdomain == "www")
        return Redirect::to(env('APP_URL', 'localhost'));
    
        if(!empty($request->subdomain)) {
            $data = UserInfo::where('subdomain', $request->subdomain)->first();
            if(empty($data)){
                if (empty($link->subdomain) && !empty($link->domain) && !empty($link->tld)) {
                    $cdomain = $link->domain . "." . $link->tld;
                    $data = UserInfo::where('customdomain', $cdomain)->first();
                    if(empty($data)){
                        return view('errors.404'); 
                    }
                }
            }
        }   
        
        
   
        $capture = Capture::where("uuid",$request->uuid)->first();
        if(empty($capture)){
            return view('errors.404');
        }

        if(empty($data)) {
          $data = UserInfo::where('user_id', $capture->user_id)->first();
        }

        $user_info = $data;
        $store_info = json_decode($data->meta_data, true);
        $campaign = $capture->campaign;
        $product = $capture->product;
        $meta_data = json_decode($campaign->meta_data, true);
        $data = Entitycard::where('campaign_id',$campaign->id)->orderBy('id','desc')->get(); 

        $store_url = \App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'store');
        
        $path   = $capture->photo->getPath();
        // $path   = $capture->photo->getFullUrl();
        // dd($capture->photo->getFullUrl());
        $type   = "png";//pathinfo($path, PATHINFO_EXTENSION);
        // $img   = $this->get_remote_data($path);
        // $capture_img = 'data:application/' . $type . ';base64,' . base64_encode($img);

        if(env('FILESYSTEM_DRIVER') == 's3') {
            $capture_img = Storage::disk('s3')->temporaryUrl(
                $path, now()->addMinutes(5)
            );
        } else {
            $capture_img = $capture->photo->getFullUrl();
        }
        
        $path   = $product->arobject[0]->object->getPath();
        // $path   = $product->arobject[0]->object->getFullUrl();
        // $type   = pathinfo($path, PATHINFO_EXTENSION);
        $type   = "png";//pathinfo($path, PATHINFO_EXTENSION);
        // // $img   = file_get_contents($path);
        // $img   = $this->get_remote_data($path);
        // $product_img = 'data:application/' . $type . ';base64,' . base64_encode($img);

        if(env('FILESYSTEM_DRIVER') == 's3') {
            $product_img = Storage::disk('s3')->temporaryUrl(
                $path, now()->addMinutes(5)
            );
        } else {
            $product_img = $product->arobject[0]->object->getFullUrl();
        }
        $product_img = $product->photo[0]->getFullUrl();
        $product_desc_short = substr($product->description,0,60);
        $product_desc_long = substr($product->description,60);
        $tryon_link = route('campaigns.publishedTryOn',$campaign->public_link)."?sku=".$product->id;

        $logo_url = "";
        if(!empty($user_info->storelogo))
            $logo_url = $user_info->storelogo->url;
        $product_details_link = \App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'product', $product->id);
        // return $tryon_link;
        return view('templates.arview.capture',compact('campaign','capture','capture_img','product','product_img','product_desc_short','product_desc_long','tryon_link','meta_data','data', 'user_info', 'store_info', 'store_url', 'product_details_link', 'logo_url'));
    }
    function get_remote_data($url)	
    {         
        $curlSession = curl_init();
        curl_setopt($curlSession, CURLOPT_URL, $url);
        curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
    
        $data = curl_exec($curlSession);
        curl_close($curlSession);     
        return $data;   
    }     
}
