<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\SubscriptionPayment;
use App\UserInfo;
use App\Models\PlanInvoice;
use App\LaravelSubscriptions\Plan;
use Gate;
use Symfony\Component\HttpFoundation\Response;
use Carbon\Carbon;

class UserPaymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // abort_if(Gate::denies('user_payments_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $user_id = auth()->user()->id;
        $userSettings = UserInfo::where("user_id", $user_id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);
        $subscription_payments = SubscriptionPayment::where('user_id', $user_id)->orderBy('id', 'desc')->get();
        
        return view('templates.admin.user_payments.index',compact('subscription_payments','user_meta_data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user_id = auth()->user()->id;
        $userSettings = UserInfo::where("user_id", $user_id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);
        $user_payment = SubscriptionPayment::where('id', $id)->first();
        // dd($user_payment);
        // $plan = plan::where('slug',$user_payment->plan_name)->first();
        $plan = plan::where('name','like',"%$user_payment->plan_name%")->get();

        foreach($plan as $key){
            $price = $key->price - $key->discount;
            if($price == $user_payment->plan_price){
                $plan_id = $key->id;
            }
        }
        if(!empty($plan_id)){
            $plan = plan::where('id',$plan_id)->first(); 
        }
        else{
            $plan = plan::where('name','like',"%$user_payment->plan_name%")->first();
        }
        $planinvoice = PlanInvoice::where('subscription_payment_id',$id)->where('user_id',$user_id)->first();
        if(empty($planinvoice))
        {
        $now = carbon::parse($user_payment->created_at)->format('Y-m-d H:i:s');
            $inv_id = PlanInvoice::whereNotNull('inv_num')->count();
            if(!empty($inv_id)){
                $inv_num = $inv_id + 1;
            }
            else{
                $inv_num = 1;
            }
        $planinvoice = PlanInvoice::create([
            'subscription_payment_id' => $user_payment->id,
            'user_id' => $user_id,
            'plan_name' => $user_payment->plan_name,
            'plan_start' => $user_payment->plan_start,
            'plan_expiry' => $user_payment->plan_expiry,
            'inv_date' => $now,
            'inv_num' => $inv_num
        ]);
        
        }
        $inv_date = Carbon::parse($user_payment->created_at)->format('d-M-Y ');
        $plan_start = Carbon::parse($user_payment->plan_start)->format('d-M-Y H:i:s');
        $plan_expiry = Carbon::parse($user_payment->plan_expiry)->format('d-M-Y H:i:s');
        // print_r($plan);
        // exit;
        return view('templates.admin.user_payments.show', compact('user_payment','user_meta_data','inv_date','plan','plan_start','plan_expiry','planinvoice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
