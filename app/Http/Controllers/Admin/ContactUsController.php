<?php

namespace App\Http\Controllers\Admin;
use App\Models\ContactUs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;

class ContactUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $shareableLink = ShareableLink::where('uuid', $request->shareable_link->uuid)->first();
        $shareable = $shareableLink->shareable;
        $user = $shareable->user;
        
        $contactData = array(
            
            "name" => $request->name,
            "mobile" => $request->mobile,
            "email" => $request->email,
            "message" => $request->textmessage,
            "user_id" => $user->id
            
        );
       
        // $request->validate([
        //     'name' => 'required',
        //     'mobile' => 'required',
        //     'email' => 'required|email|unique:users'
        // ]);

    $contactus = ContactUs::create($contactData);

  
    return back()->with('success', 'We will contact us soon.');
        
    
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
