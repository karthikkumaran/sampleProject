<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\orders;
use App\ordersProduct;
use App\ordersCustomer;
use App\Campaign;
use App\UserInfo;
use App\Entitycard;
use App\Product;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;

use App\Jobs\OrderPlaceEmailJob;
use App\Jobs\EnquiryPlaceEmailJob;

class MailController extends Controller
{
    public function orderplace_email($data, $order)
    {
        // dd($request->all());
        // $public_link = $request->key;
        // $catalogs = explode(",",$request->catalogs);
        // $product_ids = explode(",",$request->products);
        // $product_qty = $request->products_qty;
        // $product_notes = $request->product_notes;
        // $order_notes = $request->order_notes ?? null;
        // // $shareableLink = ShareableLink::where('uuid', $public_link)->first();
        // // $shareableLinks = ShareableLink::where('uuid', $catalogs)->get();
        // $cmeta_data = array();
        // $pmeta_data = array();
        // $entity_email = array();
        // $currency = $request->currency;
        // $shareableLink = ShareableLink::where('uuid', $public_link)->first();
        // $order_campaign = $shareableLink->shareable;
        // $order_campaign_id = $order_campaign->id; 
        // $order_campaign = Campaign::where("id", $order_campaign_id)->first();
        // $order = orders::create(array("status" => 0, 'notes'=> $order_notes,'campaign_id' => $order_campaign_id, 'user_id' => $order_campaign->user->id, 'team_id' => $order_campaign->team->id));
        // // dd($request->all(),$public_link,$catalogs,$product_ids,$product_qty,$shareableLink,$catalogs,$order_campaign,$order);
        // $order_total_price = 0;
        // $order_total_discount = 0;

        // foreach($catalogs as $catalog){

        // $shareableLink = ShareableLink::where('uuid', $catalog)->first();
        //     if(!empty($shareableLink)){ 
        //         $campaign = $shareableLink->shareable;
        //         $campaign_id = $campaign->id; 
        //         $campaign = Campaign::where("id", $campaign_id)->first();
        //         $campaign_name = $campaign->name;
        //         $pmeta_data['campaign_name'] = $campaign_name;
        //         $campaign_meta_data = json_decode($campaign->meta_data, true);
        //         $userSettings = UserInfo::where("user_id", $campaign->user->id)->first();
        //         $meta_data = json_decode($userSettings->meta_data, true);
        //         $product_total_price = 0;
        //         $product_total_discount = 0;
        //         $EntityCard = Entitycard::where('campaign_id',$campaign_id)->whereIn('product_id',$product_ids)->get();
        //         array_push($entity_email,$EntityCard);
        //         foreach($EntityCard as $key => $card){
        //             $product = $card->product;
        //             $product_category = $product->categories ? $product->categories : " ";
        //             // dd(count($product->categories));
        //             $pmeta_data['category'] = $product_category;
        //             $product_stock = $product->stock;
        //             if($product_stock != null){
        //                 $product_new_stock = $product_stock - $product_qty[$product->id];
        //                 Product::where("id", $product->id)->update(array("stock" => $product_new_stock));
        //             }



        //             $orderProduct = ordersProduct::create(array("order_id" => $order->id, 
        //             "product_id" => $product->id, "price" => $product->price, 
        //             "qty" => $product_qty[$product->id], "sku" => $product->sku, "name" => $product->name,

        //             //error in product notes
        //             "discount" => $product->discount,"notes" => array_key_exists($product->id, $product_notes)?$product_notes[$product->id]:null, "meta_data" => json_encode($pmeta_data),'campaign_id' => $campaign_id, 'user_id' => $campaign->user->id, 'team_id' => $campaign->team->id));
        //             if($product->discount){
        //                 $discount_price = $product->price * ($product->discount/100);
        //                 $discounted_price = $product->price - $discount_price;
        //                 $product_total_price += $product_qty[$product->id] * $discounted_price;
        //             }
        //             else{
        //                 $product_total_price += $product_qty[$product->id] * $product->price;
        //             }
        //             $product_total_discount += $product->discount;
        //         }
        //         $order_total_discount += $product_total_discount;
        //         $order_total_price += $product_total_price;
        //     }

        // }  
        // $cmeta_data['product_total_price'] = $order_total_price;
        // $cmeta_data['product_total_discount'] = $order_total_discount;
        // $cmeta_data['currency'] = $currency;
        // $customerData = array(
        //     "fname" => $request->fname,
        //     "email" => $request->email,
        //     "address1" => $request->apt_number,
        //     "address2" => $request->landmark,
        //     "city" => $request->city,
        //     "pincode" => $request->pincode,
        //     "state" => $request->state,
        //     "country" => "india",
        //     "mobileno" => $request->mnumber,
        //     "amobileno" => $request->amnumber,
        //     "addresstype" => $request->addresstype,
        //     'order_id' => $order->id,
        //     'meta_data' => json_encode($cmeta_data),
        //     'campaign_id' => $order_campaign_id, 
        //     'user_id' => $order_campaign->user->id, 
        //     'team_id' => $order_campaign->team->id);
        // $ordersCustomer = ordersCustomer::create($customerData);

        try {
            // if(empty($meta_data['settings']['emailid']))
            //     $emailFrom = $order_campaign->user->email;
            // else
            //     $emailFrom = $meta_data['settings']['emailid'];

            // if(empty($meta_data['settings']['emailfromname']))
            //     $emailFromName = $order_campaign->user->name;
            // else 
            //     $emailFromName = $meta_data['settings']['emailfromname'];

            // if(!empty($meta_data['storefront']['storename']))
            //     $emailSubject = $meta_data['storefront']['storename']." - Your order is received";
            // else
            //     $emailSubject = "Your order is received";

            // // dd($campaign_email,$entity_email);
            // $data['userSettings'] = $userSettings;
            // $data['product_qty'] = $product_qty;
            // $data['product_notes'] = $product_notes;
            // $data['order_notes'] = $order_notes;
            // $data['customerData'] = $ordersCustomer;
            // $data['meta_data'] = $meta_data;
            // $data['campaign'] = $order_campaign;
            // $data['entity'] = $entity_email;
            // $data['toMail'] = $request->email;
            // $data['toBcc'] = $emailFrom;
            // $data['toName'] = $request->fname;
            // $data['subject'] = $emailSubject;
            // $data['fromMail'] = $emailFrom;
            // $data['fromName'] = $emailFromName;
            // $data['mnumber'] = $request->mnumber;
            // $data['address'] = $request->apt_number .",<br>".$request->landmark .",<br>". $request->city ." - ". $request->pincode .",<br>".$request->state; 
            if(isset($data['order'])==true)
                dispatch(new OrderPlaceEmailJob($data));
            else if(isset($data['enquire'])==true)
                dispatch(new EnquiryPlaceEmailJob($data));

            return response()->json(['type' => "mail", 'status' => "sent", "order_id" => $order->id]);
        } catch (\Exception $e) {
            return response()->json(['type' => "mail", 'status' => "not sent", "order_id" => $order->id, $e]);
        }
    }
}
