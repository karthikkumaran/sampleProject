<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Campaign;
use App\Models\OrderStatus;
use App\Models\OrderStatusRemarks;
use App\ordersCustomer;
use App\UserInfo;
use App\Product;
use App\ProductCategory;
use App\Entitycard;
use App\orders;
use App\ordersProduct;
use App\Transaction;
use App\Customer;
use App\customer_address;
use Carbon\Carbon;
use App\ProductVariant;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;
use DB;
use Illuminate\Support\Facades\Schema;
use App\LaravelSubscriptions\Plan;
use App\LaravelSubscriptions\PlanFeature;
use App\LaravelSubscriptions\PlanSubscription;
use App\LaravelSubscriptions\PlanSubscriptionUsage;
use App\LaravelSubscriptions\PlanSubscriptionFeatures;
use App\LaravelSubscriptions\PlanSubscribedFeatures;
use Webpatser\Uuid\Uuid;

class PosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = auth()->user();
        $canUsePos = false;

        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $user->id)->first();
        }
        if(!empty($subscription)){
            $subscribed_package = $subscription->slug;
            $canUsePos = $user->subscription($subscribed_package)->canUseFeature('pos');
        }
        else{
            return view('errors.404');
        }
        if($canUsePos){
            $userSettings = UserInfo::where("user_id", $user->id)->first();
            $user_meta_data = json_decode($userSettings->meta_data, true);
            $campaigns = Campaign::where("is_added_home", "1")->orderBy('is_start','desc')->orderBy('id', 'desc')->get();
            $customers = ordersCustomer::where('fname', '!=', 'guest')->groupBy('email', 'mobileno')->get();
            $customer_address = customer::where('store_owner_id',$user->team_id)->get();
            // $product_categories = ProductCategory::all();
            $product_categories = ProductCategory::leftJoin('entitycards', function ($join) {
                $join->on('product_categories.id', '=', 'entitycards.catagory_id');
            })->where("entitycards.team_id", $user->team_id)
                ->whereNotNull('entitycards.catagory_id')
                ->whereNull('entitycards.deleted_at')
                ->groupBy('product_categories.id')
                ->orderBy('product_categories.id', 'desc')
                ->get();
            return view('admin.pos.index', compact('customers','customer_address','campaigns', 'product_categories', 'user_meta_data'));
        }
        else {
            return view('errors.404');
        }
    }

    public function ajaxProductsData(Request $request){

        $sort = ['id', 'desc'];
        $page = 8;
        $is_search = false;
        $is_filter = false;
        $category_filter = "";
        $catalogs_filter = "";
        $price_filter_low = "";
        $price_filter_high = "";
        $cat_id = array();
        $category = array();
        $catalogs = array();
        $campaign_id = array();
        $filters = json_decode($request->filter, true);
        $campaigns = Campaign::where("is_added_home", "1")->orderBy('is_start','desc')->orderBy('id', 'desc')->get();
        foreach ($campaigns as $campaign) {
            array_push($campaign_id, $campaign->id);
        }
        $user = auth()->user();
        $team_id = $user->team_id;
        $userSettings = UserInfo::where("user_id", $user->id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);
        if ($request->ajax()) {

            if (isset($request->s) && !empty($request->s)) {
                $sort = explode(',', $request->s);
            }
            if (isset($request->q) && !empty($request->q)) {
                $q = $request->q;
                $is_search = true;
            }

            if (isset($request->filter) && !empty($filters)) {
                $is_filter = true;
                foreach ($filters as $card) {
                    if (in_array("category", array_values($card)) && $card['type'] == "category") {
                        $category_filter = $card['value'];

                        if ($category_filter == "all") {
                            $cat_id = "";
                        } else {
                            $cat = explode(",", $category_filter);
                            $category = ProductCategory::whereIn('name', $cat)->get('id');
                            foreach ($category as $catg) {
                                array_push($cat_id, $catg->id);
                            }
                        }
                    }

                    if (in_array("catalog", array_values($card)) && $card['type'] == "catalog") {
                        $catalogs_filter = $card['value'];

                        if ($catalogs_filter == "all") {

                        } else {
                            $campaign_id = array();
                            array_push($campaign_id, $catalogs_filter);
                        }
                    }

                    if ($card['type'] == "price") {
                        $price_range = explode(",", $card['value']);
                        $price_filter_low = $price_range[0];
                        if (count($price_range) == 2) {
                            $price_filter_high = $price_range[1];
                        }
                    }
                }
            }

            if (($is_search == true) && ($is_filter == false)) {
                $data = Entitycard::with('product')->whereIn("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($q) {
                    $query->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                })->whereHas('product',function($query2) {
                    $query2->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy($sort[0], $sort[1])->paginate($page);
            } else if (($is_search == false) && ($is_filter == true)) {
                if (($price_filter_low != "all") && ($category_filter != "all")) {
                    if ($price_filter_high == "") {
                        $data = Entitycard::with('product')->whereIn("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($price_filter_low, $cat_id) {
                                $query->where('products.price', '>=', $price_filter_low)->whereIn('catagory_id', $cat_id);
                            })->whereHas('product', function($query) {
                                $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                            })->orderBy($sort[0], $sort[1])->paginate($page);
                    } else {
                        $data = Entitycard::with('product')->whereIn("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($price_filter_low, $price_filter_high, $cat_id) {
                                $query->where('products.price', '>=', $price_filter_low)->where('products.price', '<=', $price_filter_high)->whereIn('catagory_id', $cat_id);
                            })->whereHas('product', function($query) {
                                $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                            })->orderBy($sort[0], $sort[1])->paginate($page);
                    }
                } else if (($price_filter_low != "all") && ($category_filter == "all")) {
                    if ($price_filter_high == "") {
                        $data = Entitycard::with('product')->whereIn("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($price_filter_low, $price_filter_high) {
                                $query->where('products.price', '>=', $price_filter_low);
                            })->whereHas('product', function($query) {
                                $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                            })->orderBy($sort[0], $sort[1])->paginate($page);
                    } else {
                        $data = Entitycard::with('product')->whereIn("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($price_filter_low, $price_filter_high) {
                                $query->where('products.price', '>=', $price_filter_low)->where('products.price', '<=', $price_filter_high);
                            })->whereHas('product', function($query) {
                                $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                            })->orderBy($sort[0], $sort[1])->paginate($page);
                    }
                } else if (($price_filter_low == "all") && ($category_filter != "all")) {
                    $data = Entitycard::with('product')->whereIn("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($cat_id) {
                            $query->whereIn('catagory_id', $cat_id);
                        })->whereHas('product', function($query) {
                            $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                } else {
                    $data = Entitycard::with('product')->whereIn("campaign_id", $campaign_id)->whereHas('product', function ($query){
                        return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                    })->orderBy($sort[0], $sort[1])->paginate($page);
                }
            } else if (($is_search == true) && ($is_filter == true)) {
                if (($price_filter_low != "all") && ($category_filter != "all")) {
                    if ($price_filter_high == "") {
                        // dd("Searching","Filtering","Both Search and Filter, Both Filters only min ");
                        $data = Entitycard::with('product')->whereIn("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($price_filter_low, $cat_id, $q) {
                                $query->where('products.price', '>=', $price_filter_low)->where('catagory_id', $cat_id)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                            })->whereHas('product', function($query) {
                                $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                            })->orderBy($sort[0], $sort[1])->paginate($page);
                    } else {
                        // dd("Searching","Filtering","Both Search and Filter, Both Filters ");
                        $data = Entitycard::with('product')->whereIn("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($price_filter_low, $price_filter_high, $cat_id, $q) {
                                $query->where('products.price', '>=', $price_filter_low)->where('products.price', '<=', $price_filter_high)->where('catagory_id', $cat_id)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                            })->whereHas('product', function($query) {
                                $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                            })->orderBy($sort[0], $sort[1])->paginate($page);
                    }
                } else if (($price_filter_low != "all") && ($category_filter == "all")) {
                    if ($price_filter_high == "") {
                        // dd("Searching","Filtering","Both Search and Filter, Only price Filter only min ");
                        $data = Entitycard::with('product')->whereIn("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($price_filter_low, $price_filter_high, $q) {
                                $query->where('products.price', '>=', $price_filter_low)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                            })->whereHas('product', function($query) {
                                $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                            })->orderBy($sort[0], $sort[1])->paginate($page);
                    } else {
                        // dd("Searching","Filtering","Both Search and Filter, Only price Filter ");
                        $data = Entitycard::with('product')->whereIn("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($price_filter_low, $price_filter_high, $q) {
                            $query->where('products.price', '>=', $price_filter_low)->where('products.price', '<=', $price_filter_high)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                            })->whereHas('product', function($query) {
                                $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                            })->orderBy($sort[0], $sort[1])->paginate($page);
                    }
                } else if (($price_filter_low == "all") && ($category_filter != "all")) {
                    // dd("Searching","Filtering","Both Search and Filter, Only category Filter ");
                    $data = Entitycard::with('product')->whereIn("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($cat_id, $q) {
                            $query->where('catagory_id', 'LIKE', $cat_id)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                        })->whereHas('product', function($query) {
                            $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                } else {
                    // dd("Searching","Filtering","Both Search and Filter, both all filters");
                    $data = Entitycard::with('product')->whereIn("campaign_id", $campaign_id)->whereHas('product', function ($query) use ($q) {
                            $query->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%');
                        })->whereHas('product', function($query) {
                            $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                }
            } else {
                $data = Entitycard::with('product')->whereIn("campaign_id", $campaign_id)->whereHas('product', function ($query){
                    return $query->where('downgradeable', '=', NULL)->orWhere('downgradeable', '!=', 1);
                })->orderBy($sort[0], $sort[1])->paginate($page);
            }
            return view('cards.pos_products', compact('data','user_meta_data','is_search','is_filter','campaign','userSettings'));
        }
        return false;
    }

    public function calculateTotalAmount($products_qty,$posdiscount, $setdiscount)
    {
        $total_amount = 0;
        $discount_total = 0;
        $total_discount = 0;
        foreach ($products_qty as $key => $value) {
            $products = Product::where('sku', $key)->first();
            $product_variant=ProductVariant::where('sku',$key)->first();
         if(!empty($products))
            {
            $product_price = $products['price'];
            $product_discount = $products['discount'];
            $product_order_price = $product_price;
            $product_order_discount = $product_discount;
            if ($product_discount != null) {
                $product_order_price = $product_price - ($product_price * ($product_discount / 100));
                $product_order_discount =  $product_discount;
            }
            $temp_total = $product_order_price * (int)$value;
            $total_amount += $temp_total;
            $discount_total += $product_order_discount;
        }
        if(!empty($product_variant))
            {
                $product_price = $product_variant['price'];
                $product_discount = $product_variant['discount'];
                $product_order_price = $product_price;
                $product_order_discount = $product_discount;
                if ($product_discount != null) {
                    $product_order_price = $product_price - ($product_price * ($product_discount / 100));
                    $product_order_discount =  $product_discount;
                }
                $temp_total = $product_order_price * (int)$value;
                $total_amount += $temp_total;
                $discount_total += $product_order_discount;
            }
            $count=count((array)$products)+count((array)$product_variant);
    }
            $total_discount = $discount_total/$count;
            if($posdiscount != null){
            if($setdiscount == 'PERCENTAGE')
            {
                $total_amount = ($total_amount - (($total_amount * $posdiscount)/100));
            }
            else{
                $total_amount = $total_amount - $posdiscount;
            }
        }
        return ['total_amount' => $total_amount, 'total_discount' => $total_discount, 'set_discount' => $setdiscount, 'pos_discount' => $posdiscount];
    }
    public function update(Request $request)
    {
        $user = auth()->user();
        $customer_id = customer::where('store_owner_id',$user->team_id)->count()+1;

        $customer=Customer::where([['email',$request['email']],
        ['store_owner_id',$user->team_id]])->first();



        if(empty($customer))
        {
        $customer = customer::create(array(
            'name'=> $request->fname,
            'email' => $request->email,
            'country_code' => "91" ,
            'mobile'=> $request->mnumber,
            'password'=> Hash::make(str::random(10)),
            'remember_token'=> null,
            'verification_token'=> null,
            'verified'=> "0",
            'approved'=> "1",
            'store_owner_id'=> $user->team_id,
            'customer_id'=> $customer_id,
            'meta_data'=> null
        ));
        $customer_address = customer_address::create(array(
            'name'=> $request->fname,
            'mobileno'=> $request->mnumber,
            'amobileno'=> $request->amnumber,
            'email'=> $request->email,
            'address1'=>$request->apt_number,
            'address2'=>$request->landmark,
            'city'=>$request->city,
            'pincode'=>$request->pincode,
            'state'=>$request->state,
            'addresstype'=>$request->addresstype,
            'customer_id'=>$customer->id
        ));
        return redirect()->back()->with('success','Address updated successfully');
    }
    else{
        return redirect()->back()->with('msg','customer email address already exist ');
    }
        //
    }

    public function placeOrder(Request $request)
    {
        $customer_id = $request->customer;
        $products_sku = Product::whereIn('sku', array_keys($request->products_qty))->get()->pluck('sku')->toArray();
        $product_variant_sku=ProductVariant::whereIn('sku', array_keys($request->products_qty))->get()->pluck('sku')->toArray();
        $total = $this->calculateTotalAmount($request->products_qty,$request->posdiscount, $request->setdiscount);
        $catalog_ids  = explode(",", $request->catalogs);
        $catalog_ids=array_unique($catalog_ids);
        $product_sku = explode(",", $request->products);
        $product_qty = $request->products_qty;
        $product_notes = $request->product_notes ?? null;
        $order_notes = $request->order_notes ?? null;
        $cmeta_data = array();
        $pmeta_data = array();
        $entity_email = array();
        $currency = $request->currency;
        $user = auth()->user();
        $userSettings = UserInfo::where("user_id", $user->id)->first();
        $meta_data = json_decode($userSettings->meta_data, true);
        $order_ID=orders::where('team_id', $user->team_id)->count()+1;
        // dd(auth()->user()->id);
        $order = orders::create(array(
            'campaign_id' => 1,
            'status' => $request->status,
            'notes' => $order_notes,
            'user_id' => $user->id,
            'team_id' => $user->team_id,
            'order_mode' => "POS",
            'order_ref_id'=>$order_ID,
        ));
            $orderstatuses=OrderStatus::create(array(
                'order_id' => $order->id,
                'status'=> $request->status,
                'changed_by'=> isset(auth()->user()->id) ? auth()->user()->id :null,
            ));
            $orderstatusremark= OrderStatusRemarks::create(array(
                'order_id' => $order->id,
                'status'=> $request->status,
                'remarks'=> $request->remarks,
                'showremarks'=>$request->showremarks
            ));
            $product_sku=array_merge($product_variant_sku,$product_sku);
            $unique_sku=array_unique($product_sku);
        foreach($catalog_ids as $index => $catalog_id) {
            if (!empty($catalog_id)) {
                $campaign = Campaign::where("id", $catalog_id)->first();
                $campaign_name = $campaign->name;
                $pmeta_data['campaign_name'] = $campaign_name;
                $campaign_meta_data = json_decode($campaign->meta_data, true);
                $EntityCard_product = Entitycard::where('campaign_id', $catalog_id)->with('product')
                ->whereHas('product',function($query) use ($unique_sku){
                      $query->whereIn('sku',$unique_sku);
                })->get();
                $EntityCard_productsVariant = Entitycard::where('campaign_id', $catalog_id)->with('product')
                ->whereHas('product.product_variants',function($query) use ($unique_sku){
                 $query->whereIn('sku',$unique_sku);
                })->get();
                $entity_card=  $EntityCard_product->merge($EntityCard_productsVariant);
                foreach ($entity_card as $key => $card) {
                    $product = $card;
                    if(in_array($product['product']['sku'],$unique_sku))
                    {
                        $product_category = $product['product']['categories'] ? $product['product']['categories'] : " ";
                        $pmeta_data['category'] = $product_category;
                        $product_stock = $product['product']['stock'];
                        if ($product_stock != null) {
                            $product_new_stock = $product_stock - $product_qty[$product['product']['sku']];
                            Product::where("sku", $product['product']['sku'])->update(array("stock" => $product_new_stock));
                        }
                        if(isset($product['product']['sku']) && $product['product']['has_variants']==0)
                        {
                            $orderProduct = ordersProduct::create(array(
                                "order_id" => $order->id,
                                "product_id" => $product['product']['id'],
                                 "price" => $product['product']['price'],
                                 "qty" => $request->products_qty[$product['product']['sku']],
                                 "sku" => $product['product']['sku'],
                                 "name" => $product['product']['name'],
                                "discount" => $product['product']['discount'],
                                "notes" => null,
                                "meta_data" => json_encode($pmeta_data),
                                'campaign_id' => 1,
                                'user_id' => $user->id,
                                'team_id' => $user->team_id
                            ));
                        }
                    }
                    else{
                        $product_variant=ProductVariant::whereIn('sku',$unique_sku)->get();
                            foreach($product_variant as $product_variants)
                               {
                                 $orderProduct = ordersProduct::create(array(
                                    "order_id" => $order->id,
                                    "product_id" => $product['product']['id'],
                                    "price" => $product_variants->price,
                                     "qty" => $request->products_qty[$product_variants->sku],
                                     "sku" => $product_variants->sku,
                                     "name" => $product_variants->name,
                                       "discount" => $product_variants->discount,
                                     "product_variant_id"=> $product_variants->id,
                                    "notes" => null,
                                    "meta_data" => json_encode($pmeta_data),
                                    'campaign_id' => 1 ,
                                    'user_id' => $user->id,
                                    'team_id' => $user->team_id
                               ));
                           }
                   }
                }
                // $order_total_discount += $product_total_discount;
                // $order_total_price += $product_total_price;
            }
        }
        // $cmeta_data['product_total_price'] = $order_total_price;
        // $cmeta_data['product_total_discount'] = $order_total_discount;
        $cmeta_data['product_total_price'] = $total['total_amount'];
        $cmeta_data['product_total_discount'] = $total['total_discount'];
        $cmeta_data['set_discount'] = $total['set_discount'];
        $cmeta_data['pos_discount'] = $total['pos_discount'];
        $cmeta_data['currency'] = $currency;
        $signimg = $request->customersign;

        $sign_img = substr($signimg, strpos($signimg, "storage"));

        if($request->customer_address == "true"){
            $customerData = array(
                "fname" => $request->fname,
                "email" => $request->email,
                "address1" => $request->apt_number,
                "address2" => $request->landmark,
                "city" => $request->city,
                "pincode" => $request->pincode,
                "state" => $request->state,
                "country" => "India",
                "mobileno" => $request->mnumber,
                "amobileno" => $request->amnumber,
                "addresstype" => $request->addresstype,
                'order_id' => $order->id,
                'meta_data' => json_encode($cmeta_data),
                'campaign_id' => 1,
                'user_id' => $user->id,
                'team_id' => $user->team_id,
            );
        }else{
            if(!empty($customer_id)){
                $email = Customer::where("id", $customer_id)->pluck("email");

                $customer = customer_address::where("email", $email)->first();
                if(!empty($customer)){
                    $customerData = array(
                        "fname" => $customer->name,
                        "email" => $customer->email,
                        "address1" => $customer->address1,
                        "address2" => $customer->address2,
                        "city" => $customer->city,
                        "pincode" => $customer->pincode,
                        "state" => $customer->state,
                        "country" => $customer->country,
                        "mobileno" => $customer->mobileno,
                        "amobileno" => $customer->amobileno,
                        "addresstype" => $customer->addresstype,
                        'order_id' => $order->id,
                        'meta_data' => json_encode($cmeta_data),
                        'campaign_id' => 1,
                        'user_id' => $user->id,
                        'team_id' => $user->team_id,
                        'customer_id'=>$customer_id,
                    );
                }
                else{
                    $customer = Customer::where("id", $customer_id)->first();
                    $customerData = array(
                        "fname" => $customer->name,
                        "email" => $customer->email,
                        "address1" => "NoPosaddress",
                        "address2" => "NoPosaddress",
                        "city" => "Nocity",
                        "pincode" => '000000',
                        "state" => "Nostate",
                        "country" => "Nocountry",
                        "mobileno" => $customer->mobile,
                        "amobileno" => null,
                        "addresstype" => "pos address",
                        'order_id' => $order->id,
                        'meta_data' => json_encode($cmeta_data),
                        'campaign_id' => 1,
                        'user_id' => $user->id,
                        'team_id' => $user->team_id,
                        'customer_id'=>$customer_id,
                    );
                }
            }
            else{
                $customerData = array(


                    "fname" => "Guest",
                    "email" => "guest@example.com",
                    // "address1" => "2G/243",
                    "address2" => "",
                    // "city" => "Chennai",
                    // "pincode" => "600127",
                    // "state" => "Tamil Nadu",
                    // "country" => "India",
                    // "mobileno" => "0123456789",

                    "amobileno" => "",
                    "addresstype" => "Home",
                    'order_id' => $order->id,
                    'meta_data' => json_encode($cmeta_data),
                    'campaign_id' => 1,
                    'user_id' => $user->id,
                    'team_id' => $user->team_id,
                );
            }

        }

        $ordersCustomer = ordersCustomer::create($customerData);
        if(!empty($request->customersign)){
            $signature = str_replace('data:image/png;base64,', '', $request->customersign);
            $signature = str_replace(' ', '+', $request->customersign);
            error_log($signature);
            $current_timestamp = Carbon::now()->timestamp."-".Uuid::generate(5,'signature', Uuid::NS_DNS)->string;
            $ordersCustomer->addMediaFromBase64($signature)->usingFileName($current_timestamp.".png")->toMediaCollection('signature');
        }


        if (empty($meta_data['settings']['emailid']))
            $emailFrom = $user->email;
        else
            $emailFrom = $meta_data['settings']['emailid'];

        if (empty($meta_data['settings']['emailfromname']))
            $emailFromName = $user->name;
        else
            $emailFromName = $meta_data['settings']['emailfromname'];

        if (!empty($meta_data['storefront']['storename']))
            $emailSubject = $meta_data['storefront']['storename'] . " - Your order is received";
        else
            $emailSubject = "Your order is received";

        $data['userSettings'] = $userSettings;
        $data['product_qty'] = $product_qty;
        $data['product_notes'] = $product_notes;
        $data['order_notes'] = $order_notes;
        $data['customerData'] = $ordersCustomer;
        $data['meta_data'] = $meta_data;
        $data['campaign'] = null;
        $data['entity'] = $entity_email;
        $data['toMail'] = $request->email;
        $data['toBcc'] = $emailFrom;
        $data['toName'] = $request->fname;
        $data['subject'] = $emailSubject;
        $data['fromMail'] = $emailFrom;
        $data['fromName'] = $emailFromName;
        $data['mnumber'] = "9843890923";
        $data['address'] = "Chennai" . " - " . "Tamil nadu" . ",<br>" . "India";
        $response = app('App\Http\Controllers\Admin\MailController')->orderplace_email($data, $order);

        $store_order_id = $order->id;
        $request['store_order_id'] = $store_order_id;
        $this->store_transaction($request);

        $view = view('modals.pos_invoice', compact('data','order'))->render();
        //return redirect()->to('/admin/pos');
        // return view('modals.pos_invoice', compact('data','order'));
        return response()->json(['html'=>$view]);
    }


    public function store_transaction(Request $request)
    {
        // $product_variant_sku=ProductVariant::whereIn('sku', array_keys($request->products_qty))->get()->pluck('sku')->toArray();
        // $products_sku= Product::whereIn('sku', array_keys($request->products_qty))->get()->pluck('sku')->toArray();
        $total = $this->calculateTotalAmount($request->products_qty,$request->posdiscount,$request->setdiscount);
        $request['COD_total'] = $total;
        $transaction_details = app('App\Http\Controllers\Admin\PaymentResponseController')->payment($request);
        // $shareableLink = ShareableLink::where('uuid', $request->key)->first();
        // $order_campaign = $shareableLink->shareable;
        $transaction = Transaction::create([
            'payment_mode' => $request->payment_method, 'payment_method' => $transaction_details['method'],
            'order_id' => $transaction_details['order_id'],
            'transaction_id' => $transaction_details['trans_id'],
            'invoice_id' => '',
            'status' => 'success',
            'amount' => $transaction_details['amount'],
            'team_id' => auth()->user()->team_id,
            'meta_data' => serialize($transaction_details),
        ]);

        Orders::find($request->store_order_id)->update(['transaction_id' => $transaction->id]);
        // if ($request->input('photo', false)) {
        //     $transaction->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
        // }

        // if ($media = $request->input('ck-media', false)) {
        //     Media::whereIn('id', $media)->update(['model_id' => $transaction->id]);
        // }

        return true;
        // return ['key' => $request->key, 'order_id' => $request->store_order_id];
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
