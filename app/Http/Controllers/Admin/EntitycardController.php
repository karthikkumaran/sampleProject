<?php

namespace App\Http\Controllers\Admin;

use App\Asset;
use App\Campaign;
use App\Entitycard;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyEntitycardRequest;
use App\Http\Requests\StoreEntitycardRequest;
use App\Http\Requests\UpdateEntitycardRequest;
use App\Product;
use App\Video;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class EntitycardController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('entitycard_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Entitycard::with(['campagin', 'product', 'video', 'asset', 'team'])->select(sprintf('%s.*', (new Entitycard)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'entitycard_show';
                $editGate      = 'entitycard_edit';
                $deleteGate    = 'entitycard_delete';
                $crudRoutePart = 'entitycards';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->addColumn('campagin_name', function ($row) {
                return $row->campagin ? $row->campagin->name : '';
            });

            $table->addColumn('product_name', function ($row) {
                return $row->product ? $row->product->name : '';
            });

            $table->addColumn('video_title', function ($row) {
                return $row->video ? $row->video->title : '';
            });

            $table->addColumn('asset_name', function ($row) {
                return $row->asset ? $row->asset->name : '';
            });

            $table->editColumn('meta_data', function ($row) {
                return $row->meta_data ? $row->meta_data : "";
            });

            $table->rawColumns(['actions', 'placeholder', 'campagin', 'product', 'video', 'asset']);

            return $table->make(true);
        }

        return view('admin.entitycards.index');
    }

    public function create()
    {
        abort_if(Gate::denies('entitycard_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $campagins = Campaign::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $products = Product::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $videos = Video::all()->pluck('title', 'id')->prepend(trans('global.pleaseSelect'), '');

        $assets = Asset::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.entitycards.create', compact('campagins', 'products', 'videos', 'assets'));
    }

    public function store(StoreEntitycardRequest $request)
    {
        $entitycard = Entitycard::create($request->all());

        return redirect()->route('admin.entitycards.index');
    }

    public function edit(Entitycard $entitycard)
    {
        abort_if(Gate::denies('entitycard_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $campagins = Campaign::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $products = Product::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $videos = Video::all()->pluck('title', 'id')->prepend(trans('global.pleaseSelect'), '');

        $assets = Asset::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $entitycard->load('campagin', 'product', 'video', 'asset', 'team');

        return view('admin.entitycards.edit', compact('campagins', 'products', 'videos', 'assets', 'entitycard'));
    }

    public function update(UpdateEntitycardRequest $request, Entitycard $entitycard)
    {
        $entitycard->update($request->all());

        return redirect()->route('admin.entitycards.index');
    }

    public function show(Entitycard $entitycard)
    {
        abort_if(Gate::denies('entitycard_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $entitycard->load('campagin', 'product', 'video', 'asset', 'team');

        return view('admin.entitycards.show', compact('entitycard'));
    }

    public function destroy(Entitycard $entitycard)
    {
        abort_if(Gate::denies('entitycard_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $entitycard->delete();

        return back();
    }

    public function massDestroy(MassDestroyEntitycardRequest $request)
    {
        Entitycard::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
