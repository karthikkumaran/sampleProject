<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Symfony\Component\HttpFoundation\Response;
use Gate;
use App\enquiry;
use App\enquiryProduct;
use App\enquiryCustomer;
use App\UserInfo;
use Illuminate\Http\Request;
use App\Campaign;
use App\Entitycard;
use App\Product;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;
use App\Telegram_Bot_Subscriber;
use WeStacks\TeleBot\TeleBot;


class EnquiryController extends Controller
{

    public function index(Request $request)
    {
        $page = 10;
        $user_id = auth()->user()->id;
        $enquiries = enquiry::orderBy('id', 'desc')->paginate($page);
        $userSettings = UserInfo::where("user_id", $user_id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);
        return view('templates.admin.enquiry.index', compact('user_meta_data', 'enquiries'));
    }

    public function store(Request $request)
    {
        $tempStorage = [];
        foreach($request->products_qty as $key =>  $value){
            $product = Product::where('id', $key)->first();
            if(isset($product->minimum_quantity) <= $value)
            {
                $tempStorage[$key] = $value;
            }
            else if($value   <=  $product->maximum_quantity){
                $tempStorage[$key] = $value;
            }
            else{ 
                if($value < $product->minimum_quantity){
                    $tempStorage[$key] = $product->minimum_quantity;
                }else{
                    $tempStorage[$key] = $product->maximum_quantity;
                }
                        }
        }
        $request->products_qty =$tempStorage;

        $public_link = $request->key;
        $catalogs = explode(",", $request->catalogs);
        $product_ids = explode(",", $request->products);
        $product_qty = $request->products_qty;
        $product_notes = $request->product_notes;
        $enquiry_notes = $request->order_notes ?? null;
        $cmeta_data = array();
        $pmeta_data = array();
        $entity_email = array();
        $currency = $request->currency;
        $shareableLink = ShareableLink::where('uuid', $public_link)->first();
        $enquiry_campaign = $shareableLink->shareable;
        $enquiry_campaign_id = $enquiry_campaign->id;
        $enquiry_campaign = Campaign::where("id", $enquiry_campaign_id)->first();

        $enquiry_id=enquiry::where('team_id',$enquiry_campaign->team->id)->count()+1;

        $enquiry = enquiry::create(array(
            "status" => 0,
            'notes' => $enquiry_notes,
            'campaign_id' => $enquiry_campaign_id,
            'user_id' => $enquiry_campaign->user->id,
            'team_id' => $enquiry_campaign->team->id,
            'enquiry_id'=>$enquiry_id
        ));

        $order_total_price = 0;
        $order_total_discount = 0;
        foreach ($catalogs as $catalog) {

            $shareableLink = ShareableLink::where('uuid', $catalog)->first();
            if (!empty($shareableLink)) {
                $campaign = $shareableLink->shareable;
                $campaign_id = $campaign->id;
                $campaign = Campaign::where("id", $campaign_id)->first();
                $campaign_name = $campaign->name;
                $pmeta_data['campaign_name'] = $campaign_name;
                $campaign_meta_data = json_decode($campaign->meta_data, true);
                $userSettings = UserInfo::where("user_id", $campaign->user->id)->first();
                $meta_data = json_decode($userSettings->meta_data, true);
                $product_total_price = 0;
                $product_total_discount = 0;
                $EntityCard = Entitycard::where('campaign_id', $campaign_id)->whereIn('product_id', $product_ids)->get();
                array_push($entity_email, $EntityCard);

                foreach ($EntityCard as $key => $card) {
                    $product = $card->product;
                    $product_category = $product->categories ? $product->categories : " ";
                    // dd(count($product->categories));
                    $pmeta_data['category'] = $product_category;
                    $product_stock = $product->stock;
                    if ($product_stock != null) {
                        $product_new_stock = $product_stock - $product_qty[$product->id];
                        Product::where("id", $product->id)->update(array("stock" => $product_new_stock));
                    }

                    $enquiryProduct = enquiryProduct::create(array(
                        "enquiry_id" => $enquiry->id,
                        "product_id" => $product->id, "price" => $product->price,
                        "qty" => $product_qty[$product->id], "sku" => $product->sku, "name" => $product->name,
                        "discount" => $product->discount, "notes" => array_key_exists($product->id, $product_notes) ? $product_notes[$product->id] : null,
                        "meta_data" => json_encode($pmeta_data),
                        'campaign_id' =>  $enquiry_campaign_id,
                        'user_id' => $enquiry_campaign->user->id,
                        'team_id' => $enquiry_campaign->team->id
                    ));
                    if ($product->discount) {
                        $discount_price = $product->price * ($product->discount / 100);
                        $discounted_price = $product->price - $discount_price;
                        $product_total_price += $product_qty[$product->id] * $discounted_price;
                    } else {
                        $product_total_price += $product_qty[$product->id] * $product->price;
                    }
                    $product_total_discount += $product->discount;
                }
                $order_total_discount += $product_total_discount;
                $order_total_price += $product_total_price;
            }
        }

        $cmeta_data['product_total_price'] = $order_total_price;
        $cmeta_data['product_total_discount'] = $order_total_discount;
        $cmeta_data['currency'] = $currency;

        $customerData = array(
            "fname" => $request->fname,
            "email" => $request->email,
            "address1" => $request->apt_number,
            "address2" => $request->landmark,
            "city" => $request->city,
            "pincode" => $request->pincode,
            "state" => $request->state,
            "country" => "india",
            "country_code" => $request->country_code,
            "mobileno" => $request->mnumber,
            "amobileno" => $request->amnumber,
            "addresstype" => $request->addresstype,
            "delivery_note" => $request->delivery_note,
            'enquiry_id' => $enquiry->id,
            'meta_data' => json_encode($cmeta_data),
            'campaign_id' => $enquiry_campaign_id,
            'user_id' => $enquiry_campaign->user->id,
            'team_id' => $enquiry_campaign->team->id,
            'customer_id' => auth()->guard('customer')->check() ? auth()->guard('customer')->user()->customer_id :'',
        );
        
        $enquiryCustomer = enquiryCustomer::create($customerData);

        if (empty($meta_data['settings']['emailid']))
            $emailFrom = $enquiry_campaign->user->email;
        else
            $emailFrom = $meta_data['settings']['emailid'];

        if (empty($meta_data['settings']['emailfromname']))
            $emailFromName = $enquiry_campaign->user->name;
        else
            $emailFromName = $meta_data['settings']['emailfromname'];

        if (!empty($meta_data['storefront']['storename']))
            $emailSubject = $meta_data['storefront']['storename'] . " - Your order is received";
        else
            $emailSubject = "Your enquiry is received";

        $data['enquire']=true;
        $data['userSettings'] = $userSettings;
        $data['product_qty'] = $product_qty;
        $data['product_notes'] = $product_notes;
        $data['order_notes'] = $enquiry_notes;
        $data['customerData'] = $enquiryCustomer;
        $data['meta_data'] = $meta_data;
        $data['campaign'] = $enquiry_campaign;
        $data['entity'] = $entity_email;
        $data['toMail'] = $request->email;
        $data['toBcc'] = $emailFrom;
        $data['toName'] = $request->fname;
        $data['subject'] = $emailSubject;
        $data['fromMail'] = $emailFrom;
        $data['fromName'] = $emailFromName;
        $data['mnumber'] = $request->mnumber;
        $data['address'] = $request->apt_number . ",<br>" . $request->landmark . ",<br>" . $request->city . " - " . $request->pincode . ",<br>" . $request->state;
        $response = app('App\Http\Controllers\Admin\MailController')->orderplace_email($data, $enquiry);

        $notification_request=new Request();
        $notification_request->setMethod('POST');
        $notification_request->request->add(
            array("user_id"=>$campaign->user->id,
            "title"=>'Enquiry id: #'.$enquiry_id,
            "body"=>'Your have received an enquiry from '.$request->fname.'. Enquiry Number is #'.$enquiry_id.' Login to your SimpliSell account to process the enquiry.')
        );

        if(!isset($meta_data['notification_settings']['order_notification']) ){
            $notification=app('App\Http\Controllers\Admin\NotificationController')->sendNotification($notification_request);
        }else{
            if( $meta_data['notification_settings']['order_notification'] != "off"){
                $notification=app('App\Http\Controllers\Admin\NotificationController')->sendNotification($notification_request);
            }
        }

        $bot = new TeleBot(env('TELEGRAM_BOT_TOKEN'));
        $telegram_bot_subscribers = Telegram_Bot_Subscriber::where("user_id", $enquiry_campaign->user->id)->get();

        if(!empty($telegram_bot_subscribers)){

        $telegram_text = "Your store has received a new enquiry #". $enquiry_id ."\n"
        . "<b><a href='". route('admin.enquiry.show',$enquiry->id) . "'>Click here to view the enquiry.</a></b>\n";
        
        
        foreach ($telegram_bot_subscribers as $subscriber) {
            $bot->sendMessage([
                'chat_id' => $subscriber->telegram_chat_id,
                'parse_mode' => 'HTML',
                'text' => $telegram_text,
            ]);
        }
    }
    
    // dd($response,$response->{'original'},$response->{'original'}['order_id']);
        if ($response->{'original'}['order_id']) {
            $enquiry_id = $response->{'original'}['order_id'];
            return ['enquiry_id' => $enquiry->id, 'key' => $public_link];
        }
    }


    public function enquiryAjaxData(Request $request)
    {
        abort_if(Gate::denies('order_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $page = 10;
        $userSettings = UserInfo::where("user_id", auth()->user()->id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);
        if ($request->ajax()) {

            $q = $request->q;
            $enquiries = [];

            if (isset($request->s) && !empty($request->s)) {
                $sort = explode(',', $request->s);
            }
            if (isset($request->q) && !empty($request->q)) {
                $enquiries = enquiryCustomer::Where('enquiry_id', 'LIKE', $q . '%')
                    ->orwhere('email', "LIKE", '%' . $q . "%")
                    ->orwhere('mobileno', "LIKE", '%' . $q . "%")
                    ->orwhere('fname', "LIKE", '%' . $q . "%")
                    ->orwhere('lname', "LIKE", '%' . $q . "%")
                    ->orwhere('created_at', "LIKE", '%' . $q . "%")
                    ->orderBy('id', 'desc')
                    ->paginate($page);
            } else {
                $enquiries = enquiryCustomer::orderBy('id', 'desc')->paginate($page);
            }

            return view("cards.enquiry", compact('enquiries', 'user_meta_data'));
        }
    }

    public function show($enquiries)
    {
        abort_if(Gate::denies('order_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $enquiry = enquiryCustomer::where("enquiry_id", $enquiries)->first();
        $userSettings = UserInfo::where("user_id", auth()->user()->id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);
        return view('templates.admin.enquiry.show', compact('enquiry', 'user_meta_data'));
    }


    public function updateEnquiryStatus(Request $request)
    {
        Enquiry::find($request->enquiry_id)->update(['status' => $request->enquiry_status]);
    }
}
