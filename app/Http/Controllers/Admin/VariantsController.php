<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ProductVariant;
use App\Product;

class VariantsController extends Controller
{
    public function index(Request $request)
    {
        return view('admin.variants.index');
    }

    public function ajaxvariantdata(Request $request)
    {
        $variantdata = ProductVariant::find($request->variantid);
        return $variantdata;
    }

    public function ajaxproductdata(Request $request)
    {
        $productdata = Product::find($request->productid);
        return $productdata;
    }
}
