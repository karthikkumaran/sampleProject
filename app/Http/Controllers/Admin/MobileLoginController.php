<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MobileLoginController extends Controller
{
    public function index(Request $request)
    {
    return view('templates.admin.mobileLogin.mobileLogin');
    }
}
