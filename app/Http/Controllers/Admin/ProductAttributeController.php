<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Gate;
use Symfony\Component\HttpFoundation\Response;
use App\Product;
use Yajra\DataTables\Facades\DataTables;
use Rinvex\Attributes\Models\Attribute;
use App\AttributesMetadata;
use Rinvex\Attributes\Models\Type\Varchar;


class ProductAttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        abort_if(Gate::denies('product_attribute_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = AttributesMetadata::groupBy('attribute_group_name')->selectRaw('id, attribute_group_name , count(*) as total')->get();
            // dd($query   );
            $table = Datatables::of($query);
            $table->addColumn('attribute_count', '&nbsp;');
            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $editGate      = 'product_attribute_edit';
                $deleteGate    = 'product_attribute_delete';
                $crudRoutePart = 'product-attributes';

                return view('partials.AttributedatatablesActions', compact(
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('attribute_count', function ($row) {
                return $row->total ? $row->total    : "";
            });
            // $table->editColumn('label', function ($row) {
            //     return $row->label ? $row->label : "";
            // });
            // $table->editColumn('type', function ($row) {
            //     return $row->type ? $row->type : "";
            // });
            // $table->editColumn('values', function ($row) {
            //     return $row->values ? $row->values : "";
            // });
            // $table->editColumn('no_of_attributes', function ($row) {
            //     return $row->values ? $row->values : "";
            // });
            // $table->editColumn('attribute_group_name', function ($row) {
            //     return $row->attribute_group_name ? $row->attribute_group_name : "";
            // });

            // dd($table);
            return $table->make(true);
        }

        return view('admin.productAttributes.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_if(Gate::denies('product_attribute_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.productAttributes.create');
    }

    public function add($id)
    {
        // dd($id);
        return view('admin.productAttributes.attributesection', compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        Attribute::typeMap([
            'varchar' => Varchar::class,
        ]);

        app('rinvex.attributes.entities')->push(Product::class);

        // $product = Product::find(1);
        // $product->fill(['colour' =Attr1 > "red"])->save();
        $label = $request->label;
        $type = $request->type;
        $values = $request->value;
        $attr_group_name = $request->group;
        $attribute_id = [];
        $attribute_slug = [];

        foreach ($label as $data) {
            $attribute = app('rinvex.attributes.attribute')->create([
                'slug' => $data,
                'type' => "varchar",
                'name' => $data,
                'is_required' => 1,
                'entities' => ['App\Product'],
            ]);

            $attribute_id[] = $attribute->id;
            $attribute_slug[] = $attribute->slug;
        }


        for ($i = 0; $i < count($label); $i++) {
            AttributesMetadata::create([
                'slug' => $attribute_slug[$i],
                'label' => $label[$i],
                'type' => $type[$i],
                'values' => $values[$i],
                'attribute_id' => $attribute_id[$i],
                'attribute_group_name' => $attr_group_name,
            ]);
        }
        return redirect()->route('admin.product-attributes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort_if(Gate::denies('product_attribute_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $attribute = AttributesMetadata::find($id);

        return view('admin.productAttributes.show', compact('attribute'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($group_name)
    {
        abort_if(Gate::denies('product_attribute_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // $query = AttributesMetadata::find($id);
        $attributes = AttributesMetadata::where('attribute_group_name', $group_name)->get();
        // foreach($attributes as $attribute)
        // {
        // dd($attribute);
        // }
        return view('admin.productAttributes.edit', compact('attributes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $group_name)
    {

        $attributes_metadata = AttributesMetadata::where('attribute_group_name', $group_name)->get();

        foreach ($attributes_metadata as $key => $attributemetadata) {

            if (in_array($attributemetadata->slug, $request->slug)) {

                $attribute = app('rinvex.attributes.attribute')::where('slug', $attributemetadata->slug)->first();
                // $attributesmetadata = AttributesMetadata::where('slug', $attr->slug)->first();

                $attribute->name = $request->label[$key];
                $attribute->save();

                $attributemetadata->label = $request->label[$key];
                $attributemetadata->type = $request->type[$key];
                $attributemetadata->values = $request->value[$key];
                $attributemetadata->attribute_group_name =  $request->group;
                $attributemetadata->save();
            } else {

                $attributemetadata->delete();
                $attribute = app('rinvex.attributes.attribute')->find($attributemetadata->attribute_id);
                $attribute->entities()->delete();
                $attribute->delete();
            }
        }

        foreach ($request->slug as $key => $slug) {
            if ($slug == '1') {

                // echo $request->label[$key].'\n';
                $attribute = app('rinvex.attributes.attribute')->create([
                    'slug' => $request->label[$key],
                    'type' => "varchar",
                    'name' => $request->label[$key],
                    'is_required' => 1,
                    'entities' => ['App\Product'],
                ]);



                AttributesMetadata::create([
                    'slug' => $attribute->slug,
                    'label' => $request->label[$key],
                    'type' => $request->type[$key],
                    'values' => $request->value[$key],
                    'attribute_id' => $attribute->id,
                    'attribute_group_name' => $group_name,
                ]);
            }
        }
        return redirect()->route('admin.product-attributes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        abort_if(Gate::denies('product_attribute_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');


        $group_name = AttributesMetadata::find($id)->attribute_group_name;
        $attributes_metadata = AttributesMetadata::where('attribute_group_name', $group_name)->get();

        $flag = false;
        foreach ($attributes_metadata as $attribute_metadata) {
            $attribute = app('rinvex.attributes.attribute')::find($attribute_metadata->attribute_id);
            $values = $attribute->values(Varchar::class)->get();
            if (!$request->has('flag') && count($values) != 0) {
                return response()->json(['msg' => trans('global.cantDelete'), 'status' => "warning"]);
            } else {
                $attribute_metadata->delete();
                $attribute->entities()->delete();
                $attribute->delete();
                $flag = true;
            }
        }

        return  $flag ? response()->json(['msg' => trans('global.cantDelete'), 'status' => "success"]) :
            response()->json(['errors' => trans('global.cantDelete')], 424);
    }

    public function AttributeDestroy($id, Request $request)
    {
        abort_if(Gate::denies('product_attribute_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');


        $attribute = app('rinvex.attributes.attribute')->find($id);
        $values = $attribute->values(Varchar::class)->get();

        if (!$request->has('flag') && count($values) != 0) {
            return response()->json(['msg' => trans('global.cantDelete'), 'status' => "warning"]);;
        } else {
            AttributesMetadata::where('attribute_id', $attribute->id)->delete();
            $attribute->entities()->delete();
            $attribute->delete();
            return response()->json(['msg' => trans('global.cantDelete'), 'status' => "success"]);
        }

        return response()->json(['errors' => trans('global.cantDelete')], 424);
    }
}
