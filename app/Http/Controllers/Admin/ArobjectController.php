<?php

namespace App\Http\Controllers\Admin;

use App\Arobject;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyArobjectRequest;
use App\Http\Requests\StoreArobjectRequest;
use App\Http\Requests\UpdateArobjectRequest;
use App\Product;
use Gate;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class ArobjectController extends Controller
{
    use MediaUploadingTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('arobject_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Arobject::with(['product', 'team'])->select(sprintf('%s.*', (new Arobject)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'arobject_show';
                $editGate      = 'arobject_edit';
                $deleteGate    = 'arobject_delete';
                $crudRoutePart = 'arobjects';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->addColumn('product_name', function ($row) {
                return $row->product ? $row->product->name : '';
            });

            $table->editColumn('object', function ($row) {
                return $row->object ? '<a href="' . $row->object->getUrl() . '" target="_blank">' . trans('global.downloadFile') . '</a>' : '';
            });
            $table->editColumn('position', function ($row) {
                return $row->position ? Arobject::POSITION_SELECT[$row->position] : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'product', 'object']);

            return $table->make(true);
        }

        return view('admin.arobjects.index');
    }

    public function create()
    {
        abort_if(Gate::denies('arobject_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $products = Product::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.arobjects.create', compact('products'));
    }

    public function store(StoreArobjectRequest $request)
    {
        $arobject = Arobject::create($request->all());

        if ($request->input('object', false)) {
            $arobject->addMedia(storage_path('tmp/uploads/' . $request->input('object')))->toMediaCollection('object');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $arobject->id]);
        }
        return redirect()->route('admin.arobjects.index');
    }

    public function edit(Arobject $arobject)
    {
        abort_if(Gate::denies('arobject_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $products = Product::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $arobject->load('product', 'team');

        return view('admin.arobjects.edit', compact('products', 'arobject'));
    }

    public function update(UpdateArobjectRequest $request, Arobject $arobject)
    {
        $arobject->update($request->all());

        if ($request->input('object', false)) {
            if (!$arobject->object || $request->input('object') !== $arobject->object->file_name) {
                $arobject->addMedia(storage_path('tmp/uploads/' . $request->input('object')))->toMediaCollection('object');
            }
        } elseif ($arobject->object) {
            $arobject->object->delete();
        }

        return redirect()->route('admin.arobjects.index');
    }

    public function createorupdate(Request $request)
    {
        if ($request->input('object', false)) {
            $arobject = Arobject::updateOrCreate(array('position'=>0,'product_id'=>$request->product_id,'id'=>$request->obj_id));

            if ($request->input('object', false)) {
                if (!$arobject->object || $request->input('object') !== $arobject->object->file_name) {
                    $arobject->addMedia(storage_path('tmp/uploads/' . $request->input('object')))->toMediaCollection('object');
                }
            } elseif ($arobject->object) {
                $arobject->object->delete();
            }
        }

        // dd($arobject);

        return redirect()->back();
    }

    public function show(Arobject $arobject)
    {
        abort_if(Gate::denies('arobject_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $arobject->load('product', 'team');

        return view('admin.arobjects.show', compact('arobject'));
    }

    public function destroy(Arobject $arobject)
    {
        abort_if(Gate::denies('arobject_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $arobject->delete();

        return back();
    }

    public function massDestroy(MassDestroyArobjectRequest $request)
    {
        Arobject::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('arobject_create') && Gate::denies('arobject_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Arobject();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media', 'public');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }

    public function updateObject(Request $request)
    {
        abort_if(Gate::denies('arobject_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
       
        Arobject::find($request->pk)->update([$request->name => $request->value]);

        return response()->json(['success'=>'done']);

    }
}
