<?php

namespace App\Http\Controllers;
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyUserRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Schema;
use App\Role;
use App\Team;
use App\User;
use App\UserInfo;
use App\CompanyInfo;
use App\Country;
use App\State;
use App\City;
use Session;
use Gate;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;
use App\LaravelSubscriptions\Plan;
use App\LaravelSubscriptions\PlanFeature;
use App\LaravelSubscriptions\PlanSubscription;
use App\LaravelSubscriptions\PlanSubscriptionUsage;
use App\LaravelSubscriptions\PlanSubscriptionFeatures;
use App\LaravelSubscriptions\PlanSubscribedFeatures;
use App\Http\Controllers\Traits\CommonFunctionsTrait;

use Carbon\Carbon;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;
use QrCode;

class StoreUserController extends Controller
{
    public function index()
    {
        // echo 'hi';
        abort_if(Gate::denies('user_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $user = auth()->user();
        if($user->id != 1){

            $users = User::where('team_id', $user->team->id)->where('id','!=', $user->id)->get();

        }

        else{
            $users = User::all();
        }

        return view('admin.storeUser.index', compact('users'));
    }

    public function create()
    {
        abort_if(Gate::denies('user_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // $roles = Role::all()->pluck('title', 'id');
        $user = Auth::user();
        // $roles = Role::get();
        if($user->id != 1){

            $roles = Role::where('created_by', $user->id)->get()->pluck('title', 'id');
            $teams = Team::where('id', $user->id)->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');


        }
        else{

            $roles = Role::where('created_by', null )->get()->pluck('title', 'id');
            $teams = Team::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');


        }

        return view('admin.storeUser.create', compact('roles', 'teams'));

    }

    public function store(StoreUserRequest $request)
    {
        
        $auth_user = Auth::user();
        
        $teamid = $auth_user->team->id;
        // echo $teamid;
        // die;
        
        $request['approved'] = 1;
        $request['team_id'] = $teamid;
        $user = User::create($request->all());
        $user->roles()->sync($request->input('roles', []));

        $userInfo = UserInfo::create(array('user_id' => $user->id));
        
        $preview_link = ShareableLink::buildFor($userInfo);
        $publish_link = ShareableLink::buildFor($userInfo);
        $preview_link->setActive();

        $userInfo->update(array('id' => $userInfo->id,'preview_link' => $preview_link->build()->uuid,'public_link' => $publish_link->build()->uuid));
        $companyInfo = CompanyInfo::create(array('user_id' => $user->id));

        return redirect()->route('admin.storeUser.index');
    }

    public function edit($id)
    {
        $user = User::where('id', $id)->first();

        abort_if(Gate::denies('user_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $roles = Role::all()->pluck('title', 'id');

        $teams = Team::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $userInfo = UserInfo::where('user_id', $user->id)->first();
        $companyInfo = CompanyInfo::where('user_id', $user->id)->first();

        $countries = Country::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $cities = City::all()->pluck('code', 'id')->prepend(trans('global.pleaseSelect'), '');

        $states = State::all()->pluck('code', 'id')->prepend(trans('global.pleaseSelect'), '');
        // dd($userInfo);
        $user->load('roles', 'team');

        return view('admin.storeUser.edit', compact('roles', 'teams', 'user', 'userInfo', 'companyInfo','countries', 'cities', 'states'));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $user->update($request->all());
        $user->roles()->sync($request->input('roles', []));
        // return redirect()->route('admin.users.index');
        return redirect()->back();
    }

    public function accountSettings()
    {
        // abort_if(Gate::denies('user_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $user = Auth::user();

        $roles = Role::all()->pluck('title', 'id');

        $teams = Team::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $userInfo = UserInfo::where('user_id', $user->id)->first();
        $companyInfo = CompanyInfo::where('user_id', $user->id)->first();

        $countries = Country::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $cities = City::all()->pluck('code', 'id')->prepend(trans('global.pleaseSelect'), '');

        $states = State::all()->pluck('code', 'id')->prepend(trans('global.pleaseSelect'), '');
        // dd($userInfo);
        $user->load('roles', 'team');

        return view('templates.admin.account_settings', compact('roles', 'teams', 'user', 'userInfo', 'companyInfo','countries', 'cities', 'states'));
    
    }

    public function accountSettingsUpdate(Request $request)
    {
        $user = Auth::user();

        $user->update($request->all());
        // $user->roles()->sync($request->input('roles', []));

        // return redirect()->route('admin.users.index');
        return redirect()->back();
    }

    public function show($id)
    {
        // print_r($user);
        // dd('hi');
        
        // $url = url()->full();
        // $id = substr($url, -1);

        $user = User::where('id', $id)->first();
        abort_if(Gate::denies('user_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // print_r($user->load('team'));
        // dd('hi');
        $user->load('roles', 'team', 'usersCompanyInfos', 'userUserAlerts');

        return view('admin.storeUser.show', compact('user'));
    }

    public function destroy(Request $request)
    {
        $user = User::where('id', $request->id)->first();

        abort_if(Gate::denies('user_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $user->id)->delete();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $user->id)->delete();
        }
        $user->delete();

        return back();
    }

    public function massDestroy(MassDestroyUserRequest $request)
    {
        User::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function changePassword(Request $request)
    {
        if($request->create_password)
        {
            $request->validate(['create_password' => ['required']]);
            User::find($request->user_id)->update(['password'=> Hash::make($request->create_password)]);
        }
        else
        {
            $request->validate([
                'current_password' => ['required', new MatchOldPassword],
                'new_password' => ['required'],
                'new_confirm_password' => ['same:new_password'],
            ]);
            User::find($request->user_id)->update(['password'=> Hash::make($request->new_password)]);
        }
        
   
        
        return redirect()->back()->with('message','Password change successfully.');
        // dd('Password change successfully.');
    }

    // public function impersonate($id)
    // {
    //     abort_if(Gate::denies('impersonate_user'), Response::HTTP_FORBIDDEN, '403 Forbidden');
    //     $user = User::where('id', $id)->first();
    //     if ($user) {
    //         $this->setSubscriptionCheckSession($user);
    //         Auth::user()->impersonate($user);
    //     } 
    //     return redirect()->route("admin.home");
    // }

    // public function stopImpersonate()
    // {
    //     Auth::user()->leaveImpersonation();
    //     return redirect()->route('admin.users.index');
    // }

}


