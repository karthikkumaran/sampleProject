<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use App\LaravelSubscriptions\PlanSubscriptionFeatures;

class PlanSubscriptionFeaturesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(!empty(auth()->user()) && auth()->user()->getIsAdminAttribute()){
        if ($request->ajax()) {
            $query = PlanSubscriptionFeatures::all();
            $table = Datatables::of($query);
            $table->addColumn('id', '&nbsp;');
            $table->addColumn('name', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');
            
            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('slug', function ($row) {
                return $row->slug ? $row->slug    : "";
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name    : "";
            });
            $table->editColumn('description', function ($row) {
                return $row->description ? $row->description    : "";
            });
            return $table->make(true);
        }

            return view('admin.plan_subscription_features.index');
        }
        else{
            return view('errors.404');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!empty(auth()->user()) && auth()->user()->getIsAdminAttribute()){
            return view('admin.plan_subscription_features.create');
        }
        else{
            return view('errors.404');
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!empty(auth()->user()) && auth()->user()->getIsAdminAttribute()){
        $feature_name = $request->name;
        $feature_description = $request->description;
        PlanSubscriptionFeatures::create([
            'name' => $feature_name,
            'description' => $feature_description,
        ]);
        
            return view('admin.plan_subscription_features.index');
        }
        else{
            return view('errors.404');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
