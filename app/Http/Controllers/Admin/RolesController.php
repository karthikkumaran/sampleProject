<?php

namespace App\Http\Controllers\Admin;

use Gate;
use App\Role;
use App\User;
use App\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use App\Http\Requests\MassDestroyRoleRequest;
use Symfony\Component\HttpFoundation\Response;

class RolesController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('role_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // $roles = Role::all();
        // $user = Auth::user();
        $roles = Role::get();
        $user = auth()->user();

        if($user->email != $user->team->name){

            $users = User::where('email', $user->email)->with('roles')->get();    

        }
          
        return view('admin.roles.index', compact('roles', 'user'));
    }

    public function create()
    {
        abort_if(Gate::denies('role_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // $permissions = Permission::all()->pluck('title', 'id');
        //  $permissions = Permission::whereIn('id', $roles_permission)->pluck('title', 'id');
        //  print_r($permissions);
        //  exit;
         $user = Auth::user();
        $roles = Role::get();
        //  print_r(Auth::login($user));
         if($user->id !== 1){
            // $roles->created_by !=1

            $roles_permission = DB::table('permission_role')->where('role_id', '2')->get()->pluck('permission_id');

            $permissions = Permission::whereIn('id', $roles_permission)->pluck('title', 'id');
         }
         else{
            $permissions = Permission::all()->pluck('title', 'id');
         }
        
        return view('admin.roles.create', compact('permissions'));
    }

    public function store(StoreRoleRequest $request)
    {
        $user = Auth::user();
        $role = new Role;
        $role->title = $request->title;
        if($user->id !== 1){
        $role->created_by = $user->id;
    }
        $role->save();
    

        // $role = Role::create($request->all());
        $role->permissions()->sync($request->input('permissions', []));

        return redirect()->route('admin.roles.index');
    }

    public function edit(Role $role)
    {
        abort_if(Gate::denies('role_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $user = Auth::user();
        $roles = Role::get();
        //  print_r(Auth::login($user));
         if($user->id !== 1){
            // $roles->created_by !=1

            $roles_permission = DB::table('permission_role')->where('role_id', '2')->get()->pluck('permission_id');

            $permissions = Permission::whereIn('id', $roles_permission)->pluck('title', 'id');
         }
         else{
            $permissions = Permission::all()->pluck('title', 'id');

         }

         $role->load('permissions');


        // $permissions = Permission::all()->pluck('title', 'id');


        return view('admin.roles.edit', compact('permissions', 'role'));
    }

    public function update(UpdateRoleRequest $request, Role $role)
    {
        $role->update($request->all());
        $role->permissions()->sync($request->input('permissions', []));

        return redirect()->route('admin.roles.index');
    }

    public function show(Role $role)
    {
        abort_if(Gate::denies('role_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $role->load('permissions');

        return view('admin.roles.show', compact('role'));
    }

    public function destroy(Role $role)
    {
        abort_if(Gate::denies('role_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $role->delete();

        return back();
    }

    public function massDestroy(MassDestroyRoleRequest $request)
    {
        Role::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
