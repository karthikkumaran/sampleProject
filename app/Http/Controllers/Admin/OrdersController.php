<?php

namespace App\Http\Controllers\Admin;
use Log;
use App\orders;
use App\Models\OrderStatus;
use App\Models\OrderStatusRemarks;
use App\ordersProduct;
use App\ordersCustomer;
use App\Models\awbtracking;
use App\UserInfo;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Controllers\Controller;
use App\Campaign;
use App\Entitycard;
use App\Product;
use App\Transaction;
use Spatie\MediaLibrary\Models\Media;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Models\promocode_user;
use App\Telegram_Bot_Subscriber;
use Carbon\Carbon;
use WeStacks\TeleBot\TeleBot;
use Gabievi\Promocodes\Models\Promocode;
use Gabievi\Promocodes\Facades\Promocodes;
use Illuminate\Support\Facades\DB;
use App\Exports\OrdersExport;
use App\ProductVariant;
use Maatwebsite\Excel\Facades\Excel;


class OrdersController extends Controller
{
    use MediaUploadingTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        abort_if(Gate::denies('order_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        // if ($request->ajax()) {
        //     // $query1 = orders::with(['team'])->select(sprintf('%s.*', (new orders)->table));
        //     // $table1 = Datatables::of($query1);
        //     $query = ordersCustomer::with(['team'])->select(sprintf('%s.*', (new ordersCustomer)->table));
        //     $table = Datatables::of($query);

        //     $table->addColumn('placeholder', '&nbsp;');
        //     $table->addColumn('actions', '&nbsp;');

        //     $table->editColumn('actions', function ($row) {
        //         $viewGate      = 'order_show';
        //         $editGate      = 'order_edit';
        //         $deleteGate    = 'order_delete';
        //         $crudRoutePart = 'orders';
        //         $rows = $row;
        //         $row->id = $row->order->id;
        //         return view('partials.datatablesActions', compact(
        //             'viewGate',
        //             'editGate',
        //             'deleteGate',
        //             'crudRoutePart',
        //             'row'
        //         ));
        //     });

        //     $table->editColumn('id', function ($row) {
        //         // return $row->id ? $row->id : "";
        //         return $row->order->id;
        //     });
        //     $table->editColumn('campaign_name', function ($row) {
        //         return $row->campaign->name ? $row->campaign->name : "";
        //     });
        //     $table->editColumn('customer_name', function ($row) {
        //         // return $row->customer->fname ? $row->customer->fname : "";
        //         return $row->fname ? $row->fname : "";
        //     });
        //     $table->editColumn('status', function ($row) {
        //         // return orders::STATUS[$row->status];
        //         return orders::STATUS[$row->order->status];
        //     });

        //     $table->rawColumns(['actions', 'placeholder', 'file']);

        //     return $table->make(true);
        // }
        // return view('templates.admin.orders.index');
        $page = 10;
        $user_id = auth()->user()->id;
        $customers = ordersCustomer::orderBy('id', 'desc')->paginate($page);
        $userSettings = UserInfo::where("user_id", $user_id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);

        // $customers->load('order')->load('product');
        // $flag = empty($customer);
        // if (!$flag) {
        //     foreach ($customer as $order) {
        //         $products[$order->order_id] = ordersProduct::where("order_id", $order->order_id)->get();
        //     }
        // }
        return view('templates.admin.orders.index');
        // return view('templates.admin.orders.index', compact('customers', 'user_meta_data'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function calculateTotalAmount($products_qty,$products,$product_variant)
    {
        $total_amount = 0;
        $total_discount = 0;
        $total_weight = 0;
        // print_r($products);
        // // die;
        // // print_r($products_qty);
        // // die;
        // print_r($product_variant);
        // die;
        $sku_values=[];
        foreach($products_qty as $key => $sku)
        {
            array_push($sku_values,$key);
        }
        foreach ($products_qty as $key => $value) {
            // && isset($products[$key][0]) && in_array($products[$key][0]['sku'],$sku_values)
            if(!empty($products) && isset($products[$key][0]) && in_array($products[$key][0]['sku'],$sku_values))
            {

                $product_price = $products[$key][0]['price'];
                $product_discount = $products[$key][0]['discount'];
                $product_weight = $products[$key][0]['productweight'];
                $product_order_price = $product_price;
                $product_order_discount = $product_discount;
                if ($product_discount != null) {
                    $product_order_price = $product_price - ($product_price * ($product_discount / 100));
                    $product_order_discount =  $product_discount;
                }
                $temp_total = $product_order_price * (int)$value;
                $total_amount += $temp_total;
                $total_discount += $product_order_discount;
                $total_weight += $product_weight;
            }
            if(count($product_variant)>0 && isset($product_variant[$key][0])  && in_array($product_variant[$key][0]['sku'],$sku_values))
            {
                $product_price = $product_variant[$key][0]['price'];
                $product_discount = $product_variant[$key][0]['discount'];
                $product_weight = $product_variant[$key][0]['productweight'];
                $product_order_price = $product_price;
                $product_order_discount = $product_discount;
                if ($product_discount != null) {
                    $product_order_price = $product_price - ($product_price * ($product_discount / 100));
                    $product_order_discount =  $product_discount;
                }
                $temp_total = $product_order_price * (int)$value;
                $total_amount += $temp_total;
                $total_discount += $product_order_discount;
                $total_weight += $product_weight;
            }
            // print_r($total_amount);
            // die;
        }
        return ['total_amount' => $total_amount, 'total_discount' => $total_discount, 'total_weight' => $total_weight];
    }

    public function store(Request $request)
    {
    //    print_r($request->products_qty);
        $tempStorage = [];
        foreach($request->products_qty as $key =>  $value){
            $product = Product::where('sku', $key)->first();
            if(isset($product->minimum_quantity) <= $value)
            {
                $tempStorage[$key] = $value;
            }
            else if($value   <=  $product->maximum_quantity){
                $tempStorage[$key] = $value;
            }
            else{
                if($value < $product->minimum_quantity){
                    $tempStorage[$key] = $product->minimum_quantity;
                }else{
                    $tempStorage[$key] = $product->maximum_quantity;
                }
                        }
                    }
        $order_ID = ''; //temprarory value for order_ID instead of removing order_ID in notification.
        $campaign = '';
        $products = Product::whereIn('sku', array_keys($request->products_qty))->get()->groupBy('sku')->toArray();
        $product_variant=ProductVariant::whereIn('sku', array_keys($request->products_qty))->get()->groupBy('sku')->toArray();
        $total = $this->calculateTotalAmount($request->products_qty, $products,$product_variant);
        // $request['COD_total'] = $total;

        // $transaction_details = app('App\Http\Controllers\Admin\PaymentResponseController')->payment($request);
        // if ($transaction_details['flag']) {
        $public_link = $request->key;
        $catalogs = explode(",", $request->catalogs);
        $product_ids = explode(",", $request->products);
        $product_qty = $request->products_qty;
        $product_notes = $request->product_notes;
        $order_notes = $request->order_notes ?? null;
        $cmeta_data = array();
        $pmeta_data = array();
        $entity_email = array();
        $currency = $request->currency;
        $shareableLink = ShareableLink::where('uuid', $public_link)->first();
        $order_campaign = $shareableLink->shareable;
        $order_campaign_id = $order_campaign->id;
        $order_campaign = Campaign::where("id", $order_campaign_id)->first();
        // $transaction = Transaction::create([
        //     'payment_mode' => $request->payment_method, 'payment_method' => $transaction_details['method'],
        //     'order_id' => $transaction_details['order_id'],
        //     'transaction_id' => $transaction_details['trans_id'],
        //     'invoice_id' => '',
        //     'status' => 'success',
        //     'amount' => $transaction_details['amount'],
        //     'team_id' => $order_campaign->team->id,
        //     'meta_data' => serialize($transaction_details),
        // ]);

        // if ($request->input('photo', false)) {
        //     $transaction->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
        // }

        // if ($media = $request->input('ck-media', false)) {
        //     Media::whereIn('id', $media)->update(['model_id' => $transaction->id]);
        // }
        $order = orders::create(array(
            "status" => 0,
            'notes' => $order_notes,
            'campaign_id' => $order_campaign_id,
            'user_id' => $order_campaign->user->id,
            'team_id' => $order_campaign->team->id,
            'order_mode' => "Online-Storefront"
        ));

            $orderstatuses=OrderStatus::create(array(
                'order_id' => $order->id,
                'status'=> 0,
                'changed_by'=> isset(auth()->user()->id) ? auth()->user()->id :null,
            ));
            $orderstatusremark= OrderStatusRemarks::create(array(
                'order_id' => $order->id,
                'status'=> 0,
                'remarks'=> 'Your Order is Received',
                'showremarks'=>true
            ));
        // $order_total_price = 0;
        // $order_total_discount = 0;
        foreach ($catalogs as $catalog) {
            $shareableLink = ShareableLink::where('uuid', $catalog)->first();
            if (!empty($shareableLink)) {
                $campaign = $shareableLink->shareable;
                $campaign_id = $campaign->id;
                $campaign = Campaign::where("id", $campaign_id)->first();
                $campaign_name = $campaign->name;
                $pmeta_data['campaign_name'] = $campaign_name;
                $campaign_meta_data = json_decode($campaign->meta_data, true);
                $userSettings = UserInfo::where("user_id", $campaign->user->id)->first();
                $meta_data = json_decode($userSettings->meta_data, true);
                // $product_total_price = 0;
                // $product_total_discount = 0;
                $ids=array();
                $sku_values=$product_ids;
                  foreach($product_ids as $sku)
                  {
                      $sku_id=Product::where("sku",$sku)->pluck('id')->first();
                      if(empty($sku_id))
                      {
                          $sku_id=ProductVariant::where("sku",$sku)->pluck('product_id')->first();
                      }
                         array_push($ids,$sku_id);
                  }
                $product_ids=array_unique($ids);
                $EntityCard = Entitycard::whereIn('product_id', $product_ids)->get();
                array_push($entity_email, $EntityCard);
                // echo $EntityCard;
                // die;

                foreach ($EntityCard as $key => $card)  {
                    $product = $card->product;
                    if(in_array($product->sku ,$sku_values))
                    {
                        $product_category = $product->categories ? $product->categories : " ";
                        $pmeta_data['category'] = $product_category;
                        $product_stock = $product->stock;
                        if ($product_stock != null) {
                            $product_new_stock = $product_stock - $product_qty[$product->sku];
                            Product::where("id", $product->id)->update(array("stock" => $product_new_stock));
                        }
                        if(isset($product->sku) &&$product->has_variants==0)
                        {
                              $orderProduct = ordersProduct::create(array(
                                "order_id" => $order->id,
                                "product_id" => $product->id,
                                "price" => $product->price,
                                "qty" => $request->products_qty[$product->sku],
                                "sku" => $product->sku,
                                "name" => $product->name,
                                "discount" => $product->discount,
                                "product_variant_id"=>null,
                                "notes" => array_key_exists($product->sku, $product_notes) ? $product_notes[$product->sku] : null, "meta_data" => json_encode($pmeta_data), 'campaign_id' => $campaign_id, 'user_id' => $campaign->user->id, 'team_id' => $campaign->team->id
                            ));
                        }
                    }
                    else{
                         $product_variant=ProductVariant::where('product_id',$product->id)->get();
                         $variant=array();
                        foreach($product_variant as $variant_product)
                        {
                            if(in_array($variant_product->sku,$sku_values))
                            {
                                array_push($variant,$variant_product);
                            }
                        }
                                foreach($variant as $product_variants)
                                {
                                  $orderProduct = ordersProduct::create(array(
                                    "order_id" => $order->id,
                                    "product_id" => $product->id,
                                    "price" => $product_variants->price,
                                    "qty" => $request->products_qty[$product_variants->sku],
                                    "sku" => $product_variants->sku,
                                    "name" => $product_variants->name,
                                    "discount" => $product_variants->discount,
                                    "product_variant_id"=> $product_variants->id,
                                    "notes" => array_key_exists($product_variants->sku, $product_notes) ? $product_notes[$product_variants->sku] : null, "meta_data" => json_encode($pmeta_data), 'campaign_id' => $campaign_id, 'user_id' => $campaign->user->id, 'team_id' => $campaign->team->id
                                ));
                                }
                    }
                }
                // $order_total_discount += $product_total_discount;s
                // $order_total_price += $product_total_price;
            }
        }
        // $cmeta_data['product_total_price'] = $order_total_price;
        // $cmeta_data['product_total_discount'] = $order_total_discount;
        $cmeta_data['product_total_price'] = $total['total_amount'];
        $cmeta_data['coupon_applied'] = false;
        //apply promocode
        $sphylite_amount = $request->sphylite_amount;
        if($request->coupon_code_applied=='true')
        {
            promocode_user::create(
                ['promocode_id'=>$request->coupon_id,
                'customer_id'=>auth()->guard('customer')->user()->id,
                'used_at'=>Carbon::now()]);
            $cmeta_data['coupon_applied'] = true;
            $cmeta_data['coupon_id'] = $request->coupon_id;
            $cmeta_data['coupon_code'] = $request->couponCode;
            $cmeta_data['coupon_discount_amount'] = $request->coupon_discount_amount;
            $cmeta_data['product_total_price']= $cmeta_data['product_total_price'] - $request->coupon_discount_amount;
        }

        $cmeta_data['product_total_discount'] = $total['total_discount'];
        $cmeta_data['currency'] = $currency;
        $cmeta_data['shipping_charges'] = $request->shipping_charges;
        $cmeta_data['tax_amount'] = $request->tax_amount;
        $cmeta_data['tax_percentage'] = $request->tax_percentage;
        $extra_charges_names = explode(",", $request->extra_charges_names);
        $extra_charges_values = explode(",", $request->extra_charges_values);
        $cmeta_data['extra_charges_names'] = $extra_charges_names;
        $cmeta_data['extra_charges_values'] = $extra_charges_values;
        $total_price = $cmeta_data['product_total_price'];


        if(isset($cmeta_data['shipping_charges']) && $cmeta_data['shipping_charges'] > 0)
        {
            $total_price = (float)$total_price + (float)number_format($cmeta_data['shipping_charges'] ,2) ;
        }
        if(isset($cmeta_data['extra_charges_values']) && count($cmeta_data['extra_charges_values']) > 0)
        {
            foreach($cmeta_data['extra_charges_values'] as $key)
            {
                if(!empty($key))
                $total_price = (float)$total_price + (float)number_format($key ?? 0 ,2);
            }
        }
        if(isset($cmeta_data['tax_percentage']) && $cmeta_data['tax_percentage'] > 0)
        {
            $total_price = (float)$total_price + (float)number_format($cmeta_data['tax_amount'] ?? 0 ,2);
        }
        if(isset($sphylite_amount) && $sphylite_amount!=0)
        {
            $total_price = (float)$total_price + (float)number_format($sphylite_amount ,2) ;
        }

        $cmeta_data['Shyplite_price'] = $sphylite_amount;
        $cmeta_data['total_price'] = $total_price;

        // if(isset($meta_data['shipping_settings']['shipping_method']) && $meta_data['shipping_settings']['shipping_method'] != "shipping"){
        //     $customer = array(
        //         "fname" => $request->fname,
        //         "email" => $request->email,
        //         "address1" => $request->apt_number,
        //         "address2" => $request->landmark,
        //         "city" => $request->city,
        //         "pincode" => $request->pincode,
        //         "state" => $request->state,
        //         "country" => "india",
        //         "country_code" => $request->country_code,
        //         "mobileno" => $request->mnumber);
        //     $shippingOrder=app('App\Http\Controllers\Admin\ShippingController')->orderbooked( $order->id,$customer);
        //     $shiporder_decode = json_decode($shippingOrder);
        //     $awb = $shiporder_decode->awb;

        //     $shippingTrack=app('App\Http\Controllers\Admin\ShippingController')->getTrackEvent($awb);
        //     $shiptrack_data = json_decode($shippingTrack);
        //     $awbtrack = awbtracking::create(array(
        //         "awb_no" => $shiptrack_data->data->awbNo,
        //         "carrier_name" => $shiptrack_data->data->carrierName,
        //         "awb_id" => $shiptrack_data->data->allData->id,
        //         "child_awb_id" => $shiptrack_data->data->allData->childAwbID,
        //         "shippingorderid" => $order->id,
        //         "return" => $shiptrack_data->data->allData->returnOrReverse,
        //         "eventstatus" => $shiptrack_data->data->events[0]->status,
        //         "remark" => $shiptrack_data->data->events[0]->Remarks,
        //         "location" => $shiptrack_data->data->events[0]->Location,
        //         "eventtime" => $shiptrack_data->data->events[0]->Time
        //     ));
        //     $cmeta_data['Shyplite_order'] = $shippingOrder;
        //     $cmeta_data['Shyplite_track'] = $shippingTrack;

        // }

        // if(isset($meta_data['shipping_settings']['shipping_method']))
        // {
        //     $shippingvalue = 0;
        //     $source_code = $meta_data['footer_settings']['pincode'];
        //     $destination_code = $request->pincode;
        //     $totalweight = $total['total_weight'];
        //     $public_key_id = $meta_data['shipping_settings']['shipping']['public-key-id'];
        //     $app_id = $meta_data['shipping_settings']['shipping']['sp-app-id'];
        //     $sell_id = $meta_data['shipping_settings']['shipping']['sp-sell-id'];
        //     $private_key = $meta_data['shipping_settings']['shipping']['sp-private-key'];

            // $shippingPrice=app('App\Http\Controllers\Admin\ShippingController')->price_calculator($public_key_id, $app_id, $sell_id, $private_key,$destination_code,$source_code);
            // $shippingPrice=json_decode($shippingPrice);
            // $pricing =  (array) $shippingPrice->pricing;
            // $service = (array) $shippingPrice->service;
            // foreach($service as $item => $val)
            // {
            //     if($val == true)
            //     {
            //         foreach($pricing as $pitem => $pval)
            //         {
            //             if($item == $pitem)
            //             {
            //                 // $int_var = preg_replace('/[^0-9]/','', $item);
            //                 $int_var = (float) filter_var( $item, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
            //                 if($int_var!= "")
            //                 {
            //                     $weightvalue = substr($int_var, 1);
            //                     if($totalweight > $weightvalue)
            //                     {
            //                         $shippingvalue = $pval;
            //                         break 2;
            //                     }
            //                 }
            //             }
            //         }
            //     }
            // }

            // $cmeta_data['Shyplite_price'] = $shippingvalue;
            // $total_price += $shippingvalue;
            // $cmeta_data['total_price'] = $total_price;


            // foreach($shippingPrice as $item)
            // {
            //     foreach($item[1] as $key)
            //     {
            //         dd($key);
            //     }
            // }

            // $shippingLabel=app('App\Http\Controllers\Admin\ShippingController')->getShipmentSlip($public_key_id, $app_id, $sell_id, $private_key);
        // }

        $customerData = array(
            "fname" => $request->fname,
            "email" => $request->email,
            "address1" => $request->apt_number,
            "address2" => $request->landmark,
            "city" => $request->city,
            "pincode" => $request->pincode,
            "state" => $request->state,
            "country" => "india",
            "country_code" => $request->country_code,
            "mobileno" => $request->mnumber,
            "amobileno" => $request->amnumber,
            "addresstype" => $request->addresstype,
            "delivery_note"=>$request->delivery_note,
            'order_id' => $order->id,
            'meta_data' => json_encode($cmeta_data),
            'campaign_id' => $order_campaign_id,
            'user_id' => $order_campaign->user->id,
            'team_id' => $order_campaign->team->id,
            'customer_id' => auth()->guard('customer')->check() ? auth()->guard('customer')->user()->customer_id :'',
        );

        $ordersCustomer = ordersCustomer::create($customerData);

        if (empty($meta_data['settings']['emailid']))
            $emailFrom = $order_campaign->user->email;
        else
            $emailFrom = $meta_data['settings']['emailid'];

        if (empty($meta_data['settings']['emailfromname']))
            $emailFromName = $order_campaign->user->name;
        else
            $emailFromName = $meta_data['settings']['emailfromname'];

        if (!empty($meta_data['storefront']['storename']))
            $emailSubject = $meta_data['storefront']['storename'] . " - Your order is received";
        else
            $emailSubject = "Your order is received";

        $data['order']=true;
        $data['userSettings'] = $userSettings;
        $data['product_qty'] = $product_qty;
        $data['product_notes'] = $product_notes;
        $data['order_notes'] = $order_notes;
        $data['customerData'] = $ordersCustomer;
        $data['meta_data'] = $meta_data;
        $data['campaign'] = $order_campaign;
        $data['entity'] = $entity_email;
        $data['toMail'] = $request->email;
        $data['toBcc'] = $emailFrom;
        $data['toName'] = $request->fname;
        $data['subject'] = $emailSubject;
        $data['fromMail'] = $emailFrom;
        $data['fromName'] = $emailFromName;
        $data['mnumber'] = $request->mnumber;
        $data['address'] = $request->apt_number . ",<br>" . $request->landmark . ",<br>" . $request->city . " - " . $request->pincode . ",<br>" . $request->state;
        $response = app('App\Http\Controllers\Admin\MailController')->orderplace_email($data, $order);


        $notification_request=new Request();
        $notification_request->setMethod('POST');
        $notification_request->request->add(
            array("user_id"=>$campaign->user->id,
            "title"=>'Order id: #'.$order_ID,
            "body"=>'Your have received an order from '.$request->fname.'. Order Number is #'.$order_ID.' Login to your SimpliSell account to process the order.')
        );

    if(!isset($meta_data['notification_settings']['order_notification']) ){
        $notification=app('App\Http\Controllers\Admin\NotificationController')->sendNotification($notification_request);
    }else{
        if( $meta_data['notification_settings']['order_notification'] != "off"){
            $notification=app('App\Http\Controllers\Admin\NotificationController')->sendNotification($notification_request);
        }
    }


        $bot = new TeleBot(env('TELEGRAM_BOT_TOKEN'));
        $telegram_bot_subscribers = Telegram_Bot_Subscriber::where("user_id", $order_campaign->user->id)->get();

        if(!empty($telegram_bot_subscribers)){

        $telegram_text = "Your store has received a new order #". $order_ID ."\n"
        . "<b><a href='". route('admin.orders.show',$order->id) . "'>Click here to view the order.</a></b>\n";


        foreach ($telegram_bot_subscribers as $subscriber) {
            $bot->sendMessage([
                'chat_id' => $subscriber->telegram_chat_id,
                'parse_mode' => 'HTML',
                'text' => $telegram_text,
            ]);
        }
    }


        if ($response->{'original'}['order_id']) {
            $order_id = $response->{'original'}['order_id'];
            return ['order_id' => $order_id];
        }

    }


    public function store_transaction(Request $request)
    {
        $product_variant=ProductVariant::whereIn('sku', array_keys($request->products_qty))->get()->groupBy('sku')->toArray();
        $products = Product::whereIn('sku', array_keys($request->products_qty))->get()->groupBy('sku')->toArray();
        $total = $this->calculateTotalAmount($request->products_qty, $products,$product_variant);
        // print_r($total);
        // die;
        $customer = ordersCustomer::where('order_id',$request->store_order_id)->first();
        $cmeta_data = json_decode($customer->meta_data, true);
        $request['COD_total'] = $total;
        if($request->coupon_code_applied=='true')
        {
            $total['total_amount'] = (int)$total['total_amount'] - $request->coupon_discount_amount;
        }
        if(isset($request->shipping_charges))
        {
            $total['total_amount'] += $request->shipping_charges;
        }
        if(isset($request->tax_percentage))
        {
            $total['total_amount'] += $request->tax_amount;
        }
        $extra_charges_names = explode(",", $request->extra_charges_names);
        $extra_charges_values = explode(",", $request->extra_charges_values);
        if(isset($extra_charges_values))
        {
            foreach($extra_charges_values as $key)
            {
                $total['total_amount'] += (int)$key;
            }
        }
        if(isset($cmeta_data['Shyplite_price']))
        {
            $total['total_amount'] += (int)$cmeta_data['Shyplite_price'];

        }

        $request['COD_total'] = $total;

        $transaction_details = app('App\Http\Controllers\Admin\PaymentResponseController')->payment($request);

        $shareableLink = ShareableLink::where('uuid', $request->key)->first();
        $order_campaign = $shareableLink->shareable;

        $transaction = Transaction::create([
            'payment_mode' => $request->payment_method,
            'payment_method' => $transaction_details['method'],
            'order_id' => $transaction_details['order_id'],
            'transaction_id' => $transaction_details['trans_id'],
            'invoice_id' => '',
            'status' => 'success',
            'amount' => $transaction_details['amount'],
            'team_id' => $order_campaign->team->id,
            'meta_data' => serialize($transaction_details),
        ]);

        // $order_ref_id = orders::where('team_id',$order_campaign->team->id)->whereNotNull('transaction_id')->count()+1;
        $ref_id = orders::where('team_id',$order_campaign->team->id)->whereNotNull('transaction_id')->count();
        if(!empty($ref_id))
        {
        $ref_last_id = orders::select('order_ref_id')->where('team_id',$order_campaign->team->id)->whereNotNull('transaction_id')->latest()->first();
        $ref_prev_id = $ref_last_id->order_ref_id;
        if(!empty($ref_prev_id)) {
            $order_ref_id = ($ref_id == $ref_prev_id)?($ref_id + 1):($ref_prev_id + 1);
        }
        else {
            $order_ref_id = ($ref_id + 1);
        }
        }
        else{
            $order_ref_id = 1;
        }

        Orders::find($request->store_order_id)->update(['transaction_id' => $transaction->id, 'order_ref_id' => $order_ref_id]);

        if ($request->input('photo', false)) {
            $transaction->addMedia(storage_path('tmp/uploads/' . $request->input('photo')))->toMediaCollection('photo');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $transaction->id]);
        }

        return ['key' => $request->key, 'order_id' => $request->store_order_id];
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function show($orders)
    {
        abort_if(Gate::denies('order_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        // $orders->load('team');
        // $order = orders::where("id", $orders)->first();
        // $products = ordersProduct::where("order_id", $orders)->get();

        $transaction = Orders::find($orders)->transactions()->get();
        $transaction = count($transaction) == 0 ? null : $transaction[0];
        $customer = ordersCustomer::where("order_id", $orders)->first();
        $userSettings = UserInfo::where("user_id", auth()->user()->id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);
        return view('templates.admin.orders.show', compact('customer', 'user_meta_data', 'transaction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function edit(orders $orders)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, orders $orders)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\orders  $orders
     * @return \Illuminate\Http\Response
     */
    public function destroy(orders $orders)
    {
        //
    }

    public function massDestroy(orders $request)
    {
    }

    public function ordersajaxadata(Request $request)
    {
        abort_if(Gate::denies('order_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $page = 10;
        $userSettings = UserInfo::where("user_id", auth()->user()->id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);
        $status=$request->status ?? '*';
        $user_id = auth()->user()->id;

        if ($request->ajax()) {

            $q = $request->q;
            $products = [];

            if (isset($request->s) && !empty($request->s)) {
                $sort = explode(',', $request->s);
            }
            if (isset($request->q) && !empty($request->q)) {

                // $customers = ordersCustomer::select('*')->leftJoin('orders', function($join){
                //     $join->on('orders.id','=','orders_customers.order_id');
                //     })->where('orders.status','=',$status)
                //     ->Where('orders_customers.order_id', 'LIKE', $q . '%')
                //     ->where('orders_customers.email', "LIKE", '%' . $q . "%")
                //     ->where('orders_customers.mobileno', "LIKE", '%' . $q . "%")
                //     ->where('orders_customers.fname', "LIKE", '%' . $q . "%")
                //     // ->where('orders_customers.lname', "LIKE", '%' . $q . "%")
                //     ->where('orders_customers.created_at', "LIKE", '%' . $q . "%")
                //     ->orderBy('orders_customers.id', 'desc')
                //     ->paginate($page);

                $customers = ordersCustomer::Where('order_id', 'LIKE', $q . '%')
                    ->orwhere('email', "LIKE", '%' . $q . "%")
                    ->orwhere('mobileno', "LIKE", '%' . $q . "%")
                    ->orwhere('fname', "LIKE", '%' . $q . "%")
                    ->orwhere('lname', "LIKE", '%' . $q . "%")
                    ->orwhere('created_at', "LIKE", '%' . $q . "%")
                    ->orderBy('id', 'desc')
                    ->paginate($page);

            } else {
                $customers = ordersCustomer::orderBy('id', 'desc')->paginate($page);
            }

            return view("cards.orders_main", compact('customers', 'user_meta_data','status'));
        }
        // return view("templates.admin.customer.orders");
    }

    public function updateOrderStatus(Request $request)
    {
        orders::find($request->order_id)->update(['status' => $request->order_status]);
    }

    public function shypliteOrderBook(Request $request)
    {
        $order = orders::where('order_ref_id',$request->orderid)->first();
        $customerdata = ordersCustomer::where('order_id',$order->id)->first();
        $customer = array(
            "fname" => $customerdata->fname,
            "email" => $customerdata->email,
            "address1" => $customerdata->address1,
            "address2" => $customerdata->address2,
            "city" => $customerdata->city,
            "pincode" => $customerdata->pincode,
            "state" => $customerdata->state,
            "country" => $customerdata->country,
            "mobileno" => $customerdata->mobileno);
        $orderdata = array(
            'orderid' => $request->orderid,
            'ordertype' => $request->ordertype,
            'modetype' => $request->modetype,
            'packagelength' => $request->packagelength,
            'packagewidth' => $request->packagewidth,
            'packageheight' => $request->packageheight,
            'packageweight' => $request->packageweight
        );
           $shippingOrder=app('App\Http\Controllers\Admin\ShippingController')->orderbooked( $request->orderid,$customer,$orderdata);
        //    return $shippingOrder;
        // $shippingOrder = "{"success":true,"message":"Order successfuly saved and booked.","awb":"1263211119241","carrier":"Xpressbees","manifestID":1026943,"slip":[{"s3Path":["https://sl-sellerfiles-7896542589.s3.ap-southeast-1.amazonaws.com/143702/slips/XB1263211119241.pdf?AWSAccessKeyId=AKIAJ62P4XWZCIYQ7FYA&Expires=1635250003&Signature=PBKtwoWHU4inTI32IyXptu99Wms%3D"],"fileName":"XB1263211119241.pdf","shipmentID":"18175768","orderID":"464","notFound":false}]}";
            // $shippingOrder = "{\"success\":true,\"message\":\"Order successfuly saved and booked.\",\"awb\":\"700016018698\",\"carrier\":\"DTDC-Dotzot\",\"manifestID\":1004411,\"slip\":[{\"s3Path\":[\"https:\/\/sl-sellerfiles-7896542589.s3.ap-southeast-1.amazonaws.com\/143702\/slips\/DZ700016018698.pdf?AWSAccessKeyId=AKIAJ62P4XWZCIYQ7FYA&Expires=1634205606&Signature=buwfsAj8t%2BWmVMKhQOrIMKKLB80%3D\"],\"fileName\":\"DZ700016018698.pdf\",\"shipmentID\":\"17823847\",\"orderID\":\"435\",\"notFound\":false}]}";
            $shiporder_decode = json_decode($shippingOrder);

            $awb = $shiporder_decode->awb;

            $shippingTrack=app('App\Http\Controllers\Admin\ShippingController')->getTrackEvent($awb);
            $shiptrack_data = json_decode($shippingTrack);
            if(!empty($shiptrack_data->data->events)){
                $awbtrack = awbtracking::create(array(
                    "awb_no" => $shiptrack_data->data->awbNo,
                    "carrier_name" => $shiptrack_data->data->carrierName,
                    "awb_id" => $shiptrack_data->data->allData->id,
                    "child_awb_id" => $shiptrack_data->data->allData->childAwbID,
                    "shippingorderid" => $request->orderid,
                    "return" => $shiptrack_data->data->allData->returnOrReverse,
                    "eventstatus" => $shiptrack_data->data->events[0]->status,
                    "remark" => $shiptrack_data->data->events[0]->Remarks,
                    "location" => $shiptrack_data->data->events[0]->Location,
                    "eventtime" => $shiptrack_data->data->events[0]->Time
                ));
            }
            else{
                $awbtrack = awbtracking::create(array(
                    "awb_no" => $shiptrack_data->data->awbNo,
                    "carrier_name" => $shiptrack_data->data->carrierName,
                    "awb_id" => $shiptrack_data->data->allData->id,
                    "child_awb_id" => $shiptrack_data->data->allData->childAwbID,
                    "shippingorderid" => $request->orderid,
                    "return" => $shiptrack_data->data->allData->returnOrReverse,
                    "eventstatus" => '',
                    "remark" => '',
                    "location" => '',
                    "eventtime" => ''
                ));
            }
             $customermetadata = json_decode($customerdata->meta_data);
             $customeradd = $customermetadata;
            $customeradd->Shyplite_order = $shippingOrder;
            // $customeradd->Shyplite_track = "{\"data\":{\"awbNo\":\"700016018681\",\"carrierName\":\"DTDC-DOTZOT\",\"allData\":{\"id\":10638134,\"awbNo\":\"700016018681\",\"childAwbID\":null,\"returnOrReverse\":false,\"expectedDeliveryDate\":null,\"updated\":\"2021-10-14T09:46:53.000Z\",\"carrierName\":\"DTDC-DOTZOT\",\"cancelled\":false},\"events\":[{\"status\":\"SB\",\"Remarks\":\"Shipment Booked - SL\",\"Location\":null,\"Time\":\"2021-10-14T15:16:53.346Z\"}]}}";
            $customeradd->Shyplite_track = $shippingTrack;
            $customerencode = json_encode($customeradd);
            ordersCustomer::find($customerdata->id)->update(['meta_data' => $customerencode]);
            return true;
    }

    public function shypliteOrderTrack(Request $request){
        $shippingTrack=app('App\Http\Controllers\Admin\ShippingController')->getTrackEvent($request->awbno);
        $shiptrack_data = json_decode($shippingTrack);

        $shipawbdata = awbtracking::where('awb_no',$request->awbno)->get();
        foreach($shiptrack_data->data->events as $key => $value){
            $num = 0;
            foreach($shipawbdata as $ship){
                if($ship->eventstatus==$value->status && $ship->carrier_name == $shiptrack_data->data->carrierName){
                    $num = 1;
                }
            }
                if($num == 0){
                $awbtrack = awbtracking::create(array(
                    "awb_no" => $shiptrack_data->data->awbNo,
                    "carrier_name" => $shiptrack_data->data->carrierName,
                    "awb_id" => $shiptrack_data->data->allData->id,
                    "child_awb_id" => $shiptrack_data->data->allData->childAwbID,
                    "shippingorderid" => $request->orderid,
                    "return" => $shiptrack_data->data->allData->returnOrReverse,
                    "eventstatus" => $value->status,
                    "remark" => $value->Remarks,
                    "location" => $value->Location,
                    "eventtime" => $value->Time
                ));
            }

        }
        return true;

    }


    public function updateGenerateInvoice(Request $request)
    {
    }

    //apply promocode function
    public function ApplyCouponCode(Request $request)
    {
        $public_link=$request->shareable_link;
        $shareableLink = ShareableLink::where('uuid', $public_link)->first();
        $team_id = $shareableLink->shareable->team_id;

        $coupon=Promocode::where([['code',$request->code],['disable',0],['data->team_id',$team_id]])->first();
        if(!auth()->guard('customer')->user()){
            return ["status"=>'error',"login"=>true,"msg"=>"You should be logged into use the coupon code!"];
        }

        if(empty($coupon)){
            return ["status"=>'error',"msg"=>'Invalid Coupon Code!'];
        }

        $start_date=Carbon::createFromDate($coupon->data['start_date']);
        $end_date=Carbon::createFromDate($coupon->data['end_date']);


        if(Carbon::now() >= $start_date && Carbon::now() <= $end_date)
        {
            if($request->amount <= $coupon->data['min_cart_value'])
                return ["status"=>'min_cart_value', 'min_cart_value'=>$coupon->data['min_cart_value']];
                $user_id=auth()->guard('customer')->user()->id;
                $response=promocode_user::where([['promocode_id',$coupon->id],['customer_id',$user_id]])->count();
                if($response >= $coupon->data['usage_limit'])
                {
                    return ["status"=>'error', "msg"=>"You have already used." ];
                }

                return ["status"=>'success', "msg"=>"Coupon code applied - " ,"desc" => $coupon->data['description'] ,"id" => $coupon->id ,"discount_amount"=>$coupon->reward,"unit"=>$coupon->data['unit'],'min_cart_value'=>(double)$coupon->data['min_cart_value']];
        }else{
            return ["status"=>'error',"msg"=>'Coupon code expired!'];
        }
    }

    public function exportOrders(Request $request )
    {
        // return $request;
        // $filePath = public_path("name_of_your_file.txt");
       $starDate='';
        $endDate ='';
        if($request->range_type == 1){
            $startDate = date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
               $endDate = date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
        }
        else if($request->range_type == 2){
            $startDate = date('Y-m-d', strtotime(Carbon::yesterday()->format('Y-m-d')));
               $endDate = date('Y-m-d', strtotime(Carbon::yesterday()->format('Y-m-d')));
        }
        else if($request->range_type == 3){
            $startDate = date('Y-m-d', strtotime(Carbon::now()->subDays(7)->format('Y-m-d')));
               $endDate = date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
        }
        else if($request->range_type == 4){
            $startDate = date('Y-m-d', strtotime(Carbon::now()->subDays(30)->format('Y-m-d')));
               $endDate = date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
        }
        else if($request->range_type == 5){
            $startDate = date('Y-m-d', strtotime(Carbon::now()->month()->format('Y-m-d')));
               $endDate = date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
        }
        else if($request->range_type == 6){
            $startDate = date('Y-m-d', strtotime(Carbon::now()->year()->format('Y-m-d')));
               $endDate = date('Y-m-d', strtotime(Carbon::now()->format('Y-m-d')));
        }
        else if($request->range_type == 7){

            $startDate = date('Y-m-d', strtotime($request->startdate));
               $endDate = date('Y-m-d', strtotime($request->enddate));
        }
        //return $datas =  ordersCustomer::with('orderProducts')->select('name','sku','qty','discount')->get();
                            //    $data = Post::where('status', 1)
                            //   ->with(['comments' => function($query) {
                            //                  $query->select('id','body');
                            //                    }])->get();
      $datas =  ordersCustomer::whereDate('created_at', '>=', $startDate)->whereDate('created_at', '<=', $endDate)
    // ->with(['orderProducts' => function($query){
    //     $query->select('order_id',  'id','name','sku','qty');
    // }])
    ->with('orderProducts:order_id,id,name,qty,sku')
    ->select('order_id','meta_data','fname','lname','address1','address2','city','pincode','state','country','email','country_code','mobileno','amobileno','addresstype')->get();

       $formated =  array();
            if(!empty($datas)){
                foreach($datas as $data){
                    $data->meta_data =json_decode($data->meta_data ,true);
                    $data->product_total_price = $data->meta_data['product_total_price'];
                    $data->coupon_applied = $data->meta_data['coupon_applied'] ??  null;
                    $data->product_total_discount = $data->meta_data['product_total_discount'];
                    $data->total_price = $data->meta_data['total_price'];
                    foreach($data->orderProducts as $key =>  $orders){
                        $formated[$key]['name'] = $orders->name;
                        $formated[$key]['qty'] = $orders->qty;
                        $formated[$key]['sku'] = $orders->sku;

                    }
                   if ( $data->lname == null)
                   {

                       $data->lname = "NA";
                   }
                   if ( $data->coupon_applied == null)
                   {
                       $data ->coupon_applied = 'NA';
                   }
                   if ( $data->product_total_discount == null)
                   {
                       $data ->product_total_discount ='NA';
                   }

                    $data->order_lists =  str_replace(['[',']','\\'  ] ,"" ,json_encode($formated));

                    unset($data->meta_data);
                    unset($data->orderProducts);
                    $formatedDataCollection[] = [
                        'order_id' => $data->order_id,
                        'product_total_price' => $data->product_total_price,
                        'product_total_discount' => $data->product_total_discount,
                        'coupon_applied' => $data->coupon_applied,
                        'total_price' => $data->total_price,
                        'order_lists' => $data->order_lists,
                        'fname' => $data->fname,
                        'lname' => $data->lname,
                        'address1' => $data->address1,
                        'address2' => $data->address2,
                        'city' => $data->city,
                        'pincode' => $data->pincode,
                        'state' => $data->state,
                        'country' => $data->country,
                        'email' => $data->email,
                        'country_code' =>$data->country_code,
                        'mobileno' => $data->mobileno,
                        'amobileno' => $data->amobileno,
                        'addresstype' => $data->addresstype,
                    ];

                }

            }
           $datas = collect($formatedDataCollection);

            if($request->file_format == 1){
                return Excel::download(new OrdersExport($datas), 'users.xlsx');
            }
            else if ($request ->file_format == 2){
                return Excel::download(new OrdersExport($datas), 'users.xls');
            }
            else if ($request ->file_format == 3){
                return Excel::download(new OrdersExport($datas), 'users.csv');
            }

    }
}



