<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;
use Razorpay\Api\Api;
use Illuminate\Support\Str;
use App\Product;
use App\ProductVariant;
use App\UserInfo;
use App\Campaign;
use App\orders;

// use Sample\PayPalClient;
// use PayPalCheckoutSdk\Core\PayPalHttpClient;
// use PayPalCheckoutSdk\Core\SandboxEnvironment;
// use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
// use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;
// use URL;

class PaymentController extends Controller
{

    public function userMetaData($key)
    {
        $public_link = $key;
        $shareableLink = ShareableLink::where('uuid', $public_link)->first();
        $user_id = $shareableLink->shareable->user_id;
        $paymentSettings = UserInfo::select('meta_data')->where("user_id", $user_id)->first();
        $meta_data = json_decode($paymentSettings->meta_data, true);

        return $meta_data;
    }

    public function calculateTotalAmount($products_qty,$products,$product_variant)
    {
        $total_amount = 0;
        $total_discount = 0;
        $total_weight = 0;
        $sku_values=[];
        foreach($products_qty as $key => $sku)
        {
            array_push($sku_values,$key);
        }
        foreach ($products_qty as $key => $value) {
            // && isset($products[$key][0]) && in_array($products[$key][0]['sku'],$sku_values)
            if(!empty($products) && isset($products[$key][0]) && in_array($products[$key][0]['sku'],$sku_values))
            {

                $product_price = $products[$key][0]['price'];
                $product_discount = $products[$key][0]['discount'];
                $product_weight = $products[$key][0]['productweight'];
                $product_order_price = $product_price;
                $product_order_discount = $product_discount;
                if ($product_discount != null) {
                    $product_order_price = $product_price - ($product_price * ($product_discount / 100));
                    $product_order_discount =  $product_discount;
                }
                $temp_total = $product_order_price * (int)$value;
                $total_amount += $temp_total;
                $total_discount += $product_order_discount;
                $total_weight += $product_weight;
            }
            if(count($product_variant)>0 && isset($product_variant[$key][0])  && in_array($product_variant[$key][0]['sku'],$sku_values))
            {
                $product_price = $product_variant[$key][0]['price'];
                $product_discount = $product_variant[$key][0]['discount'];
                $product_weight = $product_variant[$key][0]['productweight'];
                $product_order_price = $product_price;
                $product_order_discount = $product_discount;
                if ($product_discount != null) {
                    $product_order_price = $product_price - ($product_price * ($product_discount / 100));
                    $product_order_discount =  $product_discount;
                }
                $temp_total = $product_order_price * (int)$value;
                $total_amount += $temp_total;
                $total_discount += $product_order_discount;
                $total_weight += $product_weight;
            }
           
        }
        // return ['total_amount' => $total_amount, 'total_discount' => $total_discount, 'total_weight' => $total_weight]
        return $total_amount;
    }

    public function payment(Request $request)
    {
        if ($request->payment_method == 'Razorpay') {
            return $this->razorpay($request);
        }

        if ($request->payment_method == 'Paypal') {
            return $this->paypal($request);
        }

        if ($request->payment_method == 'UPI') {
            return $this->upi($request);
        }

        if ($request->payment_method == 'Cash on delivery') {
            //  return $request->products_qty;
            return $this->cashOnDelivery($request);
        }

    }

    public function razorpay(Request $request)
    {

        //rp-invoice-items (start)
        // $items[] = [
        //     "name" => $products[$key][0]['name'], "description" => $products[$key][0]['description'], "amount" => $temp_total*100,
        //     'currency' => $meta_data['settings']['currency'], 'quantity' => (int)$value
        // ];
        //rp-invoice-items (end)

        $meta_data = $this->userMetaData($request->key);
        $products = Product::whereIn('sku', array_keys($request->products_qty))->get()->groupBy('sku')->toArray();
        $product_variant=ProductVariant::whereIn('sku', array_keys($request->products_qty))->get()->groupBy('sku')->toArray();
        $total_amount = $this->calculateTotalAmount($request->products_qty, $products,$product_variant);
        // $total_amount = $this->calculateTotalAmount($request->products_qty);

        if($request->coupon_code_applied=='true')
        {
            $total_amount=$total_amount-$request->coupon_discount_amount;
        }
        if(isset($request->shipping_charges))
        {
            $total_amount += $request->shipping_charges;
        }

        if(isset($request->tax_percentage))
        {
            $total_amount += $request->tax_amount;
        }
        $extra_charges_names = explode(",", $request->extra_charges_names);
        $extra_charges_values = explode(",", $request->extra_charges_values);
        if(isset($extra_charges_values))
        {
            foreach($extra_charges_values as $key)
            {
                $total_amount += (int)$key;
            }
        }
        if(isset($request->sphylite_amount))
        {
            $total_amount += $request->sphylite_amount;
        }
        

        $api = new Api($meta_data['payment_settings'][$request->payment_method]['rp-key-id'], $meta_data['payment_settings'][$request->payment_method]['rp-secret-key']);
        $receipt_id = Str::random(20);

        //rp-invoice-generations (start)
        // $invoice  = $api->invoice->create(array(
        //     "type" => "invoice",
        //     "customer" => array(
        //         'name' => $request->fname,
        //         'email' => $request->email,
        //         'contact' => $request->mnumber
        //     ),
        //     'line_items' => $items,
        //     'sms_notify' => 0,
        //     'email_notify' => 0,
        //     'partial_payment' => false
        // ));

        //rp-invoice-generations (end)
        
        $order  = $api->order->create(array('receipt' => $receipt_id, 'amount' => (int)$total_amount * 100, 'currency' => $meta_data['settings']['currency'] ?? 'INR')); // Creates order
        
        // session()->put('order_id',$order['id']);

        $store_order_id = $this->store_order($request);

        $response = [
            'store_order_id' =>  $store_order_id['order_id'],
            'orderId' => $order['id'],
            'razorpayId' => $meta_data['payment_settings'][$request->payment_method]['rp-key-id'],
            'amount' => (int)$total_amount,
            'name' => $request->fname,
            'currency' => $meta_data['settings']['currency'] ?? 'INR',
            'email' => $request->email,
            'contactNumber' => $request->mnumber,
            'address' => $request->apt_number . ',' . $request->landmark . ',' . $request->city . ',' . $request->pincode . ',' . $request->state . ',' . "india",
            'description' =>  $request->order_notes ?? '',
        ];

        return $response;
    }

    public function razorpay_mobile(Request $request){
        $meta_data = $this->userMetaData($request->key);
        $total_amount = $request->total_amount;

        $api = new Api($meta_data['payment_settings'][$request->payment_method]['rp-key-id'], $meta_data['payment_settings'][$request->payment_method]['rp-secret-key']);
        $receipt_id = Str::random(20);

        $order  = $api->order->create(array('receipt' => $receipt_id, 'amount' => (int)$total_amount * 100, 'currency' => $meta_data['settings']['currency'] ?? 'INR')); // Creates order

        // $request->orderId = $order['id'];
        // $request->razorpayId = $meta_data['payment_settings'][$request->payment_method]['rp-key-id'];
        // $request->amount = (int)$total_amount;
        // $request->currency = $meta_data['settings']['currency'] ?? 'INR';
        // $request->contactNumber = $request->mnumber;
        // $request->address = $request->apt_number . ',' . $request->landmark . ',' . $request->city . ',' . $request->pincode . ',' . $request->state . ',' . "india";
        // $request->description =  $request->order_notes ?? '';


        $response = [
            'store_order_id' =>  $request->store_order_id,
            'orderId' => $order['id'],
            'razorpayId' => $meta_data['payment_settings'][$request->payment_method]['rp-key-id'],
            'amount' => (int)$total_amount,
            'name' => $request->fname,
            'currency' => $meta_data['settings']['currency'] ?? 'INR',
            'email' => $request->email,
            'contactNumber' => $request->mnumber,
            'address' => $request->apt_number . ',' . $request->landmark . ',' . $request->city . ',' . $request->pincode . ',' . $request->state . ',' . 'india',
            'description' =>  $request->order_notes ?? '',
        ];

        //return $response;

        return view('layouts.razorpay_payment_mobile_view',compact('response'));
    }


    public function paypal(Request $request)
    {

        $meta_data = $this->userMetaData($request->key);
        $total_amount = $this->calculateTotalAmount($request->products_qty);
        if($request->coupon_code_applied=='true')
        {
            $total_amount=$total_amount-$request->coupon_discount_amount;
        }


        if(isset($request->shipping_charges))
        {
            $total_amount += $request->shipping_charges;
        }
        if(isset($request->tax_percentage))
        {
            $total_amount += $request->tax_amount;
        }
        $extra_charges_names = explode(",", $request->extra_charges_names);
        $extra_charges_values = explode(",", $request->extra_charges_values);
        if(isset($extra_charges_values))
        {
            foreach($extra_charges_values as $key)
            {
                $total_amount += (int)$key;
            }
        }
        if(isset($request->sphylite_amount))
        {
            $total_amount += $request->sphylite_amount;
        }

        $store_order_id = $this->store_order($request);


        return [
            'store_order_id' =>  $store_order_id['order_id'],

            "client_id" => $meta_data['payment_settings'][$request->payment_method]['paypal-client-id'],
            "amount" => $total_amount,
            "currency" => $meta_data['settings']['currency'] ?? 'INR',
            'orderNotes' => $request->order_notes ?? '',
            "name" => $request->fname,
            "email" => $request->email,
            "address1" => $request->apt_number,
            "address2" => $request->landmark,
            "city" => $request->city,
            "pincode" => $request->pincode,
            "state" => $request->state,
            "country" => "india",
            "mobileno" => $request->mnumber,
        ];
    }

    public function upi(Request $request)
    {
        if ($request->q == 'store') {
            $store_order_id = $this->store_order($request);
            $request['unique_store_order_id'] =orders::find($store_order_id['order_id'])->order_id;
            $request['store_order_id'] =  $store_order_id['order_id'];

            session()->put('data', $request->all());

            $total_amount = $this->calculateTotalAmount($request->products_qty);
            if($request->coupon_code_applied=='true')
            {
                $total_amount=$total_amount - $request->coupon_discount_amount;
            }


            if(isset($request->shipping_charges))
            {
                $total_amount += $request->shipping_charges;
            }
            if(isset($request->tax_percentage))
            {
                $total_amount += $request->tax_amount;
            }
            $extra_charges_names = explode(",", $request->extra_charges_names);
            $extra_charges_values = explode(",", $request->extra_charges_values);
            if(isset($extra_charges_values))
            {
                foreach($extra_charges_values as $key)
                {
                    $total_amount += (int)$key;
                }
            }
            if(isset($request->sphylite_amount))
            {
                $total_amount += $request->sphylite_amount;
            }

            session()->put('amt', $total_amount);
            return  ['key' => $request->key];
        } else {
            $campaign = $request->campaign;
            $userSettings = $request->userSettings;
            $meta_data = $this->userMetaData($userSettings->public_link);
            $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');
            $data = session()->get('data');
            $desc = $data->order_notes ?? '';
            $amount =  session()->get('amt');
            $CURRENCY = $meta_data['settings']['currency'] ?? 'INR';
            return view('themes.' . $theme . '.pages.UPIpayment', compact('data', 'CURRENCY', 'campaign', 'userSettings', 'meta_data', 'desc', 'amount'));
        }
    }

    public function store_order(Request $request)
    {
        $order = app('App\Http\Controllers\Admin\OrdersController')->store($request);
        return $order;
    }

    public function cashOnDelivery(Request $request)
    {
        $store_order_id = $this->store_order($request);
        $response['store_order_id'] =  $store_order_id['order_id'];

        return $response;
    }
}
