<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        if(auth()->check() && auth()->user()->getIsAdminAttribute())
        {
            $user = auth()->user();
            Session::put('admin', $user);
            setCookie("admin", $user->name);
        }
       return view('admin.dashboard.index');
    }
}
