<?php

namespace App\Http\Controllers\Admin;

use App\Models\Device;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Webpatser\Uuid\Uuid;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class DevicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $device = Device::all();

        return view('admin.devices.index', compact('device'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        generate:
        $keyDigit = 4;
        $key = strtoupper(str::random($keyDigit));
        $keys = str_split($key, strlen($key)/($keyDigit/4));
        $uuid_key = "";
        foreach($keys as $k) {
            if($uuid_key == "")
                $uuid_key = $k;
            else
                $uuid_key .= "-".$k;
        }

        $validator = Validator::make(["key" => $uuid_key], [
            'key' => 'required|unique:devices',
        ]);
  
        if ($validator->fails()) {
            goto generate;
        }

        // dd($uuid_key);
        Device::create([
            'key' =>$uuid_key,
            'user_id' => auth()->user()->id,
            'status' => 0]);
            return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return redirect()->route('admin.devices.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\device  $device
     * @return \Illuminate\Http\Response
     */
    public function show(device $device)
    {
        //

        return view('admin.devices.show',compact('device'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\device  $device
     * @return \Illuminate\Http\Response
     */
    public function edit(device $device)
    {
        //
        $users = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');
        return view('admin.devices.edit', compact('device','users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\device  $device
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, device $device)
    {
        //
        $device->update($request->all());
        return redirect()->back()->with('message','Update Device successfully.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\device  $device
     * @return \Illuminate\Http\Response
     */
    public function destroy(device $device)
    {
        //
        $device->delete();

        return back();
    }
}
