<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MaintenanceController extends Controller
{
    public function index()
    {
        if(!empty(auth()->user()) && auth()->user()->getIsAdminAttribute()){
            return view('maintenance.index');
        }
        else{
            return view('errors.404');
        }
        
    }
}
