<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\CompanyInfo;
use App\Country;
use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyCompanyInfoRequest;
use App\Http\Requests\StoreCompanyInfoRequest;
use App\Http\Requests\UpdateCompanyInfoRequest;
use App\State;
use App\User;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;

class CompanyInfoController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('company_info_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = CompanyInfo::with(['country', 'city', 'state', 'user', 'team'])->select(sprintf('%s.*', (new CompanyInfo)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'company_info_show';
                $editGate      = 'company_info_edit';
                $deleteGate    = 'company_info_delete';
                $crudRoutePart = 'company-infos';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : "";
            });
            $table->editColumn('email', function ($row) {
                return $row->email ? $row->email : "";
            });
            $table->editColumn('phoneno', function ($row) {
                return $row->phoneno ? $row->phoneno : "";
            });
            $table->editColumn('website', function ($row) {
                return $row->website ? $row->website : "";
            });
            $table->editColumn('subdomain', function ($row) {
                return $row->subdomain ? $row->subdomain : "";
            });
            $table->editColumn('address', function ($row) {
                return $row->address ? $row->address : "";
            });
            $table->addColumn('country_name', function ($row) {
                return $row->country ? $row->country->name : '';
            });

            $table->addColumn('city_code', function ($row) {
                return $row->city ? $row->city->code : '';
            });

            $table->addColumn('state_code', function ($row) {
                return $row->state ? $row->state->code : '';
            });

            $table->addColumn('users_name', function ($row) {
                return $row->user ? $row->user->name : '';
            });

            $table->rawColumns(['actions', 'placeholder', 'country', 'city', 'state', 'user']);

            return $table->make(true);
        }

        return view('admin.companyInfos.index');
    }

    public function create()
    {
        abort_if(Gate::denies('company_info_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $countries = Country::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $cities = City::all()->pluck('code', 'id')->prepend(trans('global.pleaseSelect'), '');

        $states = State::all()->pluck('code', 'id')->prepend(trans('global.pleaseSelect'), '');

        $users = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.companyInfos.create', compact('countries', 'cities', 'states', 'users'));
    }

    public function store(StoreCompanyInfoRequest $request)
    {
        $companyInfo = CompanyInfo::create($request->all());

        // return redirect()->route('admin.company-infos.index');
        return redirect()->back();
    }

    public function edit(CompanyInfo $companyInfo)
    {
        abort_if(Gate::denies('company_info_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $countries = Country::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $cities = City::all()->pluck('code', 'id')->prepend(trans('global.pleaseSelect'), '');

        $states = State::all()->pluck('code', 'id')->prepend(trans('global.pleaseSelect'), '');

        $users = User::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $companyInfo->load('country', 'city', 'state', 'users', 'team');

        return view('admin.companyInfos.edit', compact('countries', 'cities', 'states', 'users', 'companyInfo'));
    }

    public function update(UpdateCompanyInfoRequest $request, CompanyInfo $companyInfo)
    {
        $companyInfo->update($request->all());

        // return redirect()->route('admin.company-infos.index');
        return redirect()->back();

    }

    public function show(CompanyInfo $companyInfo)
    {
        abort_if(Gate::denies('company_info_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $companyInfo->load('country', 'city', 'state', 'users', 'team');

        return view('admin.companyInfos.show', compact('companyInfo'));
    }

    public function destroy(CompanyInfo $companyInfo)
    {
        abort_if(Gate::denies('company_info_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $companyInfo->delete();

        return back();
    }

    public function massDestroy(MassDestroyCompanyInfoRequest $request)
    {
        CompanyInfo::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
