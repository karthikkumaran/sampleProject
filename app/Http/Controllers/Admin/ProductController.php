<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Controllers\Traits\CsvImportTrait;
use App\Http\Requests\MassDestroyProductRequest;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Product;
use App\Scripts;
use App\ProductCategory;
use App\ProductTag;
use App\UserInfo;
use App\Entitycard;
use App\AttributesMetadata;
use App\Arobject;
use App\Campaign;
use Auth;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

// use App\Varchar;
use Gate;
use File;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;
use Rinvex\Attributes\Models\Attribute;
use Rinvex\Attributes\Models\Type\Varchar;
use App\Http\Controllers\Traits\CommonFunctionsTrait;
use Session;
use Illuminate\Support\Facades\Schema;
use App\LaravelSubscriptions\Plan;
use App\LaravelSubscriptions\PlanFeature;
use App\LaravelSubscriptions\PlanSubscription;
use App\LaravelSubscriptions\PlanSubscriptionUsage;
use App\LaravelSubscriptions\PlanSubscriptionFeatures;
use App\LaravelSubscriptions\PlanSubscribedFeatures;
use Spatie\Activitylog\Models\Activity;
use Illuminate\Support\Facades\Validator;



use Illuminate\Database\Eloquent\Model;

class ProductController extends Controller
{
    use MediaUploadingTrait;
    use CsvImportTrait;
    use CommonFunctionsTrait;

    public function index(Request $request)
    {
        abort_if(Gate::denies('product_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if ($request->ajax()) {
            $query = Product::with(['categories', 'tags', 'team'])->select(sprintf('%s.*', (new Product)->table));
            $table = Datatables::of($query);

            $table->addColumn('placeholder', '&nbsp;');
            $table->addColumn('actions', '&nbsp;');

            $table->editColumn('actions', function ($row) {
                $viewGate      = 'product_show';
                $editGate      = 'product_edit';
                $deleteGate    = 'product_delete';
                $crudRoutePart = 'products';

                return view('partials.datatablesActions', compact(
                    'viewGate',
                    'editGate',
                    'deleteGate',
                    'crudRoutePart',
                    'row'
                ));
            });

            $table->editColumn('id', function ($row) {
                return $row->id ? $row->id : "";
            });
            $table->editColumn('sku', function ($row) {
                return $row->sku ? $row->sku : "";
            });
            $table->editColumn('name', function ($row) {
                return $row->name ? $row->name : "";
            });
            $table->editColumn('featured', function ($row) {
                return $row->featured ? "Yes" : "No";
            });
            $table->editColumn('description', function ($row) {
                return $row->description ? $row->description : "";
            });
            $table->editColumn('price', function ($row) {
                return $row->price ? $row->price : "";
            });
            $table->editColumn('discount', function ($row) {
                return $row->discount ? $row->discount : "";
            });
            $table->editColumn('category', function ($row) {
                $labels = [];

                foreach ($row->categories as $category) {
                    $labels[] = sprintf('<span class="label label-info label-many">%s</span>', $category->name);
                }

                return implode(' ', $labels);
            });
            $table->editColumn('tag', function ($row) {
                $labels = [];

                foreach ($row->tags as $tag) {
                    $labels[] = sprintf('<span class="label label-info label-many">%s</span>', $tag->name);
                }

                return implode(' ', $labels);
            });
            $table->editColumn('photo', function ($row) {
                if (!$row->photo) {
                    return '';
                }

                $links = [];

                foreach ($row->photo as $media) {
                    $links[] = '<a href="' . $media->getUrl() . '" target="_blank"><img src="' . $media->getUrl('thumb') . '" width="50px" height="50px"></a>';
                }

                return implode(' ', $links);
            });
            $table->editColumn('url', function ($row) {
                return $row->url ? $row->url : "";
            });

            $table->rawColumns(['actions', 'placeholder', 'category', 'tag', 'photo']);

            return $table->make(true);
        }

        return view('admin.products.index');
    }

    public function create()
    {
        abort_if(Gate::denies('product_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categories = ProductCategory::all()->pluck('name', 'id');

        $tags = ProductTag::all()->pluck('name', 'id');

        return view('admin.products.create', compact('categories', 'tags'));
    }

    public function store(StoreProductRequest $request)
    {
        // dd($request->campaign_id);
        // print_r($request->producttype);die;
        $user = Auth::user();
        $product_meta_data = json_decode('', true);
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $user->id)->first();
        }
        $canCreateProduct = true;
        $canCreateMultipleProducts = true;
        if (!empty($subscription)) {
            $subscribed_package = $subscription->slug;
            $canCreateProduct = $user->subscription($subscribed_package)->canUseFeature('products');
        }

        if ($canCreateProduct) {
            $position = $request->position;
            if ($request->producttype == "single") {
                generate:
                    $keyDigit = 4;
                    $sku = strtoupper(str::random($keyDigit));
                    $totalproduct = Product::where('team_id',$user->team_id)->where('sku',$sku)->get();

                        if(count($totalproduct) > 0)
                        {
                            goto generate;
                        }
                $product = Product::create($request->all());
                Product::find($product->id)->update(['sku' => $sku]);
                if(!empty($request->disable_zoom))
                    $product_meta_data['disable_zoom'] = $request->disable_zoom;

                if(!empty($request->custom_theme))
                    $product_meta_data['custom_theme'] = $request->custom_theme;

                $product_meta_data = json_encode($product_meta_data);
                $scripts = Scripts::create(array(
                    "model_type" => "App\Product",
                    "model_id" => $product->id,
                    "script_html" => $request->script_html,
                    "script_js" => $request->script_js,
                    "meta_data" => $product_meta_data,
                ));
                if (!empty($subscription)) {
                    $user->subscription($subscribed_package)->recordFeatureUsage('products', 1);
                }

                $product->categories()->sync($request->input('categories', []));
                $product->tags()->sync($request->input('tags', []));
                if ($request->campaign_id)
                    Entitycard::create(array('product_id' => $product->id, 'campaign_id' => $request->campaign_id,'video_id' => $request->videoid, 'meta_data' => '{}'));
                foreach ($request->input('photo', []) as $file) {
                    if ($request->is_product == "NONAR")
                        $product->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('photo');
                    // if($request->is_product == "AR"){

                    //     $arobject = Arobject::create(array('product_id' => $product->id,'position' => $position));

                    //     $arobject->addMedia(storage_path('tmp/uploads/' . $request->input('object')))->toMediaCollection('object');
                    //     if ($media = $request->input('ck-media', false)) {
                    //         Media::whereIn('id', $media)->update(['model_id' => $arobject->id]);
                    //     }
                    // }
                }

                if ($media = $request->input('ck-media', false)) {
                    Media::whereIn('id', $media)->update(['model_id' => $product->id]);
                }
            } elseif ($request->producttype == "multiple") {
                if (!empty($subscription)) {
                    $remaining_usage = $this->getFeatureUsageRemaining(new Request(["featurename" => "products"]));
                    $uploaded_products = count($request->input('photo', []));
                    if ($uploaded_products <= $remaining_usage) {
                        $canCreateMultipleProducts = true;
                    } else {
                        $canCreateMultipleProducts = false;
                    }
                }

                if ($canCreateMultipleProducts) {
                    foreach ($request->input('photo', []) as $file) {

                        skugenerate:
                            $keyDigit = 4;
                            $sku = strtoupper(str::random($keyDigit));
                            $totalproduct = Product::where('team_id',$user->team_id)->where('sku',$sku)->get();

                            if(count($totalproduct) > 0)
                            {
                                goto generate;
                            }
                        $product = Product::create($request->all());
                        Product::find($product->id)->update(['sku' => $sku]);
                        if (!empty($subscription)) {
                            $user->subscription($subscribed_package)->recordFeatureUsage('products', 1);
                        }

                        $product->categories()->sync($request->input('categories', []));
                        $product->tags()->sync($request->input('tags', []));
                        if ($request->campaign_id)
                            Entitycard::create(array('product_id' => $product->id, 'campaign_id' => $request->campaign_id, 'video_id' => $request->videoid, 'meta_data' => '{}'));
                        if ($request->is_product == "NONAR") {
                            $mymedia = $product->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('photo');
                            if ($media = $request->input('ck-media', false)) {
                                Media::whereIn('id', $media)->update(['model_id' => $product->id]);
                            }
                        } else if ($request->is_product == "AR") {
                            $arobject = Arobject::create(array('product_id' => $product->id, 'position' => $position));
                            $arobject->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('object');
                            if ($media = $request->input('ck-media', false)) {
                                Media::whereIn('id', $media)->update(['model_id' => $arobject->id]);
                            }
                        } else {
                            return redirect()->route('admin.products.index');
                        }
                    }
                } else {
                    $alert = "limit_exceeded";
                    return $this->alertUsageLimitReached($alert, 'session');
                }
            }

            return redirect()->route('admin.products.index');
        } else {
            $alert = "limit_reached";
            return $this->alertUsageLimitReached($alert, 'session');
        }
    }

    public function edit(Product $product)
    {
        abort_if(Gate::denies('product_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $categories = ProductCategory::whereNull('product_category_id')
            ->with('childrenCategories')
            ->get();

        $attributes = AttributesMetadata::get()->groupBy('attribute_group_name');

        $values = varchar::where('entity_id', $product->id)->pluck('content', 'attribute_id');

        $tags = ProductTag::all()->pluck('name', 'id');

        $meta_data = json_decode($product->meta_data, true);

        $product->load('categories', 'tags', 'team', 'scripts');
        // $scripts = Scripts::where("model_id", $product->id)->first();


        return view('cards.product_edit', compact('categories', 'tags', 'product', 'attributes', 'values','meta_data'));
        //return view('admin.products.edit', compact('categories', 'tags', 'product'));
    }

    public function update(UpdateProductRequest $request, Product $product)
    {
        // print_r($product);
        // die;
        $user = Auth::user();

        $product_meta_data = array();

        $scripts = Scripts::where("model_id", $product->id);
        $scripts_data = $scripts->first();
        if(!empty($scripts_data))
            $product_meta_data = json_decode($scripts_data->meta_data ?? '',true);

        if(!empty($request->disable_zoom))
            $product_meta_data['disable_zoom'] = $request->disable_zoom;
        else
            $product_meta_data['disable_zoom'] = 0;

        if(!empty($request->custom_theme))
            $product_meta_data['custom_theme'] = $request->custom_theme;
        else
            $product_meta_data['custom_theme'] = 0;

        $product_meta_data = json_encode($product_meta_data);
        if(!Gate::denies('custom_script')) {
            if(empty($scripts->first())) {
                $scripts = Scripts::create(array(
                    "model_type" => "App\Product",
                    "model_id" => $product->id,
                    "script_html" => $request->scripthtml3dmodel ?? '',
                    "script_css" => $request->scriptcss3dmodel ?? '',
                    "script_js" => $request->scriptjs3dmodel ?? '',
                    "meta_data" => $product_meta_data,
                ));
            } else {
                $scripts->update(array(
                    "script_html" => $request->scripthtml3dmodel,
                    "script_css" => $request->scriptcss3dmodel,
                    "script_js" => $request->scriptjs3dmodel,
                    "meta_data" => $product_meta_data,
                ));
            }
        }


        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $user->id)->first();
        }
        if (!empty($subscription)) {
            $subscribed_package = $subscription->slug;
        }
        $canUseInventory = $user->subscription($subscribed_package)->canUseFeature('inventory');
        if($canUseInventory){
            $product->out_of_stock = $request->input('out_of_stock') ? true : false;
            $stock_qty = $request->input('stock_qty');
            if ($stock_qty == null) {
                $request->merge(['stock' => null]);
            }
        }
        else{
            $request->merge(['out_of_stock' => $product->out_of_stock]);
            $request->merge(['stock' => $product->stock]);
            $request->merge(['stock_qty' => null]);
        }
        $meta_data = json_decode($product->meta_data, true);

        $tryon_settings = $request->only('necklace_v_position','necklace_h_position','necklace_size',
        'ear_left_v_position','ear_left_h_position','ear_left_size',
        'ear_right_v_position','ear_right_h_position','ear_right_size',
        'watch_v_position','watch_h_position','watch_size',
        'ring_v_position','ring_h_position','ring_size',
        'bracelet_v_position','bracelet_h_position','bracelet_size',
        'apparel_v_position','apparel_h_position','apparel_size'
        );
        if(!empty($tryon_settings)){
            $meta_data['tryon_settings'] = $tryon_settings;
        }

        $product->meta_data = json_encode($meta_data);
        $product->featured = $request->input('featured') ? true : false;
        $product->new = $request->input('new') ? true : false;
        $units=$request->input('units');
        $product->downgradeable = ($request->input('downgradeable') == 1) ? true : false;
        $discount_rate = $request->input('discount_rate');
        $minimum_quantity = $request->input('minimum_quantity');
        $maximun_quantity = $request->input('maximum_quantity');
        $product->shop= $request->input('shop');
        $product->quantity = $request->input('quantity');
        $product->link = $request->input('link');
        $product->external_shop = $request->input('external_shop');
        $product->length = $request->input('length');
        $product->width = $request->input('width');
        $product->productweight = $request->input('productweight');
        $product->height = $request->input('height');
        $product->seo_title = $request->input('seo_title');
        $product->seo_description = $request->input('seo_description');
        $product->seo_keyword = $request->input('seo_keyword');


        Attribute::typeMap([
            'varchar' => Varchar::class,
        ]);

        // foreach ($request->all() as $key => $value) {
        //     $request->merge([
        //         $key => is_array($value) ? implode(',', $value) : $value
        //     ]);
        // }

        if ($request->has("attribute") && $request->attribute == 1) {
            foreach ($product->attributes() as $attribute) {
                $attr = $attribute->slug;
                if ($request->has($attr)) {
                    $request->merge([
                        $attr => is_array($request->$attr) ? implode(',', $request->$attr) : $request->$attr
                    ]);
                } else {
                    $product->$attr = null;
                    $product->save();
                }
            }
        } else {
            $values = varchar::where('entity_id', $product->id);
            if (!empty($values->get())) {
                $data = $values->pluck('content')->toArray();
                foreach ($request->all() as $key => $value) {
                    if (is_array($value) && $key != "categories" && $key != "photo" && $key != "docs") {
                        $request->request->remove($key);
                    } elseif (in_array($value, $data)) {
                        $request->request->remove($key);
                    }
                }
                $values->delete();
            }
        }

        if ($discount_rate == null) {
            $request->merge(['discount' => null]);
        }
        if($units==null){
            $request->merge(['units'=>null]);
        }


        $product->update($request->all());

        if (isset($request->categories) && !empty($request->categories[0])) {
            $product->categories()->sync($request->input('categories', []));
            if (count($product->productEntitycards) > 0) {
                Entitycard::where("product_id", $product->id)->update(array("catagory_id" => $request->categories[0]));
            }
        } else {
            // dd($request->input('categories', []));
            $product->categories()->sync([]);
            Entitycard::where("product_id", $product->id)->update(array("catagory_id" => null));
        }
        $product->tags()->sync($request->input('tags', []));

        // if (count($product->photo) > 0) {
        //     foreach ($product->photo as $media) {
        //         if (!in_array($media->file_name, $request->input('photo', []))) {
        //             $media->delete();
        //         }
        //     }
        // }

        $media = $product->photo->pluck('file_name')->toArray();
        foreach ($request->input('photo', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $product->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('photo');
            }
        }

        $media_docs = $product->docs->pluck('file_name')->toArray();
        foreach ($request->input('docs', []) as $file) {
            if (count($media_docs) === 0 || !in_array($file, $media_docs)) {
                $product->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('docs');
            }
        }

        $canUploadAR = $user->subscription($subscribed_package)->canUseFeature('ar_object');
        if($canUploadAR){
            if ($request->input('try_on_object', false)) {
                $arobject = Arobject::Create(array('ar_type' => $request->try_on_type, 'position' => $request->position, 'product_id' => $product->id));

                if ($request->input('try_on_object', false)) {
                    if (!$arobject->object) {
                        $arobject->addMedia(storage_path('tmp/uploads/' . $request->input('try_on_object')))->toMediaCollection('object');
                    }
                    else{
                        if(isset($arobject->object->file_name)){
                            if ( $request->input('try_on_object') !== $arobject->object->file_name) {
                                $arobject->addMedia(storage_path('tmp/uploads/' . $request->input('try_on_object')))->toMediaCollection('object');
                            }
                        }
                    }

                }
            }
        }

        $canUpload3D = $user->subscription($subscribed_package)->canUseFeature('3d_object');
        if($canUpload3D){
            if ($request->input('object_3d', false)) {
                $arobject = Arobject::Create(array('ar_type' => $request->type_3d, 'product_id' => $product->id));

                if ($request->input('object_3d', false)) {
                    if (!$arobject->object_3d) {
                        $arobject->addMedia(storage_path('tmp/uploads/' . $request->input('object_3d')))->toMediaCollection('object_3d');
                    }else{
                        if(isset($arobject->object_3d->file_name)){
                            if( $request->input('object_3d') !== $arobject->object_3d->file_name){
                                $arobject->addMedia(storage_path('tmp/uploads/' . $request->input('object_3d')))->toMediaCollection('object_3d');
                            }
                        }
                    }
                }
            }

            if ($request->input('object_3d_ios', false)) {
                $arobject = Arobject::Create(array('ar_type' => $request->type_3d, 'product_id' => $product->id));

                if ($request->input('object_3d_ios', false)) {
                    if (!$arobject->object_3d_ios) {
                        $arobject->addMedia(storage_path('tmp/uploads/' . $request->input('object_3d_ios')))->toMediaCollection('object_3d_ios');
                    }else{
                        if(isset($arobject->object_3d_ios->file_name)){
                            if( $request->input('object_3d_ios') !== $arobject->object_3d_ios->file_name){
                                $arobject->addMedia(storage_path('tmp/uploads/' . $request->input('object_3d_ios')))->toMediaCollection('object_3d_ios');
                            }
                        }
                    }
                }
            }
        }
        $arobj = Arobject::where('product_id',$product->id)->where('ar_type','surface')->get();
        if(count($arobj) > 0 ){
            if($request->input('wall') == 1){
                foreach($arobj as $key){
                    $key->update(['position' => 'wall']);
                }
            }
            else{
                foreach($arobj as $key){
                    $key->update(['position' => 'floor']);
                }
            }
        }
        // return redirect()->route('admin.products.index');
        // return redirect()->back();

            return true;
    }

    public function show(Product $product)
    {
        abort_if(Gate::denies('product_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $product->load('categories', 'tags', 'team', 'productEntitycards');

        return view('admin.products.show', compact('product'));
    }

    public function destroy(Product $product)
    {
        abort_if(Gate::denies('product_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $media = Media::where('model_type','App\Product')->where('model_id',$product->id)->get();
        $arobject = Arobject::where('product_id',$product->id)->get();

        // foreach($media as $model)
        // {
        //     if(file_exists(public_path('storage'.'/'. $model->id )))
        //     {
        //         $file = public_path('storage'.'/'. $model->id .'/'.$model->file_name);
        //         // $thumbnail = public_path('storage'.'/'. $model->id .'/conversions/'.$model->name.'-thumb.png');
        //         // unlink($thumbnail);
        //         unlink($file);
        //         // echo $thumbnail;
        //         // File::deleteDirectory($file);
        //     }
        //     $product->deleteMedia($model->id);

        // }


        $product->load('productEntitycards');
        if (count($product->productEntitycards) == 0) {
            $product->delete();
            foreach($media as $model)
            {
                if(file_exists(public_path('storage'.'/'. $model->id)))
                {
                    $file = public_path('storage'.'/'. $model->id );
                    File::deleteDirectory($file);
                }
                $product->deleteMedia($model->id);
            }
            foreach($arobject as $object)
            {
                $armedia =  Media::where('model_type','App\Arobject')->where('model_id',$object->id)->first();
                $object->delete();
                $armedia->delete();
            }

            $user = Auth::user();
            if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
                $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
            }
            else{
                $subscription = PlanSubscription::where('user_id', $user->id)->first();
            }
            $products_usage = PlanSubscriptionUsage::where(['subscription_id' => $subscription->id, 'feature_id' => 2])->first();
            $products = Product::where('team_id', $user->team_id)->get();
            $available_products = $products->count();
            $products_used = $products_usage->used;
            if($available_products < $products_used){
                if (!empty($products_usage)) {
                    $product_count = $products_usage->used;
                    $products_usage->used = $product_count-1;
                    $products_usage->save();
                }
            }
            // return back();
            return response()->json(['msg' => trans('global.cantDelete'), 'status' => "success"]);
        } else {
            return response()->json(['msg' => trans('global.cantDelete'), 'status' => "failure"]);
        }
        // dd($product->productEntitycards());

        return response()->json(['errors' => trans('global.cantDelete')], 424);
    }

    public function massDestroy(MassDestroyProductRequest $request)
    {
        Product::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('product_create') && Gate::denies('product_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Product();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media', 'public');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }

    public function ajaxData(Request $request)
    {
        $sort = ['id', 'desc'];
        $page = 20;
        $is_search = false;
        $is_filter = false;
        $category_filter = "";
        $price_filter_low = "";
        $price_filter_high = "";
        $cat_id = array();
        $category = array();
        $videoid = $request->videoid;
        $filters = json_decode($request->filter, true);
        if ($request->ajax()) {
            if(isset($videoid))
            {
            $campaign_products = Entitycard::where('campaign_id', $request->campaign_id)->where('video_id', $videoid)->get();
            }
            else
            {
            $campaign_products = Entitycard::where('campaign_id', $request->campaign_id)->get();
            }
            $campaign_product_ids = array();
            foreach ($campaign_products as $campaign_product) {
                if(isset($campaign_product->product_id)){
                    array_push($campaign_product_ids, $campaign_product->product_id);
                }
            }

            if (isset($request->s) && !empty($request->s)) {
                $sort = explode(',', $request->s);
            }

            if (isset($request->q) && !empty($request->q)) {
                $q = $request->q;
                $is_search = true;
            }

            if (isset($request->filter) && !empty($filters)) {
                $is_filter = true;
                foreach ($filters as $card) {
                    if (in_array("category", array_values($card)) && $card['type'] == "category") {
                        $category_filter = $card['value'];

                        if ($category_filter == "all") {
                            $cat_id = "";
                        } else {
                            $cat = explode(",", $category_filter);
                            $category = ProductCategory::whereIn('name', $cat)->get('id');
                            foreach ($category as $catg) {
                                array_push($cat_id, $catg->id);
                            }
                        }
                    }

                    if ($card['type'] == "price") {
                        $price_range = explode(",", $card['value']);
                        $price_filter_low = $price_range[0];
                        if (count($price_range) == 2) {
                            $price_filter_high = $price_range[1];
                        }
                    }
                }
            }

            if (($is_search == true) && ($is_filter == false)) {
                $data = Product::whereNotIn('id', $campaign_product_ids)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%')
                    ->orderBy($sort[0], $sort[1])->paginate($page);
            } else if (($is_search == false) && ($is_filter == true)) {
                if (($price_filter_low != "all") && ($category_filter != "all")) {
                    if ($price_filter_high == "") {
                        $data = Product::whereNotIn('id', $campaign_product_ids)->where('price', '>=', $price_filter_low)->whereHas('categories', function ($query) use ($cat_id) {
                            $query->whereIn('id', $cat_id);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    } else {
                        $data = Product::whereNotIn('id', $campaign_product_ids)->where('price', '>=', $price_filter_low)->where('price', '<=', $price_filter_high)->whereHas('categories', function ($query) use ($cat_id) {
                            $query->whereIn('id', $cat_id);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    }
                } else if (($price_filter_low != "all") && ($category_filter == "all")) {
                    if ($price_filter_high == "") {
                        $data = Product::whereNotIn('id', $campaign_product_ids)->where('price', '>=', $price_filter_low)
                            ->orderBy($sort[0], $sort[1])->paginate($page);
                    } else {
                        $data = Product::whereNotIn('id', $campaign_product_ids)->where('price', '>=', $price_filter_low)->where('price', '<=', $price_filter_high)
                            ->orderBy($sort[0], $sort[1])->paginate($page);
                    }
                } else if (($price_filter_low == "all") && ($category_filter != "all")) {
                    $data = Product::whereNotIn('id', $campaign_product_ids)->whereHas('categories', function ($query) use ($cat_id) {
                        $query->whereIn('id', $cat_id);
                    })->orderBy($sort[0], $sort[1])->paginate($page);
                } else {
                    $data = Product::whereNotIn('id', $campaign_product_ids)->orderBy($sort[0], $sort[1])->paginate($page);
                }
            } else if (($is_search == true) && ($is_filter == true)) {
                if (($price_filter_low != "all") && ($category_filter != "all")) {
                    if ($price_filter_high == "") {
                        // dd("Searching","Filtering","Both Search and Filter, Both Filters only min ");
                        $data = Product::whereNotIn('id', $campaign_product_ids)->where('price', '>=', $price_filter_low)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%')->whereHas('categories', function ($query) use ($cat_id) {
                            $query->whereIn('id', $cat_id);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    } else {
                        // dd("Searching","Filtering","Both Search and Filter, Both Filters ");
                        $data = Product::whereNotIn('id', $campaign_product_ids)->where('price', '>=', $price_filter_low)->where('price', '<=', $price_filter_high)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%')->whereHas('categories', function ($query) use ($cat_id) {
                            $query->whereIn('id', $cat_id);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    }
                } else if (($price_filter_low != "all") && ($category_filter == "all")) {
                    if ($price_filter_high == "") {
                        // dd("Searching","Filtering","Both Search and Filter, Only price Filter only min ");
                        $data = Product::whereNotIn('id', $campaign_product_ids)->where('price', '>=', $price_filter_low)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%')
                            ->orderBy($sort[0], $sort[1])->paginate($page);
                    } else {
                        // dd("Searching","Filtering","Both Search and Filter, Only price Filter ");
                        $data = Product::whereNotIn('id', $campaign_product_ids)->where('price', '>=', $price_filter_low)->where('price', '<=', $price_filter_high)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%')
                            ->orderBy($sort[0], $sort[1])->paginate($page);
                    }
                } else if (($price_filter_low == "all") && ($category_filter != "all")) {
                    // dd("Searching","Filtering","Both Search and Filter, Only category Filter ");
                    $data = Product::whereNotIn('id', $campaign_product_ids)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%')->whereHas('categories', function ($query) use ($cat_id) {
                        $query->whereIn('id', $cat_id);
                    })->orderBy($sort[0], $sort[1])->paginate($page);
                } else {
                    // dd("Searching","Filtering","Both Search and Filter, both all filters");
                    $data = Product::whereNotIn('id', $campaign_product_ids)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%')
                        ->orderBy($sort[0], $sort[1])->paginate($page);
                }
            } else {
                // dd($campaign_product_ids);
                $data = Product::whereNotIn('id', $campaign_product_ids)->orderBy($sort[0], $sort[1])->paginate($page);
                // print_r($data);
            }

            $userSettings = UserInfo::where("user_id", auth()->user()->id)->first();
            $user_meta_data = json_decode($userSettings->meta_data, true);
            // foreach ($data as $key => $product)
            // dd($data[0]->arobject[0]->object->getUrl());
            // dd($data[0]->arobject->object->getUrl());
            return view('cards.product_grid', compact('data', 'user_meta_data', 'is_search', 'is_filter','videoid'));
        }
        return false;
    }

    public function AddedajaxData(Request $request)
    {
        $sort = ['id', 'desc'];
        $page = 4;
        if ($request->ajax())
        {
            if (isset($request->s) && !empty($request->s)) {
                $sort = explode(',', $request->s);
            }

            if (isset($request->q) && !empty($request->q)) {
                $q = $request->q;
                $data = Product::where('products.name', 'LIKE', '%' . $q . '%')->orWhere('products.sku', 'LIKE', '%' . $q . '%')
                    ->orderBy($sort[0], $sort[1])->paginate($page);
            }
            else
            {
                $data = Product::orderBy($sort[0], $sort[1])->paginate($page);
            }

            return view('cards.product_list', compact('data'));
        }
        return false;
    }

    public function ajaxSelectProducts(Request $request)
    {
        $sort = ['id', 'desc'];
        $page = 20;
        $is_search = false;
        $is_filter = false;
        $category_filter = "";
        $price_filter_low = "";
        $price_filter_high = "";
        $category = array();
        $cat_id = array();
        $filters = json_decode($request->filter, true);
        if ($request->ajax()) {
            if (isset($request->s) && !empty($request->s)) {
                $sort = explode(',', $request->s);
            }
            if (isset($request->q) && !empty($request->q)) {
                $q = $request->q;
                $is_search = true;
            }
            if (isset($request->filter) && !empty($filters)) {
                $is_filter = true;
                foreach ($filters as $card) {
                    if (in_array("category", array_values($card)) && $card['type'] == "category") {
                        $category_filter = $card['value'];

                        if ($category_filter == "all") {
                            $cat_id = "";
                        } else {
                            $cat = explode(",", $category_filter);
                            $category = ProductCategory::whereIn('name', $cat)->get('id');
                            foreach ($category as $catg) {
                                array_push($cat_id, $catg->id);
                            }
                        }
                    }
                    if ($card['type'] == "price") {
                        $price_range = explode(",", $card['value']);
                        $price_filter_low = $price_range[0];
                        if (count($price_range) == 2) {
                            $price_filter_high = $price_range[1];
                        }
                    }
                }
            }
            if (($is_search == true) && ($is_filter == false)) {
                $data = Product::where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%')
                    ->orderBy($sort[0], $sort[1])->paginate($page);
            } else if (($is_search == false) && ($is_filter == true)) {
                if (($price_filter_low != "all") && ($category_filter != "all")) {
                    if ($price_filter_high == "") {
                        $data = Product::where('price', '>=', $price_filter_low)->whereHas('categories', function ($query) use ($cat_id) {
                            $query->whereIn('id', $cat_id);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    } else {
                        $data = Product::where('price', '>=', $price_filter_low)->where('price', '<=', $price_filter_high)->whereHas('categories', function ($query) use ($cat_id) {
                            $query->whereIn('id', $cat_id);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    }
                } else if (($price_filter_low != "all") && ($category_filter == "all")) {
                    if ($price_filter_high == "") {
                        $data = Product::where('price', '>=', $price_filter_low)
                            ->orderBy($sort[0], $sort[1])->paginate($page);
                    } else {
                        $data = Product::where('price', '>=', $price_filter_low)->where('price', '<=', $price_filter_high)
                            ->orderBy($sort[0], $sort[1])->paginate($page);
                    }
                } else if (($price_filter_low == "all") && ($category_filter != "all")) {
                    $data = Product::whereHas('categories', function ($query) use ($cat_id) {
                        $query->whereIn('id', $cat_id);
                    })->orderBy($sort[0], $sort[1])->paginate($page);
                } else {
                    $data = Product::orderBy($sort[0], $sort[1])->paginate($page);
                }
            } else if (($is_search == true) && ($is_filter == true)) {
                if (($price_filter_low != "all") && ($category_filter != "all")) {
                    if ($price_filter_high == "") {
                        // dd("Searching","Filtering","Both Search and Filter, Both Filters only min ");
                        $data = Product::where('price', '>=', $price_filter_low)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%')->whereHas('categories', function ($query) use ($cat_id) {
                            $query->whereIn('id', $cat_id);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    } else {
                        // dd("Searching","Filtering","Both Search and Filter, Both Filters ");
                        $data = Product::where('price', '>=', $price_filter_low)->where('price', '<=', $price_filter_high)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%')->whereHas('categories', function ($query) use ($cat_id) {
                            $query->whereIn('id', $cat_id);
                        })->orderBy($sort[0], $sort[1])->paginate($page);
                    }
                } else if (($price_filter_low != "all") && ($category_filter == "all")) {
                    if ($price_filter_high == "") {
                        // dd("Searching","Filtering","Both Search and Filter, Only price Filter only min ");
                        $data = Product::where('price', '>=', $price_filter_low)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%')
                            ->orderBy($sort[0], $sort[1])->paginate($page);
                    } else {
                        // dd("Searching","Filtering","Both Search and Filter, Only price Filter ");
                        $data = Product::where('price', '>=', $price_filter_low)->where('price', '<=', $price_filter_high)->where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%')
                            ->orderBy($sort[0], $sort[1])->paginate($page);
                    }
                } else if (($price_filter_low == "all") && ($category_filter != "all")) {
                    // dd("Searching","Filtering","Both Search and Filter, Only category Filter ");
                    $data = Product::where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%')->whereHas('categories', function ($query) use ($cat_id) {
                        $query->whereIn('id', $cat_id);
                    })->orderBy($sort[0], $sort[1])->paginate($page);
                } else {
                    // dd("Searching","Filtering","Both Search and Filter, both all filters");
                    $data = Product::where('name', 'LIKE', '%' . $q . '%')->orWhere('sku', 'LIKE', '%' . $q . '%')->orWhere('price', 'LIKE', '%' . $q . '%')
                        ->orderBy($sort[0], $sort[1])->paginate($page);
                }
            } else {
                $data = Product::orderBy($sort[0], $sort[1])->paginate($page);
            }

            $userSettings = UserInfo::where("user_id", auth()->user()->id)->first();
            $user_meta_data = json_decode($userSettings->meta_data, true);

            return view('cards.select_products', compact('data', 'user_meta_data', 'is_search', 'is_filter'));
        }
        return false;
    }

    public function product_list(Request $request)
    {
        abort_if(Gate::denies('product_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $sort = ['id', 'desc'];
        $page = 10;
        $is_search = false;
        $is_filter = false;
        $filters = json_decode($request->filter, true);
        $categories = ProductCategory::rightJoin('product_product_category', function ($join) {
            $join->on('product_categories.id', '=', 'product_product_category.product_category_id');
        })->groupBy('product_categories.id')->orderBy('product_categories.id', 'desc')->get();
        $campaigns = Campaign::all();
        $data = Product::orderBy('id', 'desc')->paginate($page);
        $userSettings = UserInfo::where("user_id", auth()->user()->id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);
        $product_enable = "true";
        $downgraded = false;
        $out_of_stock_products_only = false;
        $stock_filter_min = "";
        $stock_filter_max = "";
        $remaining_products = "false";
        $user = Auth::user();
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $user->id)->first();
        }
        if (!empty($subscription)) {
            $enabled_products = 0;
            $all_products = Product::all();
            foreach ($all_products as $product) {
                if ($product->downgradeable == 1) {
                    $downgraded = true;
                } else {
                    $enabled_products++;
                }
            }
            if ($downgraded == true) {
                $allowed_products = $user->subscription($subscription->slug)->getFeatureValue('products');
                if ($enabled_products >= $allowed_products) {
                    $product_enable = "false";
                }
                $remaining_products = $allowed_products - $enabled_products;
            }
        }
        if ($request->ajax()) {
            if (isset($request->s) && !empty($request->s)) {
                $sort = explode(',', $request->s);
            }
            if (isset($request->q) && !empty($request->q)) {
                $q = $request->q;
                $is_search = true;
            }

            if (isset($request->filter) && !empty($filters)) {
                $is_filter = true;
                foreach ($filters as $filter) {
                    if ($filter['type'] == "out_of_stock_products_only") {
                        $out_of_stock_products_only = true;
                    }

                    if ($filter['type'] == "stock") {
                        $stock_range = explode(",", $filter['value']);
                        $stock_filter_min = $stock_range[0];
                        if (count($stock_range) == 2) {
                            $stock_filter_max = $stock_range[1];
                        }
                    }
                }
            }

            if (($is_search == true) && ($is_filter == false)) {
                $data = Product::where('products.name', 'LIKE', '%' . $q . '%')->orWhere('products.sku', 'LIKE', '%' . $q . '%')->orWhere('products.price', 'LIKE', '%' . $q . '%')
                    ->orderBy($sort[0], $sort[1])->orderBy('downgradeable', 'asc')->paginate($page);
            } else if (($is_search == false) && ($is_filter == true)) {
                if($out_of_stock_products_only == true){
                    if($stock_filter_max == ""){
                        $data = Product::where('products.out_of_stock', '=', 1)->where('products.stock', '>=', $stock_filter_min)
                        ->orderBy($sort[0], $sort[1])->orderBy('downgradeable', 'asc')->paginate($page);
                    }
                    else {
                        $data = Product::where('products.out_of_stock', '=', 1)->where('products.stock', '>=', $stock_filter_min)->where('products.stock', '<=', $stock_filter_max)
                        ->orderBy($sort[0], $sort[1])->orderBy('downgradeable', 'asc')->paginate($page);
                    }
                }
                else{
                    if($stock_filter_max == ""){
                        $data = Product::where('products.stock', '>=', $stock_filter_min)
                        ->orderBy($sort[0], $sort[1])->orderBy('downgradeable', 'asc')->paginate($page);
                    }
                    else {
                        $data = Product::where('products.stock', '>=', $stock_filter_min)->where('products.stock', '<=', $stock_filter_max)
                        ->orderBy($sort[0], $sort[1])->orderBy('downgradeable', 'asc')->paginate($page);
                    }
                }
            } else if (($is_search == true) && ($is_filter == true)) {
                if($out_of_stock_products_only == true){
                    if($stock_filter_max == ""){
                        $data = Product::where('products.out_of_stock', '=', 1)->where('products.stock', '>=', $stock_filter_min)->where('products.name', 'LIKE', '%' . $q . '%')->orWhere('products.sku', 'LIKE', '%' . $q . '%')->orWhere('products.price', 'LIKE', '%' . $q . '%')
                        ->orderBy($sort[0], $sort[1])->orderBy('downgradeable', 'asc')->paginate($page);
                    }
                    else {
                        $data = Product::where('products.out_of_stock', '=', 1)->where('products.stock', '>=', $stock_filter_min)->where('products.stock', '<=', $stock_filter_max)->where('products.name', 'LIKE', '%' . $q . '%')->orWhere('products.sku', 'LIKE', '%' . $q . '%')->orWhere('products.price', 'LIKE', '%' . $q . '%')
                        ->orderBy($sort[0], $sort[1])->orderBy('downgradeable', 'asc')->paginate($page);
                    }
                }
                else{
                    if($stock_filter_max == ""){
                        $data = Product::where('products.stock', '>=', $stock_filter_min)->where('products.name', 'LIKE', '%' . $q . '%')->orWhere('products.sku', 'LIKE', '%' . $q . '%')->orWhere('products.price', 'LIKE', '%' . $q . '%')
                        ->orderBy($sort[0], $sort[1])->orderBy('downgradeable', 'asc')->paginate($page);
                    }
                    else {
                        $data = Product::where('products.stock', '>=', $stock_filter_min)->where('products.stock', '<=', $stock_filter_max)->where('products.name', 'LIKE', '%' . $q . '%')->orWhere('products.sku', 'LIKE', '%' . $q . '%')->orWhere('products.price', 'LIKE', '%' . $q . '%')
                        ->orderBy($sort[0], $sort[1])->orderBy('downgradeable', 'asc')->paginate($page);
                    }
                }
            } else {
                $data = Product::orderBy($sort[0], $sort[1])->orderBy('downgradeable', 'asc')->paginate($page);
            }
            return view('cards.productslist', compact('data', 'user_meta_data', 'is_search', 'is_filter', 'product_enable'));
        }
        if (!empty($subscription)) {
            $this->getSubscriptionFeatureUsage(new Request(["featurename" => "products"]));
        }

        return view('templates.admin.product_list', compact('user_meta_data', 'data', 'categories', 'campaigns', 'is_search', 'product_enable', 'remaining_products'));
    }

    public function updateProduct(Request $request)
    {
        $user = Auth::user();
        abort_if(Gate::denies('product_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        if($request->name == 'sku'){
            $skucount = 0;
            if(empty($request->value)){
                $sku = Product::where('id',$request->pk)->pluck('sku')->first();
                Product::find($request->pk)->update([$request->name => $sku]);
                return response()->json(['success' => 'done']);
            }
            else{
            $totalproduct = Product::where('team_id',$user->team_id)->where('sku',$request->value)->get();
                if(count($totalproduct) > 0)
                {
                                $skucount = 1;
                }
                if($skucount == 0){
                    Product::find($request->pk)->update([$request->name => $request->value]);
                    return response()->json(['success' => 'done']);
                }
                else{
                    $sku = Product::where('id',$request->pk)->pluck('sku')->first();

                    Product::find($request->pk)->update([$request->name => $sku]);

                    return response()->json(['success' => 'done']);
                        //echo "<script type='text/javascript'>alert('Sku is already used');</script>";
                        //return $this->alert('error', __('Sku is already used!'));
                    //return response()->json(['error' => 'Sku is already used!']);
                            // return redirect()->route('admin.products.product_list')->with('jsAlert', 'Sku is already used!');
                            // return redirect()->route('admin.products.product_list')->with('skumessage', 'Sku is already used!');
                            // return back()->withErrors(['message'=>'Sku is already used!']);
                }
            }
        }
        else{
            Product::find($request->pk)->update([$request->name => $request->value]);
            return response()->json(['success' => 'done']);
        }
    }

    public function removeImg(Request $request)
    {
        abort_if(Gate::denies('product_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $mediaId = $request->id;
        // if ($mediaId instanceof Media) {
        //     $mediaId = $mediaId->getKey();
        // }
        // $media = $this->media->find($mediaId);
        // if (! $media) {
        //     throw MediaCannotBeDeleted::doesNotBelongToModel($mediaId, $this);
        // }
        // $media->delete();
        $media = Media::find($request->input('id'));
        // dd($media);
        $model = Product::find($media->model_id);
        $model->deleteMedia($media->id);
        return response()->json(['success' => 'done']);
        //     Model::whereHas('media', function ($query) use($media_id){
        //         $query->whereId($media_id);
        //    })->first()->deleteMedia($media_id);
    }

    public function imagelog(Request $request)
    {
        activity()->log($request->msg);
    }
    public function imagelogshow(Request $request)
    {
        $logshow = Activity::all()->last();
        return $logshow->description;
    }

    public function productsku(Request $request){
        $isexist ='';
        $user = Auth::user();
        if(!empty($request->sku)){
            $totalproduct = Product::where('team_id',$user->team_id)->where('sku',$request->sku)->get();
            if(count($totalproduct) > 0){
                $productsku = Product::where('id',$request->productid)->pluck('sku')->first();
                $isexist = 1;
            }
            else{
                $productsku = $request->sku;
            }
        }
        else{
            $productsku = Product::where('id',$request->productid)->pluck('sku')->first();
        }
            $result  = array();
            $result['productsku'] = $productsku;
            $result['isexist'] = $isexist;
        return $result;
    }
    public function parseCsvImports(Request $request)
    {
       $import = $request->all();
       $import['all']= $this->parseCsvImport($request);
       return $import['all'];
    }

    public function processCsvImports(Request $request)
    {
        $import = $request->all();
        $import['all'] = $this->processCsvImport($request);
        return $import['all'];
    }


}
