<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\User;
use App\UserInfo;
use App\Product;
use App\Campaign;
use Carbon\Carbon;
use Razorpay\Api\Api;
use Illuminate\Support\Str;
use App\SubscriptionPayment;
use Illuminate\Support\Facades\Schema;
use App\LaravelSubscriptions\Plan;
use App\LaravelSubscriptions\PlanFeature;
use App\LaravelSubscriptions\PlanSubscription;
use App\LaravelSubscriptions\PlanSubscriptionUsage;
use App\LaravelSubscriptions\PlanSubscriptionFeatures;
use App\LaravelSubscriptions\PlanSubscribedFeatures;
use Rinvex\Subscriptions\Services\Period;
use App\Jobs\DowngradeSubscriptionJob;

class AdminSubscriptionController extends Controller
{
    public function userSubscription($user_id){
        if(!empty(auth()->user()) && auth()->user()->getIsAdminAttribute()){
        $user = User::where('id', $user_id)->first();
        $packages = Plan::get();
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $user->id)->first();
        }
            return view('admin.adminsubscription.user_subscription_details',compact('user','subscription','packages'));
        }
        else{
            return view('errors.404');
        }
    }

    public function createSubscription(Request $request){
        if(!empty(auth()->user()) && (auth()->user()->getIsAdminAttribute() || app('impersonate')->isImpersonating())){
        // dd("create",$request->all());
        $user = User::where('id', $request->user_id)->first();
        $plan = Plan::where('id', $request->plan_id)->first();
        if($plan->discount != 0 && $plan->discount < $plan->price)
        {
        $planDiscount = $plan->price - $plan->discount;
        }
        else{
            $planDiscount = $plan->price;  
        }
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $trial_subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
        }
        else{
            $trial_subscription = PlanSubscription::where('user_id', $user->id)->first();
        }
        if (!empty($trial_subscription)) {
            $trial_subscription->forceDelete();
        }
        $user->newSubscription($plan->name, $plan);
        $user->subscription_status = 1;
        $user->save();
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $user->id)->first();
        }
        $now = Carbon::now();
        $features = $plan->features;
        $subscribed_features = [];
        foreach ($features as $feature) {
            $subscribed_features[] = [
                'user_id' => $user->id,
                'plan_id' => $plan->id,
                'subscription_id' => $subscription->id,
                'feature_id' => $feature->feature_id,
                'value' => $feature->value,
                'resettable_period' => $feature->resettable_period,
                'resettable_interval' => $feature->resettable_interval,
                'is_approved' => 1,
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }
        PlanSubscribedFeatures::insert($subscribed_features);

        $campaigns = Campaign::where('user_id', $user->id)->orderBy('is_start', 'desc')->orderBy('id', 'desc')->get();
        $campaigns_count = $campaigns->count();
        if ($campaigns_count > 0) {
            PlanSubscriptionUsage::create([
                'subscription_id' => $subscription->id,
                'feature_id' => 1,
                'used' => $campaigns_count,
            ]);
        }
        
        $products = Product::where('team_id', $user->team_id)->get();
        $products_count = $products->count();
        if ($products_count > 0) {
            PlanSubscriptionUsage::create([
                'subscription_id' => $subscription->id,
                'feature_id' => 2,
                'used' => $products_count,
            ]);
        }

        $payment_metadata = array();
        $userSettings = UserInfo::where("user_id", $user->id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);
        if(empty($user_meta_data['settings']['currency'])){
            $currency = "₹";
        }
        else{
            if($user_meta_data['settings']['currency_selection'] == "currency_symbol")
                $currency = User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
            else
                $currency = $user_meta_data['settings']['currency'];
        }
        if($request->transaction_amount){
            // if($request->transaction_amount){
                $planDiscount = $request->transaction_amount;
            // }
        }
        if($request->cash_mode == 'Free'){
                $request->payment_method = null;
                $request->transaction_id = null;
                $planDiscount = 0;
        }
            if($request->plan_start_date && $request->plan_start_date){
                $subscription->starts_at = Carbon::parse($request->plan_start_date);
                $subscription->ends_at = Carbon::parse($request->plan_end_date);
                $subscription->save();
            }
            $plan_starts = $request->plan_start_date ? Carbon::parse($request->plan_start_date) : $user->subscription($subscription->slug)->starts_at;
            $plan_ends = $request->plan_end_date ? Carbon::parse($request->plan_end_date) : $user->subscription($subscription->slug)->ends_at;
        $auth_id = Auth::user()->id;
        if(app('impersonate')->isImpersonating()){
            $auth_id = 1;
        }
        $random_str = bin2hex(random_bytes(14));
        $order_id = "order_".$random_str;
        $payment_metadata["id"] = $request->transaction_id;
        $payment_metadata["entity"] = "payment";
        $payment_metadata["amount"] = $planDiscount;
        $payment_metadata["currency"] = $currency;
        $payment_metadata["status"] = "authorized";
        $payment_metadata["order_id"] = $order_id;
        $payment_metadata["invoice_id"] = null;
        $payment_metadata["international"] = false;
        $payment_metadata["method"] = $request->payment_method;
        $payment_metadata["description"] = $plan->description;
        $payment_metadata["email"] = $user->email;
        $payment_metadata["contact"] = $user->mobile;
        $payment_metadata["created_at"] = $request->transaction_date;
        $subscription_payment = SubscriptionPayment::create([
            'user_id' => $user->id,
            'plan_name' => $plan->name,
            'plan_price' => $planDiscount,
            'plan_start' => $plan_starts,
            'plan_expiry' => $plan_ends,
            'payment_id' => $request->transaction_id,
            'payment_mode' => $request->payment_method,
            'payment_method' => $request->payment_method,
            'status' => "success",
            'meta_data' => serialize($payment_metadata),
            'subscribed_by' => $auth_id,
            'comments' => $request->comments
        ]);

            return true;
        }
        else{
            return view('errors.404');
        }
    }

    public function upgradeSubscription(Request $request){
        if(!empty(auth()->user()) && auth()->user()->getIsAdminAttribute()){
        // dd("upgrade",$request->all());
        $user = User::where('id', $request->user_id)->first();
        $plan = Plan::where('id', $request->plan_id)->first();
        if($plan->discount != 0 && $plan->discount < $plan->price)
        {
        $planDiscount = $plan->price - $plan->discount;
        }
        else{
            $planDiscount = $plan->price;  
        }
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $user->id)->first();
        }
        $features = $plan->features;
        foreach ($features as $feature) {
            $plan_subscribed_feature = PlanSubscribedFeatures::where(['user_id' => $user->id, 'plan_id' => $subscription->plan_id, 'feature_id' => $feature->feature_id])->first();
            if ($plan_subscribed_feature) {
                $plan_subscribed_feature->plan_id = $plan->id;
                $plan_subscribed_feature->value = $feature->value;
                $plan_subscribed_feature->is_add_on = 0;
                $plan_subscribed_feature->add_on_value = null;
                $plan_subscribed_feature->save();
            } else {
                $plan_subscribed_features = new PlanSubscribedFeatures;
                $plan_subscribed_features->create([
                    'user_id' => $user->id,
                    'subscription_id' => $subscription->id,
                    'plan_id' => $plan->id,
                    'feature_id' => $feature->feature_id,
                    'value' => $feature->value,
                    'is_approved' => 1,
                ]);
            }
        }
        $subscription->changePlan($plan);
        $campaigns = Campaign::where('user_id', $user->id)->orderBy('is_start', 'desc')->orderBy('id', 'desc')->get();
        $available_catalogs = $campaigns->count();
        if ($available_catalogs > 0) {
            $campaign_count = 0;
            $catalogs_usage_count = 0;
            $campaign_value = $user->subscription($subscription->slug)->getFeatureValue('catalogs');
            if ($available_catalogs > $campaign_value) {
                foreach ($campaigns as $campaign) {
                    $campaign_count = $campaign_count + 1;
                    if ($campaign_count > $campaign_value) {
                        $campaign->is_start = 0;
                        $campaign->is_added_home = 0;
                        $campaign->downgradeable = 1;
                        $campaign->save();
                    } else {
                        $catalogs_usage_count = $campaign_count;
                        $campaign->downgradeable = 0;
                        $campaign->save();
                    }
                }
            } else {
                $catalogs_usage_count = $available_catalogs;
            }

            $catalogs_usage = PlanSubscriptionUsage::where(['subscription_id' => $subscription->id, 'feature_id' => 1])->first();
            if (!empty($catalogs_usage)) {
                $catalogs_usage->used = $catalogs_usage_count;
                $catalogs_usage->save();
            }
        }

        $products = Product::where('team_id', $user->team_id)->get();
        $available_products = $products->count();
        if ($available_products > 0) {
            $product_count = 0;
            $products_usage_count = 0;
            $product_value = $user->subscription($subscription->slug)->getFeatureValue('products');
            if ($available_products > $product_value) {
                foreach ($products as $product) {
                    $product_count = $product_count + 1;
                    if ($product_count > $product_value) {
                        $product->downgradeable = 1;
                        $product->save();
                    } else {
                        $products_usage_count = $product_count;
                        $product->downgradeable = 0;
                        $product->save();
                    }
                }
            } else {
                $products_usage_count = $available_products;
            }

            $products_usage = PlanSubscriptionUsage::where(['subscription_id' => $subscription->id, 'feature_id' => 2])->first();
            if (!empty($products_usage)) {
                $products_usage->used = $products_usage_count;
                $products_usage->save();
            }
        }

        $payment_metadata = array();
        $userSettings = UserInfo::where("user_id", $user->id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);
        if(empty($user_meta_data['settings']['currency'])){
            $currency = "₹";
        }
        else{
            if($user_meta_data['settings']['currency_selection'] == "currency_symbol")
                $currency = User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
            else
                $currency = $user_meta_data['settings']['currency'];
        }
        if($request->transaction_amount){
            // if($request->transaction_amount){
                $planDiscount = $request->transaction_amount;
            // }
        }
        if($request->cash_mode == 'Free'){
            $request->payment_method = null;
            $request->transaction_id = null;
            $planDiscount = 0;
        }
        if($request->plan_start_date && $request->plan_start_date){
            $subscription->starts_at = Carbon::parse($request->plan_start_date);
            $subscription->ends_at = Carbon::parse($request->plan_end_date);
            $subscription->save();
        }
        $plan_starts = $request->plan_start_date ? Carbon::parse($request->plan_start_date) : $user->subscription($subscription->slug)->starts_at;
        $plan_ends = $request->plan_end_date ? Carbon::parse($request->plan_end_date) : $user->subscription($subscription->slug)->ends_at;
        $auth_id = Auth::user()->id;
        $random_str = bin2hex(random_bytes(14));
        $order_id = "order_".$random_str;
        $payment_metadata["id"] = $request->transaction_id;
        $payment_metadata["entity"] = "payment";
        $payment_metadata["amount"] = $planDiscount;
        $payment_metadata["currency"] = $currency;
        $payment_metadata["status"] = "authorized";
        $payment_metadata["order_id"] = $order_id;
        $payment_metadata["invoice_id"] = null;
        $payment_metadata["international"] = false;
        $payment_metadata["method"] = $request->payment_method;
        $payment_metadata["description"] = $plan->description;
        $payment_metadata["email"] = $user->email;
        $payment_metadata["contact"] = $user->mobile;
        $payment_metadata["created_at"] = $request->transaction_date;
        $subscription_payment = SubscriptionPayment::create([
            'user_id' => $user->id,
            'plan_name' => $plan->name,
            'plan_price' => $planDiscount,
            'plan_start' => $plan_starts,
            'plan_expiry' => $plan_ends,
            'payment_id' => $request->transaction_id,
            'payment_mode' => $request->payment_method,
            'payment_method' => $request->payment_method,
            'status' => "success",
            'meta_data' => serialize($payment_metadata),
            'subscribed_by' => $auth_id,
            'comments' => $request->comments
        ]);

            return true;
        }
        else{
            return view('errors.404');
        }
    }

    public function downgradeSubscription(Request $request){
        if(!empty(auth()->user()) && auth()->user()->getIsAdminAttribute()){
        $user = User::where('id', $request->user_id)->first();
        $plan = Plan::where('id', $request->plan_id)->first();
        if($plan->discount != 0 && $plan->discount < $plan->price)
        {
        $planDiscount = $plan->price - $plan->discount;
        }
        else{
            $planDiscount = $plan->price;  
        }
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $user->id)->first();
        }
        if(!empty($request->immediate_downgrade))
        {
            // dd("immediate",$request->all());
            PlanSubscribedFeatures::where('user_id',$user->id)->forceDelete();
            $subscription->changePlan($plan);
            $now = Carbon::now();
            $features = $plan->features;
            $subscribed_features = [];
            foreach($features as $feature){
                $subscribed_features[] = [
                    'user_id' => $user->id,
                    'plan_id' => $plan->id,
                    'subscription_id' => $subscription->id,
                    'feature_id' => $feature->feature_id,
                    'value' => $feature->value,
                    'resettable_period' => $feature->resettable_period,
                    'resettable_interval' => $feature->resettable_interval,
                    'is_approved' => 1,
                    'created_at' => $now,
                    'updated_at' => $now,
                ];
            }
            PlanSubscribedFeatures::insert($subscribed_features);
            if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
                $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
            }
            else{
                $subscription = PlanSubscription::where('user_id', $user->id)->first();
            }
            $feature_ids = array();
            foreach ($features as $feature){
                array_push($feature_ids,$feature->feature_id);
                $feature_details = $feature->getFeatureDetails();
                $feature_value = $user->subscription($subscription->slug)->getFeatureValue($feature_details->name);
                $feature_usage = $user->subscription($subscription->slug)->getFeatureUsage($feature_details->name);
                if($feature_usage > $feature_value){
                    $usage = PlanSubscriptionUsage::where(['subscription_id' => $subscription->id,'feature_id' => $feature->feature_id])->first();
                    $usage->used = $feature_value;
                    $usage->save();
                }
                else{

                }
                $campaigns = Campaign::where('user_id',$user->id)->orderBy('is_start','desc')->orderBy('id', 'desc')->get();
                $campaign_count = 0;
                $campaign_value = $user->subscription($subscription->slug)->getFeatureValue('catalogs');
                foreach($campaigns as $campaign){
                    $campaign_count++;
                    if($campaign_count > $campaign_value){
                        $campaign->is_start = 0;
                        $campaign->is_added_home = 0;
                        $campaign->downgradeable = 1;
                        $campaign->save();
                    }
                    else{
                        $campaign->downgradeable = 0;
                        $campaign->save();
                    }
                }
                $products = Product::where('team_id',$user->team_id)->get();
                $product_count = 0;
                $product_value = $user->subscription($subscription->slug)->getFeatureValue('products');
                foreach($products as $product){
                    $product_count++;
                    if($product_count > $product_value){
                        $product->downgradeable = 1;
                        $product->save();
                    }
                    else{
                        $product->downgradeable = 0;
                        $product->save();
                    }
                }
            }
            $usages = PlanSubscriptionUsage::where('subscription_id', $subscription->id)->get();
            foreach ($usages as $usage){
                if(!in_array($usage->feature_id,$feature_ids)){ 
                    $usage->forceDelete();
                }else{

                }
            }

            $payment_metadata = array();
            $userSettings = UserInfo::where("user_id", $user->id)->first();
            $user_meta_data = json_decode($userSettings->meta_data, true);
            if(empty($user_meta_data['settings']['currency'])){
                $currency = "₹";
            }
            else{
                if($user_meta_data['settings']['currency_selection'] == "currency_symbol")
                    $currency = User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
                else
                    $currency = $user_meta_data['settings']['currency'];
            }
            if($request->transaction_amount){
                // if($request->transaction_amount){
                    $planDiscount = $request->transaction_amount;
                // }
            }
            if($request->cash_mode == 'Free'){
                $request->payment_method = null;
                $request->transaction_id = null;
                $planDiscount = 0;
            }
            if($request->plan_start_date && $request->plan_start_date){
                $subscription->starts_at = Carbon::parse($request->plan_start_date);
                $subscription->ends_at = Carbon::parse($request->plan_end_date);
                $subscription->save();
            }
            $plan_starts = $request->plan_start_date ? Carbon::parse($request->plan_start_date) : $user->subscription($subscription->slug)->starts_at;
            $plan_ends = $request->plan_end_date ? Carbon::parse($request->plan_end_date) : $user->subscription($subscription->slug)->ends_at;
            $auth_id = Auth::user()->id;
            $random_str = bin2hex(random_bytes(14));
            $order_id = "order_".$random_str;
            $payment_metadata["id"] = $request->transaction_id;
            $payment_metadata["entity"] = "payment";
            $payment_metadata["amount"] = $planDiscount;
            $payment_metadata["currency"] = $currency;
            $payment_metadata["status"] = "authorized";
            $payment_metadata["order_id"] = $order_id;
            $payment_metadata["invoice_id"] = null;
            $payment_metadata["international"] = false;
            $payment_metadata["method"] = $request->payment_method;
            $payment_metadata["description"] = $plan->description;
            $payment_metadata["email"] = $user->email;
            $payment_metadata["contact"] = $user->mobile;
            $payment_metadata["created_at"] = $request->transaction_date;
            $subscription_payment = SubscriptionPayment::create([
                'user_id' => $user->id,
                'plan_name' => $plan->name,
                'plan_price' => $planDiscount,
                'plan_start' => $plan_starts,
                'plan_expiry' => $plan_ends,
                'payment_id' => $request->transaction_id,
                'payment_mode' => $request->payment_method,
                'payment_method' => $request->payment_method,
                'status' => "success",
                'meta_data' => serialize($payment_metadata),
                'subscribed_by' => $auth_id,
                'comments' => $request->comments
            ]);
        }
        else{
            $data['user'] = $user;
            $data['plan'] = $plan;
            $data['subscription'] = $subscription;
            $subscription_end_time = $subscription->ends_at;
            $current_time = Carbon::now();
            $interval = $current_time->diff($subscription_end_time);
            DowngradeSubscriptionJob::dispatch($data)->onQueue('downgradeable')->delay($interval);
            $trial_start = Carbon::now()->add($interval);
            $trial = new Period($plan->trial_interval, $plan->trial_period, $trial_start);
            $period = new Period($plan->invoice_interval, $plan->invoice_period, $trial->getEndDate());

            $payment_metadata = array();
            $userSettings = UserInfo::where("user_id", $user->id)->first();
            $user_meta_data = json_decode($userSettings->meta_data, true);
            if(empty($user_meta_data['settings']['currency'])){
                $currency = "₹";
            }
            else{
                if($user_meta_data['settings']['currency_selection'] == "currency_symbol")
                    $currency = User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
                else
                    $currency = $user_meta_data['settings']['currency'];
            }
            if($request->transaction_amount){
                // if($request->transaction_amount){
                    $planDiscount = $request->transaction_amount;
                // }
            }
            if($request->cash_mode == 'Free'){
                $request->payment_method = null;
                $request->transaction_id = null;
                $planDiscount = 0;
            }
            if($request->plan_start_date && $request->plan_start_date){
                $subscription->starts_at = Carbon::parse($request->plan_start_date);
                $subscription->ends_at = Carbon::parse($request->plan_end_date);
                $subscription->save();
            }
            $plan_starts = $request->plan_start_date ? Carbon::parse($request->plan_start_date) : $user->subscription($subscription->slug)->starts_at;
            $plan_ends = $request->plan_end_date ? Carbon::parse($request->plan_end_date) : $user->subscription($subscription->slug)->ends_at;
            $auth_id = Auth::user()->id;
            $random_str = bin2hex(random_bytes(14));
            $order_id = "order_".$random_str;
            $payment_metadata["id"] = $request->transaction_id;
            $payment_metadata["entity"] = "payment";
            $payment_metadata["amount"] = $planDiscount;
            $payment_metadata["currency"] = $currency;
            $payment_metadata["status"] = "authorized";
            $payment_metadata["order_id"] = $order_id;
            $payment_metadata["invoice_id"] = null;
            $payment_metadata["international"] = false;
            $payment_metadata["method"] = $request->payment_method;
            $payment_metadata["description"] = $plan->description;
            $payment_metadata["email"] = $user->email;
            $payment_metadata["contact"] = $user->mobile;
            $payment_metadata["created_at"] = $request->transaction_date;
            $subscription_payment = SubscriptionPayment::create([
                'user_id' => $user->id,
                'plan_name' => $plan->name,
                'plan_price' => $planDiscount,
                'plan_start' => $plan_starts,
                'plan_expiry' => $plan_ends,
                'payment_id' => $request->transaction_id,
                'payment_mode' => $request->payment_method,
                'payment_method' => $request->payment_method,
                'status' => "success",
                'meta_data' => serialize($payment_metadata),
                'subscribed_by' => $auth_id,
                'comments' => $request->comments
            ]);
        }

            return true;
        }
        else{
            return view('errors.404');
        }
    }

    public function renewSubscription(Request $request){
        if(!empty(auth()->user()) && auth()->user()->getIsAdminAttribute()){
        // dd("renew",$request->all());
        $user = User::where('id', $request->user_id)->first();
        $plan = Plan::where('id', $request->plan_id)->first();
        if($plan->discount != 0 && $plan->discount < $plan->price)
        {
        $planDiscount = $plan->price - $plan->discount;
        }
        else{
            $planDiscount = $plan->price;  
        }
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $user->id)->first();
        }
        $user->subscription($subscription->slug)->renew();

        $payment_metadata = array();
        $userSettings = UserInfo::where("user_id", $user->id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);
        if(empty($user_meta_data['settings']['currency'])){
            $currency = "₹";
        }
        else{
            if($user_meta_data['settings']['currency_selection'] == "currency_symbol")
                $currency = User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
            else
                $currency = $user_meta_data['settings']['currency'];
        }
        
        if($request->transaction_amount){
            // if($request->transaction_amount){
                $planDiscount = $request->transaction_amount;
            // }
        }
        if($request->cash_mode == 'Free'){
            $request->payment_method = null;
            $request->transaction_id = null;
            $planDiscount = 0;
        }
            if($request->plan_start_date && $request->plan_start_date){
                $subscription->starts_at = Carbon::parse($request->plan_start_date);
                $subscription->ends_at = Carbon::parse($request->plan_end_date);
                $subscription->save();
            }
            $plan_starts = $request->plan_start_date ? Carbon::parse($request->plan_start_date) : $user->subscription($subscription->slug)->starts_at;
            $plan_ends = $request->plan_end_date ? Carbon::parse($request->plan_end_date) : $user->subscription($subscription->slug)->ends_at;
        $auth_id = Auth::user()->id;
        $random_str = bin2hex(random_bytes(14));
        $order_id = "order_".$random_str;
        $payment_metadata["id"] = $request->transaction_id;
        $payment_metadata["entity"] = "payment";
        $payment_metadata["amount"] = $planDiscount;
        $payment_metadata["currency"] = $currency;
        $payment_metadata["status"] = "authorized";
        $payment_metadata["order_id"] = $order_id;
        $payment_metadata["invoice_id"] = null;
        $payment_metadata["international"] = false;
        $payment_metadata["method"] = $request->payment_method;
        $payment_metadata["description"] = $plan->description;
        $payment_metadata["email"] = $user->email;
        $payment_metadata["contact"] = $user->mobile;
        $payment_metadata["created_at"] = $request->transaction_date;
        $subscription_payment = SubscriptionPayment::create([
            'user_id' => $user->id,
            'plan_name' => $plan->name,
            'plan_price' => $planDiscount,
            'plan_start' => $plan_starts,
            'plan_expiry' => $plan_ends,
            'payment_id' => $request->transaction_id,
            'payment_mode' => $request->payment_method,
            'payment_method' => $request->payment_method,
            'status' => "success",
            'meta_data' => serialize($payment_metadata),
            'subscribed_by' => $auth_id,
            'comments' => $request->comments
        ]);

            return true;
        }
        else{
            return view('errors.404');
        }
    }


    public function planData(Request $request){
        $plan = Plan::where('id', $request->plan_id)->first();
        if(isset($request->pay_date)){
            $trial_start = $request->pay_date;
        }
        else{
            $trial_start = Carbon::now();
        }

        $trial = new Period($plan->trial_interval, $plan->trial_period, $trial_start);
        $period = new Period($plan->invoice_interval, $plan->invoice_period, $trial->getEndDate());
        $startDate = $period->getStartDate();
        $endDate = $period->getEndDate();
        $nextPayDate = $period->getEndDate()->add('1','day');
        
        return response([
            'start_date'=> $startDate,
            'end_date' => $endDate,
            'next_pay_date' => $nextPayDate
        ]);
    }
    public function planDetails(Request $request){
        $plan = Plan::where('invoice_interval', $request->package_type)->get();
        return response($plan);

    }

    public function assignSubscription(Request $request){
        if(!empty(auth()->user()) && (auth()->user()->getIsAdminAttribute() || app('impersonate')->isImpersonating())){
            $user = User::where('id', $request->user_id)->first();
            $packages = Plan::get();
            if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
                $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
            }
            else{
                $subscription = PlanSubscription::where('user_id', $user->id)->first();
            }
        }
        foreach($packages as $key => $package){
            if($subscription){
                if($package->id == $subscription->plan_id){
                    $subscribed_package = $package;
                }
            }
        }

        $package = Plan::where('id', $request->plan_id)->first();
            if($subscription && $subscribed_package->slug != "user-trial"){
                if($package->slug == $subscribed_package->slug){
                    if($user->isSubscriptionExpired()){
                        $this->renewSubscription($request);
                        $subscribe_grade = 'Renewed';
                    }
                    else{
                        $subscribe_grade = 'current';
                    }
                }
                else{
                    if($package->price > $subscribed_package->price){
                        $this->upgradeSubscription($request);
                        $subscribe_grade = 'upgraded';
                    }
                    elseif($package->price < $subscribed_package->price){
                        $request->immediate_downgrade = 1;
                        $this->downgradeSubscription($request);
                        $subscribe_grade = 'downgraded';
                    }
                }
            }
            else{
                
                if($package->slug == "user-trial" && $user->is_on_trial){
                    $subscribe_grade = 'current';
                }
                else{
                    $this->createSubscription($request);
                    $subscribe_grade = 'subscribed';
                }
            }
        return $subscribe_grade;

    }

    public function updateSubscription(Request $request){
        $plan = Plan::find($request->edit_plan_id);
        
        $subscriptionPayment = SubscriptionPayment::where('user_id', $request->edit_user_id)->where('plan_name',$plan->name)->where('subscribed_by',1)
        // ->where('plan_start',$request->edit_plan_start)->where('plan_expiry',$request->edit_plan_start)
        ->latest()->first();
        $meta_data = unserialize($subscriptionPayment->meta_data);
        $meta_data['id'] = $request->transaction_edit_id;
        $meta_data['amount'] = $request->transaction_edit_amount;
        $meta_data["method"] = $request->payment_edit_method;
        $meta_data = serialize($meta_data);

        $subscriptionPayment->payment_method = $request->payment_edit_method;
        $subscriptionPayment->payment_mode = $request->payment_edit_method;
        $subscriptionPayment->payment_id = $request->transaction_edit_id;
        $subscriptionPayment->plan_price = $request->transaction_edit_amount;
        $subscriptionPayment->meta_data = $meta_data;
        $subscriptionPayment->save();
    }

    public function paymentDetails(Request $request){
        $plan = $request->plan; 
        $used_id = $request->user_id;
        $plan['starts_at'] =  Carbon::parse($plan['starts_at'])->setTimezone('+5:30')->format('Y-m-d H:i:s');
        $plan['ends_at'] =  Carbon::parse($plan['ends_at'])->setTimezone('+5:30')->format('Y-m-d H:i:s');
        $subscription_payment = SubscriptionPayment::where('user_id', $used_id)->where('plan_name',$plan['name'])->
                                where('plan_start',$plan['starts_at'])->where('plan_expiry',$plan['ends_at'])->first();
        return $subscription_payment;
    }
}
