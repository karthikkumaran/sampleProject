<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ordersProduct;
use App\UserInfo;
use App\orders;
use App\Campaign;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;
use App\Http\Controllers\Traits\AddEntityCardTrait;
use App\Http\Controllers\Admin\CampaignsController;



class ShippingController extends Controller
{
     
    
    public function price_calculator(Request $request) {
                // $campaign = $this->link();
        // $user_id = auth()->user()->id;
        // $userSettings = UserInfo::where("user_id", $user_id)->first();
        // $meta_data = json_decode($userSettings->meta_data, true);
        
        $shareableLink = ShareableLink::where('uuid', $request->shareable_link)->first();
            $campaign = $shareableLink->shareable;
            if ($shareableLink->shareable->table == "campaigns") {
                $campaign_id = $shareableLink->shareable->id;
            } else {
                $user_id = $shareableLink->shareable->user_id;
                $campaign_id = Campaign::where('user_id', $user_id)->first()->id;
            }
            $campaign = Campaign::where('id', $campaign_id)->first();
            
            $userSettings = UserInfo::where("user_id", $campaign->user_id)->first();
            $meta_data = json_decode($userSettings->meta_data, true);
            

            $public_key_id = $meta_data['shipping_settings']['shipping']['public-key-id'];
            $app_id = $meta_data['shipping_settings']['shipping']['sp-app-id'];
            $sell_id = $meta_data['shipping_settings']['shipping']['sp-sell-id'];
            $private_key = $meta_data['shipping_settings']['shipping']['sp-private-key'];

            // $public_key_id = "L+dZpKAT13k=";
            // $app_id = $request->app_id;
            // $sell_id = $request->sell_id;
            // $private_key = "A/ipreE9/L0f/CTZMvbYFJT37vCUnnrV7fpwKRP28kMdopgP75X31BZZ/KePG/0oNcEk0y+sgnN9W2XPjJz1EQ==";
        $timestamp = time();
        $appID = $app_id;
        $key = $public_key_id;
        $sell_id = $sell_id;
        $secret = $private_key;
        $sign = "key:". $key ."id:". $appID. ":timestamp:". $timestamp;
        $authtoken = rawurlencode(base64_encode(hash_hmac('sha256', $sign, $secret, true)));
        $ch = curl_init();

        $shyplite_length = isset($meta_data['shipping_settings']['shipping']['shyplite_length']) ? $meta_data['shipping_settings']['shipping']['shyplite_length'] : '';
        $shyplite_width = isset($meta_data['shipping_settings']['shipping']['shyplite_width']) ? $meta_data['shipping_settings']['shipping']['shyplite_width'] : '';
        $shyplite_height = isset($meta_data['shipping_settings']['shipping']['shyplite_height']) ? $meta_data['shipping_settings']['shipping']['shyplite_height'] : '';
        $shyplite_weight = isset($meta_data['shipping_settings']['shipping']['shyplite_weight']) ? $meta_data['shipping_settings']['shipping']['shyplite_weight'] : '';

        $data = array(
            "sourcePin"=>  $request->source_code,
            "destinationPin"=> $request->destination_code,
            "orderType"=> 2,
            "length"=> isset($request->length) ? $request->length : $shyplite_length,
            "width"=> isset($request->width) ? $request->width : $shyplite_width,
            "height"=> isset($request->height) ? $request->height : $shyplite_height,
            "weight"=> isset($request->weight) ? $request->weight : $shyplite_weight,
            "invoiceValue"=> 500
            );
            $data_json = json_encode($data);
            $header = array(
            "x-appid: $appID",
            "x-sellerid: $sell_id",
            "x-timestamp: $timestamp",
            "x-version:3",
            "Authorization: $authtoken",
            "Content-Type: application/json",
            "Content-Length: ".strlen($data_json)
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://api.shyplite.com/pricecalculator');
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            // var_dump($response);
            // exit;
            curl_close($ch);
            return $response;

            }
    
    

            function orderbooked($orderid,$customer,$orderdata){
                $user_id = auth()->user()->id;
                $userSettings = UserInfo::where("user_id", $user_id)->first();
                $meta_data = json_decode($userSettings->meta_data, true);
                    $public_key_id = $meta_data['shipping_settings']['shipping']['public-key-id'];
                    $app_id = $meta_data['shipping_settings']['shipping']['sp-app-id'];
                    $sell_id = $meta_data['shipping_settings']['shipping']['sp-sell-id'];
                    $private_key = $meta_data['shipping_settings']['shipping']['sp-private-key'];
                $timestamp = time();
                $appID = $app_id ;
                $key = $public_key_id;
                $sell_id = $sell_id;
                $secret = $private_key;
                $continuous = $meta_data['shipping_settings']['shipping']['sp-continuous'];
                $sign = "key:". $key ."id:". $appID. ":timestamp:". $timestamp;
                $authtoken = rawurlencode(base64_encode(hash_hmac('sha256', $sign, $secret, true)));
                $ch = curl_init();
                $orders = orders::where('order_ref_id',$orderid)->where('user_id',$user_id)->first();
                $product = ordersProduct::where('order_id',$orders->id)->get();
                $totalprice = 0;
                $skuList = Array();
                foreach($product as  $key => $val)
                {
                    $key = Array
                    (
                    "sku" => $product[$key]->sku,
                    "itemName" => $product[$key]->name,
                    "quantity" => $product[$key]->qty,
                    "price" => $product[$key]->price,
                    "itemLength" => $product[$key]->length, //optional
                    "itemWidth" => $product[$key]->width, //optional
                    "itemHeight"=> $product[$key]->height, //optional
                    "itemWeight" => $product[$key]->productweigth, //optional
                    );
                    array_push($skuList,$key);
                }

                foreach($product as  $key)
                {
                    $totalprice += (int)$key->price;
                }
                $data = Array(
                    "orders" => Array(
                    "0" => Array(
                    "orderId" => $orderid,
                    "customerName" => $customer['fname'],
                    "customerAddress" => $customer['address1'].','. $customer['address2'].','.$customer['city'].','.$customer['state'].','.$customer['country'],
                    "customerCity" => $customer['city'],
                    "customerPinCode" => $customer['pincode'],
                    "customerContact" => $customer['mobileno'],
                    "orderType" => $orderdata["ordertype"],
                    "modeType" => $orderdata["modetype"],
                    "orderDate" => "2021-10-21",
                    "package" =>Array(
                    "itemLength" => $orderdata["packagelength"],
                    "itemWidth" => $orderdata["packagewidth"],
                    "itemHeight"=> $orderdata["packageheight"],
                    "itemWeight" => $orderdata["packageweight"]
                    ),
                    "skuList" =>$skuList,            
                    "totalValue" => $totalprice,
                    "sellerAddressId" => env('SPHYLITE_SELLER_ADDRESS_ID'),
                    )
                    )
                );
                    $data_json = json_encode($data);
                    $header = array(
                    "x-appid: $appID",
                    "x-sellerid:$sell_id",
                    "x-continuous:$continuous",
                    "x-timestamp: $timestamp",
                    "x-version:3",
                    "Authorization: $authtoken",
                    "Content-Type: application/json",
                    "Content-Length: ".strlen($data_json)
                    );
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://api.shyplite.com/order?method=sku');
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
                    curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    $response = curl_exec($ch);
                    // var_dump($response);
                    // exit;
                    curl_close($ch);
                    return $response;
                
            }

    function getShipmentSlip($public_key_id, $app_id, $sell_id, $private_key) {
        $timestamp = time();
        $user_id = auth()->user()->id;
                $userSettings = UserInfo::where("user_id", $user_id)->first();
                $meta_data = json_decode($userSettings->meta_data, true);
                    $public_key_id = $meta_data['shipping_settings']['shipping']['public-key-id'];
                    $app_id = $meta_data['shipping_settings']['shipping']['sp-app-id'];
                    $sell_id = $meta_data['shipping_settings']['shipping']['sp-sell-id'];
                    $private_key = $meta_data['shipping_settings']['shipping']['sp-private-key'];
        $appID = $app_id;
        $key = $public_key_id;
        /** @var string [you will get this key after login function runs.] */
        $secret = $private_key;
        $sign = "key:". $key ."id:". $appID. ":timestamp:". $timestamp;
        $authtoken = rawurlencode(base64_encode(hash_hmac('sha256', $sign, $secret, true)));
        $ch = curl_init();
        $header = array(
        "x-appid: $appID",
        "x-timestamp: $timestamp",
        "x-sellerid:$sell_id", 
        "x-version:3", //required with auth v3
        "Authorization: $authtoken"
        );
        curl_setopt($ch, CURLOPT_URL, 'https://api.shyplite.com/getSlip?orderID='.urlencode("194"));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec($ch);
        // var_dump($server_output);
        //     exit;
       
        curl_close($ch);
    }

    function getTrackEvent($awb){
        $timestamp = time();
        $user_id = auth()->user()->id;
                $userSettings = UserInfo::where("user_id", $user_id)->first();
                $meta_data = json_decode($userSettings->meta_data, true);
                    $public_key_id = $meta_data['shipping_settings']['shipping']['public-key-id'];
                    $app_id = $meta_data['shipping_settings']['shipping']['sp-app-id'];
                    $sell_id = $meta_data['shipping_settings']['shipping']['sp-sell-id'];
                    $private_key = $meta_data['shipping_settings']['shipping']['sp-private-key'];
        $appID = $app_id;
        $key = $public_key_id;
        $sell_id = $sell_id;
        $secret = $private_key;
        $sign = "key:". $key ."id:". $appID. ":timestamp:". $timestamp;
        $authtoken = rawurlencode(base64_encode(hash_hmac('sha256', $sign, $secret, true)));
        $ch = curl_init();

        $data = array(
            
            );
            $data_json = json_encode($data);
            $header = array(
            "x-appid: $appID",
            "x-sellerid: $sell_id",
            "x-timestamp: $timestamp",
            "x-version:3",
            "Authorization: $authtoken",
            "Content-Type: application/json",
            "Content-Length: ".strlen($data_json)
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://api.shyplite.com/track/'.$awb);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            // var_dump($response);
            // exit;
            curl_close($ch);
            return $response;
    }

}
