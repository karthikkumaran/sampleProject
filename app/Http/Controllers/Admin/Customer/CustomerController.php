<?php

namespace App\Http\Controllers\Admin\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Campaign;
use App\Customer;
use App\Models\OrderStatus;
use App\Models\OrderStatusRemarks;
use App\ordersCustomer;
use App\UserInfo;
use App\orders;
use App\Rules\MatchOldPassword;
use App\Models\awbtracking;
use Illuminate\Support\Facades\Hash;
use URL;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('CheckCustomerAuthentication');
    }

    public function userData(Request $link)
    {
        $campaign = Campaign::where('public_link', $link->shareable_link->uuid)->where('is_start', 1)->first();
        if(empty($campaign)){
            $catalog = true;
            $userSettings = UserInfo::where("public_link", $link->shareable_link->uuid)->where('is_start', 1)->first();
        }else
            $userSettings = UserInfo::where("user_id", $campaign->user_id)->where('is_start', 1)->first();

        return ['catalog'=>$catalog,'campaign'=>$campaign,'userSettings'=>$userSettings];
    }

    public function CustomerProfile(Request $link)
    {
        $catalog = false;
        $data=$this->userData($link);
        $catalog=$data['catalog'];
        $campaign=$data['campaign'];
        $userSettings = $data['userSettings'];

        $meta_data = json_decode($userSettings->meta_data, true);

        //cutomer profile page layout
        $layout= 'themes.'.$meta_data['themes']['name'].'.layouts.'. ($catalog ? 'store' : 'public');

        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

        $user=auth()->guard('customer')->user()->id;

        $customer=ordersCustomer::where([['team_id',$userSettings->user->team_id],['customer_id',$user]])->first();

        return view('themes.' . $theme . '.pages.profile', compact('campaign', 'userSettings','meta_data','layout','customer'));
    }

    public function CustomerOrders(Request $link)
    {
        if($link->ajax())
        {
            $team_id=auth()->guard('customer')->user()->store_owner_id;
            
            $user_id=auth()->guard('customer')->user()->customer_id;
            $orders=ordersCustomer::where([['team_id',$team_id],['customer_id',$user_id]])->orderBy('order_id','desc')->get();
            $data=[];
            $status=$link->status;
            if(count($status) == 1){
                if(in_array('All',$status))
                    $data=$orders;
            }else{

            foreach($orders as $key=>$value){
                if(in_array(orders::STATUS[$value->order->status],$link->status))
                    $data[]=$value;
                else
                    continue;
            }
        }

            $userSettings=UserInfo::where('public_link',$link->sLink)->first();
            $meta_data = json_decode($userSettings->meta_data, true);
            $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

            return view('themes.' . $theme . '.cards.customer_orders',["orders"=>$data,'sLink'=>$link->sLink,'status'=>$status,'homeLink'=>$link->homeLink]);
        }

        $catalog = false;
        $data=$this->userData($link);
        $catalog=$data['catalog'];
        $campaign=$data['campaign'];
        $userSettings = $data['userSettings'];
        $meta_data = json_decode($userSettings->meta_data, true);

        //cutomer orders page layout
        $layout= 'themes.'.$meta_data['themes']['name'].'.layouts.'. ($catalog ? 'store' : 'public');

        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

        return view('themes.' . $theme . '.pages.orders', compact('campaign', 'userSettings','meta_data','layout'));
    }

    public function CustomerOrderDetails(Request $link)
    {
        $catalog = false;
        $data=$this->userData($link);
        $catalog=$data['catalog'];
        $campaign=$data['campaign'];
        $userSettings = $data['userSettings'];
        $meta_data = json_decode($userSettings->meta_data, true);
                
        //cutomer orders page layout
        $layout= 'themes.'.$meta_data['themes']['name'].'.layouts.'. ($catalog ? 'store' : 'public');

        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

        $customer=ordersCustomer::find($link->id);
        $orderstatus = OrderStatus::where('order_id', $customer->order_id)->get();  
        $trackingdata = awbtracking::where('shippingorderid',$customer->order->order_ref_id)->get();
        $orderstatusremark = OrderStatusRemarks::where('order_id',$customer->order_id)->get();     

        // dd($orderstatus);
        return view('themes.' . $theme . '.pages.order_details', compact('campaign', 'userSettings','meta_data','layout','customer','orderstatus','orderstatusremark','trackingdata'));
    }

    public function CustomerOrderCancel(Request $request)
    {
        
        if(!empty($request->status) ){
            $order = orders::find($request->order_id)->update(['status' => $request->status]);
            
            $orderstatus=OrderStatus::create(array(
                'order_id' => $request->order_id,
                'status'=> $request->status,
                'changed_by'=> auth()->guard('customer')->user()->id               
            ));
            $orderStatusRemarks=  OrderStatusRemarks::create(array(
                'order_id' => $request->order_id,
                'status' => $request->status,
                'remarks' => $request->remarks,
                'showremarks' => true
            ));
            return true;
        }   
    }

    public function updateProfile(Request $request)
    {
        $user_id=auth()->guard('customer')->user()->id;
        Customer::find($user_id)->update($request->only(['email','country_code','mobile','name']));
        
        return redirect()->to(URL::previous() . "#" . $request->hash);
    }

    public function updateAddress(Request $request)
    {
        $user_id=auth()->guard('customer')->user()->id;
        $customer=Customer::find($user_id);
        $meta_data=json_decode($customer->meta_data,true);
        $meta_data['address']=$request->only('address1','address2','city','pincode','state','country');
        $customer->update(['meta_data'=>json_encode($meta_data)]);

        return redirect()->to(URL::previous() . "#" . $request->hash);
    }

    

    public function ChangePassword(Request $request)
    {
        if(!Hash::check($request->old_password, auth()->guard('customer')->user()->password))
        {
            return redirect()->to(URL::previous() . "#" . $request->hash)->with('error-msg','Invalid old password');
        }

           $request->validate([
                'new_password' => ['required'],
                'new_confirm_password' => ['same:new_password'],
            ]);
            
            $res=Customer::find($request->id)->update(['password'=> Hash::make($request->new_password)]);
        
            return redirect()->to(URL::previous() . "#" . $request->hash)->with('success-msg','Password changed successfully.');
    }

    public function CustomerOrderStatus(Request $link)
    {
        $catalog = false;
        $data=$this->userData($link);
        $catalog=$data['catalog'];
        $campaign=$data['campaign'];
        $userSettings = $data['userSettings'];
        $meta_data = json_decode($userSettings->meta_data, true);
                
        //cutomer orders page layout
        $layout= 'themes.'.$meta_data['themes']['name'].'.layouts.'. ($catalog ? 'store' : 'public');

        $theme = isset($meta_data['themes']['name']) ? $meta_data['themes']['name'] : env('DEFAULT_STOREFRONT_THEME');

        $customer=ordersCustomer::find($link->id);
        $orderstatus = OrderStatus::where('order_id', $customer->order_id)->get();  
        $trackingdata = awbtracking::where('shippingorderid',$customer->order->order_ref_id)->get();
        $orderstatusremark = OrderStatusRemarks::where('order_id',$customer->order_id)->get();     

        // dd($orderstatus);
        return view('themes.' . $theme . '.pages.order_status', compact('campaign', 'userSettings','meta_data','layout','customer','orderstatus','orderstatusremark','trackingdata'));
    }

}