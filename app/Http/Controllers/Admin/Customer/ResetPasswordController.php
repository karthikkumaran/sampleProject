<?php

namespace App\Http\Controllers\Admin\Customer;

use App\Campaign;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Customer;
use App\User;
use App\UserInfo;
use Hash;
use Illuminate\Support\Facades\Crypt;


class ResetPasswordController extends Controller
{
  
    public function getPassword($token) {

        return view('auth.customer.reset', ['token' => $token]);
     }
 
     //customer password reset
     public function updatePassword(Request $request)
     {
         $rules=[
             'email' => 'required|email',
             'password' => 'required|string|min:6|same:password_confirmation',
             'password_confirmation' => 'required|min:6',
         ];

         $messages = [
            'password.min' =>'Password length must be greater than 8 characters',
            'password_confirmation.same' => 'Password Confirmation should match the Password',
            'email.email'=>'Invalid email id'];

        $validator = validator($request->all(), $rules, $messages);
        
        if ($validator->fails()) {
            return back()->with(['message'=>$validator->errors()->first(),'class'=>'danger']);
        }

        $updatePassword = DB::table('password_resets')
                             ->where(['email' => $request->email, 'token' => $request->token])
                             ->first();

 
         if(!$updatePassword)
             return back()->with(['message'=>'Invalid token!','class'=>'danger']);
 
        $start_index=stripos($request->token,'sOIPWr');

        $token=substr($request->token,$start_index+6);
        $end_index=strripos($token,'SoipwR');

        $id=Crypt::decryptString(str_split($token,$end_index)[0]);

        $customer = Customer::find($id);
        $store_id=$customer->store_owner_id;
        $customer->update(['password' => Hash::make($request->password)]);
 
        $Campaign=Campaign::where('team_id',$store_id)->get();
        $user_id=$Campaign[0]->user_id;
        $userSettings=UserInfo::where('user_id',$user_id)->first();
        // $meta_data=json_decode($userSettings->meta_data,true);

        if (empty($userSettings->customdomain)) {
            if (empty($userSettings->subdomain)) {
                $publiclink = url('/');
            } else {
                $publiclink = "https://" . $userSettings->subdomain . "." . env('SITE_URL');
            }
        } else {
            $publiclink = "https://" . $userSettings->customdomain;
        }

        if($Campaign->count()>1)
        {
            $linkstring=$userSettings->public_link;
            $publiclink .= '/s/' . $linkstring;
        }else{
            $linkstring=$Campaign->public_link;
            $publiclink .= '/s/' . $linkstring; 
        }
        
        DB::table('password_resets')->where(['email'=> $request->email])->delete();

        // return redirect()->to($publiclink)->with(['message'=>'Your password has been changed!','class'=>'success','meta_data'=>$meta_data,'userSettings'=>$userSettings,'campaign'=>$Campaign]);
        return redirect()->to($publiclink)->with(['message'=>'Your password has been changed!','class'=>'success']);

     }
 
}
