<?php

namespace App\Http\Controllers\Admin\Customer;

use App\Customer;
use App\Http\Controllers\Controller;
use App\UserInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use App\Mail\customerPasswordResetEmail;

class ForgotPasswordController extends Controller
{
   
    public function postEmail(Request $request)
    {

        $request->validate([
            'email' => 'required|email',
        ]);

        $Customer=Customer::where([['store_owner_id',$request->store_id],['email',$request->email]])->first();

        if(empty($Customer))
            return (['message'=>'Email id does not exist!','class'=>'danger']);
        
        $token = Str::random(20);
        $token= str_split($token,10);
        $token=$token[0].'sOIPWr'.Crypt::encryptString($Customer->id).'SoipwR'.$token[1];

        DB::table('password_resets')->insert(
            ['email' => $request->email, 'token' => $token, 'created_at' => Carbon::now()]
        );

        
        
        $store_owner=UserInfo::where('user_id',$request->store_id)->first();
        $userdata=json_decode($store_owner->meta_data,true);

        Mail::to($request->email)->send(
            new customerPasswordResetEmail(
                $userdata['settings']['emailid'], 
                url('/reset-password/'.$token), 
                $userdata['settings']['emailfromname'],
                $userdata['storefront']['storename']));


        return (['message'=>'We have e-mailed your password reset link!','class'=>'success']);
    }
}
