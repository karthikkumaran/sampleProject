<?php

namespace App\Http\Controllers\Admin\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\WelcomeCustomerMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Customer;
use App\UserInfo;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;
use App\Campaign;
use App\User;
use Auth;

class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    // protected $redirectTo = '/home';   

    public function __construct()
    {
        $this->middleware('guest:customer')->except('logout');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showCustomerRegistrationForm(Request $link)
    {
        //  return back()->with('registerModal',true);
        $campaign = Campaign::where('public_link', $link->shareable_link->uuid)->first();
        if(empty($campaign))
        {
            $userSettings=UserInfo::where("public_link",$link->shareable_link->uuid)->where('is_start', 1)->first();
            $campaign=Campaign::where('user_id',$userSettings->user_id)->where('is_start', 1)->first();
        }else
            $userSettings = UserInfo::where("user_id", $campaign->user_id)->where('is_start', 1)->first();

        $meta_data = json_decode($userSettings->meta_data, true);

        return view('auth.customer.register',['userSettings'=>$userSettings,'meta_data'=>$meta_data,'campaign'=>$campaign]);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'mobile' => ['required', 'string', 'max:13'],
            'password' => 'required|string|min:6',
            'cpassword' =>'required|string|min:6',
        ]);
    }

    /**
     * Create a new staff instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Staff
     */

    protected function createCustomer(Request $request)
    {
        $this->validator($request->all())->validate();
    
        $store_owner_id=$this->findStoreOwnerId($request->slink);

        $customer=Customer::where([['email',$request['email']],
        ['store_owner_id',$store_owner_id]])->first();
        if(!empty($request->slink))
        {
            $userSettings=UserInfo::where("public_link",$request->slink)->where('is_start', 1)->first();
            $user = User::where('id',$userSettings->user_id)->first();
            $team_id = $user->team_id;
            if($userSettings->is_start == 1) {
                if(!empty($userSettings->customdomain)){
                    $homeLink = '//'.$userSettings->customdomain;
            
                }else if(!empty($userSettings->subdomain)) {
                    $homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
            
                }else{
                    $homeLink = route('mystore.published',[$userSettings->public_link]);
                }
                }else{
                    $homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
                }
        }

        if(empty($customer))
        {
            $customer_id=Customer::where('store_owner_id',$store_owner_id)->count();
            $customer=Customer::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'country_code' => $request['country_code'],
                'mobile' => $request['mobile'],
                'password' => Hash::make($request['password']),
                'approved'=>1,
                'store_owner_id'=>$store_owner_id,
                'customer_id'=>$customer_id+1,
            ]);

        }else{
            return ['msg'=>'error'];
        }
        $store_owner = User::where("id",$store_owner_id)->first();
        $data = array();
        $data['customer'] = $customer;
        $data['store_owner'] = $store_owner;
        Mail::send(new WelcomeCustomerMail($data));
        if (Auth::guard('customer')->attempt(['email' => $request->email, 'password' => $request->password, 'store_owner_id'=>$store_owner_id], $request->get('remember'))) {
            return ['msg'=>'success','homeLink'=>$homeLink];
        }

    }

    protected function findStoreOwnerId($slink)
    {
        $shareableLink = ShareableLink::where('uuid', $slink)->first();
        if ($shareableLink == null) {

            $shareable = UserInfo::where('customdomain', $slink )->first();

            if ($shareable == null) {
                $shareable = UserInfo::where('subdomain', $slink )->first();
            }
        } else {
            $shareable = $shareableLink->shareable;
        }
        $team_id = $shareable->user->team_id;
        return $team_id;
    }
}
