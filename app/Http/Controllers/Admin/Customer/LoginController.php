<?php

namespace App\Http\Controllers\Admin\Customer;

use App\Http\Controllers\Controller;
use App\UserInfo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\Campaign;
use Socialite;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    // protected $redirectTo='customer_home';

    public function __construct()
    {
        $this->middleware('guest:customer')->except('logout');
    }

    public function showCustomerLoginForm(Request $link)
    {
        $campaign = Campaign::where('public_link', $link->shareable_link->uuid)->first();
        if(empty($campaign))
        {
            $userSettings=UserInfo::where("public_link",$link->shareable_link->uuid)->where('is_start', 1)->first();
            $campaign=Campaign::where('user_id',$userSettings->user_id)->where('is_start', 1)->first();
        }else
            $userSettings = UserInfo::where("user_id", $campaign->user_id)->where('is_start', 1)->first();
            
        $meta_data = json_decode($userSettings->meta_data, true);

        return view('auth.customer.login',['userSettings'=>$userSettings,'meta_data'=>$meta_data,'campaign'=>$campaign]);
    }

    //main login function
    public function customerLogin(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);
        if(!empty($request->slink))
        {
            $userSettings=UserInfo::where("public_link",$request->slink)->where('is_start', 1)->first();
            $user = User::where('id',$userSettings->user_id)->first();
            $team_id = $user->team_id;
            
            if($userSettings->is_start == 1) {
                if(!empty($userSettings->customdomain)){
                    $homeLink = '//'.$userSettings->customdomain;
            
                }else if(!empty($userSettings->subdomain)) {
                    $homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
            
                }else{
                    $homeLink = route('mystore.published',[$userSettings->public_link]);
                }
                }else{
                    $homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
                }
        }
        if(!empty($team_id)){
            if (Auth::guard('customer')->attempt(['email' => $request->email, 'password' => $request->password,'store_owner_id' => $team_id], $request->get('remember'))) {
                return ['msg'=>'success','homeLink'=>$homeLink];
            }
        }
        else{
            if (Auth::guard('customer')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
                return ['msg'=>'success'];
            }

        }
        return ['msg'=>'error'];
    }

    //function for socail login google and fb
    //redirecting the user to the OAuth provider
    public function socialLogin_Redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    //function for socail login google and fb
    // receiving the callback from the provider after authentication
    public function socialLogin_Callback(Request $request)
    {

    }

    public function logout(Request $request)
    {
        $this->guard('customer')->logout();
        $request->session()->invalidate();
        return back();
    }

    protected function guard()
    {
        return Auth::guard('customer');
    }


}