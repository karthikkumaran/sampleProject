<?php

namespace App\Http\Controllers\Telegram\TelegramHandlers;

use WeStacks\TeleBot\Interfaces\UpdateHandler;
use WeStacks\TeleBot\Objects\Update;
use WeStacks\TeleBot\TeleBot;
use App\Telegram_Bot_Subscriber;
use App\Otp_Verification;
use Seshac\Otp\Otp;

class TelegramUpdateHandler extends UpdateHandler
{
    public static function trigger(Update $update, TeleBot $bot): bool
    {
        return isset($update->message); // handle regular messages (example)
    }

    public function handle()
    {
        $update = $this->update;
        $bot = $this->bot;
        if(!empty($this->update->message->text))
        $telegeram_bot_subscriber = Telegram_Bot_Subscriber::where('telegram_chat_id', $this->update->message->chat->id)->first();
        if(empty($telegeram_bot_subscriber)){
            if(is_numeric($this->update->message->text) && strlen($this->update->message->text) == 6){
                $generated_otp = \DB::table('otps')->where('token', $this->update->message->text)->first();
                if(!empty($generated_otp)){
                    $verify = Otp::validate($generated_otp->identifier, $this->update->message->text);
                    if($verify->status == true){
                        $otp_verification = Otp_Verification::where('otp_reference_id', $generated_otp->id)->first();
                        $user_id = $otp_verification->user_id;
                        $otp_verification->forceDelete();
                        \DB::table('otps')->where('token', $this->update->message->text)->delete();
                        Telegram_Bot_Subscriber::create([
                            'user_id' => $user_id,
                            'telegram_chat_id' => $this->update->message->chat->id,
                            'username' => $this->update->message->chat->username ?? "",
                            'first_name' => $this->update->message->chat->first_name ?? "",
                            'last_name' => $this->update->message->chat->last_name ?? "",
                        ]);
                        $this->sendMessage([
                            'text' => "Your OTP has been verified.",
                        ]);
                    }
                    else{
                        $this->sendMessage([
                            'text' => 'Please enter a valid OTP.',
                        ]);
                    }
                }
                else{
                    $this->sendMessage([
                        'text' => 'Please enter a valid OTP.',
                    ]);
                }
            }
            else{
                $this->sendMessage([
                    'text' => 'Please enter your OTP to verify.',
                ]);
            }
        }
        else{
            if(substr($this->update->message->text, 0, 1) != "/"){
                if(is_numeric($this->update->message->text) && strlen($this->update->message->text) == 6){
                    $generated_otp = \DB::table('otps')->where('token', $this->update->message->text)->first();
                    if(!empty($generated_otp)){
                        $otp_verification = Otp_Verification::where('otp_reference_id', $generated_otp->id)->first();
                        $user_id = $otp_verification->user_id;
                        if($user_id == $telegeram_bot_subscriber->user_id){
                            $this->sendMessage([
                                'text' => 'You have been verified already.',
                            ]);
                        }
                        else{
                            $this->sendMessage([
                                'text' => 'You have already subscribed to a store. Please delete your device from the subscribed store, if you want to subscribe to this store.',
                            ]);
                        }
                    }
                    else{
                        $this->sendMessage([
                            'text' => 'Enter a valid message/command.',
                        ]);
                    }
                }
                else{
                    $this->sendMessage([
                        'text' => 'Enter a valid message/command.',
                    ]);
                }
            }
        }
    }
}