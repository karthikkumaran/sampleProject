<?php

namespace App\Http\Controllers\Telegram\TelegramHandlers;

use WeStacks\TeleBot\Handlers\CommandHandler;

class StartHandler extends CommandHandler
{
    protected static $aliases = [ '/start', '/s' ];
    protected static $description = 'Send "/start" or "/s" to get welcome message.';
    
    public function handle()
    {
        $this->sendMessage([
            'text' => 'Welcome to simplisell'
        ]);
    }
}
