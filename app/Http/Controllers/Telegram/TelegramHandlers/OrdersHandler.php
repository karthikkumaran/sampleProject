<?php

namespace App\Http\Controllers\Telegram\TelegramHandlers;

use WeStacks\TeleBot\Handlers\CommandHandler;
use App\Telegram_Bot_Subscriber;
use App\orders;

class OrdersHandler extends CommandHandler
{
    protected static $aliases = [ '/orders' ];
    protected static $description = 'Send "/orders" to get orders count.';
    
    public function handle()
    {
        $chat_id = $this->update->message->chat->id;
        $telegram_bot_subscriber = Telegram_Bot_Subscriber::where("telegram_chat_id",$chat_id)->first();
        if(!empty($telegram_bot_subscriber)){
            $user_id = $telegram_bot_subscriber->user_id;
            $orders_count = orders::where("user_id",$user_id)->count();
            // $telegram_text = "Total number of orders - ". $orders_count ."\n" . "<b><a href='". route('admin.orders.index') . "'>Click here to view all orders.</a></b>\n";
            $telegram_text = "Total number of orders - ". $orders_count ."\n";
            $this->sendMessage([
                'parse_mode' => 'HTML',
                'text' => $telegram_text,
                'reply_markup' => [
                    'inline_keyboard' => [[[
                        'text' => 'View all orders',
                        // 'url' => route('admin.orders.index')
                        'url' => "https://simplisell.co"
                    ]]]
                ]
            ]);
        }
    }
}
