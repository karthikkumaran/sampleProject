<?php

namespace App\Http\Controllers\Telegram\TelegramHandlers;

use WeStacks\TeleBot\Handlers\CommandHandler;
use App\Telegram_Bot_Subscriber;
use App\enquiry;

class EnquiryHandler extends CommandHandler
{
    protected static $aliases = [ '/enquiry' ];
    protected static $description = 'Send "/enquiry" to get enquiries count in your store.';
    
    public function handle()
    {
        $chat_id = $this->update->message->chat->id;
        $telegram_bot_subscriber = Telegram_Bot_Subscriber::where("telegram_chat_id",$chat_id)->first();
        $user_id = $telegram_bot_subscriber->user_id;
        $enquiry_count = enquiry::where("user_id",$user_id)->count();
        // $telegram_text = "Total number of enquiries - ". $enquiry_count ."\n" . "<b><a href='". route('admin.enquiry.index') . "'>Click here to view all enquiries.</a></b>\n";
        $telegram_text = "Total number of enquiries - ". $enquiry_count ."\n";
        $this->sendMessage([
            'parse_mode' => 'HTML',
            'text' => $telegram_text,
            'reply_markup' => [
                'inline_keyboard' => [[[
                    'text' => 'View all enquiries',
                    // 'url' => route('admin.enquiry.index')
                    'url' => "https://simplisell.co"
                ]]]
            ]
        ]);
    }
}
