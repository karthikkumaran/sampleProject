<?php

namespace App\Http\Controllers\Telegram;

use Illuminate\Http\Request;
use WeStacks\TeleBot\Laravel\TeleBot;
use Seshac\Otp\Otp;
use App\Http\Controllers\Controller;
use App\Otp_Verification;
use App\Telegram_Bot_Subscriber;

class TelegramBotController extends Controller
{
    public function generateOtp(Request $request){
        if($request->ajax()){
            $user = auth()->user();
            $identifier = $user->email . '_telegram';
            $otp =  Otp::setValidity(1440)
                        ->setLength(6)
                        ->setMaximumOtpsAllowed(10)
                        ->setOnlyDigits(true)
                        ->setUseSameToken(false)
                        ->generate($identifier);
            $generated_otp = \DB::table('otps')->where('token', $otp->token)->first();
            Otp_Verification::create([
                'user_id' => $user->id,
                'otp_reference_id' => $generated_otp->id,
                'type' => "Telegram",
            ]);
            
            return $otp->token;
        }
    }

    public function deleteTelegramBotSuscriber(Request $request){
        if($request->ajax()){
            $chat_id = $request->chat_id;
            Telegram_Bot_Subscriber::where("telegram_chat_id", $chat_id)->forceDelete();
    
            return true;
        }
    }
}
