<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Seshac\Otp\Otp;
use Session;
use Auth;
use App\Notifications\WelcomeUserNotification;

class UserVerificationController extends Controller
{
    public function approve($token)
    {
        $user = User::where('verification_token', $token)->first();
        abort_if(!$user, 404);

        $user->verified           = 1;
        $user->verified_at        = Carbon::now()->format(config('panel.date_format') . ' ' . config('panel.time_format'));
        $user->verification_token = null;
        $user->save();

        return redirect()->route('login')->with('message', trans('global.emailVerificationSuccess'));
    }

    public function approveOTP($token)
    {
        $user = User::where('verification_token', $token)->first();
        if(!empty($user)){
            $verify = Otp::validate($user->email, $token);
            if($verify->status == true)
            {
                $user->verified           = 1;
                $user->verified_at        = Carbon::now()->format(config('panel.date_format') . ' ' . config('panel.time_format'));
                $user->verification_token = null;
                $user->save();
            }
            else{
                return redirect()->back()->with(['message' => 'OTP is expired', 'expired' => 'true']);
            }
        }
        else{
            return redirect()->back()->with('message','Invalid OTP');
        }
        // abort_if(!$user, 404);

        $user->notify(new WelcomeUserNotification($user));
        Auth::loginUsingId($user->id, TRUE);
        // return redirect()->route('home');
        // return Auth::loginUsingId($user->id, TRUE);
        // Auth::login($user);
        return redirect()->route('login')->with('message', trans('global.emailVerificationSuccess'));
    }
}
