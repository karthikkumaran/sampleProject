<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use App\Team;
use App\UserInfo;
use App\CompanyInfo;
use App\Customer;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\StoreUserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use App\LaravelSubscriptions\Plan;
use App\LaravelSubscriptions\PlanSubscription;
use App\LaravelSubscriptions\PlanSubscriptionUsage;
use App\LaravelSubscriptions\PlanSubscriptionFeatures;
use App\LaravelSubscriptions\PlanSubscribedFeatures;
use App\Http\Controllers\Traits\CommonFunctionsTrait;
use Carbon\Carbon;
use Session;

// Custom Rules
use App\Rules\TempEmailServiceValidator;
use App\Rules\TopLevelEmailDomainValidator;
use App\Rules\PingableEmailDomainValidator;

class RegisterController extends Controller
{
    use CommonFunctionsTrait;

    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/otpvalidate'; //Redirect to OTP validation page 

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'mobile' => ['required', 'string', 'max:13'],
            'email' => ['required', 'string', 'email', 'max:255', 
                        // Filter temp email domains
                        new TempEmailServiceValidator(),
                        // Check for top level domains
                        new TopLevelEmailDomainValidator(),
                        // Check if domain is active
                        new PingableEmailDomainValidator(), 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $teams = Team::create(array("name" => $data['email']));
        $teamid = $teams->id;
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'mobile' => $data['mobile'],
            'password' => Hash::make($data['password']),
            "approved" => "1",
            "is_on_trial" => "0",
            "roles" => "2",
            "team_id" => $teamid,
        ]);
        $user->roles()->sync("2");
        $userInfo = UserInfo::create(array('user_id' => $user->id));
        $preview_link = ShareableLink::buildFor($userInfo);
        $publish_link = ShareableLink::buildFor($userInfo);
        $preview_link->setActive();
        $userInfo->update(array('id' => $userInfo->id, 'preview_link' => $preview_link->build()->uuid, 'public_link' => $publish_link->build()->uuid));
        $companyInfo = CompanyInfo::create(array('user_id' => $user->id));
        if(env('START_FREE_TRIAL') == "YES"){
            $this->newUserTrialSubscription($user);
        }

        return $user;
    }
  
}
