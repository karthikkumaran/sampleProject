<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\LaravelSubscriptions\Plan;
use App\LaravelSubscriptions\PlanFeature;
use App\LaravelSubscriptions\PlanSubscription;
use App\LaravelSubscriptions\PlanSubscriptionUsage;
use App\LaravelSubscriptions\PlanSubscriptionFeatures;
use App\LaravelSubscriptions\PlanSubscribedFeatures;
use App\Http\Controllers\Traits\CommonFunctionsTrait;

class LoginController extends Controller
{
    use CommonFunctionsTrait;
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if($this->guard()->validate($this->credentials($request))) {
            $user = User::where('email', $request->email)->first();
            // if(!empty($user->deleted_at)){
            //     $this->incrementLoginAttempts($request);
            //     return redirect()->route('login')->with('message', 'The user has been deleted. Kindly contact the administrator.');
            // }
            // else 
            if($user->status != 1){
                $this->incrementLoginAttempts($request);
                return redirect()->route('login')->with('message', 'The user is blocked or deactivated. Kindly contact the administrator.');
            }
            else{
                if(Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                    return $this->sendLoginResponse($request);
                }
            }
        } else {
            $this->incrementLoginAttempts($request);
            return redirect()->route('login')->with('message', 'Please enter valid credentials.');
        }
    }

    protected function authenticated(Request $request, $user)
    {
        if($user->email != $user->team->name){
        $user = User::find($user->team->id); 
        }
        
        $this->setSubscriptionCheckSession($user);

    }

}
