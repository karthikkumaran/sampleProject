<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;
use Session;
use App\User;
use Socialite;
use App\Role;
use App\Team;
use App\UserInfo;
use Carbon\Carbon;
use App\CompanyInfo;
use Illuminate\Support\Facades\Schema;
use App\Notifications\WelcomeUserNotification;
use App\LaravelSubscriptions\Plan;
use App\LaravelSubscriptions\PlanSubscription;
use App\LaravelSubscriptions\PlanSubscriptionUsage;
use App\LaravelSubscriptions\PlanSubscribedFeatures;
use App\LaravelSubscriptions\PlanSubscriptionFeatures;
use App\Http\Controllers\Traits\CommonFunctionsTrait;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;

class SocialController extends Controller
{
    use CommonFunctionsTrait;

    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function Callback($provider){
        $userSocial = Socialite::driver($provider)->stateless()->user();
        // $user = User::where(['email' => $userSocial->getEmail()])->first();
        $user = DB::table('users')->where('email', $userSocial->getEmail())->first();
        if($user){
            if(!empty($user->deleted_at)){
                return redirect()->route('login')->with('message', 'Invalid user. Please contact your administrator.');
            }
            else if($user->status != 1){
                return redirect()->route('login')->with('message', 'The user has been blocked or deactivated. Please contact your administrator.');
            }
            else{
                $users = User::where('id', $user->id)->first();
                if($users->verified != 1) {
                    $users->approved = 1;
                    $users->verified = 1;
                    $users->verified_at = Carbon::now()->format(config('panel.date_format') . ' ' . config('panel.time_format'));
                    $users->verification_token = null;
                    $users->provider = $provider;
                    $users->provider_id = $userSocial->getId();
                    $users->save();
                }
                $this->setSubscriptionCheckSession($users);
                Auth::login($users);
                return redirect()->route('login');
            }
        }else{
            $now = Carbon::now();
            $teams = Team::create(array("name" => $userSocial->getEmail()));
            $teamid = $teams->id;
            $user = User::create([
                'name' => $userSocial->getName(),
                'email' => $userSocial->getEmail(),
                "approved" => "1",
                "verified" => "1",
                "verified_at" => Carbon::now()->format(config('panel.date_format') . ' ' . config('panel.time_format')),
                "verification_token" => null,
                "roles" => "2",
                "team_id" => $teamid,
                'provider' => $provider,
                'provider_id' => $userSocial->getId(),
            ]);
            $user->roles()->sync("2");
            $userInfo = UserInfo::create(array('user_id' => $user->id));
            $preview_link = ShareableLink::buildFor($userInfo);
            $publish_link = ShareableLink::buildFor($userInfo);
            $preview_link->setActive();
            $userInfo->update(array('id' => $userInfo->id,'preview_link' => $preview_link->build()->uuid,'public_link' => $publish_link->build()->uuid));
            $companyInfo = CompanyInfo::create(array('user_id' => $user->id));

            if(env('START_FREE_TRIAL') == "YES"){
                $this->newUserTrialSubscription($user);
            }
        
            Auth::login($user);
            // Auth::loginUsingId($user->id, TRUE);
            return redirect()->route('login');
        }
    }
}
