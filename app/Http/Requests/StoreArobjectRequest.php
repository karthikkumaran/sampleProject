<?php

namespace App\Http\Requests;

use App\Arobject;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreArobjectRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('arobject_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'product_id' => [
                'required',
                'integer'],
            'object'     => [
                'required'],
            'position'   => [
                'required'],
        ];
    }
}
