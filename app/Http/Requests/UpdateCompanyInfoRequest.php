<?php

namespace App\Http\Requests;

use App\CompanyInfo;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateCompanyInfoRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('company_info_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'name'      => [
                'required'],
            'subdomain' => [
                'required'],
            'user_id'  => [
                'required',
                'integer'],
        ];
    }
}
