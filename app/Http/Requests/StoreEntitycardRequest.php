<?php

namespace App\Http\Requests;

use App\Entitycard;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class StoreEntitycardRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('entitycard_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'campagin_id' => [
                'required',
                'integer'],
        ];
    }
}
