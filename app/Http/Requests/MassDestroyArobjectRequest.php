<?php

namespace App\Http\Requests;

use App\Arobject;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyArobjectRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('arobject_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:arobjects,id',
        ];
    }
}
