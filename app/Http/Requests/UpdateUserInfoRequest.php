<?php

namespace App\Http\Requests;

use App\UserInfo;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateUserInfoRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('user_info_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'dob'     => [
                'date_format:' . config('panel.date_format'),
                'nullable'],
            'user_id' => [
                'required',
                'integer'],
        ];
    }
}
