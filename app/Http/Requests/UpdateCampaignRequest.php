<?php

namespace App\Http\Requests;

use App\Campaign;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class UpdateCampaignRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('campaign_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'name'     => [
                'required'],
            'is_start' => [
                'required'],
            'type'     => [
                'required'],
        ];
    }
}
