<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    protected $middleware = [
        \App\Http\Middleware\TrimStrings::class,
        \App\Http\Middleware\TrustProxies::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        // \Fruitcake\Cors\HandleCors::class,
        \App\Http\Middleware\Cors::class,
        \Spatie\HttpLogger\Middlewares\HttpLogger::class,
        // \RenatoMarinho\LaravelPageSpeed\Middleware\InlineCss::class,
    // \RenatoMarinho\LaravelPageSpeed\Middleware\ElideAttributes::class,
    // \RenatoMarinho\LaravelPageSpeed\Middleware\InsertDNSPrefetch::class,
    // \RenatoMarinho\LaravelPageSpeed\Middleware\RemoveComments::class,
    //\RenatoMarinho\LaravelPageSpeed\Middleware\TrimUrls::class, 
    //\RenatoMarinho\LaravelPageSpeed\Middleware\RemoveQuotes::class,
    // \RenatoMarinho\LaravelPageSpeed\Middleware\CollapseWhitespace::class, // Note: This middleware invokes "RemoveComments::class" before it runs.
    // \RenatoMarinho\LaravelPageSpeed\Middleware\DeferJavascript::class,

    ];

    protected $middlewareGroups = [
        'api' => [
            'throttle:60,1',
            'bindings',
            \App\Http\Middleware\AuthGates::class,
        ],
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            \App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
            \App\Http\Middleware\AuthGates::class,
            \App\Http\Middleware\SetLocale::class,
            \App\Http\Middleware\ApprovalMiddleware::class,
            \App\Http\Middleware\VerificationMiddleware::class,
        ],
    ];

    protected $routeMiddleware = [
        'can'              => \Illuminate\Auth\Middleware\Authorize::class,
        'auth'             => \Illuminate\Auth\Middleware\Authenticate::class,
        'guest'            => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'signed'           => \Illuminate\Routing\Middleware\ValidateSignature::class,
        'throttle'         => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'cache.headers'    => \Illuminate\Http\Middleware\SetCacheHeaders::class,
        'bindings'         => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'password.confirm' => \Illuminate\Auth\Middleware\RequirePassword::class,
        'auth.basic'       => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'shared'           => \Sassnowski\LaravelShareableModel\Http\Middleware\ValidateShareableLink::class,
        // "cors" => \App\Http\Middleware\Cors::class,
        'domainCheck' => \App\Http\Middleware\DomainCheck::class,
        'uniquevisitor' => \App\Http\Middleware\UniqeVisitor::class,
        'subscription_check' => \App\Http\Middleware\SubscriptionCheck::class,
        'store_front_subscription_check' => \App\Http\Middleware\StoreFrontSubscriptionCheck::class,
        'CheckCustomerAuthentication' => \App\Http\Middleware\CheckCustomerAuthentication::class,
        'referral' => \Questocat\Referral\Http\Middleware\CheckReferral::class,
    ];
}
