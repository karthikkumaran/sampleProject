<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Session;

class SubscriptionCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->ajax()) {
            $user = auth()->user();
            if ($user->getIsAdminAttribute() == false) {
                $subscription_session = $request->session()->get('subscription_check');

                $user_trial = $user->isOnUserTrial();
                if (!empty($subscription_session)) {
                    if ($user->isFirstTimeUser() == true) {

                        // if ($user->id == $user->team_id) {
                        if ($user->email == $user->team->name) {

                            return redirect()->route('admin.campaigns.storeIndex');

                        }
                    } else if ($subscription_session[0]["no_subscription"] == true) {
                        if (!app('impersonate')->isImpersonating()) {
                            return redirect()->route('admin.subscriptions.index')->with('no_subscription', 'Please subscribe to continue.');
                        }
                    } else {

                        if ($subscription_session[0]["trial_subscription"] == true) {

                            Session::flash('user_trial_subscribed', ['trial_subscription' => $subscription_session[0]["trial_subscription"], 'trial_duration' => $subscription_session[0]["trial_duration"]]);
                        } else if ($subscription_session[0]["subscription_expiry_alert"] == true) {
                            Session::flash('subscription_expiry_alert', $subscription_session[0]["subscription_expiry_alert"]);
                        } else {
                            Session::flash('subscription_expiry_alert', false);
                        }
                        
                        if( isset($subscription_session[0]["subscription_start_alert"]) && $subscription_session[0]["subscription_start_alert"] == true){
                            Session::flash('subscription_start_alert','Your trial subscription has ended and your plan not yet started,Please check myplan.');
                            if(!app('impersonate')->isImpersonating()){
                                       return redirect()->route('admin.subscriptions.index');
                            }
                        }
                    
                        if($subscription_session[0]["subscription_expired"] == true){
                            if($subscription_session[0]["grace_period"] != false){
                                Session::flash('subscription_grace_period_alert', $subscription_session[0]["grace_period"]);  
                            }
                            else{
                                Session::flash('subscription_grace_period_alert', false);
                                Session::flash('subscription_expired', 'Your subscription has expired kindly renew it or choose another plan to continue.');
                                if (!app('impersonate')->isImpersonating()) {
                                    return redirect()->route('admin.subscriptions.index');
                                }
                            }
                        }
                    }
                }
            }
        }

        // return $next($request);
        $response = $next($request);
        // if ($response instanceof Response) {
        // file_put_contents(__DIR__ . '/2.txt', 'hello terminate');

        $response->headers->add(['X-RESPONSE-TIME' => microtime(true) - LARAVEL_START]);
        // }

        return $response;
    }

    public function terminate($request, $response)
    {
        // At this point the response has already been sent to the browser so any
        // modification to the response (such adding HTTP headers) will have no effect
        // if ($request instanceof Request) {
        // file_put_contents(__DIR__ . '/1.txt', 'hello terminate');
        app('log')->debug('Response time', [
            'method' => $request->getMethod(),
            'uri' => $request->getRequestUri(),
            'seconds' => microtime(true) - LARAVEL_START,
        ]);
        // }
    }
}
