<?php

namespace App\Http\Middleware;

use App\Role;
use Closure;
use Illuminate\Support\Facades\Gate;
use Session;

class VerificationMiddleware
{
    public function handle($request, Closure $next)
    {
        if (auth()->check()) {
            $user = auth()->user();
            if (!$user->verified) {
                auth()->logout();

                // return redirect()->route('otpvalidate');
                Session::put('user', $user->id);
                return redirect('/otpvalidate');
            }
        }

        return $next($request);
    }
}
