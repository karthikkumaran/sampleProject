<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use App\Models\Statistics\UniqueVistor as Visitor;
use App\UserInfo;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;

class UniqeVisitor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $shareableLink = null;
        $slink = null;

        try {
            // $uri = request()->getRequestUri();
            // $slink = substr($uri, strripos($uri, '/') + 1);
            // $team_id = null;

            // if (!$slink) {
            //     $domain = $_SERVER['HTTP_HOST'];
            //     $user = UserInfo::where('customdomain', $domain)->first();
            //     $team_id = $user->user->team_id;
            // } else {
            //     $shareableLink = ShareableLink::where('uuid', $slink)->first();
            //     $team_id = $shareableLink->shareable->user->team_id;
            // }
            // $visitor = [
            //     'ip_address' => $ip = request()->ip(),
            //     'latitude' => geoip($ip)->getAttribute('lat'),
            //     'longitude' => geoip($ip)->getAttribute('lon'),
            //     'client_ips' => request()->getClientIps() ?: null,
            //     'country_name' => ucwords(geoip($ip)->getAttribute('country')),
            //     'is_from_trusted_proxy' => request()->isFromTrustedProxy(),
            //     'state_name' => geoip($ip)->getAttribute('state_name'),
            //     'postal_code' => geoip($ip)->getAttribute('postal_code'),
            //     'timezone' => geoip($ip)->getAttribute('timezone'),
            //     'city' => geoip($ip)->getAttribute('city'),
            //     'team_id' => $team_id,
            // ];

            // Visitor::create($visitor);
        } catch (Exception $e) {
            // report($e);
        }
        return $next($request);
    }
}
