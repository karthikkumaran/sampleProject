<?php

namespace App\Http\Middleware;

use App\Role;
use Closure;
use Illuminate\Support\Facades\Gate;
use Auth;

class ApprovalMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        // if ($guard == "customer" && Auth::guard($guard)->check()) {
        //     if (!auth()->user()->approved) {
        //         auth()->logout();

        //         return redirect()->route('customer')->with('message', trans('global.yourAccountNeedsAdminApproval'));
        //     }
        // }


        if (auth()->check()) {
            if (!auth()->user()->approved) {
                auth()->logout();

                return redirect()->route('otpvalidate');
                // return redirect()->route('login')->with('message', trans('global.yourAccountNeedsAdminApproval'));
            }
        }

        return $next($request);
    }
}
