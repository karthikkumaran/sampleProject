<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Redirect;
use Sassnowski\LaravelShareableModel\Shareable\Shareablerequest;
use App\UserInfo;
use App\User;

use Closure;

class StoreFrontSubscriptionCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request 
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->ajax()) {

            if ($request->subdomain == "www" && $request->domain == "simplisell")
                return Redirect::to(env('APP_URL', 'localhost'));

            if (!empty($request->subdomain) && !empty($request->domain) && !empty($request->tld)) {
                if($request->subdomain == "www")
                    $cdomain = $request->domain . "." . $request->tld;
                else
                    $cdomain = $request->subdomain . "." . $request->domain . "." . $request->tld;
                    $user_info = UserInfo::where('customdomain', $cdomain)->first();
            }
            if (empty($request->subdomain) && !empty($request->domain) && !empty($request->tld)) {
                $cdomain = $request->domain . "." . $request->tld;
                $user_info = UserInfo::where('customdomain', $cdomain)->first();
            }
        
            if (empty($user_info)){
                if (!empty($request->subdomain)){
                    $user_info = UserInfo::where('subdomain', $request->subdomain)->first();
                    if(!empty($user_info))
                    $user = $user_info->user;
                }
                else if (!empty($request->shareable_request->uuid)) {
                    $shareablerequest = Shareablerequest::where('uuid', $request->shareable_request->uuid)->first();
                    $shareable = $shareablerequest->shareable;
                    $user = $shareable->user;
                }
            }
            else {
                $user = $user_info->user;
            }
            if(!empty($user)) 
            if ($user->isSubscriptionExpired() == true) {
                $grace_period = $user->getGracePeriod();
                if ($grace_period == false) {
                    return redirect()->route('store_subscription_expired');
                }
            }
        }
        return $next($request);
    }
}