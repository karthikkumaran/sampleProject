<?php

namespace App\Http\Middleware;

use Closure;

class DomainCheck
{ 
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        // if($request->subdomain == "www")
        //     return Redirect::to(env('APP_URL', 'localhost'));
        // if(env('SITE_URL') == $request->domain.".".$request->tld)
        //     return Redirect::to("login");
        // if(!empty($request->subdomain) && !empty($request->domain) && !empty($request->tld)){
        //     $cdomain = $request->subdomain.".".$request->domain.".".$request->tld;
        //     $data = Campaign::where('customdomain', $cdomain)->first();
        // }
        // if(empty($request->subdomain) && !empty($request->domain) && !empty($request->tld)){
        //     $cdomain = $request->domain.".".$request->tld;
        //     $data = Campaign::where('customdomain', $cdomain)->first();
        // }
        // if(empty($data))
        // if(!empty($request->subdomain))
        //     $data = Campaign::where('subdomain', $request->subdomain)->first();
        
        return $next($request);
    }
}
