<?php

namespace App\Http\Livewire\Storefront\Themes\ClassicTheme;

use Livewire\Component;
use App\ProductVariant;
use App\Product;
use App\Variant;

class CatalogDetails extends Component
{
    public $campaign;
    public $userSettings;
    public $meta_data;
    public $product;
    public $attributes;
    public $values;
    public $variant;
    public $variants;
    public $parentproduct;
    public $novariantvalue;
    public $novaluetype;
    public $q;

    protected $listeners = ['productVariant','productCombinationalVariant'];

    public function mount($campaign, $userSettings, $meta_data, $product,$attributes,$values)
    {
        // $this->productVariant($this->q);
        if(!empty($product->has_variants))
        {
            

            $this->product = ProductVariant::where('product_id',$product->id)->first();
            $this->q = $this->product->id;
            $this->parentproduct = Product::where('id',$this->product->product_id)->first();
        }
        $this->variants = Variant::all();
    }
    public function productVariant($id)
    {
        $this->q = $id;
        $this->product = ProductVariant::find($id);
        $this->parentproduct = Product::where('id',$this->product->product_id)->first();

    }
    public function productCombinationalVariant($type, $value, $product)
    {
            $variantdata =json_decode($this->product->variant_data);
            $var = array();
            foreach($variantdata as $key => $val)
            {
                    if($val->type == $type)
                    {
                        $var[$key]['type'] = $val->type;
                        $var[$key]['value'] = $value; 
                    }
                    else{
                        $var[$key]['type'] = $val->type;
                        $var[$key]['value'] = $val->value; 
                    }
            }
            $data = json_encode($var);
            $variantproduct = ProductVariant::where('product_id',$this->product->product_id)->where('variant_data',$data)->first();
            if(empty($variantproduct)){
                $variantproduct = ProductVariant::where('product_id',$this->product->product_id)->get();
                foreach($variantproduct as $key => $val){
                    $variantdata = json_decode($val->variant_data);
                    foreach($variantdata as  $var)
                    {
                        if($var->value == $value)
                        {
                            break 2;
                        }
                    }
                }
                $var = array();
                foreach($variantdata as $key => $val)
                {
                    $var[$key]['type'] = $val->type;
                    $var[$key]['value'] = $val->value; 
                }
                $data = json_encode($var);  
                $variantproduct = ProductVariant::where('product_id',$this->product->product_id)->where('variant_data',$data)->first();
                $this->product = $variantproduct;
                
            }
            else{
            $this->product = $variantproduct;
            $this->q = $this->product->id;
            }
            $this->parentproduct = Product::where('id',$this->product->product_id)->first();



    }
    public function render()
    {
        return view('livewire.storefront.themes.classic-theme.catalog-details',['product' => $this->product,'variants' => $this->variants,'q' => $this->q, 'parentproduct' => $this->parentproduct]);
    }
}
