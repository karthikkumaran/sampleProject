<?php

namespace App\Http\Livewire\Storefront\Themes\RusticTheme;

use Livewire\Component;

class Checkout extends Component
{
    public $campaign;
    public $userSettings;
    public $meta_data;
    public $customer;
    public $catalogLink;
    public $shareableLink;
    public $homeLink;
    public $currency = "₹";
    public $contactNumber = "";
    public $emailFrom;
    public $guest_user=false;
    public $payment_settings;
    public $shoppableLink;

    public function mount($campaign, $userSettings, $meta_data, $customer){
        if($this->campaign->is_start == 1) {
            $this->catalogLink = route('campaigns.published',[$this->campaign->public_link]);
            $this->shareableLink = $this->campaign->public_link;
            $this->shoppableLink =  route('mystore.publishedVideo',$userSettings->public_link);
        }else{
            $this->catalogLink = route('admin.campaigns.preview',[$this->campaign->preview_link]);
            $this->shareableLink = $this->campaign->preview_link;
            $this->shoppableLink =  route('mystore.publishedVideo',$userSettings->preview_link);
        }

        if($this->userSettings->is_start == 1) {
            if(!empty($this->userSettings->customdomain)){
                $this->homeLink = '//'.$this->userSettings->customdomain;
            }else if(!empty($this->userSettings->subdomain)) {
                $this->homeLink = '//'. $this->userSettings->subdomain.".".env('SITE_URL');
            }else{
                $this->homeLink = route('mystore.published',[$this->userSettings->public_link]);
                $this->shoppableLink =  route('mystore.publishedVideo',$userSettings->public_link);
            }
        }else{
            $this->homeLink = route('admin.mystore.preview',[$this->userSettings->preview_link]);
            $this->shoppableLink =  route('mystore.publishedVideo',$userSettings->preview_link);
        }
            
        if(!empty($this->meta_data['settings']['currency'])){
            if($this->meta_data['settings']['currency_selection'] == "currency_symbol")
                $this->currency = \App\User::CURRENCY_SYMBOLS[$this->meta_data['settings']['currency']];
            else
                $this->currency = $this->meta_data['settings']['currency'];
        }
            
        if(!empty($this->meta_data['settings']['contactnumber']))
            $this->contactNumber = $this->meta_data['settings']['contactnumber'];
            
        if(empty($this->meta_data['settings']['emailid']))
            $this->emailFrom = $this->campaign->user->email;
        else
            $this->emailFrom = $this->meta_data['settings']['emailid'];
            
        if(!empty($this->meta_data['payment_settings']['payment_method']))
            $this->payment_settings = $this->meta_data['payment_settings'];
        else
            $this->payment_settings=['payment_method'=>["Enquiry"],'Enquiry'=>['desc-text'=>'']]; 
            
        if(!auth()->guard('customer')->check())
        {
            if(isset($this->meta_data['checkout_settings']) && $this->meta_data['checkout_settings']['guest_user'] == "on")
                $this->guest_user=true;
        }
    }
    
    public function render()
    {
        return view('livewire.storefront.themes.rustic-theme.checkout');
    }
}
