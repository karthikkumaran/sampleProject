<?php

namespace App\Http\Livewire\Admin\Variants;

use Livewire\WithPagination;
use Livewire\Component;
use App\Variant;
use App\Product;
use App\ProductVariant;

class ProductVariants extends Component
{
    public $product;
    public $original_product;
    public $setVariants = "block";
    public $selectProducts = "none";
    public $products_uploaded = "none";
    public $variant_types = [];
    public $variant_values = [];
    public $product_variants = [];
    public $product_variants_list = [];
    public $product_variants_count = 0;
    public $product_variants_last=[];

    public function mount($product) {
        $this->product = $product->toArray();
        $this->original_product = $product;
        $product_variant_id=ProductVariant::where('product_id',$product->id)->get()->last();
        $this->product_variants_last=$product_variant_id;
        // print_r($this->product_variants_last);
    }

    public function getProductVariants($product_variants) {
        $existing_variants = [];
        $unique_product_variants_list = [];
        $original_product_variants = $this->original_product->product_variants;
        if(!empty($original_product_variants)) {
            foreach($original_product_variants as $original_product_variant) {
                $x = $original_product_variant->variant_data;
                $y = json_decode($x, true);
                array_push($existing_variants, $y);
            }
        }

        foreach ($product_variants as $product) {
            if(!empty($existing_variants)){
                $variant_exists = false;
                foreach($existing_variants as $existing_variant){
                    foreach($product["variants"] as $variant) {
                        if(in_array($variant, $existing_variant)) {
                            $variant_exists = true;
                        }
                        else{
                            $variant_exists = false;
                        }
                    }
                }
                if($variant_exists == true) {
                    continue;
                }
                else{
                    array_push($unique_product_variants_list, $product);
                    array_push($this->product_variants_list, $product["variants"]);
                }
            }
            else{
                array_push($unique_product_variants_list, $product);
                array_push($this->product_variants_list, $product["variants"]);
            }
        }

        $this->product_variants = $unique_product_variants_list;
        $this->product_variants_count = count($unique_product_variants_list);
        $this->setVariants = "none";
        $this->selectProducts = "block";
    }

    public function addSelectedProductVariants($selected_product_variants) {

        $add_to_store = [];
        $product_name = [];
        $sku = [];
        $price = [];
        $stock = [];
        $out_of_stock = [];
        $minimum_quantity = [];
        $selected_products = array();

        foreach ($selected_product_variants as $product_variants) {
            if($product_variants["name"] == "add_to_store") {
                $add_to_store = explode (",", $product_variants["value"]);
            }
            else if($product_variants["name"] == "product_name") {
                $product_name = explode (",", $product_variants["value"]);
            }
            else if($product_variants["name"] == "sku") {
                $sku = explode (",", $product_variants["value"]);
            }
            else if($product_variants["name"] == "price") {
                $price = explode (",", $product_variants["value"]);
            }
            else if($product_variants["name"] == "stock") {
                $stock = explode (",", $product_variants["value"]);
            }
            else if($product_variants["name"] == "out_of_stock") {
                $out_of_stock = explode (",", $product_variants["value"]);
            }
            else if($product_variants["name"] == "minimum_quantity") {
                $minimum_quantity = explode (",", $product_variants["value"]);
            }
        }

        $this->original_product->has_variants = 1;
        $this->original_product->save();
        for($i = 0; $i < count($add_to_store); $i++) {
            if($add_to_store[$i] == "1"){
                array_push($selected_products, new ProductVariant([
                    'product_id' => $this->product["id"],
                    'variant_data' => json_encode($this->product_variants_list[$i]),
                    'name' => $product_name[$i] ?? NULL,
                    'price' => $price[$i] ?? NULL,
                    'sku' => $sku[$i] ?? NULL,
                    'stock' => $stock[$i] ?? NULL,
                    'out_of_stock' => $out_of_stock[$i] ?? NULL,
                    'minimum_quantity' => $minimum_quantity[$i] ? $minimum_quantity[$i] :NULL,
                    'team_id' => $this->product["team_id"],
                ]));
            }
        }
        $this->original_product->product_variants()->saveMany($selected_products);
        $this->selectProducts = "none";
        $this->products_uploaded = "block";

        return redirect()->route("admin.products.product_list");
    }


    public function render()
    {
        return view('livewire.admin.variants.product-variants');
    }
}
