<?php

namespace App\Http\Livewire\Admin\Variants;

use Livewire\Component;
use App\Variant;

class VariantsList extends Component
{
    public $variants;
    public $variant_name;
    public $variant_id;
    public $alert=false;
    public $edit = false;
    public $data = [];

    protected $rules = [
        'variant_name' => 'required',
    ];

    public function mount()
    {
        $this->variants = Variant::where("team_id", auth()->user()->team_id)->get();
    }

    //create variant
    public function createVariant()
    {
        $this->validate();

        $this->setData();

        Variant::create([
            'name' => $this->variant_name,
            'team_id' => auth()->user()->team_id,
        ]);

        $this->reset();
        $this->mount();
        $this->emit('close_variant_modal');
        // return $this->alert('success', __('Variant Created Successfully!'));
        $this->dispatchBrowserEvent('alert',
        ['type' => 'success',  'message' => 'Variant Created Successfully!']);
        return back();
    }

    // edit variant
    public function editVariant($id)
    {
        $this->edit=true;
        $this->variant_id = $id;
        $variant = Variant::find($id);
        $this->variant_name = $variant->name;
        $this->emit('open_variant_edit_modal');
    }

    //update variant
    public function updateVariant()
    {
        $this->validate();

        $this->setData();

        $variant = Variant::find($this->variant_id);
        $variant->name=$this->variant_name;
        $variant->save();

        $this->reset();
        $this->mount();
        $this->edit=false;
        $this->emit('close_variant_modal');
        // return $this->alert('success', __('Variant Updated Successfully!'));
        $this->dispatchBrowserEvent('alert',
        ['type' => 'success',  'message' => 'Variant Updated Successfully!']);
        return back();
    }

    public function setData()
    {
        $this->data['variant_name'] = $this->variant_name;
    }

    public function closeVariantEditModal()
    {
        if($this->edit==true)
        {
            $this->edit=false;
            $this->reset();
            $this->mount();
        }
    }

    public function render()
    {
        return view('livewire.admin.variants.variants-list');
    }
}
