<?php

namespace App\Http\Livewire\Admin\Orders;

use Livewire\Component;
use App\orders;
use App\ordersCustomer;
use App\UserInfo;
use App\User;
use Carbon\Carbon;

class OrderList extends Component
{

    protected $orders; //orders list

    public $currency; //currency
    public $order_info_text = "No orders found";
    public $order_status='*';
    public $order_mode='';
    public $time='';
    public $datefrom='';
    public $dateto='';

    public $page = 10; // no of pages
    public $q=''; // search query


    protected $listeners = ['SearchOrder','SearchOrderStatus','SearchOrderMode','SearchDay','updateGenerateInvoice']; //event for order search


    public function mount()
    {

        // $this->orders = ordersCustomer::join('orders','orders_customers.order_id','=','orders.id')
        //                 ->select('orders_customers.*','orders.status','orders.order_id as store_order_id')
        //                 ->paginate($this->page);

        $this->SearchOrder($this->q);

        $user_id = auth()->user()->id;
        $userSettings = UserInfo::where("user_id", $user_id)->first();
        $user_meta_data = json_decode($userSettings->meta_data, true);

        if (empty($user_meta_data['settings']['currency']))
            $this->currency = "₹";
        else
        if ($user_meta_data['settings']['currency_selection'] == "currency_symbol")
            $this->currency = User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
        else
            $this->currency = $user_meta_data['settings']['currency'];
    }

    public function SearchOrder($q)
    {

        if($this->time == 'today')
       {
           $df = strtotime('today');
          $this->datefrom =  date("Y-m-d H:i:sa", $df);
          $dt = strtotime('tomorrow');
          $this->dateto = date("Y-m-d H:i:sa", $dt);
       }
       elseif($this->time == 'yesterday')
       {
           $df = strtotime('yesterday');
          $this->datefrom =  date("Y-m-d H:i:sa", $df);
          $dt = strtotime('today');
          $this->dateto = date("Y-m-d H:i:sa", $dt);
       }
       elseif($this->time == 'last_7_days')
       {
           $df = strtotime('-7 days');
           $this->datefrom =  date("Y-m-d H:i:sa", $df);
           $dt = strtotime('tomorrow');
           $this->dateto = date("Y-m-d H:i:sa", $dt);
           // dd($this->datefrom,$this->dateto);
       }
       elseif($this->time == 'last_30_days'){
           $df = strtotime('-30 days');
           $this->datefrom =  date("Y-m-d H:i:sa", $df);
           $dt = strtotime('tomorrow');
           $this->dateto = date("Y-m-d H:i:sa", $dt);
       }

        $this->orders = ordersCustomer::join('orders',function($join) use($q) {
            if($this->order_mode && $this->order_mode != '*'){
                   $data= $join->on('orders_customers.order_id','=','orders.id');
                        if($this->time==''&& $this->order_status=='*')
                            $data->where('orders.order_mode','=',$this->order_mode)->whereYear('orders.created_at', '=', date('Y'));
                        elseif(empty($this->time) && $this->order_status!='*'){
                            $data->where('orders.order_mode','=',$this->order_mode)->where('orders.status', "=",$this->order_status);
                        }
                        elseif($this->order_status!='*'&& $this->time!='thismonth'&& $this->time!='yearly'){
                            $data->where('orders.order_mode','=',$this->order_mode)->where('orders.status', "=",$this->order_status)->where('orders.created_at',">",$this->datefrom)->where('orders.created_at',"<=",$this->dateto);
                        }
                        elseif($this->order_status!='*'&& $this->time=='yearly')
                            $data->where('orders.order_mode','=',$this->order_mode)->where('orders.status', "=",$this->order_status)->whereYear('orders.created_at', '=', date('Y'));

                        elseif($this->order_status!='*'&& $this->time=='thismonth')
                            $data->where('orders.order_mode','=',$this->order_mode)->where('orders.status', "=",$this->order_status)->whereYear('orders.created_at', '=', date('Y'))->whereMonth('orders.created_at', '=', date('m'));

                        elseif($this->order_status=='*'&& $this->time!='thismonth'&& $this->time!='yearly'){
                            $data->where('orders.order_mode','=',$this->order_mode)->where('orders.created_at',">",$this->datefrom)->where('orders.created_at',"<=",$this->dateto);
                        }

                        elseif($this->order_status=='*'&&  $this->time=='yearly')
                             $data->where('orders.order_mode','=',$this->order_mode)->where('orders.order_mode','=',$this->order_mode)->whereYear('orders.created_at', '=', date('Y'));

                        elseif($this->order_status=='*'&&  $this->time=='thismonth')
                            $data->where('orders.order_mode','=',$this->order_mode)->whereYear('orders.created_at', '=', date('Y'))->whereMonth('orders.created_at', '=', date('m'));

                         elseif($this->order_status!='*')
                            $data->where('orders.order_mode','=',$this->order_mode)->where('orders.status', "=",$this->order_status);
                    }
                    elseif($this->order_mode=='*' || $this->order_mode==''){
                        $data= $join->on('orders_customers.order_id','=','orders.id');
                        if($this->time==''&& $this->order_status=='*')
                            $data->whereYear('orders.created_at', '=', date('Y'));
                        elseif(empty($this->time) && $this->order_status!='*'){
                            $data->where('orders.status', "=",$this->order_status);
                        }
                        elseif($this->order_status!='*'&& $this->time!='thismonth'&& $this->time!='yearly'){
                            $data->where('orders.status', "=",$this->order_status)->where('orders.created_at',">",$this->datefrom)->where('orders.created_at',"<=",$this->dateto);
                        }
                        elseif($this->order_status!='*'&& $this->time=='yearly')
                            $data->where('orders.status', "=",$this->order_status)->whereYear('orders.created_at', '=', date('Y'));

                        elseif($this->order_status!='*'&& $this->time=='thismonth')
                            $data->where('orders.status', "=",$this->order_status)->whereYear('orders.created_at', '=', date('Y'))->whereMonth('orders.created_at', '=', date('m'));

                        elseif($this->order_status=='*'&& $this->time!='thismonth'&& $this->time!='yearly'){
                            $data->where('orders.created_at',">",$this->datefrom)->where('orders.created_at',"<=",$this->dateto);
                        }

                        elseif($this->order_status=='*'&&  $this->time=='yearly')
                             $data->whereYear('orders.created_at', '=', date('Y'));

                        elseif($this->order_status=='*'&&  $this->time=='thismonth')
                            $data->whereYear('orders.created_at', '=', date('Y'))->whereMonth('orders.created_at', '=', date('m'));

                         elseif($this->order_status!='*')
                            $data->where('orders.status', "=",$this->order_status);
                    }

                        $data->where(function ($query) use ($q) {
                        $query->orwhere('orders_customers.mobileno', "LIKE", '%' . $q . "%")
                        ->orwhere('orders_customers.fname', "LIKE", '%' . $q . "%")
                        ->orwhere('orders_customers.lname', "LIKE", '%' . $q . "%")
                        ->orwhere('orders_customers.meta_data->product_total_price', "LIKE", '%' . $q . "%")
                        ->orwhere('orders_customers.created_at', "LIKE", '%' . $q . "%")
                        ->orwhere('orders.order_ref_id', "LIKE", '%' . $q . "%");
                    });

        })->whereNotNull('orders.transaction_id')->select('orders_customers.*','orders.status','orders.is_generate_invoice','orders.order_ref_id as store_order_id')->orderBy('id','desc')->paginate($this->page);
        // dd($this->orders);
        if ($this->orders->total()==0) {
            $this->order_info_text = "No orders match your search";
            // $this->alert('info', __('No orders match your search!'));
            $this->dispatchBrowserEvent('alert',
            ['type' => 'info',  'message' => 'No orders match your search!']);
        }
    }

    public function SearchOrderStatus($status)
    {
        $this->order_status=$status;
        $this->SearchOrder($this->q);

    }

    public function SearchOrderMode($mode)
    {
        $this->order_mode=$mode;
        $this->SearchOrder($this->q);

    }

    public function SearchDay($d)
    {
       $this->time=$d ;
       $this->SearchOrder($this->q);
    }
    public function updateGenerateInvoice($order_id)
    {
        $orders = orders::find($order_id);
        $now = carbon::now()->format('Y-m-d H:i:s');
        $inv_id = orders::where('team_id',$orders->team->id)->whereNotNull('inv_num')->count();
        if(!empty($inv_id)){
            $inv_num = $inv_id + 1;
        }
        else{
            $inv_num = 1;
        }
        $orders->update(['is_generate_invoice' => true,'inv_date' => $now ,'inv_num' => $inv_num]);
        $this->SearchOrder($this->q);
        // $this->dispatchBrowserEvent('alert',
        // ['type' => 'error',  'success' => 'Generate Invoice updated successfully!']);
        // this->dispatchBrowserEvent('alert',
        //         ['type' => 'success',  'message' => 'User Created Successfully!']);
        // return $this->alert('success',__('Generate Invoice updated successfully!'));
    //  session()->flash('success',__('Generate Invoice updated successfully!'));
    $this->dispatchBrowserEvent('alert',
                ['type' => 'success',  'message' => 'Generate Invoice updated successfully!']);
     return back();
    }

    public function render()
    {
        return view('livewire.admin.orders.order-list', ['orders' => $this->orders]);
    }
}
