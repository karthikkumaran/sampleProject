<?php

namespace App\Http\Livewire\Admin\Coupon;

use App\Models\promocode_user;
use Livewire\Component;
use Gabievi\Promocodes\Facades\Promocodes;
use Gabievi\Promocodes\Models\Promocode;
use App\UserInfo;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\DB;

class CouponList extends Component
{
    protected $listeners = [
        'confirmed'
    ];

    public $coupons;
    public $alert=false;
    public $coupon_count;
    public $disable_coupon_id;

    public $discount;
    public $expires_in;
    public $description;
    public $start_date;
    public $min_cart_value;
    public $usage_limit;
    public $unit;
    public $data =[];


    //default values
    public $amount=1;
    public $quantity = null;
    public $is_disposable = false;

    //edit coupon
    public $edit = false;
    public $coupon_id;

    protected $rules = [
        'discount' => 'required|integer',
        'expires_in' => 'required|date|after_or_equal:start_date',
        'description' => 'required',
        'start_date' => 'required|date|before_or_equal:expires_in',
        'min_cart_value' => 'required|numeric',
        'unit'=>'required',
        'usage_limit'=>'required|integer',
    ];

    public function mount()
    {
        $user=UserInfo::where('user_id',auth()->user()->id)->first();
        $meta_data = json_decode($user->meta_data,true);
        $this->coupons=Promocode::where("data->team_id",auth()->user()->team_id)->get();
        $coupon_user=promocode_user::select('promocode_id', DB::raw('count(*) as total'))->groupBy('promocode_id')->get()->groupBy('promocode_id')->toArray();
        $this->coupon_count=$coupon_user;

        if(count($this->coupons) > 0)
        {
            if(!isset($meta_data['checkout_settings']['display_coupons']))
                $this->alert=true;
            if(isset($meta_data['checkout_settings']['display_coupons']))
                {
                    if($meta_data['checkout_settings']['display_coupons']=='off')
                    {
                        $this->alert=true;
                    }
                }

        }else
            $this->alert=false;

    }

    //create coupon
    public function createCoupon()
    {
        $this->validate();
        // Execution doesn't reach here if validation fails.

        if($this->unit=='percentage')
        {
            if($this->discount > 100)
            {
                $validator = Validator::make([], []); // Empty data and rules fields
                $validator->errors()->add('discount', 'The value must be less than 100.');
                throw new ValidationException($validator);
                return;
            }
        }

        $this->setData();

        $to = \Carbon\Carbon::createFromFormat('Y-m-d', $this->expires_in);
        $from = \Carbon\Carbon::createFromFormat('Y-m-d', $this->start_date);
        $diff_in_days = $to->diffInDays($from);

        Promocodes::create($this->amount, $this->discount, $this->data , $diff_in_days , $this->quantity,true);

        $this->reset();
        $this->mount();
        $this->emit('close_coupon_modal');

        return $this->alert('success', __('Coupon Created Successfully!'));
    }

    public function disableCoupon($id)
    {
        $this->disable_coupon_id=$id;
        $this->alert('warning', 'Are You Sure ? You want to disable this coupon code', [
            'position' =>  'center',
            'toast' => false,
            'timer' =>  '100000',
            'confirmButtonText' =>  'Yes',
            'cancelButtonText' =>  'No',
            'showCancelButton' =>  true,
            'showConfirmButton' =>  true,
            'onConfirmed' => 'confirmed',
        ]);
    }

    public function confirmed()
    {
        $coupon= Promocode::find($this->disable_coupon_id);
        $coupon->disable=1;
        $coupon->save();
        $this->mount();
        return $this->alert('success', __('Disabled Coupon!'));
    }

    public function enableCoupon($id)
    {
       $coupon= Promocode::find($id);
       $coupon->disable=0;
       $coupon->save();
       $this->mount();
       return $this->alert('success', __('Enabled Coupon!'));
    }

    public function setData()
    {
        $this->data['description']=$this->description;
        $this->data['usage_limit']=$this->usage_limit;
        $this->data['min_cart_value']=$this->min_cart_value;
        $this->data['team_id']=auth()->user()->team_id;
        $this->data['start_date']=$this->start_date;
        $this->data['end_date']=$this->expires_in;
        $this->data['unit']=$this->unit;
    }

    public function editCoupon($id)
    {
        $this->edit=true;
        $this->coupon_id=$id;
        $coupon=Promocode::find($this->coupon_id);
        $this->discount=$coupon->reward;
        $this->unit=$coupon->data['unit'];
        $this->description=$coupon->data['description'];
        $this->start_date=$coupon->data['start_date'];
        $this->expires_in = $coupon->data['end_date'];
        $this->min_cart_value=$coupon->data['min_cart_value'];
        $this->usage_limit=$coupon->data['usage_limit'];
        $this->emit('open_coupon_edit_modal');
    }

    public function updateCoupon()
    {
        $this->validate();
        // Execution doesn't reach here if validation fails.

        if($this->unit=='percentage')
        {
            if($this->discount > 100)
            {
                $validator = Validator::make([], []); // Empty data and rules fields
                $validator->errors()->add('discount', 'The value must be less than 100.');
                throw new ValidationException($validator);
                return;
            }
        }

        $this->setData();

        $coupon=Promocode::find($this->coupon_id);
        $coupon->reward=$this->discount;
        $coupon->data=$this->data ;
        $coupon->expires_at=  $this->expires_in;


        $coupon->save();

        $this->reset();
        $this->mount();
        $this->edit=false;
        $this->emit('close_coupon_modal');
        return $this->alert('success', __('Coupon Updated Successfully!'));
    }

    public function closeCouponEditModal()
    {
        if($this->edit==true)
        {
            $this->edit=false;
            $this->reset();
            $this->mount();
        }
    }

    public function render()
    {
        return view('livewire.admin.coupon.coupon-list');
    }
}
