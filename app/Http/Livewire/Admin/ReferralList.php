<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\User;

class ReferralList extends Component
{
    
    public function mount($link, $referred,$plan_id)
    {       
       $this->link = $link;
       $this->referred = $referred;
       $this->plan_id = $plan_id;   
    }
    public function render()
    {       
        $link = $this->link; 
        $referred = $this->referred;
        $plan_id = $this->plan_id;  
        return view('livewire.admin.referral-list',compact('link','referred','plan_id'));
    }
   
}
