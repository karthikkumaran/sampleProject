<?php

namespace App\Http\Livewire\Admin\Customer;

use App\ordersCustomer;
use Livewire\Component;
use App\orders;


class CustomerOrderList extends Component
{

    public $customer_info_text = "";
    protected $orders; //list of orders
    public $customer; //customer object 
    public $orders_count; // count of total orders

    public $total_amount = 0;

    protected $listeners = ['SearchOrder'];


    public $customer_id = null;
    public $email, $mobile;

    public function mount($id)
    {

        $this->customer = ordersCustomer::find($id);

        if ($this->customer->customer_id != '') {
            //registered customer
            $this->customer_id = $this->customer->customer_id;
            
            $this->orders = ordersCustomer::join('orders',function($join){
                $data= $join->on('orders_customers.order_id','=','orders.id');
                $data->where('customer_id', $this->customer_id);
            })->whereNotNull('orders.transaction_id')->select('orders_customers.*')->orderBy('order_id','desc')->paginate(10);
            // $this->orders = ordersCustomer::select('*')->where('customer_id', $this->customer_id)->orderBy('order_id','desc')->paginate(10);
            
        } else {
            //guest customer
            $this->email = $this->customer->email;
            $this->mobile = $this->customer->mobileno;

            $this->orders = ordersCustomer::join('orders',function($join){
                $data= $join->on('orders_customers.order_id','=','orders.id');
                $data->where([['email', $this->email], ['mobileno', $this->mobile], ['customer_id', '=', '']]);
            })->whereNotNull('orders.transaction_id')->select('orders_customers.*')->orderBy('order_id','desc')->paginate(10);

            // $this->orders = ordersCustomer::where([['email', $this->email], ['mobileno', $this->mobile], ['customer_id', '=', '']])->orderBy('order_id','desc')->paginate(10);
        }

        $this->orders_count=$this->orders->total();
    }

    // search order function
    public function SearchOrder($q)
    {
        if($q=='')
            return $this->mount($this->customer->id);

        if ($this->customer_id == null) {
            $this->orders = ordersCustomer::join('orders',function($join){
                $data= $join->on('orders_customers.order_id','=','orders.id');
                $data->where('customer_id', $this->customer_id);
            })->whereNotNull('orders.transaction_id')->select('orders_customers.*')->orderBy('order_id','desc')->paginate(10);
        } else {
            $this->orders = ordersCustomer::join('orders',function($join){
                $data= $join->on('orders_customers.order_id','=','orders.id');
                $data->where([['email', $this->email], ['mobileno', $this->mobile], ['customer_id', '=', '']]);
            })->whereNotNull('orders.transaction_id')->select('orders_customers.*')->orderBy('order_id','desc')->paginate(10);
        }

        $order = orders::select('*')->where('team_id', auth()->user()->team_id)->where('order_ref_id', $q)->first();
        if (empty($order)) {
            return $this->orders = $this->orders->paginate(10);
        }

        $this->orders = $this->orders->where('order_id', $order->id)->paginate(10);

        if ($this->orders->total()==0) {
            $this->customer_info_text = "No order id match your search";
            return $this->alert('info', __('No orders found'));
        }
    }

    public function render()
    {
        return view('livewire.admin.customer.customer-order-list', ['orders' => $this->orders]);
    }
}
