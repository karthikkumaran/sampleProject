<?php

namespace App\Http\Livewire\Admin\Customer;

use App\Customer;
use App\orders;
use App\ordersCustomer;
use Livewire\Component;
use App\UserInfo;
use App\User;
use Illuminate\Support\Facades\DB;


class CustomerList extends Component
{
    // use WithPagination;

    protected $customers; // customers list object
    public $registered_customers_count;
    public $guest_customers_count;


    public $currency;
    public $customer_info_text = "There are no customers yet.";
    public $register_tab; // highlight registered customers tab
    public $guest_tab; // highlight guest customers tab 


    public $customer; //induvidual customer object

    //search event 
    protected $listeners = ['SearchCustomer'];

    public function mount()
    {
        //setting the currency
        $meta_data = UserInfo::where('user_id', auth()->user()->id)->first()->meta_data;
        $user_meta_data = json_decode($meta_data, true);

        if (empty($user_meta_data['settings']['currency'])) {
            $this->currency = "₹";
        } else {
            if ($user_meta_data['settings']['currency_selection'] == "currency_symbol")
                $this->currency = User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
            else
                $this->currency = $user_meta_data['settings']['currency'];
        }

        //setting the registered customers list
        $this->RegisteredCustomersList();

        //setting the count of guest customers list
        $this->guest_customers_count = ordersCustomer::select('*')
            ->where('customer_id', '=', '')->groupBy('email', 'mobileno')->get()->count();
    }


    //list of registerd customers
    public function RegisteredCustomersList()
    {
        $this->customer_info_text=null;

        $this->guest_tab = false;
        $this->register_tab = true;

        $customers = DB::table('orders')
            ->join('orders_customers', 'orders.id', '=', 'orders_customers.order_id')
            ->join('transactions', 'transactions.id', '=', 'orders.transaction_id')
            ->select('orders_customers.*', DB::raw('count(*) as total_orders'),DB::raw('SUM(transactions.amount) As total_amount'))
            ->whereNotNull('orders.transaction_id')
            ->where([['orders_customers.team_id', auth()->user()->team_id], ['orders_customers.customer_id', '!=', '']])
            ->groupBy('customer_id')
            ->orderBy('orders.order_ref_id','desc')
            ->paginate(10);

        // $customers = ordersCustomer::join('orders',function($join){
        //     $data= $join->on('orders_customers.order_id','=','orders.id');
        //     })->select('orders_customers.*', DB::raw('count(*) as total_orders'))
        //     ->whereNotNull('orders.transaction_id')
        //     ->where([['orders_customers.team_id', auth()->user()->team_id], ['orders_customers.customer_id', '!=', '']])
        //     ->groupBy('customer_id')
        //     ->selectRaw("sum(json_unquote(json_extract(`meta_data`, '$." . "product_total_price" . "'))) as total_amount ")
        //     ->paginate(10);
        
        // $customers = ordersCustomer::select('*', DB::raw('count(*) as total'))
        //     ->where([['team_id', auth()->user()->team_id], ['customer_id', '!=', '']])
        //     ->groupBy('customer_id')
        //     ->selectRaw("sum(json_unquote(json_extract(`meta_data`, '$." . "product_total_price" . "'))) as total_amount ")
        //     ->paginate(10);

        $this->customers = $customers;
        $this->registered_customers_count = $customers->total();

        if($customers->total()==0)
            $this->customer_info_text = "There are no customers yet.";

    }

    //list of guest customers
    public function GuestCustomersList()
    {
        $this->customer_info_text=null;

        $this->register_tab = false;
        $this->guest_tab = true;
        
        $customers = DB::table('orders')
            ->join('orders_customers', 'orders.id', '=', 'orders_customers.order_id')
            ->join('transactions', 'transactions.id', '=', 'orders.transaction_id')
            ->select('orders_customers.*', DB::raw('count(*) as total_orders'),DB::raw('SUM(transactions.amount) As total_amount'))
            ->whereNotNull('orders.transaction_id')
            ->where([['orders_customers.team_id', auth()->user()->team_id], ['orders_customers.customer_id', '=', '']])
            ->groupBy('email', 'mobileno')
            ->orderBy('orders.order_ref_id','desc')
            ->paginate(10);
        // $customers = ordersCustomer::join('orders',function($join){
        //     $data= $join->on('orders_customers.order_id','=','orders.id');
        //     })->select('orders_customers.*', DB::raw('count(*) as total_orders'))
        //     ->whereNotNull('orders.transaction_id')
        //     ->where([['orders_customers.team_id', auth()->user()->team_id], ['orders_customers.customer_id', '=', '']])
        //     ->groupBy('email', 'mobileno')
        //     ->selectRaw("sum(json_unquote(json_extract(`meta_data`, '$." . "product_total_price" . "'))) as total_amount ")
        //     ->paginate(10);

        // $customers = ordersCustomer::select('*', DB::raw('count(*) as total'))
        //     ->where([['team_id', auth()->user()->team_id], ['customer_id', '=', '']])
        //     ->groupBy('email', 'mobileno')
        //     ->selectRaw("sum(json_unquote(json_extract(`meta_data`, '$." . "product_total_price" . "'))) as total_amount ")
        //     ->paginate(10);

        $this->customers = $customers;

        if($customers->total()==0)
            $this->customer_info_text = "There are no customers yet.";
    }



    //search customer function
    public function SearchCustomer($q)
    {
        $this->customer_info_text='';
        // $customers = ordersCustomer::join('orders',function($join){
        //     $data= $join->on('orders_customers.order_id','=','orders.id');
        //     })->select('orders_customers.*', DB::raw('count(*) as total_orders'))
        //     ->whereNotNull('orders.transaction_id');

        // $customers = ordersCustomer::select('*', DB::raw('count(*) as total'));

        $customers = DB::table('orders')
            ->join('orders_customers', 'orders.id', '=', 'orders_customers.order_id')
            ->join('transactions', 'transactions.id', '=', 'orders.transaction_id')
            ->select('orders_customers.*', DB::raw('count(*) as total_orders'),DB::raw('SUM(transactions.amount) As total_amount'))
            ->where([['orders_customers.team_id', auth()->user()->team_id], ['orders_customers.customer_id', '=', '']])
            ->whereNotNull('orders.transaction_id');

        if ($this->register_tab) {
            $customers = $customers->where('orders_customers.customer_id', '!=', "")
                ->groupBy('customer_id');
        } else {
            $customers = $customers->where('orders_customers.customer_id', '=', "")
                ->groupBy('email', 'mobileno');
        }

        $customers = $customers->where(function ($query) use ($q) {
            $query->orwhere('fname', 'Like', '%' . $q . '%')
                ->orWhere('lname', 'LIKE', '%' . $q . '%')
                ->orWhere('email', 'LIKE', '%' . $q . '%')
                ->orWhere('mobileno', 'LIKE', '%' . $q . '%');
        })
            ->paginate(10);

        $this->customers = $customers;

        //if customers are empty then change the text
        if($customers->total()==0){
            $this->customer_info_text = "No customers match  your search";
            $this->alert('info', __('No customers found'));  
        }
    }

   

    public function render()
    {
        return view('livewire.admin.customer.customer-list', ['customers' => $this->customers]);
    }
}
