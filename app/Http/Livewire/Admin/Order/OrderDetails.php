<?php

namespace App\Http\Livewire\Admin\Order;

use App\ordersCustomer;
use App\Models\OrderStatus;
use App\Models\OrderStatusRemarks;
use Livewire\Component;
use App\orders;
use App\user;
use App\UserInfo;
use App\Models\awbtracking;
use Auth;

class OrderDetails extends Component
{
    public $customer;
    public $transaction;
    public $products;
    public $orderstatus;
    public $currentstatus;
    public $orderstatusremark;
    public $user_meta_data;
    public $trackingdata;
    // public $order_ID;

    public $order_id;

    protected $listeners = ['updateOrderStatus','updateStatusRemarks'];


    public function mount($id)
    {
        $this->customer = ordersCustomer::find($id);
        $this->transaction = $this->customer->order->transactions;
        $this->products = $this->customer->orderProducts;
        // print_r($this->customer);
        // die;
        $this->user_meta_data = UserInfo::where("user_id", auth()->user()->id)->first();
        $this->user_meta_data = json_decode($this->user_meta_data->meta_data, true);

        $this->orderstatus = OrderStatus::where('order_id', $this->customer->order_id)->get();
        $this->currentstatus = OrderStatus::where('order_id', $this->customer->order_id)->orderBy('status','desc')->first();
        $this->orderstatusremark = OrderStatusRemarks::where('order_id',$this->customer->order_id)->get();
        $this->trackingdata = awbtracking::where('shippingorderid',$this->customer->order->order_ref_id)->get();

    }


    public function updateOrderStatus($status,$orderId,$remarks,$showremarks)
    {
        // dd($showremarks);
        //$change = $this->orderstatus->update(['status'=>$status,'changed_by'=>auth()->user()->id]);
        $order=$this->customer->order->update(['status' => $status]);
        if($order){
            $orderstatus=OrderStatus::create(array(
                'order_id' => $orderId,
                'status'=> $status,
                'changed_by'=> auth()->user()->id
            ));
            if(!empty($remarks)){
              $orderStatusRemarks=  OrderStatusRemarks::create(array(
                  'order_id' => $orderId,
                  'status' => $status,
                  'remarks' => $remarks,
                  'showremarks' => $showremarks
              ));
            }
            // return $this->alert('success', __('Order status updated successfully!'));
            $this->dispatchBrowserEvent('alert',
            ['type' => 'success',  'message' => 'Order status updated successfully!']);
            return back();
        }
        $this->dispatchBrowserEvent('alert',
        ['type' => 'error',  'message' => 'Something is Wrong!']);
        return back();
    }

    public function updateStatusRemarks($status,$orderId,$remarks,$showremarks){
        if(!empty($remarks)){
            $orderStatusRemarks = OrderStatusRemarks::create(array(
                'order_id' => $orderId,
                'status' => $status,
                'remarks' => $remarks,
                'showremarks' => $showremarks
            ));
            // return  $this->alert('success', __('Status Remarks updated successfully!'));
            $this->dispatchBrowserEvent('alert',
            ['type' => 'success',  'message' => 'Status Remarks updated successfully!']);
            return back();
          }
    }

    public function render()
    {
        $this->orderstatusremark = OrderStatusRemarks::where('order_id',$this->customer->order_id)->get();
        $this->orderstatus = OrderStatus::where('order_id', $this->customer->order_id)->get();
        $this->currentstatus = OrderStatus::where('order_id', $this->customer->order_id)->orderBy('status','desc')->first();
        return view('livewire.admin.order.order-details');
    }
}
