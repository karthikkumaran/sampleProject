<?php
// Own model
namespace App\LaravelSubscriptions;

use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlanSubscribedFeatures extends Model
{
    use SoftDeletes;

    public $table = 'plan_subscribed_features';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'user_id',
        'subscription_id',
        'plan_id',
        'feature_id',
        'value',
        'add_on_value',
        'is_add_on',
        'resettable_period',
        'resettable_interval',
        'sort_order',
        'is_approved',
        'approved_by',
        'created_by',
    ];

    public function getFeatureDetails(): ?PlanSubscriptionFeatures
    {
        $feature = PlanSubscriptionFeatures::where('id', $this->feature_id)->first();
        return $feature;
    }
}
