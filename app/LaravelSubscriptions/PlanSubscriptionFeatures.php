<?php
// Own model
namespace App\LaravelSubscriptions;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\SlugOptions;
use Rinvex\Support\Traits\HasSlug;
use Rinvex\Support\Traits\ValidatingTrait;

class PlanSubscriptionFeatures extends Model
{
    use SoftDeletes;
    use HasSlug;
    use ValidatingTrait;

    public $table = 'plan_subscription_features';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'slug',
        'name',
        'description',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The default rules that the model will validate against.
     *
     * @var array
     */
    protected $rules = [];

    /**
     * Whether the model should throw a
     * ValidationException if it fails validation.
     *
     * @var bool
     */
    protected $throwValidationExceptions = true;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setTable('plan_subscription_features');
        $this->setRules([
            'slug' => 'required|alpha_dash|max:150|unique:plan_subscription_features,slug',
            'name' => 'required|string|strip_tags|max:150',
            'description' => 'nullable|string|max:10000',
        ]);
    }

    /**
     * Get the options for generating the slug.
     *
     * @return \Spatie\Sluggable\SlugOptions
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
                          ->doNotGenerateSlugsOnUpdate()
                          ->generateSlugsFrom('name')
                          ->saveSlugsTo('slug');
    }
}
