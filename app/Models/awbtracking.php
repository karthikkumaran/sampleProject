<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class awbtracking extends Model
{
    use HasFactory;
    public $table = 'awbtrackings';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'awb_no',
        'carrier_name',
        'awb_id',
        'child_awb_id',
        'shippingorderid',
        'return',
        'eventstatus', 
        'remark',
        'location',
        'eventtime',
    ];

}
