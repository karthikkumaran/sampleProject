<?php

namespace App\Models\Statistics;

use Illuminate\Database\Eloquent\Model;

class UniqueVistor extends Model
{
    public $timestamps = false;

    public $table = 'analytics_visitor';

    protected $fillable = [
        'ip_address',
        'latitude',
        'longitude',
        'country_name',
        'client_ips',
        'is_from_trusted_proxy',
        'state_name',
        'postal_code',
        'timezone',
        'city',
        'team_id',
    ];


    protected $casts = [
        'ip_address' => 'string',
        'latitude' => 'string',
        'longitude' => 'string',
        'country_name' => 'string',
        'client_ips' => 'json',
        'is_from_trusted_proxy' => 'boolean',
        'state_name' => 'string',
        'postal_code' => 'string',
        'timezone' => 'string',
        'city' => 'string',
    ];


    protected $rules = [
        'ip_address' => 'required|string',
        'latitude' => 'required|string',
        'longitude' => 'required|string',
        'country_name' => 'required|string',
        'client_ips' => 'nullable|array',
        'is_from_trusted_proxy' => 'sometimes|boolean',
        'state_name' => 'nullable|string',
        'postal_code' => 'nullable|string',
        'timezone' => 'nullable|string|max:150|timezone',
        'city' => 'nullable|string',
    ];



    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
            // dd($model);
        });
    }

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }
}
