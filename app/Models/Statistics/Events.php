<?php

namespace App\Models\Statistics;

use App\Http\Controllers\Traits\StatisticsTrait;
use Illuminate\Database\Eloquent\Model;

 
class Events extends Model
{
    use StatisticsTrait;
    public $table = 'analytics_events';
    public $timestamps = false;


    protected $fillable = [
        'name',
        'event_name',
        'product_id',
        'campaign_id',
        'count',
        'team_id',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
        });
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function campaign()
    {
        return $this->belongsTo(Campaign::class, 'campaign_id');
    }

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }
}
