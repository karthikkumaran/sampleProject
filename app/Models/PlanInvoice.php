<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlanInvoice extends Model
{
    public $table = 'plan_invoices';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'subscription_payment_id',
        'user_id',
        'plan_name',
        'plan_start',
        'plan_expiry',
        'inv_date',
        'terms',
        'due_date',
        'inv_num'
    ];
}
