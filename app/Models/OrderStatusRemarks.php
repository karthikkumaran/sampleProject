<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderStatusRemarks extends Model
{
    use HasFactory;

    public $table = 'order_status_remarks';

    const STATUS = [
        '0' => 'Order Recieved',
        '1' =>  'Pending',
        '2' =>  'Accepted',
        '3' =>  'Shipped',
        '4' =>  'Delivered',
        '5' =>  'Rejected',
        '6' =>  'Cancelled',
        '7' =>  'Failed',

    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'order_id',
        'status',
        'remarks',
        'showremarks',
        'changed_by',        
        'created_at',
        'updated_at'
        
    ];

    public function order()
    {
        return $this->belongsTo(orders::class, 'order_id');
    }
}
