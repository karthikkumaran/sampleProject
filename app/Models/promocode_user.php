<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class promocode_user extends Model
{
    use HasFactory;

    public $table = 'promocode_user';

    public $timestamps = false;

    protected $fillable = [
        'promocode_id',
        'customer_id',
        'used_at',
    ];

}
