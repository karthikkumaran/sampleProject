<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Otp_Verification extends Model
{
    public $table = 'otp__verifications';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'user_id',
        'otp_reference_id',
        'type',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
