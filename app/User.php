<?php

namespace App;

use App\Notifications\VerifyUserNotification;
use Carbon\Carbon;
use Hash;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;
use Lab404\Impersonate\Models\Impersonate;
use App\LaravelSubscriptions\Traits\HasSubscriptions;
use App\LaravelSubscriptions\Plan;
use App\LaravelSubscriptions\PlanFeature;
use App\LaravelSubscriptions\PlanSubscription;
use App\LaravelSubscriptions\PlanSubscriptionUsage;
use App\LaravelSubscriptions\PlanSubscriptionFeatures;
use App\LaravelSubscriptions\PlanSubscribedFeatures;
use Rinvex\Subscriptions\Services\Period;
use Illuminate\Support\Facades\Schema;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;
use App\Traits\UserReferral;


class User extends Authenticatable
{
    use SoftDeletes, Notifiable, HasApiTokens, Impersonate, HasSubscriptions;
    use UserReferral;

    public $table = 'users';

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'verified_at',
        'email_verified_at',
    ];

    const STATUS_SELECT = [
        '1' => 'Active',
        '0' => 'Deactive',
        '2' => 'Blocked',
    ];

    const CURRENCY_SYMBOLS = [
        'AED' => '&#1583;.&#1573;', // ?
        'AFN' => '&#65;&#102;',
        'ALL' => '&#76;&#101;&#107;',
        'AMD' => '&#1380;',
        'ANG' => '&#402;',
        'AOA' => '&#75;&#122;', // ?
        'ARS' => '&#36;',
        'AUD' => '&#36;',
        'AWG' => '&#402;',
        'AZN' => '&#8380;',
        'BAM' => '&#75;&#77;',
        'BBD' => '&#36;',
        'BDT' => '&#2547;', // ?
        'BGN' => '&#1083;&#1074;',
        'BHD' => '.&#1583;.&#1576;', // ?
        'BIF' => '&#70;&#66;&#117;', // ?
        'BMD' => '&#36;',
        'BND' => '&#36;',
        'BOB' => '&#36;&#98;',
        'BRL' => '&#82;&#36;',
        'BSD' => '&#36;',
        'BTN' => '&#78;&#117;&#46;', // ?
        'BWP' => '&#80;',
        'BYR' => '&#112;&#46;',
        'BZD' => '&#66;&#90;&#36;',
        'CAD' => '&#36;',
        'CDF' => '&#70;&#67;',
        'CHF' => '&#67;&#72;&#70;',
        'CLF' => '&#85;&#70;', // ?
        'CLP' => '&#36;',
        'CNY' => '&#165;',
        'COP' => '&#36;',
        'CRC' => '&#8353;',
        'CUP' => '&#8396;',
        'CVE' => '&#36;', // ?
        'CZK' => '&#75;&#269;',
        'DJF' => '&#70;&#100;&#106;', // ?
        'DKK' => '&#107;&#114;',
        'DOP' => '&#82;&#68;&#36;',
        'DZD' => '&#1583;&#1580;', // ?
        'EGP' => 'E&#163;',
        'ETB' => '&#66;&#114;',
        'EUR' => '&#8364;',
        'FJD' => '&#36;',
        'FKP' => '&#163;',
        'GBP' => '&#163;',
        'GEL' => '&#4314;', // ?
        'GHS' => '&#162;',
        'GIP' => '&#163;',
        'GMD' => '&#68;', // ?
        'GNF' => '&#70;&#71;', // ?
        'GTQ' => '&#81;',
        'GYD' => '&#36;',
        'HKD' => '&#36;',
        'HNL' => '&#76;',
        'HRK' => '&#107;&#110;',
        'HTG' => '&#71;', // ?
        'HUF' => '&#70;&#116;',
        'IDR' => '&#82;&#112;',
        'ILS' => '&#8362;',
        'INR' => '&#8377;',
        'IQD' => '&#1593;.&#1583;', // ?
        'IRR' => '&#65020;',
        'ISK' => '&#107;&#114;',
        'JEP' => '&#163;',
        'JMD' => '&#74;&#36;',
        'JOD' => '&#74;&#68;', // ?
        'JPY' => '&#165;',
        'KES' => '&#75;&#83;&#104;', // ?
        'KGS' => '&#1083;&#1074;',
        'KHR' => '&#6107;',
        'KMF' => '&#67;&#70;', // ?
        'KPW' => '&#8361;',
        'KRW' => '&#8361;',
        'KWD' => '&#1583;.&#1603;', // ?
        'KYD' => '&#36;',
        'KZT' => '&#8376;',
        'LAK' => '&#8365;',
        'LBP' => '&#163;',
        'LKR' => '&#8360;',
        'LRD' => '&#36;',
        'LSL' => '&#76;', // ?
        'LTL' => '&#76;&#116;',
        'LVL' => '&#76;&#115;',
        'LYD' => '&#1604;.&#1583;', // ?
        'MAD' => '&#1583;.&#1605;.', //?
        'MDL' => '&#76;',
        'MGA' => '&#65;&#114;', // ?
        'MKD' => '&#1076;&#1077;&#1085;',
        'MMK' => '&#75;',
        'MNT' => '&#8366;',
        'MOP' => '&#77;&#79;&#80;&#36;', // ?
        'MRO' => '&#85;&#77;', // ?
        'MUR' => '&#8360;', // ?
        'MVR' => '.&#1923;', // ?
        'MWK' => '&#77;&#75;',
        'MXN' => '&#36;',
        'MYR' => '&#82;&#77;',
        'MZN' => '&#77;&#84;',
        'NAD' => '&#36;',
        'NGN' => '&#8358;',
        'NIO' => '&#67;&#36;',
        'NOK' => '&#107;&#114;',
        'NPR' => '&#8360;',
        'NZD' => '&#36;',
        'OMR' => '&#65020;',
        'PAB' => '&#66;&#47;&#46;',
        'PEN' => '&#83;&#47;&#46;',
        'PGK' => '&#75;', // ?
        'PHP' => '&#8369;',
        'PKR' => '&#8360;',
        'PLN' => '&#122;&#322;',
        'PYG' => '&#71;&#115;',
        'QAR' => '&#65020;',
        'RON' => '&#108;&#101;&#105;',
        'RSD' => '&#1044;&#1080;&#1085;&#46;',
        'RUB' => '&#8381;',
        'RWF' => '&#1585;.&#1587;',
        'SAR' => '&#65020;',
        'SBD' => '&#36;',
        'SCR' => '&#8360;',
        'SDG' => '&#163;', // ?
        'SEK' => '&#107;&#114;',
        'SGD' => '&#36;',
        'SHP' => '&#163;',
        'SLL' => '&#76;&#101;', // ?
        'SOS' => '&#83;',
        'SRD' => '&#36;',
        'STD' => '&#68;&#98;', // ?
        'SVC' => '&#36;',
        'SYP' => '&#163;',
        'SZL' => '&#76;', // ?
        'THB' => '&#3647;',
        'TJS' => '&#84;&#74;&#83;', // ? TJS (guess)
        'TMT' => '&#109;',
        'TND' => '&#1583;.&#1578;',
        'TOP' => '&#84;&#36;',
        'TRY' => '&#8356;', // New Turkey Lira (old symbol used)
        'TTD' => '&#36;',
        'TWD' => '&#78;&#84;&#36;',
        'TZS' => '&#84;&#83;&#104;',
        'UAH' => '&#8372;',
        'UGX' => '&#85;&#83;&#104;',
        'USD' => '&#36;',
        'UYU' => '&#36;&#85;',
        'UZS' => '&#1083;&#1074;',
        'VEF' => '&#66;&#115;',
        'VND' => '&#8363;',
        'VUV' => '&#86;&#84;',
        'WST' => '&#87;&#83;&#36;',
        'XAF' => '&#70;&#67;&#70;&#65;',
        'XCD' => '&#36;',
        'XDR' => '&#83;&#68;&#82;',
        'XOF' => '&#70;&#67;&#70;&#65;',
        'XPF' => '&#70;',
        'YER' => '&#65020;',
        'ZAR' => '&#82;',
        'ZMK' => '&#90;&#75;',
        'ZWL' => '&#90;&#36;',
    ];


    protected $fillable = [
        'name',
        'mobile',
        'email',
        'team_id',
        'password',
        'approved',
        'is_on_trial',
        'subscription_status',
        'verified',
        'status',
        'created_at',
        'updated_at',
        'deleted_at',
        'verified_at',
        'remember_token',
        'email_verified_at',
        'verification_token',
        'device_token',
        'provider',
        'provider_id',
        'referred_by',
        'affiliate_id'
    ];

    public function getIsAdminAttribute()
    {
        return $this->roles()->where('id', 1)->exists();
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        self::created(function (User $user) {
            if (auth()->check()) {
                $user->verified    = 1;
                $user->verified_at = Carbon::now()->format(config('panel.date_format') . ' ' . config('panel.time_format'));
                $user->save();
            } elseif (!$user->verification_token) {
                $token     = Str::random(64);
                $usedToken = User::where('verification_token', $token)->first();

                while ($usedToken) {
                    $token     = Str::random(64);
                    $usedToken = User::where('verification_token', $token)->first();
                }

                $user->verification_token = $token;
                $user->save();

                $registrationRole = config('panel.registration_default_role');

                if (!$user->roles()->get()->contains($registrationRole)) {
                    $user->roles()->attach($registrationRole);
                }

                if(empty($user->provider)){
                    $user->notify(new VerifyUserNotification($user));
                }
            }
        });
    }

    public function usersCompanyInfos()
    {
        return $this->hasMany(CompanyInfo::class, 'user_id', 'id');
    }

    public function productCampaign()
    {
        return $this->hasMany(Campaign::class, 'user_id', 'id')->where('type',3);
    }

    public function userInfo()
    {
        return $this->hasOne(UserInfo::class, 'user_id', 'id');
    }

    public function userUserAlerts()
    {
        return $this->belongsToMany(UserAlert::class);
    }

    public function getEmailVerifiedAtAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
    }

    public function setEmailVerifiedAtAttribute($value)
    {
        $this->attributes['email_verified_at'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;
    }

    public function setPasswordAttribute($input)
    {
        if ($input) {
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
        }
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    public function getVerifiedAtAttribute($value)
    {
        // return $value ? Carbon::createFromFormat('Y-m-d H-i-s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
        return $value ? Carbon::parse($value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
    }

    public function setVerifiedAtAttribute($value)
    {
        $this->attributes['verified_at'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

    public function getUserInfo()
    {
        $user_info = UserInfo::where("user_id", $this->id)->first();
        return $user_info;
    }

    public function getStoreLink($linkstring,$localpath="true"){
        $shareableLink = ShareableLink::where('uuid', $linkstring)->first();
        $userSettings = UserInfo::where("user_id", $this->id)->first();
        $publiclink = "";
        if (!empty($shareableLink)) {
            if (empty($userSettings->customdomain)) {
                if (empty($userSettings->subdomain)) {
                    $publiclink = url('/');
                } else {
                    $publiclink = "https://" . $userSettings->subdomain . "." . env('SITE_URL');
                }
            } else {
                $publiclink = "https://" . $userSettings->customdomain;
            }
        }
        
        if(empty($userSettings->subdomain) && $localpath == "true"){
            $publiclink .= '/s/' . $linkstring;
        }
        else if(empty($userSettings->subdomain) && $publiclink = ""){
            $publiclink = false;
        }

        return $publiclink;
    }

    public function isFirstTimeUser()
    {
        $user_info = UserInfo::where("user_id", $this->id)->first();
        return empty($user_info->meta_data);
    }

    public function isOnUserTrial()
    {
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $this->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $this->id)->first();
        }
        if (!empty($subscription) && $subscription->slug == "user-trial") {
            $current_time = Carbon::now();
            $trial_period = $current_time->diff($subscription->ends_at);
            return $trial_period;
        } else {
            return false;
        }
    }

    public function isSubscriptionExpired()
    {
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $this->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $this->id)->first();
        }
        if(!empty($subscription)){
           
            $subscription_ended = $this->subscription($subscription->slug)->ended();
        }
        else{
            $subscription_ended = "No Subscription";
        }
        return $subscription_ended;
    }

    public function getSubscriptionDetails()
    {
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $this->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $this->id)->first();
        }
        return $subscription;
    }

    public function getSubscriptionExpiryDuration()
    {
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $this->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $this->id)->first();
        }
        $current_time = Carbon::now();
        $expiry_duration = $current_time->diff($subscription->ends_at);
        return $expiry_duration;
    }

    public function getSubscriptionStartDuration()
    {
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $this->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $this->id)->first();
        }
        $current_time = Carbon::now();
        $start_duration = $current_time->diff($subscription->starts_at);
        return $start_duration;
    }

    public function getSubscriptionTrialDuration()
    {
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $this->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $this->id)->first();
        }
        $current_time = Carbon::now();
        $trial_duration = $current_time->diff($subscription->trial_ends_at);
        return $trial_duration;
    }

    public function getGracePeriod()
    {
        if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
            $subscription = PlanSubscription::where('subscriber_id', $this->id)->first();
        }
        else{
            $subscription = PlanSubscription::where('user_id', $this->id)->first();
        }
        $plan = Plan::find($subscription->plan_id);
        $grace_period_feature = PlanSubscriptionFeatures::where('slug', "grace-period")->first();
        $user_grace_period = PlanSubscribedFeatures::where(['user_id' => $this->id, 'feature_id' => $grace_period_feature->id])->first();
        $now = Carbon::now();

        if ($plan->hasGrace()) {
            $plan_grace_period_extension = new Period($plan->grace_interval, $plan->grace_period, $subscription->ends_at);
            $plan_grace_period_end = $plan_grace_period_extension->getEndDate();

            if (!empty($user_grace_period)) {
                $user_grace_period_extension = new Period($user_grace_period->value, $plan->add_on_value, $plan_grace_period_extension->getEndDate());
                $user_grace_period_end = $user_grace_period_extension->getEndDate();
                $grace_period_duration = $now->diff($user_grace_period_end);
                if ($now > $user_grace_period_end) {
                    return false;
                } else {
                    return $grace_period_duration;
                }
            } else {
                $grace_period_duration = $now->diff($plan_grace_period_end);
                if ($now > $plan_grace_period_end) {
                    return false;
                } else {
                    return $grace_period_duration;
                }
            }
        }
        else{
            if(!empty($user_grace_period)){
                $user_grace_period_extension = new Period($user_grace_period->value, $user_grace_period->add_on_value, $subscription->ends_at);
                $user_grace_period_end = $user_grace_period_extension->getEndDate();
                if ($now > $user_grace_period_end) {
                    return false;
                } else {
                    $grace_period_duration = $now->diff($user_grace_period_end);
                    return $grace_period_duration;
                }
            } else {
                return false;
            }
        }
    }
}
