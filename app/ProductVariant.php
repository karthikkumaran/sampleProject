<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Rinvex\Attributes\Traits\Attributable;
use App\Traits\Auditable;
use App\Traits\MultiTenantModelTrait;

class ProductVariant extends Model implements HasMedia
{
    use HasFactory, SoftDeletes, MultiTenantModelTrait, HasMediaTrait, Auditable, Attributable;

    public $table = 'product_variants';

    protected $appends = [
        'photo',
        'docs',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'product_id',
        'variant_data',
        'name',
        'description',
        'price',
        'sku',
        'discount',
        'stock',
        'out_of_stock',
        'status',
        'downgradeable',
        'url',
        'featured',
        'new',
        'quantity',
        'units',
        'minimum_quantity',
        'team_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'productweight'
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(150)->height(150)->format('png');
    }

    public function arobject()
    {
        return $this->hasMany(Arobject::class, 'product_id', 'id');
    }

    public function categories()
    {
        return $this->belongsToMany(ProductCategory::class);
    }

    public function getPhotoAttribute()
    {
        $files = $this->getMedia('photo');
        $files->each(function ($item) {
            $item->url       = $item->getUrl();
            $item->thumbnail = $item->getUrl('thumb');
            $item->thumbnailFullUrl = $item->getFullUrl('thumb');
            $item->fullUrl= $item->getFullUrl();
        });

        return $files;
    }

    public function tags()
    {
        return $this->belongsToMany(ProductTag::class);
    }

    public function getDocsAttribute()
    {
        $files = $this->getMedia('docs');
        $files->each(function ($item) {
            $item->url= $item->getUrl();
            $item->fullUrl= $item->getFullUrl();
        });

        return $files;
    }

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }
}
