<?php

namespace App;

use App\Traits\Auditable;
use App\Traits\MultiTenantModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

use Sassnowski\LaravelShareableModel\Shareable\Shareable;
use Sassnowski\LaravelShareableModel\Shareable\ShareableInterface;
use Sassnowski\LaravelShareableModel\Shareable\ShareableLink;

class Campaign extends Model implements HasMedia, ShareableInterface
{
    use SoftDeletes, MultiTenantModelTrait, HasMediaTrait, Auditable, Shareable;

    public $table = 'campaigns';

    const IS_START_SELECT = [
        '1' => 'Start',
        '0' => 'Stop',
    ];

    protected $appends = [
        'splashlogo',
        'bannerimg',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const LOADER_TYPE = [
        'border' => 'Border',
        'grow' => 'Grow',
    ];

    const BRAND_TYPE = [
        '1' => 'Store Logo',
        '2' => 'Store Name',
        '3' => 'both',
    ];

    const IS_ACTIVE_SELECT = [
        '1' => 'Active',
        '0' => 'In Active',
    ];

    const TYPE_SELECT = [
        '0' => 'Try Along',
        '1' => 'View Along',
        '2' => 'Insta Try',
        '3' => 'Catalog',
        '4' => 'Catalog Try On',
        '5' => 'MarkAR',
    ];

    protected $fillable = [
        'name',
        'type',
        'user_id',
        'team_id',
        'is_start',
        'is_active',
        'created_at',
        'updated_at',
        'deleted_at',
        'subdomain',
        'customdomain',
        'public_link',
        'preview_link',
        'meta_data',
        'is_private',
        'is_added_home',
        'downgradeable'
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function getSplashlogoAttribute()
    {
        return $this->getMedia('splashlogo')->last();
    }

    public function getBannerimgAttribute()
    {
        // return $this->getMedia('bannerimg')->last();
        $files = $this->getMedia('bannerimg');
        $files->each(function ($item) {
            $item->url       = $item->getUrl();
            $item->thumbnail = $item->getUrl('thumb');
        });

        return $files;
    }

    public function campaignEntitycards()
    {
        return $this->hasMany(Entitycard::class, 'campaign_id', 'id')->orderBy('id', 'desc');
    }
    
    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function public_shareable(){
        return $this->hasOne(ShareableLink::class, 'uuid','public_link');
    }

    public static function boot() {
        parent::boot();
        static::deleting(function($campaign) { // before delete() method call this
             $campaign->campaignEntitycards()->delete();
             // do the rest of the cleanup...
        });
    }
}
