<?php

namespace App;

use App\Http\Controllers\Traits\StatisticsTrait;
use App\Traits\Auditable;
use App\Traits\MultiTenantModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class enquiryProduct extends Model implements HasMedia
{
    use SoftDeletes, MultiTenantModelTrait, HasMediaTrait, Auditable,StatisticsTrait;

    public $table = 'enquiry_product';


    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'team_id',
        'user_id',
        'campaign_id',
        'enquiry_id',
        'product_id',
        'price',
        'qty',
        'sku',
        'name',
        'discount',
        'meta_data',
        'notes',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function enquiry()
    {
        return $this->belongsTo(enquiry::class, 'enquiry_id');
    }

    public function customer()
    {
        return $this->belongsTo(enquiryCustomer::class, 'enquiry_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function campaign()
    {
        return $this->belongsTo(Campaign::class, 'campaign_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }
    //
}
