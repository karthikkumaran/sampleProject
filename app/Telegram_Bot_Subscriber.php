<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Telegram_Bot_Subscriber extends Model
{
    public $table = 'telegram__bot__subscribers';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'user_id',
        'telegram_chat_id',
        'username',
        'first_name',
        'last_name',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
