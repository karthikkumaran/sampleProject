<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class PlanDowngradeableCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'plandowngradeable:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks if there are any plans that are pending to be downgraded.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return $this->call('queue:work', [
            '--queue' => 'downgradeable', // remove this if queue is default
            '--stop-when-empty' => null,
        ]);
    }
}
