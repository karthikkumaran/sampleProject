<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\LaravelSubscriptions\Plan;
use Illuminate\Support\Facades\Schema;
use App\LaravelSubscriptions\PlanSubscription;
use App\LaravelSubscriptions\PlanSubscriptionUsage;
use App\LaravelSubscriptions\PlanSubscribedFeatures;
use Carbon\Carbon;
use App\User;
use App\Product;
use App\Campaign;

class ExistingUsersSubscribeCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'existing_users_susbcribe:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $existing_users = User::all();
        foreach ($existing_users as $user){
            if($user->getIsAdminAttribute() == false && $user->subscription_status != 1){
                $plan = Plan::where('slug', "user-trial")->first();
                $user->newSubscription($plan->name, $plan);
                $user->subscription_status = 1;
                $user->save();
                if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
                    $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
                }
                else{
                    $subscription = PlanSubscription::where('user_id', $user->id)->first();
                }
                $now = Carbon::now();
                $features = $plan->features;
                $subscribed_features = [];
                $subscription_features_usage = [];
                foreach($features as $feature){
                    $subscribed_features[] = [
                        'user_id' => $user->id,
                        'subscription_id' => $subscription->id,
                        'plan_id' => $plan->id,
                        'feature_id' => $feature->feature_id,
                        'value' => $feature->value,
                        'resettable_period' => $feature->resettable_period,
                        'resettable_interval' => $feature->resettable_interval,
                        'is_approved' => 1,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ];
                }
                PlanSubscribedFeatures::insert($subscribed_features);

                $campaigns = Campaign::where('user_id',$user->id)->orderBy('is_start','desc')->orderBy('id', 'desc')->get();
                if($campaigns->count() > 0){
                    $campaign_count = 0;
                    $catalogs_usage_count = 0;
                    $campaign_value = $user->subscription($subscription->slug)->getFeatureValue('catalogs');
                    foreach($campaigns as $campaign){
                        $campaign_count = $campaign_count + 1;
                        if($campaign_count > $campaign_value){
                            $campaign->is_start = 0;
                            $campaign->is_added_home = 0;
                            $campaign->downgradeable = 1;
                            $campaign->save();
                        }
                        else{
                            $catalogs_usage_count = $campaign_count;
                            $campaign->downgradeable = 0;
                            $campaign->save();
                        }
                    }

                    $subscription_features_usage[] = [
                        'subscription_id' => $subscription->id,
                        'feature_id' => 1,
                        'used' => $catalogs_usage_count,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ];
                }
                
                $products = Product::where('team_id',$user->team_id)->get();
                if($products->count() > 0){
                    $product_count = 0;
                    $products_usage_count = 0;
                    $product_value = $user->subscription($subscription->slug)->getFeatureValue('products');
                    foreach($products as $product){
                        $product_count = $product_count + 1;
                        if($product_count > $product_value){
                            $product->downgradeable = 1;
                            $product->save();
                        }
                        else{
                            $products_usage_count = $product_count;
                            $product->downgradeable = 0;
                            $product->save();
                        }
                    }

                    $subscription_features_usage[] = [
                        'subscription_id' => $subscription->id,
                        'feature_id' => 2,
                        'used' => $products_usage_count,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ];
                }

                PlanSubscriptionUsage::insert($subscription_features_usage);
            }
        }
        return true;
    }
}
