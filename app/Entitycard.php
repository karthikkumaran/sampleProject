<?php

namespace App;

use App\Traits\Auditable;
use App\Traits\MultiTenantModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Entitycard extends Model
{
    use SoftDeletes, MultiTenantModelTrait, Auditable;

    public $table = 'entitycards';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $with = ['product','arobject','video'];

    protected $fillable = [
        'team_id',
        'video_id',
        'asset_id',
        'meta_data',
        'product_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'campaign_id',
        'catagory_id',

    ];

    public function campagin()
    {
        return $this->belongsTo(Campaign::class, 'campaign_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function arobject()
    {
        return $this->hasMany(Arobject::class, 'product_id', 'product_id');
    }

    public function video()
    {
        return $this->belongsTo(Video::class, 'video_id');
    }

    public function asset()
    {
        return $this->belongsTo(Asset::class, 'asset_id');
    }

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }
}
