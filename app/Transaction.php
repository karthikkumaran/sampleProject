<?php

namespace App;

use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Illuminate\Database\Eloquent\Model;
use App\Traits\MultiTenantModelTrait;


class Transaction extends Model implements HasMedia
{
    use  MultiTenantModelTrait, HasMediaTrait;

    public $table = 'transactions';

    protected $appends = [
        'photo',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'payment_mode',
        'payment_method',
        'transaction_id',
        'order_id',
        'invoice_id',
        'amount',
        'meta_data',
        'status',
        'team_id',
    ];

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }


    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(150)->height(150)->format('png');
    }


    public function getPhotoAttribute()
    {
        return $this->getMedia('photo')->last();
    }
}
