<?php

namespace App;

use App\Variant;
use App\ProductVariant;
use App\Traits\Auditable;
use App\Traits\MultiTenantModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Rinvex\Attributes\Traits\Attributable;

class Product extends Model implements HasMedia
{
    use SoftDeletes, MultiTenantModelTrait, HasMediaTrait, Auditable, Attributable;

    public $table = 'products';

    protected $appends = [
        'photo',
        'docs',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'sku',
        'url',
        'name',
        'price',
        'team_id',
        'discount',
        'stock',
        'out_of_stock',
        'status',
        'downgradeable',
        'created_at',
        'updated_at',
        'deleted_at',
        'description',
        'units',
        'minimum_quantity',
        'maximum_quantity',
        'length',
        'width',
        'height',
        'slug',
        'seo_title',
        'seo_description',
        'seo_keyword',
        'productweight',
        'meta_data'
    ];

    const LOADER_TYPE = [
        'border' => 'Border',
        'grow' => 'Grow',
    ];

    const BRAND_TYPE = [
        '1' => 'Store Logo',
        '2' => 'Store Name',
    ];

    const IS_ACTIVE_SELECT = [
        '1' => 'Active',
        '0' => 'In Active',
    ];

    const TYPE_SELECT = [
        '0' => 'Try Along',
        '1' => 'View Along',
        '2' => 'Insta Try',
        '3' => 'Catalog',
        '4' => 'Catalog Try On',
        '5' => 'MarkAR',
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(150)->height(150)->format('png');
    }

    public function productEntitycards()
    {
        return $this->hasMany(Entitycard::class, 'product_id', 'id');
    }

    // public function arobjectone()
    // {
    //     return $this->hasOne(Arobject::class, 'product_id', 'id');
    // }

    public function arobject()
    {
        return $this->hasMany(Arobject::class, 'product_id', 'id');
    }

    public function scripts() {
        return $this->hasOne(Scripts::class, 'model_id', 'id')->where('model_type', "App\Product");
    }

    public function categories()
    {
        return $this->belongsToMany(ProductCategory::class);
    }

    public function catalogs()
    {
        $campaigns = $this->hasMany(Entitycard::class, 'product_id', 'id');
        $campaigns->each(function ($item) {
            $campaign = Campaign::where('id', $item->campaign_id)->first();
            if(!empty($campaign)){
                $campaign_name = $campaign->name;
                $item->campaign_name = $campaign_name;
            }
        });

        return $campaigns;
    }

    public function tags()
    {
        return $this->belongsToMany(ProductTag::class);
    }

    public function getPhotoAttribute()
    {
        $files = $this->getMedia('photo');
        $files->each(function ($item) {
            $item->url       = $item->getUrl();
            $item->thumbnail = $item->getUrl('thumb');
            $item->thumbnailFullUrl = $item->getFullUrl('thumb');
            $item->fullUrl= $item->getFullUrl();
        });

        return $files;
    }

    public function getDocsAttribute()
    {
        $files = $this->getMedia('docs');
        $files->each(function ($item) {
            $item->url= $item->getUrl();
            $item->fullUrl= $item->getFullUrl();
        });

        return $files;
    }

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

    public function product_variants()
    {
        return $this->hasMany(ProductVariant::class, 'product_id', 'id');
    }

    public function getVariantName($variant_id) {
        return Variant::where('id', $variant_id)->first();
    }

}
