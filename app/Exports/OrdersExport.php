<?php

namespace App\Exports;

use App\ordersCustomer;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;


class OrdersExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public $datas;
    public function __construct($datas)
    {
        $this->datas = $datas;
    }

    public function collection()
    {
        $datas = $this->datas;
        return $datas;
    }
    public function headings(): array
    {
        return [
            'Unique OrderID',
            'Product total price',
            'product_total_discount',
            'coupon_applied',
            'Total Price',
            'Ordered Items',
            'First Name',
            'Last Name',
            'Address 1',
            'Address 2',
            'City',
            'Pincode',
            'State',
            'Country',
            'Email',
            'Country Code ',
            'Mobile No',
            'Alternative Mobile No',
            'Address type'
            
             
        ];
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class =>function (AfterSheet $event)
            {

                $cellRange = 'A1:R1';
                $event ->sheet->getDelegate()->getStyle($cellRange)->getFont()->setName('Times New Roman');
                $event ->sheet -> getDelegate()->getStyle($cellRange)->getFont()->setSize(16);
                $event ->sheet -> getDelegate()->getStyle($cellRange)->getFont()->setBold(true);
            },
            
        ];
    }
}
