<?php

namespace App;

use App\Traits\Auditable;
use App\Traits\MultiTenantModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Arobject extends Model implements HasMedia
{
    use SoftDeletes, MultiTenantModelTrait, HasMediaTrait, Auditable;

    public $table = 'arobjects';

    protected $appends = [
        'object',
        'object_3d',
        'object_3d_ios',
    ];

    const POSITION_SELECT = [
        'jwl-nck' => 'Necklace',
        'jwl-ear-l' => 'Earring Left',
        'jwl-ear-r' => 'Earring Right',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'team_id',
        'position',
        'ar_type',
        'product_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function getObjectAttribute()
    {
        $files = $this->getMedia('object')->last();
        if(!empty($files))
        {
            $files->url = $files->getUrl();
            $files->fullUrl= $files->getFullUrl();
        }
        return $files;
    } 

    public function getObject3dAttribute()
    {
        $files = $this->getMedia('object_3d')->last();
        if(!empty($files))
        {
            $files->url = $files->getUrl();
        }
        return $files;
    }

    public function getObject3diosAttribute()
    {
        $files = $this->getMedia('object_3d_ios')->last();
        if(!empty($files))
        {
            $files->url = $files->getUrl();
        }
        return $files;
    }
    // public function getObjectAttribute()
    // {
    //     $files = $this->getMedia('object');
    //     $files->each(function ($item) {
    //         $item->url       = $item->getUrl();
    //         $item->thumbnail = $item->getUrl('thumb');
    //     });

    //     return $files;
    // }

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }
}
