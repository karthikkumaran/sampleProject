<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\MultiTenantModelTrait;
use App\Traits\Auditable;


class AttributesMetadata extends Model
{
    use  MultiTenantModelTrait, Auditable;

    public $table = 'attributes_metadata';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'slug',
        'label',
        'type',
        'values',
        'attribute_group_name',
        'attribute_id',
        'team_id',
        'created_at',
        'updated_at',
    ];

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

    // public function attributes()
    // {
    //     return $this->belongsTo(Attribute::class);
    // }
}
