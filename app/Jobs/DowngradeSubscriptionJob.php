<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Schema;
use App\LaravelSubscriptions\Plan;
use App\LaravelSubscriptions\PlanFeature;
use App\LaravelSubscriptions\PlanSubscription;
use App\LaravelSubscriptions\PlanSubscriptionUsage;
use App\LaravelSubscriptions\PlanSubscriptionFeatures;
use App\LaravelSubscriptions\PlanSubscribedFeatures;
use App\Http\Controllers\Traits\CommonFunctionsTrait;
use Carbon\Carbon;
use App\Product;
use App\Campaign;


class DowngradeSubscriptionJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, CommonFunctionsTrait;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;
        $user = $this->data['user'];
        $plan = $this->data['plan'];
        $subscription = $this->data['subscription'];
        if($user->subscription($subscription->slug)->ended()){
            PlanSubscribedFeatures::where('user_id',$user->id)->forceDelete();
            $subscription->changePlan($plan);
            $now = Carbon::now();
            $features = $plan->features;
            $subscribed_features = [];
            foreach($features as $feature){
                $subscribed_features[] = [
                    'user_id' => $user->id,
                    'plan_id' => $plan->id,
                    'subscription_id' => $subscription->id,
                    'feature_id' => $feature->feature_id,
                    'value' => $feature->value,
                    'resettable_period' => $feature->resettable_period,
                    'resettable_interval' => $feature->resettable_interval,
                    'is_approved' => 1,
                    'created_at' => $now,
                    'updated_at' => $now,
                ];
            }
            PlanSubscribedFeatures::insert($subscribed_features);
            if (Schema::hasColumn(config('rinvex.subscriptions.tables.plan_subscriptions'), 'subscriber_id')){
                $subscription = PlanSubscription::where('subscriber_id', $user->id)->first();
            }
            else{
                $subscription = PlanSubscription::where('user_id', $user->id)->first();
            }
            $feature_ids = array();
            foreach ($features as $feature){
                array_push($feature_ids,$feature->feature_id);
                $feature_details = $feature->getFeatureDetails();
                $feature_value = $user->subscription($subscription->slug)->getFeatureValue($feature_details->name);
                $feature_usage = $user->subscription($subscription->slug)->getFeatureUsage($feature_details->name);
                if($feature_usage > $feature_value){
                    $usage = PlanSubscriptionUsage::where(['subscription_id' => $subscription->id,'feature_id' => $feature->feature_id])->first();
                    $usage->used = $feature_value;
                    $usage->save();
                }
                else{

                }
                $campaigns = Campaign::where('user_id',$user->id)->orderBy('is_start','desc')->orderBy('id', 'desc')->get();
                $campaign_count = 0;
                $campaign_value = $user->subscription($subscription->slug)->getFeatureValue('catalogs');
                foreach($campaigns as $campaign){
                    $campaign_count++;
                    if($campaign_count > $campaign_value){
                        $campaign->is_start = 0;
                        $campaign->is_added_home = 0;
                        $campaign->downgradeable = 1;
                        $campaign->save();
                    }
                    else{
                        $campaign->downgradeable = 0;
                        $campaign->save();
                    }
                }
                $products = Product::where('team_id',$user->team_id)->get();
                $product_count = 0;
                $product_value = $user->subscription($subscription->slug)->getFeatureValue('products');
                foreach($products as $product){
                    $product_count++;
                    if($product_count > $product_value){
                        $product->downgradeable = 1;
                        $product->save();
                    }
                    else{
                        $product->downgradeable = 0;
                        $product->save();
                    }
                }
            }
            $usages = PlanSubscriptionUsage::where('subscription_id', $subscription->id)->get();
            foreach ($usages as $usage){
                if(!in_array($usage->feature_id,$feature_ids)){ 
                    $usage->forceDelete();
                }else{

                }
            }
            $this->setSubscriptionCheckSession($user);
        }
        else{
            DowngradeSubscriptionJob::dispatch($this->data);
        }

    }
}
