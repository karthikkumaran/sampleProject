<?php

namespace App\Jobs;

use App\Mail\EnquiryPlaceEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderPlaceEmail;

class EnquiryPlaceEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send(new EnquiryPlaceEmail($this->data));
        // $data = array('name'=>"Virat Gandhi");
        // Mail::send('emails.orderplace', $data, function($message) {
        //     $message->to($this->data['toMail'], $this->data['toName'])->subject
        //        ($this->data['subject']);
        //     $message->from('xyz@gmail.com','Virat Gaaandhi');
        //  });
        if(count(Mail::failures()) > 0){
            return response()->json(['type' => "mail", 'status' => "not sent"], 200);
        }else{
            return response()->json(['type' => "mail", 'status' => "sent"], 200);
        }
    }
}
