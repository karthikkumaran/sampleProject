<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Mail\OrderPlaceEmail;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Mail::to($this->data['tomail'])
        // ->cc($this->data['tocc'])
        // // ->replyTo($this->data['tomail'], $this->data['name'])
        // // ->subject($this->data['subject'])
        // ->send(new OrderPlaceEmail($this->data));

        Mail::send('mail', $data, function($message) {
            $message->to('abc@gmail.com', 'Tutorials Point')->subject
               ('Laravel HTML Testing Mail');
            $message->from('xyz@gmail.com','Virat Gandhi');
         });

        if(count(Mail::failures()) > 0){
            return array('type' => 'email', 'status' => 'not-sent');
        }else{
            return array('type' => 'email', 'status' => 'sent');
        }

    }
}
