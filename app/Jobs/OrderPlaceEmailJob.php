<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderPlaceEmail;

class OrderPlaceEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Mail::to($this->data['toMail'],$this->data['toName'])
        // ->cc($this->data['toCc'])
        // ->replyTo($this->data['tomail'], $this->data['name'])
        // ->subject($this->data['subject'])
        Mail::send(new OrderPlaceEmail($this->data));
        // $data = array('name'=>"Virat Gandhi");
        // Mail::send('emails.orderplace', $data, function($message) {
        //     $message->to($this->data['toMail'], $this->data['toName'])->subject
        //        ($this->data['subject']);
        //     $message->from('xyz@gmail.com','Virat Gaaandhi');
        //  });
        if(count(Mail::failures()) > 0){
            return response()->json(['type' => "mail", 'status' => "not sent"], 200);
        }else{
            return response()->json(['type' => "mail", 'status' => "sent"], 200);
        }
    }
}
