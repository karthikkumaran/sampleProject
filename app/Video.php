<?php

namespace App;

use App\Traits\Auditable;
use App\Traits\MultiTenantModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Rinvex\Attributes\Traits\Attributable;

class Video extends Model implements HasMedia
{
    use SoftDeletes, MultiTenantModelTrait, HasMediaTrait, Auditable, Attributable;

    public $table = 'videos';

    protected $appends = [
        'thumbnail',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'title',
        'team_id',
        'video_url',
        'status',
        'created_at',
        'updated_at',
        'deleted_at',
        'description',
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(150)->height(150);
    }

    public function videoEntitycards()
    {
        return $this->hasOne(Entitycard::class, 'video_id', 'id');
    }

    public function getThumbnailAttribute()
    {
        return $this->getMedia('thumbnail')->last();
        // $files->each(function ($item) {
        //     $item->url       = $item->getUrl();
        //     $item->thumbnail = $item->getUrl('thumb');
        // });

        // return $files;
    }

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }
}
