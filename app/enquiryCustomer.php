<?php

namespace App;

use App\Traits\Auditable;
use App\Traits\MultiTenantModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use App\Http\Controllers\Traits\StatisticsTrait;


class enquiryCustomer extends Model implements HasMedia
{
    use SoftDeletes, MultiTenantModelTrait, HasMediaTrait, Auditable,StatisticsTrait;
    public $table = 'enquiry_customer';
 
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'enquiry_id',
        'team_id',
        'user_id',
        'campaign_id',
        'fname',
        'lname',
        'address1',
        'address2',
        'city',
        'pincode',
        'state',
        'country',
        'email',
        'country_code',
        'mobileno',
        'amobileno',
        'addresstype',
        'meta_data',
        'delivery_note',
        'created_at',
        'updated_at',
        'deleted_at',
        'customer_id'
    ];

    public function enquiry()
    {
        return $this->belongsTo(enquiry::class, 'enquiry_id');
    }

    public function enquiryProducts()
    {
        return $this->hasMany(enquiryProduct::class, 'enquiry_id','enquiry_id');
    }

    public function campaign()
    {
        return $this->belongsTo(Campaign::class, 'campaign_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }
    //
}