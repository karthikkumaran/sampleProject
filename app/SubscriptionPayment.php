<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\MultiTenantModelTrait;

class SubscriptionPayment extends Model
{
    public $table = 'subscription_payments';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'user_id',
        'plan_name',
        'plan_price',
        'plan_start',
        'plan_expiry',
        'payment_mode',
        'payment_method',
        'payment_id',
        'status',
        'meta_data',
        'subscribed_by',
        'comments'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
