<?php

namespace App;

use App\Traits\Auditable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Sassnowski\LaravelShareableModel\Shareable\Shareable;
use Sassnowski\LaravelShareableModel\Shareable\ShareableInterface;

class UserInfo extends Model implements HasMedia, ShareableInterface
{
    use SoftDeletes, Auditable, HasMediaTrait, Shareable;

    public $table = 'user_infos';

    protected $appends = [
        'splashlogo',
        'bannerimg',
        'storelogo',
        'invoice_signature',
        'storetryonlogo',
    ];

    protected $dates = [
        'dob',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    const GENDER_RADIO = [
        '1' => 'Male',
        '0' => 'Female',
        '2' => 'Others',
    ];

    protected $fillable = [
        'dob',
        'name',
        'gender',
        'subdomain',
        'customdomain',
        'public_link',
        'preview_link',
        'meta_data',
        'is_start',
        'user_id',
        'created_at',
        'updated_at',
        'deleted_at',
        'designation',
        'is_private',
    ];

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->width(50)->height(50);
    }

    public function getSplashlogoAttribute()
    {
        return $this->getMedia('splashlogo')->last();
    }

    public function getInvoiceSignatureAttribute()
    {
        return $this->getMedia('invoice_signature')->last();
    }

    public function getStoreLogoAttribute(){
        $files = $this->getMedia('storelogo');
        $files->each(function ($item) {
            $item->url       = $item->getUrl();
            $item->thumbnail = $item->getUrl('thumb');
            $item->thumbnailFullUrl = $item->getFullUrl('thumb');
            $item->fullUrl= $item->getFullUrl(); 
        });

        return $files->last();
    }
    public function getStoreTryonLogoAttribute(){
        $files = $this->getMedia('storetryonlogo');
        $files->each(function ($item) {
            $item->url       = $item->getUrl();
            $item->thumbnail = $item->getUrl('thumb');
            $item->thumbnailFullUrl = $item->getFullUrl('thumb');
            $item->fullUrl= $item->getFullUrl(); 
        });

        return $files->last();
    }

    public function getBannerimgAttribute()
    {
        // return $this->getMedia('bannerimg')->last();
        $files = $this->getMedia('bannerimg');
        $files->each(function ($item) {
            $item->url       = $item->getUrl();
            $item->thumbnail = $item->getUrl('thumb');
            $item->thumbnailFullUrl = $item->getFullUrl('thumb');
            $item->fullUrl= $item->getFullUrl(); 
        });

        return $files;
    }

    public function getDobAttribute($value)
    {
        return $value ? Carbon::parse($value)->format(config('panel.date_format')) : null;
    }

    public function setDobAttribute($value)
    {
        $this->attributes['dob'] = $value ? Carbon::createFromFormat(config('panel.date_format'), $value)->format('Y-m-d') : null;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
