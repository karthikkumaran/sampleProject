<?php

namespace App;

use App\Traits\Auditable;
use App\Traits\MultiTenantModelTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class orders extends Model implements HasMedia
{
    use SoftDeletes, MultiTenantModelTrait, HasMediaTrait, Auditable;

    public $table = 'orders';

    const STATUS = [
        '0' => 'Order Recieved',
        '1' =>  'Pending',
        '2' =>  'Accepted',
        '3' =>  'Shipped',
        '4' =>  'Delivered',
        '5' =>  'Rejected',
        '6' =>  'Cancelled',
        '7' =>  'Failed',

    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'order_ref_id',
        'order_mode',
        'transaction_id',
        'team_id',
        'user_id',
        'campaign_id',
        'status',
        'notes',
        'is_generate_invoice',
        'inv_date',
        'inv_num',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function orderproduct()
    {
        return $this->hasMany(ordersProduct::class, 'order_id','id');
    }

    public function customer()
    {
        return $this->hasMany(ordersCustomer::class, 'order_id','id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function campaign()
    {
        return $this->belongsTo(Campaign::class, 'campaign_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

    public function transactions()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id');
    }
}
