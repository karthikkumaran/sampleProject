<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EnquiryPlaceEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = $this->data;
        $campaign = $this->data['campaign'];
        return $this->to($this->data['toMail'],$this->data['toName'])
        ->bcc($this->data['fromMail'],$this->data['fromName'])
        ->from($this->data['fromMail'],$this->data['fromName'])
        ->subject($this->data['subject'])
        ->view('emails.enquiryplace',compact('data','campaign'));
    }
}
