<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\UserInfo;
use App\Http\Controllers\Admin\CampaignsController;

class WelcomeCustomerMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = $this->data["store_owner"];
        $customer = $this->data["customer"];
        $userSettings = UserInfo::where("user_id", $user->id)->first();
        $storeLink=CampaignsController::publicLink($userSettings->public_link,'store');
        $meta_data = json_decode($userSettings->meta_data, true);
        return $this->to($customer->email)
            ->from($user->email,$user->name)
            ->subject('Welcome to our e-commerce store')
            ->view('emails.customer_welcome',compact('user','customer','storeLink','meta_data'));
    }
}
