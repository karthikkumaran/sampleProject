<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Lang;


class customerPasswordResetEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $url;
    public $store_name;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($from,$url,$name,$store_name)
    {
      $this->from($from,$name);
      $this->url=$url;
      $this->store_name=$store_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(Lang::get('Reset Password Notification'))
        ->markdown('auth.customer.verify',[
            "introLines"=>['You are receiving this email because we received a password reset request for your account.'],
            "actionText"=>'Reset Password',
            "actionUrl"=> $this->url,
            "regards_name" => $this->store_name,
            "outroLines"=>[Lang::get('This password reset link will expire in :count minutes.', ['count' => config('auth.passwords.'.config('auth.defaults.passwords').'.expire')])],

        ]);
        
    }
}
