<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewUserDetailsEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = $this->data["user"];
        $user_meta_data = $this->data["user_meta_data"];
        $user_subscription = $this->data["user_subscription"];
        return $this->to('support@simplisell.co')
            ->subject('New User Registered at '.env('SITE_URL'))
            ->view('emails.new_user_details',compact('user','user_meta_data','user_subscription'));
    }
}
