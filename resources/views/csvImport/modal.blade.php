<div class="modal fade" id="csvImportModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  >
    <div class="modal-dialog modal-dialog-centered" role="document" style="max-height: 80%!important;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Import Product</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class='row'>
                    <div class='col-md-12'>
                        <div class="card border border-primary" style="background-color: rgba(67, 13, 129, 0.1)">
                            <div class="row d-flex justify-content-center">
                                <h5 class="text-primary ml-2">Templates</h5>
                            </div>
                            <div class="row d-flex justify-content-center">
                                <div class="col-md-3 p-1" >
                                    <a class="text-decoration-none text-primary font-weight-bold ml-1" id="csv_download"><i class="feather icon-download"></i> CSV</a>
                                </div>
                                <div class="col-md-3 p-1" >
                                    <a href="#" class="text-decoration-none font-weight-bold text-primary ml-1" id="xlsx_download"><i class="feather icon-download"></i> XLSX</a>
                                </div>
                                <div class="col-md-3 p-1" >
                                    <a href="#" class="text-decoration-none text-primary font-weight-bold ml-1" id="xls_download"><i class="feather icon-download"></i> XLS</a>
                                </div>
                            </div>
                        </div>
                        <form class="form-horizontal" method="POST" action="{{ route($route, ['model' => $model]) }}" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('csv_file') ? ' has-error' : '' }}">
                                <label for="csv_file" class="col-md-4 control-label">CSV / Excel to Import</label>

                                <div class="col-md-12">
                                    <input id="csv_file" type="file" class="form-control-file" name="csv_file" required>
                                    
                                    @if($errors->has('csv_file'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('csv_file') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                    <label class="text-gray pl-1" style="font-size: 15px; opacity:80%">Supported File Format : .xls, .xlsx, .csv</label>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="header" checked> @lang('global.app_file_contains_header_row')
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group float-right">
                                <div class="col-md-12 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Import Product
                                    </button>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
<script>
    $(document).ready(function(){
        $('#csv_download').click(function () {
    console.log('csv');
     $.ajax({
          url: "{{ asset('import_template/products/products.csv') }}",
          method: 'GET',
          xhrFields: {
              responseType: 'blob'
          },
          success: function (data) {
              var a = document.createElement('a');
              var url = window.URL.createObjectURL(data);
              a.href = url;
              a.download = 'products.csv';
              document.body.append(a);
              a.click();
              a.remove();
              window.URL.revokeObjectURL(url);
          }
      });
});
$('#xls_download').click(function () {
    console.log('csv');
     $.ajax({
          url: "{{ asset('import_template/products/products.xls') }}",
          method: 'GET',
          xhrFields: {
              responseType: 'blob'
          },
          success: function (data) {
              var a = document.createElement('a');
              var url = window.URL.createObjectURL(data);
              a.href = url;
              a.download = 'products.xls';
              document.body.append(a);
              a.click();
              a.remove();
              window.URL.revokeObjectURL(url);
          }
      });
});
$('#xlsx_download').click(function () {
    console.log('xlsx');
     $.ajax({
          url:"{{ asset('import_template/products/products.xlsx') }}",
          method: 'GET',
          xhrFields: {
              responseType: 'blob'
          },
          success: function (data) {
              var a = document.createElement('a');
              var url = window.URL.createObjectURL(data);
              a.href = url;
              a.download = 'products.xlsx';
              document.body.append(a);
              a.click();
              a.remove();
              window.URL.revokeObjectURL(url);
          }
      });
});
    })

</script>