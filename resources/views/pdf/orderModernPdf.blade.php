<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>
        /* *{ font-family:Arial, Helvetica, sans-serif !important;} */
        * {
            font-family: DejaVu Sans !important;
        }

        .table-borderless>tbody>tr>td,
        .table-borderless>tbody>tr>th,
        .table-borderless>tfoot>tr>td,
        .table-borderless>tfoot>tr>th,
        .table-borderless>thead>tr>td,
        .table-borderless>thead>tr>th {
            border: 1px;
            border-color: white;
        }
    </style>
</head>

<body>
    @php
    $cmeta_data = json_decode($customer->meta_data, true);
    if(empty($meta_data['settings']['currency']))
    $currency = "₹";
    else
    $currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
    $subtotal=0;
    $total_discounted_price = 0;

    @endphp
    <table class="table table-borderless">
        <tr>
            <td>
                <p class=" font-weight-bold h5 text-capitalize text-secondary p-0 m-0">{{$meta_data['storefront']['storename'] ?? ''}}</p>
                <div class=" p-0 m-0" style="font-size: 14px;">
                    {{$meta_data['settings']['emailid'] ?? ''}}<br>
                    {{isset($meta_data['settings']['contactnumber_country_code']) ? '+'.$meta_data['settings']['contactnumber_country_code'].'-'.$meta_data['settings']['contactnumber'] : '+91'.'-'.$meta_data['settings']['contactnumber']}}<br>
                    @if(isset($meta_data['checkout_settings']['tax_settings']) && $meta_data['checkout_settings']['tax_settings'] == '1')
                    <b> GST Number :</b>&nbsp;{{$meta_data['checkout_settings']['tax_setting_details']['gst_number']}}<br>
                    @endif
                </div>
            </td>
           
            <td>
                <p class=" font-weight-bold h5 text-secondary p-0 m-0">Order Details</p>
                <div class=" p-0 m-0" style="font-size: 14px;">
                    <b> Date:</b>&nbsp;{{date('d-m-Y h:i:s A', strtotime($customer->created_at))}}<br>
                    <b> Order #</b>{{$customer->order->order_ref_id}}<br>
                    @if(isset($customer->order->transactions->transaction_id))
                    <b> Transaction Id:</b>&nbsp;{{$customer->order->transactions->transaction_id}}
                    <br><b> Payment Method:</b>&nbsp;{{$customer->order->transactions->payment_mode}}
                    @endif
                </div>
            </td>
        </tr>
        @if($customer->address1 != 'NoPosaddress')
        <tr>
            <td>
                <p class="font-weight-bold bg-secondary text-white p-1 m-0 w-100">Billed to</p>
                <div class=" p-0 m-0" style="font-size: 14px;">
                    <b> {{ $customer->fname ?? "" }}</b>,<br>
                    {{ $customer->address1 ?? "" }},
                    @if ($customer->address2)
                    {{ $customer->address2}},<br>
                    @else
                    <br>
                    @endif
                    {{ $customer->city ?? "" }} - {{ $customer->pincode ?? "" }},<br>
                    {{ $customer->state ?? "" }}, {{ $customer->country ?? "" }}.<br>
                    {{ $customer->mobileno ?? "" }}<br>
                    {{ $customer->amobileno ?? "" }}
                </div>
            </td>
            {{-- <td>
            </td> --}}
            <td class="text-left text-bold-300">
                <p class="font-weight-bold bg-secondary text-white p-1 m-0 w-100">Ship to</p>
                <div class=" p-0 m-0" style="font-size: 14px;">
                    <b> {{ $customer->fname ?? "" }}</b>,<br>
                    {{ $customer->address1 ?? "" }},
                    @if ($customer->address2)
                    {{ $customer->address2}},<br>
                    @else
                    <br>
                    @endif
                    {{ $customer->city ?? "" }} - {{ $customer->pincode ?? "" }},<br>
                    {{ $customer->state ?? "" }}, {{ $customer->country ?? "" }}.<br>
                    {{ $customer->mobileno ?? "" }}<br>
                    {{ $customer->amobileno ?? "" }}
                </div>
            </td>
        </tr>
        @endif
    </table>
    <table class=" m-1 table table-borderless">
        <tr class="table bg-secondary text-white">
            <th scope="col" class=" text-center">S.No</th>
            <th scope="col" class=" text-center">Description</th>
            <th scope="col" class=" text-center">Item Price</th>
            <th scope="col" class=" text-center">Discount</th>
            <th scope="col" class=" text-center">Qty</th>
            <th scope="col" class=" text-center">Amount</th>
        </tr>
        @foreach ($products as $oproduct)
        @php
        // $img = $objimg = asset('XR/assets/images/placeholder.png');
        // if(count($aproduct->photo) > 0){
        // //$img = $aproduct->photo[0]->getFullUrl();
        // //$img = $aproduct->photo[0]->getUrl();
        // $img = $aproduct->photo[0]->getUrl('thumb');
        // //$img = public_path($img); // only for local images
        // }
        if($oproduct['discount'] != null)
        {
        $discount_price = $oproduct['price'] * ($oproduct['discount']/100);
        $total_discounted_price += $discount_price * $oproduct->qty;
        $discounted_price = $oproduct['price'] - $discount_price;
        $product_price = $discounted_price * $oproduct->qty;
        }

        $product = $oproduct->product;
        $img = $objimg = asset('XR/assets/images/placeholder.png');
        $obj_id = "";
        if(count($product->photo) > 0) {
            $img = $product->photo[0]->getUrl('thumb');
        }

        $actual_price=number_format($oproduct->price * $oproduct->qty,2);
        $subtotal += $oproduct->price*$oproduct->qty;
        $items_total = $subtotal - $total_discounted_price;
        @endphp
        <tr class=" table-secondary p-0 m-0">
            <td>{{ $loop->index + 1 }}</td>
        <td scope="row">

                       {{-- @if(count($product->photo) > 1)
                        <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails h-100 w-100" data-interval="false" data-ride="carousel">
                            <!--Slides-->
                            <div class="carousel-inner h-100 w-100" role="listbox">
                                @foreach($product->photo as $photo)
                                <div class="carousel-item {{$loop->index == 0?'active':''}}  h-100 w-100 d-flex justify-content-center">
                                    <img class="img-fluid mw-100 h-100 rounded" src="{{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl('thumb'):$objimg ?? ''}}" style="min-height:13rem;" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                </div>
                                @endforeach
                            </div>
                            <div class="w-100 position-absolute d-flex niceScroll" style="height:55px;bottom:5px;z-index:10;overflow-y:hidden;overflow-x:scroll">
                                <ol class="carousel-indicators w-100 position-relative">
                                    @foreach($product->photo as $photo)
                                        <li data-target="#carousel-thumb" data-slide-to="{{$loop->index}}" class="rounded-circle mr-1 {{$loop->index == 0?'active':''}}" style="width:40px; height:40px">
                                            <img class="d-flex rounded-circle border-dark border" style="width:40px;height:40px" src="{{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl('thumb'):$objimg ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                        </li>
                                    @endforeach
                                </ol>
                            </div>
                        </div>
                        @else
                            <img class="img-fluid mw-100 rounded h-100" src="{{$img ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="min-height:13rem;">
                        @endif --}}
                <label class="text-wrap text-capitalize" style="width: 7rem;font-weight:bold;word-wrap:break-word;">{{$oproduct->name ?? 'Untitled'}}</label></br>
                <p class=" text-capitalize" style="font-size: 10px">
                    @if($oproduct->sku)
                    <b class=" text-uppercase">Sku:</b>{{" ".$oproduct->sku}}<br>
                    @endif
                    @if($oproduct->notes)
                    <div style="border: 1px solid black;border-radius:5px;padding: 5px;background: #e7e9ec;">
                        <h4 class="text-dark">Notes :</h4>
                        <p>{{" ".$oproduct->notes}}</p>
                    </div>
                    @endif
                </p>
            </td>
            <td class="text-right">{!!$oproduct->price ? $cmeta_data['currency'] ."".$oproduct->price :$currency.'00.00' !!}</td>
            <td class="text-right">{{$oproduct->discount ? $oproduct->discount.'%' : '00.00%'}}</td>
            <td class="text-right">{{$oproduct->qty}}</td>
            <td class="text-right">
                @if($oproduct['discount'] != null)
                <p class=" m-0 p-0"> {!! $cmeta_data['currency'] !!} {{ number_format($product_price,2) }}</p>
                <p class=" m-0 p-0"><s>{!! $cmeta_data['currency'] !!} {{ $actual_price }}</s></p>
                @else
                {!! $cmeta_data['currency'] !!} {{ $actual_price }}
                @endif
            </td>
        </tr>
        @endforeach
        <tr class="">
            <td colspan="4" class="font-weight-bold bg-secondary text-white mt-1">
                Special notes and instructions
            </td>
            <td>Items</td>
            <td class=" text-right">
                {!! $cmeta_data['currency'] !!} {{ number_format($items_total,2) }}
            </td>
        </tr>
        
        <tr>
            <td class="table-secondary" colspan="4">
                {{$customer->order->notes ? $customer->order->notes :  "" }}
            </td>
            <td>&nbsp;</td>
            <td class=" text-right">
                &nbsp;
                {{-- -{!! $cmeta_data['currency']."".number_format( $total_discounted_price,2) !!}</s> --}}
            </td>
        </tr>
        @if(isset($cmeta_data['coupon_applied']))
        @if($cmeta_data['coupon_applied']==true)
        <tr>
        <td colspan="4">&nbsp;</td>
        <td>Coupon Discount</td>
        <td class=" text-right">
                - {!! $cmeta_data['currency'] !!} {{ number_format($cmeta_data['coupon_discount_amount'] ,2) }}
                
        </td>
        </tr>
        @endif
        @endif
        
        @if(isset($cmeta_data['shipping_charges']))
            <tr>
            <td colspan="4">&nbsp;</td>
                <td>Shipping</td>
                <td class=" text-right">
                    {!! $cmeta_data['currency'] !!} {{ number_format($cmeta_data['shipping_charges'] ,2) }}
                    
                </td>
            </tr>
        @endif
        @if(isset($cmeta_data['extra_charges_names']) && count($cmeta_data['extra_charges_names']) > 0)
            @foreach ($cmeta_data['extra_charges_names'] as $key => $extra_charge_name)
                @if($cmeta_data['extra_charges_values'][$key])
                <tr>
                    <td colspan="4">&nbsp;</td>
                    <td>{!! $extra_charge_name !!}</td>
                    <td class=" text-right">
                        {!! $cmeta_data['currency'] !!} {{ number_format($cmeta_data['extra_charges_values'][$key] ,2) }}
                        
                    </td>
                </tr>
                @endif
            @endforeach
        @endif
        @if(isset($cmeta_data['tax_percentage']) && $cmeta_data['tax_percentage'] > 0)
            <tr>
                <td colspan="4">&nbsp;</td>
                <td>Tax ({{$cmeta_data['tax_percentage']}}% GST)</td>
                <td class=" text-right">
                    {!! $cmeta_data['currency'] !!} {{ number_format($cmeta_data['tax_amount'] ,2) }}
                    
                </td>
            </tr>
        @endif
        <tr>
            <td colspan="4">&nbsp;</td>
            <td><b>Total</b></td>
            <td class=" text-right"><b>{!! $cmeta_data['currency'] !!} {{ number_format($customer->order->transactions->amount,2) }}</b></td>
        </tr>
        @if(isset($meta_data['invoice_settings']['show_signature_in_order_summary']) && $meta_data['invoice_settings']['show_signature_in_order_summary'] == 1)
            <tr>
                <td colspan="4">&nbsp;</td>
                @if(!empty($userSettings->invoice_signature))
                    @php
                        if(!empty($userSettings->getFirstMediaUrl('invoice_signature'))){
                            $signature = $userSettings->getFirstMediaUrl('invoice_signature');
                            // $signature = public_path($signature); // only for local images
                        }
                        else{
                            $signature = null;
                        }
                    @endphp
                    @if(isset($signature))
                        <td colspan="2" class="text-right">
                            For {{$meta_data['storefront']['storename'] ?? ''}}
                            <br>
                            <img src="{{$signature}}" style="max-width: 70px;max-height 30px;">
                            <br>
                            Authorized Signatory
                        <td>
                    @endif
                @endif
            </tr>
        @endif
        @if(!empty($customer->getFirstMediaUrl('signature')))
            <tr>
                <td colspan="4" class=" text-left"><b>CustomerSign:</b><img src="{{$customer->getFirstMediaUrl('signature')}}" width="100"height="100"/></td>
            </tr>
        @endif
        <tr>
            <td colspan="4" style="font-size: 20px" class=" m-0 p-0 font-weight-bolder text-secondary">
                Thank you for your business!
                <p style="font-size: 10px" class="m-0 p-0">
                    Should you have any enquieries concerning this order receipt, please contact us.
                </p>
            </td>
        </tr>
    </table>
    {{-- <!-- <p><b>Website: </b>{{App\Http\Controllers\Admin\CampaignsController::publicLink($userSettings->public_link,'store')}}</p> -->
    <div class=" position-bottom-0 w-100 bg-secondary text-white p-0 m-0 text-center">
        <p> Tel: {{$meta_data['settings']['contactnumber'] ? ' +'.$meta_data['settings']['contactnumber_country_code'].'-'.$meta_data['settings']['contactnumber'] : ' +91'.'-'.$meta_data['settings']['contactnumber']}}<br> | E-mail: {{" ".$meta_data['settings']['emailid'] ?? ''}} </p>
    </div> --}}
</body>

</html>