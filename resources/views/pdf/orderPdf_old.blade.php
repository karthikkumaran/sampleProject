<html>
<head>
<meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />
    <style>
    *{ font-family: DejaVu Sans !important;}
</style>
</head>

<body>
    @php
    $cmeta_data = json_decode($customer->meta_data, true);

    if(empty($meta_data['settings']['currency']))
    $currency = "₹";
    else
    $currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
    @endphp
    <div class="">
        <h5>{{$meta_data['storefront']['storename'] ?? ''}}</h5>
        <h6>{{isset($meta_data['settings']['contactnumber_country_code']) ? '+'.$meta_data['settings']['contactnumber_country_code'].'-'.$meta_data['settings']['contactnumber'] : '+91'.'-'.$meta_data['settings']['contactnumber']}}</h6>
        <div class="">
        <table style="width:100%" class="table table-bordered">
          <tr><th colspan="3" class="text-center">
                <h6>Order Details</h6>
                <h6>Order Date : {{date('d-m-Y h:i:s A', strtotime($customer->created_at))}}</h6>
                </th>
          </tr>
          <tr>
            <td>Order <b>#{{$customer->order->id}}</b></td>
            <td>
            No. Products : {{ count($products) }}
            </td>
            <td>Total Price : <b>{!! $currency !!}{{ $cmeta_data['product_total_price'] }}</b>
                                    </td>
                                    </tr>
        </table>

        <table style="width:100%" class="table table-bordered">
          <tr><th colspan="3" class="text-center">
                <h6>Customer Details</h6>
                </th>
          </tr>
<tr><td>Name </td><td>{{ $customer->fname ?? "" }}</td></tr>
<tr><td valign="top">Address </td><td>{{ $customer->address1 ?? "" }}, 
    @if ($customer->address2)
         <br> {{ $customer->address2}},
    @endif
<br> {{ $customer->city ?? "" }} - {{ $customer->pincode ?? "" }},
<br> {{ $customer->state ?? "" }},<br> {{ $customer->country ?? "" }}
</td></tr>

<tr><td>Email </td><td>{{ $customer->email ?? "" }}</td></tr>
<tr><td>Mobile No. </td><td>{{ $customer->mobileno ?? "" }}</td></tr>
<tr><td>Alternative Mobile No. </td><td>{{ $customer->amobileno ?? "" }}</td></tr>
@if ($customer->addresstype)
   <tr><td>Address Type </td><td>{{ $customer->addresstype ?? "" }}</td></tr> 
@endif

        </table>
           <h6>Ordered Product List</h6>
            <table style="width:100%" class="table table-bordered">
                <tr>
                    <th>No.</th>
                    <th>Product</th>
                    <th>Detail</th>
                    <th>Qty</th>
                    <th>Price</th>
                </tr>
                @foreach ($products as $oproduct)
                @php
                $aproduct = $oproduct->product;
                $img = $objimg = asset('XR/assets/images/placeholder.png');
                if(count($aproduct->photo) > 0){
                //$img = $aproduct->photo[0]->getFullUrl();
                //$img = $aproduct->photo[0]->getUrl();
                $img = $aproduct->photo[0]->getUrl('thumb');
                //$img = public_path($img); // only for local images
                }
                @endphp
                <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td><img src="{{ $img }}" width="100" /></td>
                    <td>{{$aproduct->name}}<br>{{$aproduct->sku}}</td>
                    <td>{{$oproduct->qty}}</td>
                    <td>{!! $currency ."".$oproduct->price !!}</td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</body>

</html>
