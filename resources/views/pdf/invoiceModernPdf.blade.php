<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>
        /* *{ font-family:Arial, Helvetica, sans-serif !important;} */
        * {
            font-family: DejaVu Sans !important;
        }

        .table-borderless>tbody>tr>td,
        .table-borderless>tbody>tr>th,
        .table-borderless>tfoot>tr>td,
        .table-borderless>tfoot>tr>th,
        .table-borderless>thead>tr>td,
        .table-borderless>thead>tr>th {
            border: 1px;
            border-color: white;
        }
    </style>
</head>

<body>
    @php
    $cmeta_data = json_decode($customer->meta_data, true);
    if(empty($meta_data['settings']['currency']))
    $currency = "₹";
    else
    $currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
    $subtotal=0;
    $total_discounted_price = 0;

    if(!empty($userSettings->getFirstMediaUrl('storelogo'))){
        //$img = $product->photo[0]->getFullUrl();
        //$img = $product->photo[0]->getUrl();
        $img = $userSettings->getFirstMediaUrl('storelogo');
        $img = public_path($img); // only for local images
    }
    else{
        $img = null;
    }

    @endphp
    
    <table border ="1" width="100%" height="842">
    <tr>
                <td colspan="8">
                <table class="table table-borderless">
                <tr>
                    <td>
                        @if(isset($img))
                            <img src="{{ $img }}" width="100"/>
                        @endif
                    </td>
                <!-- <p class=" font-weight-bold h5 text-capitalize text-secondary p-0 m-0">{{$userSettings->getFirstMediaUrl('storelogo')}}</p> -->
                    <td>
                        <div class=" p-0 m-0" style="font-size: 14px;">
                        <b> {{$meta_data['storefront']['storename'] ?? ''}}</b><br>
                            {{$meta_data['settings']['emailid'] ?? ''}}<br>
                            {{$meta_data['settings']['contactnumber'] ?? ''}}<br>
                    
                        </div></p>
                    </td>
        
            <!-- {{-- <td class="">
&nbsp;&nbsp;
        </td> --}} -->
                    <td align="right" >
                        <p><b>TAX INVOICE</b>
                        @if(isset($customer->order->inv_num))
                        <p>Invoice Number: INV_{{$customer->order->inv_num}}</p>
                        @endif
                     </td>
                </tr>
                </table>
                
            </td>
        </tr>        
        <tr border = "1">
            <td colspan="4" >
                
            </td>
            <td colspan="4">
            <table class="table table-borderless">
                    <tr>
                            <td>GST Number<span align="right">
                                &nbsp;:@if(isset($meta_data['checkout_settings']['tax_setting_details']['gst_number']))
                                {{  $meta_data['checkout_settings']['tax_setting_details']['gst_number']  }}@endif</span></td>
                        </tr>
                   <tr>
                     <td>Invoice Date<span align="right">&nbsp;:{{ $customer->order->inv_date}}</span></td>
                     <td></td>
                     </tr>
                    <tr>
                        <td>Terms<span align="right">&nbsp;:</span></td>
                        <td></td>                       
                    </tr>
                    <tr>
                      <td>Due Date<span align="right">&nbsp;:</span></td>
                       <td></td> 
                    </tr>
                </table>
            </td>
        </tr>
        <tr class=" table-secondary p-0 m-0">
            <td colspan="4"><b>Bill To</b></td>
            <td colspan="4"><b>Ship To</b></td>
        </tr>
        <tr>
            <td colspan="4">
                <div class=" p-0 m-0" style="font-size: 14px;">
                    <b> {{ $customer->fname ?? "" }}</b>,<br>
                    {{ $customer->address1 ?? "" }},
                    @if ($customer->address2)
                    {{ $customer->address2}},<br>
                    @else
                    <br>
                    @endif
                    {{ $customer->city ?? "" }} - {{ $customer->pincode ?? "" }},<br>
                    {{ $customer->state ?? "" }}, {{ $customer->country ?? "" }}.<br>
                    {{ $customer->mobileno ?? "" }}<br>
                    {{ $customer->amobileno ?? "" }}</p>
                </div>
            </td>
            <td colspan="4">
                <div class=" p-0 m-0" style="font-size: 14px;">
                    <b> {{ $customer->fname ?? "" }}</b>,<br>
                    {{ $customer->address1 ?? "" }},
                    @if ($customer->address2)
                    {{ $customer->address2}},<br>
                    @else
                    <br>
                    @endif
                    {{ $customer->city ?? "" }} - {{ $customer->pincode ?? "" }},<br>
                    {{ $customer->state ?? "" }}, {{ $customer->country ?? "" }}.<br>
                    {{ $customer->mobileno ?? "" }}<br>
                    {{ $customer->amobileno ?? "" }}
                </div>
            </td>
        </tr>
        <tr class=" table-secondary p-0 m-0">
            <th colspan="1" class=" text-center">S.No</th>
            <th colspan="3" class=" text-center">Items & Description</th>
            <th colspan="1" class=" text-center">Price</th>
            <th colspan="1" class=" text-center">Discount</th>            
            <th colspan="1" class=" text-center">Qty</th>            
            <th colspan="1" class=" text-center">Amount</th>
        </tr>
        @foreach ($products as $oproduct)
        @php
        // $img = $objimg = asset('XR/assets/images/placeholder.png');
        // if(count($aproduct->photo) > 0){
        // //$img = $aproduct->photo[0]->getFullUrl();
        // //$img = $aproduct->photo[0]->getUrl();
        // $img = $aproduct->photo[0]->getUrl('thumb');
        // // $img = public_path($img); // only for local image
        // }
        if($oproduct['discount'] != null)
        {
        $discount_price = $oproduct['price'] * ($oproduct['discount']/100);
        $total_discounted_price += $discount_price * $oproduct->qty;
        $discounted_price = $oproduct['price'] - $discount_price;
        $product_price = $discounted_price * $oproduct->qty;
        }
        $actual_price=number_format($oproduct->price * $oproduct->qty,2);
        $subtotal += $oproduct->price*$oproduct->qty;
        @endphp

        <tr>
            <td colspan="1">{{ $loop->index + 1 }}</td>
            <td colspan="3" scope="row"><b class=" text-capitalize">{{$oproduct->name}}</b><br>
                <p class=" text-capitalize" style="font-size: 10px">
                    @if($oproduct->sku)
                    <b class=" text-uppercase">Sku:</b>{{" ".$oproduct->sku}}<br>
                    @endif
                    @if($oproduct->notes)
                    <div style="border: 1px solid black;border-radius:5px;padding: 5px;background: #e7e9ec;">
                        <h4 class="text-dark">Notes :</h4>
                        <p>{{" ".$oproduct->notes}}</p>
                    </div>
                    @endif
                </p>
            </td>
            <td colspan="1" class="text-right">{!!$oproduct->price ? $cmeta_data['currency'] ."".$oproduct->price :$currency.'00.00' !!}</td>
            <!-- <td class="text-right">{{$oproduct->discount ? $oproduct->discount.'%' : '00.00%'}}</td> -->            
            <td class="text-right">{{$oproduct->discount ? $oproduct->discount.'%' : '00.00%'}}</td>
            <td colspan="1" class="text-right">{{$oproduct->qty}}</td>
            <td colspan="1" class="text-right">
                @if($oproduct['discount'] != null)
                <p class=" m-0 p-0"> {!! $cmeta_data['currency']."".number_format($product_price,2) !!}</p>
                <p class=" m-0 p-0"><s>{!! $cmeta_data['currency']."".$actual_price !!}</s></p>
                @else
                {!! $cmeta_data['currency']."".$actual_price !!}
                @endif
            </td>
        </tr>
        @endforeach
        <tr>

            <td colspan="5" style="border:none;">
            Total in Words<br>
            <b></b>
            </td>
            <td colspan="3">
            <table class="table table-borderless">
            @if(isset($cmeta_data['coupon_applied']))
        @if($cmeta_data['coupon_applied']==true)
        <tr>
        <td colspan="4">&nbsp;</td>
        <td>Coupon Discount</td>
        <td class=" text-right">
                - {!! $cmeta_data['currency'] !!} {{ number_format($cmeta_data['coupon_discount_amount'] ,2) }}
                
        </td>
        </tr>
        @endif
        @endif
        
        @if(isset($cmeta_data['shipping_charges']))
            <tr>
            <td colspan="3">&nbsp;</td>
                <td>Shipping</td>
                <td class=" text-right">
                    {!! $cmeta_data['currency'] !!} {{ number_format($cmeta_data['shipping_charges'] ,2) }}
                    
                </td>
            </tr>
        @endif
        @if(isset($cmeta_data['extra_charges_names']) && count($cmeta_data['extra_charges_names']) > 0)
            @foreach ($cmeta_data['extra_charges_names'] as $key => $extra_charge_name)
                @if($cmeta_data['extra_charges_values'][$key])
                <tr>
                    <td colspan="3">&nbsp;</td>
                    <td>{!! $extra_charge_name !!}</td>
                    <td class=" text-right">
                        {!! $cmeta_data['currency'] !!} {{ number_format($cmeta_data['extra_charges_values'][$key] ,2) }}
                        
                    </td>
                </tr>
                @endif
            @endforeach
        @endif
        @if(isset($cmeta_data['tax_percentage']) && $cmeta_data['tax_percentage'] > 0)
            <tr>
                <td colspan="3">&nbsp;</td>
                <td>Tax ({{$cmeta_data['tax_percentage']}}% GST)</td>
                <td class=" text-right">
                    {!! $cmeta_data['currency'] !!} {{ number_format($cmeta_data['tax_amount'] ,2) }}
                    
                </td>
            </tr>
        @endif
        <tr>
            <td colspan="3">&nbsp;</td>
            <td><b>Total</b></td>
            <td class=" text-right"><b>{!! $cmeta_data['currency'] !!} {{ number_format($customer->order->transactions->amount,2) }}</b></td>
        </tr>
            </table>
            </td>
        </tr>
        <tr>
            <td colspan="5" class="Text-left" style="border:none;">
                Bank Name&nbsp;:<br>
                A/c No&nbsp;:<br>
                Account Name&nbsp;:<br><br>
                SWIFT&nbsp;:<br><br>
                IFSC Code&nbsp;:<br><br>
                MICR Code&nbsp;:<br><br>
            </td>
            
                @if(isset($meta_data['invoice_settings']['show_signature_in_order_summary']) && $meta_data['invoice_settings']['show_signature_in_order_summary'] == 1)
            
                @if(!empty($userSettings->invoice_signature))
                    @php
                        if(!empty($userSettings->getFirstMediaUrl('invoice_signature'))){
                            $signature = $userSettings->getFirstMediaUrl('invoice_signature');
                            // $signature = public_path($signature); // only for local images
                        }
                        else{
                            $signature = null;
                        }
                    @endphp
                    @if(isset($signature))
                        <td colspan="3" class="text-right">
                            For {{$meta_data['storefront']['storename'] ?? ''}}
                            <br>
                            <img src="{{$signature}}" style="max-width: 70px;max-height 30px;">
                            <br>
                            Authorized Signatory
                    </td>
                    @endif
                @endif
        @endif
        </tr>
        <tr border="0">
            <td colspan="8" class="Text-left" style="border:none;">
                Bank Address:<br>
                <br><br><br><br>
                Branch:<br><br><br>
                Thanks for your business.
            </td>
            <!-- <td colspan="3" class="text-center">
               
            </td> -->
        </tr>

        
    </table>
    <!--{{-- <p><b>Website: </b>{{App\Http\Controllers\Admin\CampaignsController::publicLink($userSettings->public_link,'store')}}</p> -->
    <!-- <div class=" position-bottom-0 w-100 bg-secondary text-white p-0 m-0 text-center">
     <p> Tel: {{" ".$meta_data['settings']['contactnumber'] ?? ''}} | E-mail: {{" ".$meta_data['settings']['emailid'] ?? ''}} </p>
    </div> --}} -->
    <!-- <p>Wheteher the tax is payable under reverse charge-No</p> -->
</body>

</html>