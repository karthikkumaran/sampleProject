<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />
<style>
    *{ font-family: DejaVu Sans !important;}
</style>
</head>
<body>
@php
  if(empty($meta_data['settings']['currency'])){
    $currency = "₹";
  }
  else{
    if($meta_data['settings']['currency_selection'] == "currency_symbol"){
      $currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
    }
    else{
      $currency = $meta_data['settings']['currency'];
    }
  }

  if(!empty($userSettings->customdomain)){
    $storeLink = '//'.$userSettings->customdomain;
  }else if(!empty($userSettings->subdomain)) {
    $storeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
  }else{
    $storeLink = route('mystore.published',[$userSettings->public_link]);
  }
  $catalogLink = route('campaigns.published',[$campaign->public_link]);
@endphp
  <div class="">
    @if(!empty($meta_data['storefront']))
      @php
        if(!empty($userSettings->getFirstMediaUrl('storelogo'))){
          $storeLogo = $userSettings->getFirstMediaUrl('storelogo');
          // $storeLogo = public_path($storeLogo); // only for local images
        }
        else{
          $storeLogo = null;
        }
      @endphp
      @if(!empty($storeLogo))
        <img src="{{$storeLogo}}" style="max-width: 70px;max-height: 37px;">
      @endif
    @endif
    <h4><a href="{{$storeLink}}" target="blank">{{$meta_data['storefront']['storename'] ?? ''}}</a><span class="float-right h6">Date: {{ date('d-M-Y') }}</span></h4>
    <h6><a href="mailto:{{$meta_data['settings']['emailid'] ?? ''}}" target="blank">{{$meta_data['settings']['emailid'] ?? ''}}</a></h6>
    <h6>{{isset($meta_data['settings']['contactnumber_country_code']) ? '+'.$meta_data['settings']['contactnumber_country_code'].'-'.$meta_data['settings']['contactnumber'] : '+91'.'-'.$meta_data['settings']['contactnumber']}}</h6>
    <h4 class=""><a href="{{$catalogLink}}" target="blank">{{$campaign->name ?? 'Untitled catalog'}}</a></h4>
    <div class="">
      <table style="width:100%" class="table table-bordered">
        <tr>
          <th>No.</th>
          <th>Product</th> 
          <th>Detail</th>
          {{-- <th>Discount</th> --}}
          <th>Price</th>
        </tr>
        @foreach($data as $key => $card)
          @php
            $product = $card->product;
            if($product->discount != null) {   
              $discount_price = $product->price * ($product->discount/100);
              $discounted_price = number_format(($product->price - $discount_price),2);
            }
            $img = asset('XR/assets/images/placeholder.png');
            if(count($product->photo) > 0){
              //$img = $product->photo[0]->getFullUrl();
              //$img = $product->photo[0]->getUrl();
              $img = $product->photo[0]->getUrl('thumb');
              // $img = public_path($img); // only for local images
            }
            $catalogDetailsLink = route('campaigns.publishedCatalogDetails',[$product->id,$campaign->public_link]);
          @endphp
          <tr>
            <td><b>{{ $loop->index + 1 }}</b></td>
            <td><img src="{{ $img }}" width="100"/></td>
            <td>
              <b><a href="{{$catalogDetailsLink}}" target="_blank">{{$product->name ?? 'Untitled'}}</a></b>
              <br>
              @if(!empty($product->sku))
                {{$product->sku ?? ''}}
              @endif
            </td>
            <!-- <td class="text-right"><b>{!! $currency ."".$product->price !!}</b></td> -->
            {{-- <td class="text-right">{{$product->discount ? $product->discount.'%' : '00.00%'}}</td> --}}
            <td class="text-right">          
              @if($product->discount != null)
                <p>{!! $currency."".$discounted_price !!}<p>
                <p><s>{!! $currency."".$product->price !!}</s><p>
              @else
                <p>{!! $currency."".$product->price ?? '0.00' !!}<p>
              @endif
            </td>
          </tr>
        @endforeach
      </table>
    </div>
  </div>
</body>
</html>