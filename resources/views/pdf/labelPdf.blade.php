<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>
        /* *{ font-family:Arial, Helvetica, sans-serif !important;} */
        * {
            font-family: DejaVu Sans !important;
        }

        .table-borderless>tbody>tr>td,
        .table-borderless>tbody>tr>th,
        .table-borderless>tfoot>tr>td,
        .table-borderless>tfoot>tr>th,
        .table-borderless>thead>tr>td,
        .table-borderless>thead>tr>th {
            border: 1px;
            border-color: white;
        }
    </style>
</head>

<body>
    @php
    $cmeta_data = json_decode($customer->meta_data, true);
    if(empty($meta_data['settings']['currency']))
    $currency = "₹";
    else
    $currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
    $subtotal=0;
    $total_discounted_price = 0;

    @endphp
    <table class="table table-borderless">
        <tr>
            <td>
                <p class="font-weight-bold bg-secondary text-white p-1 m-0 w-100">From : </p>
                <h3>{{$meta_data['storefront']['storename'] ?? ''}}</h3>
                <div class=" p-0 m-0" style="font-size: 14px;">
                @if(isset($meta_data['footer_settings']['address1']))
                    {{$meta_data['footer_settings']['address1'] ?? ''}},
                    {{$meta_data['footer_settings']['address2'] ?? ''}}<br>
                @endif
                @if(isset($meta_data['footer_settings']['city']))
                    {{$meta_data['footer_settings']['city'] ?? ''}}-
                @endif
                @if(isset($meta_data['footer_settings']['pincode']))
                 {{$meta_data['footer_settings']['pincode'] ?? ''}},<br>
                @endif
                @if(isset($meta_data['footer_settings']['state']))
                    {{$meta_data['footer_settings']['state'] ?? ''}},
                @endif
                @if(isset($meta_data['footer_settings']['country']))
                  {{$meta_data['footer_settings']['country'] ?? ''}}.<br>
                 @endif
                    {{isset($meta_data['settings']['contactnumber_country_code']) ? '+'.$meta_data['settings']['contactnumber_country_code'].'-'.$meta_data['settings']['contactnumber'] : '+91'.'-'.$meta_data['settings']['contactnumber']}}<br>
                    @if(isset($meta_data['checkout_settings']['tax_settings']) && $meta_data['checkout_settings']['tax_settings'] == '1')
                    <b> GST Number :</b>&nbsp;{{$meta_data['checkout_settings']['tax_setting_details']['gst_number']}}<br>
                    @endif
                </div>
            </td>
            <td>
            <p class="font-weight-bold bg-secondary text-white p-1 m-0 w-100">To : </p>
                <div class=" p-0 m-0" style="font-size: 14px;">
                    <h3> {{ $customer->fname ?? "" }}</h3>
                    {{ $customer->address1 ?? "" }},
                    @if ($customer->address2)
                    {{ $customer->address2}},<br>
                    @else
                    <br>
                    @endif
                    {{ $customer->city ?? "" }} - {{ $customer->pincode ?? "" }},<br>
                    {{ $customer->state ?? "" }}, {{ $customer->country ?? "" }}.<br>
                    {{ $customer->mobileno ?? "" }}<br>
                    {{ $customer->amobileno ?? "" }}
                </div>
            </td>
    </tr>
</table>
</body>

</html>
