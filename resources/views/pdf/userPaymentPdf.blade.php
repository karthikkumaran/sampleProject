<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>
        /* *{ font-family:Arial, Helvetica, sans-serif !important;} */
        * {
            font-family: DejaVu Sans !important;
        }

        .table-borderless>tbody>tr>td,
        .table-borderless>tbody>tr>th,
        .table-borderless>tfoot>tr>td,
        .table-borderless>tfoot>tr>th,
        .table-borderless>thead>tr>td,
        .table-borderless>thead>tr>th {
            border: 1px;
            border-color: white;
        }
    </style>
</head>

<body>
@php
        $payment_data = unserialize($user_payment->meta_data);
    @endphp
    <div class="content-body">
    <section class="invoice-preview-wrapper">
            <div class="row invoice-preview">
            <div class="col-xl-9 col-md-8 col-12">
                <div class="card invoice-preview-card">
                            <div class="card-body invoice-padding pb-0">

    <table border ="1" width="100%" >
    <tr>
                <td colspan="8">
                <table class="table table-borderless">
                <tr>
                <td>
                        <img src="{{asset('XR/app-assets/images/logo/Mirrar.png')}}" height="100" width=100%/>
                    </td>
                    <td>
                    <div class=" p-0 m-0" style="font-size: 14px;">
                        <p><b> MIRRAR INNOVATION</b><br>
                        <b>TECHNOLOGIES PRIVATE LIMITED</b><br>
                            A-6,Industrial Estate,<br>
                            West Mogappair,<br>
                            Chennai Tamilnadu 600037.</p>
                        </div>
                    </td>
                    <td class="text-center">
                    <p><b>TAX INVOICE</b>
                    </td>
        
            <!-- {{-- <td class="">
&nbsp;&nbsp;
        </td> --}} -->
                    
                </tr>
                </table>
                
            </td>
        </tr>        
        <tr class=" table-secondary p-0 m-0">
            <td colspan="4"><b>Invoice Details: </b></td>
            <td colspan="4"><b>To: </b></td>
        </tr>
        <tr>
            <td colspan="4">
            <h6 class="invoice-title">
                                        Invoice Id - 
                                        <span class="invoice-number">INV-SIMPLI-{{$planinvoice->inv_num}}</span>
            </h6>
            <p>Invoice Date<span align="right">&nbsp;:{{$planinvoice->inv_date}}</span></p>
            <p>Terms<span align="right">&nbsp;:{{$planinvoice->terms}}</span></p>
            <p>Due Date<span align="right">&nbsp;:{{$planinvoice->due_date}}</span></p>
            </td>
            <td colspan="4">
                <div class=" p-0 m-1" style="font-size: 14px;">
                <b> {{$user_meta_data['storefront']['storename'] ?? ''}}</b><br>
                @if(isset($user_meta_data['footer_settings']['address1']))
                    {{$user_meta_data['footer_settings']['address1'] ?? ''}},
                    {{$user_meta_data['footer_settings']['address2'] ?? ''}}<br>
                @endif
                @if(isset($user_meta_data['footer_settings']['city']))                
                    {{$user_meta_data['footer_settings']['city'] ?? ''}}-
                @endif  
                @if(isset($user_meta_data['footer_settings']['pincode']))
                 {{$user_meta_data['footer_settings']['pincode'] ?? ''}},<br>
                @endif
                @if(isset($user_meta_data['footer_settings']['state']))
                    {{$user_meta_data['footer_settings']['state'] ?? ''}},
                @endif
                @if(isset($user_meta_data['footer_settings']['country']))
                  {{$user_meta_data['footer_settings']['country'] ?? ''}}.<br>
                 @endif
                 @if(isset($user_meta_data['settings']['contactnumber_country_code'])&&isset($user_meta_data['settings']['contactnumber']))
                    {{isset($user_meta_data['settings']['contactnumber_country_code']) ? '+'.$user_meta_data['settings']['contactnumber_country_code'].'-'.$user_meta_data['settings']['contactnumber'] : '+91'.'-'.$user_meta_data['settings']['contactnumber']}}<br>
                 @elseif(isset($user_meta_data['settings']['contactnumber'])) 
                    {{'+91'.'-'.$user_meta_data['settings']['contactnumber'] }}
                 @endif  
                </div>
            </td>
        </tr>
            </table>
                <!-- Invoice -->
                <table width=100% border='1'>
            
                <tr class=" table-secondary p-0 m-0">
                    <th colspan="1" class=" text-center">S.No</th>
                    <th colspan="3" class=" text-center">Plan Name</th>
                    <th colspan="1" class=" text-center">Qty</th>            
                    <th colspan="1" class=" text-center">Price</th>
                    <th colspan="1" class=" text-center">Discount</th>            
                    <th colspan="1" class=" text-center">Amount Paid</th>
                </tr>
                <tr>
                    <td colspan="1" class="text-center">1</td>
                    <td colspan="3" class="font-weight-bold text-center">{{$user_payment->plan_name}}</td>
                    <td colspan="1" class="text-center">1</td>
                    <td colspan="1" class="text-center">Rs.{{number_format($plan->price,2)}}</td>
                    <td colspan="1" class="text-center">{{$plan->discount}}</td>
                    <td colspan="1" class="text-center">{!! $payment_data->currency ?? $payment_data["currency"] !!}
                                                @if(!empty($payment_data->amount))
                                                    Rs.{{number_format(($payment_data->amount)/100,2)}}
                                                @else
                                                    Rs.{{number_format($payment_data["amount"],2)}}
                                                @endif</td>
                </tr>
                </table>
                    
            <table width=100% border ="1">                
            <tr>
            <td>
                <p class="font-weight-bold bg-secondary text-white p-1 m-0 w-100">Plan Details:</p>
                <div class=" p-0 m-0" style="font-size: 14px;">
                    <b>Plan: </b>{{$user_payment->plan_name}}<br>
                    <b>Plan price: </b>{!! $payment_data->currency ?? $payment_data["currency"] !!} {{number_format($plan->price,2)}}<br>
                    <b>Plan starts at: </b>{{$user_payment->plan_start}}<br>
                    <b>Plan expiry: </b>{{$user_payment->plan_expiry}}<br>
                    <br>
                </div>
            </td>
            
            <td >
                <p class="font-weight-bold bg-secondary text-white p-1 m-0 w-100">Payment Details:</p>
                <div class=" p-0 m-0" style="font-size: 14px;">
                    <b>Total Amount: </b> 
                    {!! $payment_data->currency ?? $payment_data["currency"] !!}
                    @if(!empty($payment_data->amount))
                        {{($payment_data->amount)/100}}
                    @else
                        {{$payment_data["amount"]}}
                    @endif
                    <br>
                    <b>Payment Id: </b>{{$user_payment->payment_id}}<br>
                    <b>Payment Mode: </b>{{$user_payment->payment_mode}}<br>
                    <b>Payment Method: </b>{{$user_payment->payment_method}}<br>
                    <b>Status: </b>{{$user_payment->status}}<br>
                </div>
            </td>
        </tr>
        </table>
        
        <table width=100% class="m-1">
            <tr>
            <td  class="text-center" >
            <p>Thanks for your business.</p>
                <p>This is computerized generated invoice version</p>
            </td>
            
                
            </tr>
        
        
    </table>
                    </div>
                </div>
            </div>
</body>

</html>