<!DOCTYPE html>
<html class="loaded" lang="en" data-textdirection="ltr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $meta_data['storefront']['storename'] ?? 'Store' }}</title>
    <!-- <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon.png') }}"> -->

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/vendors.min.css') }}">
    <!-- END: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/extensions/sweetalert2.min.css')}}">

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/components.css') }}">
    @livewireStyles
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/extensions/toastr.css') }}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/assets/css/style.css') }}">
    <!-- END: Custom CSS--> 
    
    <style>
        .text-blue{
            color: #3AADD8;
        }
        .text-red {
            color: #DB1F26;
        }
        .bg-maroon{
            background-color: #6C4E4F !important;
            color: #fff !important;
        }
        .bg-warning{
            background-color: #6C4E4F !important;
        }
        .bg-theme{
            background-color: #EAE1E1;
        }
        .text-brown{
            color: #6C4E4F;
        }
    </style>
    </style>
    @yield('styles')
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-169878524-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-169878524-1');
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N67BLVX');</script>
<!-- End Google Tag Manager -->
@if(!empty($meta_data['analytic_settings']['google_analytics_id']))
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-{{$meta_data['analytic_settings']['google_analytics_id']}}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '{{$meta_data['analytic_settings']['google_analytics_id']}}');
        </script>
    @endif
    @if(!empty($meta_data['custom_script_settings']['custom_script_header']))
    {!! $meta_data['custom_script_settings']['custom_script_header'] !!}
    @endif
</head>

<!-- BEGIN: Body-->
@php
if($userSettings->is_start == 1) {
    if(!empty($userSettings->customdomain)){
        $homeLink = '//'.$userSettings->customdomain;

    }else if(!empty($userSettings->subdomain)) {
        $homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');

    }else{
        $homeLink = route('mystore.published',[$userSettings->public_link]);
    }
    }else{
        $homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
    }

    $sLink = $userSettings->public_link;
    $loginLink = $homeLink.'/customer/login';
    $registerLink = $homeLink.'/customer/register';
    

@endphp

<body
    class="vertical-layout vertical-menu-modern 1-column  navbar-floating footer-static bg-body"
    data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N67BLVX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
@if(!empty($meta_data['analytic_settings']['google_analytics_gtm_id']))
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{$meta_data['analytic_settings']['google_analytics_gtm_id']}}" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    @endif
    <!-- BEGIN: Content-->
    @include('themes.'.$meta_data['themes']['name'].'.partials.public_header')
    @include('themes.'.$meta_data['themes']['name'].'.partials.splash')

    <div class="app-content content" style="display: none;">
        <div class="content-wrapper mt-0 pt-0">
        <div class="min_height_30_rem min_height_50_rem">
    
            @yield("content")
        </div>
        </div>
    </div>
        

    @include('themes.'.$meta_data['themes']['name'].'.partials.storefrontfooter')
    
    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('XR/app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{asset('XR/app-assets/vendors/js/extensions/sweetalert2.all.min.js')}}"></script>

    <script src="{{ asset('XR/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    <script src="{{ asset('js/customer.js') }}"></script>
    @livewireScripts
    
    <!-- BEGIN Vendor JS-->
    <script>$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
},
error: function (jqXHR, exception) {
    if (jqXHR.status === 0) {
        Swal.fire('You lost connectivity', 'Verify your internet connection.', 'error');
    } else if (jqXHR.status == 401) {
        window.location = "/login";
    } else if (jqXHR.status == 404) {
        Swal.fire('Requested page not found', '404', 'error');
    } else if (jqXHR.status == 500) {
        Swal.fire('Internal Server Error', '500', 'error');
    } else if (exception === 'parsererror') {
        Swal.fire('Requested JSON parse failed', 'JSON Error', 'error');
    } else if (exception === 'timeout') {
        Swal.fire('Time out error', '', 'error');
    } else if (exception === 'abort') {
        Swal.fire('Ajax request aborted', '', 'error');
    } else {
        var err = eval("(" + jqXHR.responseText + ")");
        if(err.errors.code != undefined)
            Swal.fire('Error!', err.errors.code, 'error');
        else if(err.errors.name != undefined)
            Swal.fire('Error!', err.errors.name, 'error');
        else if(err.message != undefined)
            Swal.fire('Error!', err.message, 'error');
        else
            Swal.fire('Error!', err, 'error');
    }
}
});

        var searchInputInputfield = $(".search-input input"),
    searchList = $(".search-input .search-list"),appContent = $(".app-content");
        $(".nav-link-search").on("click", function () {
            var $this = $(this);
            var searchInput = $(this).parent(".nav-search").find(".search-input");
            searchInput.addClass("open");
            searchInputInputfield.focus();
            searchList.find("li").remove();
            // bookmarkInput.removeClass("show");
        });
        $(".search-input-close i").on("click", function () {
            var $this = $(this),
            searchInput = $(this).closest(".search-input");
            if (searchInput.hasClass("open")) {
            searchInput.removeClass("open");
            searchInputInputfield.val("");
            searchInputInputfield.blur();
            searchList.removeClass("show");
            appContent.removeClass("show-overlay");
            }
        });
        $(window).on('load', function () {
            $('.app-content').hide();
            setTimeout(removeLoader, 3000); //wait for page load PLUS two seconds.
        });

        function removeLoader() {
            $("#loading-bg").fadeOut(500, function () {
                // fadeOut complete. Remove the loading div
                $('.app-content').show();
                $("#loading-bg").remove(); //makes page more lightweight 
            });
        }

        function share(share_name,msg) {
            var u="{{Request::fullUrl()}}";
            var t=msg;
            if(msg=="")
            {
                t="I found this website interesting, you could give a try";
            }
            switch (share_name) {
                case 'fb':
                    window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t),'sharer','toolbar=0,status=0,width=626,height=436');
                    break;
                case 'whatsapp':
                    var message = encodeURIComponent(u);
                    // var whatsapp_url = "whatsapp://send?text=" + message;
                    // window.location.href = whatsapp_url;

                    var whatsapp_url = "https://wa.me/?text=" + message;
                    openInNewTab(whatsapp_url);
                    break;
                case 'twitter':
                    window.open("https://twitter.com/intent/tweet?text="+t+"&url="+u,'sharer','toolbar=0,status=0,width=626,height=436')
                    break;
            }
        }

        //lazy loading
        if ('loading' in HTMLImageElement.prototype) {
            const images = document.querySelectorAll("img.lazyload");
            images.forEach(img => {
                img.src = img.dataset.src;
            });
        } else {
            // Dynamically import the LazySizes library
            let script = document.createElement("script");
            script.async = true;
            script.src =
            "https://cdnjs.cloudflare.com/ajax/libs/lazysizes/4.1.8/lazysizes.min.js";
            document.body.appendChild(script);
        }

        $('.togglePassword').click( function() {
            const password = document.querySelector('#' + $(this).attr('data-id'));
            // toggle the type attribute
            const  type = password.getAttribute('type') === 'password' ? 'text' : 'password';
            password.setAttribute('type', type);
            // toggle the eye slash icon
            this.classList.toggle('fa-eye-slash');
        })

    </script>
    @yield('scripts')
    @if(!empty($meta_data['custom_script_settings']['custom_script_footer']))
    {!! $meta_data['custom_script_settings']['custom_script_footer'] !!}
    @endif
</body>

</html>
