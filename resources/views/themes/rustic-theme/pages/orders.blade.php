@extends($layout)
@section('content')

@php

if($userSettings->is_start == 1) {
    
$sLink = $userSettings->public_link;

if(!empty($userSettings->customdomain)){
$homeLink = '//'.$userSettings->customdomain;

}else if(!empty($userSettings->subdomain)) {
$homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');

}else{
$homeLink = route('mystore.published',[$userSettings->public_link]);

}
}else{
$homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
}



@endphp
<div class="margin_desk_3 margin_mob_2 p-2">
<h4 class=" position-absolute mt-1 order_title">Your Orders</h4>

<div class="dropdown text-right " >
    
    <button class="btn btn-lg btn-white dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Filter
    </button>
    <div class="dropdown-menu scrollable-dropdown" aria-labelledby="dropdownMenuButton" >
    <div class="nicescroll" style="height: 300px; overflow-y:scroll;">
    <a class="dropdown-item" href="#">
            <span class="vs-checkbox-con vs-checkbox-primary">
                <input type="checkbox" class="all" id="filter" name="filter" value="All" checked>
                <span class="vs-checkbox">
                    <span class="vs-checkbox--check">
                        <i class="vs-icon feather icon-check m-0"></i>
                    </span>
                </span>
                <span class="">All</span>
            </span>
        </a>
        @foreach(App\orders::STATUS as $key => $label)
        <a class="dropdown-item" href="#">
            <span class="vs-checkbox-con vs-checkbox-primary">
                <input class="filter" type="checkbox" id="filter" name="filter" value="{{$label}}">
                <span class="vs-checkbox">
                    <span class="vs-checkbox--check">
                        <i class="vs-icon feather icon-check m-0"></i>
                    </span>
                </span>
                <span class="">{{$label}}</span>
            </span>
        </a>
        @endforeach
    </div>
    <hr>
    <button id="apply" class="btn btn-primary dropdown-item text-white mt-1 mx-1 my-0">Apply</button></br>
    <button id="clear" class="btn btn-warning dropdown-item text-white  mx-1 my-0">Clear</button>
    </div>
</div>




<div class="mt-1 order-content">
  
</div>
</div>


@endsection
@section('scripts')
@parent

<script>
$(document).ready(function(){
$('#apply').click()

})

$('.filter').click(function(){
    if ($('.filter:checked').length >=1)
        $('.all').prop('checked',false)
    else if($('.filter:checked').length ==0)
        $('.all').prop('checked',true)
})
$('.status').change(function(){
      var data= $(this).val();
      array=[];
      array.push($(this).val())
      alert(data);
    $.ajax({
        url:"{{route('customer.orders.ajax')}}",
        method:"POST",
        data:{
            status:array,
            sLink:"{{$sLink}}",
            homeLink:"{{$homeLink}}"
        },
        success:function(response){
            $('.order-content').empty().html(response);
            $('#loading-bg').hide();
        }
    })

    });

$('#apply').click(function(){
$('#loading-bg').show();

    array=[]
    $("input:checkbox[name=filter]:checked").each(function(){
        array.push($(this).val())
    });

    $.ajax({
    url:"{{route('customer.orders.ajax')}}",
    method:"POST",
    data:{
        status:array,
        sLink:"{{$sLink}}",
        homeLink:"{{$homeLink}}"
    },
    success:function(response){
        $('.order-content').empty().html(response);
        $('#loading-bg').hide();
    }
    });
});
</script>

@endsection