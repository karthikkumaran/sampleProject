@extends($layout)
@section('content')
@php
$cmeta_data = json_decode($customer->meta_data, true);
$transaction =$customer->order->transactions;
@endphp
@php
$sLink = $userSettings->public_link;
@endphp


<div class="row outer_margin p-2 ">
    <div class="col-md-12 p-4 lg_hide">
        <div class="float-left"> <span class="h5 order_detail_title"><b>Order ID #{{$customer->order->order_ref_id}}</b></span>&nbsp;&nbsp;&nbsp;<span class="order_detail_text">Order Placed {{date('d/m/Y', strtotime($customer->created_at))}} </span></div>
        <div class="float-right"> <span class="h5 order_detail_title"><b>Order status</b></span>&nbsp;&nbsp;&nbsp;<span class="order_detail_text">{{App\orders::STATUS[$customer->order->status]}}</span></div>
    </div>
    <div class="col-md-12 px-4 py-2 sm_hide"> <span class="h5 order_detail_title"><b>Order ID #{{$customer->order->order_ref_id}}</b></span><br><span class="order_detail_text">Order Placed {{date('d/m/Y', strtotime($customer->created_at))}} </span></div>
    <div class="col-md-12 px-4 py-2 sm_hide"> <span class="h5 order_detail_title"><b>Order status</b></span>&nbsp;&nbsp;&nbsp;<span class="order_detail_text">{{App\orders::STATUS[$customer->order->status]}}</span></div>
    <div class="col-md-12 py-2 my-1">
        <a href="{{ route('customer_export_order_pdf', $customer->order_id) }}" class="mr-1 mb-1">
        <button class="text-brown border-button m-1 btn btn-md track_border "><b>Download PDF</b></button>
        </a>
        @if($customer->order->is_generate_invoice == true)
            <a href="{{ route('admin.export_invoice_pdf', $customer->order_id) }}" class="mr-1 mb-1">
            <button class="text-brown border-button m-1 btn btn-md track_border "><b>Download Invoice</b></button>
            </a>
        @endif
    </div>
   
    <div class="col-md-12 ">
        <div class="card border-radius shadow_mob_zero">
            @foreach ($customer->orderProducts as $oproduct)
                @php
                $pmeta_data = json_decode($oproduct['meta_data'], true);
                $pmeta_data['campaign_name'];
                // $product_categories = $pmeta_data['category'];
                // $product = $oproduct->product;
                if(isset($pmeta_data['category']))
                {
                    $product_categories = $pmeta_data['category'];
                }
                else{
                    $product_categories = [];
                }
                if(empty($oproduct->product_variant_id))
                {
                    $product = $oproduct->product;
                }
                else {
                    $product = $oproduct->productVariant;
                }
                if($oproduct['discount'] != null)
                {
                $discount_price = $oproduct['price'] * ($oproduct['discount']/100);
                $discounted_price = $oproduct['price'] - $discount_price;
                }
                $img = $objimg = asset('XR/assets/images/placeholder.png');
                $obj_id = "";
                if(count($product->photo) > 0) {
                $img = $product->photo[0]->getUrl('thumb');
                $objimg = asset('XR/assets/images/placeholder.png');
                $obj_id = "";
                } else if(count($product->arobject) > 0) {
                if(!empty($product->arobject[0]->object))
                if(!empty($product->arobject[0]->object->getUrl()))
                $img = $product->arobject[0]->object->getUrl();
                }
                if(count($product->arobject) > 0){
                if(!empty($product->arobject[0]->object))
                if(!empty($product->arobject[0]->object->getUrl())) {
                $objimg = $product->arobject[0]->object->getUrl();
                $obj_id = $product->arobject[0]->id;
                }
                }
                @endphp
                <div class="mx-2 lg_hide">
                    <div class="row mb-1 w-100 p-2" style="min-height:13rem;">
                            <div class="col-lg-3 col-md-3 col-sm-12 d-flex justify-content-center p-1 rounded">
                                @if(count($product->photo) > 1)
                                    <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails h-100 w-100" data-interval="false" data-ride="carousel">
                                        <div class="carousel-inner h-100 w-100" role="listbox">
                                            @foreach($product->photo as $photo)
                                            <div class="carousel-item {{$loop->index == 0?'active':''}} px-2 text-center">
                                                <img class="img-fluid mw-100 h-100 rounded"  src="{{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl('thumb'):$objimg ?? ''}}" style="min-height:13rem;" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="w-100 position-absolute d-flex niceScroll" style="width: 200px; overflow: hidden; display: inline-block; margin: 0px auto; padding: 3px; outline: none;" tabindex="0">
                                            <ol class="carousel-indicators position-relative " style="display:none;">
                                                @foreach($product->photo as $photo)
                                                <li data-target="#carousel-thumb" data-slide-to="{{$loop->index}}" class=" {{$loop->index == 0?'active':''}}" style="width:50px; height:50px;">
                                                    <img class="d-block img-fluid" style="max-width:50px;max-height: 50px;" src="{{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl('thumb'):$objimg ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                                </li>
                                                @endforeach
                                            </ol>
                                        </div>
                                    </div>
                                @else
                                    <img class="img-fluid mw-100 rounded h-100" src="{{$img ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="min-height:13rem;">
                                @endif
                                @if(!empty($obj_id))
                                    <div class="position-absolute" style="top:0;left:0;">
                                        <div class="badge badge-glow badge-info">
                                            <img class="img-fluid" src="{{asset('XR/assets/images/ar.png')}}" style="width: 32px; max-height:32px;">
                                            <h6 class="text-white m-0">Tryon</h6>
                                        </div>
                                    </div>
                                @endif
                                </div>
                                <div class="col-lg-6 col-md-6 col-6 p-2 d-flex flex-column justify-content-md-center">
                                    <div class="item-name text-dark">
                                        <input type="hidden" class="tryonobj" value="{{$objimg ?? ''}}" />
                                        <input type="hidden" class="obj_id" value="{{$obj_id ?? ''}}" />
                                        <h5 ><strong class="order_detail_head">{{$oproduct['name'] ?? 'Untitled'}}</strong></h5>
                                        
                                    </div>
                                    <div class="item-price order_detail_text">
                                        Qty: <span>{{$oproduct['qty'] ?? ''}}</span>
                                    </div>
                                    
                                </div>
                                <div class="col-lg-3 col-md-3 col-6 p-2 d-flex flex-column justify-content-md-center">
                                    <div class="item-wrapper">
                                        <div class="item-rating">
                                            
                                        </div>
                                        <div class="item-cost text-center">
                                            <h5 class="item-price order_detail_head">
                                                {!! $cmeta_data['currency'] !!}
                                                @if($oproduct['discount'] != null)
                                                {{number_format($discounted_price,2)}}
                                                <span class="text-black"><s>{{$oproduct['price']}}</s></span>
                                                <br><span class="text-warning " style="font-size: 80%;">{{$oproduct['discount'] ?? ''}}%</span>
                                                @else
                                                <span>{{$oproduct['price'] ? number_format($oproduct['price'],2) : '00.00'}}</span>
                                                @endif
                                            </h5>
                                        </div>
                                        
                                    </div>
                                    <div class="wishlist bg-white">
                                    </div>
                                </div>
                                
                                    
                    </div>
                </div>
                <div class="mx-2 sm_hide">
                    <div class="row mb-1 w-100 p-2" style="min-height:13rem;">
                            <div class="col-lg-4 col-md-4 col-sm-4 d-flex justify-content-center p-1 rounded" style="width:50%">
                                @if(count($product->photo) > 1)
                                    <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails h-100 w-100" data-interval="false" data-ride="carousel">
                                        <div class="carousel-inner h-100 w-100" role="listbox">
                                            @foreach($product->photo as $photo)
                                            <div class="carousel-item {{$loop->index == 0?'active':''}} px-2 text-center">
                                                <img class="img-fluid mw-100 h-100 rounded"  src="{{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl('thumb'):$objimg ?? ''}}" style="min-height:13rem;" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="w-100 position-absolute d-flex niceScroll" style="width: 200px; overflow: hidden; display: inline-block; margin: 0px auto; padding: 3px; outline: none;" tabindex="0">
                                            <ol class="carousel-indicators position-relative " style="display:none;">
                                                @foreach($product->photo as $photo)
                                                <li data-target="#carousel-thumb" data-slide-to="{{$loop->index}}" class=" {{$loop->index == 0?'active':''}}" style="width:50px; height:50px;">
                                                    <img class="d-block img-fluid" style="max-width:50px;max-height: 50px;" src="{{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl('thumb'):$objimg ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                                </li>
                                                @endforeach
                                            </ol>
                                        </div>
                                    </div>
                                @else
                                    <img class="img-fluid mw-100 rounded h-100" src="{{$img ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="min-height:13rem;">
                                @endif
                                @if(!empty($obj_id))
                                    <div class="position-absolute" style="top:0;left:0;">
                                        <div class="badge badge-glow badge-info">
                                            <img class="img-fluid" src="{{asset('XR/assets/images/ar.png')}}" style="width: 32px; max-height:32px;">
                                            <h6 class="text-white m-0">Tryon</h6>
                                        </div>
                                    </div>
                                @endif
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-12 p-2 d-flex flex-column justify-content-md-center" style="width:50%">
                                    <div class="item-name text-dark">
                                        <input type="hidden" class="tryonobj" value="{{$objimg ?? ''}}" />
                                        <input type="hidden" class="obj_id" value="{{$obj_id ?? ''}}" />
                                        <h5 ><strong class="order_detail_head p-2"><b>{{$oproduct['name'] ?? 'Untitled'}}</b></strong></h5>
                                        
                                    </div>
                                    <div class="item-price order_detail_text p-2">
                                        Qty: <span>{{$oproduct['qty'] ?? ''}}</span>
                                    </div>
                                    
                                    <div class="item-wrapper">
                                        <div class="item-rating">
                                            
                                        </div>
                                        <div class="item-cost ">
                                            <h5 class="item-price order_detail_head">
                                                {!! $cmeta_data['currency'] !!}
                                                @if($oproduct['discount'] != null)
                                                <b>{{number_format($discounted_price,2)}}</b>
                                                <span class="text-black"><s>{!! $cmeta_data['currency'] !!}{{$oproduct['price']}}</s></span>
                                                <br><span class="text-warning bg-warn" style="font-size: 80%;">{{$oproduct['discount'] ?? ''}}%</span>
                                                @else
                                                <span><b>{{$oproduct['price'] ? number_format($oproduct['price'],2) : '00.00'}}</b></span>
                                                @endif
                                            </h5>
                                        </div>
                                        
                                    </div>
                                    <div class="wishlist bg-white">
                                    </div>
                                </div>
                                
                                    
                    </div>
                </div>
                <hr>
            @endforeach
        </div>
    </div>
    
    @if($transaction!=null)
    <div class="col-md-12 py-2">
        <div class="float-left"> <span class="h5 order_detail_title font_size_mob_1"><b>Transaction Details</b></span></div>
        <div class="float-right"> <span class="h5 order_detail_title text-brown font_size_mob_1"><b>Mode:</b></span>&nbsp;&nbsp;&nbsp;<span class="order_detail_text text-brown"><b>{{$transaction->payment_mode}}</b></span></div>
    </div>
    <div class="col-md-12 py-2">
        <div class="card shadow_mob_zero">
            <div class="card-body">
                <div class="row m-3 p-2">
                    <div class="col-md-12  ">
                    <span class="transaction_text">{{ date('d-m-Y', strtotime($transaction->created_at)) }} &nbsp;&nbsp; {{ date('h:i:s A', strtotime($transaction->created_at)) }}</span><br>
                    <span class="transaction_text">Transaction ID #{{$transaction->transaction_id}}</span><br>
                    <span class="transaction_text">Order ID #{{$transaction->order_id}}</span><br>
                    <span class=" order_detail_title">Paid Amount: {!! $transaction['amount']==0?" ":$cmeta_data['currency'].''.$transaction['amount'] !!}</span>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    @endif

    <div class="col-md-12 py-2">
        <div class="float-left"> <span class="h5 order_detail_title"><b>Delivered Address</b></span></div>
    </div>
    <div class="col-md-12 py-2">
        <div class="card shadow_mob_zero">
            <div class="card-body">
                <div class="row m-3 p-2">
                    <div class="col-md-12  ">
                        {{$customer->address1 ?? "" }},
                        @if(isset($customer->address2))
                        {{$customer->address2 ?? "" }},
                        @endif
                        {{$customer->city?? "" }},
                        {{$customer->pincode?? "" }},
                        {{$customer->state ?? "" }},
                        {{$customer->country ?? "" }}.
                    </div>
                </div>
            </div>
        </div>
    </div>            



    <div class="col-md-12 p-4">
        <div class="float_left_desk text_center_mob"> 
            <button class="text-white border-button my-2 btn btn-md brown_back" id="colorbtn" onclick="location.href='{{ route('customer.show.order_status',['id'=>$customer->id,'shareable_link'=> $sLink]) }}';" >Track Order</button><br>
            @if($customer->order->status < 3)
            <button class="text-brown border-button my-2 btn btn-md track_border ordercancel">Cancel Order</button>
            @endif
        </div>
        <div class="float-right mr-5"> 
        <table class="table table-borderless">
            <tr>
                <td class=" pay_detail_text">
                    No. of. Items
                </td>
                <td class="pay_detail_text">
                    {{count($customer->orderProducts)}}
                </td>
            </tr>
            <tr>
                <td class=" pay_detail_text">
                    Sub Total
                </td>
                <td class="pay_detail_text">
                    {!! $cmeta_data['currency'] !!}{{ number_format($cmeta_data['product_total_price'],2) }}
                </td>
            </tr>
            @if(isset($meta_data['shipping_settings']) && empty($meta_data['shipping_settings']['shipping_method'] && isset($cmeta_data['shipping_charges'])))
            <tr>
                <td class=" pay_detail_text">
                    Delivery
                </td>
                <td class="pay_detail_text">
                    {!! $cmeta_data['currency'] !!}{{ number_format($cmeta_data['shipping_charges'] ,2) }}
                </td>
            </tr>
            @elseif(isset($cmeta_data['shipping_charges']))
            <tr>
                <td class=" pay_detail_text">
                    Delivery
                </td>
                <td class="pay_detail_text">
                    {!! $cmeta_data['currency'] !!}{{ number_format($cmeta_data['shipping_charges'] ,2) }}
                </td>
            </tr>
            @endif
            <tr>
                <td class=" pay_detail_text">
                    Total Price
                </td>
                <td class="pay_detail_text">
                    {!! $cmeta_data['currency'] !!}{{ number_format($cmeta_data['total_price'] ,2) }}
                </td>
            </tr>
        </table>
            {{--<div class="detail m-1">
                <span class="detail-title detail-total pay_detail_text">No. of. Items:</span>&nbsp;
                <span class="cart-item-count text-dark pay_detail_text">{{count($customer->orderProducts)}}</span>
            </div>
            
            <div class="detail m-1">
                <span class="detail-title detail-total pay_detail_text">Sub Total:</span>&nbsp;
                <span class="detail-amt total-amt text-dark pay_detail_text">{!! $cmeta_data['currency'] !!}{{ $cmeta_data['product_total_price'] }}</span>
            </div>
            @if(isset($meta_data['shipping_settings']) && empty($meta_data['shipping_settings']['shipping_method'] && isset($cmeta_data['shipping_charges'])))
                <div class="detail m-1">
                    <span class="detail-total pay_detail_text">Delivery:</span>&nbsp;
                    <span class="detail-amt delivery-charge-amt text-dark pay_detail_text">{!! $cmeta_data['currency'] !!}{{ $cmeta_data['shipping_charges'] }}</span>
                </div>
            @endif
            <div class="detail m-1">
                <span class="detail-title detail-total pay_amount_text">Total Price:</span>&nbsp;
                <span class="detail-amt grand-amt text-dark text-bold-700 pay_amount_text" id="totalpay">{!! $cmeta_data['currency'] !!}{{ $cmeta_data['total_price'] }}</span>
            </div> --}}
        </div>
    </div>
</div >
    
    

    

@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.6.2/viewer.min.js" integrity="sha512-VzJLwaOOYyQemqxRypvwosaCDSQzOGqmBFRrKuoOv7rF2DZPlTaamK1zadh7i2FRmmpdUPAE/VBkCwq2HKPSEQ==" crossorigin="anonymous"></script>
@parent
<script>

    var url = "{{route('customer.order.cancel')}}"
    console.log(url)
    // console.log(url);
function show(x,statusContent){
    var order_data = JSON.parse(x)
    $('.remark').css("display", "none");
    $('.remarks-'+order_data.status).css("display", "block");
    var remarkhtml = "<b>"+statusContent+":</b>";
    $("#showremarks").html(remarkhtml)
 }

 var y='';

 function showtrack(x){
    $('.trackvalue_'+y).css("display", "none");
    y=x;
    $('.lastkey').css("display", "none");
    $('.trackvalue_'+x).css("display", "block");
 }

 $(document).on('click',".ordercancel",function(){

    swal.fire({
        icon:'warning',
        title:'Write the comments :',
        html:'<div>To cancel the order : </div>'+
      '<textarea row="5" cols="40" id="comment"></textarea></div> <br>',
        showCancelButton:true,
        cancelButtonText:"Cancel",
        confirmButtonText:"Ok",
        cancelButtonColor: "#DD6B55",
        preConfirm: function() {
            return new Promise((resolve, reject) => {
                resolve({
                    remarks: $('#comment').val(),
                });
            });
        }
    }).then(function(result){

        if(result.value)
        {
            console.log(result.value.remarks)
           if(result.value.remarks != '') {
               console.log(url)

            $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        order_id:{{$customer->order->id}},
                        sharablelink:"{{$sLink}}",
                        remarks: result.value.remarks,
                        status: 6,
                    },
                    success:function(data){
                        console.log(data);
                        Swal.fire({
                            type: "success",
                            title: 'order cancelled',
                            text: 'Your order is cancelled',
                            confirmButtonClass: 'btn btn-success',
                        }).then((result) => {
                            window.location.reload();
                    });
                    },
            });
           }
           else{
            Swal.fire({
                icon: 'error',
                title: 'write something in comment'
                });
           }
        }
    })
 });
</script>
@endsection
