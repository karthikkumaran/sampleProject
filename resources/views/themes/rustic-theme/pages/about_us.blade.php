@extends('themes.'.$meta_data['themes']['name'].'.layouts.store')
@section('content')
@php
$agent=new \Jenssegers\Agent\Agent();
    if($userSettings->is_start == 1) {
        if(!empty($userSettings->customdomain)){
        $homeLink = '//'.$userSettings->customdomain;
        }else if(!empty($userSettings->subdomain)) {
        $homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
        }else{
        $homeLink = route('mystore.published',[$userSettings->public_link]);
        }
    }else{
        $homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
    }
@endphp
<div class="">
        <div class="row">
            <div class="col-12 ">

                    <img class="img-responsive w-100 h-100 lg_hide" src="{{asset('themes/rustic-theme/images/png/image_Mountain.png')}}" alt="">
                    <img class="img-responsive w-100 h-100 sm_hide" src="{{asset('themes/rustic-theme/images/png/Banner_about_mobile.png')}}" alt="">
                    
            </div>
        </div>
</div>
<div class="row margin_mob_y_3 margin_desk_3 pad_desk_3  ">
            <div class="col-lg-12 col-md-12 card margin-bot-zero p-0">
            <img class="img-responsive w-100 h-100 lg_hide" src="{{asset('themes/rustic-theme/images/png/crop_grown.png')}}" alt="">
            <img class="img-responsive w-100 h-100 sm_hide" src="{{asset('themes/rustic-theme/images/png/cropdrown_mobile.png')}}" alt="">
            </div>
            <div class="col-lg-12 col-md-12 card margin-bot-zero p-5 greydiv shadow_zero">
            <div class="m-auto">
              <h3 class="consume_title text-center"><b>Uniqueness of Rustic</b></h3>
              <p class="unique_text py-1">
                Four generations of farming practices with around 200 years of combined
                experience connecting tradition with the present, optimizing natural and digital experiences where
                women entrepreneurial collaborations play an important role, is what makes RUSTIC unique.<br><br>

                The sheer beauty of the cloud-laden hills of Coorg, drenched in the fragrance of wet soil and
                cocooned in nature’s lush green cradle, is an experience to behold. Crops such as spices, paddy,
                coffee, fruits and honey that we grow naturally and hand-pick from our land or ‘Thota’ as we lovingly
                call it, are chemical and preservative free. Being a part of the Western Ghats, Coorg has been
                bestowed the UNESCO World Heritage tag which is taken very seriously by the nature worshipping
                hill tribe who revere their land, soil and the high altitude that turns the produce nutritious, rich in
                flavour and intensely aromatic. Very Natural.
              </p>   
            </div>
            </div>
</div>


<div class="row margin_desk_x_1 pad_desk_3 margin_mob_x_1 pad_mob_1">
    <h1 class="text-center mb-4 consume_title">Becoming aware about what you consume </h1>
    <p class="unique_text py-1">
    As a first step to respecting yourself, indulge in your body, mind
    and soul to bring out the best in you. Nourish it with healthy, nutritious, mindfully gathered spices,
    beverages, rice, fruits and honey from RUSTIC. You are what you consume sensorily and physically. Make self-care go beyond a trend to becoming a part of you. Just like the ancient scriptures and our ancestors handed down to us, become aware of what you consume to be energized and aligned  within. RUSTIC’s food products are hand-picked with immense love just for your
    </p>
</div>

<div class="row margin_desk_x_3 pad_desk_3 ">
    <div class="col-lg-6 col-md-6 col-sm-12 p-2">
        <div class="card cardborder shadow_zero margin-bot-zero" >
        <img class="card-img-top" src="{{asset('themes/rustic-theme/images/png/soil_compost.png')}}" height="300">
        <div class="card-body p-4 greydiv min_height_50_em">
            <h2 class=" my-2 compost_title">Soil Composting</h2>
            <p class="unique_text p-1">
            One of the natural processes happening in our hills is soil composting- the
            decomposition of the soil making it nutrient-rich. We have been growing our produce with this
            ‘black-gold’ for generations and this has been perfect for our produce. With consistent aeration,
            moisture and temperature, it has a balance of green, brown, air and water. The everyday sustainable
            practices that Nature carries out at the hills have been embraced by the farmers making it a daily
            daily routine in the hills.
            </p>
        </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-12 p-2">
        <div class="card cardborder shadow_zero margin-bot-zero">
        <img class="card-img-top" src="{{asset('themes/rustic-theme/images/png/element.png')}}" height="300">
        <div class="card-body p-4 greydiv min_height_50_em">
            <h2 class=" my-2 compost_title">Five Elements</h2>
            <p class="unique_text p-1">
                Elements: Rustic- Earth to Soul is a women entrepreneurial dream that has taken flight,
                connecting your taste buds to the food sourced from the land where the Five elements of Nature
                (Panchabhoothas) reside. RUSTIC handpicks products that flourish in the presence of these five
                elements revered rich Soil, warm Sun, flowing Water, cool Breeze and abundant azure Space. Thosewho have experienced our products share that they are distinct, flavourful, aromatic and rich in
                texture.
            </p>
        </div>
        </div>
    </div>
    
</div>

<div class="row">
  <div class="container-fluid mt-5">
      
          <h1 class="text-center mb-4 life_rus_head">Life of Rustic Natives we work with</h1>
  
      <div id="carouselExampleIndicatorsblog" class="carousel slide" data-ride="carousel">
          
                    
                          <div class="row align-items-center">

                              <div class="col-md-1   ">

                                  <div  class="carousel-indicators mt-5 position-relative lg_hide">
            
                                      <div   data-target="#carouselExampleIndicatorsblog" data-slide-to="0" ><img  src="{{asset('themes/rustic-theme/images/svg/bi_arrow-left-circle-fill.svg')}}" alt="">
                                      </div>
                  
                                  </div>
                                  
                              </div>
                              
                              




              
            <div class="col-md-10   ">
            
              <div class="carousel-inner">
                      
                  
                  
                  
                  <div class="carousel-item active ">
                      <div class="row ">
                          <div class="col-md-6 col-lg-4 my-2 lg_hide">
                              <div class="card shadow">
                                  <img src="{{asset('themes/rustic-theme/images/png/life_rust_1.png')}}" class="card-img-top img-responsive" alt="" height="500">
                                      <!-- <div class="card-body">
                                          <span class="card-text">ceylon cinnamon to spice up your healthy life- Find your true friend</span>
                                      </div> -->
                              </div>
                          </div>
                          
                          <div class="col-md-6 col-lg-4 my-2 ">
                              <div class="card shadow">
                                  <img src="{{asset('themes/rustic-theme/images/png/life_rust_3.png')}}" class="card-img-top img-responsive" alt="" height="500">
                                      <!-- <div class="card-body">
                                          <span class="card-text">Why should you buy organic products ? is this just a trend ?</span>
                                      </div> -->
                              </div>
                          </div>
                          
                          <div class="col-md-6 col-lg-4 my-2 lg_hide tabl_hide">
                              <div class="card shadow" >
                                  <img src="{{asset('themes/rustic-theme/images/png/life_rust_2.png')}}" class="card-img-top img-responsive" alt="" height="500">
                                      <!-- <div class="card-body">
                                          <span class="card-text">Benefits of coffee for your weight loss use of coffee    </span>
                                      </div> -->
                              </div>
                          </div>
                      </div>
                  </div>



                          <div class="carousel-item  ">
                              <div class="row  ">
                                  <div class="col-md-6 col-lg-4 my-2 tabl_hide">
                                      <div class="card shadow">
                                          <img src="{{asset('themes/rustic-theme/images/png/life_rust_1.png')}}" class="card-img-top img-responsive" alt="" height="500">
                                              <!-- <div class="card-body">
                                                  <span class="card-text">ceylon cinnamon to spice up your healthy life- Find your true friend</span>
                                              </div> -->
                                      </div>
                                  </div>
                                  
                                  <div class="col-md-6 col-lg-4 my-2 lg_hide">
                                      <div class="card shadow" >
                                          <img src="{{asset('themes/rustic-theme/images/png/life_rust_3.png')}}" class="card-img-top img-responsive" alt="" height="500">
                                              <!-- <div class="card-body">
                                                  <span class="card-text">Why should you buy organic products ? is this just a trend ?</span>
                                              </div> -->
                                      </div>
                                  </div>
                                  
                                  <div class="col-md-6 col-lg-4 my-2 lg_hide">
                                      <div class="card shadow">
                                          <img src="{{asset('themes/rustic-theme/images/png/life_rust_2.png')}}" class="card-img-top img-responsive" alt="" height="500">
                                              <!-- <div class="card-body">
                                                  <span class="card-text">Benefits of coffee for your weight loss use of coffee </span>
                                              </div> -->
                                      </div>
                                  </div>
                              </div>
                          </div>

                  
                  
                  </div>
            </div>      
                        
                          <div class="col-md-1  ">

                              <div  class="carousel-indicators mt-3 position-relative lg_hide">
        
                                  <div   data-target="#carouselExampleIndicatorsblog" data-slide-to="1" ><img  src="{{asset('themes/rustic-theme/images/svg/bi_arrow-right-circle-fill.svg')}}" alt="">
                                  </div>
              
                              </div>
                              
                          </div>
                        <ol  class="carousel-indicators p-5 mx-0 position-relative text-center sm_hide">
                            <li   data-target="#carouselExampleIndicatorsblog" style="background-color: transparent !important;" data-slide-to="0" ><img class="position-absolute  top-50 end-50 translate-middle-x" src="{{asset('themes/rustic-theme/images/svg/bi_arrow-left-circle-fill.svg')}}" alt=""></li>
                            <li  data-target="#carouselExampleIndicatorsblog" style="background-color: transparent !important;" data-slide-to="1"><img class="position-absolute mx-5 top-60 start-50 translate-middle-x" src="{{asset('themes/rustic-theme/images/svg/bi_arrow-right-circle-fill.svg')}}" alt=""></li>
                        </ol>
                          
          
      </div> 
  </div>
</div>
  
@endsection