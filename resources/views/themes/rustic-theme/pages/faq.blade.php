@extends('themes.' . $meta_data['themes']['name'] . '.layouts.store')
@section('content')
    @php
    if ($userSettings->is_start == 1) {
        $shareableLink = $userSettings->public_link;
    } else {
        $shareableLink = $userSettings->preview_link;
    }
    @endphp
    <div id="colordiv" class="">

        <div class="row lg_hide">
            <div class="container-fluid my-5">
                <h4 class="text-center mb-4 fir">FAQs</h4>
                <h1 class="text-center mb-4 two"> Ask us anything </h1>
                <h3 class="text-center mb- thr">
                    Have any questions? We're here to assist you.
                </h3>
                <br>
            </div>
        </div>

        <div class="row sm_hide">
            <div class="container-fluid my-5">

                <h4 class="text-center mb-4 fir">FAQs</h4>
                <h1 class="text-center mb-4 two"> Ask us anything </h1>
                <h3 class="text-center mb- thr">
                    Have any questions? We're here to assist you.
                </h3>
                <br>
            </div>
        </div>
    </div>
    <div class="">
        <div class="row titlesub ">

            <div class="col-lg-4 col-md-6 col-sm-12 p-4">
                <div class="row m-5">
                    <div class="m-1 p-1 ">
                        <img class="background" src="{{ asset('themes/rustic-theme/images/png/icon_mail.png') }}"
                            alt="">
                    </div>
                    <h4 class="title">How do I change my account email?</h4>
                    <h5 class="sub">
                        You can log in to your account and change it from your Profile > Edit Profile. Then go to the
                        general
                        tab to
                        change your email.
                    </h5>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-12 p-4">
                <div class="row m-5">
                    <div class="m-1 p-1">
                        <img class="background" src="{{ asset('themes/rustic-theme/images/png/icon_credit-card.png') }}"
                            alt="">
                    </div>
                    <h4 class="title">What should I do if my payment fails?</h4>
                    <h5 class="sub">
                        If your payment fails, you can use the (COD) payment option, if available on that order. If your
                        payment
                        is
                        debited from your account after a payment failure, it will be credited back within 7-10 days.
                    </h5>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 p-4">
                <div class="row m-5">
                    <div class="m-1 p-1">
                        <img class="background" src="{{ asset('themes/rustic-theme/images/png/icon_slash.png') }}"
                            alt="">
                    </div>
                    <h4 class="title">What is your cancellation policy?</h4>
                    <h5 class="sub">
                        You can now cancel an order when it is in packed/shipped status. Any amount paid will be credited
                        into
                        the
                        same payment mode using which the payment was made
                    </h5>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-sm-12 p-4">
                <div class="row m-5">
                    <div class="m-1 p-1">
                        <img class="background" src="{{ asset('themes/rustic-theme/images/png/icon_truck.png') }}"
                            alt="">
                    </div>

                    <h4 class="title">How do I check order delivery status?</h4>
                    <h5 class="sub">
                        Please tap on “My Orders” section under main menu of App/Website/M-site to check your order status.
                    </h5>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-12 p-4">
                <div class="row m-5">
                    <div class="m-1 p-1">
                        <img class="background" src="{{ asset('themes/rustic-theme/images/png/icon_dollar-sign.png') }}"
                            alt="">
                    </div>

                    <h4 class="title">What is Instant Refunds?</h4>
                    <h5 class="sub">
                        Upon successful pickup of the return product at your doorstep, Myntra will instantly initiate the
                        refund
                        to
                        your source account or chosen method of refund. Instant Refunds is not available in a few select pin
                        codes
                        and for all self ship returns.
                    </h5>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-12 p-4">
                <div class="row m-5">
                    <div class="m-1 p-1">
                        <img class="background" src="{{ asset('themes/rustic-theme/images/png/icon_tag.png') }}"
                            alt="">
                    </div>

                    <h4 class="title">How do I apply a coupon on my order?</h4>
                    <h5 class="sub">
                        You can apply a coupon on cart page before order placement. The complete list of your unused and
                        valid
                        coupons will be available under “My Coupons” tab of App/Website/M-site.
                    </h5>
                </div>
            </div>
        </div>

        <div class="text-center m-5">
            <h2 class="midd"> Contact us</h2>
            <h4 class="us">
                If you have more question or feedback,
                <br>
                Please contact us.
            </h4>
        </div>
        <div id="colordiv" class="mb-2">

            <div class="row">
                <div class="d-flex justify-content-center my-5 ">
                    <form action="#" name="contact_form" class=" faq_form">
                        @csrf
                        <div class="form-group">
                            <label class="text-left" for="name">Name</label><br>
                            <input type="text" id="name" class="form-control required" name="name">
                        </div>
                        <div class="form-group ">
                            <label class="text-left" for="name">Mobile</label><br>
                            <input type="number" id="mobile" class="form-control required" name="mobile"
                                maxlength="10">
                        </div>
                        <div class="form-group">
                            <label class="text-left" for="name">Email</label><br>
                            <input type="email" id="email" class="form-control required" name="email"
                                value="">
                        </div>
                        <div class="form-group">
                            <label class="text-left" for="name">Message</label><br>
                            <textarea name="textmessage" id="textarea" class="form-control required" cols="46" rows="10"></textarea>
                        </div>
                        <div>
                            <button type="button" class="btn bg-maroon btn1" onclick="sendMessage()">Send
                                Message</button>
                        </div>
                    </form>
                </div>
            </div>
            
        </div>
        <br>
        <br><br><br><br><br><br>
    </div>
@endsection

<script>
    function sendMessage() {

        var data = ($('form[name=contact_form]').serialize());
        $.ajax({
            url: "{{ route('store.storeContactFaq', [$shareableLink]) }}",
            type: "POST",
            data: data,
            success: function(response) {
                window.location.reload();
            }
        });

    }
</script>





<style>
    .form-group {
        width: 67%;
        margin-left: 60px;
    }

    .form {
        width: 35%;
        margin-left: 3%;
    }

    .bg-maroon {
        background-color: #6C4E4F !important;
        color: #fff !important;
        margin-left: 89px;
        width: 50%;
    }

    .background {
        background: #E7E3E3;
        border-radius: 17.6506px;
    }

    .box {
        padding: 120px !important;
    }

    .title {
        width: 265.79px;
        font-style: normal;
        font-weight: 500;
        font-size: 14.7088px;
        line-height: 22px;
        color: #432F2F;
    }

    .sub {
        width: 265.79px;
        height: 45.89px;
        font-style: normal;
        font-weight: 500;
        font-size: 11.767px;
        line-height: 18px;
        color: #432F2F;

    }

    .fir {
        left: calc(50% - 1440px/2);
        top: 91px;
        color: #6C4E4F;

    }

    .position-relative .form-control {
        display: flex;
        flex-direction: row;
        align-items: center;
        padding: 8.82528px 11.767px;
        gap: 5.88px;
        margin-left: 578px;
        width: 235.34px;
        height: 35.65px;
        background: #FFFFFF;
        box-shadow: 0px 0.73544px 1.47088px rgb(16 24 40 / 5%);
        border-radius: 5.88352px;
    }

    .form-control-position {
        margin-left: 408px;
        margin-top: -25px;
        top: 2px;
        right: 0;
        z-index: 2;
        display: block;
        width: 2.5rem;
        height: 2.5rem;
        line-height: 2.5rem;
        text-align: center;
    }

    .two {
        font-family: 'Inter';
        font-style: normal;
        font-weight: 600;
        font-size: 35.3011px;
        line-height: 44px;
        text-align: center;
        letter-spacing: -0.02em;
        color: #6C4E4F;

    }

    .required1 {
        margin-bottom: 20px;
    }

    .thr {
        font-family: 'Inter';
        font-style: normal;
        font-weight: 400;
        font-size: 14.7088px;
        line-height: 22px;
        text-align: center;
        color: #6C4E4F;

    }

    .mob {
        margin-left: 10%
    }

    .midd {
        height: 44px;
        left: 624.32px;
        font-style: normal;
        font-weight: 700;
        color: #000000;
    }

    .us {
        height: 48px;
        font-family: 'Inter';
        font-style: normal;
        font-weight: 600;
        font-size: 18px;
        line-height: 22px;
        color: #090808;
    }

    .text-left {
        font-family: 'Inter';
        font-style: normal;
        font-size: 16px;
        align-items: center;
        letter-spacing: -0.011em;
        color: #000000;
    }

    .btn1 {
        font-family: 'Proxima Nova';
        font-style: normal;
        font-weight: 700;
        font-size: 18px;
        line-height: 22px;
        margin-left: 102px;
        color: #FFFFFF;
    }

    .btn2 {
        width: 60%;
        margin-left: 70px;
        margin-top: 20px;
    }

    /* mobile */
    @media (max-width: 767.98px) {

        .form {
            width: 100%;
            margin-left: 3%;
        }

    }
</style>
