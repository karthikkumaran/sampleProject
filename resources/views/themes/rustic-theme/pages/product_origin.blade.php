@extends('themes.'.$meta_data['themes']['name'].'.layouts.store')
@section('content')
@php
$agent=new \Jenssegers\Agent\Agent();
if($userSettings->is_start == 1) {
if(!empty($userSettings->customdomain)){
$homeLink = '//'.$userSettings->customdomain;
}else if(!empty($userSettings->subdomain)) {
$homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
}else{
$homeLink = route('mystore.published',[$userSettings->public_link]);
}
}else{
$homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
}
@endphp
<div>
<div id="greendiv" class="container-fluid lg_hide tabl_hide">

            <div class="container-fluid">
                 <div class="row greendiv">
         
                    
                    
            <div class="col-md-6 position-relative z_high">
                        <div>
                            <h1 id="banner-text" class="orgin_title position-absolute mx-2 h1 top-25 start-10 translate-middle-y text-white text-center">
                            Essence of Rustic From Coorg
                            </h1>      
                            <p class="position-absolute mx-2 top-50 start-10 translate-middle-y text-white text-left orgin_text">
                            At Rustic, we aim to bring the serenity and aura of Coorg to you <br>
                            through pure and organic products sourced from native growers.<br>
                             We affirm 100% authenticity and purity of every granule of our<br>
                              products because we source them only from growers who share <br>
                              our view of delivering goodness, warmth, and value. <br>        
                            </p>                  
                        </div>
            </div>
        
            <div class="col-md-6 h-100">  
             
                    <img id="banimg" class="img-responsive float-right" src="{{asset('themes/rustic-theme/images/png/origin_photo.png')}}" alt="">
                
              </div>
 
        </div>
    </div>
</div> 
<div class=" sm_hide tabl_show">

        <div class="row ">
                                            
                 
            <div class="col-lg-12 col-md-12 col-sm-12 w-100 h-100">  
             
                    <img id="" class="img-responsive  w-100 h-100" src="{{asset('themes/rustic-theme/images/png/origin_photo.png')}}" alt="">
                
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 greendiv py-4" >
                <div>
                    <h1 id="banner-text" class="orgin_title  mx-2 h1   text-white text-left px-2">
                    Essence of Rustic From Coorg
                    </h1>      
                    <p class=" mx-2   text-white text-left px-2 orgin_text">
                    At Rustic, we aim to bring the serenity and aura of Coorg to you <br>
                    through pure and organic products sourced from native growers.<br>
                        We affirm 100% authenticity and purity of every granule of our<br>
                        products because we source them only from growers who share <br>
                        our view of delivering goodness, warmth, and value. <br>        
                    </p>                  
                </div>
 
        </div>
    </div>
    
</div> 
<div class="row text-center my-4">
   <div class="col-md-12 col-lg-12 col-sm-12 h-50">

      <img class="img-responsive w-50 h-50 " src="{{asset('themes/rustic-theme/images/png/coorg_map.png')}}" alt="">

   </div>
</div>    
<div class="row mt-5 mx-3 p-2">
    <h3 class="orgin_title text-left p-2"><b>Coorg</b></h3><br><br>
    <p class="orgin_text">
    Many centuries ago, Kodag was known as Kudumalai- which meant steep mountains. Coorg or Kodag as it is locally known, is called the Scotland of India as the Scottish who settled here during the British rule are said to have started coffee plantations here as they found a lot of similarities between Coorg and Scotland - in the mountainous terrain, fertile soil, farming practices and the personality of the warrior community members. This soil and the verdant ecology ensured that the produce grown on this land was unique in flavour, aroma and texture.
    <br><br>                                    

    The highest elevation in Coorg or Kodag is Thadiyandamolu (meaning broad-based hills) Kundu (mountain) which is 5,740 ft. above mean sea level and the lowest elevation is 3,000 ft. The second highest peak in Kodagu is Pushpagiri which is 5,627 ft, above mean sea level. The anglicised version of Kodag is Coorg. River Cauvery is the goddess worshipped by the residents of the region and the river that bubbles out at Talacauvery flows down the hills and is joined at Bhagamandala by Rivers Kannike, Sujyothi. This place where the three rivers meet Trivenisangama is a place of worship for people across the Catchment region Kodag followed by Hunsur, Mysore, Mandya and Bangalore from where the river flows all the way to the Delta region of Tamilnadu.  River Cauvery which is an 805 km long river flows through Karnataka, Tamilnadu, Kerala, and the Union Territory of Puducherry making it a river of life for those who benefit from it
    <br><br>                                    

    Igguthappa Kundu is another important place in Kodag. ‘Iggu’ means God, ‘appa’ means father and ‘Kundu’ means hill). This is a place of worship for the Nature worshipping tribe of the land the Kodavas.
    <br><br>                                    


    The sheer beauty of the cloud-laden hills of Coorg, drenched in the fragrance of wet soil and cocooned in nature’s lush green cradle, is an experience to behold. Crops such as spices, paddy, coffee, fruits and honey that we grow naturally and hand-pick from our land or ‘Thota’ as we lovingly call it, are chemical and preservative free. Being a part of the Western Ghats, Coorg has been bestowed the UNESCO World Heritage tag which is taken very seriously by the nature worshipping hill tribe who revere their land, soil and the high altitude that turns the produce nutritious, rich in flavour and intensely aromatic. Very Natural.
    <br><br>                                                       

</div> 
<div class="row my-5 margin_desk_x_1 ">
    <h3 class="orgin_title text-left margin_desk_x_1 margin_mob_x_15 p-2"><b>Coorg Culture</b></h3><br><br>
    <img class="img-responsive w-100 h-100  my-2" src="{{asset('themes/rustic-theme/images/png/coorg_culture.png')}}" alt="">
</div>  
<div class="row my-1 mx-3 p-2">  
    <p class="orgin_text">
        
     Kodavas, the residents of Kodag or Coorg are wedded to the soil. They are nature worshippers who worship trees, the rivers, the sun, the moon, the earth and even the paddy that they grow. They worship their Karonas. Karonas are their ancestors, who are, according to Kodavas, ‘those who are responsible for the birth of each Kodava’. Each family has their own ‘Karona’ and Kodavas are ancestor worshippers. Their first ancestor is the founder of their family - ie., Okka. He is respected as God.
     <br><br>                                   
     Kodavas give a lot of importance to dancing in the temples, mandh (open spaces of worship, weddings, etc). They have their own traditional Dudi Kottu (percussion instruments). The traditional dances by men are Kombattu, Kolatu, Chouri aat, Bolekat, Kathi aat, Pareya kali and so on. The women dance the Ummat aat for a set of devotional songs. After a function or a strenuous day, Kodavas dance Kodavaat (Kodava dance) to the beats of the drums almost 
     <br><br>                            
    </p>                                                                        
</div>                   
</div>
@endsection