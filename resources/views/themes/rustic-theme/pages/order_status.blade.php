@extends($layout)
@section('content')
<div class="card my-4 mx-3 border-radius">
        <div class="card-title p-3 text-white border-top-radius" id="colorbtn">
            Your Tracking<br>
            #{{$customer->order->transactions->transaction_id}}
        </div>
        <div class="card-body">
                <div class="row mt-2 mb-2">
                    <div class="col-md-6">
                            <div class="d-flex flex-row justify-content-between align-items-center order">
                                <div class="d-flex flex-column order-details"><h3>Order Status</h3></div>
                            </div>
                        </div>
                    <div class="col-md-6 pr-2 text-right">

                    {{--@if($customer->order->status < 3)
                    <button class="btn btn-danger ordercancel" >Cancel</button>
                    @endif--}}
                        </div>
                    </div>
                </div>

                <div class="row mt-1 mb-1 pl-2 pr-2">
                    <div class="col-md-12 lg_hide">
                            <div class="d-flex flex-row justify-content-between align-items-center">

                                @foreach($orderstatus as $key => $value)
                                    <div class="d-flex flex-column align-items-start">{{App\orders::STATUS[$value->status]}}</div>
                                @endforeach

                                @if($customer->order->status == 5 && empty(App\orders::STATUS[$value->status]))
                                <div class="d-flex flex-column align-items-start">{{App\orders::STATUS[5]}}</div>
                                @elseif($customer->order->status == 6 && empty(App\orders::STATUS[$value->status]))
                                <div class="d-flex flex-column align-items-start">{{App\orders::STATUS[6]}}</div>
                                @elseif($customer->order->status == 7 && empty(App\orders::STATUS[$value->status]))
                                <div class="d-flex flex-column align-items-start">{{App\orders::STATUS[7]}}</div>
                                @else
                                    @foreach(App\orders::STATUS as $key => $label)
                                        @if($key > $customer->order->status && $key <= 4)
                                        <div class="d-flex flex-column align-items-start">{{ $label }}</div>
                                        @endif
                                    @endforeach
                                @endif
                                
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center">

                                @foreach($orderstatus as $key => $value)
                                    @if($key == 0)
                                        @if($key != count($orderstatus)-1)
                                            <span class="cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')"><img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Order Receiver.png')}}" alt=""></span>
                                        @else
                                            <span class="d-flex justify-content-center align-items-center cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')"><img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Order Receiver.png')}}" alt=""></span>
                                        @endif
                                    @elseif($key != count($orderstatus)-1)
                                        @if($value->status < 5)
                                        <hr class="flex-fill track-line align-items-start">
                                        <span class=" cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')">
                                            @if($value->status == 1)
                                            <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Pending.png')}}" alt="">
                                            @elseif($value->status == 2)
                                            <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Accepted.png')}}" alt="">
                                            @elseif($value->status == 3)
                                            <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Shipped.png')}}" alt="">
                                            @elseif($value->status == 4)
                                            <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Delivered.png')}}" alt="">
                                            @endif
                                        </span>
                                        @else
                                        <hr class="flex-fill failtrack-line align-items-start">
                                        <span class="d-flex justify-content-center align-items-center  cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')">
                                            @if($value->status == 5)
                                                <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Rejected.png')}}" alt="">
                                            @elseif($value->status == 6)
                                                <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Cancel.png')}}" alt="">
                                            @elseif($value->status == 6)
                                                <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Failed.png')}}" alt="">
                                            @endif
                                        </span>
                                        @endif

                                    @else
                                        @if($customer->order->status < 5)
                                            <hr class="flex-fill track-line">
                                            <span class="d-flex justify-content-center align-items-center   cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')">
                                                @if($value->status == 1)
                                                <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Pending.png')}}" alt="">
                                                @elseif($value->status == 2)
                                                <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Accepted.png')}}" alt="">
                                                @elseif($value->status == 3)
                                                <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Shipped.png')}}" alt="">
                                                @elseif($value->status == 4)
                                                <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Delivered.png')}}" alt="">
                                                @endif
                                            </span>
                                        @else
                                            <hr class="flex-fill failtrack-line align-items-start">
                                            <span class="d-flex justify-content-center align-items-center  cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')">
                                                @if($value->status == 5)
                                                    <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Rejected.png')}}" alt="">
                                                @elseif($value->status == 6)
                                                    <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Cancel.png')}}" alt="">
                                                @elseif($value->status == 6)
                                                    <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Failed.png')}}" alt="">
                                                @endif
                                            </span>
                                        @endif
                                    @endif
                                @endforeach
                                @foreach(App\orders::STATUS as $key => $label)
                                    @if($key > $customer->order->status && $key <= 4)
                                    <hr class="flex-fill untrack-line align-items-start">
                                    <span class="">
                                        @if($key == 1)
                                            <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Pending_not active.png')}}" alt="">
                                        @elseif($key == 2)
                                            <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/accepted_not active.png')}}" alt="">
                                        @elseif($key == 3)
                                            <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Shipped_not active.png')}}" alt="">
                                        @elseif($key == 4)
                                            <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Delivered_not active.png')}}" alt="">
                                        @endif
                                    </span>
                                </span>
                                    @endif
                                @endforeach

                                @php
                                     $statusvalue =  App\orders::STATUS[$customer->order->status] ;
                                 @endphp

                               
                                </div>
                            <div class="d-flex flex-row justify-content-between align-items-center">
                                @foreach($orderstatus as $key => $value)
                                <div class="d-flex flex-column align-items-start">
                                    @php
                                        $s= $value->created_at ;
                                        $date = $s->format('m/d/Y');
                                        $time = date("h:i:s A",strtotime($s));
                                    @endphp
                                    {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                    <br>
                                    {{ $time }}
                                </div>
                                @endforeach
                                @if($customer->order->status == 5 && empty(App\orders::STATUS[$value->status]))
                                        @php
                                             $s= $value->created_at ;
                                             $date = $s->format('m/d/Y');
                                             $time = date("h:i:s A",strtotime($s));
                                         @endphp
                                    {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                    <br>
                                    {{ $time }}
                                @elseif($customer->order->status == 6 && empty(App\orders::STATUS[$value->status]))
                                        @php
                                             $s= $value->created_at ;
                                             $date = $s->format('m/d/Y');
                                             $time = date("h:i:s A",strtotime($s));
                                         @endphp
                                    {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                    <br>
                                    {{ $time }}
                                    @elseif($customer->order->status == 7 && empty(App\orders::STATUS[$value->status]))
                                        @php
                                             $s= $value->created_at ;
                                             $date = $s->format('m/d/Y');
                                             $time = date("h:i:s A",strtotime($s));
                                         @endphp
                                    {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                    <br>
                                    {{ $time }}
                                    @else
                                        @foreach(App\orders::STATUS as $key => $label)
                                        @if($key > $customer->order->status && $key <= 4)
                                        <span>-</span>
                                        @endif
                                        @endforeach
                                @endif
                                </div>

                                <!-- <div class="d-flex flex-column justify-content-center"><span>15 Mar</span><span>Order placed</span></div>
                                <div class="d-flex flex-column justify-content-center align-items-center"><span>15 Mar</span><span>Order Dispatched</span></div>
                                <div class="d-flex flex-column align-items-center"><span>15 Mar</span><span>Out for delivery</span></div>
                                <div class="d-flex flex-column align-items-end"><span>15 Mar</span><span>Delivered</span></div> -->
                            </div>
                    </div>
                    <div class="col-md-12 sm_hide">
                        <div class="row">
                            <div class="col-sm-4" style="width:33%">
                                @foreach($orderstatus as $key => $value)
                                    @if($key == 0)
                                        @if($key != count($orderstatus)-1)
                                            <div class="cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')"><img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Order Receiver.png')}}" alt=""></div>
                                        @else
                                            <div class="d-flex justify-content-center align-items-center cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')"><img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Order Receiver.png')}}" alt=""></div>
                                        @endif
                                        
                                    @elseif($key != count($orderstatus)-1)
                                        @if($value->status < 5)
                                        
                                        <div class="m-auto track-vertical-line">
                                            <span class="vr"></span>
                                        </div>
                                        <div class=" cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')">
                                            @if($value->status == 1)
                                            <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Pending.png')}}" alt="">
                                            @elseif($value->status == 2)
                                            <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Accepted.png')}}" alt="">
                                            @elseif($value->status == 3)
                                            <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Shipped.png')}}" alt="">
                                            @elseif($value->status == 4)
                                            <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Delivered.png')}}" alt="">
                                            @endif
                                        </div>
                                        @else
                                        <div class="m-auto failtrack-vertical-line">
                                            <span class="vr"></span>
                                        </div>
                                        <div class="d-flex justify-content-center align-items-center  cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')">
                                            @if($value->status == 5)
                                                <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Rejected.png')}}" alt="">
                                            @elseif($value->status == 6)
                                                <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Cancel.png')}}" alt="">
                                            @elseif($value->status == 6)
                                                <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Failed.png')}}" alt="">
                                            @endif
                                        </div>
                                        @endif
                                    @else
                                        @if($customer->order->status < 5)
                                            <div class="m-auto track-vertical-line" >
                                                <span class="vr"></span>
                                            </div>
                                            <div class="d-flex justify-content-center align-items-center   cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')">
                                                @if($value->status == 1)
                                                <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Pending.png')}}" alt="">
                                                @elseif($value->status == 2)
                                                <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Accepted.png')}}" alt="">
                                                @elseif($value->status == 3)
                                                <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Shipped.png')}}" alt="">
                                                @elseif($value->status == 4)
                                                <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Delivered.png')}}" alt="">
                                                @endif
                                            </div>
                                        @else
                                            <div class="m-auto failtrack-vertical-line">
                                                <span class="vr"></span>
                                            </div>
                                            <div class="d-flex justify-content-center align-items-center  cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')">
                                                @if($value->status == 5)
                                                    <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Rejected.png')}}" alt="">
                                                @elseif($value->status == 6)
                                                    <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Cancel.png')}}" alt="">
                                                @elseif($value->status == 6)
                                                    <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Failed.png')}}" alt="">
                                                @endif
                                            </div>
                                        @endif
                                    @endif
                                    
                                @endforeach  
                                @foreach(App\orders::STATUS as $key => $label)
                                    @if($key > $customer->order->status && $key <= 4)
                                    <div class="m-auto untrack-vertical-line" >
                                                <span class="vr"></span>
                                    </div>
                                    <div class="">
                                        @if($key == 1)
                                            <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Pending_not active.png')}}" alt="">
                                        @elseif($key == 2)
                                            <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/accepted_not active.png')}}" alt="">
                                        @elseif($key == 3)
                                            <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Shipped_not active.png')}}" alt="">
                                        @elseif($key == 4)
                                            <img class="img-responsive w-100 h-100 " src="{{asset('themes/rustic-theme/images/png/Delivered_not active.png')}}" alt="">
                                        @endif
                                    </div>
                                    @endif
                                @endforeach
                                      
                            </div>
                            <div class="col-sm-4" style="width:66%">
                            @foreach($orderstatus as $key => $value)
                                    @if($key == 0)
                                        @if($key != count($orderstatus)-1)
                                        <div class="cursor-pointer min_h_100" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')">
                                            <div><b>{{App\orders::STATUS[$value->status]}}</div>
                                            @php
                                                $s= $value->created_at ;
                                                $date = $s->format('m/d/Y');
                                                $time = date("h:i:s A",strtotime($s));
                                            @endphp
                                            {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                            <br>
                                            {{ $time }}
                                        </div>
                                        @else
                                        <div class="cursor-pointer min_h_100" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')">
                                            <div><b>{{App\orders::STATUS[$value->status]}}</div>
                                            @php
                                                $s= $value->created_at ;
                                                $date = $s->format('m/d/Y');
                                                $time = date("h:i:s A",strtotime($s));
                                            @endphp
                                            {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                            <br>
                                            {{ $time }}
                                        </div>
                                        @endif
                                        
                                    @elseif($key != count($orderstatus)-1)
                                        @if($value->status < 5)
                                        
                                        <div class="m-auto white-track-vertical-line">
                                            <span class="vr"></span>
                                        </div>
                                        <div class=" cursor-pointer min_h_100" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')">
                                            
                                            @if($value->status == 1)
                                            <div><b>{{App\orders::STATUS[$value->status]}}</div>
                                            @php
                                                $s= $value->created_at ;
                                                $date = $s->format('m/d/Y');
                                                $time = date("h:i:s A",strtotime($s));
                                            @endphp
                                            {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                            <br>
                                            {{ $time }}
                                            @elseif($value->status == 2)
                                            <div><b>{{App\orders::STATUS[$value->status]}}</div>
                                            @php
                                                $s= $value->created_at ;
                                                $date = $s->format('m/d/Y');
                                                $time = date("h:i:s A",strtotime($s));
                                            @endphp
                                            {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                            <br>
                                            {{ $time }}
                                            @elseif($value->status == 3)
                                            <div><b>{{App\orders::STATUS[$value->status]}}</div>
                                            @php
                                                $s= $value->created_at ;
                                                $date = $s->format('m/d/Y');
                                                $time = date("h:i:s A",strtotime($s));
                                            @endphp
                                            {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                            <br>
                                            {{ $time }}
                                            @elseif($value->status == 4)
                                            <div><b>{{App\orders::STATUS[$value->status]}}</div>
                                            @php
                                                $s= $value->created_at ;
                                                $date = $s->format('m/d/Y');
                                                $time = date("h:i:s A",strtotime($s));
                                            @endphp
                                            {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                            <br>
                                            {{ $time }}
                                            @endif
                                            
                                        </div>
                                        @else
                                        <div class="m-auto white-track-vertical-line">
                                            <span class="vr"></span>
                                        </div>
                                        <div class="d-flex justify-content-center align-items-center  cursor-pointer min_h_100" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')">
                                            
                                                @if($value->status == 5)
                                                    <div><b>{{App\orders::STATUS[5]}}</div>
                                                    @php
                                                        $s= $value->created_at ;
                                                        $date = $s->format('m/d/Y');
                                                        $time = date("h:i:s A",strtotime($s));
                                                    @endphp
                                                    {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                                    <br>
                                                    {{ $time }}
                                                @elseif($value->status == 6)
                                                    <div><b>{{App\orders::STATUS[6]}}</div>
                                                    @php
                                                        $s= $value->created_at ;
                                                        $date = $s->format('m/d/Y');
                                                        $time = date("h:i:s A",strtotime($s));
                                                    @endphp
                                                    {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                                    <br>
                                                    {{ $time }}
                                                @elseif($value->status == 7)
                                                    <div><b>{{App\orders::STATUS[7]}}</div>
                                                    @php
                                                        $s= $value->created_at ;
                                                        $date = $s->format('m/d/Y');
                                                        $time = date("h:i:s A",strtotime($s));
                                                    @endphp
                                                    {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                                    <br>
                                                    {{ $time }}
                                                @endif
                                            
                                        </div>
                                        @endif
                                    @else
                                        @if($customer->order->status < 5)
                                            <div class="m-auto white-track-vertical-line" >
                                                <span class="vr"></span>
                                            </div>
                                            <div class="d-flex justify-content-center align-items-center min_h_100  cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')">
                                                
                                                @if($value->status == 1)
                                                <div><b>{{App\orders::STATUS[$value->status]}}</div>
                                                @php
                                                    $s= $value->created_at ;
                                                    $date = $s->format('m/d/Y');
                                                    $time = date("h:i:s A",strtotime($s));
                                                @endphp
                                                {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                                <br>
                                                {{ $time }}
                                                @elseif($value->status == 2)
                                                <div><b>{{App\orders::STATUS[$value->status]}}</div>
                                                @php
                                                    $s= $value->created_at ;
                                                    $date = $s->format('m/d/Y');
                                                    $time = date("h:i:s A",strtotime($s));
                                                @endphp
                                                {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                                <br>
                                                {{ $time }}
                                                @elseif($value->status == 3)
                                                <div><b>{{App\orders::STATUS[$value->status]}}</div>
                                                @php
                                                    $s= $value->created_at ;
                                                    $date = $s->format('m/d/Y');
                                                    $time = date("h:i:s A",strtotime($s));
                                                @endphp
                                                {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                                <br>
                                                {{ $time }}
                                                @elseif($value->status == 4)
                                                <div><b>{{App\orders::STATUS[$value->status]}}</div>
                                                @php
                                                    $s= $value->created_at ;
                                                    $date = $s->format('m/d/Y');
                                                    $time = date("h:i:s A",strtotime($s));
                                                @endphp
                                                {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                                <br>
                                                {{ $time }}
                                                @endif
                                                
                                            </div>
                                        @else
                                            <div class="m-auto white-track-vertical-line">
                                                <span class="vr"></span>
                                            </div>
                                            <div class="d-flex justify-content-center align-items-center min_h_100 cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')">
                                                
                                                @if($value->status == 5)
                                                    <div><b>{{App\orders::STATUS[$value->status]}}</div>
                                                    @php
                                                        $s= $value->created_at ;
                                                        $date = $s->format('m/d/Y');
                                                        $time = date("h:i:s A",strtotime($s));
                                                    @endphp
                                                    {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                                    <br>
                                                    {{ $time }}
                                                @elseif($value->status == 6)
                                                    <div><b>{{App\orders::STATUS[$value->status]}}</div>
                                                    @php
                                                        $s= $value->created_at ;
                                                        $date = $s->format('m/d/Y');
                                                        $time = date("h:i:s A",strtotime($s));
                                                    @endphp
                                                    {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                                    <br>
                                                    {{ $time }}
                                                @elseif($value->status == 7)
                                                    <div><b>{{App\orders::STATUS[$value->status]}}</div>
                                                    @php
                                                        $s= $value->created_at ;
                                                        $date = $s->format('m/d/Y');
                                                        $time = date("h:i:s A",strtotime($s));
                                                    @endphp
                                                    {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                                    <br>
                                                    {{ $time }}
                                                @endif
                                                
                                            </div>
                                        @endif
                                    @endif
                                    
                                @endforeach  
                                @foreach(App\orders::STATUS as $key => $label)
                                    @if($key > $customer->order->status && $key <= 4)
                                    <div class="m-auto white-track-vertical-line" >
                                                <span class="vr"></span>
                                    </div>
                                    <div class="min_h_100">
                                        
                                        @if($key == 1)
                                            <div><b>{{App\orders::STATUS[1]}}</div>
                                        @elseif($key == 2)
                                            <div><b>{{App\orders::STATUS[2]}}</div>
                                        @elseif($key == 3)
                                            <div><b>{{App\orders::STATUS[3]}}</div>
                                        @elseif($key == 4)
                                            <div><b>{{App\orders::STATUS[4]}}</div>
                                        @endif
                                        
                                    </div>
                                    @endif
                                @endforeach
                            </div>
                            
                        </div>
                    </div>

                    <div class="row mt-1 mb-2 pl-2 pr-2">
                        <div class="col-md-12">
                            <hr class="divider mb-2" style="color:black;">

                            <div class="text-left">

                            @foreach($orderstatus as $key => $value)
                                <div>
                                @if($value->status == $customer->order->status)
                                        <div id='showremarks'><b>{{ App\orders::STATUS[$value->status] }}: </b>
                                        </div>
                                @endif
                                </div>
                            @endforeach
                             <div>
                             @foreach($orderstatusremark as $key => $value)
                                @if($value->status == $customer->order->status)
                                    @if($value->showremarks == 1)
                                    <div class="remark remarks-{{$value->status}}">
                                        <p> {{$value->remarks}} on {{$value->created_at}}</p>
                                    </div>
                                    @endif
                                @else
                                     @if($value->showremarks == 1)
                                    <div class="remark remarks-{{$value->status}}" style="display: none">
                                        <p> {{$value->remarks}} on {{$value->created_at}}</p>
                                    </div>
                                    @endif
                                @endif
                             @endforeach
                            </div>
                            </div>
                       </div>
                    </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.6.2/viewer.min.js" integrity="sha512-VzJLwaOOYyQemqxRypvwosaCDSQzOGqmBFRrKuoOv7rF2DZPlTaamK1zadh7i2FRmmpdUPAE/VBkCwq2HKPSEQ==" crossorigin="anonymous"></script>
<script>
    function show(x,statusContent){
    var order_data = JSON.parse(x)
    $('.remark').css("display", "none");
    $('.remarks-'+order_data.status).css("display", "block");
    var remarkhtml = "<b>"+statusContent+":</b>";
    $("#showremarks").html(remarkhtml)
 }

 var y='';

 function showtrack(x){
    $('.trackvalue_'+y).css("display", "none");
    y=x;
    $('.lastkey').css("display", "none");
    $('.trackvalue_'+x).css("display", "block");
 }
</script>
@endsection