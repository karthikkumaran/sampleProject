@extends('themes.'.$meta_data['themes']['name'].'.layouts.store')
@section ('styles')
<style>
.bg-white{
   background-color : #fff;                                     
}
.outer{
  width: 60px;
  height: 60px;
  position: relative;
  background-color: #fff;
}
.first-circle{
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background-color: #ccc;
}

.second-circle{
  width: 90%;
  height: 90%;
  border-radius: 50%;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  margin: auto;
  background-color: white;
}
.paddingCart{
  padding: 0.5rem;                                      
}
</style>

@endsection

@section('content')
@php
 $agent=new \Jenssegers\Agent\Agent();
@endphp
@include('themes.'.$meta_data['themes']['name'].'.partials.public_banner')
@php
    if(empty($meta_data['settings']['currency'])){
        $currency = "₹";
    }
    else{
        if($meta_data['settings']['currency_selection'] == "currency_symbol"){
            $currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
        }
        else{
            $currency = $meta_data['settings']['currency'];
        }
    }
    if(isset($catalog)){
      $campaign = $catalog[0]; 
    }
@endphp


<div class="container-fluid">
    @php
        if($campaign->is_start == 1) {
            $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
            $catalogLink = route('campaigns.published',$campaign->public_link);
            $videoLink = route('campaigns.publishedVideo',$campaign->public_link);
            $shoppableLink = route('mystore.publishedVideo',$userSettings->public_link);
            $showallLink = route('campaigns.showVideo',$campaign->public_link);
        }else{
            $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
            $catalogLink = route('admin.campaigns.preview',$campaign->preview_link);
            $videoLink = route('campaigns.publishedVideo',$campaign->preview_link);
            $shoppableLink = route('mystore.publishedVideo',$userSettings->preview_link);
            $showallLink = route('campaigns.showVideo',$campaign->preview_link);
        }
        $entitycards = $campaign->campaignEntitycards;
        $n = count($entitycards);
        $procount = 0;
        $entitycard_count = 0;
    @endphp                              
    <h1 class="text-center mb-3 mt-5 lg_hide">{{$campaign->name}}<a href="{{$catalogLink}}" style="text-decoration:none;"><p id="viewall"
        class="text-end1 h5 view_500" style="cursor:pointer; font-weight: 700;
        font-size: 18px; margin-left: 91%;
        line-height: 22px; 
        color: #6C4E4F; margin-top: -1%;"> View All</p></a></h1>
    <h1 class="text-left mb-3 mt-5 sm_hide">{{$campaign->name}}<a href="{{$catalogLink}}" style="text-decoration:none;">
        <p id="viewall"
                    
        class="text-end h5" style="cursor: pointer;
        font-weight: 700;
        font-size: 18px;
        margin-left: 72%;
        line-height: 22px;
        color: #6C4E4F;
        margin-top: -7%;
        margin-left: 12px"> View All</p></a></h1>
    

    <div  class="container">
        
             
            <div id="contain" class="scrolling-wrapper inner_scroll_x row flex-row flex-nowrap mt-4 pb-2 pt-2">
            @foreach ($entitycards as $key => $card)

                @if(!empty($card->product) && empty($card->video_id))
                @php
                    $entitycard_count++;
                    //if($entitycard_count > 5){
                    //    break;
                    //}
                    $product = $card->product;
                    if($product->downgradeable == 1){
                        continue;
                    }
                    if($product->discount != null) {
                        $discount_price = $product->price * ($product->discount/100);
                        $discounted_price = round(($product->price - $discount_price),2);
                    }
                    if(count($product->photo) > 0){
                        $img = $product->photo[0]->getUrl('thumb');
                        $objimg = asset('XR/assets/images/placeholder.png');
                        $obj_id = "";
                        $obj_3d = false;
                    } else if(count($product->arobject) > 0){
                        if(!empty($product->arobject[0]->object))
                            if(!empty($product->arobject[0]->object->getUrl('thumb'))) {
                                $img = $product->arobject[0]->object->getUrl('thumb');
                            }
                    } else {
                        $img = asset('XR/assets/images/placeholder.png');
                        $objimg = asset('XR/assets/images/placeholder.png');
                    }
                    foreach ($product->arobject as $key => $arobject) {
                        if(!empty($arobject->object)){
                            if($arobject->ar_type == "face") {
                                if(!empty($arobject->object->getUrl('thumb'))) {
                                    $objimg = $arobject->object->getUrl('thumb');
                                    $obj_id = $arobject->id;
                                }
                            }
                        }
                        //echo $arobject->ar_type;
                        if($arobject->ar_type == "surface") {
                            if(!empty($arobject->object_3d)) {
                                $obj_3d = true;
                            }
                        }
                    }
                    if($campaign->is_start == 1) {
                        if(count($product->categories) > 0) {
                            $categoryLink = route('campaigns.publishedProductByCategory',[$product->categories[0]['id'],$campaign->public_link]);
                        }
                        $catalogId = $campaign->public_link;
                        $tryonLink = route('campaigns.publishedTryOn',$campaign->public_link)."?sku=".$product->id;
                        $viewARLink = route('campaigns.publishedViewAR',$campaign->public_link)."?sku=".$product->id;
                        $catalogDetailsLink = route('campaigns.publishedCatalogDetails',[$product->id,$campaign->public_link]);
                        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
                    }else{
                        if(count($product->categories) > 0) {
                            $categoryLink = route('campaigns.publishedProductByCategory',[$product->categories[0]['id'],$campaign->preview_link]);
                        }
                        $catalogId = $campaign->preview_link;
                        $tryonLink = route('admin.campaigns.previewTryOn',$campaign->preview_link)."?sku=".$product->id;
                        $viewARLink = route('admin.campaigns.previewViewAR',$campaign->preview_link)."?sku=".$product->id;
                        $catalogDetailsLink = route('admin.campaigns.previewCatalogDetails',[$product->id,$campaign->preview_link]);
                        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
                    }
                @endphp
                @endif 
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div  class="card shadow-lg rounded-lg border-none minCardHeight"
                       style="box-shadow: 0 1rem 1rem rgb(34 41 47 / 18%) !important; min-height:34rem !important;">
                       
 
                                                           
                        
                                        <div class="card-body text-center padx">
                                            <div> 

                                 
                                    
                                                    <div class="h-100 ">
                                                    <a href="{{$catalogDetailsLink}}"><img id="product" src="{{$img}}" class="img-responsive  " alt="..."></a>
                                                    </div>

                                  


                                                 

                                 

                                                <div class="allx">
                                                    
                                                    <div class="mt-5">
                                                    <h6 class="productText">
                                                        <b>@if(isset($product->name)){{$product->name}}
                                                        @else <br>
                                                        
                                                    @endif</b></h6>
                                                    </div> 
                                                    

                                                    @if($product->discount != null)
                                                        <div class="mt-2 productText">
                                                        <span class="text-decoration-line-through discount_text">₹{{number_format($product->price,2)}}
                                                        </span> <b>₹{{number_format($discounted_price,2)}}</b>  <span id="offer" class=" bg-warn">{{$product->discount}}% OFF</span>
                                                        </div>
                                                    @else
                                                        <div class="mt-2 productText">
                                                            <b class="bg-light" style="background-color: white !important;
                                                            ">@if(isset($product->price))₹{{$product->price}}
                                                                @else
                                                        <br>
                                                        
                                                        @endif</b>
                                                        </div>
                                                    @endif 

                                                    <div class="mt-2">
                                                    @if((@isset($meta_data['themes']['product_details_display'])) && ($meta_data['themes']['product_details_display'] == "new_page"))
                                                        @if($product->out_of_stock != null)
                                                            <div class="cart bg-maroon paddingCart " style="background: #d9534f !important;">
                                                                <div class="">
                                                                    Out of stock
                                                                </div>
                                                            </div>
                                                            @else
                                                            <div class="cart quantityController bg-maroon paddingCart cardborder" href="#carouselIndicators2" role="button" >
                                                                @if(!empty($product->shop) && $product->external_shop == 1)
                                                                <a href="{{$product->link}}" style="color:white; text-decoration: none; font-weight: 700;
                                                                    font-size: 18px;
                                                                    line-height: 22px;" target="_blank">{{$product->shop}}</a>
                                                                @else
                                                                @if($product->has_variants == 1)
                                                                <a href="{{$catalogDetailsLink}}" class="a-tag-text"><div class="item-wrapper d-inline-flex  h-100 txtx" onclick="getProductDetails({{$card->id}},{{$campaign->id}})" data-toggle="modal" data-target="#productDetailsModal" style="cursor:pointer;">
                                                                    {{-- <i class="feather icon-box m-2"></i> --}}
                                                                    View Product</div></a>
                                                                @else
                                                                    <i class="feather icon-shopping-cart"></i>
                                                                    <span class="add-to-cart 2 txtx" onclick="addToCart({{$product->id}})" data-id="{{$product->id}}" data-name="{{$product->name ?? ''}}" data-quantity="{{$product->minimum_quantity ?? '1'}}" data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-maximum_quantity="{{ $product->maximum_quantity ?? '10000' }}"
                                                                        data-price="{{$product->price ?? '0'}}" data-productweight="{{$product->productweight ?? '0'}}" data-length="{{$product->length ?? '0'}}" data-width="{{$product->width ?? '0'}}" data-height="{{$product->height ?? '0'}}"
                                                                        data-discounted_price="{{$product->discount ? $discounted_price:'0'}}" data-catalogid="{{$catalogId}}"
                                                                        data-catagory="{{(count($product->categories) > 0)?$product->categories[0]['name']:''}}"
                                                                        data-img="{{$img}}" data-sku="{{ $product->sku }}" data-product_id="{{ $product->id }}"
                                                                        data-quantity="{{ $product->minimum_quantity ?? '1' }}">Add to cart
                                                                    </span>
                                                                @endif
                                                                
                                                                <span class="view-in-cart d-none 10">
                                                                    <div class="input-group quantity-counter-wrapper">
                                                                        <input type="text" id="product_qty_{{$product->sku}}" class="quantity-counter" onclick="productQuantity({{$product->id}})"  data-id="{{$product->sku}}" value="{{$product->minimum_quantity ?? '1'}}" data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-maximum_quantity="{{ $product->maximum_quantity ?? '10000' }}" data-sku="{{ $product->sku }}">
                                                                        {{-- <input type="text" id="product_qty_{{$product->sku}}" class="quantity-counter" data-id="{{$product->id}}" value="{{$product->minimum_quantity ?? '1'}}" data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-sku="{{ $product->sku }} " data-maximum_quantity="{{ $product->maximum_quantity ?? '10000' }}" > --}}
                                                                    </div>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        @endif
                                                    @else
                                                        @if($product->out_of_stock != null)
                                                            <div class="cart bg-maroon paddingCart" style="background: #d9534f !important;">
                                                                <div class="">
                                                                    Out of stock
                                                                </div>
                                                            </div>
                                                        @else
                                                            <div class="cart quantityController bg-maroon paddingCart cardborder" href="#carouselIndicators2" role="button" >
                                                                @if(!empty($product->shop) && $product->external_shop == 1)
                                                                <a href="{{$product->link}}" style="color:white; text-decoration: none; font-weight: 700;
                                                                    font-size: 18px;
                                                                    line-height: 22px;" target="_blank">{{$product->shop}}</a>
                                                                @else
                                                                @if($product->has_variants == 1)
                                                                <a href="{{$catalogDetailsLink}}" class="a-tag-text"><div class="item-wrapper d-inline-flex  h-100 txtx" onclick="getProductDetails({{$card->id}},{{$campaign->id}})" data-toggle="modal" data-target="#productDetailsModal" style="cursor:pointer;">
                                                                    {{-- <i class="feather icon-box m-2"></i> --}}
                                                                    View Product</div></a>
                                                                @else
                                                                    <i class="feather icon-shopping-cart"></i>
                                                                    <span class="add-to-cart 2 txtx" onclick="addToCart({{$product->id}})" data-id="{{$product->id}}" data-name="{{$product->name ?? ''}}" data-quantity="{{$product->minimum_quantity ?? '1'}}" data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-maximum_quantity="{{ $product->maximum_quantity ?? '10000' }}"
                                                                        data-price="{{$product->price ?? '0'}}" data-productweight="{{$product->productweight ?? '0'}}" data-length="{{$product->length ?? '0'}}" data-width="{{$product->width ?? '0'}}" data-height="{{$product->height ?? '0'}}"
                                                                        data-discounted_price="{{$product->discount ? $discounted_price:'0'}}" data-catalogid="{{$catalogId}}"
                                                                        data-catagory="{{(count($product->categories) > 0)?$product->categories[0]['name']:''}}"
                                                                        data-img="{{$img}}" data-sku="{{ $product->sku }}" data-product_id="{{ $product->id }}"
                                                                        data-quantity="{{ $product->minimum_quantity ?? '1' }}">Add to cart
                                                                    </span>
                                                                @endif
                                                                
                                                                <span class="view-in-cart d-none 10">
                                                                    <div class="input-group quantity-counter-wrapper">
                                                                        <input type="text" id="product_qty_{{$product->sku}}" class="quantity-counter" onclick="productQuantity({{$product->id}})"  data-id="{{$product->sku}}" value="{{$product->minimum_quantity ?? '1'}}" data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-maximum_quantity="{{ $product->maximum_quantity ?? '10000' }}" data-sku="{{ $product->sku }}">
                                                                        {{-- <input type="text" id="product_qty_{{$product->sku}}" class="quantity-counter" data-id="{{$product->id}}" value="{{$product->minimum_quantity ?? '1'}}" data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-sku="{{ $product->sku }} " data-maximum_quantity="{{ $product->maximum_quantity ?? '10000' }}" > --}}
                                                                    </div>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        @endif
                                                    @endif
                                                    </div> 
                                
                                                </div>
                             
                                            </div>
                                        </div>
                                   
                </div>  
                </div>
            @endforeach
                
                
                 
       
        </div>
        
                
        
    </div>
    
    <div class="text-end mt-3 border-none"> 
        <img id="slideleft" src="{{asset('themes/rustic-theme/images/svg/bi_arrow-left-circle-fill.svg')}}" alt="">  
        <img id="slideright" src="{{asset('themes/rustic-theme/images/svg/bi_arrow-right-circle-fill.svg')}}" alt=""> 
   </div>
</div>
<!-- our purpose section starts -->
<div class="row mt-3 sm_hide">
    <div class="col-md-6 ">
        
        
            <img class="img-responsive w-100" src="{{asset('themes/rustic-theme/images/png/mountain_mobile.png')}}"    alt="...">
        
    </div>
</div>

<div id="colordiv" class="container-fluid  p-5  ">

    
    <div class="container">
        <div class="row ">
            
             

            <div class="col-md-6 lg_hide">
             
                
                    <img class="img-responsive w-100" src="{{asset('themes/rustic-theme/images/png/Mask group.png')}}"    alt="...">
              
            </div>
             
             
            <div class="col-md-6">
                <div >
                    <h1 class="text-left purpose_title mb-5">Our Purpose</h1>
                    <p class="purpose_text">Rustic- Earth to Soul is a women entrepreneurial dream that has taken flight with an aspiration to kindle the senses and soul of its consumers by bringing the best quality natural produce from the hills of Coorg and offering a near-reality feel of the region’s enchanting beauty. Rustic appeals to the consumers through its offerings that emanate the warmth of the glowing sun, the playfulness of the cool wind, the endurance of Mother Earth, the freshness of flowing water, and the luster of the blue sky. </p>
                </div>
            </div>
        
            
        
            
        </div>
    </div>
</div>
</div>
<!-- value that we bring to you section  -->
<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-12 my-2 sm_hide">

            <img class="img-responsive w-100 h-100" src="{{asset('themes/rustic-theme/images/png/leaflet_mobile.png')}}" alt="">
            
        </div>
</div>

<div class="container-fluid mt-5  ">
    <div class="container ">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-12 my-2 lg_hide">

                    <img class="img-responsive w-100 h-100" src="{{asset('themes/rustic-theme/images/png/Mask group-1.png')}}" alt="">
                    
                </div>

             
            
            <div class="col-lg-4 col-md-6 col-sm-12 my-2 ">
                <div class="container ">
                <h5 class="overflow-hidden value_title" >Value that we bring to you</h5>
                <span class="value_head">Reliable -</span> <span class="value_text"> Delivering only the best to the consumers, hand-picked from plants grown on fertile soil where native women work together as a team </span><br><br>

                <span class="value_head">Authentic -</span> <span class="value_text"> Providing natural, single origin products directly to you, delivered with warmth</span><br><br>

                <span class="value_head">High Quality -</span> <span class="value_text"> Putting the products through multiple tests to ensure the highest quality deliverance</span><br><br>

                <span class="value_head">Futuristic -</span> <span class="value_text"> Continue holding the foresight to save and reinstate the richness of nature</span>
                    </div>
            </div>
            <div class="col-lg-5 col-md-12 col-sm-12 my-2" id="colordiv">
                <div class="jumbotron" id="colordiv">
                <div  class="card-body">
                    <h5 class="card-heading value_title text-brown">Vision</h5>
                    <p class="value_text text-brown">Rustic is a women entrepreneurial dream that has taken flight with an aspiration to connect your soul, to the 5 Elements of nature.  To deliver nourishment to the consumers’ bodies and minds through purely sourced natural produce from women entrepreneurial ventures bringing them close to nature, while offering a virtual rendition of the land it comes from, Coorg.  
                         </p>
                 
                        <h5 class="value_title text-brown">Mission</h5>
                        <p class="value_text text-brown">To procure naturally grown products from responsible and nature-loving natives of Coorg and to show the beauty of their land to nature and health enthusiasts outside. 
                            </p>
                   
                    </div>
                </div>



            </div>
        </div>
    </div>
</div>




<!-- testimonials section starts   -->

<div id="colordiv" class="container-fluid mt-5 p-5 min_height_500 desk_show" style="display:none">
    <h2 class="text-center">Testimonials</h2>
    <div class="container mt-5">
        <div class="row">
             
            <div class="col-md-12">

                <div id="carouselExampleIndicatorstest" class="carousel slide" data-ride="carousel">
                   
                    
                               

        <div class="carousel-inner">
                
            <div class="carousel-item active ">

                <div class="row d-flex justify-content-center">
                        
                    <div class="col-md-6 my-2 w-75">
                        <div id="border" class="card-body bg-white my-2 border  h-100">
                                <div class="d-flex mt-4 ">
                                    <div class="col-4 rightborder">
                                        <div class="text-center"><img src="{{asset('themes/rustic-theme/images/png/Ellipse 8.png')}}" alt=""></div>
                                        <div class="mx-3 " >
                                        <h5 class="testimonal_name text-center">Hema Mohnani</h5>
                                        <h6 class="text-muted testimonal_address text-center">Founder, Jaipur Junction,<br>
                                                                concept store, Mysore
                                                                </h6>
                                    </div>
                                    </div>
                                    <div class="col-8">
                                    <p class="m-3 text-muted testimoni_text">"I was never a coffee fan but started loving coffee after I tried Rustic filter coffee powder. It has a very organic and natural coffee taste to it.   I can feel the difference in taste and quality from the regular coffee brands I had tried"".</p>
                                    </div>
                                </div>
                                
                        </div>                      
                    </div>
                   
                </div>
            </div>

            <div class="carousel-item  ">

                <div class="row d-flex justify-content-center">
                            
                            <div class="col-md-6 my-2 w-75">
                                <div id="border" class="card-body bg-white my-2 border  h-100">
                                        <div class="d-flex mt-4">
                                            <div class="col-4 rightborder">
                                                <div class="text-center"><img src="{{asset('themes/rustic-theme/images/png/Ellipse 9.png')}}" alt=""></div>
                                                <div class="mx-3 " >
                                            <h5 class="testimonal_name text-center">Suchithra</h5>
                                            <h6 class="text-muted testimonal_address text-center"> Chennai
                                                                    </h6>
                                            </div>
                                            </div>
                                            <div class="col-8">
                                            <p class="m-3 text-muted testimoni_text">"I used to buy Rustic products honey, turmeric and pepper. Quality of the products are really good"</p>
                                            </div>
                                        </div>
                                </div>                      
                            </div>

                </div>
            </div>        


            <div class="carousel-item  ">

                <div class="row d-flex justify-content-center">
                            
                            <div class="col-md-6 my-2 w-75">
                                <div id="border" class="card-body bg-white my-2 border  h-100">
                                        <div class="d-flex mt-4">
                                            <div class="col-4 rightborder">
                                                <div class="text-center"><img src="{{asset('themes/rustic-theme/images/png/Ellipse 7.png')}}" alt=""></div>
                                                <div class="mx-3 " >
                                            <h5 class="testimonal_name text-center">Nivedita Sundaram</h5>
                                            <h6 class="text-muted testimonal_address text-center">Guardian Life,<br> Chennai, INDIA
                                                                    </h6>
                                            </div>
                                            </div>
                                            <div class="col-8">
                                            <p class="m-3 text-muted testimoni_text">“Congratulations on launching your business online! Happy to share that we are one of the First customers of Rustic products! Loved their aromatic spices, famous coffee from their own plantation and the flavourful honey, all packed in sustainable glass bottles or brown paper packages. I got to taste pickled green pepper for the first time and boy, was it spicy! Looking forward to ordering all my favourites and trying new stuff from Rustic's brand new website!”</p>
                                            </div>
                                        </div>
                                </div>                      
                            </div>

                </div>
            </div>
        </div>
           
        <ul class="carousel-indicators  my-4 position-relative text-center">
            <li id="colorbutton" data-target="#carouselExampleIndicatorstest" data-slide-to="0" class="active"><button class="buttonradius" class="btn border-none" style="display:none"></button></li>
            <li id="colorbutton" data-target="#carouselExampleIndicatorstest" data-slide-to="1"><button class="buttonradius"class="btn border-none" style="display:none"></button></li>
            <li id="colorbutton" data-target="#carouselExampleIndicatorstest" data-slide-to="2"><button class="buttonradius" class="btn border-none" style="display:none"></button> </li>
        </ul>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<div id="colordiv" class="container-fluid mt-5 p-5 min_height_500 tabl_show" style="display:none">
    <h2 class="text-center">Testimonials</h2>
    <div class="container mt-5">
        <div class="row">
             
            <div class="col-md-12">

                <div id="carouselExampleIndicatorstest" class="carousel slide" data-ride="carousel">
                   
                    
                               

        <div class="carousel-inner">
                
            <div class="carousel-item active ">

                <div class="row d-flex justify-content-center">
                        
                    <div class="col-md-6 my-2 w-100">
                        <div id="border" class="card-body bg-white my-2 border  h-100">
                                <div class="d-flex mt-4 ">
                                    <div class="col-4 rightborder">
                                        <div class="text-center"><img src="{{asset('themes/rustic-theme/images/png/Ellipse 8.png')}}" alt=""></div>
                                        <div class="mx-3 " >
                                        <h5 class="testimonal_name text-center">Hema Mohnani</h5>
                                        <h6 class="text-muted testimonal_address text-center">Founder, Jaipur Junction,<br>
                                                                concept store, Mysore
                                                                </h6>
                                    </div>
                                    </div>
                                    <div class="col-8">
                                    <p class="m-3 text-muted testimoni_text">"I was never a coffee fan but started loving coffee after I tried Rustic filter coffee powder. It has a very organic and natural coffee taste to it.   I can feel the difference in taste and quality from the regular coffee brands I had tried"".</p>
                                    </div>
                                </div>
                                
                        </div>                      
                    </div>
                   
                </div>
                </div>




            <div class="carousel-item  ">

                <div class="row d-flex justify-content-center">
                            
                            <div class="col-md-6 my-2 w-100">
                                <div id="border" class="card-body bg-white my-2 border  h-100">
                                        <div class="d-flex mt-4">
                                            <div class="col-4 rightborder">
                                                <div class="text-center"><img src="{{asset('themes/rustic-theme/images/png/Ellipse 9.png')}}" alt=""></div>
                                                <div class="mx-3 " >
                                            <h5 class="testimonal_name text-center">Suchithra</h5>
                                            <h6 class="text-muted testimonal_address text-center">Chennai
                                                                    </h6>
                                            </div>
                                            </div>
                                            <div class="col-8">
                                            <p class="m-3 text-muted testimoni_text">"I used to buy Rustic products honey, turmeric and pepper. Quality of the products are really good"</p>
                                            </div>
                                        </div>
                                </div>                      
                            </div>

                </div>
            </div>

            <div class="carousel-item  ">

                <div class="row d-flex justify-content-center">
                            
                            <div class="col-md-6 my-2 w-100">
                                <div id="border" class="card-body bg-white my-2 border  h-100">
                                        <div class="d-flex mt-4">
                                            <div class="col-4 rightborder">
                                                <div class="text-center"><img src="{{asset('themes/rustic-theme/images/png/Ellipse 7.png')}}" alt=""></div>
                                                <div class="mx-3 " >
                                            <h5 class="testimonal_name text-center">Nivedita Sundaram</h5>
                                            <h6 class="text-muted testimonal_address text-center">Guardian Life,<br> Chennai, INDIA
                                                                    </h6>
                                            </div>
                                            </div>
                                            <div class="col-8">
                                            <p class="m-3 text-muted testimoni_text">“Congratulations on launching your business online! Happy to share that we are one of the First customers of Rustic products! Loved their aromatic spices, famous coffee from their own plantation and the flavourful honey, all packed in sustainable glass bottles or brown paper packages. I got to taste pickled green pepper for the first time and boy, was it spicy! Looking forward to ordering all my favourites and trying new stuff from Rustic's brand new website!”</p>
                                            </div>
                                        </div>
                                </div>                      
                            </div>

                </div>
            </div>
    </div>

        <ul class="carousel-indicators  my-4 position-relative text-center">
            <li id="colorbutton" data-target="#carouselExampleIndicatorstest" data-slide-to="0" class="active"><button class="buttonradius" class="btn border-none" style="display:none"></button></li>
            <li id="colorbutton" data-target="#carouselExampleIndicatorstest" data-slide-to="1"><button class="buttonradius"class="btn border-none" style="display:none"></button></li>
            <li id="colorbutton" data-target="#carouselExampleIndicatorstest" data-slide-to="2"><button class="buttonradius" class="btn border-none" style="display:none"></button> </li>
        </ul>
           
        
                    
                </div>
            </div>
        </div>
    </div>
</div>
<div id="colordiv" class=" mt-2 p-2 min_height_500 mob_show" style="display:none">
    <h2 class="text-center">Testimonials</h2>
    <div class=" mt-5">
        <div class="row">
             
            <div class="col-md-12 m-2">

                <div id="carouselExampleIndicatorstest" class="carousel slide" data-ride="carousel">
                   
                    
                               

        <div class="carousel-inner">
                
            <div class="carousel-item active ">

                <div class="row d-flex justify-content-center">
                        
                    <div class="col-md-6 my-2 ">
                        <div id="border" class="card-body bg-white my-2 border  h-100">
                                <div class="mt-4 ">
                                    <div class="col-12 borderborder">
                                        <div class="text-center"><img src="{{asset('themes/rustic-theme/images/png/Ellipse 8.png')}}" alt=""></div>
                                        <div class="mx-3 m-auto" >
                                        <h5 class="testimonal_name text-center">Hema Mohnani</h5>
                                        <h6 class="text-muted testimonal_address text-center">Founder, Jaipur Junction,<br>
                                                                concept store, Mysore
                                                                </h6>
                                    </div>
                                    </div>
                                    <div class="col-12">
                                    <p class="m-3 text-muted testimoni_text">"I was never a coffee fan but started loving coffee after I tried Rustic filter coffee powder. It has a very organic and natural coffee taste to it.   I can feel the difference in taste and quality from the regular coffee brands I had tried"".</p>
                                    </div>
                                </div>
                                
                        </div>                      
                    </div>
                   
                </div>
                </div>




        <div class="carousel-item  ">

            <div class="row d-flex justify-content-center">
                        
                        <div class="col-md-6 my-2 ">
                            <div id="border" class="card-body bg-white my-2 border  h-100">
                                    <div class=" mt-4">
                                        <div class="col-12 borderborder">
                                            <div class="text-center"><img src="{{asset('themes/rustic-theme/images/png/Ellipse 9.png')}}" alt=""></div>
                                            <div class="mx-3 " >
                                        <h5 class="testimonal_name text-center">Suchithra</h5>
                                        <h6 class="text-muted testimonal_address text-center">Chennai
                                                                </h6>
                                        </div>
                                        </div>
                                        <div class="col-12">
                                        <p class="m-3 text-muted testimoni_text">"I used to buy Rustic products honey, turmeric and pepper. Quality of the products are really good"</p>
                                        </div>
                                    </div>
                            </div>                      
                        </div>

            </div>
        </div>

        <div class="carousel-item  ">

            <div class="row d-flex justify-content-center">
                        
                        <div class="col-md-6 my-2 ">
                            <div id="border" class="card-body bg-white my-2 border  h-100">
                                    <div class=" mt-4">
                                        <div class="col-12 borderborder">
                                            <div class="text-center"><img src="{{asset('themes/rustic-theme/images/png/Ellipse 7.png')}}" alt=""></div>
                                            <div class="mx-3 " >
                                        <h5 class="testimonal_name text-center">Nivedita Sundaram</h5>
                                        <h6 class="text-muted testimonal_address text-center">Guardian Life,<br> Chennai, INDIA
                                                                </h6>
                                        </div>
                                        </div>
                                        <div class="col-12">
                                        <p class="m-3 text-muted testimoni_text">“Congratulations on launching your business online! Happy to share that we are one of the First customers of Rustic products! Loved their aromatic spices, famous coffee from their own plantation and the flavourful honey, all packed in sustainable glass bottles or brown paper packages. I got to taste pickled green pepper for the first time and boy, was it spicy! Looking forward to ordering all my favourites and trying new stuff from Rustic's brand new website!”</p>
                                        </div>
                                    </div>
                            </div>                      
                        </div>

            </div>
        </div>
    </div>
           
        <ul class="carousel-indicators  my-4 position-relative text-center">
            <li id="colorbutton" data-target="#carouselExampleIndicatorstest" data-slide-to="0" class="active"><button class="buttonradius" class="btn border-none" ></button></li>
            <li id="colorbutton" data-target="#carouselExampleIndicatorstest" data-slide-to="1"><button class="buttonradius"class="btn border-none" ></button></li>
            <li id="colorbutton" data-target="#carouselExampleIndicatorstest" data-slide-to="2"><button class="buttonradius" class="btn border-none" ></button> </li>
        </ul>
        
                    
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('scripts')
@parent
<script src="{{asset('XR/assets/js/rustic_cart.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')}}">
<script src="{{ asset('themes/classic-theme/app-ecommerce-shop.js') }}"></script>
<script src="{{ asset('XR/app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js') }}"></script>
<script src="{{ asset('js/common.js') }}"></script>
<script>
 var quantityCounter = $(".quantity-counter"),
    CounterMin = 1,
    CounterMax = 10000;
    var min_qty = 1;
  if (quantityCounter.length > 0) {
    quantityCounter.TouchSpin({
      min: CounterMin,
      max: CounterMax
    }).on('touchspin.on.startdownspin', function () {
      var $this = $(this);
    
    umami('product-quantity-decrement-store-sku-'+ $(this).data("id"));
      let min_qty = $(this).data("minimum_quantity") || 1;
      $this.trigger("touchspin.updatesettings", {min: min_qty})
      $('.bootstrap-touchspin-up').removeClass("disabled-max-min");
      if ($this.val() <= min_qty) {
            $(this).siblings().find('.bootstrap-touchspin-down').addClass("disabled-max-min");
           
        }
        obj.setCountForItem($this.data('sku'),$this.val());
    }).on('touchspin.on.startupspin', function () {
        var $this = $(this);
        umami('product-quantity-increment-store-sku-'+ $(this).data("id"));
        let max_qty = $(this).data("maximum_quantity") || 10;
        $('.bootstrap-touchspin-down').removeClass("disabled-max-min");
        if ($this.val() <= max_qty) {
            $(this).siblings().find('.bootstrap-touchspin-up').addClass("disabled-max-min");
        }
        obj.setCountForItem(Number($this.data('id')),$this.val());
    }).on('change', function () {
      var $this = $(this);
      let min_qty = $(this).data("minimum_quantity") || 1;
      let max_qty = $(this).data("maximum_quantity") || 10;
      if ($this.val() == 0) {
            obj.setCountForItem($this.data('sku'),min_qty);
            $("#product_qty_" + $this.data('sku')).val(min_qty);
        } else if ($this.val() > CounterMax) {
            obj.setCountForItem($this.data('sku'),CounterMax);
      }
      else if($this.val() < min_qty){
        obj.setCountForItem($this.data('sku'),min_qty);
        $("#product_qty_" + $this.data('sku')).val(min_qty);
      }
      else if($this.val() > max_qty){
        obj.setCountForItem($this.data('sku'),max_qty);
        $("#product_qty_" + $this.data('sku')).val(max_qty);
      }
      else {
         obj.setCountForItem($this.data('sku'),$this.val());
      }
    });
  }
    obj.setCountForItem = function(sku,qty) {
        for (var i in cart) {
            console.log(cart[i].qty);
            if (cart[i].sku=== sku) {
                cart[i].qty = qty;;
                break;
            }
        }
        saveCart();
        obj.totalCart();
    };

  obj.removeItemFromCart = function(sku) {
        for (var item in cart) {
            if (cart[item].sku== sku) {
                //   cart[item].qty --;
                //   if(cart[item].qty === 0) {
                cart.splice(item, 1);
                //   }
                //   break;
            }
        }
        saveCart();
    }

  $( document ).ready(function() {
    if (localStorage.getItem("shoppingCart") != null) {
        cart = JSON.parse(localStorage.getItem('shoppingCart'));
        var cartItemCount = count = cart.length;
            if (cart.length > 0) {
                var products = [];
                for (var item in cart) {
                    var sku = cart[item].sku,
                        qty = cart[item].qty;
                    $("#product_qty_" + (sku)).val(qty);
                }

            }
    }
    });      

    
</script>
<script src="{{asset('themes/rustic-theme/js/script.js')}}"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
            <script  src="{{asset('themes/rustic-theme/js/jquery.nicescroll.min.js')}}"></script>
            <script>
                $(".contain").niceScroll();
                console.log('hghhds');
            </script>
@endsection

<style>
    .productText {
        color: #432F2F;
        font-weight: 800;
        font-size: 18px;
        line-height: 22px;
    }
    .left{
        margin-right:10px;
    }

    .txtx {
        font-weight: 700;
        font-size: 18px;
        line-height: 22px;
    }

    .product {
        width: 165.82px;
        height: 238.36px;
        left: 220.09px;
        top: 740.19px;
    }

    .bg-light {
        font-style: normal;
        font-weight: 700;
        font-size: 18px;
        line-height: 22px;
        background-color: white !important;
    }

    .img {
        width: 165.82px;
        height: 238.36px;
        left: 220.09px;
        top: 740.19px;
    }
    .slide{
        margin-right: 10px;
    }

    .padx {
        margin-top: 30px;
        margin-bottom: 30px;
    }

    .cardborder {
        width:95%;
        height: 40px;
        left: 599px;
        top: 1148.9px;
    }

    

    .border-none {
        margin-right: 16px;
    }
    
</style>