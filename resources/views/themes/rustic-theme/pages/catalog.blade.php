@extends('themes.'.$meta_data['themes']['name'].'.layouts.public')
@section ('styles')
<style>
.bg-white{
   background-color : #fff;                                     
}
.outer{
  width: 60px;
  height: 60px;
  position: relative;
  background-color: #fff;
}
.first-circle{
  width: 100%;
  height: 100%;
  border-radius: 50%;
  background-color: #ccc;
}

.second-circle{
  width: 90%;
  height: 90%;
  border-radius: 50%;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  margin: auto;
  background-color: white;
}

</style>
@endsection

@section('content')
@php
    if($campaign->is_start == 1) {
        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
        $catalogLink = route('campaigns.published',$campaign->public_link);
        $shoppableLink = route('mystore.publishedVideo',$userSettings->public_link);
    }else{
        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
        $catalogLink = route('admin.campaigns.preview',$campaign->preview_link);
        $shoppableLink = route('mystore.publishedVideo',$userSettings->preview_link);
    }
 $agent=new \Jenssegers\Agent\Agent();
@endphp
@include('themes.'.$meta_data['themes']['name'].'.partials.public_product_banner')
@php
    if(empty($meta_data['settings']['currency'])){
        $currency = "₹";
    }
    else{
        if($meta_data['settings']['currency_selection'] == "currency_symbol"){
            $currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
        }
        else{
            $currency = $meta_data['settings']['currency'];
        }
    }
@endphp
<div class="bg-white">
<!-- Ecommerce Products Starts -->
<section id="ecommerce-productscatalog" class="grid-view d-block mb-4">

<div id="catalog_prod" class="row m-1">

</div>
</section>
<!-- Ecommerce Products Ends -->
</div>



@endsection
@section('scripts')
@parent
<script src="{{asset('XR/assets/js/rustic_cart.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js"></script>
<script>
var shareable_link = "{{($userSettings->is_start == 1)?$campaign->public_link:$campaign->preview_link}}";
var q = "";
    var searchData = "";
    var sort = "";
    var filter = [];
    var page = 0;
    var filterData = "";
    var preview = 0;
    var video_id = 0;
    var meta = @json($meta_data);
    var theme;
    if (meta) {
        if (meta['themes']) {
            if (meta['themes']['name'])
                theme = meta['themes']['name'];
        } else
            theme = "default-theme";
    } else {
        theme = "default-theme";
    }
    var page_url = window.location.pathname;
    q = 'q=' + searchData + '&s=' + sort + '&page=' + page + '&preview=' + preview + '&filterData=' + filterData + '&shareable_link=' + shareable_link + '&theme=' + theme + '&filter=' + JSON.stringify(filter)
    @php
        if(isset($video_id)){
            @endphp
            q = q+'&video_id=' + '{{$video_id}}';
            @php
        }
        @endphp
        $.ajax({
            url: "{{route('admin.campaigns.ajaxProductData')}}" + '?' + q,
            type: "get",
            datatype: "html"
        }).done(function(data) {
            
            $("#catalog_prod").empty().html(data);
            $('.loading').hide();
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            $('.loading').hide();
        });

</script>
@endsection