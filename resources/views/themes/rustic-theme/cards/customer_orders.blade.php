
<div class="card border-radius shadow_mob_zero">
    @foreach($orders as $order)
    @php
    $order_meta_data= json_decode($order->meta_data, true);

    if(App\orders::STATUS[$order->order->status] == 'Order Recieved')
    $class="badge-primary";
    elseif (App\orders::STATUS[$order->order->status] == 'Pending')
    $class="badge-warning";
    elseif (App\orders::STATUS[$order->order->status] == 'Accepted')
    $class="badge-success";
    elseif (App\orders::STATUS[$order->order->status] == 'Shipped')
    $class="badge-success";
    elseif (App\orders::STATUS[$order->order->status] == 'Delivered')
    $class="badge-success";
    elseif (App\orders::STATUS[$order->order->status] == 'Rejected')
    $class="badge-danger";
    elseif (App\orders::STATUS[$order->order->status] == 'Cancelled')
    $class="badge-danger";
    elseif (App\orders::STATUS[$order->order->status] == 'Failed')
    $class="badge-danger";
    @endphp
    @if(isset($order->order->transactions->amount))


    {{--<div class="card ecommerce-card m-2" id="order_list_{{$order->order_id}}">
        <div class="card-body">
            <div class="row">
                <div class="col col-md-4 order-1 cursor-pointer" onclick="location.href='{{ route('customer.show.order_details',['id'=>$order->id,'shareable_link'=> $sLink]) }}';">

                    <h5><span class='badge {{$class}}' id='colorbtn'>{{App\orders::STATUS[$order->order->status]}}</span></h5>
                    <h5> Order <b>#{{$order->order->order_ref_id}}</b></h5>
                    <span class="mb-1">Transaction .ID</span>
                    @if(isset($order->order->transactions->transaction_id))
                    <h5><b>#{{$order->order->transactions->transaction_id}}</b></h5>
                    @endif

                </div>
                <div class="col col-md-4 order-2 d-flex flex-column justify-content-md-center text-md-center cursor-pointer" onclick="location.href='{{ route('customer.show.order_details',['id'=>$order->id,'shareable_link'=> $sLink]) }}';">

                    <span>Delivered to </span></br>
                    <span class="h5"><b> {{' '.$order->addresstype}}</b></span>

                </div>
                <div class="col col-md-4 order-3 text-md-right cursor-pointer" onclick="location.href='{{ route('customer.show.order_details',['id'=>$order->id,'shareable_link'=> $sLink]) }}';">
                    <h6 class="mb-md-2" id="text-light">{{ date('d-m-Y', strtotime($order->created_at)) }}</h6>
                    <span>Total Payment</span>
                    <h4><b>{{ $order->order->transactions->amount }}</b></h4>

                </div>
            </div>
        </div>

    </div>--}}
    <div class="pad_mob_05">
            <div class="row m-2 pad_mob_1">
                <div class="col-lg-3 col-md-3 col-sm-12 order-1 cursor-pointer d-flex flex-column justify-content-md-center text-md-center" onclick="location.href='{{ route('customer.show.order_details',['id'=>$order->id,'shareable_link'=> $sLink]) }}';">
                    <div class="p-1">
                        <h5 class="order_head"><u><b> Order ID #{{$order->order->order_ref_id}}</b></u></h5>
                        <h5 class="order_text"> Order Placed {{ date('d/m/Y', strtotime($order->created_at)) }}</h5>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-3 col-sm-12 order-2 d-flex flex-column justify-content-md-center text-md-center cursor-pointer" onclick="location.href='{{ route('customer.show.order_details',['id'=>$order->id,'shareable_link'=> $sLink]) }}';">
                    <div class="p-1">
                        <h5 class="order_head"><b>Total Price</b></h5>
                        <h5 class="order_text">₹{{ $order->order->transactions->amount }}</h5>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-3 col-sm-12 order-3 d-flex flex-column justify-content-md-center text-md-center cursor-pointer" onclick="location.href='{{ route('customer.show.order_status',['id'=>$order->id,'shareable_link'=> $sLink]) }}';">
                    <div class="p-1">
                        <h5 class="status_head"><b>Order Status</b></h5>
                        <h5 class="order_text">{{App\orders::STATUS[$order->order->status]}}</h5>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-12 order-4 d-flex flex-column justify-content-md-center text-md-center cursor-pointer" >
                    <div class="p-1">
                        <button class="text-white border-button m-1 btn btn-md brown_back btn_width_90" onclick="location.href='{{ route('customer.show.order_status',['id'=>$order->id,'shareable_link'=> $sLink]) }}';" id="colorbtn"><span >Track Order</span></button><br>
                        @if($order->order->status < 3)
                        <button class="text-brown border-button m-1 btn btn-md track_border btn_width_90 " onclick="cancelOrder({{$order->order->id}})"><span >Cancel Order</span></button>
                        @endif
                    </div>
                </div>
            </div>
            <hr>
    </div>

    @endif

    @endforeach

   
    @if(count($orders)==0)
        @if(count($status) == 1)
            @if(in_array('All',$status))
            <div class="text-center my-2 p-2">
            <h4 >You haven't placed an order yet. We are excited to recieve your order</h4>
            <a href="{{$homeLink}}" class="btn btn-primary">Go to shop</a>
            </div>
            @else
            <div class="text-center">
            <h4 >No orders available matching your filter criteria</h4>
            <a href="#" onclick="window.location.reload();" class="btn btn-primary">Show all orders</a>
            </div>
            @endif
        @endif
    @endif
</div>

<script>
function cancelOrder(order_id){
    var url = "{{route('customer.order.cancel')}}"

swal.fire({
    icon:'warning',
    title:'Write the comments :',
    html:'<div>To cancel the order : </div>'+
  '<textarea row="5" cols="40" id="comment"></textarea></div> <br>',
    showCancelButton:true,
    cancelButtonText:"Cancel",
    confirmButtonText:"Ok",
    cancelButtonColor: "#DD6B55",
    preConfirm: function() {
        return new Promise((resolve, reject) => {
            resolve({
                remarks: $('#comment').val(),
            });
        });
    }
}).then(function(result){

    if(result.value)
    {
        console.log(result.value.remarks)
       if(result.value.remarks != '') {
           console.log(url)

        $.ajax({
                url: url,
                type: "POST",
                data: {
                    order_id:order_id,
                    sharablelink:"{{$sLink}}",
                    remarks: result.value.remarks,
                    status: 6,
                },
                success:function(data){
                    console.log(data);
                    Swal.fire({
                        type: "success",
                        title: 'order cancelled',
                        text: 'Your order is cancelled',
                        confirmButtonClass: 'btn btn-success',
                    }).then((result) => {
                        window.location.reload();
                });
                },
        });
       }
       else{
        Swal.fire({
            icon: 'error',
            title: 'write something in comment'
            });
       }
    }
})
}
</script>