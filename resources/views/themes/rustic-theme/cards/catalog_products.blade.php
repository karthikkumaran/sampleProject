@section('styles')
    <style>
        .bg-white {
            background-color: #fff;
        }

        .outer {
            width: 60px;
            height: 60px;
            position: relative;
            background-color: #fff;
        }

        .first-circle {
            width: 100%;
            height: 100%;
            border-radius: 50%;
            background-color: #ccc;
        }

        .second-circle {
            width: 90%;
            height: 90%;
            border-radius: 50%;
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            margin: auto;
            background-color: white;
        }

        .paddingCart {
            padding: 0.5rem;
        }
    </style>
@endsection
@php
$productcount = 0;
if(empty($meta_data['settings']['currency']))
        $currency = "₹";
else
    if($meta_data['settings']['currency_selection'] == "currency_symbol")
        $currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
    else
        $currency = $meta_data['settings']['currency'];
@endphp

<h1 class="text-center mb-3 mt-5">{{$campaign->name}}</h1>
<div class="row m-1" >
        @foreach ($data as $key => $card)
                @php
                    $product = $card->product;
                    if($product->discount != null)
                    {
                        $discount_price = (float)$product->price * ((float)$product->discount/100);
                        $discounted_price = round(($product->price - $discount_price),2);
                    }
                    $obj_id = "";
                    $obj_3d = false;
                    if(count($product->photo) > 0){
                    $img = $product->photo[0]->getUrl('thumb');
                    $objimg = asset('XR/assets/images/placeholder.png');
                    $obj_id = "";
                    $obj_3d = false;
                    } else if(count($product->arobject) > 0){
                        if(!empty($product->arobject[0]->object))
                        if(!empty($product->arobject[0]->object->getUrl('thumb'))) {
                            $img = $product->arobject[0]->object->getUrl('thumb');
                        }
                    } else {
                    $img = asset('XR/assets/images/placeholder.png');
                    $objimg = asset('XR/assets/images/placeholder.png');
                    }

                    foreach ($product->arobject as $key => $arobject) {
                            if(!empty($arobject->object)){
                                if($arobject->ar_type == "face") {
                                    if(!empty($arobject->object->getUrl('thumb'))) {
                                        $objimg = $arobject->object->getUrl('thumb');
                                        $obj_id = $arobject->id;
                                    }
                                }
                            }
                            //echo $arobject->ar_type;
                            if($arobject->ar_type == "surface") {

                                if(!empty($arobject->object_3d)) {
                                    $obj_3d = true;
                                }
                                if(!empty($arobject->object_3d_ios)) {
                                    $obj_3d = true;
                                }
                            }
                    }

                    if($campaign->is_start == 1) {
                        if(count($product->categories) > 0)
                        {
                            $categoryLink = route('campaigns.publishedProductByCategory',[$product->categories[0]['id'],$campaign->public_link]);
                        }
                        $catalogId = $campaign->public_link;
                        $tryonLink = route('campaigns.publishedTryOn',$campaign->public_link)."?sku=".$product->id;
                        $viewARLink = route('campaigns.publishedViewAR',$campaign->public_link)."?sku=".$product->id;
                        $catalogDetailsLink = route('campaigns.publishedCatalogDetails',[$product->id,$campaign->public_link]);
                        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
                    }else{
                        if(count($product->categories) > 0)
                        {
                            $categoryLink = route('campaigns.publishedProductByCategory',[$product->categories[0]['id'],$campaign->preview_link]);
                        }
                        $catalogId = $campaign->preview_link;
                        $tryonLink = route('admin.campaigns.previewTryOn',$campaign->preview_link)."?sku=".$product->id;
                        $viewARLink = route('admin.campaigns.previewViewAR',$campaign->preview_link)."?sku=".$product->id;
                        $catalogDetailsLink = route('admin.campaigns.previewCatalogDetails',[$product->id,$campaign->preview_link]);
                        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
                    }
                @endphp
                

                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div  class="card shadow-lg rounded-lg border-none " 
                    style= "box-shadow: 0 1rem 1rem rgb(34 41 47 / 18%) !important; min-height:34rem !important;">
                    
                                        <div class="card-body text-center padx1">
                                            <div> 

                                 
                                    
                                                    <div class="h-100 ">
                                                    <a href="{{$catalogDetailsLink}}"><img id="product" src="{{$img}}" class="img-responsive  " alt="..."></a>
                                                    </div>

                                  

                                 

                                                    <div class="allx1">
                                
                                                        <div class="mt-5">
                                                            <h6 class="productText">
                                                                <b>@if(isset($product->name)){{$product->name}}
                                                                @else <br>
                                                                
                                                            @endif</b></h6>
                                                            </div> 

                                                    @if($product->discount != null)
                                                        <div class="mt-2 productText">
                                                        <span class="text-decoration-line-through discount_text">₹{{number_format($product->price,2)}}
                                                        </span> <b>₹{{number_format($discounted_price,2)}}</b>  <span id="offer" class="bg-warn">{{$product->discount}}% OFF</span>
                                                        </div>
                                                    @else
                                                        <div class="mt-2 productText">
                                                            <b class="bg-light" style="background: white !important;">
                                                                @if(isset($product->price))₹{{$product->price}}
                                                                @else
                                                                <br>
                                                                @endif     
                                                            </b>
                                                            
                                                        </div>
                                                    @endif 

                                                    <div class="mt-2">
                                                    @if((@isset($meta_data['themes']['product_details_display'])) && ($meta_data['themes']['product_details_display'] == "new_page"))
                                                        @if($product->out_of_stock != null)
                                                            <div class="cart bg-maroon paddingCart" style="background: #d9534f !important;">
                                                                <div class="">
                                                                    Out of stock
                                                                </div>
                                                            </div>
                                                            @else
                                                            <div class="cart quantityController bg-maroon paddingCart cardborder"  >
                                                                @if(!empty($product->shop) && $product->external_shop == 1)
                                                                <a href="{{$product->link}}" style="color:white; text-decoration: none" target="_blank">{{$product->shop}}</a>
                                                                @else
                                                                @if($product->has_variants == 1)
                                                                <a href="{{$catalogDetailsLink}}" class="a-tag-text"><div class="item-wrapper d-inline-flex  h-100 txtx" onclick="getProductDetails({{$card->id}},{{$campaign->id}})" data-toggle="modal" data-target="#productDetailsModal" style="cursor:pointer">
                                                                    {{-- <i class="feather icon-box m-2"></i> --}}
                                                                    View Product</div></a>
                                                                @else
                                                                    <i class="feather icon-shopping-cart txtx"></i>
                                                                    <span class="add-to-cart 2 pointer" onclick="addToCart({{$product->id}})" data-id="{{$product->id}}" data-name="{{$product->name ?? ''}}" data-quantity="{{$product->minimum_quantity ?? '1'}}" data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-maximum_quantity="{{ $product->maximum_quantity ?? '10000' }}"
                                                                        data-price="{{$product->price ?? '0'}}" data-productweight="{{$product->productweight ?? '0'}}" data-length="{{$product->length ?? '0'}}" data-width="{{$product->width ?? '0'}}" data-height="{{$product->height ?? '0'}}"
                                                                        data-discounted_price="{{$product->discount ? $discounted_price:'0'}}" data-catalogid="{{$catalogId}}"
                                                                        data-catagory="{{(count($product->categories) > 0)?$product->categories[0]['name']:''}}"
                                                                        data-img="{{$img}}" data-sku="{{ $product->sku }}" data-product_id="{{ $product->id }}"
                                                                        data-quantity="{{ $product->minimum_quantity ?? '1' }}">Add to Cart
                                                                    </span>
                                                                @endif
                                                                
                                                                <span class="view-in-cart d-none 10">
                                                                    <div class="input-group quantity-counter-wrapper">
                                                                        <input type="text" id="product_qty_{{$product->sku}}" class="quantity-counter" onclick="productQuantity({{$product->id}})"  data-id="{{$product->sku}}" value="{{$product->minimum_quantity ?? '1'}}" data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-maximum_quantity="{{ $product->maximum_quantity ?? '10000' }}" data-sku="{{ $product->sku }}">
                                                                        {{-- <input type="text" id="product_qty_{{$product->sku}}" class="quantity-counter" data-id="{{$product->id}}" value="{{$product->minimum_quantity ?? '1'}}" data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-sku="{{ $product->sku }} " data-maximum_quantity="{{ $product->maximum_quantity ?? '10000' }}" > --}}
                                                                    </div>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        @endif
                                                    @else
                                                        @if($product->out_of_stock != null)
                                                            <div class="cart bg-maroon paddingCart" style="background: #d9534f !important;">
                                                                <div class="">
                                                                    Out of stock
                                                                </div>
                                                            </div>
                                                        @else
                                                            <div class="cart quantityController bg-maroon paddingCart cardborder"  >
                                                                @if(!empty($product->shop) && $product->external_shop == 1)
                                                                <a href="{{$product->link}}" style="color:white; text-decoration: none" target="_blank">{{$product->shop}}</a>
                                                                @else
                                                                @if($product->has_variants == 1)
                                                                <a href="{{$catalogDetailsLink}}" class="a-tag-text"><div class="item-wrapper d-inline-flex  h-100 txtx" onclick="getProductDetails({{$card->id}},{{$campaign->id}})" data-toggle="modal" data-target="#productDetailsModal" style="cursor:pointer;font-weight: 700;font-size: 18px;">
                                                                    {{-- <i class="feather icon-box m-2"></i> --}}
                                                                    View Product</div></a>
                                                                @else
                                                                    <i class="feather icon-shopping-cart txtx"></i>
                                                                    <span class="add-to-cart 2 pointer txtx" onclick="addToCart({{$product->id}})" data-id="{{$product->id}}" data-name="{{$product->name ?? ''}}" data-quantity="{{$product->minimum_quantity ?? '1'}}" data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-maximum_quantity="{{ $product->maximum_quantity ?? '10000' }}"
                                                                        data-price="{{$product->price ?? '0'}}" data-productweight="{{$product->productweight ?? '0'}}" data-length="{{$product->length ?? '0'}}" data-width="{{$product->width ?? '0'}}" data-height="{{$product->height ?? '0'}}"
                                                                        data-discounted_price="{{$product->discount ? $discounted_price:'0'}}" data-catalogid="{{$catalogId}}"
                                                                        data-catagory="{{(count($product->categories) > 0)?$product->categories[0]['name']:''}}"
                                                                        data-img="{{$img}}" data-sku="{{ $product->sku }}" data-product_id="{{ $product->id }}"
                                                                        data-quantity="{{ $product->minimum_quantity ?? '1' }}">Add to Cart
                                                                    </span>
                                                                @endif
                                                                
                                                                <span class="view-in-cart d-none 10">
                                                                    <div class="input-group quantity-counter-wrapper">
                                                                        <input type="text" id="product_qty_{{$product->sku}}" class="quantity-counter" onclick="productQuantity({{$product->id}})"  data-id="{{$product->sku}}" value="{{$product->minimum_quantity ?? '1'}}" data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-maximum_quantity="{{ $product->maximum_quantity ?? '10000' }}" data-sku="{{ $product->sku }}">
                                                                        {{-- <input type="text" id="product_qty_{{$product->sku}}" class="quantity-counter" data-id="{{$product->id}}" value="{{$product->minimum_quantity ?? '1'}}" data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-sku="{{ $product->sku }} " data-maximum_quantity="{{ $product->maximum_quantity ?? '10000' }}" > --}}
                                                                    </div>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        @endif
                                                    @endif
                                                    </div> 
                                
                                                </div>
                             
                                            </div>
                                        </div>

                                
                                
                </div>  
                </div>
                @endforeach
</div>
@if($productcount == 0 && !empty($video_id))
    <div class=" d-flex justify-content-center text-center text-light m-1">
                <h4><b class="text-light">There is no product  available for this video</b></h4>
        </div>
@endif

@if( (($is_search == true) or ($is_filter == true)) and (count($data) == 0))
    <h3 class="text-light text-center p-2 w-100">"No products found matching your search or filter criteria"</h3>
@endif
                {!! $data->render() !!}
                <script src="{{asset('XR/assets/js/rustic_cart.js')}}"></script>


<script src="{{asset('XR/app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')}}">
<script>
// events tracking functions
    $(document).ready(async function () {
        if(typeof(umami) == 'undefined'){
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = "https://stats.weavexr.com/umami.js";
        s.setAttribute('data-website-id',"b4385fca-c77c-4003-b4ce-c02475d2e079");
        $("head").append(s);
        }
    });

  function tryOn(product_id){
        umami('tryon-button-clicked-sku-'+product_id);
    }

  function addToCart(product_id){
        umami('add-to-cart-product-list-sku-'+product_id);
    }

  function viewInCart(product_id){
        umami('view-in-cart-sku-'+product_id);
    }
    function productQuantity(product_id){
        umami('product-quantity-count-sku-'+product_id);
    }

    $('#productssearchTotal').empty().html({{$data->total()}});
// checkout quantity counter
  var quantityCounter = $(".quantity-counter"),
    CounterMin = 1,
    CounterMax = 10000;

  if (quantityCounter.length > 0) {
    quantityCounter.TouchSpin({
      min: CounterMin,
      max: CounterMax
    }).on('touchspin.on.startdownspin', function () {
      var $this = $(this);
      umami('product-quantity-decrement-product-list-sku-'+ $(this).data("id"));
      let min_qty = $(this).data("minimum_quantity") || 1;

      $this.trigger("touchspin.updatesettings", {min: min_qty})
      console.log(min_qty);
      $('.bootstrap-touchspin-up').removeClass("disabled-max-min");
      if ($this.val() <= min_qty) {
            $(this).siblings().find('.bootstrap-touchspin-down').addClass("disabled-max-min");
            // $(this).siblings().find('.bootstrap-touchspin-down').addClass("danger-max-min");
            // obj.removeItemFromCart(Number($(this).data('id')));
            // obj.totalCart();
            // $(this).closest('.view-in-cart').siblings('.add-to-cart').removeClass("d-none");
            // $(this).closest('.view-in-cart').siblings('.add-to-cart').addClass("d-inline-block");
            // $(this).closest('.view-in-cart').removeClass("d-inline-block");
            // $(this).closest('.view-in-cart').addClass("d-none");
            // e.stopImmediatePropagation();
        }
        obj.setCountForItem(Number($this.data('id')),$this.val());
        //$("#product_qty_"+$this.data('id')).val($this.val());

    }).on('touchspin.on.startupspin', function () {
      var $this = $(this);
      umami('product-quantity-increment-product-list-sku-'+ $(this).data("id"));
      let max_qty = $(this).data("maximum_quantity") || 10;
      $('.bootstrap-touchspin-down').removeClass("disabled-max-min");
      if ($this.val() <= max_qty) {
        $(this).siblings().find('.bootstrap-touchspin-up').addClass("disabled-max-min");
      }
      obj.setCountForItem(Number($this.data('id')),$this.val());
      //$("#product_qty_"+$this.data('id')).val($this.val());

    }).on('change', function () {
      var $this = $(this);
      let min_qty = $(this).data("minimum_quantity") || 1;
      let max_qty = $(this).data("maximum_quantity") || 10;
    //   if ($this.val() == 1) {
    //     $(this).siblings().find('.bootstrap-touchspin-down').addClass("danger-max-min");
    //     }
      if ($this.val() == 0) {
            obj.setCountForItem(Number($this.data('id')),min_qty);
            $("#product_qty_" + $this.data('id')).val(min_qty);
        } else if ($this.val() > CounterMax) {
            obj.setCountForItem(Number($this.data('id')),CounterMax);
      }
      else if($this.val() < min_qty){
        obj.setCountForItem(Number($this.data('id')),min_qty);
        $("#product_qty_" + $this.data('id')).val(min_qty);
      }
      else if($this.val() >= max_qty){
        obj.setCountForItem(Number($this.data('id')),max_qty);
        $("#product_qty_" + $this.data('id')).val(max_qty);
      }
      else {
         obj.setCountForItem(Number($this.data('id')),$this.val());
      }
    });
  }
  obj.setCountForItem = function(id, qty) {
    for(var i in cart) {
      if (cart[i].id === id) {
        cart[i].qty = qty;
        break;
      }
    }
    saveCart();
    obj.totalCart();
  };
  obj.removeItemFromCart = function(id) {
        for (var item in cart) {
            if (cart[item].id === id) {
                //   cart[item].qty --;
                //   if(cart[item].qty === 0) {
                cart.splice(item, 1);
                //   }
                //   break;
            }
        }
        saveCart();
    }

  $( document ).ready(function() {
    if (localStorage.getItem("shoppingCart") != null) {
        cart = JSON.parse(localStorage.getItem('shoppingCart'));
        var cartItemCount = count = cart.length;
        if(cart.length > 0) {
            var products = [];
            for(var item in cart) {
                var id = cart[item].id, qty = cart[item].qty;
                $("#product_qty_"+(id)).val(qty);
            }

        }
    }
    });

</script>
<script src="{{asset('themes/rustic-theme/js/script.js')}}"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
            <script  src="{{asset('themes/rustic-theme/js/jquery.nicescroll.min.js')}}"></script>
            
            <script>
                $(".contain").niceScroll();
                console.log('hghhds');
            </script>

<style>
    .productText {
        color: #432F2F;
        font-weight: 800;
        font-size: 18px;
        line-height: 22px;
    }

    .txtx {
        font-weight: 700;
        font-size: 18px;
        line-height: 22px;
    }

    .product {
        width: 165.82px;
        height: 238.36px;
        left: 220.09px;
        top: 740.19px;
    }

    .bg-light {
        font-style: normal;
        font-weight: 700;
        font-size: 18px;
        line-height: 22px;
        background-color:white;
    }

    .padx1 {
        margin-top: 30px;
        margin-bottom: 30px;
    }

    .cardborder {
        width: 95%;
        height: 40px;
        left: 599px;
        top: 1148.9px;
        font-weight:700;
        font-size: 18px;
        line-height: 22px;
    }

    .allx1 {
        margin-left: 50px;
    }
   .border-none{
    margin-right:16px;
   } 
    

</style>