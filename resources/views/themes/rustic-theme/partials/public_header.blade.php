    <style>
        .badge {
            display: inline-block;
            padding: 0.35em 0.65em;
            font-size: .75em;
            font-weight: 700;
            line-height: 1;
            color: #fff;
            text-align: center;
            white-space: nowrap;
            vertical-align: baseline;
            border-radius: 1rem !important;
        }
    </style>
    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@6.1.1/css/fontawesome.min.css"> -->
        
       <!-- own css file  -->
    <link rel="stylesheet" href="{{asset('themes/rustic-theme/css/style.css')}}">
    
<!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous"> -->
    

    <!-- bootstrap 4 jquery  -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/booxtstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script> -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- boostrap 5 js cdn -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <!-- bootstrap 5 css cdn   -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    
    @php
        if(!empty($userSettings->customdomain)){

            $homeUrl = '//'.$_SERVER['HTTP_HOST'];  

        }else if(!empty($userSettings->subdomain)) {

            $homeUrl = '//'.$_SERVER['HTTP_HOST'];  

        }else{
        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')   
                $homeUrl = "https://";   
                else  
                    $homeUrl = "http://";   
                // Append the host(domain name, ip) to the URL.   
                $homeUrl.= $_SERVER['HTTP_HOST'];   
                
                $homeUrl.= $_SERVER['REQUEST_URI']; 

        }

        if($userSettings->is_start == 1) {
            $shareableLink = $userSettings->public_link;
        }else{
            $shareableLink = $userSettings->preview_link;
        }
        if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')   
         $url = "https://";   
        else  
            $url = "http://";   
        // Append the host(domain name, ip) to the URL.   
        $url.= $_SERVER['HTTP_HOST'];   
        
        $url.= $_SERVER['REQUEST_URI'];    
        
        if(!empty($meta_data['footer_settings']))
            $footer_settings = $meta_data['footer_settings'];

        $catalog = $userSettings->user->productCampaign;
        $campaign = $catalog[count($catalog)-1]; 
        if($campaign->is_start == 1) {
            $catalogLink = route('campaigns.published',$campaign->public_link);
            $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
        }else{
            $catalogLink = route('admin.campaigns.preview',$campaign->preview_link);
            $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
        }
        
    @endphp
@if(isset($meta_data['announcement_settings']) && !empty($meta_data['announcement_settings']['announcement_text']))
    <nav class="navbar sticky-top navbar-expand-lg navbar-light cyan shadow-lg p-0">
        <div class="w-100" style="background: {{$meta_data['announcement_settings']['announcement_background_color']}}">
            <marquee style="color: {{$meta_data['announcement_settings']['announcement_text_color']}}; margin-top: 5px;">{!! $meta_data['announcement_settings']['announcement_text'] !!}</marquee>
        </div>
    </nav>
@endif
    <nav class="navbar navbar-expand-lg navbar-light bg-white p-2 sticky-top  shadow ">
        
        <div class="container">
        
            <div class="navbar-brand">
                 @if(!empty($meta_data['storefront']))
                        @if(!empty($userSettings->getFirstMediaUrl('storelogo')))
                            <a class="nav-link" href="{{$homeLink}}"><i class="ficon"></i><img class=" lazyload" loading="lazy" data-src="{{ $userSettings->getFirstMediaUrl('storelogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                            src="{{asset('landing/img/blank.gif')}}" onerror="this.onerror=null;this.src='{{asset('landing/img/blank.gif')}}';" alt="Logo" style="height: 50px;"></a> 
                        @else
                            <a class="nav-link" href="{{$homeLink}}"><i class="ficon"></i><h5 style="color: {{ $meta_data['splash']['brand_color'] ?? '' }}"><b>{{$meta_data['storefront']['storename']}}</b></h5></a>
                        @endif
                @endif
            </div>
    
             
            <button type="button" class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" >
                <span class="navbar-toggler-icon"></span>
            </button>
            
            <div class="collapse navbar-collapse " id="navbarCollapse">
            <ul id="size" class="navbar-nav ms-auto justify-content-right align-items-center">
                @if(request()->is('/') || request()->is('s/*'))
                <li class="nav-item">
                    <a class="nav-link " href="{{$homeLink}}" style=" color:#6C4E4F;">
                    <h5 class="mb-0 mt-1 text-brown">Home</h5>
                        <span class="d-flex justify-content-center mt-1">
                            <img src="{{asset('themes/rustic-theme/images/png/linw.png')}}" alt="">
                        </span>  
                    </a>
                </li>
                @else
                <li class="nav-item">
                    <a class="nav-link " href="{{$homeLink}}">Home</a>
                </li>
                @endif
                
                @if($url == $catalogLink)
                <li class="nav-item">
                    <a class="nav-link " href="{{$catalogLink}}" style="color:#6C4E4F;">
                    <h5 class="mb-0 mt-1 text-brown"><b>Products</b></h5>
                        <span class="d-flex justify-content-center mt-1">
                            <img src="{{asset('themes/rustic-theme/images/png/linw.png')}}" alt="">
                        </span>
                </a>
                </li>
                @else
                <li class="nav-item">
                    <a class="nav-link" href="{{$catalogLink}}">Products</a>
                </li>
                @endif

                @if($url == route('store.storeAboutUs', [$shareableLink]))
                <li class="nav-item">
                    <a class="nav-link " href="{{route('store.storeAboutUs', [$shareableLink])}}" style="color:#6C4E4F;">
                    <h5 class="mb-0 mt-1 text-brown"><b>About Us</b></h5>
                        <span class="d-flex justify-content-center mt-1">
                            <img src="{{asset('themes/rustic-theme/images/png/linw.png')}}" alt="">
                        </span>
                </a>
                </li>
                @else
                <li class="nav-item">
                    <a class="nav-link" href="{{route('store.storeAboutUs', [$shareableLink])}}">About Us</a>
                </li>
                @endif 

                @if($url == route('store.storeOrigin', [$shareableLink]))
                <li class="nav-item">
                    <a class="nav-link " href="{{route('store.storeOrigin', [$shareableLink])}}" style="color:#6C4E4F;">
                    <h5 class="mb-0 mt-1 text-brown"><b>Product Origin</b></h5>
                        <span class="d-flex justify-content-center mt-1">
                            <img src="{{asset('themes/rustic-theme/images/png/linw.png')}}" alt="">
                        </span>
                </a>
                </li>
                @else
                <li class="nav-item">
                    <a class="nav-link" href="{{route('store.storeOrigin', [$shareableLink])}}">Product Origin</a>
                </li>
                @endif

                @if($url == route('store.storeFaq', [$shareableLink]))
                <li class="nav-item">
                    <a class="nav-link " href="{{route('store.storeFaq', [$shareableLink])}}" style="color:#6C4E4F;">
                    <h5 class="mb-0 mt-1 text-brown"><b>FAQ</b></h5>
                        <span class="d-flex justify-content-center mt-1">
                            <img src="{{asset('themes/rustic-theme/images/png/linw.png')}}" alt="">
                        </span>
                </a>
                </li>
                {{-- <li class="nav-item">
                    <a class="nav-link text-brown" href="{{route('store.storeFaq', [$shareableLink])}}" 
                    style="text-decoration:underline; color:#6C4E4F;">
                    <h5 class="mb-0"><b>FAQ</b></h5>
                </a>
                </li> --}}
                @else
                <li class="nav-item">
                    <a class="nav-link" href="{{route('store.storeFaq', [$shareableLink])}}">FAQ</a>
                </li>
                {{-- <li class="nav-item">
                    <a class="nav-link" href="{{route('store.storeFaq', [$shareableLink])}}">FAQ</a>
                </li> --}}
                @endif
                
                {{--@if(isset($meta_data['content_settings']['show_contact_us']) && $meta_data['content_settings']['show_contact_us'] == 1)
                                    @php
                                        $content_line = true;
                                    @endphp
                                    @if($url == route('store.storeContactUs', [$shareableLink]))
                                    <li class="nav-item ">
                                        <a class="nav-link " href="{{route('store.storeAboutUs', [$shareableLink])}}" style=" color:#6C4E4F;">
                                        <h5 class="mb-0 mt-1 text-brown"><b>Contact Us</b></h5>
                                            <span class="d-flex justify-content-center mt-1">
                                                <img src="{{asset('themes/rustic-theme/images/png/linw.png')}}" alt="">
                                            </span>
                                        </a>
                                    </li>
                                    @else
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{route('store.storeContactUs', [$shareableLink])}}">Contact Us</a>
                                    </li>
                                    @endif
                                @else
                    
                @endif--}}

                @if(Auth::guard('customer')->check())
                    <li class="nav-item dropdown dropdown-user p-1 bordershow badge-brown lg_hide tabl_hide">
                        <a class="nav-link dropdown-toggle dropdown-user-link " id="dropdown-user" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <!-- <i class="fa fa-user-circle fa-2x m-0"></i>  -->
                                <i class="fa fa-user m-0" aria-hidden="true" style="color:white;"></i>
                                <div class="text-wrap badge badge-brown text-white p-1 text-left m-0" style="width:min-content;">
                                    {{Auth::guard('customer')->user()->name}}
                                </div>
                                {{--<span class="h5 text-wrap">{{Auth::guard('customer')->user()->name}}</span> --}}
                        </a>
                        <div class="dropdown-menu dropdown-menu-right m-0" aria-labelledby="dropdown-user">
                            <a class="dropdown-item" href="{{route('customer.profile',$shareableLink)}}"> <i class=" fa fa-user"></i> My Profile</a>
                            <a class="dropdown-item" href="{{route('customer.orders',$shareableLink)}}"> <i class=" fa fa-list-ul"></i> My Orders</a>
                            <div class=" dropdown-divider" style="display:hide;"></div>
                            <a class="dropdown-item m-0" href="{{ route('customer.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" style="margin-block: 10px;margin-inline: 8px;">
                            <i class="fa fa-sign-out"></i> Logout</a>
                            <form id="logout-form" action="{{ route('customer.logout') }}" method="POST" style="display: none;">
                            @csrf
                            </form>
                        </div>
                    </li>
                    <li class="nav-item dropdown dropdown-user  sm_hide tabl_show">
                            <a class="nav-link dropdown-toggle dropdown-user-link bordershow badge-brown text-center p-1" id="dropdown-user" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <!-- <i class="fa fa-user-circle fa-2x m-0"></i>  -->
                                    <i class="fa fa-user m-0" aria-hidden="true" style="color:white;"></i>
                                    <div class="text-wrap badge badge-brown text-white p-1 text-left m-0" style="width:min-content;">
                                        {{Auth::guard('customer')->user()->name}}
                                    </div>
                                    {{--<span class="h5 text-wrap">{{Auth::guard('customer')->user()->name}}</span> --}}
                            </a>
                        <div class="dropdown-menu dropdown-menu-right m-0" aria-labelledby="dropdown-user">
                            <a class="dropdown-item" href="{{route('customer.profile',$shareableLink)}}"> <i class=" fa fa-user"></i> My Profile</a>
                            <a class="dropdown-item" href="{{route('customer.orders',$shareableLink)}}"> <i class=" fa fa-list-ul"></i> My Orders</a>
                            <div class=" dropdown-divider" style="display:hide;"></div>
                            <a class="dropdown-item m-0" href="{{ route('customer.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" style="margin-block: 10px;margin-inline: 8px;">
                            <i class="fa fa-sign-out"></i> Logout</a>
                            <form id="logout-form" action="{{ route('customer.logout') }}" method="POST" style="display: none;">
                            @csrf
                            </form>
                        </div>
                    </li>
                @else
                    <li class="nav-item m-1">
                        @php 
                        $loginLink = $homeLink.'/customer/login';
                    @endphp 
                        @if($url == route('customer.storelogin', [$shareableLink]))
                            <a href="{{ route('customer.storelogin' , [$shareableLink]) }}" style="text-decoration: none;">
                            <input id="" type="button" class=" nav-link btn text-white bg-maroon bordershow" value="    Login     ">
                            </a> 
                        @else 
                            <a href="{{ route('customer.storelogin' , [$shareableLink]) }}" style="text-decoration: none;">
                            <input id="" type="button" class=" nav-link btn text-black bordershow" value="    Login     ">
                            </a> 
                        @endif
                    </li>
                @endif
                @if(!Auth::guard('customer')->check())
                <li class="nav-item m-1">
                    <a href="{{ route('customer.storeregister' , [$shareableLink]) }}" style="text-decoration: none;">
                    @if($url == route('customer.storeregister', [$shareableLink]))
                    <input id="" type="button" class=" nav-link btn text-white bg-maroon bordershow" value="    Register     ">
                    @else 
                    <input id="" type="button" class=" nav-link btn text-black bordershow" value="    Register     ">
                    @endif
                    </a> 
                </li>
                @endif
                
                {{--<li id="btn" class="nav-item">
                @if(Auth::guard('customer')->check())
                       <li class="nav-item">

                            <a class="nav-link btn btn-outline-dark py-1 px-3" href="{{ route('customer.logout') }} bg-maroon" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" style="margin-block: 10px;margin-inline: 8px;">
                            {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('customer.logout') }}" method="POST" style="display: none;">

                            @csrf
                            </form>
                        </li> 

                        @else
                         <li class="nav-item">
                         @php 
                            $loginLink = $homeLink.'/customer/login';
                        @endphp 
                            <a href="{{ route('customer.storelogin' , [$shareableLink]) }}" style="text-decoration: none;">
                            <input id="colorbtn" type="button" class=" nav-link btn text-white " value="    Login     ">
                            </a> 
                        </li>
                        @endif
                </li>--}}
                
                <li class="nav-item">
                        {{-- @if(!empty($checkoutLink) && !request()->is('register/*') && !request()->is('login/*')) --}}
                        @if(!empty($checkoutLink))
                        <li class="dropdown dropdown-notification nav-item my-3">
                            <a class="" href="{{$checkoutLink}}">
                            <img id="cart" class="nav-link" src="{{asset('themes/rustic-theme/images/svg/bxs_cart-alt.svg')}}" alt="">
                                <span class="badge badge-pill badge-danger badge-up cart-item-count"></span></a>
                        </li>
                        @endif 
                    
                </li>
    
            </ul>
    
        
        </div>
    
        </nav>
<script src="{{asset('XR/assets/js/rustic_cart.js')}}"></script>
