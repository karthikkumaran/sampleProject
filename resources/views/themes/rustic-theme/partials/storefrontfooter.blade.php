<!-- footer section starts    -->
@php
        $content_line = false;

        if($userSettings->is_start == 1) {
            $shareableLink = $userSettings->public_link;
        }else{
            $shareableLink = $userSettings->preview_link;
        }


        if(!empty($meta_data['payment_settings']['payment_method']))
            $payment_settings = $meta_data['payment_settings'];
        else
            $payment_settings = null;

        if(!empty($meta_data['footer_settings']))
            $footer_settings = $meta_data['footer_settings'];

@endphp

<div id="colordiv" class="container-fluid p-5 footer">
                <div class="container ">
                    <div class="row  d-flex justify-content-between">
                        <div class="col-md-4">
                            <ul class="list-unstyled">
                                <li class="footer_title">Information</li>
                                <li><a href="{{route('store.storeAboutUs', [$shareableLink])}}"  class="footer_text">About Us</a></li>
                                @if(isset($meta_data['content_settings']['show_privacy_policy']) && $meta_data['content_settings']['show_privacy_policy'] == 1)
                                    @php
                                        $content_line = true;
                                    @endphp
                                    <span><a href="{{route('store.storePrivacyPolicy', [$shareableLink])}}" target="_blank"  class="footer_text"><li>Privacy Policy</li></a></span>
                                @else 
                                <li>Privacy Policy</li>
                                @endif
                                @if(isset($meta_data['content_settings']['show_terms_and_conditions']) && $meta_data['content_settings']['show_terms_and_conditions'] == 1)
                                    @php
                                        $content_line = true;
                                    @endphp
                                    <span><a href="{{route('store.storeTermsAndConditions', [$shareableLink])}}" target="_blank" class="footer_text"><li>Terms & Conditions</li></a></span>
                                @else 
                                <li>Terms & Conditions</li>
                                @endif
                                @if(isset($meta_data['content_settings']['show_refund_and_return_policy']) && $meta_data['content_settings']['show_refund_and_return_policy'] == 1)
                                    @php
                                        $content_line = true;
                                    @endphp
                                    <span><a href="{{route('store.storeRefundAndReturnPolicy', [$shareableLink])}}" target="_blank" class="footer_text"><li>Cancellation & Refunds Policy</li></a></span>
                                @else 
                                <li>Cancellation & Refunds Policy</li>
                                @endif
                                
                                
                                @if(isset($meta_data['content_settings']['show_contact_us']) && $meta_data['content_settings']['show_contact_us'] == 1)
                                    @php
                                        $content_line = true;
                                    @endphp
                                    <span><a href="{{route('store.storeContactUs', [$shareableLink])}}" target="_blank" class="footer_text text-dark "><li>Contact Us</li></a></span>
                                @else
                                <li>Contact Us</li>
                                @endif
                            
                            </ul>
                        </div>
                        <div class="col-md-4 ">
                            <ul class="list-unstyled">
                                <li class="footer_title"><b>Our Products</b></li>
                                <li class="footer_text">Coffee</li>
                                <li class="footer_text">Black Pepper</li>
                                <li class="footer_text">Cinnamon</li>
                                <li class="footer_text">Cardamom</li>
                                 
                            
                            </ul>

                        </div>
                        <div class="col-md-4">
                            <ul class="list-unstyled">
                                <li class="footer_title">FOLLOW US</li>
                                @if( isset($footer_settings['facebook_id']) && $footer_settings['facebook_id']!='#' )
                                <a href="https://www.facebook.com/{{$footer_settings['facebook_id']}}" target="_blank" rel="noopener noreferrer">
                                <img src="{{asset('themes/rustic-theme/images/png/ant-design_facebook-filled.png')}}" alt="">
                                </a>
                                @else
                                <img src="{{asset('themes/rustic-theme/images/png/ant-design_facebook-filled.png')}}" alt="">
                                @endif

                                @if( isset($footer_settings['instagram_link']) && $footer_settings['instagram_link']!='#' )
                                <a href="https://instagram.com/{{$footer_settings['instagram_link']}}/" target="_blank" rel="noopener noreferrer">
                                <img src="{{asset('themes/rustic-theme/images/png/ant-design_instagram-filled.png')}}" alt="">
                                </a>
                                @else
                                <img src="{{asset('themes/rustic-theme/images/png/ant-design_instagram-filled.png')}}" alt="">
                                @endif
                                
                                @if( isset($footer_settings['twitter_id']) && $footer_settings['twitter_id']!='#' )
                                <a href="https://twitter.com/{{$footer_settings['twitter_id']}}/" target="_blank" rel="noopener noreferrer">
                                <img src="{{asset('themes/rustic-theme/images/png/ant-design_twitter-square-filled.png')}}" alt="">  
                                </a>
                                @else
                                <img src="{{asset('themes/rustic-theme/images/png/ant-design_twitter-square-filled.png')}}" alt="">  
                                @endif                               
                                 
                            
                            </ul>
                        </div>
                        {{--<div class="col-md-4">
                            <ul class="list-unstyled">
                                <li class="footer_title">Subscribe now</li>
                                <li class="footer_text">Get updates on our product</li>
                                
                            </ul>

                            <div class="text-right">
                            
                                <div class="input-group">
                                    <div class="input-group-text" id="btnGroupAddon"><img src="{{asset('themes/rustic-theme/images/svg/ic_round-email.svg')}}" alt=""></div>
                                    <input type="email" class="form-control" placeholder="Enter younr email ID" aria-label="Input group example" aria-describedby="btnGroupAddon">
                                    <button id="colorbtn" class="btn text-white footer_text">Subscribe</button>
                                </div>

                            
                            </div>

                        </div> --}}

                    </div>
                </div>
            </div>
       
            <!-- footer section ends  -->
<style>
    .footer {
        position: absolute;
    }
    html body[data-col='1-column'] .content, html body[data-col='1-column'] .footer {
    width: 100% !important;
}
</style>