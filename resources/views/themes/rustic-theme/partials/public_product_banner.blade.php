<div id="colordiv" class="container-fluid lg_hide">

    

                <div class="container-fluid">
                     <div class="row ">
             
                        
                        
                <div class="col-md-6 position-relative z_high">
                            <div>
                                <h1 id="banner-text" class="position-absolute mx-2 h1 top-50 start-0 translate-middle-y text-center">
                                Authentic spices from the hills of Coorg brought to your kitchen
                                </h1>                        
                            </div>
                </div>

                
            
            
                <div class="col-md-6 h-100">  
                 
                        <img id="banimg" class="img-responsive float-right" src="{{asset('themes/rustic-theme/images/png/Banner_product_tablet.png')}}" alt="">
                    
                  </div>
               
               
               
                
            
            
            
            
            </div>
        </div>
</div>
<div id="colordiv" class="container-fluid sm_hide ">

                <div class="container">
                     <div class="row ">
             
                        
                        
                <div class="col-md-12 position-relative mt-5 z_high">
                            <div>
                                <h1 id="banner-text" class="position-absolute mx-2 my-1 h1 top-50 start-0 translate-middle-y text-center">
                                Authentic spices from the hills of Coorg brought to your kitchen</h1>                        
                            </div>
                </div>

                
            
            
                <div class="col-md-12 h-100">  
                        <img id="banimg" class="img-responsive" src="{{asset('themes/rustic-theme/images/png/Banner_product_mobile.png')}}" alt="">
                    
                  </div>
               
               
               
                
            
            
            
            
            </div>
        </div>
</div>