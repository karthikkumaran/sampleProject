@extends('themes.'.$meta_data['themes']['name'].'.layouts.customer')
@section('content')
@php
if($userSettings->is_start == 1) {
    if(!empty($userSettings->customdomain)){
        $homeLink = '//'.$userSettings->customdomain;

    }else if(!empty($userSettings->subdomain)) {
        $homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');

    }else{
        $homeLink = route('mystore.published',[$userSettings->public_link]);
    }
    }else{
        $homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
    }

    $sLink = $userSettings->public_link;
    $loginLink = $homeLink.'/customer/login';
    $registerLink = $homeLink.'/customer/register';

        if($userSettings->is_start == 1) {
            $shareableLink = $userSettings->public_link;
        }else{
            $shareableLink = $userSettings->preview_link;
        }
    

@endphp
<div class="row my-5 text-center">
    {{--@if(!empty($meta_data['storefront']))
            @if(!empty($userSettings->getFirstMediaUrl('storelogo')))
                <a class="nav-link" href="{{$homeLink}}"><i class="ficon"></i><img class=" lazyload" loading="lazy" data-src="{{ $userSettings->getFirstMediaUrl('storelogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                    onerror="this.onerror=null;this.src='{{asset('XR/app-assets/images/logo/logo.png')}}';" alt="Logo" style="height: 50px;"></a>
            @else
                <a class="nav-link" href="{{$homeLink}}"><i class="ficon"></i><h5 style="color: {{ $meta_data['splash']['brand_color'] ?? '' }}"><b>{{$meta_data['storefront']['storename']}}</b></h5></a>
            @endif
    @endif --}}
</div>
<div class="d-flex justify-content-center">
<div class="row bg-white pad_mob_15" style="width:500px;">
         <form method="POST" id="loginform" action="javascript:void(0);" enctype="multipart/form-data">
            @csrf
            
                    <p class="fw-bolder text-brown w-100 h3 " id="loginModalLabel" style="font-weight: 800;">Log In</p>
                    
                @if(session('message'))
                <div class=" mx-1 my-1 alert alert-{{session('class')}}" role="alert">
                    {{ session('message') }}
                </div>
                @endif
                <div id="login-flash-message"></div>


                 {{--   <div class="d-flex justify-content-center">
                            <a href="{{ route('customer.social.login','facebook') }}" class="btn btn-facebook mx-1"><span class="fa fa-facebook">acebook</span></a>
                            <a href="{{ route('customer.social.login','google') }}" class="btn btn-google mx-1"><span class="fa fa-google">oogle</span></a>
                    </div>
                    <hr class="hr-text mt-0" data-content="or" /> --}}

                    <div class="form-group">
                        <label class="form-label" for="register-email">Email</label>
                        <input type="email" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" required placeholder="{{ trans('global.login_email') }}" value="{{ old('email', null) }}">
                        <div class="invalid-feedback">
                            Please enter your email id
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="form-label" for="register-password">Password</label>
                        <div class="input-group input-group-merge form-password-toggle">
                            <input type="password" name="password" id="lpassword" minlength="6" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" required placeholder="{{ trans('global.login_password') }}" autocomplete="off">
                            <div class="input-group-append">
                                <span class="input-group-text cursor-pointer">
                                    <i class="fa fa-eye togglePassword" data-id="lpassword" aria-hidden="true"></i>
                                </span>
                            </div>
                            <div class="invalid-feedback">
                                Please enter your password
                            </div>
                        </div>
                    </div>

                    <div class="form-group ">
                    <div class="custom-control custom-checkbox">
                        <span class="text-start">                                  
                       <input class="custom-control-input" name="remember" id="remember" type="checkbox" tabindex="4" />
                       <label class="custom-control-label" for="remember">Remember Me</label>
                       </span>
                       <span class="float-right"><a class="btn btn-link text-brown p-1" href="{{ route('customer.storeforgotpassword' , [$shareableLink]) }}" id='forgot_password' style="padding-inline: 0px;padding-block:6px;">Forgot password?</a></span>
                    </div>

                    </div> 
                <input type="hidden" name="slink" value="{{$sLink ?? ''}}">

                <button type="submit" id='login' class="btn bg-maroon m-1 w-100" onclick="logInBtn()" >LOG IN</button>

                <h6 class="text-center my-2"><span>Don't have an account?</span>
                    <a href="{{ route('customer.storeregister' , [$shareableLink]) }}" class="text-brown" onclick="signUpBtn()" ><span><b>Sign Up</b></span></a>
                </h6>
                <h6 class="text-center my-3">
                    <span >
                    <input   type="checkbox" checked/>
                    </span> 
                    <span>
                       By creating an account or Logged in you<br>
                       agree to <a href="{{route('store.storeTermsAndConditions', $sLink)}}" class="text-primary" target="_blank">Terms & Conditions</a> and <a href="{{route('store.storePrivacyPolicy', $sLink)}}" class="text-primary" target="_blank">&nbsp;<u>Privacy Policy</u></a> 
                    </span> 
                 </h6>

        </form>                                     

</div>
</div>
@endsection