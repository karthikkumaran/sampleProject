@extends('themes.'.$meta_data['themes']['name'].'.layouts.customer')
@section('content')
@php
if($userSettings->is_start == 1) {
    if(!empty($userSettings->customdomain)){
        $homeLink = '//'.$userSettings->customdomain;

    }else if(!empty($userSettings->subdomain)) {
        $homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');

    }else{
        $homeLink = route('mystore.published',[$userSettings->public_link]);
    }
    }else{
        $homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
    }

    $sLink = $userSettings->public_link;
    $loginLink = $homeLink.'/customer/login';
    $registerLink = $homeLink.'/customer/register';
    
    if($userSettings->is_start == 1) {
            $shareableLink = $userSettings->public_link;
        }else{
            $shareableLink = $userSettings->preview_link;
        }

@endphp
<div class="row my-5 text-center">
    {{--@if(!empty($meta_data['storefront']))
            @if(!empty($userSettings->getFirstMediaUrl('storelogo')))
                <a class="nav-link" href="{{$homeLink}}"><i class="ficon"></i><img class=" lazyload" loading="lazy" data-src="{{ $userSettings->getFirstMediaUrl('storelogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                    onerror="this.onerror=null;this.src='{{asset('XR/app-assets/images/logo/logo.png')}}';" alt="Logo" style="height: 50px;"></a>
            @else
                <a class="nav-link" href="{{$homeLink}}"><i class="ficon"></i><h5 style="color: {{ $meta_data['splash']['brand_color'] ?? '' }}"><b>{{$meta_data['storefront']['storename']}}</b></h5></a>
            @endif
    @endif --}}
</div>
<div class="d-flex justify-content-center">
<div class="row bg-white pad_mob_15" style="width:500px;">
        <form method="POST" action="javascript:void(0)" id="registerform" enctype="multipart/form-data">
                    <p class="fw-bolder text-brown w-100 h3" id="loginModalLabel" style="font-weight: 800;">Sign Up</p>
                    
                <div id="register-flash-message"></div>

                    @csrf
                    <div class="form-group">
                        <label class="form-label" for="register-username">Your name</label>
                        <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" required autofocus placeholder="{{ trans('global.user_name') }}" value="{{ old('name', null) }}">
                        <div class="invalid-feedback">
                            Please enter your name
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-6">
                            <label for="country_code">Country code</label>
                            <select id="country_code" class="form-control {{ $errors->has('country_code') ? ' is-invalid' : '' }}" name="country_code" required>
                                @foreach(App\Customer::MOBILE_COUNTRY_CODE as $key=>$value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Please enter your mobile number
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label " for="register-email">Mobile</label>
                            <input class="form-control {{ $errors->has('mobile') ? ' is-invalid' : '' }}" type="tel" name="mobile" placeholder="8888888888" pattern="[0-9]{10}" maxlength="10" required value="{{ old('mobile', null) }}" />
                            <div class="invalid-feedback">
                                Please enter your mobile number
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="register-email">Email</label>
                        <input type="email" name="email" id="remail" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required placeholder="{{ trans('global.login_email') }}" value="{{ old('email', null) }}">
                        <div class="invalid-feedback">
                            Please enter your email id
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="register-password">Password</label>
                        <div class="input-group input-group-merge form-password-toggle">
                            <input type="password" name="password" minlength="6" id="rpassword" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required placeholder="{{ trans('global.login_password') }}" value="{{ old('password', null) }}"  autocomplete="off">
                            <div class="input-group-append">
                                <span class="input-group-text cursor-pointer">
                                    <i class="fa fa-eye togglePassword" data-id="rpassword" aria-hidden="true"></i></span>
                            </div>
                            <div class="invalid-feedback">
                                Please enter a password
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="register-password">Confirm Password</label>
                        <div class="input-group input-group-merge form-password-toggle">
                            <input type="password" name="cpassword" minlength="6" id="cpassword" class="form-control{{ $errors->has('cpassword') ? ' is-invalid' : '' }}" required placeholder="{{ trans('global.login_password') }}" value="{{ old('cpassword', null) }}" autocomplete="off">
                            <div class="input-group-append">
                                <span class="input-group-text cursor-pointer">
                                    <i class="fa fa-eye togglePassword" data-id="cpassword" aria-hidden="true"></i></span>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="form-group">
                                <input  id="privacy-policy" type="checkbox" required/>
                                <label class="form-label" for="privacy-policy">
                                    I have read and agree the <a href="{{route('store.storePrivacyPolicy', $sLink)}}" target="_blank">&nbsp;privacy policy</a> and <a href="{{route('store.storeTermsAndConditions', $sLink)}}" target="_blank">terms & conditions</a>
                                </label>
                        </div> --}}
                    
                    <div style="margin-top: -20px;font-size:10px;" id="CheckPasswordMatch"></div>
                    <div style="margin-top: -20px;font-size:10px;" id="CheckPolicy"></div>
                    <input type="hidden" name="slink" value="{{$sLink ?? ''}}">


                <button type="submit" id="signup"  onclick="signUpBtn()" class="btn bg-maroon my-3 w-100" tabindex="5">Sign Up</button>
                <p class="text-center mt-2"><span>Already have an account?</span>
                    <a href="{{ route('customer.storelogin' , [$shareableLink]) }}" class="text-brown p-1" onclick="logInBtn()" id='loginLink' style="padding-inline: 0px;padding-block:6px;"><span>&nbsp; <b><u>LOGIN</u></b></span></a>
                </p>

        </form>
</div>
</div>

@endsection
