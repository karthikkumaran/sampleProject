@extends('themes.'.$meta_data['themes']['name'].'.layouts.customer')
@section('content')
@php
if($userSettings->is_start == 1) {
    if(!empty($userSettings->customdomain)){
        $homeLink = '//'.$userSettings->customdomain;

    }else if(!empty($userSettings->subdomain)) {
        $homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');

    }else{
        $homeLink = route('mystore.published',[$userSettings->public_link]);
    }
    }else{
        $homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
    }

    $sLink = $userSettings->public_link;
    $loginLink = $homeLink.'/customer/login';
    $registerLink = $homeLink.'/customer/register';

        if($userSettings->is_start == 1) {
            $shareableLink = $userSettings->public_link;
        }else{
            $shareableLink = $userSettings->preview_link;
        }
    

@endphp
<div class="row my-5 text-center">
    {{--@if(!empty($meta_data['storefront']))
            @if(!empty($userSettings->getFirstMediaUrl('storelogo')))
                <a class="nav-link" href="{{$homeLink}}"><i class="ficon"></i><img class=" lazyload" loading="lazy" data-src="{{ $userSettings->getFirstMediaUrl('storelogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                    onerror="this.onerror=null;this.src='{{asset('XR/app-assets/images/logo/logo.png')}}';" alt="Logo" style="height: 50px;"></a>
            @else
                <a class="nav-link" href="{{$homeLink}}"><i class="ficon"></i><h5 style="color: {{ $meta_data['splash']['brand_color'] ?? '' }}"><b>{{$meta_data['storefront']['storename']}}</b></h5></a>
            @endif
    @endif--}} 
</div>
<div class="d-flex justify-content-center m-5">
<div class="row bg-white " style="width:500px;">
        <form method="POST" id="forgotPasswordform" action="javascript:void(0);" enctype="multipart/form-data">
            @csrf
            
                    <p class="modal-title fw-bolder text-dark w-100 h3" id="loginModalLabel" style="font-weight: 800;">{{ trans('global.reset_password') }}</p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                <div id="forgotPassword-flash-message"></div>



                    <p class="">Please enter your email address and we'll send you instructions on how to reset your password.</p>

                    <input type="hidden" name="store_id" value="{{isset($campaign)? $campaign->team_id : $userSettings->user->team_id}}">
                    <div class="form-label-group">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" required autocomplete="email" autofocus placeholder="{{ trans('global.login_email') }}">

                        <label for="inputEmail">Email</label>
                    </div>

                    <a href="{{ route('customer.storelogin' , [$shareableLink]) }}" class="btn btn-link btn-sm p-1 btn-outline-primary float-left" id="forgotPwModal_loginLink">Back to Login</a>

                    <button type="submit" id="resetPassword" class="btn btn-primary btn-sm p-1 float-right">Recover Password</button>
                
        </form>                                        

</div>
</div>
@endsection