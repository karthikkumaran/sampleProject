{{-- @if(count($userSettings->bannerimg) > 0)
    <div class="col-md-12 col-sm-12 p-0">
        <div class="card mb-0">
            <div class="card-content">
                <div class="card-body" style="padding:0px;">
                    <div id="banner" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                        @foreach($userSettings->bannerimg as $bannerimg)
                            @if($loop->index <= (count($userSettings->bannerimg) - 1))
                            <li data-target="#banner" data-slide-to="{{$loop->index}}" class="{{$loop->index == 0?'active':''}}"></li>
                            @endif
                        @endforeach
                        </ol>

                        <div class="carousel-inner" role="listbox">
                       
                        @foreach($userSettings->bannerimg as $bannerimg)
                            @if($loop->index <= (count($userSettings->bannerimg) - 1))
                            <div class="carousel-item {{$loop->index == 0 ? 'active':''}}">
                                <img class="d-block w-100 ban" style="width:100%;height:100%;max-height:280px;border-radius:5px;" src="{{$bannerimg->getUrl()}}">
                                
                            </div>
                            @endif
                        @endforeach
                        </div>
                        
                        <a class="carousel-control-prev" href="#banner" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#banner" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif --}}

{{--@if(count($userSettings->bannerimg) > 0)--}}
<div id="colordiv" class="container-fluid lg_hide">

    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ul class="carousel-indicators  ">
            {{--@foreach($userSettings->bannerimg as $bannerimg)
                @if($loop->index <= (count($userSettings->bannerimg) - 1))
                <li id="colorbutton" data-target="#carouselExampleIndicators" data-slide-to="{{$loop->index}}" class="{{$loop->index == 0?'active':''}}"><button class="buttonradius" class="btn border-none"></button></li>
                @endif
            @endforeach--}}
          <li id="colorbutton" data-target="#carouselExampleIndicators" data-slide-to="0" class="active"><button class="buttonradius" class="btn border-none"></button></li>
          <li id="colorbutton" data-target="#carouselExampleIndicators" data-slide-to="1"><button class="buttonradius"class="btn border-none"></button></li>
          <li id="colorbutton" data-target="#carouselExampleIndicators" data-slide-to="2"><button class="buttonradius" class="btn border-none"></button> </li>
        </ul>

                <div class="container-fluid">
                     <div class="row ">
             
                        
                        
                <div class="col-md-6 position-relative ">
                            <div>
                                <h1 id="banner-text" class="position-absolute mx-2 h1 top-50 start-0 translate-middle-y">
                                Grown naturally with love,
                                to take you back to your roots.
                                </h1>                        
                            </div>
                </div>

                
            
            
                <div class="col-md-6 h-100">  
                 <div class="carousel-inner">
                        
                    {{--@foreach($userSettings->bannerimg as $bannerimg)
                        @if($loop->index <= (count($userSettings->bannerimg) - 1))
                        <div class="carousel-item active">
                            <img id="banimg" class="img-responsive" src="{{$bannerimg->getUrl()}}">
                        </div>
                        @endif
                    @endforeach--}}
                    
                    <div class="carousel-item active">
                        <img id="banimg" class="img-responsive float-right tabl_hide desk_show" src="{{asset('themes/rustic-theme/images/png/bannercropped.png')}}" alt="">
                        <img id="banimg" class="img-responsive float-right tabl_show desk_hide" src="{{asset('themes/rustic-theme/images/png/home_ban_ipad_01.png')}}" alt="">
                    </div> 
                    
                    <div class="carousel-item">
                        <img id="banimg" class="img-responsive float-right tabl_hide desk_show" src="{{asset('themes/rustic-theme/images/png/Banner_02.png')}}" alt="">
                        <img id="banimg" class="img-responsive float-right tabl_show desk_hide" src="{{asset('themes/rustic-theme/images/png/home_ban_ipad_02.png')}}" alt="">
                    </div>
                      
                    <div class="carousel-item">
                        <img id="banimg" class="img-responsive float-right tabl_hide desk_show" src="{{asset('themes/rustic-theme/images/png/Banner_03.png')}}" alt="">
                        <img id="banimg" class="img-responsive float-right tabl_show desk_hide" src="{{asset('themes/rustic-theme/images/png/home_ban_ipad_03.png')}}" alt="">
                     </div>
                     
                    
                   
                    </div>
                    
                  </div>
               
               
               
                
            
            
            
            
            </div>
        </div>
    </div>
</div>
{{-- @endif --}}
<div id="colordiv" class="container-fluid sm_hide ">

    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ul class="carousel-indicators  ">
            {{--@foreach($userSettings->bannerimg as $bannerimg)
                @if($loop->index <= (count($userSettings->bannerimg) - 1))
                <li id="colorbutton" data-target="#carouselExampleIndicators" data-slide-to="{{$loop->index}}" class="{{$loop->index == 0?'active':''}}"><button class="buttonradius" class="btn border-none"></button></li>
                @endif
            @endforeach--}}
          <li id="colorbutton" data-target="#carouselExampleIndicators" data-slide-to="0" class="active"><button class="buttonradius" class="btn border-none"></button></li>
          <li id="colorbutton" data-target="#carouselExampleIndicators" data-slide-to="1"><button class="buttonradius"class="btn border-none"></button></li>
          <li id="colorbutton" data-target="#carouselExampleIndicators" data-slide-to="2"><button class="buttonradius" class="btn border-none"></button> </li>
        </ul>

                <div class="container">
                     <div class="row ">
             
                        
                        
                <div class="col-md-12 position-relative mt-5 z_high">
                            <div>
                                <h1 id="banner-text" class="position-absolute mx-2 h1 top-50 start-0 translate-middle-y">
                            Bring the organic food from the origin to your home.</h1>                        
                            </div>
                </div>

                
            
            
                <div class="col-md-12 h-100">  
                 <div class="carousel-inner">
                        
                    {{--@foreach($userSettings->bannerimg as $bannerimg)
                        @if($loop->index <= (count($userSettings->bannerimg) - 1))
                        <div class="carousel-item active">
                            <img id="banimg" class="img-responsive" src="{{$bannerimg->getUrl()}}">
                        </div>
                        @endif
                    @endforeach--}}
                    
                    <div class="carousel-item active">
                        <img id="banimg" class="img-responsive" src="{{asset('themes/rustic-theme/images/png/home_ban_mob1.png')}}" alt="">
                    </div> 
                    
                    <div class="carousel-item">
                        <img id="banimg" class="img-responsive" src="{{asset('themes/rustic-theme/images/png/home_ban_mob2.png')}}" alt="">
                    </div>
                      
                    <div class="carousel-item">
                        <img id="banimg" class="img-responsive" src="{{asset('themes/rustic-theme/images/png/home_ban_mob3.png')}}" alt="">
                     </div>
                     
                    
                   
                    </div>
                    
                  </div>
               
               
               
                
            
            
            
            
            </div>
        </div>
    </div>
</div>