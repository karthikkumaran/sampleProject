@extends('themes.'.$meta_data['themes']['name'].'.layouts.policies')
@section('content')
@php
$agent=new \Jenssegers\Agent\Agent();
if($userSettings->is_start == 1) {
if(!empty($userSettings->customdomain)){
$homeLink = '//'.$userSettings->customdomain;
}else if(!empty($userSettings->subdomain)) {
$homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
}else{
$homeLink = route('mystore.published',[$userSettings->public_link]);
}
}else{
$homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
}
@endphp
  <div class="content-header row">
    <div class="content-header-left col-12 mb-2">
      <div class="row breadcrumbs-top">
        <div class="col-12">
          <div class="breadcrumb-wrapper">
            <ol class="breadcrumb">
              <li class="breadcrumb-item active"><a href="{{$homeLink}}">Home</a></li>
              <li class="breadcrumb-item active">{!! trans('about_us.title') !!}</li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="content-body">
    <div class="card">
      <div class="card-body">
        <p class="card-text">
          @if(isset($meta_data['content_settings']['about_us']))
            @if(strip_tags($meta_data['content_settings']['about_us']) == "")
              {!! trans('about_us.content') !!}
            @else
              {!! $meta_data['content_settings']['about_us'] !!}
            @endif
          @else
            {!! trans('about_us.content') !!}
          @endif
        </p>
      </div>
    </div>
  </div>
@endsection