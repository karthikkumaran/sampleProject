@extends('themes.'.$meta_data['themes']['name'].'.layouts.public')
@section('content')
    @livewire('storefront.themes.'.$meta_data['themes']['name'].'.checkout', ['campaign' => $campaign, 'userSettings' => $userSettings, 'meta_data' => $meta_data, 'customer' => $customer])
@endsection