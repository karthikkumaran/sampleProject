@extends($layout)
@section('content')
@php
$cmeta_data = json_decode($customer->meta_data, true);
$transaction =$customer->order->transactions;
@endphp

<div class="content-header row mx-1">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <a href="{{ url()->previous() }}">
                    <h4 class="text-primary"><i class="feather icon-arrow-left"></i> Back to Orders</h4>
                </a>
            </div>
        </div>
    </div>
</div>


<div class="row mx-1">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-4 text-left ">
                        <h5>Order Details</h5>
                        <h5><b>Order #{{$customer->order->order_ref_id}}</b></h5>
                        <h5>No. Products : <b>{{ count($customer->orderProducts) }}</b></h5>
                        @if(isset($customer->order->transactions->amount))
                        <h5>Total Price : <b>{!! $cmeta_data['currency'] !!}{{ $customer->order->transactions->amount }}</b></h5>
                        @endif
                        <!-- <h5>Total Price : <b>{!! $cmeta_data['currency'] !!}{{ $cmeta_data['product_total_price'] }}</b></h5> -->
                    </div>
                    <div class="col-md-4 text-md-center p-1 ">
                        @if(isset($meta_data['settings']['whatsapp']))
                        <a href="https://wa.me/{{isset($meta_data['settings']['whatsapp_country_code']) ? $meta_data['settings']['whatsapp_country_code'].$meta_data['settings']['whatsapp'] : '91'.$meta_data['settings']['whatsapp']}}" class="mr-1 mb-1">
                            <i style="color:#25D366" class="fa fa-whatsapp fa-2x"></i></a>
                        @endif
                            <a href="{{ route('customer_export_order_pdf', $customer->order_id) }}" class="mr-1 mb-1">
                            <i style="color:red" class="fa fa-file-pdf-o fa-2x"></i></a>
                        @if($customer->order->is_generate_invoice == true)
                            <a href="{{ route('admin.export_invoice_pdf', $customer->order_id) }}" class="mr-1 mb-1">
                            <i style="color:green" class="fa fa-file-pdf-o fa-2x"></i><span class="text-info">invoicepdf</span></a>
                        @endif

                    </div>
                    <div class="col-md-4 text-md-right">
                        @php
                        if(App\orders::STATUS[$customer->order->status] == 'Order Recieved')
                            $class="badge-primary";
                            elseif (App\orders::STATUS[$customer->order->status] == 'Pending')
                            $class="badge-warning";
                            elseif (App\orders::STATUS[$customer->order->status] == 'Accepted')
                            $class="badge-success";
                            elseif (App\orders::STATUS[$customer->order->status] == 'Shipped')
                            $class="badge-success";
                            elseif (App\orders::STATUS[$customer->order->status] == 'Delivered')
                            $class="badge-success";
                            elseif (App\orders::STATUS[$customer->order->status] == 'Rejected')
                            $class="badge-danger";
                            elseif (App\orders::STATUS[$customer->order->status] == 'Cancelled')
                            $class="badge-danger";
                            elseif (App\orders::STATUS[$customer->order->status] == 'Failed')
                            $class="badge-danger";
                        @endphp

                        <h5 class="mb-md-1">Order Status: <span class=' badge {{$class}}'>{{App\orders::STATUS[$customer->order->status]}}</span></h5>
                        @if($customer->order->order_mode)
                        <h6>Order mode: <b>{{$customer->order->order_mode=="Online-Storefront" ? "Online" : ''}}</b></h6>
                        @endif
                        <span style="font-size:1.5rem;">Order date: </span><b>{{date('d-m-Y h:i:s A', strtotime($customer->created_at))}}</b>

                        @if($customer->order->notes)
                        <h4>Notes : </h4>
                        <h4>{!! $customer->order->notes !!}</h4>
                        @endif

                    </div>
                </div>
            </div>
        </div>


    <div class="card ">
        <div class="card-body">
                <div class="row mt-2 mb-2">
                    <div class="col-md-6">
                        <!-- <div class="bg-white p-2 border rounded px-3"> -->
                            <div class="d-flex flex-row justify-content-between align-items-center order">
                                <div class="d-flex flex-column order-details"><h3>Order Status</h3></div>
                            </div>
                        </div>
                    <div class="col-md-6 pr-2 text-right">

                    @if($customer->order->status < 4)
                    <button class="btn btn-danger ordercancel" >Cancel</button>
                    @endif
                        </div>
                    </div>
                </div>

                <div class="row mt-1 mb-1 pl-2 pr-2">
                    <div class="col-md-12">
                            <!-- <hr class="divider mb-4"> -->
                            <div class="d-flex flex-row justify-content-between align-items-center">

                                @foreach($orderstatus as $key => $value)
                                    <div class="d-flex flex-column align-items-start">{{App\orders::STATUS[$value->status]}}</div>
                                @endforeach

                                @if($customer->order->status == 5 && empty(App\orders::STATUS[$value->status]))
                                <div class="d-flex flex-column align-items-start">{{App\orders::STATUS[5]}}</div>
                                @elseif($customer->order->status == 6 && empty(App\orders::STATUS[$value->status]))
                                <div class="d-flex flex-column align-items-start">{{App\orders::STATUS[6]}}</div>
                                @elseif($customer->order->status == 7 && empty(App\orders::STATUS[$value->status]))
                                <div class="d-flex flex-column align-items-start">{{App\orders::STATUS[7]}}</div>
                                @else
                                    @foreach(App\orders::STATUS as $key => $label)
                                        @if($key > $customer->order->status && $key <= 4)
                                        <div class="d-flex flex-column align-items-start">{{ $label }}</div>
                                        @endif
                                    @endforeach
                                @endif
                                <!-- <div class="d-flex flex-column justify-content-center"><span>Pending</span></div>
                                <div class="d-flex flex-column justify-content-center align-items-center"><span>Accepted</span></div>
                                <div class="d-flex flex-column align-items-center"><span>Shipping</span></div>
                                <div class="d-flex flex-column align-items-end"><span>Delivered</span></div> -->
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center">

                                @foreach($orderstatus as $key => $value)
                                    @if($key == 0)
                                        @if($key != count($orderstatus)-1)
                                            <span class="dot cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')"></span>
                                        @else
                                            <span class="d-flex justify-content-center align-items-center big-dot dot cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')"><i class="fa fa-check text-white"></i></span>
                                        @endif
                                    @elseif($key != count($orderstatus)-1)
                                        @if($value->status < 5)
                                        <hr class="flex-fill track-line align-items-start"><span class="dot cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')"></span>
                                        @else
                                        <hr class="flex-fill failtrack-line align-items-start"><span class="d-flex justify-content-center align-items-center red-dot cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')"><i class="fa fa-times text-white"></i></span>
                                        @endif

                                    @else
                                        @if($customer->order->status < 5)
                                            <hr class="flex-fill track-line"><span class="d-flex justify-content-center align-items-center big-dot dot cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')"><i class="fa fa-check text-white"></i></span>
                                        @else
                                            <hr class="flex-fill failtrack-line align-items-start"><span class="d-flex justify-content-center align-items-center red-dot cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')"><i class="fa fa-times text-white"></i></span>
                                        @endif
                                    @endif
                                @endforeach
                                @foreach(App\orders::STATUS as $key => $label)
                                    @if($key > $customer->order->status && $key <= 4)
                                    <hr class="flex-fill untrack-line align-items-start"><span class="white-dot"></span></span>
                                    @endif
                                @endforeach

                                @php
                                     $statusvalue =  App\orders::STATUS[$customer->order->status] ;
                                 @endphp

                                <!-- <hr class="flex-fill track-line"><span class="dot"></span>
                                <hr class="flex-fill track-line"><span class="dot"></span>
                                <hr class="flex-fill track-line"><span class="d-flex justify-content-center align-items-center big-dot dot"><i class="fa fa-check text-white"></i></span> -->
                                </div>
                            <div class="d-flex flex-row justify-content-between align-items-center">
                                @foreach($orderstatus as $key => $value)
                                <div class="d-flex flex-column align-items-start">
                                    @php
                                        $s= $value->created_at ;
                                        $date = $s->format('m/d/Y');
                                        $time = date("h:i:s A",strtotime($s));
                                    @endphp
                                    {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                    <br>
                                    {{ $time }}
                                </div>
                                @endforeach
                                @if($customer->order->status == 5 && empty(App\orders::STATUS[$value->status]))
                                        @php
                                             $s= $value->created_at ;
                                             $date = $s->format('m/d/Y');
                                             $time = date("h:i:s A",strtotime($s));
                                         @endphp
                                    {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                    <br>
                                    {{ $time }}
                                @elseif($customer->order->status == 6 && empty(App\orders::STATUS[$value->status]))
                                        @php
                                             $s= $value->created_at ;
                                             $date = $s->format('m/d/Y');
                                             $time = date("h:i:s A",strtotime($s));
                                         @endphp
                                    {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                    <br>
                                    {{ $time }}
                                    @elseif($customer->order->status == 7 && empty(App\orders::STATUS[$value->status]))
                                        @php
                                             $s= $value->created_at ;
                                             $date = $s->format('m/d/Y');
                                             $time = date("h:i:s A",strtotime($s));
                                         @endphp
                                    {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                    <br>
                                    {{ $time }}
                                    @else
                                        @foreach(App\orders::STATUS as $key => $label)
                                        @if($key > $customer->order->status && $key <= 4)
                                        <span>-</span>
                                        @endif
                                        @endforeach
                                @endif
                                </div>

                                <!-- <div class="d-flex flex-column justify-content-center"><span>15 Mar</span><span>Order placed</span></div>
                                <div class="d-flex flex-column justify-content-center align-items-center"><span>15 Mar</span><span>Order Dispatched</span></div>
                                <div class="d-flex flex-column align-items-center"><span>15 Mar</span><span>Out for delivery</span></div>
                                <div class="d-flex flex-column align-items-end"><span>15 Mar</span><span>Delivered</span></div> -->
                            </div>
                        <!-- </div> -->
                    </div>
                    <div class="row mt-1 mb-2 pl-2 pr-2">
                        <div class="col-md-12">
                            <hr class="divider mb-2" style="color:black;">

                            <div class="text-left">

                            @foreach($orderstatus as $key => $value)
                                <div>
                                @if($value->status == $customer->order->status)
                                        <div id='showremarks'><b>{{ App\orders::STATUS[$value->status] }}: </b>
                                        </div>
                                @endif
                                </div>
                            @endforeach
                             <div>
                             @foreach($orderstatusremark as $key => $value)
                                @if($value->status == $customer->order->status)
                                    @if($value->showremarks == 1)
                                    <div class="remark remarks-{{$value->status}}">
                                        <p> {{$value->remarks}} on {{$value->created_at}}</p>
                                    </div>
                                    @endif
                                @else
                                     @if($value->showremarks == 1)
                                    <div class="remark remarks-{{$value->status}}" style="display: none">
                                        <p> {{$value->remarks}} on {{$value->created_at}}</p>
                                    </div>
                                    @endif
                                @endif
                             @endforeach
                            </div>
                            </div>
                       </div>
                    </div>
        </div>
    </div>



    @if($transaction!=null)
    <div class="col-md-12">
        <section id="collapsible">
            <div class="collapse-default collapse-icon">
                <div class="card">
                    <div id="headingCollapse4" class="card-header pb-1 cursor-pointer" data-toggle="collapse" role="button" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">

                        <span class="lead collapse-title text-dark h3">Transaction Details - #{{$transaction->transaction_id}}</span>
                    </div>
                    <div id="collapse4" role="tabpanel" aria-labelledby="headingCollapse4" class="collapse" aria-expanded="false">
                        <div class="card-body">
                            <div class="row">
                                <div class="col col-md-4 order-1 cursor-pointer">
                                    <h5>Mode: <b>{{$transaction->payment_mode}}</b></h5>
                                    <h5>{{$transaction->payment_method}}</h5>

                                    @if($transaction->payment_mode == "UPI" && !empty($transaction->photo->getUrl('thumb')))
                                    <img id="upi-ss" class="img-fluid p-1" style="max-width: 150px; max-height:150px;" src="{{$transaction->photo->getUrl('thumb')}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                    @endif
                                </div>
                                <div class="col col-md-4 order-2 d-flex flex-md-row flex-column justify-content-md-center">

                                    <a href="#" class="mr-1 mb-1">Order #{{$transaction->order_id}}</a>


                                </div>
                                <div class="col col-md-4 order-3 text-md-right cursor-pointer">

                                    <h5>{{ date('d-m-Y', strtotime($transaction->created_at)) }}</h5>
                                    <h5>{{ date('h:i:s A', strtotime($transaction->created_at)) }}</h5>

                                    <h5><b>{!! $transaction['amount']==0?" ":$cmeta_data['currency'].''.$transaction['amount'] !!}</b></h5>

                                    <h4>Status: <b>{{$transaction->status}}</b></h4>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @endif
    <!-- && count($trackingdata) > 0 -->

    @if(!empty($cmeta_data['Shyplite_price']) && count($trackingdata) > 0)
    <div class="col-md-12 p-1">
        <section id="collapsible">
            <div class="collapse-default collapse-icon">
                <div class="card ">
                    <div id="headingCollapse5" class="card-header pb-1 cursor-pointer" data-toggle="collapse" role="button" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">

                        <label class="h3" >Shipping Details<span class="lead collapse-title text-dark h3"> - #</span></label>
                    </div>
                    <div id="collapse5" role="tabpanel" aria-labelledby="headingCollapse5" class="collapse" aria-expanded="false">
                         <div class="card-body">
                            <!--<form method="POST" id="formsplashscreen" action="" enctype="multipart/form-data">
                                <h5> Seller address information :</h5>
                                <div class="form-group">
                                    <center>
                                        <div class="row">
                                            <div class="col-4 align-center text-right">
                                                <label for="seller_address">Address name:</label>
                                            </div>
                                            <div class="col-8 text-left">
                                                <select id="seller_address"><option>AS01</option></select><br>
                                            </div>
                                        </div>
                                    </center>
                                </div>
                                <h5> Order information :</h5>
                                <div class="form-group">
                                    <div class="row">
                                            <div class="col-4 align-center text-right">
                                                <label for="order_id">Order ID:</label>
                                            </div>
                                            <div class="col-6 text-left">
                                                <input type="text" class="form-control" id="order_id"  value="{{$customer->order->id}}"/><br>
                                            </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-4 align-center text-right">
                                                <label for="order_date">Order Date:</label>
                                            </div>
                                            <div class="col-6 text-left">
                                                <input type="text" class="form-control" id="order_date"  value="{{date('d-m-Y', strtotime($customer->created_at))}}"><br>
                                            </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-4 align-center text-right">
                                                <label for="order_type">Type:</label>
                                            </div>
                                            <div class="col-6 text-left">
                                                <input type="text" class="form-control" id="order_type"  value=""><br>
                                            </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-4 align-center text-right">
                                                <label for="mode_type">Mode Type:</label>
                                            </div>
                                            <div class="col-6 text-left">
                                                <select><option>Air</option></select><br><br>
                                            </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-4 align-center text-right">
                                                <label for="total_value">Total value</label>
                                            </div>
                                            <div class="col-6 text-left">
                                                <input type="text" class="form-control" id="total_value"  value=""><br>
                                            </div>
                                    </div>

                                </div>
                                <h5>Customer information :</h5>
                                <div class="form-group">
                                    <div class="row">
                                            <div class="col-4 align-center text-right">
                                                <label for="customer_name">Name</label>
                                            </div>
                                            <div class="col-6 text-left">
                                                <input type="text" class="form-control" id="customer_name"  value="{{$customer->fname ?? '' }}"><br>
                                            </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-4 align-center text-right">
                                                <label for="customer_address">Full address</label>
                                            </div>
                                            <div class="col-6 text-left">
                                                <textarea id="customer_address" rows="8" cols="30" >
                                                    {{$customer->address1 ?? "" }}
                                                    {{$customer->address2 ?? "" }}
                                                    {{$customer->city ?? "" }}
                                                    {{$customer->pincode ?? "" }}
                                                    {{$customer->state ?? "" }}
                                                    {{$customer->country ?? "" }}
                                                </textarea><br>
                                            </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-4 align-center text-right">
                                                <label for="contact_number">Contact NO:</label>
                                            </div>
                                            <div class="col-6 text-left">
                                                <input type="number" class="form-control" id="contact_number"  value="{{ $customer->mobileno ?? '' }}"><br>
                                            </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-4 align-center text-right">
                                                <label for="city">City</label>
                                            </div>
                                            <div class="col-6 text-left">
                                                <input type="text" class="form-control" id="city"  value="{{$customer->city ?? '' }}"><br>
                                            </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-4 align-center text-right">
                                                <label for="pin">pincode</label>
                                            </div>
                                            <div class="col-6 text-left">
                                                <input type="text" class="form-control" id="pin"  value="{{$customer->pincode ?? '' }}"><br>
                                            </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                                <hr> -->
                                <h4>Shipment Tracking :</h4>
                                <div class="d-flex flex-row justify-content-between align-items-center" >
                                @foreach($trackingdata as $key => $value)
                                    <div class="d-flex flex-column align-items-start" >
                                        @if($value->eventstatus == 'SB')
                                        <b>Shipment Booked</b><br>
                                        @elseif($value->eventstatus == 'PU')
                                        <b>Picked Up</b><br>
                                        @elseif($value->eventstatus == 'IT')
                                        <b>In Transit</b><br>
                                        @elseif($value->eventstatus == 'DY')
                                        <b>Delay</b><br>
                                        @elseif($value->eventstatus == 'EX')
                                        <b>Exception</b><br>
                                        @elseif($value->eventstatus == 'OD')
                                        <b>Out for Delivery</b><br>
                                        @elseif($value->eventstatus == 'RT')
                                        <b>Return</b><br>
                                        @elseif($value->eventstatus == 'DL')
                                        <b>Delivered</b><br>
                                        @endif</div>
                                @endforeach
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center">

                                @foreach($trackingdata as $key => $value)
                                    @if($key == 0)
                                        @if($key != count($trackingdata)-1)
                                            <span class="dot cursor-pointer" onmouseover="showtrack('{{ $key }}')"></span>
                                        @else
                                            <span class="d-flex justify-content-center align-items-center big-dot dot cursor-pointer" onmouseover="showtrack('{{ $key }}')"><i class="fa fa-check text-white"></i></span>
                                        @endif
                                    @elseif($key != count($trackingdata)-1)
                                        <hr class="flex-fill track-line align-items-start"><span class="dot cursor-pointer" onmouseover="showtrack('{{ $key }}')"></span>
                                    @elseif($key == count($trackingdata)-1)
                                    <hr class="flex-fill track-line align-items-start"><span class="d-flex justify-content-center align-items-center big-dot dot cursor-pointer" onmouseover="showtrack('{{ $key }}')"><i class="fa fa-check text-white"></i></span>
                                    @endif
                                @endforeach
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center">
                                @foreach($trackingdata as $key => $value)
                                <b> {{ \Carbon\Carbon::parse($value->eventtime)}}</b>
                                @endforeach
                                </div>
                                <hr class="divider mb-2" style="color:black;">

                            <div class="text-left">
                            @foreach($trackingdata as $key => $value)
                                    @if($key == count($trackingdata)-1)
                                        <div class='lastkey'><h4>{{$value->eventstatus}}: </h4>
                                        @if($value->eventstatus == 'SB')
                                        <b>Status:</b>Shipment Booked<br>
                                        @elseif($value->eventstatus == 'PU')
                                        <b>Status:</b>Picked Up<br>
                                        @elseif($value->eventstatus == 'IT')
                                        <b>Status:</b>In Transit<br>
                                        @elseif($value->eventstatus == 'DY')
                                        <b>Status:</b>Delay<br>
                                        @elseif($value->eventstatus == 'EX')
                                        <b>Status:</b>Exception<br>
                                        @elseif($value->eventstatus == 'OD')
                                        <b>Status:</b>Out for Delivery<br>
                                        @elseif($value->eventstatus == 'RT')
                                        <b>Status:</b>Return<br>
                                        @elseif($value->eventstatus == 'DL')
                                        <b>Status:</b>Delivered<br>
                                        @endif
                                        <b>AWB ID:</b>{{$value->awb_id}}<br>
                                        <b>AWB NO:</b>{{$value->awb_no}}<br>
                                        <b>Carrier Name:</b>{{$value->carrier_name}}<br>
                                        <b>Remarks:</b>{{$value->remark}}<br>
                                        <b>Location:</b>{{$value->location}}<br>
                                        <b>Time:</b>{{ \Carbon\Carbon::parse($value->eventtime)}}
                                        </div>
                                    @endif
                            @endforeach

                            @foreach($trackingdata as $key => $value)
                                        <div class='trackvalue_{{$key}}' style="display:none"><h4>{{$value->eventstatus}}: </h4>
                                        @if($value->eventstatus == 'SB')
                                        <b>Status:</b>Shipment Booked<br>
                                        @elseif($value->eventstatus == 'PU')
                                        <b>Status:</b>Picked Up<br>
                                        @elseif($value->eventstatus == 'IT')
                                        <b>Status:</b>In Transit<br>
                                        @elseif($value->eventstatus == 'DY')
                                        <b>Status:</b>Delay<br>
                                        @elseif($value->eventstatus == 'EX')
                                        <b>Status:</b>Exception<br>
                                        @elseif($value->eventstatus == 'OD')
                                        <b>Status:</b>Out for Delivery<br>
                                        @elseif($value->eventstatus == 'RT')
                                        <b>Status:</b>Return<br>
                                        @elseif($value->eventstatus == 'DL')
                                        <b>Status:</b>Delivered<br>
                                        @endif
                                        <b>AWB ID:</b>{{$value->awb_id}}<br>
                                        <b>AWB NO:</b>{{$value->awb_no}}<br>
                                        <b>Carrier Name:</b>{{$value->carrier_name}}<br>
                                        <b>Remarks:</b>{{$value->remark}}<br>
                                        <b>Location:</b>{{$value->location}}<br>
                                        <b>Time:</b>{{ \Carbon\Carbon::parse($value->eventtime)}}
                                        </div>
                            @endforeach
                             <div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @endif

    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3>Customer Details</h3>
            </div>
            <div class="card-body">
                <div class="row no-gutters">
                    <div class="col-md-6  order-1 no-gutters">
                        <div class="row no-gutters">
                            <div class="col col-md-2 no-gutters">
                                Name
                            </div>
                            <div class="col col-md-5 no-gutters">
                                <b> {{$customer->fname ?? "" }}</b>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col col-md-2  no-gutters">
                                Address
                            </div>
                            <div class="col col-md-5 no-gutters">
                                <b>
                                    {{$customer->address1 ?? "" }}<br>
                                    {{$customer->address2 ?? "" }}<br>
                                    {{$customer->city?? "" }}<br>
                                    {{$customer->pincode?? "" }}<br>
                                    {{$customer->state ?? "" }}<br>
                                    {{$customer->country ?? "" }}<br>
                                </b>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 order-2 no-gutters">
                        <div class="row no-gutters">
                            <div class="col col-md-5 no-gutters">
                                Email
                            </div>
                            <div class="col-6 col-md-6 no-gutters">
                                <b>{{ $customer->email ?? "" }}</b>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col col-md-5 no-gutters">
                                Mobile No
                            </div>
                            <div class="col col-md-5 no-gutters">
                                <b>{{ $customer->mobileno ?? "" }}</b>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col col-md-5 no-gutters">
                                Alternative Mobile No
                            </div>
                            <div class="col-6 col-md-5 no-gutters">
                                <b>{{ $customer->amobileno ?? "" }}</b>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col col-md-5 no-gutters">
                                Address Type
                            </div>
                            <div class="col col-md-5 no-gutters">
                                <b>{{ $customer->addresstype?? "" }}</b>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>
</div>

<div class="ecommerce-application mr-3 ml-3">
    <h3>Ordered Product List</h3>

    <section class="list-view d-block mb-3">
        <div class="" id="product_list">

            @foreach ($customer->orderProducts as $oproduct)
            @php
            $pmeta_data = json_decode($oproduct['meta_data'], true);
            $pmeta_data['campaign_name'];
            // $product_categories = $pmeta_data['category'];
            // $product = $oproduct->product;
            if(isset($pmeta_data['category']))
            {
                $product_categories = $pmeta_data['category'];
            }
            else{
                $product_categories = [];
            }
            if(empty($oproduct->product_variant_id))
            {
                $product = $oproduct->product;
            }
            else {
                $product = $oproduct->productVariant;
            }
            if($oproduct['discount'] != null)
            {
            $discount_price = $oproduct['price'] * ($oproduct['discount']/100);
            $discounted_price = $oproduct['price'] - $discount_price;
            }
            $img = $objimg = asset('XR/assets/images/placeholder.png');
            $obj_id = "";
            if(count($product->photo) > 0) {
            $img = $product->photo[0]->getUrl('thumb');
            $objimg = asset('XR/assets/images/placeholder.png');
            $obj_id = "";
            } else if(count($product->arobject) > 0) {
            if(!empty($product->arobject[0]->object))
            if(!empty($product->arobject[0]->object->getUrl()))
            $img = $product->arobject[0]->object->getUrl();
            }
            if(count($product->arobject) > 0){
            if(!empty($product->arobject[0]->object))
            if(!empty($product->arobject[0]->object->getUrl())) {
            $objimg = $product->arobject[0]->object->getUrl();
            $obj_id = $product->arobject[0]->id;
            }
            }
            @endphp
            <div class="card ecommerce-card" id="product_list_{{$product->id}}">
                <div class="card p-3">
                    <div class="row mb-1 w-100 " style="min-height:13rem;">
                        <div class="col-lg-3 col-md-3 col-sm-12 d-flex justify-content-center p-1 rounded">
                        @if(count($product->photo) > 1)
                        <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails h-100 w-100" data-interval="false" data-ride="carousel">
                            <div class="carousel-inner h-100 w-100" role="listbox">
                                @foreach($product->photo as $photo)
                                <div class="carousel-item {{$loop->index == 0?'active':''}}">
                                    <img class="img-fluid mw-100 h-100 rounded"  src="{{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl('thumb'):$objimg ?? ''}}" style="min-height:13rem;" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                </div>
                                @endforeach
                            </div>
                            <div class="w-100 position-absolute d-flex niceScroll" style="width: 200px; overflow: hidden; display: inline-block; margin: 0px auto; padding: 3px; outline: none;" tabindex="0">
                                <ol class="carousel-indicators position-relative ">
                                    @foreach($product->photo as $photo)
                                    <li data-target="#carousel-thumb" data-slide-to="{{$loop->index}}" class=" {{$loop->index == 0?'active':''}}" style="width:50px; height:50px;">
                                        <img class="d-block img-fluid" style="max-width:50px;max-height: 50px;" src="{{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl('thumb'):$objimg ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                    </li>
                                    @endforeach
                                </ol>
                            </div>
                        </div>
                            @else
                            <img class="img-fluid mw-100 rounded h-100" src="{{$img ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="min-height:13rem;">
                            @endif
                            @if(!empty($obj_id))
                            <div class="position-absolute" style="top:0;left:0;">
                                <div class="badge badge-glow badge-info">
                                    <img class="img-fluid" src="{{asset('XR/assets/images/ar.png')}}" style="width: 32px; max-height:32px;">
                                    <h6 class="text-white m-0">Tryon</h6>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="col-lg-6 col-md-6 col-6 p-2">
                            <div class="item-name text-dark">
                                <input type="hidden" class="tryonobj" value="{{$objimg ?? ''}}" />
                                <input type="hidden" class="obj_id" value="{{$obj_id ?? ''}}" />
                                <h5><strong>{{$oproduct['name'] ?? 'Untitled'}}</strong></h5>
                                <p class="item-company">
                                    @if(count($product_categories) > 0)
                                <div class="badge badge-pill badge-glow badge-secondary text-dark mr-1">
                                    {{$product_categories[0]['name']}}
                                </div>
                                @endif
                                </p>
                            </div>
                            <div>
                                <span>{{$oproduct['sku'] ?? ''}}</span>
                            </div>

                            <div>
                                <h4 class="text-dark">{!! $pmeta_data['campaign_name'] !!}</h4>
                            </div>
                            @if($oproduct->notes)
                            <div>
                                <h4 class="text-dark">Note:{{" ".$oproduct->notes}}</h4>
                            </div>

                            @endif
                            <!-- <div>
                                    <p class="item-description update" data-pk="{{$product->id}}"
                                        data-url="{{route('admin.product_list.update',$product->id)}}" data-type="textarea"
                                        data-title="Enter description" data-name="description">
                                        {{$product->description ?? 'No Description'}}
                                    </p>
                                </div> -->
                            <div>
                                <span>{{$product->url ?? ''}}</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-6 p-2">
                            <div class="item-wrapper">
                                <div class="item-rating">
                                    @if(!empty($obj_id))
                                    <!-- <a href="#" data-id="{{$product->id}}"
                                            class="badge badge-pill badge-glow badge-success">Try On</a> -->
                                    @endif
                                    <!-- <a href="#" onclick="deleteProduct($(this))" data-id="{{$product->id}}"
                                            class="badge badge-pill badge-glow badge-danger"><i
                                                class="feather icon-trash-2 m-0"></i></a> -->
                                </div>
                                <!-- <div class="item-cost"> -->
                                <div class="item-cost text-right">
                                    <h5 class="item-price">
                                        {!! $cmeta_data['currency'] !!}
                                        @if($oproduct['discount'] != null)
                                        {{$discounted_price}}
                                        <span class="text-primary"><s>{{$oproduct['price']}}</s></span>
                                        <br><span class="text-warning" style="font-size: 80%;">{{$oproduct['discount'] ?? ''}}%</span>
                                        @else
                                        <span>{{$oproduct['price'] ?? '00.00'}}</span>
                                        @endif
                                    </h5>
                                </div>
                                <div class="item-cost text-right">
                                    <h5 class="item-price">
                                        Qty: <span>{{$oproduct['qty'] ?? ''}}</span>
                                    </h5>

                                </div>
                                <!-- </div> -->
                            </div>
                            <div class="wishlist bg-white">
                                <!-- <i class="fa fa-heart-o mr-25"></i> Wishlist -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </section>

</div>
@php
$sLink = $userSettings->public_link;
@endphp


@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.6.2/viewer.min.js" integrity="sha512-VzJLwaOOYyQemqxRypvwosaCDSQzOGqmBFRrKuoOv7rF2DZPlTaamK1zadh7i2FRmmpdUPAE/VBkCwq2HKPSEQ==" crossorigin="anonymous"></script>
@parent
<script>

    var url = "{{route('customer.order.cancel')}}"
    console.log(url)
    // console.log(url);
function show(x,statusContent){
    var order_data = JSON.parse(x)
    $('.remark').css("display", "none");
    $('.remarks-'+order_data.status).css("display", "block");
    var remarkhtml = "<b>"+statusContent+":</b>";
    $("#showremarks").html(remarkhtml)
 }

 var y='';

 function showtrack(x){
    $('.trackvalue_'+y).css("display", "none");
    y=x;
    $('.lastkey').css("display", "none");
    $('.trackvalue_'+x).css("display", "block");
 }

 $(document).on('click',".ordercancel",function(){

    swal.fire({
        icon:'warning',
        title:'Write the comments :',
        html:'<div>To cancel the order : </div>'+
      '<textarea row="5" cols="40" id="comment"></textarea></div> <br>',
        showCancelButton:true,
        cancelButtonText:"Cancel",
        confirmButtonText:"Ok",
        cancelButtonColor: "#DD6B55",
        preConfirm: function() {
            return new Promise((resolve, reject) => {
                resolve({
                    remarks: $('#comment').val(),
                });
            });
        }
    }).then(function(result){

        if(result.value)
        {
            console.log(result.value.remarks)
           if(result.value.remarks != '') {
               console.log(url)

            $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        order_id:{{$customer->order->id}},
                        sharablelink:"{{$sLink}}",
                        remarks: result.value.remarks,
                        status: 6,
                    },
                    success:function(data){
                        console.log(data);
                        Swal.fire({
                            type: "success",
                            title: 'order cancelled',
                            text: 'Your order is cancelled',
                            confirmButtonClass: 'btn btn-success',
                        }).then((result) => {
                            window.location.reload();
                    });
                    },
            });
           }
           else{
            Swal.fire({
                icon: 'error',
                title: 'write something in comment'
                });
           }
        }
    })
 });
</script>
@endsection
