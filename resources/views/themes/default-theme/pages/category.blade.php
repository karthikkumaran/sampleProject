@extends('themes.'.$meta_data['themes']['name'].'.layouts.public')
@section ('styles')
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
@endsection
@php
if($campaign->is_start == 1) {
    $catalogLink = route('campaigns.published',[$campaign->public_link]);
}else{
    $catalogLink = route('admin.campaigns.preview',[$campaign->preview_link]);
}
if($userSettings->is_start == 1) {
    if(!empty($userSettings->customdomain)){
        $homeLink = '//'.$userSettings->customdomain;
    }else if(!empty($userSettings->subdomain)) {
        $homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
    }else{
        $homeLink = route('mystore.published',[$userSettings->public_link]);
    }
    }else{
        $homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
    }
    
if(empty($meta_data['settings']['currency']))
        $currency = "₹";
else
    if($meta_data['settings']['currency_selection'] == "currency_symbol")
        $currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
    else
        $currency = $meta_data['settings']['currency'];
@endphp
@section('content')
@include('themes.'.$meta_data['themes']['name'].'.modals.details_product')
@include('themes.'.$meta_data['themes']['name'].'.partials.public_banner')
<div class="content-header row px-1">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb border-0">
                        <li class="breadcrumb-item"><a class="text-green" href="{{$homeLink}}">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a class="text-green" href="{{$catalogLink}}">{!! $campaign->name == "Untitled"?"&nbsp;":$campaign->name !!}</a>
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="ecommerce-application h-100 px-1">
    <div class="card-body">
        <section id="ecommerce-category_products" class="grid-view d-block mb-4">
            <div id="category_products" class="row">
            
            </div>
        </section>
    </div>

</div>
@endsection
@section('scripts')
@parent
<script src="{{asset('XR/assets/js/cart.js')}}"></script>
<script>
    var q = "";
    var sort = "";
    var page = 0;
    var preview = 0;
    var meta = @json($meta_data);
    var theme;
    var category_id = "{{$category->id}}";
    if (meta) {
        if (meta['themes']) {
            if (meta['themes']['name'])
                theme = meta['themes']['name'];
        } else
            theme = "default-theme";
    } else {
        theme = "default-theme";
    }
    var page_url = window.location.pathname;
    var shareable_link = "{{($userSettings->is_start == 1)?$campaign->public_link:$campaign->preview_link}}";

    var u = window.location.href;
    if (u.includes("preview")) {
        preview = 1;
    }
    getCategoryProducts(0);

    $("#ecommerce-category_products").on('click', '.pagination a', function(event) {
        event.preventDefault();

        $('li').removeClass('active');
        $(this).parent('li').addClass('active');

        var myurl = $(this).attr('href');
        var page = $(this).attr('href').split('page=')[1];

        getCategoryProducts(page);
    });

    function getCategoryProducts(page) {
        $('.loading1').show();
        q = 'q=' + sort + '&page=' + page + '&preview=' + preview + '&shareable_link=' + shareable_link + '&theme=' + theme + '&category_id=' + category_id;
        $.ajax({
            url: "{{route('admin.campaigns.ajaxCategoryProductData')}}" + '?' + q,
            type: "get",
            datatype: "html"
        }).done(function(data) {
            $("#category_products").empty().html(data);
            //$("#product_list").append(data);
            $('.loading').hide();
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            $('.loading').hide();
            // alert('No response from server');
        });
    }
    function tryon(pid){
        window.location.href = window.location.href + "?sku=" + pid;
    }
    // $('img').on("error", function() {
    //   $(this).attr('src', '{{asset('XR/assets/images/placeholder.png')}}');
    //   $(this).css('width', '95%');
    //   $(this).css('max-height', '100%');
    // });
    
</script>

<script src="{{asset('XR/app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')}}">
<script>
// checkout quantity counter
  var quantityCounter = $(".quantity-counter"),
    CounterMin = 1,
    CounterMax = 10;
  if (quantityCounter.length > 0) {
    quantityCounter.TouchSpin({
      min: CounterMin,
      max: CounterMax
    }).on('touchspin.on.startdownspin', function () {
      var $this = $(this);
      $('.bootstrap-touchspin-up').removeClass("disabled-max-min");
      if ($this.val() == 1) {
        $(this).siblings().find('.bootstrap-touchspin-down').addClass("disabled-max-min");
        }
        obj.setCountForItem(Number($this.data('id')),$this.val());
        //$("#product_qty_"+$this.data('id')).val($this.val());
        
    }).on('touchspin.on.startupspin', function () {
      var $this = $(this);
      $('.bootstrap-touchspin-down').removeClass("disabled-max-min");
      if ($this.val() == 10) {
        $(this).siblings().find('.bootstrap-touchspin-up').addClass("disabled-max-min");
      }
      obj.setCountForItem(Number($this.data('id')),$this.val());
      //$("#product_qty_"+$this.data('id')).val($this.val());
      
    });
  }
  obj.setCountForItem = function(id, qty) {
    // console.log("set count "+ id,qty);
    for(var i in cart) {
      if (cart[i].id === id) {
        cart[i].qty = qty;
        break;
      }
    }
    saveCart();
    obj.totalCart();
  };
  
  $( document ).ready(function() {
        cart = JSON.parse(localStorage.getItem('shoppingCart'));
        var cartItemCount = count = cart.length;
        //console.log(cart)
        if(cart.length > 0) {
            var products = [];
            for(var item in cart) {
                var id = cart[item].id, qty = cart[item].qty;
                //console.log("loadcount "+ id,qty);
                $("#product_qty_"+(id)).val(qty);
            }
            
        }
    });

</script>
@endsection