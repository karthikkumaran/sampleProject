 @extends('themes.'.$meta_data['themes']['name'].'.layouts.public')
 @section ('styles')
 @parent
 <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')}}">
 <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
 <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/plugins/forms/wizard.css')}}">
 <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/file-uploaders/dropzone.min.css')}}">
 <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/plugins/file-uploaders/dropzone.css')}}">
 <style>
     .dropzone {
         /* position: relative; */
         width: 315px;
     }

     .dropzone .dz-message {
         height: auto !important;
     }

     .img-responsive {
         margin: 0 auto;
         width: 50px;
     }
 </style>

 @endsection
 @section('content')
 @php

 if($campaign->is_start == 1) {

 $catalogLink = route('campaigns.published',[$campaign->public_link]);

 }else{

 $catalogLink = route('admin.campaigns.preview',[$campaign->preview_link]);

 }


 if($userSettings->is_start == 1) {

 if(!empty($userSettings->customdomain)){

 $homeLink = '//'.$userSettings->customdomain;

 }else if(!empty($userSettings->subdomain)) {

 $homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');

 }else{

 $homeLink = route('mystore.published',[$userSettings->public_link]);
 }

 }else{
 $homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
 }


 if(empty($meta_data['settings']['currency']))
 $currency = "₹";
 else
 if($meta_data['settings']['currency_selection'] == "currency_symbol")
 $currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
 else
 $currency = $meta_data['settings']['currency'];

 $upi_id=$meta_data['payment_settings']['UPI']['upi-id'];
 $upi_name=$meta_data['payment_settings']['UPI']['upi-name'];

 $agent=new \Jenssegers\Agent\Agent();

 @endphp

 <body>

     <div class="content-header row px-1">
         <div class="content-header-left col-md-9 col-12 mb-2">
             <div class="row breadcrumbs-top">
                 <div class="col-12">
                     <div class="breadcrumb-wrapper col-12">
                         <ol class="breadcrumb border-0">
                             <li class="breadcrumb-item"><a href="#">Home</a>
                             </li>
                             <li class="breadcrumb-item"><a href="#">{!! $campaign->name == "Untitled"?"&nbsp;":$campaign->name !!}</a>
                             </li>
                             <li class="breadcrumb-item "><a href="#">Checkout</a>
                             </li>
                             <li class="breadcrumb-item active">Payment
                             </li>
                         </ol>
                     </div>
                 </div>
             </div>
         </div>
     </div>
     <div class="content-body ecommerce-application checkout-tab-steps icons-tab-steps  wizard-circle mx-1">



         <!-- Checkout Place order starts -->
         <h6><i class="step-icon step feather icon-shopping-cart"></i>Cart</h6>
         <fieldset class="checkout-step-1 px-0">

         </fieldset>
         <!-- Checkout Place order Ends -->

         <!-- Checkout Customer Address Starts -->
         <h6><i class="step-icon step feather icon-home"></i>Address</h6>
         <fieldset class="checkout-step-2 px-0">
         </fieldset>

         <!-- Checkout Customer Address Ends -->




         <!-- checkout payment starts -->
         <h6><i class="step-icon step fa fa-money"></i>Payment</h6>

         <fieldset class="checkout-step-3 px-0">
             <div class="card p-1">
                 <div class="text-center pay-step-1" style="margin:0 auto;">
                     <img class="img-responsive" src="{{ asset('XR/app-assets/images/icons/upi.svg') }}" style="width:100px;">

                     <div class="text-center">

                         <img class="img-responsive" src="{{ asset('XR/app-assets/images/icons/bhim-logo.png') }}">

                         <img class="img-responsive" src="{{ asset('XR/app-assets/images/icons/paytm-logo.png') }}">


                         <img class="img-responsive" src="{{ asset('XR/app-assets/images/icons/phonepe-logo.png') }}">

                         <img class="img-responsive" src="{{ asset('XR/app-assets/images/icons/gpay-logo.png') }}">

                         <!-- <img class="img-responsive" src="{{ asset('XR/app-assets/images/icons/axispay-logo.png') }}">

                         <img class="img-responsive" src="{{ asset('XR/app-assets/images/icons/sbipay-logo.png') }}">

                         <img class="img-responsive" src="{{ asset('XR/app-assets/images/icons/hdfcpay-logo.png') }}"> -->
                     </div>

                     <div class="mb-1 p-1" id="qrcode" data-url="upi://pay?pa={{$upi_id}}&tn={{$desc}}&am={{$amount}}&pn={{$upi_name}}" style="width:fit-content; margin: 0 auto;border-style: solid; border-color:black;"></div>


                     <!-- <div style="margin:0 auto;"> -->
                     <div class="text-center" style="border:dotted;border-radius: 10px;width: 100%;word-break: break-all;min-width:280px;">
                         <button class="btn btn-flat btn-icon" style="" onclick="catalogCopyToClipboard('#public_catalog_link_copy')">
                             <b> <span id="public_catalog_link_copy">{{$upi_id}}</span></b><br><br><i class="feather icon-copy"></i> Tap to copy
                         </button>
                     </div>
                     </br>

                     <div class="text-left">
                         <h4>Order <b>#{{$data['unique_store_order_id']}}</b></h4>

                         <h4>UPI Name: <b>{{$upi_name}}</b></h4>
                         <h4>
                             Amount: <b>{!!$currency!!}{{$amount}}</b>
                         </h4>
                     </div>
                     <!-- </div> -->
                     {{-- @if($agent->isTablet() || $agent->isMobile())
                             <a href="upi://pay?pa={{$upi_id}}&tn={{$desc}}&am={{$amount}}&pn={{$upi_name}}" id="pay_now" class="btn btn-primary">Pay Now</a>
                     @endif
                     --}}
                     <!-- <p class="">
                              In the next screen you will be requested to submit your transaction details. </br>After making the payment please take screenshot and note down the transaction reference no. 
                          </p>
                              </br>
                        <button type="button" id="next" class="btn btn-primary float-right">Next</button> -->

                 </div>
                 <div style="margin:0 auto;width: 100%;max-width:280px;" class="pay-step-1">

                     <p class="text-left p-1" style="background-color: #e2e2e2;">
                         <b> Note:</b>
                         </br>
                         In the next screen you will be requested to submit your transaction details. After making the payment please take screenshot and note down the transaction reference no.
                     </p>
                     </br>
                     <button type="button" id="next" class="btn btn-primary float-right">Next</button>
                 </div>
                 <div class="pay-step-2 d-flex justify-content-center">
                     <div class="mb-1">
                         <h3>Enter the transaction details </h3><br>

                         <label for="trans_id">UPI Reference No./ Transaction No.</label>
                         <input class="form-control col-md-12 col-sm-12 " type="text" name="trans_id" id="trans_id" placeholder="Transaction No" aria-describedby="transaction_num" required>

                         <small id="transaction_num" class="form-text  text-break ">
                             Enter the UPI Reference No./ Transaction No. after completing the payment
                         </small>
                     </div>
                 </div>

                 <form id="upi_form"></form>

                 <div class="pay-step-2" style="margin:0 auto;">

                     <div class="dropzone dropzone-area mb-1" id="photo-dropzone">
                         <div class="dz-message" style='font-size:18px;'>Upload the screenshot of transaction completion</div>
                     </div>

                     <input type="hidden" id="data" value="{{json_encode( $data)}}">
                     <button type="button" id="back" class="btn btn-primary float-left">Back</button>
                     <button type="button" id="submit" class='submit btn btn-primary float-right' disabled>Submit</button>
                 </div>


             </div>
         </fieldset>

         <!-- checkout payment ends -->

     </div>



 </body>
 @endsection
 @section('scripts')
 @parent
 <script src="{{asset('XR/assets/js/cart.js')}}"></script>
 <script src="{{asset('XR/app-assets/vendors/js/extensions/jquery.steps.min.js')}}"></script>
 <script src="https://cdn.jsdelivr.net/npm/easyqrcodejs@4.1.0/dist/easy.qrcode.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
 <!-- <script src="https://cdn.jsdelivr.net/npm/davidshimjs-qrcodejs@0.0.2/qrcode.min.js"></script> -->
 <script>
     Dropzone.autoDiscover = false;

     $(document).ready(function() {
         "use strict";
         // Checkout Wizard
         var checkoutWizard = $(".checkout-tab-steps"),
             checkoutValidation = checkoutWizard.show();
         if (checkoutWizard.length > 0) {
             $(checkoutWizard).steps({
                 headerTag: "h6",
                 bodyTag: "fieldset",
                 transitionEffect: "fade",
                 titleTemplate: '<span class="step">#index#</span> #title#',
                 enablePagination: false,
                 onStepChanging: function(event, currentIndex, newIndex) {
                     //DON'T allows to go back to previous step IF
                     if (currentIndex > newIndex) {
                         return false;
                     } else {
                         return true;
                     }

                 },
                 onFinished: function(event, currentIndex) {

                     var qrcode = new QRCode(document.getElementById('qrcode'), {
                         text: $('#qrcode').attr('data-url'),
                         width: 250,
                         height: 250,
                         colorDark: "#000000",
                         colorLight: "#ffffff",
                         logo: "{{ asset('XR/app-assets/images/icons/upi.svg') }}",
                         correctLevel: QRCode.CorrectLevel.H
                     });

                     $('#trans_id').on('input', function() {
                         submit_button();
                     });

                     $('#next').on('click', function() {
                         $('.pay-step-1').hide();
                         $('.pay-step-2').children().show();
                     });

                     $('#pay_now').on('click', function() {
                         $('.pay-step-1').hide();
                         $('.pay-step-2').children().show();
                     });

                     $('#back').on('click', function() {
                         $('.pay-step-1').show();
                         $('.pay-step-2').children().hide();
                     });

                     var uploadedPhotoMap = {};
                     PaymentScreenshotDropzone = new Dropzone('#photo-dropzone', {
                         url: "{{ route('payment.storeMedia')}}",
                         maxFilesize: 10, // MB
                         maxFiles: 1,
                         acceptedFiles: '.jpeg,.jpg,.png,.gif',
                         addRemoveLinks: true,
                         headers: {
                             'X-CSRF-TOKEN': "{{ csrf_token() }}"
                         },
                         params: {
                             size: 40,
                             width: 4086,
                             height: 4096
                         },
                         success: function(file, response) {
                            $('#upi_form').append('<input type="hidden" name="photo" value="' + response.name + '">')
                            uploadedPhotoMap[file.name] = response.name
                         },
                         removedfile: function(file) {
                             if (this.files.length === 0 || $('#trans_id').val() == '') {
                                 $("#submit").prop('disabled', true);
                             } else {
                                 $("#submit").prop('disabled', false);
                             }

                             file.previewElement.remove()
                             var name = ''
                             if (typeof file.file_name !== 'undefined') {
                                 name = file.file_name
                             } else {
                                 name = uploadedPhotoMap[file.name]
                             }

                             $('#upi_form').find('input[name="photo"][value="' + name + '"]').remove()
                         },
                         init: function() {
                             myDropzone = this; // closure
                             this.on("addedfile", function(file) {
                                 if (this.files.length != 0 && $('#trans_id').val() != '') {
                                     $("#submit").prop('disabled', false);
                                 } else {
                                     $("#submit").prop('disabled', true);
                                 }
                             });
                         },
                     });

                     $('.submit').click(function() {
                         store_transaction();
                     });
                 },


             });
             // to move to next step on place order and save address click

             $(".checkout-tab-steps").steps("next", {});
             $(".checkout-tab-steps").steps("next", {});
             $(".checkout-tab-steps").steps("finish", {});




         }

         $('.pay-step-1').show();
         $('.pay-step-2').children().hide();

     });


     function store_transaction() {
         data = $('#data').val();
         data = JSON.parse(data);
         data['transaction_id'] = $('#trans_id').val();
        //  data['photo'] = uploadedPhotoMap;
         data[$('#upi_form').serializeArray()[0].name] = $('#upi_form').serializeArray()[0].value;
         $('.submit').attr('disabled', 'disabled');
         var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> loading...';
         $('.submit').html(loadingText);
         $.ajax({
             url: "{{route('transaction.store')}}",
             type: "POST",
             data: data,
             success: function(response) {
                 url = window.location.origin + '/pc/' + response.key + '/' + response.order_id;
                 window.location = url;
             }
         });
     }

     function catalogCopyToClipboard(element) {
         var $temp = $("<input>");
         $(".card").append($temp);
         $temp.val($(element).text());
         $temp.select();
         document.execCommand("copy");
         $temp.remove();
         toastr.success('Share Link', 'Copied!');
     }




     var myDropzone = null;
     var uploadedPhotoMap = {};
     PaymentScreenshotDropzone = new Dropzone('#photo-dropzone', {
         url: "{{ route('payment.storeMedia')}}",
         maxFilesize: 5, // MB
         maxFiles: 1,
         acceptedFiles: '.jpeg,.jpg,.png,.gif',
         addRemoveLinks: true,
         headers: {
             'X-CSRF-TOKEN': "{{ csrf_token() }}"
         },
         params: {
             size: 40,
             width: 4086,
             height: 4096
         },
         success: function(file, response) {
             $('#upi_form').append('<input type="hidden" name="photo" value="' + response.name + '">')
             uploadedPhotoMap[file.name] = response.name
         },
         removedfile: function(file) {
             submit_button();

             //  if (this.files.length === 0 || $('#trans_id').val() == '') {
             //      $("#submit").prop('disabled', true);
             //  } else {
             //      $("#submit").prop('disabled', false);
             //  }

             file.previewElement.remove()
             var name = ''
             if (typeof file.file_name !== 'undefined') {
                 name = file.file_name
             } else {
                 name = uploadedPhotoMap[file.name]
             }

             $('#upi_form').find('input[name="photo"][value="' + name + '"]').remove()
         },
         init: function() {

             var submitButton = document.querySelector("#submit");
             myDropzone = this; // closure
             //  submitButton.addEventListener("click", function() {
             //      if (myDropzone.getUploadingFiles().length === 0 && myDropzone.getQueuedFiles().length === 0) {} else {
             //          myDropzone.processQueue();
             //      }
             //  });


             this.on("addedfile", function(file) {
                 //  if (this.files.length != 0 && $('#trans_id').val() != '') {
                 //      $("#submit").prop('disabled', false);
                 //  } else {
                 //      $("#submit").prop('disabled', true);
                 //  }
                 submit_button();
             });


         },
     });

     function submit_button() {
        //  console.log('1');
         count = myDropzone ? myDropzone.files.length : 0;
         value = $('#trans_id').val();
         if (count != 0 && value != '') {
             $("#submit").prop('disabled', false);
         } else {
             $("#submit").prop('disabled', true);
         }
     }
 </script>
 @endsection