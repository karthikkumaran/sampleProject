@extends('themes.'.$meta_data['themes']['name'].'.layouts.public')
@section ('styles')
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">

<link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/extensions/nouislider.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/ui/prism.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/forms/select/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/core/colors/palette-gradient.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/plugins/extensions/noui-slider.min.css') }}">
<style>
    .videoIcon{
        margin:auto;
        height:200px;
        bottom:0;
        top:0;
        left: 0;
        right: 0;
    }
    @media (max-width: 576px){
    .iframe{
        height:270px;
        width: 300px;
    }
}
</style>
@endsection
@php
$attributes = [
  'type' => null,
  'class' => 'iframe',
  'data-html5-parameter' => true
];
$whitelist = []; $params = [];
$agent=new \Jenssegers\Agent\Agent();
if($userSettings->is_start == 1) {
if(!empty($userSettings->customdomain)){
$homeLink = '//'.$userSettings->customdomain;
}else if(!empty($userSettings->subdomain)) {
$homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
}else{
$homeLink = route('mystore.published',[$userSettings->public_link]);
}
}else{
$homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
}

if(empty($meta_data['settings']['currency']))
$currency = "₹";
else
if($meta_data['settings']['currency_selection'] == "currency_symbol")
$currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
else
$currency = $meta_data['settings']['currency']
@endphp
@section('content')
@include('themes.'.$meta_data['themes']['name'].'.modals.details_product')
@include('themes.'.$meta_data['themes']['name'].'.partials.public_banner')


@php
    if($campaign->is_start == 1) {
        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
        $catalogLink = route('campaigns.published',$campaign->public_link);
        $shoppableLink = route('mystore.publishedVideo',$userSettings->public_link);
    }else{
        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
        $catalogLink = route('admin.campaigns.preview',$campaign->preview_link);
        $shoppableLink = route('mystore.publishedVideo',$userSettings->preview_link);
    }
@endphp

<div class="content-header row px-1">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <div class="breadcrumb-wrapper col-12" style="margin-left: -44px;">
                    <ol class="breadcrumb border-0">
                        @if( (request()->is('s/*') || request()->is('admin/storePreview/*')) == false)
                        <li class="breadcrumb-item ml-2" onclick="track_events('breadcrumb-home-button-clicked',null)" style="font-size:14px;color: #0B1A89 !important;"><a class="text-green" href="{{$homeLink}}"><i class="fa fa-home mr-1"></i>Home</a></li>
                            @if(!empty($video_id))
                            <li class="breadcrumb-item">Shoppable Videos</li>
                            <li class="breadcrumb-item ">{!! $campaign->name == "Untitled"?"&nbsp;":$campaign->name !!}</li>
                            @else
                            <li class="breadcrumb-item">{!! $campaign->name == "Untitled"?"&nbsp;":$campaign->name !!}</li>
                            @endif
                        @else
                        <li class="breadcrumb-item ml-2" onclick="track_events('breadcrumb-home-button-clicked',null)" style="font-size:14px;color: #0B1A89 !important;"><a class="text-green" href="{{$homeLink}}"><i class="fa fa-home mr-1"></i>Home</a></li>
                        <li class="breadcrumb-item" style="font-size:14px">{!! $campaign->name == "Untitled"?"&nbsp;":$campaign->name !!}</li>
                        @endif
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="ecommerce-application h-100 px-1">
    <div class="content-detached content-right">
        @if(empty($video_id))
        <div class="content-body ">
        @else
        <div class="content-body ml-0">
        @endif
        @if(empty($video_id))
            <!-- Ecommerce Content Section Starts -->
            <section id="ecommerce-header">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ecommerce-header-items">
                            <div class="result-toggler">
                                <!-- <button class="navbar-toggler shop-sidebar-toggler" type="button" data-toggle="collapse">
                                    <span class="navbar-toggler-icon d-block d-lg-none"><i class="feather icon-filter"></i></span>
                                </button> -->

                                <div class="search-results">
                                    <span id="productssearchTotal"></span> results found
                                </div>
                            </div>
                            <div class="view-options">
                                <!-- <select class="price-options form-control" id="ecommerce-price-options">
                                    <option selected>Featured</option>
                                        <option value="">Price: Low to High</option>
                                        <option value="">Price: High to Low</option>
                                        <option value="">Name: A to Z</option>
                                        <option value="">Name: Z to A</option>
                                    </select> -->
                                <!-- <div class="view-btn-option d-lg-inline-block d-none d-md-none d-sm-none">
                                    <button class="btn btn-white view-btn" id="show_filter_btn" style="display:none;" onclick="show_filter()">
                                        <i class="feather icon-filter"></i>
                                    </button>
                                </div> -->
                                <!-- <div class="view-btn-option">
                                        <button class="btn btn-white view-btn grid-view-btn active">
                                            <i class="feather icon-grid"></i>
                                        </button>
                                        <button class="btn btn-white list-view-btn view-btn">
                                            <i class="feather icon-list"></i>
                                        </button>
                                    </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!-- background Overlay when sidebar is shown  starts-->
            <!-- <div class="shop-content-overlay"></div> -->
            <!-- background Overlay when sidebar is shown  ends-->

            <!-- Ecommerce Search Bar Starts -->
            <section id="ecommerce-searchbar">
                <div class="row mt-1 ">
                    <div class="col-sm-12 d-inline-flex">
                        @if(empty($video_id))
                        <div id="searchdiv" style="{{($agent->isDesktop()||$agent->isTablet())?'width:90%;':'width:85%;'}}">
                        @else
                        <div id="searchdiv" style="{{($agent->isDesktop()||$agent->isTablet())?'width:100%;':'width:100%;'}}">
                        @endif
                            <fieldset class="form-group position-relative">
                                <input type="text" class="form-control search-product" id="CatalogProductSearch" placeholder="Search here">
                                <div class="form-control-position">
                                    <i class="feather icon-search"></i>
                                </div>
                            </fieldset>
                        </div>
                        @if(empty($video_id))
                        <div id="filterdiv" class="result-toggler justify-content-center align-items-center" style="{{($agent->isDesktop()||$agent->isTablet())?'width:10%;':'width:15%;'}}display:flex;">
                            <button class="navbar-toggler shop-sidebar-toggler w-100 h-75 d-flex justify-content-center align-items-center" type="button" data-toggle="collapse" onclick="show_filter()">
                                <span class="navbar-toggler-icon d-block d-lg-block d-md-block d-sm-block"><i id="filterbuttondiv" class="feather icon-filter" style="font-size:21px;display:flex;"></i></span>
                            </button>
                        </div>
                        @endif
                    </div>
                </div>
            </section>
            <!-- Ecommerce Search Bar Ends -->
        @endif
            @if(!empty($video_data))
            @foreach ($video_data as $key => $card)
                    @php
                    $video = $card->video;
                    @endphp
                    @if($card->video_id == $video_id)
                        <div class="card ecommerce-card  p-1 w-100" style="height:430px; display: inline-block">
                                <div class="card-content " style=" position: relative; padding-bottom: 30%; padding-top: 10px;  height: 0; ">
                                <h4 class="text-truncate"><b>{{$video->title}}</b></h4>
                                <center>
                                    <div style="overflow: hidden;">
                                        {!! \LaravelVideoEmbed::parse( old('video_url', $video->video_url), $whitelist, $params,$attributes ) !!}

                                    </div>
                                    <hr>
                                    <span class="float-left p-1 cursor-pointer text-green" id="hidevideo" onclick='hidevideo()' ><i class="feather icon-box"></i> Products </span>
                                    <span class="float-right p-1 cursor-pointer relatedVideos" id="hideproduct" onclick='hideproduct()'><i class="feather icon-video"></i> Related Videos </span>
                                </center>

                                </div>

                        </div>
                    @endif
                @endforeach
                @endif

            <!-- Ecommerce Products Starts -->
            <section id="ecommerce-productscatalog" class="grid-view d-block mb-4">

                <div id="catalog_prod" class="row">

                </div>
            </section>
            <!-- Ecommerce Products Ends -->
            <!-- Ecommerce Videos Starts -->
            <section id="ecommerce-videocatalog" class="grid-view d-block mb-4">
                <div id="catalog_video" class="row">

                </div>
            </section>
            <!-- Ecommerce Videos Ends -->
            <!-- <div id="catalog_video" style="display:none;">
            @if(!empty($video_data))
            @foreach ($video_data as $key => $card)
                    @php
                    $video = $card->video;
                    @endphp
                    @php
                    $video = $card->video;
                    if(!empty($video->thumbnail))
                    {
                       $img = $video->thumbnail->getUrl('thumb');
                    }
                    else{
                        $img = asset('XR/assets/images/placeholder.png');
                    }
                    @endphp
                    @if($card->video_id != $video_id)
                    @php
                    $isproduct = false;
                    @endphp
                        @foreach($data as $key => $show)
                            @if($show->video_id == $card->video_id && !empty($show->product_id))
                                @php
                                    $isproduct = true;
                                @endphp
                            @endif
                        @endforeach
                        @if($isproduct == true)

                        <div class="card ecommerce-card col-lg-3 col-md-6 col-sm-12 p-1 " style="width:320px;  display: inline-block">
                                <div class="card-content" style="margin:auto;">
                                        <a href="{{$catalogLink}}?video-id={{$video->id}}"> <i class="feather icon-play-circle fa-5x position-absolute text-green d-flex justify-content-center videoIcon"  ></i>
                                        <img class="img-responsive mw-80 mh-80 rounded" src="{{$img}}" style="height:200px; width:100%; cursor:pointer;"/></a>
                                        <b>{!! $video->title !!}</b>
                                    <hr>
                                </div>
                                <div class="cart rounded text-center" style=" background-color:blueviolet;" >
                                    <a href="{{$catalogLink}}?video-id={{$card->video_id}}" style="color:white;" >View video product</a>
                                </div>
                        </div>
                        @endif

                    @endif
                @endforeach
                @endif
</div> -->

        </div>
    </div>
    @if(empty($video_id))
    <div id="sidebar" class="sidebar-detached sidebar-left">
        <div class="sidebar">
            <!-- Ecommerce Sidebar Starts -->
            <div class="sidebar-shop pb-sm-0" id="ecommerce-sidebar-toggler">
                <div class="row">
                    <div class="col-sm-12">
                        <h6 class="filter-heading d-none d-lg-block">
                            Filters
                            <span class="float-right" onclick="hide_filter()">
                                <i class="feather icon-x fa-lg" style="cursor:pointer;"></i>
                            </span>
                        </h6>
                    </div>
                </div>

                <span class="sidebar-close-icon d-block d-md-block d-lg-none" id="mobile_close_icon" onclick="hide_filter()">
                    <i class="feather icon-x"></i>
                </span>

                <div class="card" style="padding-bottom: 0;">
                    <div class="card-body" style="padding-bottom: 2vh;">
                        <div class="multi-range-price">
                            <div class="multi-range-title pb-75">
                                <h6 class="filter-title mb-0">Price Range</h6>
                            </div>
                            <!-- <ul class="list-unstyled price-range" id="price-range">
                                <li>
                                    <span class="vs-radio-con vs-radio-primary py-25">
                                        <input type="radio" name="price-range" checked value="all">
                                        <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                        </span>
                                        <span class="ml-50">All</span>
                                    </span>
                                </li>

                                <li>
                                    <span class="vs-radio-con vs-radio-primary py-25">
                                        <input type="radio" name="price-range" value="0,500">
                                        <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                        </span>
                                        <span class="ml-50">&lt; {!! $currency !!} 500</span>
                                    </span>
                                </li>

                                <li>
                                    <span class="vs-radio-con vs-radio-primary py-25">
                                        <input type="radio" name="price-range" value="500,1000">
                                        <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                        </span>
                                        <span class="ml-50">{!! $currency !!} 500 - {!! $currency !!} 1000</span>
                                    </span>
                                </li>

                                <li>
                                    <span class="vs-radio-con vs-radio-primary py-25">
                                        <input type="radio" name="price-range" value="1000,10000">
                                        <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                        </span>
                                        <span class="ml-50">{!! $currency !!} 1000 - {!! $currency !!} 10000</span>
                                    </span>
                                </li>

                                <li>
                                    <span class="vs-radio-con vs-radio-primary py-25">
                                        <input type="radio" name="price-range" value="10000">
                                        <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                        </span>
                                        <span class="ml-50">&gt; {!! $currency !!} 10000</span>
                                    </span>
                                </li>
                            </ul>-->
                        </div>
                        <!-- /Price Filter -->
                        <!-- <hr> -->

                        <!-- /Price Slider -->
                        <div class="price-slider" data-currency="{{$currency}}">
                            <div class="form-group row mt-1">
                                <div class="form-group col-6 w-100">
                                    <input class="form-control" style="width: 100%" type="number" name="price_slider_min" id="price_slider_min" placeholder="Min">
                                </div>
                                <div class="form-group col-6 w-100">
                                    <input class="form-control" style="width: 100%" type="number" name="price_slider_max" id="price_slider_max" placeholder="Max">
                                </div>
                                <div class="alert alert-warning" id="error_min" style="display:none;" role="alert">Minimum value should be less than maximum value.</div>
                                <div class="alert alert-warning" id="error_max" style="display:none;" role="alert">Maximum value should be greater than minimum value.</div>
                            </div>
                            <div class="price-slider">
                                <div class="price_slider_amount mb-2">
                                </div>
                                <div class="form-group">
                                    <div class="slider-sm my-1 range-slider" id="price-slider"></div>
                                </div>
                            </div>
                        </div>
                        <!-- /Price Range -->
                        <!-- <hr> -->
                        @if(count($categories)>0)
                        <!-- Categories Starts -->
                        <div id="product-categories">
                            <div class="product-category-title">
                                <h6 class="filter-title mb-1">Categories</h6>
                            </div>
                            <ul class="list-unstyled categories-list">
                                <li class="d-flex justify-content-between align-items-center py-25">
                                    <span class="vs-checkbox-con vs-checkbox-primary">
                                        <input type="checkbox" name="category-filter-all" checked value="all">
                                        <span class="vs-checkbox">
                                            <span class="vs-checkbox--check">
                                                <i class="vs-icon feather icon-check"></i>
                                            </span>
                                        </span>
                                        <span class="">All</span>
                                    </span>
                                </li>
                                @foreach($categories as $key => $category)
                                <li class="d-flex justify-content-between align-items-center py-25">
                                    <span class="vs-checkbox-con vs-checkbox-primary">
                                        <input type="checkbox" name="category-filter" value="{!! $category->name == " Untitled"?"&nbsp;":$category->name !!}">
                                        <span class="vs-checkbox">
                                            <span class="vs-checkbox--check">
                                                <i class="vs-icon feather icon-check"></i>
                                            </span>
                                        </span>
                                        <span class="">{!! $category->name == "Untitled"?"&nbsp;":$category->name !!}</span>
                                    </span>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- Categories Ends -->
                        <hr>
                        @endif

                        {{-- <!-- Attributes Starts -->
                        @if(count($values)>0)
                        <div class="niceScroll" id="product-attributes" style="max-height: 250px; overflow-y: scroll;">
                            <div class="product-attributes-title">
                                <h6 class="filter-title mb-1 ">Attributes</h6>
                            </div>
                            <ul class="list-unstyled attributes-list">
                                <li class="d-flex justify-content-between align-items-center py-25">
                                    <span class="vs-checkbox-con vs-checkbox-primary">
                                        <input type="checkbox" name="attributes-filter-all" checked value="all">
                                        <span class="vs-checkbox">
                                            <span class="vs-checkbox--check">
                                                <i class="vs-icon feather icon-check"></i>
                                            </span>
                                        </span>
                                        <span class="">All</span>
                                    </span>
                                </li>
                                @foreach ($attributes as $groupname => $attributes)
                                    <b><span class="align-items-center">-- {{$groupname}}</span></b><br>
                                    @foreach($attributes as $attribute)
                                        @if ($attribute->type=="textbox")
                                            @continue
                                        @endif
                                        <span class="ml-1">-- {{$attribute->label}}</span>
                                        @php
                                            $values_arr = explode (",", $attribute->values);
                                        @endphp
                                        @foreach($values_arr as $value)
                                            @if(!in_array($value, $content_array))
                                                @continue
                                            @endif
                                            <li class="d-flex justify-content-between align-items-center py-25 ml-2">
                                                <span class="vs-checkbox-con vs-checkbox-primary">
                                                    <input type="checkbox" name="attributes-filter" value="{{$value}}" data-attribute_id="{{$attribute->id}}">
                                                    <span class="vs-checkbox">
                                                        <span class="vs-checkbox--check">
                                                            <i class="vs-icon feather icon-check"></i>
                                                        </span>
                                                    </span>
                                                    <span>{{$value}}</span>
                                                </span>
                                            </li>
                                        @endforeach
                                    @endforeach
                                @endforeach
                            </ul>
                        </div>
                        <hr>
                        @endif
                        <!-- Attributes Ends --> --}}

                        <!-- Apply Filters Starts -->
                        <div id="apply_filters">
                            <button class="btn btn-block btn-warning" onclick="track_events('apply-filter-button-clicked',null)" id="apply_filter " onclick="apply_filter()">APPLY FILTERS</button>
                        </div>
                        <!-- Apply Filters Ends -->
                        <br>
                        <!-- Clear Filters Starts -->
                        <div id="clear-filters">
                            <button class="btn btn-block btn-primary" onclick="track_events('clear-filter-button-clicked',null)" id="clear_filter " onclick="clear_filter()">CLEAR ALL FILTERS</button>
                        </div>
                        <!-- Clear Filters Ends -->

                    </div>
                </div>
            </div>
            <!-- Ecommerce Sidebar Ends -->
        </div>
    </div>
    @endif
</div>
@endsection
@section('scripts')
@parent
<script src="{{asset('XR/assets/js/cart.js')}}"></script>

<!-- BEGIN: Page Vendor JS-->
<script src="{{ asset('XR/app-assets/vendors/js/ui/prism.min.js') }}"></script>
<script src="{{ asset('XR/app-assets/vendors/js/extensions/wNumb.js') }}"></script>
<script src="{{ asset('XR/app-assets/vendors/js/extensions/nouislider.min.js') }}"></script>
<script src="{{ asset('XR/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{ asset('XR/app-assets/js/core/app-menu.js') }}"></script>
<script src="{{ asset('XR/app-assets/js/core/app.js') }}"></script>
<script src="{{ asset('XR/app-assets/js/scripts/components.js') }}"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="{{ asset('XR/app-assets/js/scripts/pages/app-ecommerce-shop.js') }}"></script>
<!-- END: Page JS -->

<script>



    $(document).ready(function() {

        url = window.location.pathname;
        slink = "{{($userSettings->is_start == 1)?$campaign->public_link:$campaign->preview_link}}";

        if (url.includes('/s')) {
            track_events("store", null, slink);
        }
        track_events("catalog", null, slink);
    });


    function tryon(pid) {
        window.location.href = window.location.href + "?sku=" + pid;
    }
    // $('img').on("error", function() {
    //   $(this).attr('src', '{{asset('XR/assets/images/placeholder.png')}}');
    //   $(this).css('width', '95%');
    //   $(this).css('max-height', '100%');
    // });
    var q = "";
    var searchData = "";
    var sort = "";
    var filter = [];
    var page = 0;
    var filterData = "";
    var preview = 0;
    var video_id = 0;
    var meta = @json($meta_data);
    var theme;
    if (meta) {
        if (meta['themes']) {
            if (meta['themes']['name'])
                theme = meta['themes']['name'];
        } else
            theme = "default-theme";
    } else {
        theme = "default-theme";
    }
    var page_url = window.location.pathname;
    // shareable_link=page_url.slice(page_url.lastIndexOf('/')+1);
    var shareable_link = "{{($userSettings->is_start == 1)?$campaign->public_link:$campaign->preview_link}}";

    var u = window.location.href;
    if (u.includes("preview")) {
        preview = 1;
    }

    var price_slider_min = document.getElementById('price_slider_min');
    var price_slider_max = document.getElementById('price_slider_max');
    var slider = document.getElementById('price-slider');

    getProducts(0);

    $("#CatalogProductSearch").on('input', function() {
        searchData = $(this).val();
        if (searchData.length >= 3 || searchData == "")
            //console.log(searchData);
            getProducts(0);
    });
    $(document).on('click',"#ecommerce-productscatalog .pagination a", function(event) {
        console.log('prod')
        event.preventDefault();

        $('li').removeClass('active');
        $(this).parent('li').addClass('active');

        var myurl = $(this).attr('href');
        var page = $(this).attr('href').split('page=')[1];

        getProducts(page);
    });


    function getProducts(page) {
        $('.loading1').show();

        q = 'q=' + searchData + '&s=' + sort + '&page=' + page + '&preview=' + preview + '&filterData=' + filterData + '&shareable_link=' + shareable_link + '&theme=' + theme + '&filter=' + JSON.stringify(filter)
        @php
        if(isset($video_id)){
            @endphp
            q = q+'&video_id=' + '{{$video_id}}';
            @php
        }
        @endphp
        //console.log(q);
        $.ajax({
            url: "{{route('admin.campaigns.ajaxProductData')}}" + '?' + q,
            type: "get",
            datatype: "html"
        }).done(function(data) {
            //console.log(data);
            //console.log("sucess");
            $("#catalog_prod").empty().html(data);
            //$("#product_list").append(data);
            $('.loading').hide();
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            $('.loading').hide();
            // alert('No response from server');
        });
    }

    $(document).on('click',".relatedVideos", function(event) {
        event.preventDefault();
        console.log('video')
        // $('li').removeClass('active');
        // $(this).parent('li').addClass('active');

        // var myurl = $(this).attr('href');
        // var page = $(this).attr('href').split('page=')[1];
        getVideo(0);
    });


    function getVideo(page) {
        console.log('video test')
        $('.loading1').show();
        q = 'q=' + searchData + '&s=' + sort + '&page=' + page + '&preview=' + preview + '&filterData=' + filterData + '&shareable_link=' + shareable_link + '&theme=' + theme + '&filter=' + JSON.stringify(filter) ;
        @php
        if(isset($video_id)){
        @endphp
            q = q+'&video_id=' + '{{$video_id}}';
        @php
        }
        @endphp

        $.ajax({
            url: "{{route('admin.campaigns.ajaxRelatedVideo')}}" + '?' + q,
            type: "get",
            datatype: "html"
        }).done(function(data) {
            //console.log(data);
            //console.log("sucess");
            $("#catalog_video").empty().html(data);
            //$("#product_list").append(data);
            $('.loading').hide();
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            $('.loading').hide();
            // alert('No response from server');
        });
    }

    function clear_filter() {
        filter = [];
        $("[name=price-range]").filter("[value=all]").prop("checked", true);
        $("[name=category-filter-all]").filter("[value=all]").prop("checked", true);
        $("[name=category-filter]").prop("checked", false);
        $("[name=attributes-filter-all]").filter("[value=all]").prop("checked", true);
        $("[name=attributes-filter]").prop("checked", false);
        filter = [];
        getProducts(0);
    }

    function apply_filter() {
        filter = [];
        var price_count = 0;
        var category_count = 0;
        // var attributes_count = 0;
        var price_values_array = [];
        var category_values_array = [];
        // var attributes_values_array = [];
        var price = document.getElementsByName('price-range');
        for (i = 0; i < price.length; i++) {
            if (price[i].checked) {
                price_count++;
                filter.push({
                    type: 'price',
                    value: price[i].value
                });
            }
        }
        if (price_count == 0) {
            if ((price_slider_min.value != "") && (price_slider_max.value == "")) {
                filter.push({
                    type: 'price',
                    value: price_slider_min.value
                });
            } else if ((price_slider_min.value == "") && (price_slider_max.value != "")) {
                price_values_array.push(0);
                price_values_array.push(price_slider_max.value);
                var price_values = price_values_array.toString();
                filter.push({
                    type: 'price',
                    value: price_values
                });
            } else {
                price_values_array.push(price_slider_min.value);
                price_values_array.push(price_slider_max.value);
                var price_values = price_values_array.toString();
                filter.push({
                    type: 'price',
                    value: price_values
                });
            }
        }
        var catg = document.getElementsByName('category-filter');
        if(catg.length == 0){
            category_count++;
            filter.push({
                type: 'category',
                value: 'all'
            });
        }
        else{
            if ($('input[name="category-filter-all"]').is(":checked") == true) {
                category_count++;
                filter.push({
                    type: 'category',
                    value: $('input[name="category-filter-all"]').val()
                });
            } else {
                for (i = 0; i < catg.length; i++) {
                    if (catg[i].checked) {
                        category_count++;
                        category_values_array.push(catg[i].value);
                            var category_values = category_values_array.toString();
                    }
                }
                if (category_count > 0) {
                    filter.push({
                        type: 'category',
                        value: category_values
                    });
                }
            }
        }

        // var attributes = document.getElementsByName('attributes-filter');
        // if ($('input[name="attributes-filter-all"]').is(":checked") == true) {
        //     attributes_count++;
        //     filter.push({
        //         type: 'attributes',
        //         value: $('input[name="attributes-filter-all"]').val()
        //     });
        // } else {
        //     for (i = 0; i < attributes.length; i++) {
        //         if (attributes[i].checked) {
        //             attributes_count++;
        //             var attribute_id = attributes[i].dataset.attribute_id;
        //             var attribute_value = attributes[i].value;
        //             attributes_values_array.push({
        //                 'attribute_id':attribute_id,
        //                 'attribute_value':attribute_value
        //             });
        //             var attributes_values = attributes_values_array.toString();
        //         }
        //     }
        //     if (attributes_count > 0) {
        //         filter.push({
        //             type: 'attributes',
        //             value: attributes_values_array
        //         });
        //     }
        // }

        if ($("#mobile_close_icon").is(':visible') == true) {
            var s = $(".shop-content-overlay");
            var t = $(".sidebar-shop");
            t.removeClass("show");
            s.removeClass("show");
        }

        // console.log(filter);
        getProducts(0);
    }
    $('input[name="category-filter-all"]').on('change', function() {
        if ($('input[name="category-filter-all"]').is(":checked") == true) {
            $('input[name="category-filter"]').prop('checked', false);
        }
    });

    $('input[name="category-filter"]').on('change', function() {
        if ($('input[name="category-filter"]').is(":checked") == true) {
            $('input[name="category-filter-all"]').prop('checked', false);
        }
    });

    // $('input[name="attributes-filter-all"]').on('change', function() {
    //     if ($('input[name="attributes-filter-all"]').is(":checked") == true) {
    //         $('input[name="attributes-filter"]').prop('checked', false);
    //     }
    // });

    // $('input[name="attributes-filter"]').on('change', function() {
    //     if ($('input[name="attributes-filter"]').is(":checked") == true) {
    //         $('input[name="attributes-filter-all"]').prop('checked', false);
    //     }
    // });

    $("#price_slider_min").on('input', function() {
        $('input[name="price-range"]').prop('checked', false);
        slider.noUiSlider.set([$(this).val(), null]);
        var val = parseInt($(this).val());
        var max = parseInt($("#price_slider_max").val());
        if (val > max) {
            $('#error_min').show();
            $('#apply_filter').attr('disabled', true);
        } else {
            $('#error_max').hide();
            $('#error_min').hide();
            $('#apply_filter').attr('disabled', false);
        }
    });

    $("#price_slider_max").on('input', function() {
        $('input[name="price-range"]').prop('checked', false);
        slider.noUiSlider.set([null, $(this).val()]);
        var val = parseInt($(this).val());
        var min = parseInt($("#price_slider_min").val());
        if (val < min) {
            $('#error_max').show();
            $('#apply_filter').attr('disabled', true);
        } else {
            $('#error_max').hide();
            $('#error_min').hide();
            $('#apply_filter').attr('disabled', false);
        }

    });

    function hide_filter() {
        document.getElementById("filterbuttondiv").style.display = "flex";
        document.getElementById("filterdiv").style.display = "flex";
        document.getElementById("searchdiv").style.width = "{{($agent->isDesktop()||$agent->isTablet())?'90%':'85%'}}";
        document.getElementById("filterdiv").style.width = "{{($agent->isDesktop()||$agent->isTablet())?'10%':'15%'}}";
        // $("#hide_filter_btn").hide();
        // $("#show_filter_btn").show();
        $(".sidebar-detached").hide();
        $('.content-body').addClass('ml-0');
    }

    function show_filter() {
        document.getElementById("filterbuttondiv").style.display = "none";
        document.getElementById("filterdiv").style.display = "none";
        document.getElementById("searchdiv").style.width = "100%";
        document.getElementById("filterdiv").style.width = "0%";
        // $("#hide_filter_btn").show();
        // $("#show_filter_btn").hide();
        $(".sidebar-detached").show();
        $('.content-body').removeClass('ml-0');
    }

    $(document).ready(function() {
        slider.noUiSlider.on('slide', function(values, handle) {
            $('input[name="price-range"]').prop('checked', false);
            var value = values[handle];
            if (handle) {
                price_slider_max.value = value;
            } else {
                price_slider_min.value = value;
            }
        });
    });

function hideproduct()
{
    $("#catalog_prod").hide();
    $("#catalog_video").show();
    $('#hideproduct').addClass('text-green');
    $('#hidevideo').removeClass('text-green');
}


function hidevideo()
{
    $("#catalog_video").hide();
    $("#catalog_prod").show();
    $('#hidevideo').addClass('text-green');
    $('#hideproduct').removeClass('text-green');
}
</script>
@endsection
