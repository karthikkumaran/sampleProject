@extends('layouts.cam')
@section ('styles')
<style>
.main {
    background: black;
}

.cph {
    display: none;
}

.loader {
    border: 16px solid #f3f3f3;
    /* Light grey */
    border-top: 16px solid #3498db;
    /* Blue */
    /*#e80707 */
    border-radius: 50%;
    width: 170px;
    height: 170px;
    animation: spin 2s linear infinite;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    margin: auto;
    overflow: hidden;
    z-index: 1;
}

#imgLoading {
    display: none;
}

.content-hv {
    background-color: white;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    margin: auto;
    overflow: hidden;
    z-index: 1;
    max-width: 100%;
    max-height: 100%;

}

.hv-con {
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    margin: auto;
}

.nopadding {
    padding: 0 !important;
    margin: 0 !important
}

#camError {
    display: none;
}

@keyframes spin {
    0% {
        transform: rotate(0deg);
    }

    100% {
        transform: rotate(360deg);
    }
}

.wrapper {
    text-align: center;
    /* width: 500px;
height: 751px; */
    position: absolute;
    margin: auto;
    left: 0;
    right: 0;
    top: 0;
    /* bottom: 0; */
}

.nxtImg {
    display: none;
    overflow-x: auto;
    white-space: nowrap;
    /* width: 90%;
margin: 15px; */
    background: #f8f4f9;
}

.nxtImg img {
    margin: 5px;
    width: 80px;
    height: 80px;
    padding: 8px;
    border-radius: 10px;
}

.selected {
    border: solid 2px #c3c3c3;;
    background: #ffffff;
    margin: 10px;
}

.nxtImg::-webkit-scrollbar {
    display: none;
}

.nxtImgDetails{
    display: none;
    /* height:60px;
    background: white;
    border-top-left-radius: 10px;
    border-top-right-radius: 10px; */
    left: 0;
    right: 0;
    margin: auto;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
}

#buynow {
    padding: 7px;
}

#product_price {
    font-weight: 900;
}

#product_name {
    font-weight: 900;
}

.nxtprebtn {
    /* position: absolute; */
    z-index: 1;
    width: 100%;
    bottom: 5px;
    /* margin-left: -7px; */
    /* overflow-x: auto;  */
    cursor: pointer;
}


.takephoto-btn {
    position: absolute;
    /* z-index: 1; */
    bottom: 0;
    cursor: pointer;
    width: 60px;
    padding: 5px;
    /* left: 0; */
    right: 0;
    margin: auto;
}

/* modal */

.close {
    right: 20px
}

.INSTATRYSDK-modal {
    background-color: #fff;
    border-radius: 20px 20px 0 0;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%)
}

.INSTATRYSDK-modal-content {
    background-color: #fefefe;
    margin: auto;
    padding: 5px;
    border-radius: 20px 20px 0 0;
    width: 100%
}

#capImg {
    width: 100%;
    border-radius: 20px 20px 0 0
}

.share-footer {
    height: 45px
}

.share-close {
    left: 10px;
    text-align: center;
}

.share-download {
    left: 10px;
    text-align: center
}

.share-popup {
    /* position: absolute;
top: 0;
left: 0;
background-color: #fff;
border-radius: 10px;
position: fixed;
top: 50%;
left: 50%;
transform: translate(-50%, -50%) */
    max-width: 220px;
    right: -60px;
}


.share-popup .resp-sharing-button__link {
    width: 200px
}

.share-overlay {
    display: none;
    position: fixed;
    z-index: 9999;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgb(0, 0, 0, .5)
}

.share-header-close {
    right: 0;
    position: absolute;
}

#preimg {
    width: 100%;
    border: 2px solid white;
}

.btn-share {
    position: absolute;
    left: 0;
    right: 0;
    bottom: -20px;
    margin: auto;
}

.INSTATRYSDK-modal-overlay {
    display: none;
    position: fixed;
    z-index: 2;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgb(0, 0, 0, .5)
}

.share-footer-overlay {
    top: 0;
    left: 0;
    z-index: 1040;
    width: 100vw;
    height: 60px;
    background-color: #000;
    opacity: .5
}

.share-social {
    position: absolute;
    width: 100%;
    text-align: right;
    right: 10px
}

.share-more {
    display: none
}

.resp-sharing-button__icon,
.resp-sharing-button__link {
    display: inline-block
}


.resp-sharing-button__link {
    text-decoration: none;
    color: #fff;
    margin: .5em;
    cursor: pointer
}

.resp-sharing-button {
    border-radius: 5px;
    transition: 25ms ease-out;
    padding: .5em .75em;
    font-family: Helvetica Neue, Helvetica, Arial, sans-serif
}

.resp-sharing-button__icon svg {
    width: 1em;
    height: 1em;
    margin-right: .4em;
    vertical-align: top
}

.resp-sharing-button--small svg {
    margin: 0;
    vertical-align: middle
}

.resp-sharing-button__icon {
    stroke: #fff;
    fill: none
}

.resp-sharing-button__icon--solid,
.resp-sharing-button__icon--solidcircle {
    fill: #fff;
    stroke: none
}

.resp-sharing-button--twitter {
    background-color: #55acee
}

.resp-sharing-button--twitter:hover {
    background-color: #2795e9
}

.resp-sharing-button--pinterest {
    background-color: #bd081c
}

.resp-sharing-button--pinterest:hover {
    background-color: #8c0615
}

.resp-sharing-button--facebook {
    background-color: #3b5998
}

.resp-sharing-button--facebook:hover {
    background-color: #2d4373
}

.resp-sharing-button--tumblr {
    background-color: #35465c
}

.resp-sharing-button--tumblr:hover {
    background-color: #222d3c
}

.resp-sharing-button--reddit {
    background-color: #5f99cf
}

.resp-sharing-button--reddit:hover {
    background-color: #3a80c1
}

.resp-sharing-button--google {
    background-color: #dd4b39
}

.resp-sharing-button--google:hover {
    background-color: #c23321
}

.resp-sharing-button--linkedin {
    background-color: #0077b5
}

.resp-sharing-button--linkedin:hover {
    background-color: #046293
}

.resp-sharing-button--email {
    background-color: #777
}

.resp-sharing-button--email:hover {
    background-color: #5e5e5e
}

.resp-sharing-button--xing {
    background-color: #1a7576
}

.resp-sharing-button--xing:hover {
    background-color: #114c4c
}

.resp-sharing-button--whatsapp {
    background-color: #25d366
}

.resp-sharing-button--whatsapp:hover {
    background-color: #1da851
}

.resp-sharing-button--hackernews {
    background-color: #f60
}

.resp-sharing-button--hackernews:focus,
.resp-sharing-button--hackernews:hover {
    background-color: #fb6200
}

.resp-sharing-button--vk {
    background-color: #507299
}

.resp-sharing-button--vk:hover {
    background-color: #43648c
}

.resp-sharing-button--facebook {
    background-color: #3b5998;
    border-color: #3b5998
}

.resp-sharing-button--facebook:active,
.resp-sharing-button--facebook:hover {
    background-color: #2d4373;
    border-color: #2d4373
}

.resp-sharing-button--twitter {
    background-color: #55acee;
    border-color: #55acee
}

.resp-sharing-button--twitter:active,
.resp-sharing-button--twitter:hover {
    background-color: #2795e9;
    border-color: #2795e9
}

.resp-sharing-button--google {
    background-color: #dd4b39;
    border-color: #dd4b39
}

.resp-sharing-button--google:active,
.resp-sharing-button--google:hover {
    background-color: #c23321;
    border-color: #c23321
}

.resp-sharing-button--tumblr {
    background-color: #35465c;
    border-color: #35465c
}

.resp-sharing-button--tumblr:active,
.resp-sharing-button--tumblr:hover {
    background-color: #222d3c;
    border-color: #222d3c
}

.resp-sharing-button--close {
    background: white;
    border-color: white;
    border-bottom-left-radius: 80px;
    padding-top: 0;
    padding-right: 5px;
    color: #35465c;
}


.resp-sharing-button--email {
    background-color: #777;
    border-color: #777
}

.resp-sharing-button--email:active,
.resp-sharing-button--email:hover {
    background-color: #5e5e5e;
    border-color: #5e5e5e
}

.resp-sharing-button--pinterest {
    background-color: #bd081c;
    border-color: #bd081c
}

.resp-sharing-button--pinterest:active,
.resp-sharing-button--pinterest:hover {
    background-color: #8c0615;
    border-color: #8c0615
}


/* Extra small devices (phones, 600px and down) */
@media only screen and (max-width: 600px) {}

/* Small devices (portrait tablets and large phones, 600px and up) */
@media only screen and (min-width: 600px) {}

/* Medium devices (landscape tablets, 768px and up) */
@media only screen and (min-width: 768px) {}

/* Large devices (laptops/desktops, 992px and up) */
@media only screen and (min-width: 992px) {}

/* Extra large devices (large laptops and desktops, 1200px and up) */
@media only screen and (min-width: 1200px) {}

/* @media (min-width: 320px) and (max-width: 1024px) {
    #video {
        max-width: 100%;
        width: 81%;
    }
}
@media (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) {
    #video {
        max-width: 100%;
        width: 87%;
    }
}
@media (min-width: 481px) and (max-width: 767px) {
    #video {
        max-width: 100%;
        width: 85%;
    }
}
@media (min-width: 320px) and (max-width: 480px) {
    #video {
        max-width: 100%;
        width: 87%;
    }
} */
</style>
@endsection
@section('content')

<div class="row">
    <div class="col-md-12 col-sm-12 text-center p-0"> 
        <canvas width="350" height="250" id='video'></canvas>
        <!-- <canvas width="500" height="751" id='video'></canvas> -->
        <div class="wrapper splashscreen">
           
            <!-- <div class="content-hv" id="loader">
                <div class="loader"></div>
                <img class="hv-con" src="{{ asset('XR/core/InstaTryLogo.png') }}" />
            </div> -->
            <div class="content-hv" id="camError">
                <h5>If your camera access is blocked. <br>Please Allow camera access to proceed further.</h5>
                <h3>This experience requires:</h3>
                Safari on iPhone (iOS 11.2+) <br>
                Chrome for Android 29+ <br><br>
                <b>You can try it on your mobile device by opening <br>its camera and scanning this QR-Code</b>
                </h3>
                <img class="hv-con" src="" id="camErrorImg" />
                <img class="hv-con" src="{{ asset('XR/core/InstaTryLogo.png') }}" style="top: 270px;" />
            </div>
        </div>
        <div class="wrapper" id="mywrapper">

            <img src="{{ asset('XR/core/capture.png') }}" class=" takephoto-btn" onclick="takePicture()">
            <img class="cph hv-con placeholder" src="{{ asset('XR/core/faceholder.png') }}">
            
        </div>
        <div class="nxtprebtn">
               <!-- <div class="text-center nxtImgDetails">
                    <h5 class="text-left m-2"><span id="product_name"></span></h5>
                    <span class="pull-left m-1 ml-2"><span id="product_price"></span></span><span class="pull-right m-1"><a href="#" target="__blank" class="btn btn-primary btn-sm" id="buynow">Buy Now</a></span>
               </div> -->
               <div class="card mb-0 nxtImgDetails">
                <div class="card-content">
                <div class="card-body p-1 text-left">
                    <span id="product_name" class="text-uppercase text-dark d-block text-truncate">No Title</span><br>
                    <span><span id="product_sku"></span></span>
                    <span><span id="product_price">₹ 00.00</span></span>
                    <span class="float-right"><a href="#" target="__blank" class="btn btn-primary btn-sm text-white text-uppercase" id="buynow">Buy Now</a></span>
                </div>
                </div>
                </div>
            <div id="nxtImg" class="nxtImg text-center">
            @foreach ($data as $key => $card)
                @php
                $product = $card->product;
                if(count($product->photo) > 0){
                    $img_url = $product->photo[0]->getUrl();
                    $objimg = asset('XR/assets/images/placeholder.png');
                    $obj_id = "";
                    } else if(count($product->arobject) > 0){
                        $img_url = $product->arobject[0]->object->getUrl();
                    }
                    if(count($product->arobject) > 0){
                        $objimg = $product->arobject[0]->object->getUrl();
                        $obj_id = $product->arobject[0]->id;
                    }
                $product_name= $product->name;
              
                $selected_product['img_url'] = $img_url;
                $selected_product['name'] = empty($product_name)?'No Title':$product_name;
                $product_json = array('id' => $product->id,
                'name' =>$product->name ?? 'No Title', 
                'img_url' => $img_url,
                'description' =>$product->description,
                'price' => '₹ '. ($product->price ?? '00.00'),
                'sku' =>$product->sku ?? '',
                'discount_price' =>$product->discount_price,
                'url' =>$product->url);
                
                $product_json = json_encode($product_json,true);
                @endphp
                @if(isset($_GET['sku']) && $_GET['sku'] == $product->id || $loop->index == 0)
                    <script>var product_json_selected = '<?php echo $product_json;?>';</script>
                @endif
                <img onclick="selImg(this,'NECKLACE','{{$product_json ?? ''}}')" id="sku_{{$product->id}}" src="{{$img_url ?? ''}}">
            @endforeach
                    
            </div>
        </div>

    </div>
</div>


<div class="modal animated bounceIn" id="instatry_preview" data-easein="bounceIn" tabindex="-1" role="dialog"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content">
            <div class="modal-body text-center nopadding">
                <div class="share-header-close">
                    <a class="share-close" id="share-close1" href="#" rel="noopener"
                        data-dismiss="modal" aria-label="">
                        <div class="resp-sharing-button resp-sharing-button--close resp-sharing-button--small">
                            <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                                X
                            </div>
                        </div>
                    </a>
                </div>
                <img id="preimg" src="">
                <button class="btn resp-sharing-button--tumblr btn-lg btn-share text-white" onclick="share_modal()">Share</button>
            </div>
            
        </div>
        
    </div>
</div>

<div class="modal nopadding share-overlay" data-backdrop="static" id="share-overlay">
    <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
        <div class="modal-content share-popup">
            <div class="modal-body text-center nopadding">
                <a class="resp-sharing-button__link share-fb" href="" target="_blank" rel="noopener" aria-label="">
                    <div class="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--small">
                        <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                <path
                                    d="M18.77 7.46H14.5v-1.9c0-.9.6-1.1 1-1.1h3V.5h-4.33C10.24.5 9.5 3.44 9.5 5.32v2.15h-3v4h3v12h5v-12h3.85l.42-4z" />
                            </svg>
                            Share on Facebook
                        </div>
                    </div>
                </a>
                <br>
                <a class="resp-sharing-button__link share-whatsapp" href="" target="_blank" rel="noopener"
                    aria-label="">
                    <div class="resp-sharing-button resp-sharing-button--whatsapp resp-sharing-button--small">
                        <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                <path style=" stroke:none;fill-rule:nonzero;fill:rgb(100%,100%,100%);fill-opacity:1;"
                                    d="M 12.039062 0 C 5.484375 0 0.160156 5.324219 0.160156 11.839844 C 0.160156 13.90625 0.714844 15.894531 1.75 17.722656 L 0 24 L 6.398438 22.292969 C 8.265625 23.285156 10.210938 23.839844 12.039062 23.839844 C 18.515625 23.839844 24 18.359375 24 11.839844 C 24 5.324219 18.636719 0 12.039062 0 Z M 12.039062 23.046875 C 10.292969 23.046875 8.425781 22.488281 6.636719 21.496094 L 6.476562 21.417969 L 1.113281 22.886719 L 2.582031 17.601562 L 2.503906 17.445312 C 1.511719 15.734375 0.953125 13.789062 0.953125 11.839844 C 0.953125 5.761719 5.921875 0.792969 12.039062 0.792969 C 18.199219 0.792969 23.207031 5.761719 23.207031 11.839844 C 23.207031 17.921875 18.078125 23.046875 12.039062 23.046875 Z M 12.039062 23.046875 " />
                                <path style=" stroke:none;fill-rule:nonzero;fill:rgb(100%,100%,100%);fill-opacity:1;"
                                    d="M 12.039062 1.988281 C 6.597656 1.988281 2.144531 6.398438 2.144531 11.839844 C 2.144531 13.789062 2.703125 15.617188 3.695312 17.246094 L 3.855469 17.445312 L 2.78125 21.21875 L 6.675781 20.226562 L 6.875 20.34375 C 8.503906 21.335938 10.292969 21.855469 12.039062 21.855469 C 17.523438 21.855469 22.011719 17.363281 22.011719 11.839844 C 22.011719 6.398438 17.523438 1.988281 12.039062 1.988281 Z M 12 21.097656 C 10.410156 21.097656 8.78125 20.582031 7.273438 19.667969 L 6.792969 19.351562 L 3.894531 20.066406 L 4.6875 17.285156 L 4.371094 16.769531 C 3.417969 15.296875 2.941406 13.589844 2.941406 11.839844 C 2.941406 6.875 7.03125 2.78125 12.039062 2.78125 C 17.085938 2.78125 21.21875 6.875 21.21875 11.839844 C 21.21875 16.925781 17.085938 21.058594 12 21.097656 Z M 12 21.097656 " />
                                <path style=" stroke:none;fill-rule:nonzero;fill:rgb(100%,100%,100%);fill-opacity:1;"
                                    d="M 18 14.027344 L 15.496094 12.875 C 15.140625 12.714844 14.78125 12.835938 14.542969 13.113281 L 13.589844 14.34375 C 12.875 14.066406 10.886719 13.191406 9.734375 11.046875 L 10.609375 10.011719 C 10.808594 9.773438 10.847656 9.457031 10.730469 9.179688 L 9.65625 6.636719 C 9.535156 6.359375 9.296875 6.160156 8.980469 6.160156 L 8.226562 6.121094 C 7.90625 6.121094 7.550781 6.199219 7.3125 6.4375 C 6.953125 6.753906 6.277344 7.429688 6.078125 8.34375 C 5.800781 9.65625 6.238281 11.207031 7.351562 12.714844 C 8.027344 13.667969 10.09375 16.488281 14.066406 17.601562 C 14.464844 17.722656 14.859375 17.761719 15.21875 17.761719 C 15.894531 17.761719 16.53125 17.5625 17.085938 17.246094 C 17.722656 16.847656 18.160156 16.171875 18.316406 15.457031 L 18.4375 14.902344 C 18.515625 14.542969 18.316406 14.183594 18 14.027344 Z M 17.484375 15.257812 C 17.363281 15.773438 17.046875 16.25 16.609375 16.53125 C 15.933594 16.96875 15.140625 17.046875 14.265625 16.808594 C 10.570312 15.773438 8.621094 13.113281 7.988281 12.238281 C 7.03125 10.925781 6.636719 9.574219 6.875 8.503906 C 7.03125 7.789062 7.550781 7.269531 7.867188 6.992188 C 7.945312 6.914062 8.066406 6.875 8.183594 6.875 L 8.980469 6.953125 L 10.054688 9.457031 L 9.097656 10.53125 C 8.941406 10.726562 8.902344 11.046875 9.019531 11.285156 C 10.332031 13.828125 12.714844 14.820312 13.429688 15.058594 C 13.707031 15.140625 13.988281 15.058594 14.183594 14.820312 L 15.140625 13.550781 L 17.640625 14.703125 Z M 17.484375 15.257812 " />
                            </svg>
                            Share on Whatsapp
                        </div>
                    </div>
                </a>
                <br>
                <!-- Sharingbutton Twitter -->
                <a class="resp-sharing-button__link share-twitter" href="" target="_blank" rel="noopener" aria-label="">
                    <div class="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--small">
                        <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                <path
                                    d="M23.44 4.83c-.8.37-1.5.38-2.22.02.93-.56.98-.96 1.32-2.02-.88.52-1.86.9-2.9 1.1-.82-.88-2-1.43-3.3-1.43-2.5 0-4.55 2.04-4.55 4.54 0 .36.03.7.1 1.04-3.77-.2-7.12-2-9.36-4.75-.4.67-.6 1.45-.6 2.3 0 1.56.8 2.95 2 3.77-.74-.03-1.44-.23-2.05-.57v.06c0 2.2 1.56 4.03 3.64 4.44-.67.2-1.37.2-2.06.08.58 1.8 2.26 3.12 4.25 3.16C5.78 18.1 3.37 18.74 1 18.46c2 1.3 4.4 2.04 6.97 2.04 8.35 0 12.92-6.92 12.92-12.93 0-.2 0-.4-.02-.6.9-.63 1.96-1.22 2.56-2.14z" />
                            </svg>
                            Share on Twitter
                        </div>
                    </div>
                </a>
                <br>
                <!-- Sharingbutton Pinterest -->
                <a class="resp-sharing-button__link share-pinterest" href="" target="_blank" rel="noopener"
                    aria-label="">
                    <div class="resp-sharing-button resp-sharing-button--pinterest resp-sharing-button--small">
                        <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                                <path
                                    d="M12.14.5C5.86.5 2.7 5 2.7 8.75c0 2.27.86 4.3 2.7 5.05.3.12.57 0 .66-.33l.27-1.06c.1-.32.06-.44-.2-.73-.52-.62-.86-1.44-.86-2.6 0-3.33 2.5-6.32 6.5-6.32 3.55 0 5.5 2.17 5.5 5.07 0 3.8-1.7 7.02-4.2 7.02-1.37 0-2.4-1.14-2.07-2.54.4-1.68 1.16-3.48 1.16-4.7 0-1.07-.58-1.98-1.78-1.98-1.4 0-2.55 1.47-2.55 3.42 0 1.25.43 2.1.43 2.1l-1.7 7.2c-.5 2.13-.08 4.75-.04 5 .02.17.22.2.3.1.14-.18 1.82-2.26 2.4-4.33.16-.58.93-3.63.93-3.63.45.88 1.8 1.65 3.22 1.65 4.25 0 7.13-3.87 7.13-9.05C20.5 4.15 17.18.5 12.14.5z" />
                            </svg>
                            Share on Pinterest
                        </div>
                    </div>
                </a>
                <br>
                <a class="resp-sharing-button__link" href="#" onclick="savePicture()"
                    rel="noopener" aria-label="">
                    <div class="resp-sharing-button resp-sharing-button--tumblr resp-sharing-button--small">
                        <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                            Download Picture
                        </div>
                    </div>
                </a>
                <br>
                <a class="resp-sharing-button__link" href="#" onclick="share_modal()"
                    rel="noopener" aria-label="">
                    <div class="resp-sharing-button resp-sharing-button--tumblr resp-sharing-button--small">
                        <div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                            Close
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
</div>
</div>


@endsection
@section('scripts')
@parent
<script type="text/javascript" src="{{ asset('XR/core/teZFDwxrt7dEU8ges6_release.js') }}"></script>
<script type="text/javascript" src="{{ asset('XR/core/three.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('XR/core/dFRWvphekvxePGBa_release.js') }}"></script>
<script type="text/javascript" src="{{ asset('XR/core/main_release.js') }}"></script>
<script type="text/javascript" src="{{ asset('XR/core_c_js/instatry_preview.js') }}"></script>
@endsection
