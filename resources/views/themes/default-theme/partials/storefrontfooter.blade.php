    @php
        $content_line = false;

        if($userSettings->is_start == 1) {
            $shareableLink = $userSettings->public_link;
        }else{
            $shareableLink = $userSettings->preview_link;
        }


        if(!empty($meta_data['payment_settings']['payment_method']))
            $payment_settings = $meta_data['payment_settings'];
        else
            $payment_settings = null;

        if(!empty($meta_data['footer_settings']))
            $footer_settings = $meta_data['footer_settings'];

    @endphp
    <footer class="footer footer-static footer-light text-center bg-foot">
        <p class="clearfix blue-grey lighten-2">
        @if((isset($footer_settings['facebook_id']) && $footer_settings['facebook_id']!='#')||( isset($footer_settings['whatsapp_num']) && $footer_settings['whatsapp_num']!='#' )||( isset($footer_settings['twitter_id']) && $footer_settings['twitter_id']!='#' )||( isset($footer_settings['google_link']) && $footer_settings['google_link']!='#' )||( isset($footer_settings['youtube_link']) && $footer_settings['youtube_link']!='#' ))
            <span class="float-md-right d-md-block"><strong>Follows Us</strong>
            @endif
            @if( isset($footer_settings['facebook_id']) && $footer_settings['facebook_id']!='#' )
                <a href="https://www.facebook.com/{{$footer_settings['facebook_id']}}" target="_blank" rel="noopener noreferrer"><i class="fa fa-facebook fa-fw"></i></a>
            @endif

            {{--@if( isset($footer_settings['whatsapp_num']) && $footer_settings['whatsapp_num']!='#' )
                <a href="https://wa.me/{{$footer_settings['whatsapp_num']}}" target="_blank" rel="noopener noreferrer"><i class="fa fa-whatsapp fa-fw"></i></a>
            @endif--}}

            @if( isset($footer_settings['twitter_id']) && $footer_settings['twitter_id']!='#' )
                <a href="https://twitter.com/{{$footer_settings['twitter_id']}}/" target="_blank" rel="noopener noreferrer"><i class="fa fa-twitter fa-fw"></i></a>
            @endif

            @if( isset($footer_settings['instagram_link']) && $footer_settings['instagram_link']!='#' )
                <a href="https://instagram.com/{{$footer_settings['instagram_link']}}/" target="_blank" rel="noopener noreferrer"><i class="fa fa-instagram fa-fw"></i></a>
            @endif

            @if( isset($footer_settings['pininterest_link']) && $footer_settings['pininterest_link']!='#' )
                <a href="http://pinterest.com/{{$footer_settings['pininterest_link']}}/" target="_blank" rel="noopener noreferrer"><i class="fa fa-pinterest fa-fw"></i></a>
            @endif

            {{--@if( isset($footer_settings['google_link']) && $footer_settings['google_link']!='#' )
                <a href="https://{{$footer_settings['google_link']}}" target="_blank" rel="noopener noreferrer"><i class="fa fa-google fa-fw"></i></a>
            @endif--}}

            @if( isset($footer_settings['youtube_link']) && $footer_settings['youtube_link']!='#' )
                <a href="{{$footer_settings['youtube_link']}}" target="_blank" rel="noopener noreferrer"><i class="fa fa-youtube fa-fw"></i></a>
            @endif

            </span>
        </p>
        @if(isset($payment_settings))
        
        @if( isset($payment_settings['payment_method']) &&  count($payment_settings['payment_method']) >= 1 )
            @if(count($payment_settings['payment_method']) ==1 && in_array('Enquiry',$payment_settings['payment_method'])) 
            @else
            <hr>
            <p class="clearfix blue-grey lighten-2">
                <h6><strong>Supported Payment Methods</strong></h6>
             @endif 
                @foreach($payment_settings['payment_method'] as $method)

                    @if($method == "Enquiry")
                        @continue
                    @endif
                    @if($method=="Razorpay")
                        <img src="{{ asset('XR\app-assets\images\icons\razorpay_logo.svg') }}" style="width:80px;">
                    @endif
                    @if($method=="Cash on delivery")
                        <img src="{{ asset('XR\app-assets\images\icons\cash_on_delivery.png') }}" style="width:25px;"> <span style='font-size:16px' class="text-dark">COD</span>
                    @endif
                    @if($method=="Paypal")
                        <img src="{{ asset('XR\app-assets\images\icons\paypal_logo.svg') }}" style="width:70px;">
                    @endif
                    @if($method=="UPI")
                        <img src="{{ asset('XR\app-assets\images\icons\upi.svg') }}" style="width:70px;">
                    @endif
                @endforeach
            </p>
        @endif
        @endif
        <p class="clearfix blue-grey lighten-2 mb-1">
            @if(isset($meta_data['content_settings']['show_about_us']) && $meta_data['content_settings']['show_about_us'] == 1)
                @php
                    $content_line = true;
                @endphp
                <hr>
                <span><a href="{{route('store.storeAboutUs', [$shareableLink])}}" target="_blank">About us</a></span>
            @endif
            @if(isset($meta_data['content_settings']['show_privacy_policy']) && $meta_data['content_settings']['show_privacy_policy'] == 1)
                @php
                    $content_line = true;
                @endphp
                <span><a href="{{route('store.storePrivacyPolicy', [$shareableLink])}}" target="_blank">Privacy Policy</a></span>
            @endif
            @if(isset($meta_data['content_settings']['show_terms_and_conditions']) && $meta_data['content_settings']['show_terms_and_conditions'] == 1)
                @php
                    $content_line = true;
                @endphp
                <span><a href="{{route('store.storeTermsAndConditions', [$shareableLink])}}" target="_blank">Term & Conditions</a></span>
            @endif
            @if(isset($meta_data['content_settings']['show_refund_and_return_policy']) && $meta_data['content_settings']['show_refund_and_return_policy'] == 1)
                @php
                    $content_line = true;
                @endphp
                <span><a href="{{route('store.storeRefundAndReturnPolicy', [$shareableLink])}}" target="_blank">Refund & Return Policy</a></span>
            @endif
            @if(isset($meta_data['content_settings']['show_contact_us']) && $meta_data['content_settings']['show_contact_us'] == 1)
                @php
                    $content_line = true;
                @endphp
                <span><a href="{{route('store.storeContactUs', [$shareableLink])}}" target="_blank">Contact us</a></span>
            @endif
            @if(isset($meta_data['content_settings']['show_shipping_policy']) && $meta_data['content_settings']['show_shipping_policy'] == 1)
                @php
                    $content_line = true;
                @endphp
                <span><a href="{{route('store.storeShippingPolicy', [$shareableLink])}}" target="_blank">Shipping Policy</a></span>
            @endif
        </p>
        @if( isset($footer_settings['address']) && $footer_settings['address']==TRUE )
        <hr>
        <h6><strong>Contact Us</strong></h6>
        <p class="clearfix blue-grey lighten-2 mb-lg-0 mb-4">
        @if($footer_settings['address1'])
        {{$footer_settings['address1'] .', '}}
        @endif
        @if($footer_settings['address2'])
        {{$footer_settings['address2'] .', '}}
        @endif
        @if($footer_settings['city'])
        {{$footer_settings['city'] .', '}}
        @endif
        @if($footer_settings['state'])
        {{$footer_settings['state'] .', '}}
        @endif
        @if($footer_settings['country'])
        {{$footer_settings['country'] .', '}}
        @endif
        @if($footer_settings['pincode'])
        {{$footer_settings['pincode'] }}
        @endif
        </p>
        @endif
        <hr>
        @foreach($payment_settings['payment_method'] as $method)
        @if($method=="Razorpay")
                        <img src="{{ asset('XR\app-assets\images\icons\razorpay.png') }}" style="width:300px;">
         @endif
        @endforeach
        <p class="clearfix blue-grey lighten-2 mb-lg-0 mb-4">
            <span class="d-block d-md-inline-block mt-25">COPYRIGHT &copy; {{ date('Y') }}
                <a class="text-bold-800 grey text-green darken-2" href="{{$homeLink}}">
                    {{ $meta_data['storefront']['storename'] ?? "Store" }},
                </a>
                All rights Reserved. </br></br>
                This e-commerce store is created using<a class="text-bold-800 grey darken-2" href="{{env('APP_URL')}}" target="_blank"><b><span class="text-blue">Simpli</span><span class="text-red">Sell</span></b></a>Platform.
            </span>
        
            <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="feather icon-arrow-up"></i></button>
        </p>    
        @include('themes.'.$meta_data['themes']['name'].'.partials.public_bottommenu')
    </footer>
