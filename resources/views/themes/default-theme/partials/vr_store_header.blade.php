<style>
    .hm-gradient {
    background-image: linear-gradient(to top, #f3e7e9 0%, #e3eeff 99%, #e3eeff 100%);
}
.navbar .dropdown-menu a:hover {
    color: #00000 !important;
}
.darken-grey-text {
    color: #2E2E2E;
}
.navbar {
    -webkit-box-shadow: 0 8px 6px -6px #999;
    -moz-box-shadow: 0 8px 6px -6px #999;
    box-shadow: 0 8px 6px -6px #999;
    /* the rest of your styling */
}

@media (min-width: 576px){
    .play{
        display:none !important;
    }
    
}
.badgeup{
    top:0.5rem;
    position:absolute;
}
</style>

<div class="me-3" style="position: absolute; top: 10px; right: 10px;">
  <div class="" style="background: none; display: flex;">
    <div id="cart-btn" style="cursor: pointer; text-decoration: none;" onclick="toggleCart()" >
        <img class="ms-4" src="{{ asset('themes/default-theme/cart.svg') }}" height="30" />
        <span class="badge badge-pill badge-danger badgeup cart-item-count" style="position: absolute; top: -5px; right: -5px;"></span>
    </div>
    <img class="ms-2 d-none" src="{{ asset('themes/default-theme/profile.svg') }}" style="cursor: pointer; margin-left: 10px" height="30" />
  </div>
</div>

<script>
    var toggleCart = () => {
        $("#mirrar-vr-cart").toggle();
        let iframe = $("#mirrar-vr-cart-iframe")[0];
        iframe.contentWindow.location.reload();
    }
</script>