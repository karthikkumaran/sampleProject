<style>
    .hm-gradient {
    background-image: linear-gradient(to top, #f3e7e9 0%, #e3eeff 99%, #e3eeff 100%);
}
.navbar .dropdown-menu a:hover {
    color: #00000 !important;
}
.darken-grey-text {
    color: #2E2E2E;
}
.navbar {
    -webkit-box-shadow: 0 8px 6px -6px #999;
    -moz-box-shadow: 0 8px 6px -6px #999;
    box-shadow: 0 8px 6px -6px #999;
    /* the rest of your styling */
}

@media (min-width: 576px){
    .play{
        display:none !important;
    }
    
}
.badgeup{
    top:0.5rem;
    position:absolute;
}
</style>
@php

if($userSettings->is_start == 1) {
    $shoppableLink =  route('mystore.publishedVideo',$userSettings->public_link);
    if(!empty($userSettings->customdomain)){
        $homeLink = '//'.$userSettings->customdomain;
        $sLink=$userSettings->customdomain;

    }else if(!empty($userSettings->subdomain)) {
        $homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
        $sLink=$userSettings->subdomain;

    }else{
        $homeLink = route('mystore.published',[$userSettings->public_link]);
        $sLink=$userSettings->public_link;
    }
    }else{
        $homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
        $shoppableLink =  route('mystore.publishedVideo',$userSettings->preview_link);
        $sLink=$userSettings->preview_link;
    }
    $sharableLink=$userSettings->public_link;

@endphp
@if(isset($meta_data['announcement_settings']) && !empty($meta_data['announcement_settings']['announcement_text']))
    <nav class="navbar sticky-top navbar-expand-lg navbar-light cyan shadow-lg p-0">
        <div class="w-100" style="background: {{$meta_data['announcement_settings']['announcement_background_color']}}">
            <marquee style="color: {{$meta_data['announcement_settings']['announcement_text_color']}}; margin-top: 5px;">{!! $meta_data['announcement_settings']['announcement_text'] !!}</marquee>
        </div>
    </nav>
@endif
<nav class="mb-0 navbar sticky-top navbar-expand-lg navbar-light cyan shadow-lg bg-green">
            
    @if(!empty($meta_data['storefront']))
        @if(!empty($userSettings->getFirstMediaUrl('storelogo')))
            @if($meta_data['storefront']['logo_type'] == 1)
                <a class="nav-brand d-flex align-items-center w-100" href="{{$homeLink}}">
                    <!-- <img class=" lazyload d-block" loading="lazy" data-src="{{ $userSettings->getFirstMediaUrl('storelogo')  }}"
                        onerror="this.onerror=null;this.src='{{asset('XR/assets/images/blank.gif')}}';" alt="Logo" src="{{asset('XR/assets/images/blank.gif')}}" style="max-width: 70px;max-height: 37px;margin-right:5px;"> -->
                        <img src="{{asset('XR/assets/images/blank.gif')}}" style="background: url({{ $userSettings->getFirstMediaUrl('storelogo') }}) no-repeat left; max-width: 300px; width: 100%; height: 100%; background-size: contain; height: 70px;">
                    </a>
            @elseif($meta_data['storefront']['logo_type'] == 2)
            <a class="nav-brand d-flex align-items-center" href="{{$homeLink}}"><span class="text-green" style="font-size:130%;"><b>{{$meta_data['storefront']['storename']}}</b></span></a>            
            @else
            <a class="nav-brand d-flex align-items-center" href="{{$homeLink}}">
                        <img src="{{asset('XR/assets/images/blank.gif')}}" style="background: url({{ $userSettings->getFirstMediaUrl('storelogo') }}) no-repeat left; max-width: 300px; width: 100%; height: 100%; background-size: contain; height: 70px;">
                        <span class="text-green" style="font-size:130%;"><b>{{$meta_data['storefront']['storename']}}</b></span>
            </a>
            @endif
        @else
            <a class="nav-brand p-1" href="{{$homeLink}}"><span class="text-green" style="font-size:130%"><b>{{$meta_data['storefront']['storename']}}</b></span>
            </a>
        @endif
        
            <!-- <a href="{{$shoppableLink}}" class="play"><img src="{{asset('XR\app-assets\images\icons\video-shopping.png')}}" style="width:50px;height:50px;"/><br>
                    <b>Video<br>Shopping</b></i></a> -->
           
    @endif 
    <button class="navbar-toggler d-none" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    
    <div class="collapse navbar-collapse w-100" id="navbarSupportedContent-4">
    <!-- <a class="nav-brand p-1 text-center" href="{{$homeLink}}"><span class="text-green" style="font-size:130%"><b>shoppable videos</b></span> -->
        <ul class="navbar-nav ml-auto">

        @if(isset($meta_data['settings']['storefront_shop_tv']) && $meta_data['settings']['storefront_shop_tv'] == 1)         
            <li class=" nav-item mr-2">
                <a class="nav-link nav-link-label p-0" href="{{route('mystore.publishedVideo',$sharableLink)}}">
                    <span class=" text-center  ml-2"><img src="{{asset('XR\app-assets\images\icons\video-shopping.png')}}"  style="width:30px;height:30px;margin-top:10px;"/></span><br>
                    <h5 class="margin-top:10px;">Shop TV</h5>
                    <!-- <i class=" fa fa-youtube-play fa-3x " style="margin-top:0.5rem; margin-right:1rem;"></i> -->
                </a>

            </li>
        @endif
            
        @if(!empty($checkoutLink) && !request()->is('register/*') && !request()->is('login/*'))
            
            
            <li class=" nav-item mr-1">
                <a class="nav-link nav-link-label mt-1 p-0" onclick="track_events('cart-button-clicked',null)" href="{{$checkoutLink}}">
                    <i class="pt-1 ficon feather icon-shopping-cart fa-2x" ></i>
                    <span class="badge badge-pill badge-danger badgeup cart-item-count" style="margin-top: 5px;margin-right: 10px;"></span>
                </a>

            </li>
            {{--  <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i class="ficon feather icon-maximize"></i></a></li> --}}
            @endif 
            @if(!Auth::guard('customer')->check())
            <li class="nav-item">
                <a href="javascript:void(0);" class="nav-link btn p-1 btn-red text-white rounded-pill registerButton" onclick="track_events('signup-open-button-clicked',null)" style="margin-block: 10px;margin-inline: 8px;border-radius: 20px !important;">
                    <b>Sign Up</b>
                </a> 
            </li> 
                <li class="nav-item">
                <a href="javascript:void(0);" class="nav-link btn p-1 btn-primary text-white rounded-pill loginButton" onclick="track_events('login-open-button-clicked',null)"  style="margin-block: 10px;margin-inline: 8px;border-radius: 20px !important;">
                    <b>Login</b>
                </a> 
            </li>
            @endif
            @if(!empty($meta_data['settings']['whatsapp']) && !request()->is('register/*') && !request()->is('login/*'))
            <li class="nav-item">
                <a class="nav-link btn btn-outline-dark btn-white list-view-btn view-btn" onclick="track_events('whatsapp-enquire-button-clicked',null)"  href="https://api.whatsapp.com/send?phone={{isset($meta_data['settings']['whatsapp_country_code']) ? $meta_data['settings']['whatsapp_country_code'].$meta_data['settings']['whatsapp'] : '91'.$meta_data['settings']['whatsapp']}}&text={{$meta_data['settings']['enquiry_text'] ?? ''}}" target="_blank" style="border-radius: 24px;padding: 10px 20px;margin: 10px;">
                    <img src="{{asset('/XR/app-assets/images/icons/whatsapp.svg')}}" style="width: 24px;"><b>&nbsp;Enquire</b>
                </a>
            </li>
            @endif
            @if(Auth::guard('customer')->check())
            <li class="nav-item dropdown dropdown-user p-1">
                <a class="nav-link dropdown-toggle dropdown-user-link" id="dropdown-user" href="javascript:void(0);" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user-circle fa-2x m-0"></i> 
                        <div class="text-wrap badge badge-white text-muted p-0 text-left m-0" style="width:min-content;">
                            {{Auth::guard('customer')->user()->name}}
                        </div>
                        {{--<span class="h5 text-wrap">{{Auth::guard('customer')->user()->name}}</span> --}}
                </a>
                <div class="dropdown-menu dropdown-menu-right m-0" aria-labelledby="dropdown-user">
                    <a class="dropdown-item" href="{{route('customer.profile',$sharableLink)}}"> <i class=" fa fa-user"></i> My Profile</a>
                    <a class="dropdown-item" href="{{route('customer.orders',$sharableLink)}}"> <i class=" fa fa-list-ul"></i> My Orders</a>
                    <div class=" dropdown-divider"></div>
                    <a class="dropdown-item m-0" href="{{ route('customer.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" style="margin-block: 10px;margin-inline: 8px;">
                    <i class="fa fa-sign-out"></i> Logout</a>
                    <form id="logout-form" action="{{ route('customer.logout') }}" method="POST" style="display: none;">
                    @csrf
                    </form>
                </div>
            </li>
            @endif
        </ul>
    </div>
</nav>
@section('scripts')
@parent
<script>
</script>
@endsection

