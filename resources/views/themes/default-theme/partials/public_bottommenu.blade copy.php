<meta name="viewport" content="width=device-width, initial-scale=1">
<div class="navbar w-100 d-lg-none d-md-inline-flex d-inline-flex border-top-secondary" style="position:fixed;bottom:0;height:50px;z-index:10;background:#F1F1F1;left: 0;">
    <div class="w-25 h-100 d-flex justify-content-center align-items-center"><a  href="{{$homeLink}}"><i class="feather icon-home" style="color:#111;font-size:160%;"></i></a></div>
    @if(!empty($checkoutLink) && !request()->is('register/*') && !request()->is('login/*'))
    <div class="w-25 h-100 d-flex justify-content-center align-items-center">
       <a class="nav-link nav-link-label" href="{{$checkoutLink}}">
            <i class="ficon feather icon-shopping-cart" style="color:#111;font-size:160%;"></i>
            <span class="badge badge-pill badge-danger cart-item-count" style="bottom:12px;right:10px;position:relative;"></span></a>
    </div>
    @endif
    <div class="w-25 h-100 d-flex justify-content-center align-items-center">
        <a href="https://api.whatsapp.com/send?phone={{isset($meta_data['settings']['whatsapp_country_code']) ? $meta_data['settings']['whatsapp_country_code'].$meta_data['settings']['whatsapp'] : '91'.$meta_data['settings']['whatsapp']}}&text={{$meta_data['settings']['enquiry_text'] ?? ''}}" target="_blank" onclick='share("whatsapp","")'>
            <i class="fa fa-whatsapp fa-fw" style="color:#111;font-size:160%;"></i>
        </a>
    </div>
    <div class="w-25 h-100 d-flex justify-content-center align-items-center">
        <a href="#" data-title="New Customer?" data-placement="top" data-html="true" data-toggle="popover" data-trigger="focus" data-content='<div><a href="javascript:void(0);" class="register btn text-dark" data-dismiss="alert" id="registerButton">Sign Up</a><br/><a href="javascript:void(0);" data-dismiss="alert" class="login btn w-100 text-dark" id="loginButton">Login</a></div>'>
            <i class="feather icon-more-vertical" style="color:#111;font-size:160%;"></i>
        </a>
    </div>
</div>
<!-- <script src="js/jquery-3.5.1.min.js"></script> -->
<!-- <script src="js/popper.min.js"></script> -->
<!-- <script src="js/bootstrap.min.js"></script> -->
<!-- <script src="{{asset('XR/app-assets/vendors/js/ui/popper.min.js')}}"></script> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="popover"]').popover();   
    $(document).on("click", ".register .login" , function(){
        $('[data-toggle="popover"]').popover('hide');
    });
});
</script>