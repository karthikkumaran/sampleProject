
@php
    $theme_name = $meta_data["themes"]["name"] ?? env('DEFAULT_STOREFRONT_THEME');
    $default_color_path = "../resources/views/themes/" . $theme_name . "/partials/default_color.php";
    if(file_exists($default_color_path))
        include($default_color_path);
@endphp
<style>
    .badge-primary,
    .btn-primary,
    .bg-primary,
    .app-content .wizard>.steps>ul>li.done:last-child .step,
    .cart,
    .vs-radio-primary input:checked~.vs-radio .vs-radio--circle,
    .vs-checkbox-primary input:checked~.vs-checkbox .vs-checkbox--check,
    .noUi-connect,
    .ecommerce-application .sidebar-shop .range-slider.noUi-horizontal .noUi-handle .noUi-tooltip,
    .app-content .wizard.wizard-circle>.steps>ul>li:before,
    .app-content .wizard.wizard-circle>.steps>ul>li:after,
    .app-content .wizard>.steps>ul>li.current .step {
        background-color: {!! isset($meta_data['themes']['theme_background_color']) ? $meta_data['themes']['theme_background_color'] : $background_default_color !!} !important;
    }

    .bg-red,
    .btn-red {
        background: #DB1F26;
    }

    .bootstrap-touchspin-up,
    .bootstrap-touchspin-down {
        border: 2px solid {!! isset($meta_data['themes']['theme_background_color']) ? $meta_data['themes']['theme_background_color'] : $background_default_color !!} !important;
        color: #000000 !important;
    }

    .bootstrap-touchspin-down {
        background-color: #FDECEC !important;
    }

    .bootstrap-touchspin-up {
        background-color: #E1F1EF !important;
    }

    .quantity-counter {
        background-color: #F1F1F1 !important;
    }

    .banner_box_zero{
    box-shadow: 0 4px 25px 0 rgb(0 0 0 / 0%) !important;
    border-radius: 0rem;
    }

    .vs-radio-primary,
    .vs-checkbox-primary,
    .text-green,
    .app-content .wizard>.steps>ul>li.current>a {
        color: {!! isset($meta_data['themes']['theme_background_color']) ? $meta_data['themes']['theme_background_color'] : $background_default_color !!} !important;
    }
    .bg-green{
        background-color:{!! isset( $meta_data['themes']['theme_text_color']) ?  $meta_data['themes']['theme_text_color'] : $text_default_color !!} !important;
    }
    .bg-mobNav{
        background-color:{!! isset( $meta_data['themes']['theme_mobNav_color']) ?  $meta_data['themes']['theme_mobNav_color'] : $bg_mobNav_color !!} !important;
    }
    .bg-foot{
        background-color:{!! isset( $meta_data['themes']['theme_footer_color']) ?  $meta_data['themes']['theme_footer_color'] : $bg_footer_color !!} !important;
    }
    .bg-body{
        background-color:{!! isset($meta_data['themes']['theme_body_color']) ?  $meta_data['themes']['theme_body_color'] : $bg_body_color !!} !important;
    }
    .vs-checkbox-primary input:checked~.vs-checkbox,
    .noUi-handle,
    .border-primary,
    .app-content .wizard>.steps>ul>li.current .step,
    .app-content .wizard>.steps>ul>li.done .step {
        border-color: {!! isset($meta_data['themes']['theme_background_color']) ? $meta_data['themes']['theme_background_color'] : $background_default_color !!} !important;
    }
</style>
