@php
    $theme_name = $meta_data["themes"]["name"] ?? env('DEFAULT_STOREFRONT_THEME');
    $default_color_path = "../resources/views/themes/" . $theme_name . "/partials/default_color.php";
    if(file_exists($default_color_path))
        include($default_color_path);
@endphp
@php
if($userSettings->is_start == 1) {
    $shoppableLink =  route('mystore.publishedVideo',$userSettings->public_link);
    if(!empty($userSettings->customdomain)){
        $homeLink = '//'.$userSettings->customdomain;

    }else if(!empty($userSettings->subdomain)) {
        $homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');

    }else{
        $homeLink = route('mystore.published',[$userSettings->public_link]);
    }
    }else{
        $homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
        $shoppableLink =  route('mystore.publishedVideo',$userSettings->preview_link);
    }

    $sLink = $userSettings->public_link;

@endphp
<style>
.dropdown-backdrop{
    position: static;
}
.bottom_navbar {
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 55px;
    box-shadow: 0 0 3px rgba(0, 0, 0, 0.2);
    background-color: #ffffff;
    display: flex;
    overflow-x: auto;
    z-index: 99999;
}

.bottom_nav__link {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    flex-grow: 1;
    min-width: 50px;
    overflow: hidden;
    white-space: nowrap;
    font-family: sans-serif;
    font-size: 13px;
    color: #444444;
    text-decoration: none;
    -webkit-tap-highlight-color: transparent;
    transition: background-color 0.1s ease-in-out;
}

.bottom_nav__link:hover {
    background-color: {!! isset($meta_data['themes']['theme_background_color']) ? $meta_data['themes']['theme_background_color'] : $background_default_color !!} !important;
    color: #ffffff !important;
}

.bottom_nav__link--active {
    color:{!! isset($meta_data['themes']['theme_background_color']) ? $meta_data['themes']['theme_background_color'] : $background_default_color !!} !important;
}

.bottom_nav__icon {
    font-size: 18px;
    margin-bottom: 2px;
    margin-top: 2px;
}
</style>
<meta name="viewport" content="width=device-width, initial-scale=1">
<nav class="bottom_navbar d-lg-none bg-mobNav">
  <a href="{{$homeLink}}" class="bottom_nav__link {{ request()->is('/') || request()->is('s/*') || request()->is('p/*') || request()->is('d/*') ? 'bottom_nav__link--active' : '' }}">
    <i class="feather icon-home bottom_nav__icon"></i>
    <span class="nav__text">Home</span>
  </a>
        @if(isset($meta_data['settings']['storefront_shop_tv']) && $meta_data['settings']['storefront_shop_tv'] == 1) 
   
            <a href="{{route('mystore.publishedVideo',$sLink)}}" class="bottom_nav__link {{  request()->is('sd/*')  ? 'bottom_nav__link--active' : '' }}"><img src="{{asset('XR\app-assets\images\icons\video-shopping.png')}}" style="width:25px;height:25px;margin-top:3px;"/>
                    <span class="nav__text">Shop TV</span></a>
        @endif  
  @if(!empty($checkoutLink) && !request()->is('register/*') && !request()->is('login/*'))
  <a href="{{$checkoutLink}}" class="bottom_nav__link  {{ request()->is('c/*') ? 'bottom_nav__link--active' : '' }}">
    <i class="feather icon-shopping-cart bottom_nav__icon"></i>
    <span class="nav__text">Cart</span>
    <span class="badge badge-pill badge-danger cart-item-count position-absolute mb-2 ml-2"></span></a>

  </a>
  @endif
  @if(isset($meta_data['settings']['whatsapp_country_code']))
  <a href="https://api.whatsapp.com/send?phone={{isset($meta_data['settings']['whatsapp_country_code']) ? $meta_data['settings']['whatsapp_country_code'] .''. $meta_data['settings']['whatsapp'] : '91'.$meta_data['settings']['whatsapp']}}&text={{$meta_data['settings']['enquiry_text'] ?? ''}}" target="_blank" onclick='share("whatsapp","")' class="bottom_nav__link">
    <i class="fa fa-whatsapp fa-fw bottom_nav__icon"></i>
    <span class="nav__text">Whatsapp</span>
  </a>
  @endif
  
  @if(!Auth::guard('customer')->check())
  <a href="javascript:void(0);" class="bottom_nav__link loginButton">
  @else
    <a href="{{route('customer.profile',$sLink)}}" class="bottom_nav__link {{ request()->is('profile/*') ? 'bottom_nav__link--active' : '' }}">
  @endif
    <i class="feather icon-user bottom_nav__icon"></i>
    <span class="nav__text">Account</span>
  </a>
</nav>

