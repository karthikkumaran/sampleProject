@include('themes.'.$meta_data['themes']['name'].'.partials.colour_theme_style')
<style>
    .cart{
        padding: 0.8rem 1rem;
        cursor: pointer;
        font-weight: 600;
        font-size: 0.875rem;
        text-transform: uppercase;
        color: white;
    }
</style>
@php
$productcount = 0;
if(empty($meta_data['settings']['currency']))
        $currency = "₹";
else
    if($meta_data['settings']['currency_selection'] == "currency_symbol")
        $currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
    else
        $currency = $meta_data['settings']['currency'];
@endphp




        @foreach ($data as $key => $card)
                @php
                    $product = $card->product;
                    if($product->discount != null)
                    {
                        $discount_price = $product->price * ($product->discount/100);
                        $discounted_price = round(($product->price - $discount_price),2);
                    }
                    $obj_id = "";
                    $obj_3d = false;
                    if(count($product->photo) > 0){
                    $img = $product->photo[0]->getUrl('thumb');
                    $objimg = asset('XR/assets/images/placeholder.png');
                    $obj_id = "";
                    $obj_3d = false;
                    } else if(count($product->arobject) > 0){
                        if(!empty($product->arobject[0]->object))
                        if(!empty($product->arobject[0]->object->getUrl('thumb'))) {
                            $img = $product->arobject[0]->object->getUrl('thumb');
                        }
                    } else {
                    $img = asset('XR/assets/images/placeholder.png');
                    $objimg = asset('XR/assets/images/placeholder.png');
                    }

                    foreach ($product->arobject as $key => $arobject) {
                            if(!empty($arobject->object)){
                                if($arobject->ar_type == "face") {
                                    if(!empty($arobject->object->getUrl('thumb'))) {
                                        $objimg = $arobject->object->getUrl('thumb');
                                        $obj_id = $arobject->id;
                                    }
                                }
                            }
                            //echo $arobject->ar_type;
                            if($arobject->ar_type == "surface") {

                                if(!empty($arobject->object_3d)) {
                                    $obj_3d = true;
                                }
                                if(!empty($arobject->object_3d_ios)) {
                                    $obj_3d = true;
                                }
                            }
                    }

                    if($campaign->is_start == 1) {
                        if(count($product->categories) > 0)
                        {
                            $categoryLink = route('campaigns.publishedProductByCategory',[$product->categories[0]['id'],$campaign->public_link]);
                        }
                        $catalogId = $campaign->public_link;
                        $tryonLink = route('campaigns.publishedTryOn',$campaign->public_link)."?sku=".$product->id;
                        $viewARLink = route('campaigns.publishedViewAR',$campaign->public_link)."?sku=".$product->id;
                        $catalogDetailsLink = route('campaigns.publishedCatalogDetails',[$product->id,$campaign->public_link]);
                        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
                    }else{
                        if(count($product->categories) > 0)
                        {
                            $categoryLink = route('campaigns.publishedProductByCategory',[$product->categories[0]['id'],$campaign->preview_link]);
                        }
                        $catalogId = $campaign->preview_link;
                        $tryonLink = route('admin.campaigns.previewTryOn',$campaign->preview_link)."?sku=".$product->id;
                        $viewARLink = route('admin.campaigns.previewViewAR',$campaign->preview_link)."?sku=".$product->id;
                        $catalogDetailsLink = route('admin.campaigns.previewCatalogDetails',[$product->id,$campaign->preview_link]);
                        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
                    }
                @endphp
                <div class="col-lg-6 col-md-6 col-sm-12">
                    <div class="card ecommerce-card " >
                        @php
                            $productcount++;
                        @endphp
                        <div class="card-content">
                            @if($product->discount != null)
                                <span class="badge badge-primary badge-pill" style="position: absolute;top:145px;left:5px;"><strong>{{$product->discount}}% OFF</strong></span>
                            @endif
                            @if(count($product->categories) > 0)
                                <a href="{{$categoryLink}}">
                                    <span class="badge badge-pill badge-primary text-white text-truncate" style="position: absolute;top:145px;right:5px;max-width:40%">{{$product->categories[0]['name']}}
                                    </span>
                                </a>
                            @endif
                            @if($product->new != 0)
                                <img src="{{asset('XR/app-assets/images/icons/new.png')}}" style="position: absolute;z-index:10;width:70px;height:70px;"/>
                            @endif
                            @if($product->featured != 0)
                                <img src="{{asset('XR/app-assets/images/icons/featured1.png')}}" style="position: absolute;z-index: 10;right:0.001vw;width:70px;height:70px;"/>
                            @endif

                            <div class="text-center d-flex justify-content-center bg-white rounded-top">
                                {{-- @if(@isset($meta_data['themes']['product_details_display']) && $meta_data['themes']['product_details_display'] == "new_page")
                                <!-- <a href="{{$catalogDetailsLink}}"><img class="img-responsive mw-100 mh-100 rounded-top" src="{{$img}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';"  style="height:170px;"></a> -->
                                @else --}}
                                    <img class="img-responsive mw-100 mh-100 rounded-top" src="{{$img}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';"  style="height:170px;cursor:pointer;" onclick="getProductDetails({{$card->id}},{{$campaign->id}},{{$video_id}})" data-toggle="modal" data-target="#productDetailsModal">
                                {{-- @endif --}}
                                @if(!empty($obj_id))
                                    <a href="{{$tryonLink}}" data-id="{{$product->id}}">
                                        @if(@isset($meta_data['themes']['product_details_display']) && $meta_data['themes']['product_details_display'] == "new_page")
                                            <div class="position-absolute" style="right:15px;bottom:200px;">
                                        @else
                                            <div class="position-absolute" style="right:15px;bottom:165px;">
                                        @endif
                                            <div class="badge badge-glow badge-info">
                                                <img class="img-fluid" src="{{asset('XR/assets/images/ar.png')}}"
                                                    style="width: 32px; max-height:32px;">
                                                <h6 class="text-white m-0">Tryon</h6>
                                            </div>
                                        </div>
                                    </a>
                                @endif
                                @if($obj_3d == true)
                                    <a href="{{$viewARLink}}" data-id="{{$product->id}}">
                                        @if(@isset($meta_data['themes']['product_details_display']) && $meta_data['themes']['product_details_display'] == "new_page")
                                        <div class="position-absolute" style="left:15px;bottom:200px;">
                                        @else
                                        <div class="position-absolute" style="left:15px;bottom:165px;">
                                        @endif
                                            <div class="badge badge-glow badge-info">
                                                <img class="img-fluid" src="{{asset('XR/assets/images/ar.png')}}"
                                                    style="width: 32px; max-height:32px;">
                                                <h6 class="text-white m-0">3D/AR View</h6>
                                            </div>
                                        </div>
                                    </a>
                                @endif
                            </div>
                        </div>
                        {{-- @if(@isset($meta_data['themes']['product_details_display']) && $meta_data['themes']['product_details_display'] == "new_page") --}}
                        <!-- <div class="d-inline-flex w-100 h-100"> -->
                        {{-- @else --}}
                        <div class="d-inline-flex w-100 h-100" onclick="getProductDetails({{$card->id}})" data-toggle="modal" data-target="#productDetailsModal" style="cursor:pointer;">
                        {{-- @endif --}}

                            <div class="w-100 h-100 p-1">
                                <div>
                                    <h6 class="item-price">
                                        @if($product->discount != null)
                                            <span class="">{!!empty($product->price)?"":$currency!!}</span><span class="">{{$discounted_price}}{!!empty($product->units)?"":" / $product->quantity $product->units"!!}</span>
                                            &nbsp;<span class="text-muted"><s>{!!empty($product->price)?"":$currency!!}{{$product->price}}</s></span>
                                            <!-- <span class="text-danger">{{$product->discount}}% OFF</span> -->
                                        @else
                                            <span class="">{!!empty($product->price)?"":$currency!!}</span><span class="">{!! $product->price ?? '&nbsp;' !!}{!!empty($product->units)?"":" / $product->quantity $product->units"!!}</span>
                                        @endif
                                    </h6>
                                </div>
                                <div class="item-rating">
                                    @if(!empty($obj_id))
                                    <!-- <a href="{{route('campaigns.publishedTryOn',$campaign->public_link)}}?sku={{$product->id}}" target="__blank" data-id="{{$product->id}}"
                                        class="badge badge-pill badge-glow badge-success">Try On</a> -->
                                    @endif
                                </div>
                                <div class="item-name mt-0 text-truncate">
                                    <a href="{{$catalogDetailsLink}}">{!! $product->name ?? '&nbsp;' !!}</a>
                                    <!-- <p class="item-company">By <span class="company-name"></span></p> -->
                                </div>
                                @if(isset($meta_data['settings']['storefront_show_inventory']) && $meta_data['settings']['storefront_show_inventory'] == 1)
                                    @if($product->out_of_stock == null && $product->stock != null)
                                        <div class="text-truncate"><strong>Stock: {{$product->stock}}</strong></div>
                                    @else
                                        <br>
                                    @endif
                                @endif
                            </div>

                        </div>
                        <div class="item-options text-center">
                            {{-- @if(@isset($meta_data['themes']['product_details_display']) && $meta_data['themes']['product_details_display'] == "new_page") --}}
                            <!-- <div class="wishlist">
                                <i class="fa fa-file-text"></i>
                                <a href="{{$catalogDetailsLink}}" style="color:black;">Details</a>
                            </div>  -->
                            {{-- @if($product->out_of_stock != null)
                                <div class="cart" style="background: #d9534f !important;">
                                    <div class="">
                                        Out of stock
                                    </div>
                                </div>
                                @else
                                <div class="cart " style=" background-color:blueviolet;" >
                                    <!-- <a onclick="" href="{{$product->url ?? '#'}}" target="__blank"
                                    class="btn btn-block text-white bg-primary" data-color="primary">{{ trans('cruds.product.fields.order') }}</a> -->
                                    @if(!empty($product->shop) && $product->external_shop == 1)
                                                <a href="{{$product->link}}" style="color:white;" target="_blank">{{$product->shop}}</a>
                                                @else
                                                <i class="feather icon-shopping-cart"></i>
                                                <span class="add-to-cart" data-id="{{$product->id}}" data-name="{{$product->name ?? ''}}" data-quantity="{{$product->minimum_quantity ?? '1'}}" data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-maximum_quantity="{{ $product->maximum_quantity ?? '10000' }}"
                                                    data-price="{{$product->price ?? '0'}}" data-discounted_price="{{$product->discount ? $discounted_price:'0'}}" data-catalogid="{{$catalogId}}" data-catagory="{{(count($product->categories) > 0)?$product->categories[0]['name']:''}}" 
                                                    data-sku="{{ $product->sku }}" data-img="{{$img}}">Add to cart
                                                </span>
                                                <a href="{{$checkoutLink}}" class="view-in-cart d-none ">View In Cart</a>
                                                @endif
                                </div>
                            @endif --}}
                            {{-- @else --}}
                                @if($product->out_of_stock != null)
                                    <div class="cart" style="background: #d9534f !important;">
                                        <div class="">
                                            Out of stock
                                        </div>
                                    </div>
                                @else
                                    <div class="cart quantityController rounded-bottom">
                                        <!-- <a onclick="" href="{{$product->url ?? '#'}}" target="__blank"
                                        class="btn btn-block text-white bg-primary" data-color="primary">{{ trans('cruds.product.fields.order') }}</a> -->
                                        @if(!empty($product->shop) && $product->external_shop == 1)
                                                <a href="{{$product->link}}" style="color:white;" target="_blank">{{$product->shop}}</a>
                                                @else
                                                <i class="feather icon-shopping-cart"></i>
                                                <span class="add-to-cart" data-id="{{$product->id}}" data-name="{{$product->name ?? ''}}" data-quantity="{{$product->minimum_quantity ?? '1'}}" data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-maximum_quantity="{{ $product->maximum_quantity ?? '10000' }}"
                                                    data-price="{{$product->price ?? '0'}}" data-discounted_price="{{$product->discount ? $discounted_price:'0'}}" data-catalogid="{{$catalogId}}" data-catagory="{{(count($product->categories) > 0)?$product->categories[0]['name']:''}}"
                                                    data-sku="{{ $product->sku }}" data-img="{{$img}}">Add to cart
                                                </span>
                                                <!-- <span class="view-in-cart d-none"> -->
                                                    <!-- View in cart -->
                                                <!-- </span> -->
                                                <span class="view-in-cart d-none 6">
                                                    <div class="input-group quantity-counter-wrapper">
                                                        <input type="text" id="product_qty_{{$product->id}}" class="quantity-counter"  data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-maximum_quantity="{{ $product->maximum_quantity ?? '10000' }}" data-id="{{$product->id}}" value="{{$product->minimum_quantity ?? '1'}}">
                                                    </div>
                                                </span>
                                                @endif
                                    </div>
                                @endif
                            {{-- @endif --}}
                        </div>
                    </div>
                </div>
                @endforeach
@if($productcount == 0 && !empty($video_id))
    <div class=" d-flex justify-content-center text-center text-light m-1">
                <h4><b class="text-light">There is no product  available for this video</b></h4>
        </div>
@endif

@if( (($is_search == true) or ($is_filter == true)) and (count($data) == 0))
    <h3 class="text-light text-center p-2 w-100">"No products found matching your search or filter criteria"</h3>
@endif
                {!! $data->render() !!}
                <script src="{{asset('XR/assets/js/cart.js')}}"></script>


<script src="{{asset('XR/app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')}}">
<script>
    $('#productssearchTotal').empty().html({{$data->total()}});
// checkout quantity counter
  var quantityCounter = $(".quantity-counter"),
    CounterMin = 1,
    CounterMax = 10000;
  if (quantityCounter.length > 0) {
    quantityCounter.TouchSpin({
      min: CounterMin,
      max: CounterMax
    }).on('touchspin.on.startdownspin', function () {
      var $this = $(this);
      let min_qty = $(this).data("minimum_quantity") || 1;
      $this.trigger("touchspin.updatesettings", {min: min_qty})
      console.log(min_qty)
      $('.bootstrap-touchspin-up').removeClass("disabled-max-min");
      if ($this.val() <= min_qty) {
            $(this).siblings().find('.bootstrap-touchspin-down').addClass("disabled-max-min");
            // $(this).siblings().find('.bootstrap-touchspin-down').addClass("danger-max-min");
            // obj.removeItemFromCart(Number($(this).data('id')));
            // obj.totalCart();
            // $(this).closest('.view-in-cart').siblings('.add-to-cart').removeClass("d-none");
            // $(this).closest('.view-in-cart').siblings('.add-to-cart').addClass("d-inline-block");
            // $(this).closest('.view-in-cart').removeClass("d-inline-block");
            // $(this).closest('.view-in-cart').addClass("d-none");
            // e.stopImmediatePropagation();
        }
        obj.setCountForItem(Number($this.data('id')),$this.val());
        //$("#product_qty_"+$this.data('id')).val($this.val());

    }).on('touchspin.on.startupspin', function () {
      var $this = $(this);
      $('.bootstrap-touchspin-down').removeClass("disabled-max-min");
      if ($this.val() == CounterMax) {
        $(this).siblings().find('.bootstrap-touchspin-up').addClass("disabled-max-min");
      }
      obj.setCountForItem(Number($this.data('id')),$this.val());
      //$("#product_qty_"+$this.data('id')).val($this.val());

    }).on('change', function () {
      var $this = $(this);
    //   if ($this.val() == 1) {
    //     $(this).siblings().find('.bootstrap-touchspin-down').addClass("danger-max-min");
    //     }
      if ($this.val() == 0) {
            obj.setCountForItem(Number($this.data('id')),1);
        } else if ($this.val() > CounterMax) {
            obj.setCountForItem(Number($this.data('id')),CounterMax);
      } else {
         obj.setCountForItem(Number($this.data('id')),$this.val());
      }
    });
  }
  obj.setCountForItem = function(id, qty) {
    for(var i in cart) {
      if (cart[i].id === id) {
        cart[i].qty = qty;
        break;
      }
    }
    saveCart();
    obj.totalCart();
  };
  obj.removeItemFromCart = function(id) {
        for (var item in cart) {
            if (cart[item].id === id) {
                //   cart[item].qty --;
                //   if(cart[item].qty === 0) {
                cart.splice(item, 1);
                //   }
                //   break;
            }
        }
        saveCart();
    }

  $( document ).ready(function() {
    if (localStorage.getItem("shoppingCart") != null) {
        cart = JSON.parse(localStorage.getItem('shoppingCart'));
        var cartItemCount = count = cart.length;
        if(cart.length > 0) {
            var products = [];
            for(var item in cart) {
                var id = cart[item].id, qty = cart[item].qty;
                $("#product_qty_"+(id)).val(qty);
            }

        }
    }
    });

</script>
