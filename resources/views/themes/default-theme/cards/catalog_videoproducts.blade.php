@if(!empty($video_data))
    @php 
        $videocard = 0;
    @endphp

    @foreach ($video_data as $key => $card)                    
        @php
        $video = $card->video;
        if(!empty($video->thumbnail))
        {
            $img = $video->thumbnail->getUrl('thumb');
        }
        else{
            $img = asset('XR/assets/images/video-placeholder.png');
        }
        @endphp
        @if($card->video_id != $video_id)
            @php
                $isproduct = false;
                $catalogLink = "";
            @endphp
            
            @foreach($data as $key => $show)
                @if($show->video_id == $card->video_id)
                    @php
                        $isproduct = true;
                    @endphp
                @endif
            @endforeach
            @if($isproduct == true)
                @php
                    $videocard++;
                @endphp
                <div class="card ecommerce-card col-lg-3 col-md-6 col-sm-12 p-1 m-1" style="width:320px;  display: inline-block">
                        <div class="card-content" style="margin:auto;">
                                <a href="{{$catalogLink}}?video-id={{$video->id}}"> <i class="feather icon-play-circle fa-5x position-absolute text-light d-flex justify-content-center videoIcon"  ></i>
                                <img class="img-responsive mw-80 mh-80 rounded" src="{{$img}}" style="height:200px; width:100%; cursor:pointer;"/></a>
                                <b>{!! $video->title !!}</b>
                            <hr>
                        </div>
                        <div class="cart rounded text-center" style=" background-color:blueviolet;" >
                            <a href="{{$catalogLink}}?video-id={{$card->video_id}}" style="color:white;" >View video product</a>
                        </div>  
                </div>
            @endif  
            
        @endif  
    @endforeach
    @if($videocard == 0)
        <div class=" d-flex justify-content-center text-center text-light m-1">
                <h4><b class="text-light">There is no related video available</b></h4>   
        </div>
    @endif
@endif    