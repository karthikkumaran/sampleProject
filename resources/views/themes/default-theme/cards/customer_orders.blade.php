@foreach($orders as $order)
    @php
    $order_meta_data= json_decode($order->meta_data, true);

    if(App\orders::STATUS[$order->order->status] == 'Order Recieved')
    $class="badge-primary";
    elseif (App\orders::STATUS[$order->order->status] == 'Pending')
    $class="badge-warning";
    elseif (App\orders::STATUS[$order->order->status] == 'Accepted')
    $class="badge-success";
    elseif (App\orders::STATUS[$order->order->status] == 'Shipped')
    $class="badge-success";
    elseif (App\orders::STATUS[$order->order->status] == 'Delivered')
    $class="badge-success";
    elseif (App\orders::STATUS[$order->order->status] == 'Rejected')
    $class="badge-danger";
    elseif (App\orders::STATUS[$order->order->status] == 'Cancelled')
    $class="badge-danger";
    elseif (App\orders::STATUS[$order->order->status] == 'Failed')
    $class="badge-danger";
    @endphp
    @if(isset($order->order->transactions->amount))



    <div class="card ecommerce-card" id="order_list_{{$order->order_id}}">
        <div class="card-body">
            <div class="row">
                <div class="col col-md-4 order-1 cursor-pointer" onclick="location.href='{{ route('customer.show.order_details',['id'=>$order->id,'shareable_link'=> $sLink]) }}';">

                    <h5><span class='badge {{$class}}'>{{App\orders::STATUS[$order->order->status]}}</span></h5>
                    <h5> Order <b>#{{$order->order->order_ref_id}}</b></h5>
                    <span class="mb-1">Transaction .ID</span>
                    @if(isset($order->order->transactions->transaction_id))
                    <h5><b>#{{$order->order->transactions->transaction_id}}</b></h5>
                    @endif

                </div>
                <div class="col col-md-4 order-2 d-flex flex-column justify-content-md-center text-md-center cursor-pointer" onclick="location.href='{{ route('customer.show.order_details',['id'=>$order->id,'shareable_link'=> $sLink]) }}';">

                    <span>Delivered to </span></br>
                    <span class="h5"><b> {{' '.$order->addresstype}}</b></span>

                </div>
                <div class="col col-md-4 order-3 text-md-right cursor-pointer" onclick="location.href='{{ route('customer.show.order_details',['id'=>$order->id,'shareable_link'=> $sLink]) }}';">
                    <h6 class="mb-md-2 text-light">{{ date('d-m-Y', strtotime($order->created_at)) }}</h6>
                    <span>Total Payment</span>
                    <h4><b>{{ $order->order->transactions->amount }}</b></h4>
                    <!-- <h4><b>{!! $order_meta_data['product_total_price']==0?" ":$order_meta_data['currency'].''.$order_meta_data['product_total_price'] !!}</b></h4> -->

                </div>
            </div>
        </div>

    </div>
    @endif

    @endforeach

   
    @if(count($orders)==0)
        @if(count($status) == 1)
            @if(in_array('All',$status))
            <div class="text-center">
            <h4 >You haven't placed an order yet. We are excited to recieve your order</h4>
            <a href="{{$homeLink}}" class="btn btn-primary">Go to shop</a>
            </div>
            @else
            <div class="text-center">
            <h4 >No orders available matching your filter criteria</h4>
            <a href="#" onclick="window.location.reload();" class="btn btn-primary">Show all orders</a>
            </div>
            @endif
        @endif
    @endif