<!DOCTYPE html>
@php
$agent=new \Jenssegers\Agent\Agent();
@endphp

<html class="loaded" lang="en" style="width:100%;margin:auto;">
@php

if($campaign->is_start == 1) {

$checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
$catalogLink = route('campaigns.published',[$campaign->public_link]);

if(!empty($userSettings->customdomain)){

$homeLink = '//'.$userSettings->customdomain;

}else if(!empty($userSettings->subdomain)) {

$homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');

}else{

$homeLink = route('mystore.published',[$userSettings->public_link]);

}

}else{
    
$checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
$checkoutLink = route('admin.campaigns.preview',[$campaign->preview_link]);
$homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
$sLink = $userSettings->preview_link;
}

$sLink = $userSettings->public_link;

\Debugbar::disable();
if (app('impersonate')->isImpersonating() || (auth()->user() && auth()->user()->getIsAdminAttribute() )) {
\Debugbar::enable();
}

@endphp

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @if(!empty($product))
    @if(!empty($product->name))
    <title>{{ $meta_data['storefront']['storename'] ?? 'Store'}} - {{$product->name}}</title>
    @else
    <title>{{ $meta_data['storefront']['storename'] ?? 'Store'}}</title>
    @endif
    @else
    <title>{{ $meta_data['storefront']['storename'] ?? 'Store'}} - {{$campaign->name}}</title>
    @endif
    <!-- <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon.png') }}"> -->
    @livewireStyles
    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/vendors.min.css') }}">
    <!-- END: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/extensions/sweetalert2.min.css')}}">

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/components.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/extensions/toastr.css') }}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/assets/css/style.css') }}">
    <!-- <link rel="stylesheet" type="text/css" href="{{asset('themes/default-theme/style.css')}}"> -->
    @include('themes.'.$meta_data['themes']['name'].'..partials.colour_theme_style')
    <!-- END: Custom CSS-->
    <style>
        .text-blue{
            color: #3AADD8;
        }
        .text-red {
            color: #DB1F26;
        }
        @media (max-width: 767px){
        .content-size{
            width:100% !important;
        }
    }
    </style>
    @yield('styles')

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script src="{{ asset('js/common.js') }}"></script>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-169878524-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-169878524-1');
    </script>
    <!-- Google Tag Manager -->
    <script>
        (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-N67BLVX');
    </script>
    <!-- End Google Tag Manager -->
    @if(!empty($meta_data['analytic_settings']['google_analytics_id']))
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-{{$meta_data['analytic_settings']['google_analytics_id']}}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '{{$meta_data['analytic_settings']['google_analytics_id']}}');
        </script>
    @endif
    @if(!empty($meta_data['custom_script_settings']['custom_script_header']))
    {!! $meta_data['custom_script_settings']['custom_script_header'] !!}
    @endif
</head>

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 1-column  navbar-floating footer-static" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
<!-- Button trigger modal -->
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N67BLVX" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @if(!empty($meta_data['analytic_settings']['google_analytics_gtm_id']))
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{$meta_data['analytic_settings']['google_analytics_gtm_id']}}" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    @endif
    <!-- BEGIN: Content-->
    @include('themes.'.$meta_data['themes']['name'].'.partials.splash')
    @include('themes.'.$meta_data['themes']['name'].'.partials.public_header')

    @include('themes.'.$meta_data['themes']['name'].'.modals.customer')

   
    
    <div class="app-content content content-size" style="display: none;margin-top:-15px; width:35%; margin:auto !important;">
        <!-- <div class="content-overlay"></div> -->
        <!-- <div class="header-navbar-shadow"></div> -->
        <div class="content-wrapper m-0 p-0">
        @yield('content')
        </div>
    </div>

    @include('themes.'.$meta_data['themes']['name'].'.partials.storefrontfooter')

    <!-- BEGIN: Vendor JS-->
    <!-- Don't forget to add it after jQuery and Bootstrap -->
    <script src="{{ asset('XR/app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{asset('XR/app-assets/vendors/js/extensions/sweetalert2.all.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    <script src="{{ asset('js/customer.js') }}"></script>
    @livewireScripts
    <!-- BEGIN Vendor JS-->
    <script>

                @if(session('message'))
                    $('.loginButton').click()
                @endif

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            error: function(jqXHR, exception) {
                if (jqXHR.status === 0) {
                    Swal.fire('You lost connectivity', 'Verify your internet connection.', 'error');
                } else if (jqXHR.status == 401) {
                    window.location = "/login";
                } else if (jqXHR.status == 404) {
                    Swal.fire('Requested page not found', '404', 'error');
                } else if (jqXHR.status == 500) {
                    Swal.fire('Internal Server Error', '500', 'error');
                } else if (exception === 'parsererror') {
                    Swal.fire('Requested JSON parse failed', 'JSON Error', 'error');
                } else if (exception === 'timeout') {
                    Swal.fire('Time out error', '', 'error');
                } else if (exception === 'abort') {
                    Swal.fire('Ajax request aborted', '', 'error');
                } else {
                    var err = eval("(" + jqXHR.responseText + ")");
                    if (err.errors.code != undefined)
                        Swal.fire('Error!', err.errors.code, 'error');
                    else if (err.errors.name != undefined)
                        Swal.fire('Error!', err.errors.name, 'error');
                    else if (err.message != undefined)
                        Swal.fire('Error!', err.message, 'error');
                    else
                        Swal.fire('Error!', err, 'error');
                }
            }
        });

        var searchInputInputfield = $(".search-input input"),
            searchList = $(".search-input .search-list"),
            appContent = $(".app-content");
        $(".nav-link-search").on("click", function() {
            var $this = $(this);
            var searchInput = $(this).parent(".nav-search").find(".search-input");
            searchInput.addClass("open");
            searchInputInputfield.focus();
            searchList.find("li").remove();
            // bookmarkInput.removeClass("show");
        });
        $(".search-input-close i").on("click", function() {
            var $this = $(this),
                searchInput = $(this).closest(".search-input");
            if (searchInput.hasClass("open")) {
                searchInput.removeClass("open");
                searchInputInputfield.val("");
                searchInputInputfield.blur();
                searchList.removeClass("show");
                appContent.removeClass("show-overlay");
            }
        });
        $(window).on('load', function() {
            $('.app-content').hide();
            setTimeout(removeLoader, 3000); //wait for page load PLUS two seconds.
        });

        function removeLoader() {
            $("#loading-bg").fadeOut(500, function() {
                // fadeOut complete. Remove the loading div
                $('.app-content').show();
                $("#loading-bg").remove(); //makes page more lightweight 
            });
        }

        function share(share_name, msg) {
            var u = "{{Request::fullUrl()}}";
            var t = msg;
            if (msg == "") {
                t = "I found this website interesting, you could give a try";
            }
            switch (share_name) {
                case 'fb':
                    window.open('http://www.facebook.com/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t), 'sharer', 'toolbar=0,status=0,width=626,height=436');
                    break;
                case 'whatsapp':
                    var message = encodeURIComponent(u) + " " + t;
                    // var whatsapp_url = "whatsapp://send?text=" + message;
                    // window.location.href = whatsapp_url;

                    var whatsapp_url = "https://wa.me/?text=" + message;
                    openInNewTab(whatsapp_url);
                    break;
                case 'twitter':
                    window.open("https://twitter.com/intent/tweet?text=" + t + "&url=" + u, 'sharer', 'toolbar=0,status=0,width=626,height=436')
                    break;
            }
        }

        //lazy loading
        if ('loading' in HTMLImageElement.prototype) {
            const images = document.querySelectorAll("img.lazyload");
            images.forEach(img => {
                img.src = img.dataset.src;
            });
        } else {
            // Dynamically import the LazySizes library
            let script = document.createElement("script");
            script.async = true;
            script.src =
                "https://cdnjs.cloudflare.com/ajax/libs/lazysizes/4.1.8/lazysizes.min.js";
            document.body.appendChild(script);
        }

        function track_events(event_name, product_id, slink) {
            $.ajax({
                url: "{{route('admin.stats.ajaxEvents')}}",
                type: "post",
                data: {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    "event": event_name,
                    "slink": slink,
                    "product_id": product_id,
                },
            });
        }


            
$('.togglePassword').click( function() {
    const password = document.querySelector('#' + $(this).attr('data-id'));
    // toggle the type attribute
    const  type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);
    // toggle the eye slash icon
    this.classList.toggle('fa-eye-slash');
})

    $(".niceScroll").niceScroll({
        cursorcolor: "grey", // change cursor color in hex
        cursoropacitymin: 0, // change opacity when cursor is inactive (scrollabar "hidden" state), range from 1 to 0
        cursoropacitymax: 1, // change opacity when cursor is active (scrollabar "visible" state), range from 1 to 0
        cursorwidth: "5px", // cursor width in pixel (you can also write "5px")
        cursorborder: "1px solid #fff", // css definition for cursor border
        cursorborderradius: "5px", // border radius in pixel for cursor
        zindex: "auto", // change z-index for scrollbar div
        scrollspeed: 60, // scrolling speed
        mousescrollstep: 40, // scrolling speed with mouse wheel (pixel)
        touchbehavior: false, // enable cursor-drag scrolling like touch devices in desktop computer
        hwacceleration: true, // use hardware accelerated scroll when supported
        boxzoom: false, // enable zoom for box content
        dblclickzoom: true, // (only when boxzoom=true) zoom activated when double click on box
        gesturezoom: true, // (only when boxzoom=true and with touch devices) zoom activated when pinch out/in on box
        grabcursorenabled: true, // (only when touchbehavior=true) display "grab" icon
        autohidemode: true, // how hide the scrollbar works, possible values: 
        background: "", // change css for rail background
        iframeautoresize: true, // autoresize iframe on load event
        cursorminheight: 32, // set the minimum cursor height (pixel)
        preservenativescrolling: true, // you can scroll native scrollable areas with mouse, bubbling mouse wheel event
        railoffset: false, // you can add offset top/left for rail position
        bouncescroll: false, // (only hw accell) enable scroll bouncing at the end of content as mobile-like 
        spacebarenabled: true, // enable page down scrolling when space bar has pressed
        disableoutline: true, // for chrome browser, disable outline (orange highlight) when selecting a div with nicescroll
        horizrailenabled: true, // nicescroll can manage horizontal scroll
        railalign: "right", // alignment of vertical rail
        railvalign: "bottom", // alignment of horizontal rail
        enabletranslate3d: true, // nicescroll can use css translate to scroll content
        enablemousewheel: true, // nicescroll can manage mouse wheel events
        enablekeyboard: true, // nicescroll can manage keyboard events
        smoothscroll: true, // scroll with ease movement
        sensitiverail: true, // click on rail make a scroll
        enablemouselockapi: true, // can use mouse caption lock API (same issue on object dragging)
        cursorfixedheight: false, // set fixed height for cursor in pixel
        hidecursordelay: 400, // set the delay in microseconds to fading out scrollbars
        irectionlockdeadzone: 6, // dead zone in pixels for direction lock activation
        nativeparentscrolling: true, // detect bottom of content and let parent to scroll, as native scroll does
        enablescrollonselection: true, // enable auto-scrolling of content when selection text
        cursordragspeed: 0.3, // speed of selection when dragged with cursor
        rtlmode: "auto", // horizontal div scrolling starts at left side
        cursordragontouch: false, // drag cursor in touch / touchbehavior mode also
        oneaxismousemode: "auto",
        scriptpath: "", // define custom path for boxmode icons ("" => same script path)
        preventmultitouchscrolling: true, // prevent scrolling on multitouch events
        disablemutationobserver: false,
    });
    </script>
    @yield('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10">
    </script>
    ...   <x-livewire-alert::scripts />
    @if(!empty($meta_data['custom_script_settings']['custom_script_footer']))
    {!! $meta_data['custom_script_settings']['custom_script_footer'] !!}
    @endif
</body>

</html>