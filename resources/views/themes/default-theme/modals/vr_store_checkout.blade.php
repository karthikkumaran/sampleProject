<div class="modal modal-fullscreen p-0 fade ecommerce-application" id="vr-store-checkout" tabindex="-1" role="dialog" aria-labelledby="vr-store-checkout" aria-hidden="true">
  @livewire('storefront.themes.'default-theme'.checkout', ['campaign' => $campaign, 'userSettings' => $userSettings, 'meta_data' => $meta_data, 'customer' => $customer])
</div>