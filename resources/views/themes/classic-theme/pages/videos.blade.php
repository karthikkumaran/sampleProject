@extends('themes.'.$meta_data['themes']['name'].'.layouts.video')
@section ('styles')
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">

<link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/extensions/nouislider.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/ui/prism.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/forms/select/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/core/colors/palette-gradient.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/plugins/extensions/noui-slider.min.css') }}">

<style>
    .iframe{
        width: 100%;  
        height:270px;
        border-radius:8px;
        padding:2px;
    }
    
}

</style>
@endsection
@section('content')
@php 
if($userSettings->is_start == 1) {
if(!empty($userSettings->customdomain)){
$homeLink = '//'.$userSettings->customdomain;
}else if(!empty($userSettings->subdomain)) {
$homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
}else{
$homeLink = route('mystore.published',[$userSettings->public_link]);
}
}else{
$homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
}

if(empty($meta_data['settings']['currency']))
$currency = "₹";
else
if($meta_data['settings']['currency_selection'] == "currency_symbol")
$currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
else
$currency = $meta_data['settings']['currency']
@endphp
@section('content')
@include('themes.'.$meta_data['themes']['name'].'.modals.details_product')


                @php
                    if($campaign->is_start == 1) {
                        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
                        $catalogLink = route('campaigns.published',$campaign->public_link);
                        $shoppableLink = route('mystore.publishedVideo',$userSettings->public_link);  
                    }else{
                        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
                        $catalogLink = route('admin.campaigns.preview',$campaign->preview_link);
                        $shoppableLink = route('mystore.publishedVideo',$userSettings->preview_link);
                    }
                @endphp
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2 pl-0">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb border-0">
                        @if( (request()->is('s/*') || request()->is('admin/storePreview/*')) == false)
                        <li class="breadcrumb-item"><a class="text-green" href="{{$homeLink}}">Home</a></li>
                            @if(!empty($video_id))
                            <li class="breadcrumb-item">Shoppable Videos</li>
                            <li class="breadcrumb-item">{!! $campaign->name == "Untitled"?"&nbsp;":$campaign->name !!}</li>
                            @else
                            <li class="breadcrumb-item">{!! $campaign->name == "Untitled"?"&nbsp;":$campaign->name !!}</li>
                            @endif
                        @else
                        <li class="breadcrumb-item">{!! $campaign->name == "Untitled"?"&nbsp;":$campaign->name !!}</li>
                        @endif
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
@foreach ($video_data as $key => $card)

    @php
    $attributes = [
        'type' => null,
        'class' => 'iframe',
        'data-html5-parameter' => true
    ];
    $whitelist = []; $params = [];
    $video = $card->video;
    @endphp
@if($card->video_id == $video_id)
<div class="card rounded m-0 " style="width:100%;height:100%;">
    <div class="card-header p-0">
        <h4 class="text-truncate p-1"><b>{{$video->title}}</b></h4>
    </div>
    <div class="card-content">
        <div style="overflow: hidden;">
            {!! \LaravelVideoEmbed::parse( old('video_url', $video->video_url), $whitelist, $params,$attributes ) !!}
        </div>
    </div>      
    <div class="card-footer " style="padding:1rem;background-color:#fff">
        <div class="float-left p-1 cursor-pointer w-50 text-center cart quantityController" id="hidevideo" onclick='hidevideo()'><i class="feather icon-box"></i> Products </div>   
        <div class="float-right p-1 cursor-pointer relatedVideos w-50 text-center" id="hideproduct" onclick='hideproduct()'><i class="feather icon-video"></i> Related Videos </div>
    </div> 
</div>
@endif
@endforeach 
<!-- Ecommerce Products Starts -->
<section id="ecommerce-productscatalog" style="margin:auto;width:95%;" class="grid-view d-block mb-4 mt-1">
                    
    <div id="catalog_prod" class="row">

    </div>
</section>
<!-- Ecommerce Products Ends -->
<!-- Ecommerce Videos Starts -->
<section id="ecommerce-videocatalog" class="grid-view d-block mb-4">
    <div id="catalog_video" class="row" style="margin-top:-35px;">
    
    </div>
</section>
                <!-- Ecommerce Videos Ends -->
@endsection
@section('scripts')
@parent
<script src="{{asset('XR/assets/js/cart.js')}}"></script>

<!-- BEGIN: Page Vendor JS-->
<script src="{{ asset('XR/app-assets/vendors/js/ui/prism.min.js') }}"></script>
<script src="{{ asset('XR/app-assets/vendors/js/extensions/wNumb.js') }}"></script>
<script src="{{ asset('XR/app-assets/vendors/js/extensions/nouislider.min.js') }}"></script>
<script src="{{ asset('XR/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{ asset('XR/app-assets/js/core/app-menu.js') }}"></script>
<script src="{{ asset('XR/app-assets/js/core/app.js') }}"></script>
<script src="{{ asset('XR/app-assets/js/scripts/components.js') }}"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="{{ asset('XR/app-assets/js/scripts/pages/app-ecommerce-shop.js') }}"></script>
<!-- END: Page JS -->
<script>
    
    var q = "";
    var searchData = "";
    var sort = "";
    var filter = [];
    var page = 0;
    var filterData = "";
    var preview = 0;
    var meta = @json($meta_data);
    var theme;
    if (meta) {
        if (meta['themes']) {
            if (meta['themes']['name'])
                theme = meta['themes']['name'];
        } else
            theme = "default-theme";
    } else {
        theme = "default-theme";
    }
    var page_url = window.location.pathname;
    // shareable_link=page_url.slice(page_url.lastIndexOf('/')+1);
    var shareable_link = "{{($userSettings->is_start == 1)?$campaign->public_link:$campaign->preview_link}}";

    var u = window.location.href;
    if (u.includes("preview")) {
        preview = 1;
    }
    getProducts(0);
    $(document).on('click',"#ecommerce-productscatalog",'.pagination a', function(event) {
        console.log('prod')
        event.preventDefault();

        $('li').removeClass('active');
        $(this).parent('li').addClass('active');

        var myurl = $(this).attr('href');
        var page = $(this).attr('href').split('page=')[1];

        getProducts(page);
    });

    
    function getProducts(page) {
        $('.loading1').show();
        q = 'q=' + searchData + '&s=' + sort + '&page=' + page + '&preview=' + preview + '&filterData=' + filterData + '&shareable_link=' + shareable_link + '&theme=' + theme + '&filter=' + JSON.stringify(filter) + '&video_id=' + '{{$video_id}}';
        // console.log(q);
        $.ajax({
            url: "{{route('admin.campaigns.ajaxVideoProductData')}}" + '?' + q,
            type: "get",
            datatype: "html"
        }).done(function(data) {
            //console.log(data);
            //console.log("sucess");  
            $("#catalog_prod").empty().html(data);
            //$("#product_list").append(data);
            $('.loading').hide();
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            $('.loading').hide();
            // alert('No response from server');
        });
    }
    
    $(document).on('click',".relatedVideos", function(event) {
        event.preventDefault();
        console.log('video')
        // $('li').removeClass('active');
        // $(this).parent('li').addClass('active');

        // var myurl = $(this).attr('href');
        // var page = $(this).attr('href').split('page=')[1];
        getVideo(0);
    });


    function getVideo(page) {
        console.log('video test')
        $('.loading1').show();
        q = 'q=' +  '&s=' + searchData + sort + '&page=' + page + '&preview=' + preview + '&filterData=' + filterData + '&shareable_link=' + shareable_link + '&theme=' + theme + '&filter=' + JSON.stringify(filter) + '&video_id=' + '{{$video_id}}';
        
        $.ajax({
            url: "{{route('admin.campaigns.ajaxRelatedVideoData')}}" + '?' + q,
            type: "get",
            datatype: "html"
        }).done(function(data) {
            //console.log(data);
            //console.log("sucess");  
            $("#catalog_video").empty().html(data);
            //$("#product_list").append(data);
            $('.loading').hide();
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            $('.loading').hide();
            // alert('No response from server');
        });
    }

    function hideproduct()
{
    $("#catalog_prod").hide();
    $("#catalog_video").show();
    $('#hideproduct').addClass('cart');
    $('#hideproduct').addClass('quantityController');
    $('#hidevideo').removeClass('cart');
    $('#hidevideo').removeClass('quantityController');
}


function hidevideo()
{
    $("#catalog_video").hide();
    $("#catalog_prod").show();
    $('#hidevideo').addClass('cart');
    $('#hidevideo').addClass('quantityController');
    $('#hideproduct').removeClass('cart');
    $('#hideproduct').removeClass('quantityController');
}
</script>
@endsection
