@extends($layout)
@section('styles')
@parent

<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/forms/select/select2.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/pickers/pickadate/pickadate.css')}}">




@endsection
@section('content')
@php

$user=Auth::guard('customer')->user();
$user_metadata=json_decode($user->meta_data,true);
if(isset($user_metadata['address']))
    $address=$user_metadata['address'];
else
    $address=$customer;
@endphp


<div class="mt-md-1">
    <h2 class="content-header-title mb-md-1">Account Settings</h2>
    <!-- account setting page start -->
    <section id="page-account-settings">
        <div class="row">
            <!-- left menu section -->
            <div class="col-md-3 mb-2 mb-md-0">
                <ul class="nav nav-pills flex-column mt-md-0 mt-1">
                    <li class="nav-item">
                        <a class="nav-link d-flex py-75 active" id="my-account-pill" data-toggle="pill" href="#my-account" aria-expanded="true">
                            <i class="fa fa-user mr-50 font-medium-3"></i>
                            My Account
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link d-flex py-75 " id="my-address-pill" data-toggle="pill" href="#my-address" aria-expanded="false">
                            <i class="fa fa-address-book mr-50 font-medium-3"></i>
                            My Address
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link d-flex py-75" id="change-password-pill" data-toggle="pill" href="#change-password" aria-expanded="false">
                            <i class="feather icon-lock mr-50 font-medium-3"></i>
                            Change Password
                        </a>
                    </li>
                    @if(Auth::guard('customer')->check())
                    <li class="nav-item d-lg-none">
                        <a class="nav-link d-flex py-75" href="{{ route('customer.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" >
                            <i class="fa fa-sign-out mr-50 font-medium-3"></i> Logout</a>
                            <form id="logout-form" action="{{ route('customer.logout') }}" method="POST" style="display: none;">
                            @csrf
                            </form>
                    </li>
                    @endif

                </ul>
            </div>
            <!-- right content section -->
            <div class="col-md-9">
                <div class="card">
                    <div class="card-content">
                        <div class="card-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="my-account" role="tabpanel" aria-labelledby="my-account-pill" aria-expanded="true">

                                    <h4>My Account</h4>
                                    <hr>
                                    <form method="POST" id="formMyAccount" action="{{route('customer.update.profile')}}" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="hash" value="my-account">

                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" class="form-control" id='name' name="name" value="{{$user->name}}" Required>
                                        </div>

                                        <div class="row form-group">
                                            <div class="col-6">
                                                <label for="country_code">Country code</label>
                                                <select id="country_code" class="form-control" name="country_code" required>
                                                    @foreach(App\Customer::MOBILE_COUNTRY_CODE as $key=>$value)
                                                    
                                                        <option value="{{$key}}" {{ old('country_code', $user->country_code ?? '91') ==  $key ? 'selected' : '' }}>{{$value}}</option>
                                                    @endforeach
                                                </select>
                                            
                                            </div>
                                            <div class="col-6">
                                                <label for="mobile">Mobile Number</label>
                                                <input type="tel" class="form-control" id='mobile' name="mobile" value="{{$user->mobile}}" placeholder="9999999999" pattern="[0-9]{10}" aria-describedby="mobileHelpBlock" Required>
                                                <small id="mobileHelpBlock" class="form-text text-muted">
                                                    Your mobile number must be 10 digits Ex: 9999999999
                                                </small>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="text" class="form-control" id='email' name="email" value="{{$user->email}}" Required>
                                        </div>

                                        <!-- <button type="submit" class="btn btn-danger float-right mb-1">Save Changes</button>
                                                    <button type="reset" class="btn btn-outline-warning">Cancel</button> -->
                                        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                            <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save
                                                changes</button>
                                            <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                        </div>

                                    </form>
                                </div>


                                <div class="tab-pane fade" id="my-address" role="tabpanel" aria-labelledby="my-address-pill" aria-expanded="false">
                                    <h4>My Address</h4>
                                    <hr>
                                    <form method="POST" id="address-form" action="{{route('customer.update.address')}}" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$user->id}}">
                                        <input type="hidden" name="hash" value="my-address">

                                        <div class="row">
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label for="checkout-apt_number">Flat, House No, Street<span class="text-danger">*</span>:</label>
                                                    <input type="text" id="checkout-apt_number" class="form-control required" name="address1" value="{{isset($address['address1'])?$address['address1']:''}}"required>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label for="checkout-landmark">Landmark e.g. near apollo hospital<span class="text-danger">*</span>:</label>
                                                    <input type="text" id="checkout-landmark" class="form-control" name="address2"value="{{isset($address['address2'])?$address['address2'] : ''}}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label for="checkout-city">Town/City<span class="text-danger">*</span>:</label>
                                                    <input type="text" id="checkout-city" class="form-control required" name="city" value="{{isset($address['city'])?$address['city']:''}}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label for="checkout-pincode">Pincode<span class="text-danger">*</span>:</label>
                                                    <input type="number" id="checkout-pincode" class="form-control required" name="pincode" value="{{isset($address['pincode'])?$address['pincode']:''}}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label for="checkout-state">State<span class="text-danger">*</span>:</label>
                                                    <input type="text" id="checkout-state" class="form-control required" name="state" value="{{isset($address['state'])?$address['state']:''}}" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-12">
                                                <div class="form-group">
                                                    <label for="checkout-country">Country<span class="text-danger">*</span>:</label>
                                                    <input type="text" id="checkout-country" class="form-control required" name="country" value="{{isset($address['country'])?$address['country']:''}}" required>
                                                </div>
                                            </div>
                                            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                                <button type="submit" id="pw_submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save changes</button>
                                                <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                            </div>
                                        </div>
                                    </form>

                                </div>



                                <div class="tab-pane fade" id="change-password" role="tabpanel" aria-labelledby="change-password-pill" aria-expanded="false">
                                    <h4>Change Password</h4>
                                    <hr>
                                    @if(session('error-msg'))
                                    <div class="alert alert-danger alert-dismissible fade show" role='alert'>
                                        <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                                        <strong> {{session('error-msg')}} </strong>
                                    </div>
                                    @endif
                                    @if(session('success-msg'))
                                    <div class="alert alert-success alert-dismissible fade show" role='alert'>
                                        <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                                        <strong> {{session('success-msg')}} </strong>
                                    </div>
                                    @endif
                                    <form method="POST" id="password-reset-form" action="{{route('customer.change.password')}}" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$user->id}}">
                                        <input type="hidden" name="hash" value="change-password">

                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label for="account-old-password">Old Password</label>
                                                        <input type="password" name="old_password" class="form-control" id="old-password" placeholder="Old Password" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label for="account-new-password">New Password</label>
                                                        <input type="password" name="new_password" class="form-control" id="new-password" placeholder="New Password" minlength="6" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 mb-1">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label for="account-retype-new-password">Retype New Password</label>
                                                        <input type="text" name="new_confirm_password" class="form-control" id="re-new-password" placeholder="New Password" minlength="6" required>
                                                    </div>
                                                </div>
                                                <div style="margin-top: -20px;font-size:10px;" id="PasswordMatch"></div>

                                            </div>
                                            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                                <button type="submit" id="pw_submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save changes</button>
                                                <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- account setting page end -->
</div>
@endsection
@section('scripts')
@parent
<script src="{{asset('XR/app-assets/js/scripts/pages/account-setting.js') }}"></script>
<script src="{{asset('XR/app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
<script src="{{asset('XR/app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
<script src="{{asset('XR/app-assets/vendors/js/pickers/pickadate/picker.js')}}"></script>
<script src="{{asset('XR/app-assets/vendors/js/pickers/pickadate/picker.date.js')}}"></script>

<script>
    var hash = window.location.hash;
    // console.log(hash)
    hash && $('ul.nav a[href="' + hash + '"]').tab('show');

    $('.nav-item .nav-link').click(function(e) {
        $(this).tab('show');
        var scrollmem = $('body').scrollTop();
        window.location.hash = this.hash;
    });


    new_password = document.getElementById('new-password');
    new_confirm_password = document.getElementById('re-new-password');

    new_password.oninput = function() {
        if (new_confirm_password.value != '') {
            checkPasswordMatch()
        }
    }

    new_confirm_password.oninput = checkPasswordMatch

    function checkPasswordMatch() {
        if (new_password.value != new_confirm_password.value) {
            $('#pw_submit').prop('disabled', true);
            $("#PasswordMatch").html("Password does not match !").css("color", "red");
        } else {
            $("#PasswordMatch").html(" ");
            $('#pw_submit').prop('disabled', false);
        }
    }
</script>

@endsection