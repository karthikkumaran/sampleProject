@extends('themes.'.$meta_data['themes']['name'].'.layouts.public')
@section ('styles')
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/plugins/forms/wizard.css')}}">

@endsection
@section('content')
@php
if($campaign->is_start == 1) {
$catalogLink = route('campaigns.published',[$campaign->public_link]);
}else{
$catalogLink = route('admin.campaigns.preview',[$campaign->preview_link]);
}
if($userSettings->is_start == 1) {
if(!empty($userSettings->customdomain)){
$homeLink = '//'.$userSettings->customdomain;
}else if(!empty($userSettings->subdomain)) {
$homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
}else{
$homeLink = route('mystore.published',[$userSettings->public_link]);
}
}else{
$homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
}

if(empty($meta_data['settings']['currency']))
$currency = "₹";
else
if($meta_data['settings']['currency_selection'] == "currency_symbol")
$currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
else
$currency = $meta_data['settings']['currency'];

if(empty($meta_data['settings']['contactnumber']))
$contactNumber = "";
else
$contactNumber = $meta_data['settings']['contactnumber'];

if(empty($meta_data['settings']['emailid']))
$emailFrom = $campaign->user->email;
else
$emailFrom = $meta_data['settings']['emailid'];

if(!empty($meta_data['payment_settings']['payment_method']))
    $payment_settings = $meta_data['payment_settings'];
else
    $payment_settings=['payment_method'=>["Enquiry"],'Enquiry'=>['desc-text'=>'']];

    //guest user

$guest_user=false;
if(!auth()->guard('customer')->check())
{
    if(isset($meta_data['checkout_settings']) && $meta_data['checkout_settings']['guest_user'] == "on")
        $guest_user=true;
}

@endphp
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb border-0">
                        <li class="breadcrumb-item"><a href="{{$homeLink}}">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="{{$catalogLink}}">{!! $campaign->name == "Untitled"?"&nbsp;":$campaign->name !!}</a>
                        </li>
                        <li class="breadcrumb-item active">Checkout
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body ecommerce-application">
    <form action="#" name="checkout_form" class="icons-tab-steps checkout-tab-steps wizard-circle">
        @csrf
        <input type="hidden" value="{{$campaign->public_link}}" name="key" />
        <!-- Checkout Place order starts -->
        <h6><i class="step-icon step feather icon-shopping-cart"></i>Cart</h6>
        <fieldset class="checkout-step-1 px-0">
            <section id="place-order" class="list-view product-checkout">
                <div class="checkout-items">

                </div>
                <div class="checkout-options">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <!-- <p class="options-title">Options</p>
                                            <div class="coupons">
                                                <div class="coupons-title">
                                                    <p>Coupons</p>
                                                </div>
                                                <div class="apply-coupon">
                                                    <p>Apply</p>
                                                </div>
                                            </div>
                                            <hr> -->
                                <div class="price-details">
                                    <p>Price Details</p>
                                </div>
                                <div class="detail" style="display:none;">
                                    <div class="detail-title">
                                        Total MRP
                                    </div>
                                    <div class="detail-amt mrp-amt">

                                    </div>
                                </div>
                                <!-- <div class="detail">
                                                <div class="detail-title">
                                                    Bag Discount
                                                </div>
                                                <div class="detail-amt discount-amt">
                                                    -25$
                                                </div>
                                            </div>
                                            <div class="detail">
                                                <div class="detail-title">
                                                    Estimated Tax
                                                </div>
                                                <div class="detail-amt">
                                                    $1.3
                                                </div>
                                            </div> -->
                                <!-- <div class="detail">
                                                <div class="detail-title">
                                                    EMI Eligibility
                                                </div>
                                                <div class="detail-amt emi-details">
                                                    Details
                                                </div>
                                            </div>
                                            <div class="detail">
                                                <div class="detail-title">
                                                    Delivery Charges
                                                </div>
                                                <div class="detail-amt discount-amt">
                                                    Free
                                                </div>
                                            </div> -->
                                <hr>
                                <div class="detail">
                                    <div class="detail-title detail-total">Total</div>
                                    <div class="detail-amt total-amt text-dark"></div>
                                </div>
                                <div class="detail">
                                    <div class="detail-title detail-total">Discount</div>
                                    <div class="detail-amt discount-amt text-dark text-bold-700">- {!! $currency !!}0</div>
                                </div>
                                @if(isset($meta_data['checkout_settings']['display_coupons']) && $meta_data['checkout_settings']['display_coupons'] == 'on')
                                <div class="detail">
                                    <div class="detail-title detail-total">Coupon Discount</div>
                                    <div class="detail-amt coupon-discount-amt text-dark text-bold-700">- {!! $currency !!}0</div>
                                </div>
                                @endif
                                <div class="detail">
                                    <div class="detail-title detail-total">Grand Total</div>
                                    <div class="detail-amt grand-amt text-dark text-bold-700"></div>
                                </div>
                                <hr>
                                @if(isset($meta_data['checkout_settings']['display_coupons']) && $meta_data['checkout_settings']['display_coupons'] == 'on')
                                <div class="detail-title detail-total"><b>Coupon Code</b></div> <br>
                                <div class="input-group">
                                    <input class="form-control" name="couponCode" id="coupon_code">
                                    <div class="alert alert-success alert-dismissible"  id="coupon_alert" style="width:10rem;" hidden>
                                        <a href="javascript:void(0);" class="close" id="coupon_alert_close"  aria-label="close">&times;</a>
                                        <strong><span id="coupon_badge" >Indi-cates<span></strong>
                                    </div>
                                </div>
                                <small class="text-danger text-sm" id="coupon_error_message"></small>
                                <small class="text-success text-sm" id="coupon_success_message"></small>


                                <div class="mt-1 btn btn-primary btn-block apply_coupon cursor-pointer" id="apply_coupon">Apply Coupon Code</div>
                                <hr>
                                @endif
                                <!-- <div class="detail"> -->
                                <!-- </div> -->
                                <!-- <hr> -->
                                <div class="detail-title detail-total"><b>Special Notes</b></div> <br>
                                <div class="input-group">
                                    <textarea class="form-control" aria-label="With textarea" name="order_notes" maxlength="1000"></textarea>
                                </div>
                                <!-- <hr>
                                <div>
                                    Note: Payment and delivery details will be shared with you during confirmation of the order.
                                </div>
                                <hr> -->
                                @if(in_array( 'Enquiry' , $payment_settings['payment_method']))
                                    <div class=" mt-1 btn btn-primary btn-block place-order cursor-pointer" id="enquiry_btn">
                                        Enquire
                                    </div>
                                @endif
                                @if(in_array( 'Enquiry' , $payment_settings['payment_method']) && count( $payment_settings['payment_method'])>1)
                                    <h4 class="text-center mt-1">-OR-</h4>
                                @endif
                                @if( !in_array( 'Enquiry' , $payment_settings['payment_method']) || count( $payment_settings['payment_method'])>1 )
                                <div class=" mt-1 btn btn-primary btn-block place-order cursor-pointer" id="place_order_btn">
                                    Place Order
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </fieldset>
        <!-- Checkout Place order Ends -->

        <!-- Checkout Customer Address Starts -->
        <h6><i class="step-icon step feather icon-home"></i>Address</h6>
        <fieldset class="checkout-step-2 px-0">
            <section id="checkout-address" class="product-checkout">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title text-right">Enter Your Address</h4>
                        @if(!Auth::guard('customer')->check())
                            <h5 class="text-left">Already a customer? <button type="button" class="btn btn-link p-0" id='checkout_login_link'>Login</button></h5>
                        @endif
                    </div>
                    <div class="card-content">
                        <div class="card-body">
                            <h6><span>*</span> Required fields</h6>
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="checkout-name">Full Name<span class="text-danger">*</span>:</label>
                                        <input type="text" id="checkout-name" class="form-control required" name="fname" value="{{ isset($customer)? $customer['name']:''}}">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group row">
                                        <div class="col-6">
                                            <label for="country_code">Country code</label>
                                            <select id="checkout-country_code" class="form-control required" name="country_code">
                                                @foreach(App\Customer::MOBILE_COUNTRY_CODE as $key=>$value)
                                                    <option value="{{$key}}" {{ isset($customer["country_code"]) && $customer["country_code"] == $key ? 'selected' : '' }}>{{$value}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-6">
                                            <label for="checkout-number">Mobile Number<span class="text-danger">*</span>:</label>
                                            <input type="tel" id="checkout-mnumber" class="form-control required" name="mnumber"  value="{{ isset($customer)? $customer['mobile'] :''}}" placeholder="Contact Number here..." onkeypress="return isNumberKey(event,this)" pattern="[0-9]{10}" maxlength="10">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="checkout-number">Alternative Mobile Number:</label>
                                        <input type="number" id="checkout-amnumber" class="form-control" name="amnumber" value="{{ isset($customer['amobileno'])? $customer['amobileno'] :''}}">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="checkout-email">Email<span class="text-danger">*</span>:</label>
                                        <input type="email" id="checkout-email" class="form-control required" name="email" value="{{  isset($customer)? $customer['email'] :''}}">
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="checkout-apt_number">Flat, House No, Street<span class="text-danger">*</span>:</label>
                                        <input type="text" id="checkout-apt_number" class="form-control required" name="apt_number" value="{{ isset($customer['address1'])? $customer['address1'] :''}}">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="checkout-landmark">Landmark e.g. near apollo hospital:</label>
                                        <input type="text" id="checkout-landmark" class="form-control" name="landmark" value="{{ isset($customer['address2'])? $customer['address2'] :''}}">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="checkout-city">Town/City<span class="text-danger">*</span>:</label>
                                        <input type="text" id="checkout-city" class="form-control required" name="city" value="{{ isset($customer['city'])? $customer['city'] :''}}">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="checkout-pincode">Pincode<span class="text-danger">*</span>:</label>
                                        <input type="number" id="checkout-pincode" class="form-control required" name="pincode" value="{{ isset($customer['pincode'])? $customer['pincode'] :''}}">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="checkout-state">State<span class="text-danger">*</span>:</label>
                                        <input type="text" id="checkout-state" class="form-control required" name="state" value="{{ isset($customer['state'])? $customer['state'] :''}}">
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="add-type">Address Type:</label>
                                        <select class="form-control" id="add-type" name="addresstype">
                                            <option "{{ isset($customer['addresstype'])&& $customer['addresstype']=='Home' ? 'selected':''}}" >Home</option>
                                            <option "{{ isset($customer['addresstype'])&& $customer['addresstype']=='work' ? 'selected':''}}" >Work</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <button type="reset" class="btn btn-secondary mb-1 float-left">CLEAR ADDRESS</button>
                            <button type="button" class="btn btn-primary mb-1 delivery-address float-right" id="delivery_address">
                                NEXT
                            </button>
                        </div>
                    </div>
                </div>

            </section>
        </fieldset>

        <!-- Checkout Customer Address Ends -->

        <!-- checkout payment starts -->
        <h6><i class="step-icon step fa fa-money"></i>Payment</h6>

        <fieldset class="checkout-step-3 px-0">
            <section id="checkout-payment" class="product-checkout">

                <h4 class="mb-1"><b>Select a payment method</b></h4>

                <div class="row">

                    @foreach($payment_settings['payment_method'] as $method)
                    @if($method == "Enquiry")
                        @continue
                    @endif
                    <div class="col-12 col-md-3 col-sm-6">
                        <div class="card text-center cursor-pointer paymentMethod" data-method="{{$method}}" id="{{str_replace(' ','-',$method)}}" style="max-height:101px;">
                            <div class="card-body">
                                <div class="avatar-content">
                                    @if($method=="Razorpay")
                                    <img src="{{ asset('XR\app-assets\images\icons\razorpay_logo.svg') }}" style="width:150px;">
                                    @endif

                                    @if($method=="Cash on delivery")
                                    <img src="{{ asset('XR\app-assets\images\icons\cash_on_delivery.png') }}" style="width:31px;margin-left:-11px"> <span style='font-size:18px' class="text-dark"><b>Cash on Delivery</b></span>
                                    @endif

                                    @if($method=="Paypal")
                                    <img src="{{ asset('XR\app-assets\images\icons\paypal_logo.svg') }}" style="width:129px;margin:-50px">
                                    @endif

                                    @if($method=="UPI")
                                    <img src="{{ asset('XR\app-assets\images\icons\upi.svg') }}" style="width:129px;margin:-16px">
                                    @endif

                                </div>
                            </div>
                        </div>

                    </div>
                    @endforeach
                </div>
                <hr>
                @foreach($payment_settings['payment_method'] as $method)

                <div class="pay_description" id="desc_{{str_replace(' ','-',$method)}}" hidden=true>
                    {{$payment_settings[$method]['desc-text']}}
                </div>
                @endforeach
                <button type="button" class="btn btn-primary make-payment float-right " disabled>
                    Make Payment
                </button><br><br>
                <div class="float-right" id="paypal-button-container" style="margin-top:-40px;"></div>

            </section>
        </fieldset>


        <!-- coupon fields -->
        <input type="hidden" name='coupon_code_applied' id="coupon_code_applied">
        <input type="hidden" name='coupon_discount_amount' id="coupon_discount_amount">
        <input type="hidden" name='coupon_id' id="coupon_id">

    </form>

</div>
@endsection
@section('scripts')
@parent
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="{{asset('XR/app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')}}"></script>
<script src="{{asset('XR/app-assets/vendors/js/extensions/jquery.steps.min.js')}}"></script>
<script src="{{asset('XR/app-assets/vendors/js/forms/validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('js/customer.js')}}"></script>
<script>
    var cart = [];
    var obj = {};
    var slink = "";
    var payment_method = '';
    var init_id = "";
    var place_order_btn_disabled = 0,enquiry_btn_disabled = 0;

    var coupon_applied=false;
    var min_cart_value='';
    var coupon_amount = '';
    var coupon_code='';
    var coupon_unit='';
    var coupon_id='';
    var coupon_desc='';
    var discount=0;
    var coupon_discount=0;



    function payment_selection(id) {

        if (id != init_id) {
            $('#' + id).addClass('border-primary border-3 p-0');
            payment_method = $('#' + id).attr('data-method');

            if (init_id != "")
                $('#' + init_id).removeClass('border-primary border-3');
            init_id = id;
        } else if (id == init_id) {
            return;
        }

        $('.pay_description').prop('hidden', true);
        $('#desc_' + id).prop('hidden', false);

        if (payment_method == "Cash on delivery") {
            if ($('.make-payment').html != 'Place Order')
                payment_button('Place Order');
        } else if (payment_method == "Paypal") {
            $('.make-payment').attr('hidden', 'hidden');
            $('#paypal-button-container').removeAttr('hidden');
            $('.make-payment').click();
        } else {
            if ($('.make-payment').html != 'Make Payment')
                payment_button('Make Payment');
        }
        // console.log($('.make-payment').is(":disabled"));
    }

    function payment_button(method_name) {
        $('.make-payment').attr('disabled', false);
        $('.make-payment').removeAttr('hidden');
        $('.make-payment').html(method_name);
        $('#paypal-button-container').attr('hidden', 'hidden');
    }

    $(document).ready(function() {
        "use strict";
        // Checkout Wizard
        var checkoutWizard = $(".checkout-tab-steps"),
            checkoutValidation = checkoutWizard.show();
        if (checkoutWizard.length > 0) {
            $(checkoutWizard).steps({
                headerTag: "h6",
                bodyTag: "fieldset",
                transitionEffect: "fade",
                titleTemplate: '<span class="step">#index#</span> #title#',
                enablePagination: false,
                onStepChanging: function(event, currentIndex, newIndex) {
                    // allows to go back to previous step if form is
                    if (currentIndex > newIndex) {
                        return true;
                    }

                    // Needed in some cases if the user went back (clean up)
                    if (currentIndex < newIndex) {
                        // To remove error styles
                        checkoutValidation.find(".body:eq(" + newIndex + ") label.error").remove();
                        checkoutValidation.find(".body:eq(" + newIndex + ") .error").removeClass("error");
                    }
                    // check for valid details and show notification accordingly

                    // if (newIndex == 1 && getCookie('customer') == 'old')
                    if (newIndex == 1 )
                    {
                        // console.log('cookie');
                        @if(!auth()->guard('customer')->check() || $customer['address']==FALSE)

                        document.checkout_form.fname.value = getCookie("fname");
                        document.checkout_form.mnumber.value = getCookie("mnumber");
                        document.checkout_form.amnumber.value = getCookie("amnumber");
                        document.checkout_form.email.value = getCookie("email");
                        document.checkout_form.apt_number.value = getCookie("apt_number");
                        document.checkout_form.landmark.value = getCookie("landmark");
                        document.checkout_form.city.value = getCookie("city");
                        document.checkout_form.pincode.value = getCookie("pincode");
                        document.checkout_form.state.value = getCookie("state");
                        document.checkout_form.addresstype.value = getCookie("addresstype");

                        @endif
                    }

                    // if (currentIndex === 1 && Number($(".form-control.required").val().length) < 1) {
                    //     toastr.warning('Error', 'Please Enter Valid Details', {
                    //         "positionClass": "toast-bottom-right"
                    //     });
                    // }

                    checkoutValidation.validate().settings.ignore = ":disabled,:hidden";
                    return checkoutValidation.valid();
                },
                onStepChanged: function(event, currentIndex, priorIndex) {
                    // console.log($('.paymentMethod').is(":visible"));
                    event.preventDefault();
                    $('.paymentMethod').click(function() {
                        payment_selection($(this).attr('id'));
                    });

                },
                onFinished: function(event, currentIndex) {

                    checkoutValidation.validate().settings.ignore = ":disabled,:hidden";
                    if (checkoutValidation.valid()) {

                        track_events('confirm_order', null, slink);

                        var $this = $('.make-payment');
                        // var $this = $('.delivery-address');
                        var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> loading...';

                        @if(!auth()->guard('customer')->check()  || $customer['address']==FALSE)
                        setCookie("fname", document.checkout_form.fname.value);
                        setCookie("mnumber", document.checkout_form.mnumber.value);
                        setCookie("amnumber", document.checkout_form.amnumber.value);
                        setCookie("email", document.checkout_form.email.value);
                        setCookie("apt_number", document.checkout_form.apt_number.value);
                        setCookie("landmark", document.checkout_form.landmark.value);
                        setCookie("city", document.checkout_form.city.value);
                        setCookie("pincode", document.checkout_form.pincode.value);
                        setCookie("state", document.checkout_form.state.value);
                        setCookie("addresstype", document.checkout_form.addresstype.value);
                        // setCookie("customer",'old');
                        @endif

                        if ($this.html() !== loadingText) {
                            $this.data('original-text', $this.html());
                            $this.html(loadingText);
                            cart = JSON.parse(localStorage.getItem('shoppingCart'));
                            if (cart.length > 0) {
                                for (var item in cart) {
                                    if ($("#product_notes_" + cart[item].id).val() != "") {
                                        var pn = $("#product_notes_" + cart[item].id).val();
                                    } else {
                                        var pn = "";
                                    }
                                    $("form").append("<input type='hidden' id='product_notes_" + cart[item].id + "' name='product_notes[" + cart[item].id + "]' value='" + pn + "'>");
                                }
                            }
                            // console.log($('form').serialize());
                            formSubmit($('form').serialize(), $this);
                        }
                        // setTimeout(function() {
                        // $this.html($this.data('original-text'));
                        // }, 2000);
                    }

                    // var form = $(this);
                    // Submit form input
                    // form.submit();
                },
            });

             //apply coupon code
            $('#apply_coupon').click(function(){applyCouponCode()});
            $('#coupon_alert_close').click(function(){CouponCodeInput()})

            // to move to next step on place order and save address click
            $(".place-order").on("click", function() {
                @if(!auth()->guard('customer')->check() && !$guest_user)
                    $('.loginButton').click()
                @else
                    if($(this).attr('id') == "enquiry_btn"){
                        $(".delivery-address").empty().html("SUBMIT");
                    }
                    $(".checkout-tab-steps").steps("next", {});
                @endif
            });

            // check if user has entered valid cvv
            $(".delivery-address").on("click", function() {
                        if(document.getElementById("delivery_address").innerHTML.trim() == "NEXT")
                            $(".checkout-tab-steps").steps("next", {});
                        else
                            $(".checkout-tab-steps").steps("finish", {});
            });

            $(".make-payment").on("click", function() {
                $(".checkout-tab-steps").steps("finish", {});
            });

            $('#checkout_login_link').click(function(){
                $('.loginButton').click();
            });

        }

        // checkout quantity counter
        var quantityCounter = $(".quantity-counter"),
            CounterMin = 1,
            CounterMax = 10000;
        if (quantityCounter.length > 0) {
            quantityCounter.TouchSpin({
                min: CounterMin,
                max: CounterMax
            }).on('touchspin.on.startdownspin', function() {
                var $this = $(this);
                $('.bootstrap-touchspin-up').removeClass("disabled-max-min");
                var product_min_qty = Number($this.data('min_qty'));
                if ($this.val() < product_min_qty) {
                    if($(this).siblings().find('.bootstrap-touchspin-down').hasClass('disabled-max-min') == false) {
                        place_order_btn_disabled++;
                        enquiry_btn_disabled++;
                        $("#enquiry_btn").addClass('disabled');
                        $("#enquiry_btn").attr("disabled", "disabled").off('click');
                        $("#place_order_btn").addClass('disabled');
                        $("#place_order_btn").attr("disabled", "disabled").off('click');
                        var product_id = Number($this.data('id'));
                        $("#product_quantity_alert_"+ product_id).show();
                        $(this).parent().parent().siblings().find('.minimum_product_quantity').text(product_min_qty);
                        // console.log($this.val(),"less",place_order_btn_disabled,enquiry_btn_disabled);
                    }
                }
                else{
                    obj.setCountForItem(Number($this.data('id')), $this.val());
                    $("#product_qty_" + $this.data('id')).val($this.val());
                    if (Number($this.data('discounted_price')) == 0) {
                        $('.item-price-' + $this.data('id')).html('{!! $currency !!}' + $this.val() * Number($this.data('price')));
                    } else {
                        $('.item-price-' + $this.data('id')).html('{!! $currency !!}' + $this.val() * Number($this.data('discounted_price')) + '&nbsp;<span class="text-light"><s>{!! $currency !!}' + $this.val() * Number($this.data('price')) + '</span></s>');
                    }
                }
                if ($this.val() == 1) {
                    $(this).siblings().find('.bootstrap-touchspin-down').addClass("disabled-max-min");
                }
                checkCoupon();


            }).on('touchspin.on.startupspin', function() {
                var $this = $(this);
                $('.bootstrap-touchspin-down').removeClass("disabled-max-min");
                if ($this.val() == CounterMax) {
                    $(this).siblings().find('.bootstrap-touchspin-up').addClass("disabled-max-min");
                }
                var product_min_qty = Number($this.data('min_qty'));
                if ($this.val() <= product_min_qty && place_order_btn_disabled > 0 && enquiry_btn_disabled > 0) {
                    place_order_btn_disabled--;
                    enquiry_btn_disabled--;
                    // console.log($this.val(),"more",place_order_btn_disabled,enquiry_btn_disabled);
                }
                if ($this.val() >= product_min_qty) {
                    var product_id = Number($this.data('id'));
                    $("#product_quantity_alert_"+ product_id).hide();
                    if(place_order_btn_disabled == 0){
                        $("#place_order_btn").removeClass('disabled');
                        $("#place_order_btn").attr("disabled", false).on('click');
                    }
                    if(place_order_btn_disabled == 0){
                        $("#enquiry_btn").removeClass('disabled');
                        $("#enquiry_btn").attr("disabled", false).on('click');
                    }
                    obj.setCountForItem(Number($this.data('id')), $this.val());
                    $("#product_qty_" + $this.data('id')).val($this.val());
                    if (Number($this.data('discounted_price')) == 0) {
                        $('.item-price-' + $this.data('id')).html('{!! $currency !!}' + $this.val() * Number($this.data('price')));
                    } else {
                        $('.item-price-' + $this.data('id')).html('{!! $currency !!}' + $this.val() * Number($this.data('discounted_price')) + '&nbsp;<span class="text-light"><s>{!! $currency !!}' + $this.val() * Number($this.data('price')) + '</span></s>');
                    }
                }
                checkCoupon();


            }).on('change', function() {
                var $this = $(this);
                var product_min_qty = Number($this.data('min_qty'));
                if ($this.val() < product_min_qty) {
                    $("#enquiry_btn").addClass('disabled');
                    $("#enquiry_btn").attr("disabled", "disabled").off('click');
                    $("#place_order_btn").addClass('disabled');
                    $("#place_order_btn").attr("disabled", "disabled").off('click');
                    var product_id = Number($this.data('id'));
                    $("#product_quantity_alert_"+ product_id).show();
                    $(this).parent().parent().siblings().find('.minimum_product_quantity').text(product_min_qty);
                    $("#product_qty_" + $this.data('id')).val(product_min_qty);
                    obj.setCountForItem(Number($this.data('id')), product_min_qty);
                } else if ($this.val() > CounterMax) {
                    obj.setCountForItem(Number($this.data('id')), CounterMax);
                } else {
                    obj.setCountForItem(Number($this.data('id')), $this.val());
                    if (Number($this.data('discounted_price')) == 0) {
                        $('.item-price-' + $this.data('id')).html('{!! $currency !!}' + $this.val() * Number($this.data('price')));
                    } else {
                        $('.item-price-' + $this.data('id')).html('{!! $currency !!}' + $this.val() * Number($this.data('discounted_price')) + '&nbsp;<span class="text-light"><s>{!! $currency !!}' + $this.val() * Number($this.data('price')) + '</span></s>');
                    }
                }
            });
        }

        // remove items from wishlist page
        $(".remove-wishlist , .move-cart").on("click", function() {
            $(this).closest(".checkout_item").remove();
            obj.removeItemFromCart(Number($(this).data('id')));
            obj.totalCart();
            load_data_Cart();
            track_events("remove_product", $(this).data('id'), slink);

        })


    });

    // Set count from item
    obj.setCountForItem = function(id, qty) {
        // console.log("set count "+ id,qty);
        for (var i in cart) {
            if (cart[i].id === id) {
                cart[i].qty = qty;
                break;
            }
        }
        saveCart();
        obj.totalCart();
    };
    // Remove item from cart
    obj.removeItemFromCart = function(id) {
        for (var item in cart) {
            if (cart[item].id === id) {
                //   cart[item].qty --;
                //   if(cart[item].qty === 0) {
                cart.splice(item, 1);
                //   }
                //   break;
            }
        }
        saveCart();
    }

    // Remove all items from cart
    obj.removeItemFromCartAll = function(name) {
        for (var item in cart) {
            if (cart[item].name === name) {
                cart.splice(item, 1);
                break;
            }
        }
        saveCart();
    }
    // Count cart
    obj.totalCount = function() {
        var totalCount = 0;
        for (var item in cart) {
            totalCount += cart[item].qty;
        }
        return totalCount;
    }
    // Total cart
    obj.totalCart = function() {
        var totalCart = 0;
        var discountCart = 0;

        for (var item in cart) {
            if (cart[item].discounted_price != 0) {
                discountCart += cart[item].price * cart[item].qty - cart[item].discounted_price * cart[item].qty;
            }
                totalCart += cart[item].price * cart[item].qty;
                //console.log(totalCart);
        }

        $('.total-amt, .mrp-amt, .items-amt').html('{!! $currency !!}' + Number(totalCart.toFixed(2)));
        $('.discount-amt').html('- {!! $currency !!}' + Number(discountCart.toFixed(2)));
        discount=discountCart;

        if (cart.length > 0) {
            $(".cart-item-count").html(cart.length);
            $(".price-items").html("Price of " + cart.length + " items");
        } else {
            $(".cart-item-count").html("");
            $(".ecommerce-application").empty();
            $(".ecommerce-application").html("<h2 class='text-light text-center'><i class='pt-1 ficon feather icon-shopping-cart fa-3x'></i><br>No item in cart<br><a class='btn btn-success m-2' href='{{$homeLink}}'>Go to store</a></h2>");
        }
        return Number(totalCart.toFixed(2));
    }
    // Clear cart
    obj.clearCart = function() {
        cart = [];
        saveCart();
    }
    // Save cart
    function saveCart() {
        const now = new Date();
        var time = now.getTime() + 1000 * 60 * 60 * 2;
        localStorage.setItem('shoppingCart', JSON.stringify(cart));
        localStorage.setItem('expiretime', JSON.stringify(time));
    }
    var currency_code = "{{html_entity_decode($currency)}}";
    var currency = String(currency_code);

    function loadCart() {
        cart = JSON.parse(localStorage.getItem('shoppingCart'));
        var cartItemCount = count = cart.length;
        $(".checkout-items").empty();
        if (cart.length > 0) {
            $(".cart-item-count").html(count);
            var products = [];
            var catalogs = [];
            for (var item in cart) {
                if (cart[item].discounted_price == 0) {
                    var html_1='<div class="row bg-white shadow-lg rounded mb-1 checkout_item" style="min-height:200px"><div class="col-lg-3 col-md-3 col-sm-12 h-100 d-flex justify-content-center rounded p-1"><div class="item-img text-center"><a href="#"><img src="' + cart[item].img + '" style="min-height:180px;" class="mw-100 h-100"></a></div></div><div class="col-lg-9 col-md-9 col-sm-12 h-100"><div class="row h-50 w-100 m-0"><div class="col-lg-6 col-md-6 col-sm-6 h-100 p-1"><div class="item-name"><a href="javascript:void(0);" data-id="' + cart[item].id + '" data-catalog_id="' + cart[item].catalogid + '" onclick="catalogDetailsPage(this)" >' + cart[item].name + '</a><span></span><div class="item-company"><div class="badge badge-pill badge-glow badge-primary mr-1 d-none">' + cart[item].catagory + ' </div></div></div><div class="item-quantity"><div><p class="quantity-title">Quantity</p></div><div class="input-group quantity-counter-wrapper"><input type="text" class="quantity-counter" data-id="' + cart[item].id + '" data-min_qty="' + cart[item].min_qty + '" data-price="' + cart[item].price + '" data-discounted_price="' + cart[item].discounted_price + '"  value="' + cart[item].qty + '"></div></div><div id="product_quantity_alert_' + cart[item].id + '" class="alert alert-danger mt-1" role="alert" style="display:none;"><i class="feather icon-info mr-1 align-middle"></i><span>Please select a minimum of <span class="minimum_product_quantity"></span> to proceed with the order.</span></div></div><div class="col-lg-6 col-md-6 col-sm-6 h-100 p-1"><div class="item-wrapper text-right"><div class="item-rating"><div class="badge badge-primary badge-md hidden">4<i class="feather icon-star ml-25"></i></div></div><div class="item-cost"><h6 class="item-price item-price-' + cart[item].id + '">{!! $currency !!}' + cart[item].qty * cart[item].price + ' </h6><p class="shipping hidden"><i class="feather icon-shopping-cart"></i>Free Shipping </p></div></div></div></div><div class="row h-50 w-100 m-0"><div class="col-lg-9 col-md-9 col-sm-12 h-100 p-1"><div class="input-group"><textarea placeholder="Enter your comments / questions here" id="product_notes_' + cart[item].id + '" class="form-control" aria-label="With textarea product" maxlength="1000"></textarea></div></div><div class="col-lg-3 col-md-3 col-sm-12 h-100"><button type="button" class="btn btn-danger wishlist remove-wishlist w-100" data-id="' + cart[item].id + '"><i class="feather icon-x align-middle"></i>Remove</button><div class="cart remove-wishlist hidden"><i class="fa fa-heart-o mr-25"></i>Wishlist </div></div></div></div></div>';
                } else {
                    var html_1='<div class="row bg-white shadow-lg rounded mb-1 checkout_item" style="min-height:200px"><div class="col-lg-3 col-md-3 col-sm-12 h-100 d-flex justify-content-center rounded p-1"><div class="item-img text-center"><a href="#"><img src="' + cart[item].img + '" style="min-height:180px;" class="mw-100 h-100"></a></div></div><div class="col-lg-9 col-md-9 col-sm-12 h-100"><div class="row h-50 w-100 m-0"><div class="col-lg-6 col-md-6 col-sm-6 h-100 p-1"><div class="item-name"><a href="javascript:void(0);" data-id="' + cart[item].id + '" data-catalog_id="' + cart[item].catalogid + '" onclick="catalogDetailsPage(this)" >' + cart[item].name + '</a><span></span><div class="item-company"><div class="badge badge-pill badge-glow badge-primary mr-1 d-none">' + cart[item].catagory + ' </div></div></div><div class="item-quantity"><div><p class="quantity-title">Quantity</p></div><div class="input-group quantity-counter-wrapper"><input type="text" class="quantity-counter" data-id="' + cart[item].id + '" data-min_qty="' + cart[item].min_qty + '"  data-price="' + cart[item].price + '" data-discounted_price="' + cart[item].discounted_price + '"  value="' + cart[item].qty + '"></div></div><div id="product_quantity_alert_' + cart[item].id + '" class="alert alert-danger mt-1" role="alert" style="display:none;"><i class="feather icon-info mr-1 align-middle"></i><span>Please select a minimum of <span class="minimum_product_quantity"></span> to proceed with the order.</span></div></div><div class="col-lg-6 col-md-6 col-sm-6 h-100 p-1"><div class="item-wrapper text-right"><div class="item-rating"><div class="badge badge-primary badge-md hidden">4<i class="feather icon-star ml-25"></i></div></div><div class="item-cost"><h6 class="item-price item-price-' + cart[item].id + '">{!! $currency !!}' + cart[item].qty * cart[item].discounted_price + ' &nbsp;<span class="text-light"><s>{!! $currency !!}' + cart[item].qty * cart[item].price + '</s></span></h6><p class="shipping hidden"><i class="feather icon-shopping-cart"></i>Free Shipping </p></div></div></div></div><div class="row h-50 w-100 m-0"><div class="col-lg-9 col-md-9 col-sm-12 h-100 p-1"><div class="input-group"><textarea placeholder="Enter your comments / questions here" id="product_notes_' + cart[item].id + '" class="form-control" aria-label="With textarea product" maxlength="1000"></textarea></div></div><div class="col-lg-3 col-md-3 col-sm-12 h-100 "><button type="button" class="btn btn-danger wishlist remove-wishlist w-100" data-id="' + cart[item].id + '"><i class="feather icon-x align-middle"></i>Remove</button><div class="cart remove-wishlist hidden"><i class="fa fa-heart-o mr-25"></i>Wishlist </div></div></div></div></div>';
                }
                var html = html_1;
                $(".checkout-items").append(html);
                products.push(cart[item].id);
                var id = cart[item].id,
                    qty = cart[item].qty,
                    catalogid = cart[item].catalogid;
                $("form").append("<input type='hidden' id='product_qty_" + id + "' name='products_qty[" + id + "]' value='" + qty + "'>");
                for (var catalog in cart) {
                    if (catalogs.indexOf(catalogid) == -1)
                        catalogs.push(catalogid);
                }
                obj.totalCart();
            }
            $("form").append("<input type='hidden' name='products' value='" + products + "'>");
            $("form").append("<input type='hidden' name='catalogs' value='" + catalogs + "'>");
            $("form").append("<input type='hidden' name='currency' value='" + currency + "'>");
            grandTotal()

        } else {
            $(".cart-item-count").html("");
            $(".ecommerce-application").empty();
            $(".ecommerce-application").html("<h2 class='text-light text-center'><i class='pt-1 ficon feather icon-shopping-cart fa-3x'></i><br>No item in cart<br><a class='btn btn-success m-2' href='{{$homeLink}}'>Go to store</a></h2>");

        }
    }

    function load_data_Cart() {
        $('input[name="products"]').remove();
        $('input[name="catalogs"]').remove();
        $('input[name^=products_qty]').remove();

        cart = JSON.parse(localStorage.getItem('shoppingCart'));
        var cartItemCount = count = cart.length;
        // console.log(cart)
        if (cart.length > 0) {
            var products = [];
            var catalogs = [];
            for (var item in cart) {
                products.push(cart[item].id);
                var id = cart[item].id,
                    qty = cart[item].qty,
                    catalogid = cart[item].catalogid;
                $("form").append("<input type='hidden' id='product_qty_" + id + "' name='products_qty[" + id + "]' value='" + qty + "'>");
                for (var catalog in cart) {
                    if (catalogs.indexOf(catalogid) == -1)
                        catalogs.push(catalogid);
                }
                obj.totalCart();
            }
            $("form").append("<input type='hidden' name='products' value='" + products + "'>");
            $("form").append("<input type='hidden' name='catalogs' value='" + catalogs + "'>");
            $("form").append("<input type='hidden' name='currency' value='" + currency + "'>");

            checkCoupon();


        } else {
            $(".cart-item-count").html("");
            $(".ecommerce-application").empty();
            $(".ecommerce-application").html("<h2 class='text-light text-center'><i class='pt-1 ficon feather icon-shopping-cart fa-3x'></i><br>No item in cart<br><a class='btn btn-success m-2' href='{{$homeLink}}'>Go to store</a></h2>");

        }
    }

    if (localStorage.getItem("shoppingCart") != null) {
        const now = new Date();
        var now_time = now.getTime();
        var expire_time = localStorage.getItem("expiretime");
        // console.log(now_time);
        // console.log(expire_time);
        if (now_time > expire_time) {
            // console.log("Exceeded");
            obj.removeItemFromCartAll();
            obj.totalCart();
        } else {
            // console.log(" NOt Exceeded");
            loadCart();
        }
    } else {
        obj.totalCart();
    }

    var url;
    var order_id;

    function formSubmit(data, $this) {
        data = data + '&payment_method=' + payment_method;

        if(document.getElementById("delivery_address").innerHTML.trim() == "NEXT"){
            if (payment_method != 'Paypal') {
                $('.make-payment').attr('disabled', 'disabled');
                var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> loading...';
                $('.make-payment').html(loadingText);
            }

            if (payment_method == 'UPI') {
                data = data + '&q=store';
            }

            // if (payment_method != 'Cash on delivery') {
            $.ajax({
                url: "{{route('orders.payment')}}",
                type: "POST",
                data: data,
                success: function(response) {
                    obj.clearCart();
                    payment(response, data);
                }
            });
        }
        else{
            $('.delivery-address').attr('disabled', 'disabled');
            var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> loading...';
            $('.delivery-address').html(loadingText);

            $.ajax({
                url: "{{route('enquiry.store')}}",
                type: "POST",
                data: data,
                success: function(response) {
                    obj.clearCart();
                    url = window.location.origin + '/ec/' + response.key + '/' + response.enquiry_id;
                    window.location = url;
                }
            });
        }
    }

    var today = new Date();
    var expiry = new Date(today.getTime() + 30 * 24 * 3600 * 1000); // plus 30 days

    function setCookie(name, value) {
        document.cookie = name + "=" + escape(value) + "; path=/; expires=" + expiry.toGMTString();
    }

    function getCookie(name) {
        var re = new RegExp(name + "=([^;]+)");
        var value = re.exec(document.cookie);
        // console.log(value);
        return (value != null) ? unescape(value[1]) : null;
    }

    function catalogDetailsPage(that) {
        var product_id = that.getAttribute("data-id");
        var catalog_id = that.getAttribute("data-catalog_id");
        var url = '{{ route('campaigns.publishedCatalogDetails',[":product_id",":catalog_id"]) }}';
        url = url.replace(':product_id', product_id);
        url = url.replace(':catalog_id', catalog_id);
        // console.log(url);
        window.location.href = url;
    }

    // var x = document.getElementsByClassName("navbar-wrapper")[0].clientHeight;
    // $(".content-wrapper").css('margin-top', x);

    window.onload = function() {
        url = window.location.pathname;
        slink = url.slice(url.lastIndexOf('/') + 1);
        track_events('checkout_page', null, slink);
    };

    function payment(response, data) {
        // customer_data = data;
        data = data + "&store_order_id=" + response.store_order_id;
        if (payment_method == "Razorpay") {
            var order_id = response['order_id'];
            var options = {
                "key": response['razorpayId'], // Enter the Key ID generated from the Dashboard
                "amount": response['amount'], // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
                "currency": response['currency'],
                "name": response['name'],
                "description": response['description'],
                "order_id": response['orderId'], //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
                "handler": function(response) {
                    // console.log("handler", response);
                    data = data + "&rzp_paymentid=" + response.razorpay_payment_id + "&rzp_orderid=" + response.razorpay_order_id + "&rzp_signature=" + response.razorpay_signature + "&status=success";
                    page_redirection(data);
                },
                "prefill": {
                    "name": response['name'],
                    "email": response['email'],
                    "contact": response['contactNumber']
                },
                "notes": {
                    "address": response['address']
                },
                "theme": {
                    "color": "#528FF0"
                }
            };

            var rzp1 = new Razorpay(options);
            rzp1.open();

            rzp1.on('payment.failed', function(response) {
                data = data + "&status=failure" + "&rzp_paymentid=" + response.metadata.payment_id + "&rzp_orderid=" + response.metadata.order_id + "&response=" + response;
                page_redirection(data);
            });
        }
        else if (payment_method == "Paypal") {
            loaded = false;
            var s = document.createElement('script');
            s.type = "text/javascript";
            s.setAttribute('src', "https://www.paypal.com/sdk/js?client-id=" + response.client_id + "&components=buttons&currency=" + response.currency);
            s.addEventListener("load", function() {
                paypal.Buttons({
                    enableStandardCardFields: true,
                    style: {
                        layout: 'horizontal',
                        color: 'blue',
                        shape: 'rect',
                        label: 'paypal',
                        size: 'responsive',
                    },
                    createOrder: function(data, actions) {
                        return actions.order.create({
                            payer: {
                                name: {
                                    given_name: response.name,
                                },
                                email_address: response.email,
                                phone: {
                                    phone_type: "MOBILE",
                                    phone_number: {
                                        national_number: response.mobileno
                                    }
                                }
                            },
                            purchase_units: [{
                                amount: {
                                    value: response.amount,
                                },
                            }]
                        });
                    },
                    onApprove: function(data, actions) {
                        // This function captures the funds from the transaction.
                        return actions.order.capture().then(function(details) {
                            // console.log('approved', details);
                            window.alert('approved');
                            // This function shows a transaction success message to your buyer.
                            // alert('Transaction completed by ' + details.payer.name.given_name);
                        });
                    },
                    onCancel: function(data) {
                        // console.log('cancel', data);
                        window.alert('cancel');
                        // Show a cancel page, or return to cart
                    },
                    onError: function(err) {
                        // console.log('error', err);
                        window.alert('error');
                        // For example, redirect to a specific error page
                        // window.location.href = "/your-error-page-here";
                    },
                }).render('#paypal-button-container');
            });
            document.body.appendChild(s);
        }
        else if (payment_method == "UPI") {
            // console.log(response);
            // $('#checkout-payment').children().hide();
            // $('#checkout-payment-upi').prop('hidden', false);
            // $('#checkout-payment-upi').append(response);
            url = window.location.origin + '/pay/' + response.key;
            window.location = url;
        }
        else if (payment_method == "Cash on delivery") {
            page_redirection(data);
        }
    }

    function page_redirection(data) {
        $.ajax({
            url: "{{route('transaction.store')}}",
            type: "POST",
            data: data,
            success: function(response) {
                order_id = response.order_id;
                url = window.location.origin + '/pc/' + response.key + '/' + response.order_id;
                window.location = url;
            }
        });
    }


    function applyCouponCode (){

        var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> loading...';
        if($('#coupon_code').val()=='')
            return $('#coupon_error_message').html('Please Enter the coupon code');
            $('#apply_coupon').html(loadingText);

        if(coupon_code == $('#coupon_code').val() && !coupon_applied )
        {
            coupon_applied=true;
            coupon_code=$('#coupon_code').val();
            load_data_Cart()
            $('#apply_coupon').html('Apply Coupon Code');
            CouponCodeBadge(coupon_code)
            $('#coupon_error_message').html('');
            $('#coupon_success_message').html('Coupon code applied - '+coupon_desc);
            return;
        }
        $.ajax({
            url:"{{ route('coupon.apply') }}",
            type:"POST",
            data:{
                'X-CSRF-TOKEN': "{{ csrf_token() }}",
                code:$('#coupon_code').val(),
                amount : obj.totalCart() - discount,
                shareable_link:"{{$campaign->public_link}}" },
            success:function(res){
                if(res.status=='error'){
                    $('#apply_coupon').html('Apply Coupon Code')
                    $('#coupon_success_message').html('')
                    $('#coupon_error_message').html(res.msg);
                    if(res.login==true)
                        $('.loginButton').click()
                }
                if(res.status=='min_cart_value'){
                    $('#apply_coupon').html('Apply Coupon Code')
                    $('#coupon_success_message').html('')
                    $('#coupon_error_message').html("You have to purchase more than {!! $currency !!}"+res.min_cart_value+ " to use this coupon" );
                }
                if(res.status=='success'){
                        coupon_applied=true
                        min_cart_value= res.min_cart_value
                        coupon_amount=res.discount_amount
                        coupon_unit=res.unit
                        coupon_id=res.id
                        coupon_desc=res.desc
                        coupon_code=$('#coupon_code').val()
                        load_data_Cart()
                        $('#apply_coupon').html('Apply Coupon Code');
                        CouponCodeBadge(coupon_code)
                        $('#coupon_error_message').html('')
                        $('#coupon_success_message').html(res.msg+coupon_desc)
                }
            }
        })
    }

    function checkCoupon(){

        var amount=obj.totalCart()- discount;
        var discount_amount=0

        if(coupon_applied)
        {
                if(min_cart_value > (amount))
                {
                    CouponCodeInput()
                    coupon_applied=false;
                    discount_amount=0
                    discount=discount+discount_amount
                    $('.coupon-discount-amt').html('- {!! $currency !!}' + Number(discount.toFixed(2)));
                    $('#coupon_code').val('');
                    $('#coupon_success_message').html('');
                    $('#coupon_error_message').html('');
                    $('#coupon_error_message').html("You have to purchase more than {!! $currency !!}"+min_cart_value+ " to use this coupon");

                }else{
                        if(coupon_unit=='percentage'){
                            discount_amount= (amount/100) * coupon_amount
                            // amount=obj.totalCart() - discount_amount;
                            $('.coupon-discount-amt').html('- ('+coupon_amount+'%) {!! $currency !!}' + Number(discount_amount.toFixed(2)));
                        }

                        if(coupon_unit=='amount'){
                            discount_amount=coupon_amount;
                            // amount=obj.totalCart() - coupon_amount;
                            $('.coupon-discount-amt').html('- {!! $currency !!}' + Number(coupon_amount.toFixed(2)));
                        }


                    $('#coupon_id').val(coupon_id);
                    $('#coupon_code').val(coupon_code);
                    $("#coupon_discount_amount").val(discount_amount);
                    $("#coupon_code_applied").val('true');
                }
        }else
        {
            $('#coupon_code').val('');
            $("#coupon_code_applied").val('false');
            $("#coupon_discount_amount").val('');
            $('#coupon_id').val('');
        }

        coupon_discount=discount_amount
        grandTotal()

    }

    function CouponCodeBadge(coupon_code){
        $('#coupon_code').hide()
        $('#apply_coupon').hide()
        $('#coupon_badge').html(coupon_code)
        $('#coupon_alert').prop('hidden',false)
    }

    function CouponCodeInput(){
        coupon_applied=false;
        coupon_discount=0
        $('.coupon-discount-amt').html('- {!! $currency !!}' + Number(coupon_discount.toFixed(2)));
        grandTotal()
        $('#coupon_success_message').html('');
        $('#coupon_badge').html('')
        $('#coupon_alert').prop('hidden',true)
        $('#coupon_code').show()
        $('#apply_coupon').show()
    }


    function grandTotal()
    {
        amount=obj.totalCart()-discount-coupon_discount
        $('.grand-amt').html('{!! $currency !!}' + Number(amount.toFixed(2)));
    }

    function isNumberKey(event) {
        // Allow only backspace and delete
        if (event.keyCode == 8 ) {
            // let it happen, don't do anything
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.keyCode < 48 || event.keyCode > 57 ) {
                event.preventDefault();
            }
        }
    }


</script>

</script>
@endsection
