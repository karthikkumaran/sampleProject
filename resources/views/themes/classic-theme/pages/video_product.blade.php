@extends('themes.'.$meta_data['themes']['name'].'.layouts.public')
@section ('styles')
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">

<link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/extensions/nouislider.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/ui/prism.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/forms/select/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/core/colors/palette-gradient.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/plugins/extensions/noui-slider.min.css') }}">

@endsection
@php
$agent=new \Jenssegers\Agent\Agent();
if($userSettings->is_start == 1) {
if(!empty($userSettings->customdomain)){
$homeLink = '//'.$userSettings->customdomain;
}else if(!empty($userSettings->subdomain)) {
$homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
}else{
$homeLink = route('mystore.published',[$userSettings->public_link]);
}
}else{
$homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
}

        if($campaign->is_start == 1) {
                        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
                        $catalogLink = route('campaigns.published',$campaign->public_link);
                        $videoLink = route('campaigns.publishedVideo',$campaign->public_link);
                        $shoppableLink = route('mystore.publishedVideo',$userSettings->public_link);
                        $showallLink = route('campaigns.showVideo',$campaign->public_link);
                    }else{
                        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
                        $catalogLink = route('admin.campaigns.preview',$campaign->preview_link);
                        $videoLink = route('campaigns.publishedVideo',$campaign->preview_link);
                        $shoppableLink = route('mystore.publishedVideo',$userSettings->preview_link);
                        $showallLink = route('campaigns.showVideo',$campaign->preview_link);
                    }

if(empty($meta_data['settings']['currency']))
$currency = "₹";
else
if($meta_data['settings']['currency_selection'] == "currency_symbol")
$currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
else
$currency = $meta_data['settings']['currency']
@endphp
@section('content')
@include('themes.'.$meta_data['themes']['name'].'.modals.details_product')
@include('themes.'.$meta_data['themes']['name'].'.partials.public_banner')

<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb border-0">
                        @if( (request()->is('s/*') || request()->is('admin/storePreview/*')) == false)
                        <li class="breadcrumb-item"><a class="text-green" href="{{$homeLink}}">Home</a></li>
                        <li class="breadcrumb-item">Shoppable Videos</li>
                        <li class="breadcrumb-item">{!! $campaign->name == "Untitled"?"&nbsp;":$campaign->name !!}</li>
                        @else
                        <li class="breadcrumb-item">{!! $campaign->name == "Untitled"?"&nbsp;":$campaign->name !!}</li>
                        @endif
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="ecommerce-application h-100">
    <div class="content-detached content-right">
        <div class="content-body ml-0 ">
            <!-- Ecommerce Content Section Starts -->
            <section id="ecommerce-header">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ecommerce-header-items">
                            <div class="result-toggler">


                                <!-- <div class="search-results">
                                    <span id="productssearchTotal"></span> results found
                                </div> -->
                            </div>
                            <div class="view-options">

                            </div>
                        </div>
                    </div>
                </div>
            </section>



            <!-- Ecommerce Products Starts -->
            <section id="ecommerce-productscatalog" class="grid-view d-block mb-4">
                <div id="catalog_prod" class="row">

                </div>
            </section>
            <!-- Ecommerce Products Ends -->
        </div>
    </div>


        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script src="{{asset('XR/assets/js/cart.js')}}"></script>

<!-- BEGIN: Page Vendor JS-->
<script src="{{ asset('XR/app-assets/vendors/js/ui/prism.min.js') }}"></script>
<script src="{{ asset('XR/app-assets/vendors/js/extensions/wNumb.js') }}"></script>
<script src="{{ asset('XR/app-assets/vendors/js/extensions/nouislider.min.js') }}"></script>
<script src="{{ asset('XR/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="{{ asset('XR/app-assets/js/core/app-menu.js') }}"></script>
<script src="{{ asset('XR/app-assets/js/core/app.js') }}"></script>
<script src="{{ asset('XR/app-assets/js/scripts/components.js') }}"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="{{ asset('XR/app-assets/js/scripts/pages/app-ecommerce-shop.js') }}"></script>
<!-- END: Page JS -->

<script>
    $(document).ready(function() {

        url = window.location.pathname;
        slink = "{{($userSettings->is_start == 1)?$campaign->public_link:$campaign->preview_link}}";

        if (url.includes('/s')) {
            track_events("store", null, slink);
        }
        track_events("video catalog", null, slink);
    });


    function tryon(pid) {
        window.location.href = window.location.href + "?sku=" + pid;
    }
    // $('img').on("error", function() {
    //   $(this).attr('src', '{{asset('XR/assets/images/placeholder.png')}}');
    //   $(this).css('width', '95%');
    //   $(this).css('max-height', '100%');
    // });
    var q = "";
    var searchData = "";
    var sort = "";
    var filter = [];
    var page = 0;
    var filterData = "";
    var preview = 0;
    var meta = @json($meta_data);
    var theme;
    if (meta) {
        if (meta['themes']) {
            if (meta['themes']['name'])
                theme = meta['themes']['name'];
        } else
            theme = "default-theme";
    } else {
        theme = "default-theme";
    }
    var page_url = window.location.pathname;
    // shareable_link=page_url.slice(page_url.lastIndexOf('/')+1);
    var shareable_link = "{{($userSettings->is_start == 1)?$campaign->public_link:$campaign->preview_link}}";

    var u = window.location.href;
    if (u.includes("preview")) {
        preview = 1;
    }

    var price_slider_min = document.getElementById('price_slider_min');
    var price_slider_max = document.getElementById('price_slider_max');
    var slider = document.getElementById('price-slider');

    getProducts(0);

    $("#CatalogProductSearch").on('input', function() {
        searchData = $(this).val();
        if (searchData.length >= 3 || searchData == "")
            //console.log(searchData);
            getProducts(0);
    });
    // $(document).on('click',"#ecommerce-productscatalog",'.pagination a', function(event) {
    //     console.log('prod')
    //     event.preventDefault();

    //     $('li').removeClass('active');
    //     $(this).parent('li').addClass('active');

    //     var myurl = $(this).attr('href');
    //     var page = $(this).attr('href').split('page=')[1];

    //     getProducts(page);
    // });


    function getProducts(page) {
        $('.loading1').show();
        q = 'q=' + searchData + '&s=' + sort + '&page=' + page + '&preview=' + preview + '&filterData=' + filterData + '&shareable_link=' + shareable_link + '&theme=' + theme + '&filter=' + JSON.stringify(filter);
        //console.log(q);
        $.ajax({
            url: "{{route('admin.campaigns.ajaxVideoData')}}" + '?' + q,
            type: "get",
            datatype: "html"
        }).done(function(data) {
            //console.log(data);
            //console.log("sucess");
            $("#catalog_prod").empty().html(data);
            //$("#product_list").append(data);
            $('.loading').hide();
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            $('.loading').hide();
            // alert('No response from server');
        });
    }

    // function clear_filter() {
    //     filter = [];
    //     $("[name=price-range]").filter("[value=all]").prop("checked", true);
    //     $("[name=category-filter-all]").filter("[value=all]").prop("checked", true);
    //     $("[name=category-filter]").prop("checked", false);
    //     $("[name=attributes-filter-all]").filter("[value=all]").prop("checked", true);
    //     $("[name=attributes-filter]").prop("checked", false);
    //     filter = [];
    //     getProducts(0);
    // }

    // function apply_filter() {
    //     filter = [];
    //     var price_count = 0;
    //     var category_count = 0;
    //     // var attributes_count = 0;
    //     var price_values_array = [];
    //     var category_values_array = [];
    //     // var attributes_values_array = [];
    //     var price = document.getElementsByName('price-range');
    //     for (i = 0; i < price.length; i++) {
    //         if (price[i].checked) {
    //             price_count++;
    //             filter.push({
    //                 type: 'price',
    //                 value: price[i].value
    //             });
    //         }
    //     }
    //     if (price_count == 0) {
    //         if ((price_slider_min.value != "") && (price_slider_max.value == "")) {
    //             filter.push({
    //                 type: 'price',
    //                 value: price_slider_min.value
    //             });
    //         } else if ((price_slider_min.value == "") && (price_slider_max.value != "")) {
    //             price_values_array.push(0);
    //             price_values_array.push(price_slider_max.value);
    //             var price_values = price_values_array.toString();
    //             filter.push({
    //                 type: 'price',
    //                 value: price_values
    //             });
    //         } else {
    //             price_values_array.push(price_slider_min.value);
    //             price_values_array.push(price_slider_max.value);
    //             var price_values = price_values_array.toString();
    //             filter.push({
    //                 type: 'price',
    //                 value: price_values
    //             });
    //         }
    //     }
    //     var catg = document.getElementsByName('category-filter');
    //     if(catg.length == 0){
    //         category_count++;
    //         filter.push({
    //             type: 'category',
    //             value: 'all'
    //         });
    //     }
    //     else{
    //         if ($('input[name="category-filter-all"]').is(":checked") == true) {
    //             category_count++;
    //             filter.push({
    //                 type: 'category',
    //                 value: $('input[name="category-filter-all"]').val()
    //             });
    //         } else {
    //             for (i = 0; i < catg.length; i++) {
    //                 if (catg[i].checked) {
    //                     category_count++;
    //                     category_values_array.push(catg[i].value);
    //                         var category_values = category_values_array.toString();
    //                 }
    //             }
    //             if (category_count > 0) {
    //                 filter.push({
    //                     type: 'category',
    //                     value: category_values
    //                 });
    //             }
    //         }
    //     }

        // var attributes = document.getElementsByName('attributes-filter');
        // if ($('input[name="attributes-filter-all"]').is(":checked") == true) {
        //     attributes_count++;
        //     filter.push({
        //         type: 'attributes',
        //         value: $('input[name="attributes-filter-all"]').val()
        //     });
        // } else {
        //     for (i = 0; i < attributes.length; i++) {
        //         if (attributes[i].checked) {
        //             attributes_count++;
        //             var attribute_id = attributes[i].dataset.attribute_id;
        //             var attribute_value = attributes[i].value;
        //             attributes_values_array.push({
        //                 'attribute_id':attribute_id,
        //                 'attribute_value':attribute_value
        //             });
        //             var attributes_values = attributes_values_array.toString();
        //         }
        //     }
        //     if (attributes_count > 0) {
        //         filter.push({
        //             type: 'attributes',
        //             value: attributes_values_array
        //         });
        //     }
        // }

    //     if ($("#mobile_close_icon").is(':visible') == true) {
    //         var s = $(".shop-content-overlay");
    //         var t = $(".sidebar-shop");
    //         t.removeClass("show");
    //         s.removeClass("show");
    //     }

    //     // console.log(filter);
    //     getProducts(0);
    // }
    // $('input[name="category-filter-all"]').on('change', function() {
    //     if ($('input[name="category-filter-all"]').is(":checked") == true) {
    //         $('input[name="category-filter"]').prop('checked', false);
    //     }
    // });

    // $('input[name="category-filter"]').on('change', function() {
    //     if ($('input[name="category-filter"]').is(":checked") == true) {
    //         $('input[name="category-filter-all"]').prop('checked', false);
    //     }
    // });

    // $('input[name="attributes-filter-all"]').on('change', function() {
    //     if ($('input[name="attributes-filter-all"]').is(":checked") == true) {
    //         $('input[name="attributes-filter"]').prop('checked', false);
    //     }
    // });

    // $('input[name="attributes-filter"]').on('change', function() {
    //     if ($('input[name="attributes-filter"]').is(":checked") == true) {
    //         $('input[name="attributes-filter-all"]').prop('checked', false);
    //     }
    // });

    // $("#price_slider_min").on('input', function() {
    //     $('input[name="price-range"]').prop('checked', false);
    //     slider.noUiSlider.set([$(this).val(), null]);
    //     var val = parseInt($(this).val());
    //     var max = parseInt($("#price_slider_max").val());
    //     if (val > max) {
    //         $('#error_min').show();
    //         $('#apply_filter').attr('disabled', true);
    //     } else {
    //         $('#error_max').hide();
    //         $('#error_min').hide();
    //         $('#apply_filter').attr('disabled', false);
    //     }
    // });

    // $("#price_slider_max").on('input', function() {
    //     $('input[name="price-range"]').prop('checked', false);
    //     slider.noUiSlider.set([null, $(this).val()]);
    //     var val = parseInt($(this).val());
    //     var min = parseInt($("#price_slider_min").val());
    //     if (val < min) {
    //         $('#error_max').show();
    //         $('#apply_filter').attr('disabled', true);
    //     } else {
    //         $('#error_max').hide();
    //         $('#error_min').hide();
    //         $('#apply_filter').attr('disabled', false);
    //     }

    // });

    // function hide_filter() {
    //     document.getElementById("filterbuttondiv").style.display = "flex";
    //     document.getElementById("filterdiv").style.display = "flex";
    //     document.getElementById("searchdiv").style.width = "{{($agent->isDesktop()||$agent->isTablet())?'90%':'85%'}}";
    //     document.getElementById("filterdiv").style.width = "{{($agent->isDesktop()||$agent->isTablet())?'10%':'15%'}}";
    //     // $("#hide_filter_btn").hide();
    //     // $("#show_filter_btn").show();
    //     $(".sidebar-detached").hide();
    //     $('.content-body').addClass('ml-0');
    // }

    // function show_filter() {
    //     document.getElementById("filterbuttondiv").style.display = "none";
    //     document.getElementById("filterdiv").style.display = "none";
    //     document.getElementById("searchdiv").style.width = "100%";
    //     document.getElementById("filterdiv").style.width = "0%";
    //     // $("#hide_filter_btn").show();
    //     // $("#show_filter_btn").hide();
    //     $(".sidebar-detached").show();
    //     $('.content-body').removeClass('ml-0');
    // }

    // $(document).ready(function() {
    //     slider.noUiSlider.on('slide', function(values, handle) {
    //         $('input[name="price-range"]').prop('checked', false);
    //         var value = values[handle];
    //         if (handle) {
    //             price_slider_max.value = value;
    //         } else {
    //             price_slider_min.value = value;
    //         }
    //     });
    // });


</script>
@endsection
