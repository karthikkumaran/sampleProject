@extends('themes.'.$meta_data['themes']['name'].'.layouts.stories')
@section('content')
@php
if($userSettings->is_start == 1) {
if(!empty($userSettings->customdomain)){
$homeLink = '//'.$userSettings->customdomain;
$checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
$catalogLink = route('campaigns.published',[$campaign->public_link]);
}else if(!empty($userSettings->subdomain)) {
$homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
$checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
$catalogLink = route('campaigns.published',[$campaign->public_link]);
}else{
$homeLink = route('mystore.published',[$userSettings->public_link]);
$checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
$catalogLink = route('campaigns.published',[$campaign->public_link]);
}
}else{
$homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
$checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
$catalogLink = route('admin.campaigns.preview',[$campaign->preview_link]);
}
$mytime = Carbon\Carbon::now();
$mytime = $mytime->timestamp;
if(empty($meta_data['settings']['currency']))
$currency = "₹";
else
if($meta_data['settings']['currency_selection'] == "currency_symbol")
$currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
else
$currency = $meta_data['settings']['currency']
@endphp
@section('content')
@include('themes.'.$meta_data['themes']['name'].'.partials.public_banner')

<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb border-0">
                        @if( (request()->is('s/*') || request()->is('admin/storePreview/*')) == false)
                        <li class="breadcrumb-item"><a href="{{$homeLink}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{$catalogLink}}">{!! $campaign->name == "Untitled"?"&nbsp;":$campaign->name !!}</a></li>
                        @else
                        <li class="breadcrumb-item"><a href="{{$catalogLink}}">{!! $campaign->name == "Untitled"?"&nbsp;":$campaign->name !!}</a></li>
                        @endif
                        <li class="breadcrumb-item">Stories</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>

    <h1>Product Stories</h1>
    <div id="stories" class="storiesWrapper Snapgram">
    </div>

</div>
@endsection

@section('scripts')
@parent
<script src="{{asset('XR/assets/js/cart.js')}}"></script>
<script>
  
  var timestamp = function() {
  var timeIndex = 0;
  var shifts = [35, 60, 60 * 3, 60 * 60 * 2, 60 * 60 * 25, 60 * 60 * 24 * 4, 60 * 60 * 24 * 10];

  var now = new Date();
  var shift = shifts[timeIndex++] || 0;
  var date = new Date(now - shift * 1000);

  return date.getTime() / 1000;
};
    @php
        $story_img = asset('XR/assets/images/placeholder.png');
        if(!empty($data) && (count($data)>0)){
            $product = $data[0]->product;
            if(count($product->photo) > 0){
                $story_img = $product->photo[0]->getUrl();
            }
        }
    @endphp
      var stories = new Zuck('stories', {
        backNative: true,
        previousTap: true,
        skin: 'Snapgram',
        autoFullScreen: false,
        avatars: true,
        list: false,
        cubeEffect: true,
        paginationArrows: false,
        localStorage: true,
        stories: [ 
          Zuck.buildTimelineItem(
            "{!! $campaign->name == "Untitled"?"&nbsp;":$campaign->name !!}", 
            "{{$story_img}}",
            "{!! $campaign->name == "Untitled"?"&nbsp;":$campaign->name !!}",
            "#",
            timestamp(),
            [
                @foreach ($data as $key => $card)
                @php
                $product = $card->product;
                if(!isset($product->id)){
                    continue;
                }
                if($product->discount != null)
                {   
                    $discount_price = $product->price * ($product->discount/100);
                    $discounted_price = round(($product->price - $discount_price),2);
                }
                if(count($product->photo) > 0){
                    $img = $product->photo[0]->getUrl();
                    $objimg = asset('XR/assets/images/placeholder.png');
                    $obj_id = "";
                    } else if(count($product->arobject) > 0){
                        if(!empty($product->arobject[0]->object))
                        if(!empty($product->arobject[0]->object->getUrl('thumb'))) {
                            $img = $product->arobject[0]->object->getUrl('thumb');
                        }
                    } else {                        
                    $img = asset('XR/assets/images/placeholder.png');
                    $objimg = asset('XR/assets/images/placeholder.png');
                    }
                    if(count($product->arobject) > 0){
                        if(!empty($product->arobject[0]->object))
                        if(!empty($product->arobject[0]->object->getUrl('thumb'))) {
                        $objimg = $product->arobject[0]->object->getUrl('thumb');
                        $obj_id = $product->arobject[0]->id;
                        }
                    }
                    if($campaign->is_start == 1) {
                        if(count($product->categories) > 0)
                        {
                            $categoryLink = route('campaigns.publishedProductByCategory',[$product->categories[0]['id'],$campaign->public_link]);
                        }
                        $catalogId = $campaign->public_link;
                        $tryonLink = route('campaigns.publishedTryOn',$campaign->public_link)."?sku=".$product->id;
                        $catalogDetailsLink = route('campaigns.publishedCatalogDetails',[$product->id,$campaign->public_link]);
                        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
                    }else{
                        if(count($product->categories) > 0)
                        {
                            $categoryLink = route('campaigns.publishedProductByCategory',[$product->categories[0]['id'],$campaign->preview_link]);
                        }
                        $catalogId = $campaign->preview_link;
                        $tryonLink = route('admin.campaigns.previewTryOn',$campaign->preview_link)."?sku=".$product->id;
                        $catalogDetailsLink = route('admin.campaigns.previewCatalogDetails',[$product->id,$campaign->preview_link]);
                        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
                    }
                @endphp
              ["{{$product->id}}", "photo", 10, "{{$img}}", "{{$img}}", "<div class='overlay'><h4 class='text-white'><b>{{$product->name ?? ''}}</b></h4><h6><span class='text-white' id='product_currency'>{!!empty($product->price)?"":$currency!!}</span>@if($product->discount != null)<span class='text-white' id='product_discount_price'>{{$discounted_price}}</span>&nbsp;<span class='text-white' id='product_price'><s>{{$product->price}}</s></span></h6><h6><span class='text-warning' id='product_discount'>{{$product->discount}}% OFF</span>@else<span class='text-white' id='product_price'>{!! $product->price ?? '&nbsp;' !!}</span>@endif</h6>@if($product->out_of_stock != null)<h4 class='text-danger'>Out of stock</h4>@else<div onclick='addProductToCart($(this));' class='cart btn btn-md btn-primary mr-0 mr-sm-1 mb-1 mb-sm-0 text-uppercase text-white' style='cursor: pointer !important;'><i class='feather icon-shopping-cart'></i> <span class='add-to-cart'  data-id='{{$product->id}}' data-name='{{$product->name ?? ''}}' data-price='{{$product->price ?? '0'}}' data-catalogid='{{$catalogId}}' data-discounted_price='{{$product->discount ? $discounted_price:'0'}}' data-catagory='{{(count($product->categories) > 0)?$product->categories[0]['name']:''}}' data-img='{{ (count($product->photo) > 0) ? $product->photo[0]->getUrl() : asset('XR/assets/images/placeholder.png')}}'>Add to cart</span><a href='{{$checkoutLink}}' target='blank' class='view-in-cart d-none text-white'>View In Cart</a></div>@endif</div>", '', false,timestamp()],
              @endforeach
            ]
          )
        ]
      });

      $( document ).ready(function() {
        $(".story:first-child").click();
      });
    </script>

@endsection