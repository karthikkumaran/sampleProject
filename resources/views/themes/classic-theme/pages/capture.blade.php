@extends('layouts.cam')
@section('styles')
@parent
  <!-- Google Fonts -->
  <meta property="og:image" content="{{$capture->photo->getUrl('thumb')}}"/>
<link href='https://fonts.googleapis.com/css?family=Capriola' rel='stylesheet'>
<link rel="stylesheet" type="text/css" href="{{ asset('XR/themes/sharescreen/css/style.css') }}">

@endsection
@section('content')

<section id="hero">
    <div class="hero-container text-center">
      <div id="main">
      <div class="card m-1">
          <div class="card-content">
              <div class="top-left p-2" style="top:auto;right:0;cursor:pointer;" data-toggle="collapse" data-parent="#socialshare" href="#share">
                <img class="sharebtn" src="{{ asset('XR/themes/sharescreen/img/share.svg') }}">
              </div>
              <div class="card-body p-1">
              <img class="img-fluid" src="{{ $capture_img }}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}'" style="width:100%;max-height: 465px;">
              </div>
          </div>
          <div class="card-footer text-muted p-1">
            <div class="row m-0 p-0">
              <div class="col-4 col-md-4 col-sm-4 col-xs-4 p-0">
                <div class="card p-1 m-0"><img src="{{ $product_img }}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}'" style="width: 75px;"></div>
                
            </div>
              <div class="col-8 col-md-8 col-sm-8 col-xs-8 p-0">
                <h2 class="text-light text-justify text-truncate pl-1 m-0 mb-1">{!! $product->name ?? "&nbsp" !!}</h2>
                <a href="{{$tryon_link}}" class="btn btn-danger btn-sm"> TRY NOW <img src="{{ asset('XR/themes/sharescreen/img/arrow.svg') }}"
                style="width: 30px;"></a>
              </div>
            </div>
          </div>
      </div>
              </div>
    </div>
    <div class="panel-group box2" id="socialshare">
      <div class="panel panel-default">
        <div id="share" class="panel-collapse collapse in box1">
          <div class="hero-container sharelink text-white">
            <a href="#" onclick='share("fb","")' class="btn btn-lg btn-social btn-facebook sharefile">
              <i class="fa fa-facebook fa-fw"></i> Share on Facebook&nbsp;
            </a><br>
           <a href="#" onclick='share("whatsapp","")' class="btn btn-lg btn-social btn-whatsapp sharefile">
              <i class="fa fa-whatsapp fa-fw"></i> Share on whatsapp
            </a><br>
            <a href="#" onclick='share("twitter","")' class="btn btn-lg btn-social btn-twitter sharefile">
              <i class="fa fa-twitter fa-fw"></i> Share on twitter&nbsp;&nbsp;&nbsp;
            </a><br>
            <a class="btn btn-lg btn-social btn-twitter sharefile" href="#"
                onclick='share("copy","")'>
                <i class="fa fa-copy fa-fw"></i> Copy Share Link&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </a><br>
          </div>
          <div class="overlay mt-0" style="height: 10%;">
            <a href="#" class="icon">
              <img src="{{ asset('XR/themes/sharescreen/img/close.png') }}" data-toggle="collapse" data-parent="#socialshare" href="#share"
                class="closelink">
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>


@endsection
@section('scripts')
@parent
<script> 
setTimeout(removeLoader, 3000);
window.addEventListener("load",function() {
        setTimeout(function() {
            window.scrollTo(0, 1);
        }, 4000);
    });
/* Get the documentElement (<html>) to display the page in fullscreen */
var elem = document.documentElement;
openFullscreen();
/* View in fullscreen */
function openFullscreen() {
  // if (elem.requestFullscreen) {
  //   elem.requestFullscreen();
  // } else 
  if (elem.mozRequestFullScreen) { /* Firefox */
    elem.mozRequestFullScreen();
  } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) { /* IE/Edge */
    elem.msRequestFullscreen();
  }
}

/* Close fullscreen */
function closeFullscreen() {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.mozCancelFullScreen) { /* Firefox */
    document.mozCancelFullScreen();
  } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) { /* IE/Edge */
    document.msExitFullscreen();
  }
}
function readmore() {
      var dots = document.getElementById("dots");
      var moreText = document.getElementById("more");
      var btnText = document.getElementById("readmore");

      if (dots.style.display === "none") {
        dots.style.display = "inline";
        btnText.innerHTML = "Read more";
        moreText.style.display = "none";
      } else { 
        dots.style.display = "none";
        btnText.innerHTML = "Read less";
        moreText.style.display = "inline";
      }
    }

    function share(share_name,msg) {
      var u="{{Request::fullUrl()}}";
    var t="I tried this jewellery on me using MirrAR Virtual TryOn. I recommend you to give it a Try. Click on the Try Now button and Allow Camera access to experience the design. @mirrarofficial";
    switch (share_name) {
        case 'fb':
            window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t),'sharer','toolbar=0,status=0,width=626,height=436');
             break;
        case 'whatsapp':
            var message = encodeURIComponent(u) + " " + t;
            // var whatsapp_url = "whatsapp://send?text=" + message;
            // window.location.href = whatsapp_url;

            var whatsapp_url = "https://wa.me/?text=" + message;
            openInNewTab(whatsapp_url);
            break;
        case 'twitter':
            window.open("https://twitter.com/intent/tweet?text="+t+"&url="+u,'sharer','toolbar=0,status=0,width=626,height=436')
            break;
        case 'copy':
            var $temp = $("<input>");
            $("#share").append($temp);
            $temp.val(u);
            $temp.select();
            document.execCommand("copy");
            $temp.remove();
            $('#share').collapse('hide');
            toastr.success('Share Link', 'Copied!');
            break;
    }
  }
</script>  
@endsection