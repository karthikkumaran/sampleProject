
@extends('themes.'.$meta_data['themes']['name'].'.layouts.store')
@section ('styles')
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
<style>
    .videoIcon{
        margin:auto;
        height:200px;
        bottom:0;
        top: 0;
        left: 0;
        right: 0;
    }
@media (max-width: 576px) {
  .videocard {
     height: 410px;
     width: 300px;
     float:left;
     display: inline-block;
  }
  .viewmore{
      font-size: 3vw !important;
  }
}
@media (min-width: 576px){
.videosize{
    height:430px;
    width:450px;
    float:left;
    display: inline-block;
    }

}
  .showall{
    width: 90px;
     height: 100px;
     border-radius: 8px;
     border: 1px solid rgb(37, 117, 252);
     background-color: rgb(240, 245, 255);
     display: flex;
     flex-direction: column;
     padding: 12px;
     -webkit-box-align: center;
     align-items: center;
     -webkit-box-pack: center;
     justify-content: center;
     margin-left: 4px;
     margin-top: 30px;
     align-self: flex-start;
  }
  .chevron{
    background: blue;
    border-radius: 50%;
    width: 100px;
    height: 100px;
      color:white;
  }

</style>
@endsection

@section('content')
@php
    if(empty($meta_data['settings']['currency'])){
        $currency = "₹";
    }
    else{
        if($meta_data['settings']['currency_selection'] == "currency_symbol"){
            $currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
        }
        else{
            $currency = $meta_data['settings']['currency'];
        }
    }
 $agent=new \Jenssegers\Agent\Agent();
@endphp
@include('themes.'.$meta_data['themes']['name'].'.modals.details_product')
@include('themes.'.$meta_data['themes']['name'].'.partials.public_banner')

<div class="ecommerce-application h-100 mt-2">
    <div class="container-fluid w-100 ">
            @foreach ($catalog as $campaign)
                @php
                    if($campaign->is_start == 1) {
                        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
                        $catalogLink = route('campaigns.published',$campaign->public_link);
                        $videoLink = route('campaigns.publishedVideo',$campaign->public_link);
                        $shoppableLink = route('mystore.publishedVideo',$userSettings->public_link);
                        $showallLink = route('campaigns.showVideo',$campaign->public_link);
                    }else{
                        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
                        $catalogLink = route('admin.campaigns.preview',$campaign->preview_link);
                        $videoLink = route('campaigns.publishedVideo',$campaign->preview_link);
                        $shoppableLink = route('mystore.publishedVideo',$userSettings->preview_link);
                        $showallLink = route('campaigns.showVideo',$campaign->preview_link);
                    }
                    $entitycards = $campaign->campaignEntitycards;
                    $n = count($entitycards);

                @endphp
                @if(count($entitycards)== 0 )
                    @continue
                @endif
                @php
                $procount = 0;
                @endphp
                @foreach($entitycards as $card)
                @php
                $cardproduct = $card->product;
                $cardvideo = $card->video;
                if(isset($cardvideo) && isset($card->product_id))
                {
                    $procount++;
                }
                @endphp
                @endforeach
                <div class="row mb-3">
                <div class="col-12 ">
                @if((!empty($cardvideo) && ($procount > 0)) || (empty($cardvideo) && (!empty($cardproduct))))
                    <div class="row">
                        <div class="col">
                            <h4 class="mb-0"><b>{!! $campaign->name == "Untitled"?"&nbsp;":$campaign->name !!}</b></h4>
                        </div>
                        <div class="col text-right">
                        @foreach ($entitycards as $key => $card)
                            @php
                            $video = $card->video;
                            @endphp
                        @endforeach
                        @if(isset($video))
                            <a class="text-green btn btn-outline-primary viewmore" href="{{$videoLink}}"><b>VIEW MORE <i class="fa fa-angle-right" aria-hidden="true"></i></b></a>
                        @else
                            <a class="text-green btn btn-outline-primary viewmore" href="{{$catalogLink}}"><b>VIEW MORE <i class="fa fa-angle-right" aria-hidden="true"></i></b></a>
                        @endif
                        </div>
                    </div>
                @endif
                    <div class="row " style="display: flex;">
                    <div class="w-100 {{($agent->isTablet() || $agent->isMobile())?'':'niceScroll'}}" style="overflow-x: scroll;white-space: nowrap;">
                    @foreach ($entitycards as $key => $card)
                    @php
                    $entitycard_count = 0;
                    $isProductExist = false;
                    $video = $card->video;
                    if(!empty($video->thumbnail))
                    {
                       $img = $video->thumbnail->getUrl('thumb');
                    }
                    else{
                        $img = asset('XR/assets/images/video-placeholder.png');
                    }
                    @endphp
                    @if(!empty($card->video_id) && empty($card->product))
                    <div class="card ecommerce-card ml-1 mr-1 mt-1 p-1 rounded  video_id_{{$video->id}} videosize videocard">

                        <div class="card-content" style="overflow:hidden;">
                        <h5 class="text-truncate"><b>{!! $video->title !!}</b></h5>

                                <center>
                                    <div class="" style="margin:auto;">
                                        <a href="{{$showallLink}}?video-id={{$video->id}}"> <i class="feather icon-play-circle fa-5x position-absolute text-light d-flex justify-content-center videoIcon"  ></i>
                                        @if($img == asset('XR/assets/images/video-placeholder.png'))
                                            @if($agent->isMobile())
                                                <img class="nav-brand d-flex align-items-center rounded img-responsive" src="{{$img}}" style="height:200px; width:260px; cursor:pointer;"/></a>
                                            @else
                                            <img class="nav-brand d-flex align-items-center rounded img-responsive" src="{{$img}}" style="height:200px; width:420px; cursor:pointer;"/></a>
                                            @endif
                                        @else
                                        @if($agent->isMobile())
                                                <img class="nav-brand d-flex align-items-center rounded img-responsive" src="{{$img}}" style="cursor:pointer;height:200px; width:260px;"/></a>
                                            @else
                                                <img class="nav-brand d-flex align-items-center rounded img-responsive" src="{{$img}}" style="height:200px; width:420px; cursor:pointer;"/></a>
                                            @endif
                                        @endif
                                    </div>
                                </center>
                            <hr>
                                @foreach($entitycards as $key => $video_product)
                                @php
                                if(!empty($video_product->product) && $video_product->video_id == $card->video_id)
                                {
                                    $entitycard_count++;
                                    if($agent->isMobile()){
                                        if($entitycard_count > 2){
                                            break;
                                        }
                                    }
                                    else{
                                        if($entitycard_count > 3){
                                            break;
                                        }
                                    }
                                        $product = $video_product->product;
                                        if($product->downgradeable == 1){
                                            continue;
                                        }
                                        if($product->discount != null) {
                                            $discount_price = (float)$product->price * ((float)$product->discount/100);
                                            $discounted_price = round(($product->price - $discount_price),2);
                                        }
                                        if(count($product->photo) > 0){
                                            $img = $product->photo[0]->getUrl('thumb');
                                            $objimg = asset('XR/assets/images/placeholder.png');
                                            $obj_id = "";
                                            $obj_3d = false;
                                        } else if(count($product->arobject) > 0){
                                            if(!empty($product->arobject[0]->object))
                                                if(!empty($product->arobject[0]->object->getUrl('thumb'))) {
                                                    $img = $product->arobject[0]->object->getUrl('thumb');
                                                }
                                        } else {
                                            $img = asset('XR/assets/images/placeholder.png');
                                            $objimg = asset('XR/assets/images/placeholder.png');
                                        }
                                        foreach ($product->arobject as $key => $arobject) {
                                            if(!empty($arobject->object)){
                                                if($arobject->ar_type == "face") {
                                                    if(!empty($arobject->object->getUrl('thumb'))) {
                                                        $objimg = $arobject->object->getUrl('thumb');
                                                        $obj_id = $arobject->id;
                                                    }
                                                }
                                            }
                                            //echo $arobject->ar_type;
                                            if($arobject->ar_type == "surface") {
                                                if(!empty($arobject->object_3d)) {
                                                    $obj_3d = true;
                                                }
                                            }
                                        }
                                        if($campaign->is_start == 1) {
                                            if(count($product->categories) > 0) {
                                                $categoryLink = route('campaigns.publishedProductByCategory',[$product->categories[0]['id'],$campaign->public_link]);
                                            }
                                            $catalogId = $campaign->public_link;
                                            $tryonLink = route('campaigns.publishedTryOn',$campaign->public_link)."?sku=".$product->id;
                                            $viewARLink = route('campaigns.publishedViewAR',$campaign->public_link)."?sku=".$product->id;
                                            $catalogDetailsLink = route('campaigns.publishedCatalogDetails',[$product->id,$campaign->public_link]);
                                            $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
                                        }else{
                                            if(count($product->categories) > 0) {
                                                $categoryLink = route('campaigns.publishedProductByCategory',[$product->categories[0]['id'],$campaign->preview_link]);
                                            }
                                            $catalogId = $campaign->preview_link;
                                            $tryonLink = route('admin.campaigns.previewTryOn',$campaign->preview_link)."?sku=".$product->id;
                                            $viewARLink = route('admin.campaigns.previewViewAR',$campaign->preview_link)."?sku=".$product->id;
                                            $catalogDetailsLink = route('admin.campaigns.previewCatalogDetails',[$product->id,$campaign->preview_link]);
                                            $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
                                        }
                                    }

                            @endphp

                                        @if($video_product->video_id == $card->video_id && !empty($video_product->product))
                                        @php
                                            $isProductExist = true;
                                        @endphp
                                        @if($agent->isMobile())
                                         <div style="width:80px; float:left;">
                                        @else
                                         <div style="width:100px; float:left;">
                                        @endif
                                        <p><div class='card pl-1 pr-1 m-0'><img class="img-responsive mw-80 mh-80 rounded" src="{{$img}}" style="height:100px" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';"  style="height:170px;cursor:pointer;" onclick="getProductDetails({{$video_product->id}},{{$campaign->id}},{{$card->video_id}})" data-toggle="modal" data-target="#productDetailsModal"></div>
                                            @if($product->discount != null)
                                                        <span class="pl-1">{!!empty($product->price)?"":$currency!!}</span><span class="">{{$discounted_price}}{!!empty($product->units)?"":" / $product->quantity $product->units"!!}</span>
                                                        &nbsp;<span class="text-muted"><s>{!!empty($product->price)?"":$currency!!}{{$product->price}}</s></span>
                                                        <!-- <span class="text-danger">{{$product->discount}}% OFF</span> -->
                                                    @else
                                                        <span class="pl-1">{!!empty($product->price)?"":$currency!!}</span><span class="">{!! $product->price ?? '&nbsp;' !!}{!!empty($product->units)?"":" / $product->quantity $product->units"!!}</span>
                                                    @endif
                                </p>
                                        </div>
                                        @endif
                                @endforeach

                                @if($isProductExist == true)
                                <div class="text-center pt-1 showall" >

                                <a href="{{$showallLink}}?video-id={{$video->id}}" ><b>Show all </b><br><i class="feather icon-chevron-right chevron"></i>
                                </a></div>
                                @else
                                <script type="text/javascript">
                                     document.getElementsByClassName("video_id_{{$video->id}}")[0].style.display = 'none';
                                </script>
                                @endif
                            </div>
                        </div>
                        @endif
                    @endforeach
                        @foreach ($entitycards as $key => $card)

                            @if(!empty($card->product) && empty($card->video_id))
                            @php
                                $entitycard_count++;
                                if($entitycard_count > 5){
                                    break;
                                }
                                $product = $card->product;
                                if($product->downgradeable == 1){
                                    continue;
                                }
                                if($product->discount != null) {
                                    $discount_price = $product->price * ($product->discount/100);
                                    $discounted_price = round(($product->price - $discount_price),2);
                                }
                                if(count($product->photo) > 0){
                                    $img = $product->photo[0]->getUrl('thumb');
                                    $objimg = asset('XR/assets/images/placeholder.png');
                                    $obj_id = "";
                                    $obj_3d = false;
                                } else if(count($product->arobject) > 0){
                                    if(!empty($product->arobject[0]->object))
                                        if(!empty($product->arobject[0]->object->getUrl('thumb'))) {
                                            $img = $product->arobject[0]->object->getUrl('thumb');
                                        }
                                } else {
                                    $img = asset('XR/assets/images/placeholder.png');
                                    $objimg = asset('XR/assets/images/placeholder.png');
                                }
                                foreach ($product->arobject as $key => $arobject) {
                                    if(!empty($arobject->object)){
                                        if($arobject->ar_type == "face") {
                                            if(!empty($arobject->object->getUrl('thumb'))) {
                                                $objimg = $arobject->object->getUrl('thumb');
                                                $obj_id = $arobject->id;
                                            }
                                        }
                                    }
                                    //echo $arobject->ar_type;
                                    if($arobject->ar_type == "surface") {
                                        if(!empty($arobject->object_3d)) {
                                            $obj_3d = true;
                                        }
                                    }
                                }
                                if($campaign->is_start == 1) {
                                    if(count($product->categories) > 0) {
                                        $categoryLink = route('campaigns.publishedProductByCategory',[$product->categories[0]['id'],$campaign->public_link]);
                                    }
                                    $catalogId = $campaign->public_link;
                                    $tryonLink = route('campaigns.publishedTryOn',$campaign->public_link)."?sku=".$product->id;
                                    $viewARLink = route('campaigns.publishedViewAR',$campaign->public_link)."?sku=".$product->id;
                                    $catalogDetailsLink = route('campaigns.publishedCatalogDetails',[$product->id,$campaign->public_link]);
                                    $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
                                }else{
                                    if(count($product->categories) > 0) {
                                        $categoryLink = route('campaigns.publishedProductByCategory',[$product->categories[0]['id'],$campaign->preview_link]);
                                    }
                                    $catalogId = $campaign->preview_link;
                                    $tryonLink = route('admin.campaigns.previewTryOn',$campaign->preview_link)."?sku=".$product->id;
                                    $viewARLink = route('admin.campaigns.previewViewAR',$campaign->preview_link)."?sku=".$product->id;
                                    $catalogDetailsLink = route('admin.campaigns.previewCatalogDetails',[$product->id,$campaign->preview_link]);
                                    $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
                                }
                            @endphp
                            <div class="card ecommerce-card m-1" style="width:300px; display: inline-block">
                                <div class="card-content">
                                    @if($product->discount != null)
                                        <span class="badge badge-primary badge-pill" style="position: absolute;top:145px;left:5px;z-index: 10;"><strong>{{$product->discount}}% OFF</strong></span>
                                    @endif
                                    @if(count($product->categories) > 0)
                                        <a href="{{$categoryLink}}">
                                            <span class="badge badge-pill badge-primary text-white text-truncate" style="position: absolute;top:145px;right:5px;max-width:40%">{{$product->categories[0]['name']}}
                                            </span>
                                        </a>
                                    @endif
                                    @if($product->new != 0)
                                        <img src="{{asset('XR/app-assets/images/icons/new.png')}}" style="position: absolute;z-index:10;width:70px;height:70px;"/>
                                    @endif
                                    @if($product->featured != 0)
                                        <img src="{{asset('XR/app-assets/images/icons/featured1.png')}}" style="position: absolute;z-index: 10;right:0.001vw;width:70px;height:70px;"/>
                                    @endif
                                    <div class="text-center d-flex justify-content-center bg-white rounded">
                                        @if((@isset($meta_data['themes']['product_details_display'])) && ($meta_data['themes']['product_details_display'] == "new_page"))
                                        <a href="{{$catalogDetailsLink}}"><img class="img-responsive mw-100 mh-100 rounded-top" src="{{$img}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';"  style="height:170px;"></a>
                                        @else
                                            <img class="img-responsive mw-100 mh-100 rounded-top" src="{{$img}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';"  style="height:170px;cursor:pointer;" onclick="getProductDetails({{$card->id}},{{$campaign->id}})" data-toggle="modal" data-target="#productDetailsModal">
                                        @endif
                                        @if(!empty($obj_id))
                                            <a href="{{$tryonLink}}" data-id="{{$product->id}}">
                                                <div class="position-absolute" style="right:15px;bottom:200px;">
                                                    <div class="badge badge-glow badge-info">
                                                        <img class="img-fluid" src="{{asset('XR/assets/images/ar.png')}}"
                                                            style="width: 32px; max-height:32px;">
                                                        <h6 class="text-white m-0 umami--click--tryon-button-clicked-sku-{{$product->id}}">Tryon</h6>
                                                    </div>
                                                </div>
                                            </a>
                                        @endif
                                        @if($obj_3d == true)
                                            <a href="{{$viewARLink}}" data-id="{{$product->id}}">
                                                <div class="position-absolute" style="left:15px;bottom:200px;">
                                                    <div class="badge badge-glow badge-info">
                                                        <img class="img-fluid" src="{{asset('XR/assets/images/ar.png')}}"
                                                            style="width: 32px; max-height:32px;">
                                                        <h6 class="text-white m-0">3D/AR View</h6>
                                                    </div>
                                                </div>
                                            </a>
                                        @endif
                                    </div>
                                    @if((@isset($meta_data['themes']['product_details_display'])) && ($meta_data['themes']['product_details_display'] == "new_page"))
                                    <div class="item-wrapper d-inline-flex w-100 h-100">
                                    @else
                                    <div class="item-wrapper d-inline-flex w-100 h-100" onclick="getProductDetails({{$card->id}},{{$campaign->id}})" data-toggle="modal" data-target="#productDetailsModal" style="cursor:pointer;">
                                    @endif
                                        <div class="w-100 h-100 p-1">
                                            <div>
                                                <h6 class="item-price">
                                                    @if($product->discount != null && empty($product->discount))
                                                        <span class="">{!!empty($product->price)?"":$currency!!}</span><span class="">{{$discounted_price}}{!!empty($product->units)?"":" / $product->quantity $product->units"!!}</span>
                                                        &nbsp;<span class="text-muted"><s>{!!empty($product->price)?"":$currency!!}{{$product->price}}</s></span>
                                                        <!-- <span class="text-danger">{{$product->discount}}% OFF</span> -->
                                                    @else
                                                        <span class="">{!!empty($product->price)?"":$currency!!}</span><span class="">{!! $product->price ?? '&nbsp;' !!}{!!empty($product->units)?"":" / $product->quantity $product->units"!!}</span>
                                                    @endif
                                                </h6>
                                            </div>
                                            <!-- <div class="item-rating">
                                                @if(!empty($obj_id))
                                                    <a href="{{route('campaigns.publishedTryOn',$campaign->public_link)}}?sku={{$product->id}}" target="__blank" data-id="{{$product->id}}"
                                                    class="badge badge-pill badge-glow badge-success">Try On</a>
                                                @endif
                                            </div> -->
                                            <div class="item-name mt-0 text-truncate">
                                                <a href="{{$catalogDetailsLink}}">{!! $product->name ?? '&nbsp;' !!}</a>
                                            </div>
                                            @if(isset($meta_data['settings']['storefront_show_inventory']) && $meta_data['settings']['storefront_show_inventory'] == 1)
                                                @if($product->out_of_stock == null && $product->stock != null)
                                                    <div class="text-truncate"><strong>Stock: {{$product->stock}}</strong></div>
                                                @else
                                                    <br>
                                                @endif
                                            @endif
                                        </div>
                                        <!-- <div class="item-wrapper">

                                            @if(count($product->categories) > 0)
                                                <br>
                                                <h6>
                                                <a href="{{$categoryLink}}">
                                                    <div class="badge badge-pill badge-glow badge-primary text-white w-50 text-truncate mr-1 ">{{$product->categories[0]['name']}}
                                                    </div>
                                                </a>
                                                </h6>
                                            @else
                                                <br><br>
                                            @endif

                                            <div>
                                                <h6 class="item-price">
                                                    @if($product->discount != null)
                                                        <span class="">{!!empty($product->price)?"":$currency!!}</span><span class="">{{$discounted_price}}</span>
                                                        &nbsp;<span class="text-muted"><s>{!!empty($product->price)?"":$currency!!}{{$product->price}}</s></span>
                                                        <span class="text-danger">{{$product->discount}}% OFF</span>
                                                    @else
                                                        <span class="">{!!empty($product->price)?"":$currency!!}</span><span class="">{!! $product->price ?? '&nbsp;' !!}</span>
                                                    @endif
                                                </h6>
                                            </div>
                                            <div class="item-rating">
                                                @if(!empty($obj_id))
                                                    <a href="{{route('campaigns.publishedTryOn',$campaign->public_link)}}?sku={{$product->id}}" target="__blank" data-id="{{$product->id}}"
                                                    class="badge badge-pill badge-glow badge-success">Try On</a>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="item-name mt-0">
                                            <a href="#">{!! $product->name ?? '&nbsp;' !!}</a>
                                        </div> -->
                                    </div>
                                    <div class="item-options text-center">
                                    @if((@isset($meta_data['themes']['product_details_display'])) && ($meta_data['themes']['product_details_display'] == "new_page"))
                                        <div class="wishlist">
                                            <i class="fa fa-file-text"></i>
                                            <a href="{{$catalogDetailsLink}}" style="color:black;">Details</a>
                                        </div>
                                        @if($product->out_of_stock != null)
                                            <div class="cart rounded-bottom" style="background: #d9534f !important;">
                                                <div class="">
                                                    Out of stock
                                                </div>
                                            </div>
                                        @else
                                            <div class="cart rounded-bottom" style=" background-color:blueviolet;" >
                                                <!-- <a onclick="" href="{{$product->url ?? '#'}}" target="__blank"
                                                class="btn btn-block text-white bg-primary" data-color="primary">{{ trans('cruds.product.fields.order') }}</a> -->
                                                @if(!empty($product->shop) && $product->external_shop == 1)
                                                <a href="{{$product->link}}" style="color:white;" target="_blank">{{$product->shop}}</a>
                                                @else
                                                <i class="feather icon-shopping-cart"></i>
                                                <span class="add-to-cart" data-id="{{$product->id}}" data-name="{{$product->name ?? ''}}" data-quantity="{{$product->minimum_quantity ?? '1'}}" data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-maximum_quantity="{{ $product->maximum_quantity ?? '10000' }}"
                                                    data-price="{{$product->price ?? '0'}}" data-discounted_price="{{$product->discount ? $discounted_price:'0'}}" data-productweight="{{$product->productweight ?? '0'}}" data-length="{{$product->length ?? '0'}}" data-width="{{$product->width ?? '0'}}" data-height="{{$product->height ?? '0'}}"
                                                    data-catalogid="{{$catalogId}}" data-catagory="{{(count($product->categories) > 0)?$product->categories[0]['name']:''}}" data-sku="{{ $product->sku }}"
                                                    data-img="{{$img}}">Add to cart
                                                </span>
                                                <a href="{{$checkoutLink}}" class="view-in-cart d-none ">View In Cart</a>
                                                @endif
                                            </div>

                                        @endif
                                    @else
                                        @if($product->out_of_stock != null)
                                            <div class="cart rounded-bottom" style="background: #d9534f !important;">
                                                <div class="">
                                                    Out of stock
                                                </div>
                                            </div>
                                        @else
                                            <div class="cart quantityController rounded-bottom">
                                                <!-- <a onclick="" href="{{$product->url ?? '#'}}" target="__blank"
                                                class="btn btn-block text-white bg-primary" data-color="primary">{{ trans('cruds.product.fields.order') }}</a> -->
                                                @if(!empty($product->shop) && $product->external_shop == 1)
                                                <a href="{{$product->link}}" style="color:white;" target="_blank">{{$product->shop}}</a>
                                                @else
                                                <i class="feather icon-shopping-cart"></i>
                                                <span class="add-to-cart" data-id="{{$product->id}}" data-name="{{$product->name ?? ''}}" data-quantity="{{$product->minimum_quantity ?? '1'}}" data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-maximum_quantity="{{ $product->maximum_quantity ?? '10000' }}"
                                                    data-price="{{$product->price ?? '0'}}" data-discounted_price="{{$product->discount ? $discounted_price:'0'}}"
                                                    data-productweight="{{$product->productweight ?? '0'}}" data-length="{{$product->length ?? '0'}}" data-width="{{$product->width ?? '0'}}" data-height="{{$product->height ?? '0'}}"
                                                     data-catalogid="{{$catalogId}}" data-catagory="{{(count($product->categories) > 0)?$product->categories[0]['name']:''}}" data-sku="{{ $product->sku }}"
                                                    data-img="{{$img}}">Add to cart
                                                </span>
                                                <!-- <span class="view-in-cart d-none"> -->
                                                    <!-- View in cart -->
                                                <!-- </span> -->
                                                <span class="view-in-cart d-none">
                                                    <div class="input-group quantity-counter-wrapper">
                                                        <input type="text" id="product_qty_{{$product->id}}" class="quantity-counter" data-id="{{$product->id}}" value="{{$product->minimum_quantity ?? '1'}}" data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-maximum_quantity="{{ $product->maximum_quantity ?? '10000' }}">
                                                    </div>
                                                </span>
                                                @endif
                                            </div>
                                        @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @endif
                        @endforeach
                    </div>
                    </div>
                </div>
                </div>
            @endforeach

    </div>
</div>
@endsection
@section('scripts')
@parent
<script src="{{asset('XR/assets/js/cart.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js"></script>
<script src="{{asset('XR/app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')}}">
<script>

    // checkout quantity counter
  var quantityCounter = $(".quantity-counter"),
    CounterMin = 1,
    CounterMax = 10000;
    var min_qty = 1;
  if (quantityCounter.length > 0) {
    quantityCounter.TouchSpin({
      min: CounterMin,
      max: CounterMax
    }).on('touchspin.on.startdownspin', function () {
      var $this = $(this);
      let min_qty = $(this).data("minimum_quantity") || 1;
      $this.trigger("touchspin.updatesettings", {min: min_qty})
      $('.bootstrap-touchspin-up').removeClass("disabled-max-min");
      if ($this.val() <= min_qty) {
            $(this).siblings().find('.bootstrap-touchspin-down').addClass("disabled-max-min");
            // $(this).siblings().find('.bootstrap-touchspin-down').attr("disabled", true);
            // $(this).siblings().find('.bootstrap-touchspin-down').addClass("danger-max-min");
            // obj.removeItemFromCart(Number($(this).data('id')));
            // obj.totalCart();
            // $(this).closest('.view-in-cart').siblings('.add-to-cart').removeClass("d-none");
            // $(this).closest('.view-in-cart').siblings('.add-to-cart').addClass("d-inline-block");
            // $(this).closest('.view-in-cart').removeClass("d-inline-block");
            // $(this).closest('.view-in-cart').addClass("d-none");
            // e.stopImmediatePropagation();
        }
        obj.setCountForItem(Number($this.data('id')),$this.val());
        //$("#product_qty_"+$this.data('id')).val($this.val());

    }).on('touchspin.on.startupspin', function () {
        var $this = $(this);
        let max_qty = $(this).data("maximum_quantity") || 10;
        console.log(max_qty)
        $('.bootstrap-touchspin-down').removeClass("disabled-max-min");
        if ($this.val() <= max_qty) {
            $(this).siblings().find('.bootstrap-touchspin-up').addClass("disabled-max-min");
        }
        obj.setCountForItem(Number($this.data('id')),$this.val());

    }).on('change', function () {
      var $this = $(this);
      let min_qty = $(this).data("minimum_quantity") || 1;
      let max_qty = $(this).data("maximum_quantity") || 10;
    //   if ($this.val() == 1) {
    //     $(this).siblings().find('.bootstrap-touchspin-down').addClass("danger-max-min");
    //     }
      if ($this.val() == 0) {
            obj.setCountForItem(Number($this.data('id')),min_qty);
            $("#product_qty_" + $this.data('id')).val(min_qty);
        } else if ($this.val() > CounterMax) {
            obj.setCountForItem(Number($this.data('id')),CounterMax);
      }
      else if($this.val() < min_qty){
        obj.setCountForItem(Number($this.data('id')),min_qty);
        $("#product_qty_" + $this.data('id')).val(min_qty);
      }
      else if($this.val() > max_qty){
        obj.setCountForItem(Number($this.data('id')),max_qty);
        $("#product_qty_" + $this.data('id')).val(max_qty);
      }
       else {
         obj.setCountForItem(Number($this.data('id')),$this.val());
      }
    });
  }
  obj.setCountForItem = function(id, qty) {
    // console.log("set count "+ id,qty);
    for(var i in cart) {
      if (cart[i].id === id) {
        cart[i].qty = qty;
        break;
      }
    }
    saveCart();
    obj.totalCart();
  };
  obj.removeItemFromCart = function(id) {
        for (var item in cart) {
            if (cart[item].id === id) {
                //   cart[item].qty --;
                //   if(cart[item].qty === 0) {
                cart.splice(item, 1);
                //   }
                //   break;
            }
        }
        saveCart();
    }

  $( document ).ready(function() {
    if (localStorage.getItem("shoppingCart") != null) {
        cart = JSON.parse(localStorage.getItem('shoppingCart'));
        var cartItemCount = count = cart.length;
        //console.log(cart)
        if(cart.length > 0) {
            var products = [];
            for(var item in cart) {
                var id = cart[item].id, qty = cart[item].qty;
                //console.log("loadcount "+ id,qty);
                $("#product_qty_"+(id)).val(qty);
            }

        }
    }
    });

</script>
<script>
    function tryon(pid) {
        window.location.href = window.location.href + "?sku=" + pid;
    }
    // $('img').on("error", function() {
    //   $(this).attr('src', '{{asset('XR/assets/images/placeholder.png')}}');
    //   $(this).css('width', '95%');
    //   $(this).css('max-height', '100%');
    // });
    // var x = document.getElementsByClassName("navbar-wrapper")[0].clientHeight;
    // console.log(x);
    // $(".content-wrapper").css('margin-top', x);

    window.onload = function() {

        // if (document.cookie.indexOf('store_event') == -1) {
        url = window.location.pathname;
        slink = url.slice(url.lastIndexOf('/') + 1);
        if (slink == '') {
            slink = window.location.host;
        }
        track_events("store", null, slink);
        // document.cookie = 'store_event=1';
        // }

    };




    function track_events(event_name, product_id, slink) {
        $.ajax({
            url: "{{route('admin.stats.ajaxEvents')}}",
            type: "post",
            data: {
                "_token": $('meta[name="csrf-token"]').attr('content'),
                "event": event_name,
                "slink": slink,
                "product_id": product_id,
            },
        });
    }

    $(".niceScroll").niceScroll({
        cursorcolor: "grey", // change cursor color in hex
        cursoropacitymin: 0, // change opacity when cursor is inactive (scrollabar "hidden" state), range from 1 to 0
        cursoropacitymax: 1, // change opacity when cursor is active (scrollabar "visible" state), range from 1 to 0
        cursorwidth: "5px", // cursor width in pixel (you can also write "5px")
        cursorborder: "1px solid #fff", // css definition for cursor border
        cursorborderradius: "5px", // border radius in pixel for cursor
        zindex: "auto", // change z-index for scrollbar div
        scrollspeed: 60, // scrolling speed
        mousescrollstep: 40, // scrolling speed with mouse wheel (pixel)
        touchbehavior: false, // enable cursor-drag scrolling like touch devices in desktop computer
        hwacceleration: true, // use hardware accelerated scroll when supported
        boxzoom: false, // enable zoom for box content
        dblclickzoom: true, // (only when boxzoom=true) zoom activated when double click on box
        gesturezoom: true, // (only when boxzoom=true and with touch devices) zoom activated when pinch out/in on box
        grabcursorenabled: true, // (only when touchbehavior=true) display "grab" icon
        autohidemode: false, // how hide the scrollbar works, possible values:
        background: "", // change css for rail background
        iframeautoresize: true, // autoresize iframe on load event
        cursorminheight: 32, // set the minimum cursor height (pixel)
        preservenativescrolling: true, // you can scroll native scrollable areas with mouse, bubbling mouse wheel event
        railoffset: false, // you can add offset top/left for rail position
        bouncescroll: false, // (only hw accell) enable scroll bouncing at the end of content as mobile-like
        spacebarenabled: true, // enable page down scrolling when space bar has pressed
        disableoutline: true, // for chrome browser, disable outline (orange highlight) when selecting a div with nicescroll
        horizrailenabled: true, // nicescroll can manage horizontal scroll
        railalign: "right", // alignment of vertical rail
        railvalign: "bottom", // alignment of horizontal rail
        enabletranslate3d: true, // nicescroll can use css translate to scroll content
        enablemousewheel: true, // nicescroll can manage mouse wheel events
        enablekeyboard: true, // nicescroll can manage keyboard events
        smoothscroll: true, // scroll with ease movement
        sensitiverail: true, // click on rail make a scroll
        enablemouselockapi: true, // can use mouse caption lock API (same issue on object dragging)
        cursorfixedheight: false, // set fixed height for cursor in pixel
        hidecursordelay: 400, // set the delay in microseconds to fading out scrollbars
        irectionlockdeadzone: 6, // dead zone in pixels for direction lock activation
        nativeparentscrolling: true, // detect bottom of content and let parent to scroll, as native scroll does
        enablescrollonselection: true, // enable auto-scrolling of content when selection text
        cursordragspeed: 0.3, // speed of selection when dragged with cursor
        rtlmode: "auto", // horizontal div scrolling starts at left side
        cursordragontouch: false, // drag cursor in touch / touchbehavior mode also
        oneaxismousemode: "auto",
        scriptpath: "", // define custom path for boxmode icons ("" => same script path)
        preventmultitouchscrolling: true, // prevent scrolling on multitouch events
        disablemutationobserver: false,
    });

    @if(session('message'))
                    $('.loginButton').click()
                @endif

</script>

@endsection
