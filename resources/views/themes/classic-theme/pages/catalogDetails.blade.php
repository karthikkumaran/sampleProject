@extends('themes.'.$meta_data['themes']['name'].'.layouts.public')
@section ('styles')
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-details.css')}}">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.6.2/viewer.min.css">
@endsection
@section('content')
@php
$agent=new \Jenssegers\Agent\Agent();

if($campaign->is_start == 1) {
$catalogId = $campaign->public_link;
$tryonLink = route('campaigns.publishedTryOn',$campaign->public_link)."?sku=".$product->id;
$viewARLink = route('campaigns.publishedViewAR',$campaign->public_link)."?sku=".$product->id;
$checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
$catalogLink = route('campaigns.published',[$campaign->public_link]);
}else{
$catalogId = $campaign->preview_link;
$tryonLink = route('admin.campaigns.previewTryOn',$campaign->preview_link)."?sku=".$product->id;
$viewARLink = route('admin.campaigns.previewViewAR',$campaign->preview_link)."?sku=".$product->id;
$checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
$catalogLink = route('admin.campaigns.preview',[$campaign->preview_link]);
}
if($userSettings->is_start == 1) {
if(!empty($userSettings->customdomain)){
$homeLink = '//'.$userSettings->customdomain;
}else if(!empty($userSettings->subdomain)) {
$homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
}else{
$homeLink = route('mystore.published',[$userSettings->public_link]);
}
}else{
$homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
}

if(empty($meta_data['settings']['currency']))
$currency = "₹";
else
if($meta_data['settings']['currency_selection'] == "currency_symbol")
$currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
else
$currency = $meta_data['settings']['currency'];
if($product->discount != null)
{
$discount_price = $product->price * ($product->discount/100);
$discounted_price = round(($product->price - $discount_price),2);
}
if(count($product->photo) > 0){
$img = $product->photo[0]->getUrl();
$objimg = asset('XR/assets/images/placeholder.png');
$obj_id = "";
$obj_3d = false;
} else if(count($product->arobject) > 0 && $product->arobject[0]->ar_type == "face"){
$img = $product->arobject[0]->object->getUrl();
}
if(count($product->arobject) > 0 && $product->arobject[0]->ar_type == "face"){
$objimg = $product->arobject[0]->object->getUrl();
$obj_id = $product->arobject[0]->id;
}
foreach ($product->arobject as $key => $arobject) {
        if(!empty($arobject->object)){
            if($arobject->ar_type == "face") {
                if(!empty($arobject->object->getUrl('thumb'))) {
                    $objimg = $arobject->object->getUrl('thumb');
                    $obj_id = $arobject->id;
                }
            }
        }
        //echo $arobject->ar_type;
        if($arobject->ar_type == "surface") {

            if(!empty($arobject->object_3d)) {
                $obj_3d = true;
            }
        }
}
@endphp
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb border-0">
                        <li class="breadcrumb-item"><a href="{{$homeLink}}">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="{{$catalogLink}}">{!! $campaign->name == "Untitled"?"&nbsp;":$campaign->name !!}</a>
                        </li>
                        <li class="breadcrumb-item active">Product Details
                        </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content-body">
    <!-- app ecommerce details start -->
    <section class="app-ecommerce-details">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 col-md-6 ">
                        <div class="w-100 d-flex justify-content-center">
                            @if(count($product->photo) > 1 || $obj_3d == true)
                            <!--Carousel Wrapper-->
                            <div id="carousel-thumb" class="w-100 carousel slide carousel-fade carousel-thumbnails" data-interval="false" data-ride="carousel">
                                <!--Slides-->
                                <div class="carousel-inner" role="listbox" id="productimages">
                                    @foreach($product->photo as $photo)
                                    <div class="carousel-item {{$loop->index == 0?'active':''}} d-flex justify-content-center" style="width:100%;">
                                        <img class="img-fluid cursor-pointer rounded w-100" style="max-height:350px;max-width:300px;" src="{{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl():$objimg ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                    </div>
                                    @endforeach
                                    @if($obj_3d == true)
                                    <div class="carousel-item d-flex justify-content-center" style="width:100%;">
                                        <img class="img-fluid cursor-pointer rounded mw-100" style="max-height:350px;opacity:0.1;" src="#{{(count($product->photo) > 0)?$product->photo[0]->getUrl():$objimg ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/360.png')}}';">
                                        <a href="{{$viewARLink}}" class="btn btn-info" style="position: absolute; bottom: 40%;">3D View</a>
                                    </div>
                                    @endif
                                </div>
                                <div class="w-100 position-relative d-inline-flex justify-content-center niceScroll mt-1" style="overflow-y: hidden;white-space: nowrap;overflow: auto;height:60px;overflow-x:scroll;">
                                    @foreach($product->photo as $photo)
                                        <div class="{{$loop->index == 0?'active':''}} h-100 ml-1" data-target="#carousel-thumb" data-slide-to="{{$loop->index}}"  style="min-width:90px;max-width:90px;">
                                            <img class="d-inline-block cursor-pointer img-responsive w-100 h-100 rounded" style="" src="{{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl():$objimg ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                        </div>
                                    @endforeach
                                    @if($obj_3d == true)
                                    <div class="h-100 d-flex justify-content-center" data-target="#carousel-thumb" data-slide-to="{{count($product->photo)}}" >
                                        <img class="d-inline-block cursor-pointer img-responsive ml-1 h-100 rounded" style="max-width:90px;" src="" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/360.png')}}';">
                                    </div>
                                    @endif
                                </div>
                                <!--/.Slides-->
                            </div>
                            <!--/.Carousel Wrapper-->
                            @else
                            <img class="img-fluid cursor-pointer rounded " style="max-height:350px;" src="{{$img ?? ''}}" id="productimages" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="width: 100%; max-height:350px;">
                            @endif
                        </div>
                    </div>

                    <div class="col-12 col-md-6 p-1">
                        <h5 id="product_name"><b>{!! $product->name ?? '&nbsp;' !!}</b></h5>
                        @if(isset($meta_data['settings']['storefront_show_sku']) && $meta_data['settings']['storefront_show_sku'] == 1 && isset($product->sku))
                            <h5 id="product_sku">SKU: {!! $product->sku ?? '' !!}</h5>
                        @endif
                        @if(isset($meta_data['settings']['storefront_show_inventory']) && $meta_data['settings']['storefront_show_inventory'] == 1)
                            @if($product->out_of_stock == null && $product->stock != null)
                                <h6 class="text-truncate"><strong>Stock: {{$product->stock}}</strong></h6>
                            @else
                                <!-- <br> -->
                            @endif
                        @endif
                        <p class="text-muted">
                            @if(count($product->categories) > 0)
                        <div class="badge badge-pill badge-glow badge-primary text-white mr-1">{{$product->categories[0]['name']}}
                        </div>
                        @endif
                        </p>


                        <div class="ecommerce-details-price d-flex flex-wrap">
                            <p class="font-medium-3 mr-1 mb-0">
                                @if($product->discount != null)
                                    <span class="text-primary" id="product_currency">{!!empty($product->price)?"":$currency!!}</span><span class="text-primary" id="product_discount_price">{{$discounted_price}}{!!empty($product->units)?"":" / $product->quantity $product->units"!!} </span>
                                    &nbsp;<span class="text-dark" id="product_price"><s>{!!empty($product->price)?"":$currency!!}{{$product->price}}</s></span>
                                    <br><span class="text-warning" style="font-size: 80%;" id="product_discount">{{$product->discount}}% OFF</span>
                                @else
                                    <span class="text-primary" id="product_currency">{!!empty($product->price)?"":$currency!!}</span><span class="text-primary" id="product_price">{!! $product->price ?? '&nbsp;' !!}</span><span class="text-primary">{!!empty($product->units)?"":" / $product->quantity $product->units"!!}</span>
                                @endif
                            </p>
                        </div>
                        <hr>
                        <div class="d-flex flex-column flex-sm-row">
                            @if($product->out_of_stock != null)
                            <h4 class="text-danger">Out of stock</h4>
                            @else
                            <div class="cart quantityController btn btn-primary mr-0 mr-sm-1 mb-1 mb-sm-0 p-2 text-uppercase text-white" style="cursor: pointer;">
                            @if(!empty($product->shop) && $product->external_shop == 1)
                                 <a href="{{$product->link}}" style="color:white;" target="_blank">{{$product->shop}}</a>
                             @else
                                <i class="feather icon-shopping-cart"></i>
                                <span class="add-to-cart" data-id="{{$product->id}}" data-name="{{$product->name ?? ''}}" data-quantity="{{$product->minimum_quantity ?? '1'}}" data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-maximum_quantity="{{ $product->maximum_quantity ?? '10000' }}"
                                     data-price="{{$product->price ?? '0'}}" data-discounted_price="{{$product->discount ? $discounted_price:'0'}}" data-productweight="{{$product->productweight ?? '0'}}" data-length="{{$product->length ?? '0'}}" data-width="{{$product->width ?? '0'}}" data-height="{{$product->height ?? '0'}}" data-sku="{{ $product->sku }}"
                                      data-catalogid="{{$catalogId}}" data-catagory="{{(count($product->categories) > 0)?$product->categories[0]['name']:''}}" data-img="{{ (count($product->photo) > 0) ? $product->photo[0]->getUrl() : asset('XR/assets/images/placeholder.png')}}">Add to cart
                                </span>
                                <span class="view-in-cart d-none ">
                                    <div class="input-group quantity-counter-wrapper">
                                        <input type="text" id="product_qty_{{$product->id}}" class="quantity-counter" data-sku="{{ $product->sku }}" data-id="{{$product->id}}" data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-maximum_quantity="{{ $product->maximum_quantity ?? '10000' }}" value="{{$product->minimum_quantity ?? '1'}}">
                                    </div>
                                </span>
                            @endif
                            </div>
                            @endif

                            @if(!empty($obj_id))
                            <a href="{{$tryonLink}}" class="btn btn-info mr-1">
                                <img class="img-fluid" src="{{asset('XR/assets/images/ar.png')}}" style="width: 32px; max-height:32px;"> TRYON</a>
                            @endif
                            @if($obj_3d == true)
                                <a href="{{$viewARLink}}" class="btn btn-info">
                                <img class="img-fluid" src="{{asset('XR/assets/images/ar.png')}}" style="width: 32px; max-height:32px;">3D/AR View</a>
                            @endif
                        </div>
                        <hr>
                        @if(strip_tags($product->description) == "")

                        @else
                        {!! $product->description !!}
                        <hr>
                        @endif
                        @php
                            $shipdata = '';
                            if(isset($meta_data['shipping_settings'])){
                                foreach($meta_data['shipping_settings']['shipping_method'] as $val){
                                    $shipdata = $val;
                                }
                            }
                        @endphp
                        @if(!empty($shipdata))
                        <div class="row">
                            @if($agent->isMobile())
                            <div class="col-md-3 d-flex justify-content-center my-1 ">
                            @else
                            <div class="col-md-3 d-flex justify-content-end my-1 ">
                            @endif
                                <label for="delivery m-1">Delivery to</label>
                            </div>
                            @if($agent->isMobile())
                            <div class="col-md-3 text-center m-1">
                            @else
                            <div class="col-md-3 text-center ">
                            @endif
                                <input type="number"class="form-control" id="delivery" placeholder="deliver pincode check"></input>
                            </div>
                            <div class="col-md-3">
                            @if($agent->isMobile())
                                <button class="btn btn-md btn-danger m-1" id="deliverypin">Check</button>
                            @else
                            <button class="btn btn-md btn-danger " id="deliverypin">Check</button>
                            @endif
                            </div>
                        </div>
                        <h4><span id="checkedresult"></span></h4>
                        <hr>
                        @endif
                        @if(count($values)>=1)
                        <div id="attribute_list">
                            @foreach ($attributes as $groupname=>$attributes)
                            <label class=" bg-light w-100 mb-1 text-capitalize font-weight-bold" style="padding:5px;">{{$groupname}}</label>
                            <div>
                                @foreach($attributes as $attribute)
                                <div class="row">
                                    @if ($attribute->type=="textbox")
                                    @if(isset($values[$attribute->attribute_id]))
                                    <div class="col-6">
                                        <label class="text-capitalize font-weight-bold" for="{{$attribute->label}}">{{ $attribute->label }}</label>
                                    </div>
                                    <div class="col-6">
                                        <label class="text-capitalize font-weight-bold float-right" for="{{$attribute->label}}">{{ $values[$attribute->attribute_id] }}</label>
                                    </div>
                                    @endif
                                    @elseif($attribute->type=="checkbox")
                                    @if(isset($values[$attribute->attribute_id]))
                                    <div class="col-6">
                                        <label class="text-capitalize font-weight-bold" for="{{$attribute->label}}">{{ $attribute->label }}</label>
                                    </div>
                                    <div class="col-6">
                                        <label class="text-capitalize font-weight-bold float-right" for="{{$attribute->label}}">{{ $values[$attribute->attribute_id] }}</label>
                                    </div>
                                    @endif
                                    @elseif($attribute->type=="radio_button")
                                    @if(isset($values[$attribute->attribute_id]))
                                    <div class="col-6">
                                        <label class="text-capitalize font-weight-bold" for="{{$attribute->label}}">{{ $attribute->label }}</label>
                                    </div>
                                    <div class="col-6">
                                        <label class="text-capitalize font-weight-bold float-right" for="{{$attribute->label}}">{{ $values[$attribute->attribute_id] }}</label>
                                    </div>
                                    @endif
                                    @elseif($attribute->type=="dropdown")
                                    @if(isset($values[$attribute->attribute_id]))
                                    <div class="col-6">
                                        <label class="text-capitalize font-weight-bold" for="{{$attribute->label}}">{{ $attribute->label }}</label>
                                    </div>
                                    <div class="col-6">
                                        <label class="text-capitalize font-weight-bold float-right" for="{{$attribute->label}}">{{ $values[$attribute->attribute_id] }}</label>
                                    </div>
                                    @endif
                                    @endif
                                </div>
                                @endforeach
                            </div>
                            @endforeach
                        </div>
                        <hr>
                        @else

                        @endif
                        <!-- <p>Available - <span class="text-success">In stock</span></p> -->

                        <div class="row">
                            <h5 class="align-self-center">Share&nbsp&nbsp</h5>&nbsp;
                            <button type="button" class="btn btn-icon rounded-circle btn-outline-primary mr-1 mb-1 shareBtn" <?php if ($campaign->is_private == 1) { ?> disabled <?php   } ?> data-label="fb"><i class="feather icon-facebook"></i></button>
                            <button type="button" class="btn btn-icon rounded-circle btn-outline-info mr-1 mb-1 shareBtn" <?php if ($campaign->is_private == 1) { ?> disabled <?php   } ?> data-label="twitter"><i class="feather icon-twitter"></i></button>
                            <!-- <button type="button" class="btn btn-icon rounded-circle btn-outline-primary mr-1 mb-1"><i class="feather icon-instagram"></i></button> -->
                            <button type="button" class="btn btn-icon rounded-circle btn-outline-primary mr-1 mb-1 shareBtn" <?php if ($campaign->is_private == 1) { ?> disabled <?php   } ?> data-label="whatsapp"><i class="fa fa-whatsapp"></i></button>
                            <button type="button" class="btn btn-icon rounded-circle btn-outline-primary mr-1 mb-1 share-button shareBtn" style="display:none;" id="mobile_share"><i class="feather icon-share-2"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- app ecommerce details end -->
</div>

<div class="modal fade" id="share_options_modal" tabindex="-1" data-bs-backdrop="static" data-bs-keyboard="false" aria-labelledby="share_options_modal" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <a href="#" class=" btn btn-sm border-2 border-primary mb-1 share-option" data-value="txt">Share as Text</a></br>
                    <a href="#" class=" btn btn-sm border-2 border-primary share-option" data-value="img">Share as Image</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.6.2/viewer.min.js" integrity="sha512-VzJLwaOOYyQemqxRypvwosaCDSQzOGqmBFRrKuoOv7rF2DZPlTaamK1zadh7i2FRmmpdUPAE/VBkCwq2HKPSEQ==" crossorigin="anonymous"></script>
<script src="{{asset('XR/assets/js/cart.js')}}"></script>
<script src="{{asset('XR/app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')}}">
<script>
    // checkout quantity counter
    var quantityCounter = $(".quantity-counter"),
        CounterMin = 1,
        CounterMax = 10000;
    if (quantityCounter.length > 0) {
        quantityCounter.TouchSpin({
            min: CounterMin,
            max: CounterMax
        }).on('touchspin.on.startdownspin', function() {
            var $this = $(this);
            let min_qty = $(this).data("minimum_quantity") || 1;
            $this.trigger("touchspin.updatesettings", {min: min_qty})
            $('.bootstrap-touchspin-up').removeClass("disabled-max-min");
            if ($this.val() == 1) {
                $(this).siblings().find('.bootstrap-touchspin-down').addClass("disabled-max-min");
            }
            obj.setCountForItem(Number($this.data('id')), $this.val());
            //$("#product_qty_"+$this.data('id')).val($this.val());

        }).on('touchspin.on.startupspin', function() {
            var $this = $(this);
            let max_qty = $(this).data("maximum_quantity") || 10;
            console.log(max_qty)
            $('.bootstrap-touchspin-down').removeClass("disabled-max-min");
            if ($this.val() <= max_qty) {
                $(this).siblings().find('.bootstrap-touchspin-up').addClass("disabled-max-min");
            }
            obj.setCountForItem(Number($this.data('id')),$this.val());

        }).on('change', function() {
            var $this = $(this);
            let min_qty = $(this).data("minimum_quantity") || 1;
            let max_qty = $(this).data("maximum_quantity") || 10;
            if ($this.val() == 0) {
                obj.setCountForItem(Number($this.data('id')), min_qty);
                $("#product_qty_" + $this.data('id')).val(min_qty);
            } else if ($this.val() > CounterMax) {
                obj.setCountForItem(Number($this.data('id')), CounterMax);
            } else if($this.val() < min_qty){
                obj.setCountForItem(Number($this.data('id')),min_qty);
                $("#product_qty_" + $this.data('id')).val(min_qty);
            }
            else if($this.val() > max_qty){
                obj.setCountForItem(Number($this.data('id')),max_qty);
                $("#product_qty_" + $this.data('id')).val(max_qty);
            }
            else {
                obj.setCountForItem(Number($this.data('id')), $this.val());
            }
        });
    }

    obj.setCountForItem = function(id, qty) {
        for (var i in cart) {
            if (cart[i].id === id) {
                cart[i].qty = qty;
                break;
            }
        }
        saveCart();
        obj.totalCart();
    };

            $(document).ready(function() {
                if (localStorage.getItem("shoppingCart") != null) {
                    cart = JSON.parse(localStorage.getItem('shoppingCart'));
                    var cartItemCount = count = cart.length;
                    if (cart.length > 0) {
                        var products = [];
                        for (var item in cart) {
                            var id = cart[item].id,
                                qty = cart[item].qty;
                            $("#product_qty_" + (id)).val(qty);
                        }

                    }
                }
            });
</script>
<script>
    $('#delivery').focusin(function(){

        $('#checkedresult').text('');
});
$('#deliverypin').click(function(){
    var deliverypin = $('#delivery').val();
    var weight = "{{$product->productweight}}";
    var height = "{{$product->height}}";
    var width = "{{$product->width}}";
    var length = "{{$product->length}}";
    if(deliverypin.length == 6)
    {
        @if(isset($meta_data['shipping_settings']['shipping_method']) && $meta_data['shipping_settings']['shipping_method'] != "shipping")
            @if(isset($meta_data['footer_settings']['pincode']))
                source_code = {{$meta_data['footer_settings']['pincode']}};
            @endif
                destination_code = deliverypin;
                data = '&source_code='+ source_code +'&destination_code=' + destination_code + '&weight=' + weight + '&height=' + height + '&length=' + length + '&width=' + width  +'&shareable_link='+'{{$catalogId}}';
                $.ajax({
                url: "{{route('shipping.price')}}",
                type: "POST",
                data: data,
                datatype:"json",
                success: function(data) {
                    data =  JSON.parse(data);
                    console.log(data['pricing']);
                    if(data['pricing']){

                    let displayShip = [];
                    for(var service in data["service"]){

                        if(data["service"][service]){
                            let name = service.replace(/[^0-9.]/g, "") //remove logic
                            displayShip.push({
                                name: name,
                                price: data["pricing"][service]
                            })
                        }
                    }
                    if(displayShip.length > 0){
                        console.log(displayShip);
                        // alert('Shipping service available');
                        $('#checkedresult').text('Shipping service availble');
                        $('.cart').css("pointer-events", "auto");
                        $('.cart').css("opacity", 1);
                    }
                    else{
                        // alert('Sorry we are unable to deliver to your pincode:'+ deliverypin);
                        $('#checkedresult').text('Sorry we are unable to deliver to your pincode:'+ deliverypin);
                        $('.cart').css("pointer-events", "none");
                        $('.cart').css("opacity", 0.5);
                    }
                    }
                    else{
                            // alert('Shipping service is not available for this product');
                            $('#checkedresult').text('Shipping service is not available for this product');
                            $('.cart').css("pointer-events", "none");
                            $('.cart').css("opacity", 0.5);
                        }
                }
                });
        @endif
    }
    else{
        $('#checkedresult').text('');
        alert('enter valid pin code');
        $('.cart').css("pointer-events", "none");
        $('.cart').css("opacity", 0.5);
    }
});
    window.onload = function() {
        url = window.location.pathname;
        slink = url.slice(url.lastIndexOf('/') + 1);
        productId = $('.add-to-cart').data("id");
        track_events("product_details", productId, slink);
    };



    function tryon(pid) {
        window.location.href = window.location.href + "?sku=" + pid;
    }
    /////zoom
    document.getElementById("productimages").onmousemove= function() {

        let toolbar = {
            zoomOut: 3,
            zoomIn: 3,
            prev: 3,
            play: 3,
            next: 3,
            reset: 3,
        }
        const viewer = new Viewer(this, {
            inline: false,
            toolbar: toolbar,
            title: false,
            zoomOnTouch: true,
            slideOnTouch: true,
            movable: false,
            className: "bg-dark",
        });
    }

    // var x = document.getElementsByClassName("navbar-wrapper")[0].clientHeight;
    // console.log(x);
    // $(".content-wrapper").css('margin-top', x);


    window.onload = function() {
        url = window.location.pathname;
        slink = url.slice(url.lastIndexOf('/') + 1);
        productId = $('.add-to-cart').data("id");
        track_events("product_details", productId, slink);
    };

    var u="{{Request::fullUrl()}}";
    var txt="";
    var file_name = "{!! $product->name ?? 'Untitled Product' !!}";
    file_name += ".png";
    // var shareButton = document.querySelector('.share-button');
    var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    var file,i;
    var filesArray = [];
    var product_images = [];
    var product_name = $("#product_name").text();
    var product_price = $("#product_price").text();
    var product_currency = $("#product_currency").text();
    var product_sku = $("#product_sku").text();
    var product_discount = $("#product_discount").text();
    var product_discount_price = $("#product_discount_price").text();
    var t = txt + "\n";
    if(product_name){
        t += "*"+product_name+"*";
    }
    if(product_price){
        if(product_discount){
            t += "\n" + product_currency + product_discount_price + " ~" + product_price + "~";
        }
        else{
            t += "\n" + product_currency + product_price;
        }
    }
    if(product_discount){
        t += "\n" + "*Discount:* " + product_discount;
    }
    if(product_sku){
        t += "\n" + product_sku;
    }
    t += "\n" + "*Order Now:* ";
    // t += "*"+product_name+"*" + "\n" + product_currency + product_price + "\n" + product_sku + "\n" + product_discount;

    function dataURLtoFile(dataurl, file_name) {
        var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while(n--){
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], file_name, {type:mime});
    }

    function product_share(share_name,msg) {
        var u="{{Request::fullUrl()}}";
        switch (share_name) {
            case 'fb':
                window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t),'sharer','toolbar=0,status=0,width=626,height=436');
                break;
            case 'whatsapp':
                var message = encodeURIComponent(u) + " " + t;
                // var whatsapp_url = "whatsapp://send?text=" + message;
                // window.location.href = whatsapp_url;

                var whatsapp_url = "https://wa.me/?text=" + message;
                openInNewTab(whatsapp_url);
                break;
            case 'twitter':
                window.open("https://twitter.com/intent/tweet?text="+t+"&url="+u,'sharer','toolbar=0,status=0,width=626,height=436')
                break;
        }
    }

    if (isMobile) {
        $("#mobile_share").show();
    }

    $("#productimages img").each(function(){
        if(this.src){product_images.push(this.src);}
    });

    if(product_images.length == 0){
        var x = document.getElementById("productimages").src;
        if(x){product_images.push(x);}
    }

    function toDataURL(src, callback, outputFormat) {
        var img = new Image();
        img.crossOrigin = 'Anonymous';
        img.onload = function() {
            var canvas = document.createElement('CANVAS');
            var ctx = canvas.getContext('2d');
            var dataURL;
            canvas.height = this.naturalHeight;
            canvas.width = this.naturalWidth;
            ctx.drawImage(this, 0, 0);
            dataURL = canvas.toDataURL(outputFormat);
            file = dataURLtoFile(dataURL, file_name);
            filesArray.push(file);
            // callback(dataURL);
        };

        img.src = src;
        if (img.complete || img.complete === undefined) {
            img.src = src;
        }
    }

    for(i=0;i<product_images.length;i++){
        toDataURL(product_images[i]);
    }

    // shareButton.addEventListener('click', event => {
        $('.shareBtn').click(function() {
        var share_config = {
            title: 'Simplisell',
            text: t,
            files: filesArray,
            url: u,
        };

        if (isMobile) {
            var ShareOptionModal = new bootstrap.Modal(document.getElementById('share_options_modal'));
            ShareOptionModal.toggle();

            $('.share-option').click(function() {
                if ($(this).attr('data-value') == 'img') {
                    share_config = {
                        title: 'Simplisell',
                        text: t,
                        files: filesArray,
                        url: u,
                    };
                    mobile_share(share_config);
                } else {
                    share_config = {
                        title: 'Simplisell',
                        text: t,
                        url: u,
                    };
                    mobile_share(share_config);
                }
            });


        } else {
            var u = "{{Request::fullUrl()}}";
            share_name = $(this).attr('data-label');
            switch (share_name) {
                case 'fb':
                    window.open('http://www.facebook.com/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t), 'sharer', 'toolbar=0,status=0,width=626,height=436');
                    break;
                case 'whatsapp':
                    var message = encodeURIComponent(u) + " " + t;
                    // var whatsapp_url = "whatsapp://send?text=" + message;
                    // window.location.href = whatsapp_url;

                    var whatsapp_url = "https://wa.me/?text=" + message;
                    openInNewTab(whatsapp_url);
                    break;
                case 'twitter':
                    window.open("https://twitter.com/intent/tweet?text=" + t + "&url=" + u, 'sharer', 'toolbar=0,status=0,width=626,height=436')
                    break;
            }
        }



    });

    function mobile_share(share_config) {
        if ((navigator.canShare) && (navigator.canShare({files: filesArray}))) {
            navigator.share(
                    share_config
                    // {
                    //     title: 'Simplisell',
                    //     text: t,
                    //     files: filesArray,
                    //     url: u
                    // }
                ).then(() => {
                    // console.log("thanks ");
                })
                .catch(console.error);
        } else {
            alert(navigator.share);
        }
    }

    $(".niceScroll").niceScroll({
        cursorcolor: "grey", // change cursor color in hex
        cursoropacitymin: 0, // change opacity when cursor is inactive (scrollabar "hidden" state), range from 1 to 0
        cursoropacitymax: 1, // change opacity when cursor is active (scrollabar "visible" state), range from 1 to 0
        cursorwidth: "5px", // cursor width in pixel (you can also write "5px")
        cursorborder: "1px solid #fff", // css definition for cursor border
        cursorborderradius: "5px", // border radius in pixel for cursor
        zindex: "auto", // change z-index for scrollbar div
        scrollspeed: 60, // scrolling speed
        mousescrollstep: 40, // scrolling speed with mouse wheel (pixel)
        touchbehavior: false, // enable cursor-drag scrolling like touch devices in desktop computer
        hwacceleration: true, // use hardware accelerated scroll when supported
        boxzoom: false, // enable zoom for box content
        dblclickzoom: true, // (only when boxzoom=true) zoom activated when double click on box
        gesturezoom: true, // (only when boxzoom=true and with touch devices) zoom activated when pinch out/in on box
        grabcursorenabled: true, // (only when touchbehavior=true) display "grab" icon
        autohidemode: true, // how hide the scrollbar works, possible values:
        background: "", // change css for rail background
        iframeautoresize: true, // autoresize iframe on load event
        cursorminheight: 32, // set the minimum cursor height (pixel)
        preservenativescrolling: true, // you can scroll native scrollable areas with mouse, bubbling mouse wheel event
        railoffset: false, // you can add offset top/left for rail position
        bouncescroll: false, // (only hw accell) enable scroll bouncing at the end of content as mobile-like
        spacebarenabled: true, // enable page down scrolling when space bar has pressed
        disableoutline: true, // for chrome browser, disable outline (orange highlight) when selecting a div with nicescroll
        horizrailenabled: true, // nicescroll can manage horizontal scroll
        railalign: "right", // alignment of vertical rail
        railvalign: "bottom", // alignment of horizontal rail
        enabletranslate3d: true, // nicescroll can use css translate to scroll content
        enablemousewheel: true, // nicescroll can manage mouse wheel events
        enablekeyboard: true, // nicescroll can manage keyboard events
        smoothscroll: true, // scroll with ease movement
        sensitiverail: true, // click on rail make a scroll
        enablemouselockapi: true, // can use mouse caption lock API (same issue on object dragging)
        cursorfixedheight: false, // set fixed height for cursor in pixel
        hidecursordelay: 400, // set the delay in microseconds to fading out scrollbars
        irectionlockdeadzone: 6, // dead zone in pixels for direction lock activation
        nativeparentscrolling: true, // detect bottom of content and let parent to scroll, as native scroll does
        enablescrollonselection: true, // enable auto-scrolling of content when selection text
        cursordragspeed: 0.3, // speed of selection when dragged with cursor
        rtlmode: "auto", // horizontal div scrolling starts at left side
        cursordragontouch: false, // drag cursor in touch / touchbehavior mode also
        oneaxismousemode: "auto",
        scriptpath: "", // define custom path for boxmode icons ("" => same script path)
        preventmultitouchscrolling: true, // prevent scrolling on multitouch events
        disablemutationobserver: false,
    });
    @php
        $shipdata = '';
        if(isset($meta_data['shipping_settings'])){
            foreach($meta_data['shipping_settings']['shipping_method'] as $val){
                $shipdata = $val;
            }
        }
    @endphp
    @if(!empty($shipdata))
            $('.cart').css("pointer-events", "none");
            $('.cart').css("opacity", 0.5);
    @endif
</script>
@endsection
