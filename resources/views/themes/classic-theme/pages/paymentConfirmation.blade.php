@extends('themes.'.$meta_data['themes']['name'].'.layouts.public')
@section ('styles')
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/plugins/forms/wizard.css')}}">

@endsection
@section('content')

@php
if($campaign->is_start == 1) {

$catalogLink = route('campaigns.published',[$campaign->public_link]);

}else{

$catalogLink = route('admin.campaigns.preview',[$campaign->preview_link]);

}


if($userSettings->is_start == 1) {

if(!empty($userSettings->customdomain)){

$homeLink = '//'.$userSettings->customdomain;

}else if(!empty($userSettings->subdomain)) {

$homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');

}else{

$homeLink = route('mystore.published',[$userSettings->public_link]);
}

}else{
$homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
}

if(empty($meta_data['settings']['emailid']))
$emailFrom = $campaign->user->email;
else
$emailFrom = $meta_data['settings']['emailid'];

if(empty($meta_data['settings']['contactnumber']))
$contactNumber = "";
else
$contactNumber = isset($meta_data['settings']['contactnumber_country_code']) ? '+'.$meta_data['settings']['contactnumber_country_code'].'-'.$meta_data['settings']['contactnumber'] : '+91'.'-'.$meta_data['settings']['contactnumber'];

@endphp
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <div class="breadcrumb-wrapper col-12">
                    <ol class="breadcrumb border-0">
                        <li class="breadcrumb-item"><a href="{{$homeLink}}">Home</a>
                        </li>
                        <li class="breadcrumb-item"><a href="{{$catalogLink}}">{!! $campaign->name == "Untitled"?"&nbsp;":$campaign->name !!}</a>
                        </li>
                        <li class="breadcrumb-item "><a href="{{url()->previous()}}">Checkout</a>
                        </li>
                        @if(isset($transaction_id))
                        <li class="breadcrumb-item active">Payment Confirmation
                        </li>
                        @else
                        <li class="breadcrumb-item active">Enquiry Submission
                        </li>
                        @endif
                    </ol>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card p-1">
    @if(isset($transaction_id))

    <div><a class="btn btn-outline-danger float-right" id="order_pdf_download" onclick="event.preventDefault();downloadpdf({{$order->id ?? $enquiry_id}})" href="" style="padding: 8px !important;border-radius: 8px;">
            <i style="color:red" class="fa fa-file-pdf-o"></i> Download Order PDF <i style="color:red" class="fa fa-download"></i></a></div>
    <h3 class="ml-2 text-center "><b>{{ $meta_data['checkout_settings']['order_confirmation_title'] ?? 'Payment Successful!' }}</b></h3>
    <h3 class="ml-2 text-center "><b>{{isset($order)?'Order id #'.$order->order_ref_id:''}}</b></h3>
    <h3 class="ml-2 text-center "><b>{{ !empty($transaction_id) ? 'Transaction id : '.$transaction_id:''}}</b></h3>

    <p class="h4 ml-2 text-center" style="line-height: 2;">
        {!! $meta_data['checkout_settings']['order_confirmation_description'] ?? 'Thanks for placing an order with us,our team will get in touch with you shortly to process the order.' !!}
        Meanwhile if you want to reach us please feel free to reach us at {{$emailFrom}}{{empty($contactNumber)?"":" or call us at Store ".$contactNumber}} or
        <a class="btn btn-success text-white " style=" font-size:18px;" href="https://api.whatsapp.com/send?phone={{isset($meta_data['settings']['whatsapp_country_code']) ? $meta_data['settings']['whatsapp_country_code'].$meta_data['settings']['whatsapp'] : '91'.$meta_data['settings']['whatsapp']}}&text={{$meta_data['settings']['enquiry_text'] ?? ''}}" target="_blank">
            <i class="ficon fa fa-whatsapp text-white "></i>&nbsp;Contact us on WhatsApp</a>
    </p><br>
    @else
    <h3 class="ml-2 text-center "><b>Enquiry Submitted Successfully!</b></h3></b></h3>
    <p class="h4 ml-2 text-center" style="line-height: 2;">
         Thanks for raising an enquiry with us,our team will get in touch with you shortly to process the enquiry.
        Meanwhile if you want to reach us please feel free to reach us at {{$emailFrom}}{{empty($contactNumber)?"":" or call us at Store ".$contactNumber}} or
        <a class="btn btn-success text-white " style=" font-size:18px;" href="https://api.whatsapp.com/send?phone={{isset($meta_data['settings']['whatsapp_country_code']) ? $meta_data['settings']['whatsapp_country_code'].$meta_data['settings']['whatsapp'] : '91'.$meta_data['settings']['whatsapp']}}&text={{$meta_data['settings']['enquiry_text'] ?? ''}}" target="_blank">
            <i class="ficon fa fa-whatsapp text-white "></i>&nbsp;Contact us on WhatsApp</a>
    </p><br>
    @endif
    <div class="col-lg-12" style="text-align:center"><a href='{{$homeLink}}' class='btn center-block btn-primary'>Continue Shopping</a></div>
</div>
@endsection

<script>
    function downloadpdf(order_id) {
        // console.log(order_id);
        url = "{{ route('customer_export_order_pdf',':order_id') }}";
        url = url.replace(':order_id', order_id);
        window.location.href = url;
    }
</script>