<style>
    .videoIcon{
        margin:auto;
        height:200px;
        bottom:0;
        top:0;
        left: 0;
        right: 0;        
    }
    @media (max-width: 576px) {
  .videocard {
     height: 410px;
     width: 320px; 
     float:left; 
     display: inline-block;
  }
}
.videosize{
    height:430px; 
    width:450px; 
    float:left; 
    display: inline-block;
}
  .showall{
    width: 90px;
     height: 100px;
     border-radius: 8px;
     border: 1px solid rgb(37, 117, 252);
     background-color: rgb(240, 245, 255);
     display: flex;
     flex-direction: column;
     padding: 12px;
     -webkit-box-align: center;
     align-items: center;
     -webkit-box-pack: center;
     justify-content: center;
     margin-left: 4px;
     margin-top: 30px;
     align-self: flex-start;
  }
  .chevron{
    background: blue;
    border-radius: 50%;
    width: 100px;
    height: 100px;      
      color:white;
  }
</style>
@php
$agent=new \Jenssegers\Agent\Agent();
if($campaign->is_start == 1) {
                        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
                        $catalogLink = route('campaigns.published',$campaign->public_link);
                        $videoLink = route('campaigns.publishedVideo',$campaign->public_link);
                        $showallLink = route('campaigns.showVideo',$campaign->public_link);                    
                    }else{
                        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
                        $catalogLink = route('admin.campaigns.preview',$campaign->preview_link);
                        $videoLink = route('campaigns.publishedVideo',$campaign->preview_link);
                        $showallLink = route('campaigns.showVideo',$campaign->preview_link);                    
                    }
if(empty($meta_data['settings']['currency']))
        $currency = "₹";
else
    if($meta_data['settings']['currency_selection'] == "currency_symbol")
        $currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
    else
        $currency = $meta_data['settings']['currency'];
@endphp
                @foreach ($video_card as $key => $card)
                    @php
                    $entitycard_count = 0; 
                    $video = $card->video;
                    $product = $card->product;
                    $video = $card->video;
                    if(!empty($video->thumbnail))
                    {
                       $img = $video->thumbnail->getUrl('thumb');
                    }
                    else{
                        $img = asset('XR/assets/images/video-placeholder.png');
                    }
                    @endphp
                    @php
                        $isProductExist = false; 
                    @endphp
                    @if($agent->isMobile())
                        <div class="card ecommerce-card ml-1 mr-1 mt-1 p-1 rounded  video_id_{{$video->id}} videocard">
                        @else
                        <div class="card ecommerce-card ml-1 mr-1 mt-1 p-1 rounded  video_id_{{$video->id}} videosize">
                        @endif
                        <div class ="card-content" style="overflow:hidden;">
                        <h4 class="text-truncate"><b>{!! $video->title !!}</b></h4>
                        <center>
                            <div class="" style="margin:auto;">
                                <a href="{{$showallLink}}?video-id={{$video->id}}"> <i class="feather icon-play-circle fa-5x position-absolute d-flex justify-content-center text-light videoIcon" ></i>
                                @if($img == asset('XR/assets/images/video-placeholder.png'))
                                    @if($agent->isMobile())
                                        <img class="nav-brand d-flex align-items-center rounded img-responsive" src="{{$img}}" style="height:200px; width:260px; cursor:pointer;"/></a>
                                    @else
                                        <img class="nav-brand d-flex align-items-center rounded img-responsive" src="{{$img}}" style="height:200px; width:420px; cursor:pointer;"/></a>
                                    @endif
                                @else
                                    @if($agent->isMobile())
                                        <img class="nav-brand d-flex align-items-center rounded img-responsive" src="{{$img}}" style="height:200px; width:260px; cursor:pointer;"/></a>
                                    @else
                                        <img class="nav-brand d-flex align-items-center rounded img-responsive" src="{{$img}}" style="height:200px; width:420px; cursor:pointer;"/></a>
                                    @endif
                                @endif
                            </div>
                        </center>
                        <hr>
                                @foreach($data as $key => $video_product)
                                @php
                                {
                                    if($video_product->video_id == $card->video_id)
                                        {
                                        $entitycard_count++;
                                        
                                            if($entitycard_count > 2){
                                                break;
                                            }
                                        }       
                                        $product = $video_product->product;
                                        if($product->downgradeable == 1){
                                            continue;
                                        }
                                        if($product->discount != null) {   
                                            $discount_price = $product->price * ($product->discount/100);
                                            $discounted_price = round(($product->price - $discount_price),2);
                                        }
                                        if(count($product->photo) > 0){
                                            $img = $product->photo[0]->getUrl('thumb');
                                            $objimg = asset('XR/assets/images/placeholder.png');
                                            $obj_id = "";
                                            $obj_3d = false;
                                        } else if(count($product->arobject) > 0){
                                            if(!empty($product->arobject[0]->object))
                                                if(!empty($product->arobject[0]->object->getUrl('thumb'))) {
                                                    $img = $product->arobject[0]->object->getUrl('thumb');
                                                }
                                        } else {                        
                                            $img = asset('XR/assets/images/placeholder.png');
                                            $objimg = asset('XR/assets/images/placeholder.png');
                                        }
                                        foreach ($product->arobject as $key => $arobject) {                        
                                            if(!empty($arobject->object)){
                                                if($arobject->ar_type == "face") {
                                                    if(!empty($arobject->object->getUrl('thumb'))) {
                                                        $objimg = $arobject->object->getUrl('thumb');
                                                        $obj_id = $arobject->id;
                                                    }                        
                                                }                               
                                            }    
                                            //echo $arobject->ar_type; 
                                            if($arobject->ar_type == "surface") {
                                                if(!empty($arobject->object_3d)) {
                                                    $obj_3d = true;
                                                }                        
                                            }                        
                                        }
                                        if($campaign->is_start == 1) {
                                            if(count($product->categories) > 0) {
                                                $categoryLink = route('campaigns.publishedProductByCategory',[$product->categories[0]['id'],$campaign->public_link]);
                                            }
                                            $catalogId = $campaign->public_link;
                                            $tryonLink = route('campaigns.publishedTryOn',$campaign->public_link)."?sku=".$product->id;
                                            $viewARLink = route('campaigns.publishedViewAR',$campaign->public_link)."?sku=".$product->id;
                                            $catalogDetailsLink = route('campaigns.publishedCatalogDetails',[$product->id,$campaign->public_link]);
                                            $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
                                        }else{
                                            if(count($product->categories) > 0) {
                                                $categoryLink = route('campaigns.publishedProductByCategory',[$product->categories[0]['id'],$campaign->preview_link]);
                                            }
                                            $catalogId = $campaign->preview_link;
                                            $tryonLink = route('admin.campaigns.previewTryOn',$campaign->preview_link)."?sku=".$product->id;
                                            $viewARLink = route('admin.campaigns.previewViewAR',$campaign->preview_link)."?sku=".$product->id;
                                            $catalogDetailsLink = route('admin.campaigns.previewCatalogDetails',[$product->id,$campaign->preview_link]);
                                            $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
                                        }
                                    }
                            @endphp
                            @if($video_product->video_id == $card->video_id && !empty($video_product->product))
                            @php    
                                $isProductExist = true;
                            @endphp
                                    <div style="width:100px; float:left;">   
                                        <p><div class='card pl-1 pr-1 m-0'><img class="img-responsive mw-80 mh-80 rounded" src="{{$img}}" style="height:100px" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';"  style="height:170px;cursor:pointer;" onclick="getProductDetails({{$video_product->id}},{{$campaign->id}},{{$card->video_id}})" data-toggle="modal" data-target="#productDetailsModal"></div>
                                            @if($product->discount != null)
                                                        <span class="pl-1">{!!empty($product->price)?"":$currency!!}</span><span class="">{{$discounted_price}}{!!empty($product->units)?"":" / $product->quantity $product->units"!!}</span>
                                                        &nbsp;<span class="text-muted"><s>{!!empty($product->price)?"":$currency!!}{{$product->price}}</s></span>
                                                        <!-- <span class="text-danger">{{$product->discount}}% OFF</span> -->
                                                    @else
                                                        <span class="pl-1">{!!empty($product->price)?"":$currency!!}</span><span class="">{!! $product->price ?? '&nbsp;' !!}{!!empty($product->units)?"":" / $product->quantity $product->units"!!}</span> 
                                                    @endif
                                        </p>
                                        </div>
                            @endif
                            @endforeach
                            @if($isProductExist == 'true')
                            <div class="text-center pt-1 showall" >                                
                                <a href="{{$showallLink}}?video-id={{$video->id}}" ><b>Show all </b><br><i class="feather icon-chevron-right chevron"></i>      
                                </a></div>
                            @else
                            <script type="text/javascript">
                                     document.getElementsByClassName("video_id_{{$video->id}}")[0].style.display = 'none';
                            </script>
                            @endif
                            </div>
                        </div>
                        </div>

                @endforeach 
                    
                
      
                


                
<script src="{{asset('XR/app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')}}">
<script>
    $('#productssearchTotal').empty().html({{$video_card->count()}});
// checkout quantity counter
  var quantityCounter = $(".quantity-counter"),
    CounterMin = 1,
    CounterMax = 10000;
  if (quantityCounter.length > 0) {
    quantityCounter.TouchSpin({
      min: CounterMin,
      max: CounterMax
    }).on('touchspin.on.startdownspin', function () {
      var $this = $(this);
      let min_qty = $(this).data("minimum_quantity") || 1;
      $this.trigger("touchspin.updatesettings", {min: min_qty})
      console.log(min_qty)
      $('.bootstrap-touchspin-up').removeClass("disabled-max-min");
      if ($this.val() <= min_qty) {
            $(this).siblings().find('.bootstrap-touchspin-down').addClass("disabled-max-min");
            // $(this).siblings().find('.bootstrap-touchspin-down').addClass("danger-max-min");
            // obj.removeItemFromCart(Number($(this).data('id')));
            // obj.totalCart();
            // $(this).closest('.view-in-cart').siblings('.add-to-cart').removeClass("d-none");
            // $(this).closest('.view-in-cart').siblings('.add-to-cart').addClass("d-inline-block");
            // $(this).closest('.view-in-cart').removeClass("d-inline-block");
            // $(this).closest('.view-in-cart').addClass("d-none");
            // e.stopImmediatePropagation();
        }
        obj.setCountForItem(Number($this.data('id')),$this.val());
        //$("#product_qty_"+$this.data('id')).val($this.val());
        
    }).on('touchspin.on.startupspin', function () {
      var $this = $(this);
      $('.bootstrap-touchspin-down').removeClass("disabled-max-min");
      if ($this.val() == CounterMax) {
        $(this).siblings().find('.bootstrap-touchspin-up').addClass("disabled-max-min");
      }
      obj.setCountForItem(Number($this.data('id')),$this.val());
      //$("#product_qty_"+$this.data('id')).val($this.val());
      
    }).on('change', function () {
      var $this = $(this);
    //   if ($this.val() == 1) {
    //     $(this).siblings().find('.bootstrap-touchspin-down').addClass("danger-max-min");
    //     }
      if ($this.val() == 0) {
            obj.setCountForItem(Number($this.data('id')),1);
        } else if ($this.val() > CounterMax) {
            obj.setCountForItem(Number($this.data('id')),CounterMax);      
      } else {
         obj.setCountForItem(Number($this.data('id')),$this.val());      
      }
    });
  }
  obj.setCountForItem = function(id, qty) {
    for(var i in cart) {
      if (cart[i].id === id) {
        cart[i].qty = qty;
        break;
      }
    }
    saveCart();
    obj.totalCart();
  };
  obj.removeItemFromCart = function(id) {
        for (var item in cart) {
            if (cart[item].id === id) {
                //   cart[item].qty --;
                //   if(cart[item].qty === 0) {
                cart.splice(item, 1);
                //   }
                //   break;
            }
        }
        saveCart();
    }
  
  $( document ).ready(function() {
    if (localStorage.getItem("shoppingCart") != null) {
        cart = JSON.parse(localStorage.getItem('shoppingCart'));
        var cartItemCount = count = cart.length;
        if(cart.length > 0) {
            var products = [];
            for(var item in cart) {
                var id = cart[item].id, qty = cart[item].qty;
                $("#product_qty_"+(id)).val(qty);
            }

        }
    }
    });

</script>