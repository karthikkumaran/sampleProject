<style>
    
.showall{
    width: 90px;
     height: 100px;
     border-radius: 8px;
     border: 1px solid rgb(37, 117, 252);
     background-color: rgb(240, 245, 255);
     display: flex;
     flex-direction: column;
     padding: 12px;
     -webkit-box-align: center;
     align-items: center;
     -webkit-box-pack: center;
     justify-content: center;
     margin-left: 4px;
     margin-top: 14px;
     align-self: flex-start;
  }
  .chevron{
    background: blue;
    border-radius: 50%;
    width: 100px;
    height: 100px;      
      color:white;
  }
</style>    
@if(!empty($video_data))
    @php 
        $agent=new \Jenssegers\Agent\Agent();
        if(empty($meta_data['settings']['currency']))
            $currency = "₹";
        else
            if($meta_data['settings']['currency_selection'] == "currency_symbol")
                $currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
            else
                $currency = $meta_data['settings']['currency'];

        $attributes = [
            'type' => null,
            'class' => 'iframe',
            'data-html5-parameter' => true
        ];
        $whitelist = []; $params = [];
        $videocard = 0;
    @endphp

    @foreach ($video_data as $key => $card)                    
        @php
        $video = $card->video;
        if(!empty($video->thumbnail))
        {
            $img = $video->thumbnail->getUrl('thumb');
        }
        else{
            $img = asset('XR/assets/images/video-placeholder.png');
        }
        @endphp
        @if($card->video_id != $video_id)
            @php
                $isproduct = false;
                $catalogLink = "";
            @endphp
            
            @foreach($data as $key => $show)
                @if($show->video_id == $card->video_id)
                    @php
                        $isproduct = true;
                    @endphp
                @endif
            @endforeach
            @if($isproduct == true)
                @php
                    $entitycard_count = 0;   
                    $videocard++;
                @endphp
                <div class="card ecommerce-card col-lg-12 col-md-12 col-sm-12 " style="width:100%;  display: inline-block; margin-bottom:0.5rem;">
                        <div class="card-header">
                            <h4 class="text-truncate"><b>{!! $video->title !!}</b></h4>
                        </div>
                        <div class="card-content">
                            <div style="overflow: hidden;">
                                {!! \LaravelVideoEmbed::parse( old('video_url', $video->video_url), $whitelist, $params,$attributes ) !!}
                            </div>
                        </div> 
                        <!-- <div class="cart rounded text-center" style=" background-color:blueviolet;" >
                            <a href="" style="color:white;" >View video product</a>
                        </div>   -->
                        <div class="card-footer" style="background-color:white; padding:0.5rem;">
                        @foreach($data as $key => $video_product)
                                @php
                                if(!empty($video_product->product) && $video_product->video_id == $card->video_id)
                                {
                                    $entitycard_count++;
                                    if($agent->isMobile()){
                                        if($entitycard_count > 2){
                                            break;
                                        }
                                    }
                                    else{
                                        if($entitycard_count > 3){
                                            break;
                                        }
                                    }
                                        $product = $video_product->product;
                                        if($product->downgradeable == 1){
                                            continue;
                                        }
                                        if($product->discount != null) {   
                                            $discount_price = $product->price * ($product->discount/100);
                                            $discounted_price = round(($product->price - $discount_price),2);
                                        }
                                        if(count($product->photo) > 0){
                                            $img = $product->photo[0]->getUrl('thumb');
                                            $objimg = asset('XR/assets/images/placeholder.png');
                                            $obj_id = "";
                                            $obj_3d = false;
                                        } else if(count($product->arobject) > 0){
                                            if(!empty($product->arobject[0]->object))
                                                if(!empty($product->arobject[0]->object->getUrl('thumb'))) {
                                                    $img = $product->arobject[0]->object->getUrl('thumb');
                                                }
                                        } else {                        
                                            $img = asset('XR/assets/images/placeholder.png');
                                            $objimg = asset('XR/assets/images/placeholder.png');
                                        }
                                        foreach ($product->arobject as $key => $arobject) {                        
                                            if(!empty($arobject->object)){
                                                if($arobject->ar_type == "face") {
                                                    if(!empty($arobject->object->getUrl('thumb'))) {
                                                        $objimg = $arobject->object->getUrl('thumb');
                                                        $obj_id = $arobject->id;
                                                    }                        
                                                }                               
                                            }    
                                            //echo $arobject->ar_type; 
                                            if($arobject->ar_type == "surface") {
                                                if(!empty($arobject->object_3d)) {
                                                    $obj_3d = true;
                                                }                        
                                            }                        
                                        }
                                        if($campaign->is_start == 1) {
                                            if(count($product->categories) > 0) {
                                                $categoryLink = route('campaigns.publishedProductByCategory',[$product->categories[0]['id'],$campaign->public_link]);
                                            }
                                            $catalogId = $campaign->public_link;
                                            $tryonLink = route('campaigns.publishedTryOn',$campaign->public_link)."?sku=".$product->id;
                                            $viewARLink = route('campaigns.publishedViewAR',$campaign->public_link)."?sku=".$product->id;
                                            $catalogDetailsLink = route('campaigns.publishedCatalogDetails',[$product->id,$campaign->public_link]);
                                            $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
                                        }else{
                                            if(count($product->categories) > 0) {
                                                $categoryLink = route('campaigns.publishedProductByCategory',[$product->categories[0]['id'],$campaign->preview_link]);
                                            }
                                            $catalogId = $campaign->preview_link;
                                            $tryonLink = route('admin.campaigns.previewTryOn',$campaign->preview_link)."?sku=".$product->id;
                                            $viewARLink = route('admin.campaigns.previewViewAR',$campaign->preview_link)."?sku=".$product->id;
                                            $catalogDetailsLink = route('admin.campaigns.previewCatalogDetails',[$product->id,$campaign->preview_link]);
                                            $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
                                        }
                                    }
                                    
                            @endphp

                                    @if($video_product->video_id == $card->video_id && !empty($video_product->product))
                                    @php    
                                        $isProductExist = true;
                                    @endphp
                                        @if($agent->isMobile())
                                         <div style="width:80px; float:left;">
                                        @else   
                                         <div style="width:100px; float:left;">  
                                        @endif 
                                        <p><div class='card   pl-0 pr-0 ' style="margin:0.5rem; border:1px solid #F8F8F8;"><img class="img-responsive mw-80 mh-80 rounded" src="{{$img}}" style="height:100px" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';"  style="height:170px;cursor:pointer;" onclick="getProductDetails({{$video_product->id}},{{$campaign->id}},{{$card->video_id}})" data-toggle="modal" data-target="#productDetailsModal"></div>
                                            @if($product->discount != null)
                                                        <span class="pl-1">{!!empty($product->price)?"":$currency!!}</span><span class="">{{$discounted_price}}{!!empty($product->units)?"":" / $product->quantity $product->units"!!}</span>
                                                        &nbsp;<span class="text-muted"><s>{!!empty($product->price)?"":$currency!!}{{$product->price}}</s></span>
                                                        <!-- <span class="text-danger">{{$product->discount}}% OFF</span> -->
                                            @else
                                                        <span class="pl-1">{!!empty($product->price)?"":$currency!!}</span><span class="">{!! $product->price ?? '&nbsp;' !!}{!!empty($product->units)?"":" / $product->quantity $product->units"!!}</span> 
                                            @endif
                                        </p>
                                        </div>
                                    @endif
                                @endforeach
                                
                                @if($isProductExist == true)
                                <div class="text-center pt-1 showall" >
                                
                                <a href="{{$catalogLink}}?video-id={{$video->id}}" ><b>Show all </b><br><i class="feather icon-chevron-right chevron"></i>      
                                </a></div>
                                @else
                                <script type="text/javascript">
                                     document.getElementsByClassName("video_id_{{$video->id}}")[0].style.display = 'none';
                                </script>
                                @endif
                            
                        </div>    
                </div>
            @endif  
            
        @endif  
    @endforeach
    @if($videocard == 0)
        <div class=" d-flex justify-content-center text-center text-light m-1">
                <h4><b class="text-light">There is no related video available</b></h4>   
        </div>
    @endif
@endif    