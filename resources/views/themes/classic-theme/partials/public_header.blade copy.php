<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="navbar-collapse" id="navbar-mobile">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                        <ul class="nav navbar-nav">
                            <!-- <li class="nav-item mobile-menu d-xl-none mr-auto">
                        <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ficon feather icon-menu"></i></a> 
                    </li> -->
                    <li class="nav-item mr-auto">
                    @if(!empty($meta_data['storefront']))
                        @if(!empty($userSettings->getFirstMediaUrl('storelogo')))
                            <a class="nav-link" href="{{$homeLink}}"><i class="ficon"></i><img class=" lazyload" loading="lazy" data-src="{{ $userSettings->getFirstMediaUrl('storelogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                                onerror="this.onerror=null;this.src='{{asset('XR/app-assets/images/logo/logo.png')}}';" alt="Logo" style="height: 50px;"></a>
                        @else
                            <a class="nav-link" href="{{$homeLink}}"><i class="ficon"></i><h5 style="color: {{ $meta_data['splash']['brand_color'] ?? '' }}"><b>{{$meta_data['storefront']['storename']}}</b></h5></a>
                        @endif
                    @endif
                    </li> 
                        </ul>
                    </div>
                    <ul class="nav navbar-nav float-right">
                        <!-- <li class="nav-item nav-search {{request()->is('s/*') || request()->is('admin/storePreview/*') ? 'd-none' : ''}}"><a class="nav-link nav-link-search"><i
                                    class="ficon feather icon-search"></i></a>
                            <div class="search-input">
                                <div class="search-input-icon"><i class="feather icon-search primary"></i></div>
                                <input class="input" id="CatalogProductSearch" type="text" placeholder="Search Product" tabindex="-1"
                                    data-search="template-list">
                                <div class="search-input-close"><i class="feather icon-x"></i></div>
                                <ul class="search-list search-list-main"></ul>
                            </div>
                        </li>  -->
                      @if(!empty($checkoutLink) && !request()->is('register/*') && !request()->is('login/*'))
                        <li class="dropdown dropdown-notification nav-item">
                            <a class="nav-link nav-link-label" href="{{$checkoutLink}}">
                                <i class="ficon feather icon-shopping-cart"></i>
                                <span class="badge badge-pill badge-primary badge-up cart-item-count"></span></a>
                        </li>
                        {{--  <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i class="ficon feather icon-maximize"></i></a></li> --}}
                        @endif 
                        @if(Auth::guard('customer')->check())
                       <li class="nav-item">

                            <a class="nav-link btn btn-outline-dark p-1  rounded-pill" href="{{ route('customer.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" style="margin-block: 10px;margin-inline: 8px;">
                            {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('customer.logout') }}" method="POST" style="display: none;">
                            @csrf
                            </form>
                        </li> 

                        @else
                {{--  <li class="nav-item ">
    
                        <a href="javascript:void(0);" class="nav-link btn btn-outline-dark p-1  rounded-pill" id="registerButton"  style="margin-block: 10px;margin-inline: 8px;">
                            Sign Up
                        </a> 
                        </li> --}}
                         <li class="nav-item">
        
                            <a href="javascript:void(0);" class="nav-link btn btn-outline-dark p-1  rounded-pill" id="loginButton"  style="margin-block: 10px;margin-inline: 8px;">
                            Login
                            </a> 
                        </li>
                        @endif
                        @if(!empty($meta_data['settings']['whatsapp']) && !request()->is('register/*') && !request()->is('login/*'))
                        <li class="nav-item">
                            <a class="nav-link btn btn-outline-dark btn-white list-view-btn view-btn " href="https://api.whatsapp.com/send?phone={{isset($meta_data['settings']['whatsapp_country_code']) ? $meta_data['settings']['whatsapp_country_code'].$meta_data['settings']['whatsapp'] : '91'.$meta_data['settings']['whatsapp']}}&text={{$meta_data['settings']['enquiry_text'] ?? ''}}" target="_blank" style="border-radius: 24px;padding: 10px 20px;margin: 10px;">
                                <img src="{{asset('/XR/app-assets/images/icons/whatsapp.svg')}}" style="width: 24px;"><b>&nbsp;Enquire</b>
                            </a>
                           </li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </nav>


   

<!-- Modal -->
