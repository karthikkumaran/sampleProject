@section('styles')
@parent
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/plugins/file-uploaders/dropzone.css')}}">
<style>
    .dropzone .dz-message {
        height: auto !important;
    }
</style>
@endsection

<div class="modal fade ecommerce-application" id="productDetailsModal" tabindex="-1" role="dialog" aria-labelledby="productDetailsModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="productDetailsModalTitle">Product Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="row" id="productDetails">

                </div>
            </div>
        </div>
    </div>
</div>

@section('scripts')
@parent

<script>

    var page_url = window.location.pathname;
    // shareable_link=page_url.slice(page_url.lastIndexOf('/')+1);
    var shareable_link = "";
    var campaign_id = "";
    @if(!empty($campaign))
        @if($userSettings->is_start == 1)
            shareable_link = "{{$campaign->public_link}}";
        @else
            shareable_link = "{{$campaign->preview_link}}";
        @endif
    @endif
    var preview = 0;
    var u = window.location.href;
    if (u.includes("preview")) {
        preview = 1;
    }

    function getProductDetails(card_id,campaign_id,video_id="") {
        $('.loading1').show();
        var q = "";
     q = '&card_id= ' + card_id + '&preview=' + preview + '&shareable_link=' + shareable_link + '&campaign_id=' + campaign_id + '&video_id=' + video_id;
        $.ajax({
            url: "{{route('admin.campaigns.ajaxProductDetails')}}" + '?' + q,
            type: "get",
            datatype: "html"
        }).done(function(data) {
            $("#productDetails").empty().html(data);
            // $("#product_list").append(data);
            url = window.location.pathname;
            slink = url.slice(url.lastIndexOf('/') + 1);
            slink = url=="/"? window.location.hostname : url.slice(url.lastIndexOf('/') + 1);
            productId = $('.add-to-cart').data("id");
            track_events("product_details", productId, slink);


            $('.loading1').hide();
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            $('.loading1').hide();
            // alert('No response from server');
        });
    }
</script>
@endsection
