<!--register Modal -->
<div class="modal fade" id="registerModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="registerModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm ">
        <form method="POST" action="javascript:void(0)" id="registerform" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header bg-transparent text-center">
                    <p class="modal-title fw-bolder text-dark w-100 h3" id="loginModalLabel" style="font-weight: 800;">Sign Up</p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>
                <div id="register-flash-message"></div>
                <div class="modal-body">


                    <!-- <div class="row">
                <div class="col">
                    <a href="javascript:void(0)" class="btn btn-outline-dark float-right bg-transparent">
                        <img src="{{asset('/XR/app-assets/images/icons/facebook-icon.svg')}}" style="width: 15px;"></i> Facebook </a>    
                </div>
                <div class="col">
                    <a href="javascript:void(0)" class="btn btn-outline-dark float-left bg-transparent">
                        <img src="{{asset('/XR/app-assets/images/icons/google-icon.svg')}}" style="width: 15px;"> Google </a> 
                </div>               
            </div> -->

                    <!-- <hr class="hr-text mt-0" data-content="or" /> -->

                    @csrf
                    <div class="form-group">
                        <label class="form-label" for="register-username">Your name</label>
                        <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" required autofocus placeholder="{{ trans('global.user_name') }}" value="{{ old('name', null) }}">
                        <div class="invalid-feedback">
                            Please enter your name
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-6">
                            <label for="country_code">Country code</label>
                            <select id="country_code" class="form-control {{ $errors->has('country_code') ? ' is-invalid' : '' }}" name="country_code" required>
                                @foreach(App\Customer::MOBILE_COUNTRY_CODE as $key=>$value)
                                    <option value="{{$key}}">{{$value}}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Please enter your mobile number
                            </div>
                        </div>
                        <div class="col-6">
                            <label class="form-label " for="register-email">Mobile</label>
                            <input class="form-control {{ $errors->has('mobile') ? ' is-invalid' : '' }}" type="tel" name="mobile" placeholder="8888888888" pattern="[0-9]{10}" maxlength="10" required value="{{ old('mobile', null) }}" />
                            <div class="invalid-feedback">
                                Please enter your mobile number
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="register-email">Email</label>
                        <input type="email" name="email" id="remail" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required placeholder="{{ trans('global.login_email') }}" value="{{ old('email', null) }}">
                        <div class="invalid-feedback">
                            Please enter your email id
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="register-password">Password</label>
                        <div class="input-group input-group-merge form-password-toggle">
                            <input type="password" name="password" minlength="6" id="rpassword" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required placeholder="{{ trans('global.login_password') }}" value="{{ old('password', null) }}" autocomplete="off">
                            <div class="input-group-append">
                                <span class="input-group-text cursor-pointer">
                                    <i class="fa fa-eye togglePassword" data-id="rpassword" aria-hidden="true"></i></span>
                            </div>
                            <div class="invalid-feedback">
                                Please enter a password
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="register-password">Confirm Password</label>
                        <div class="input-group input-group-merge form-password-toggle">
                            <input type="password" name="cpassword" minlength="6" id="cpassword" class="form-control{{ $errors->has('cpassword') ? ' is-invalid' : '' }}" required placeholder="{{ trans('global.login_password') }}" value="{{ old('cpassword', null) }}" autocomplete="off">
                            <div class="input-group-append">
                                <span class="input-group-text cursor-pointer">
                                    <i class="fa fa-eye togglePassword" data-id="cpassword" aria-hidden="true"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                                <input  id="privacy-policy" type="checkbox" required/>
                                <label class="form-label" for="privacy-policy">
                                    I have read and agree the <a href="{{route('store.storePrivacyPolicy', $sLink)}}" target="_blank">&nbsp;privacy policy</a> and <a href="{{route('store.storeTermsAndConditions', $sLink)}}" target="_blank">terms & conditions</a>
                                </label>
                        </div>
                    <div style="margin-top: -20px;font-size:10px;" id="CheckPasswordMatch"></div>

                    <input type="hidden" name="slink" value="{{$sLink}}">

                </div>

                <button type="submit" id="signup" class="btn btn-primary mr-1 ml-1" tabindex="5">Sign Up</button>
                <p class="text-center mt-2"><span>Have an account?</span>
                    <a href="javascript:void(0);" class="btn btn-link" id='loginLink' style="padding-inline: 0px;padding-block:6px;"><span>&nbsp; LOGIN</span></a>
                </p>

            </div>
        </form>

    </div>
</div>

<!-- login modal -->
<div class="modal fade" id="loginModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="loginModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm ">
        <form method="POST" id="loginform" action="javascript:void(0);" enctype="multipart/form-data">
            @csrf
            <div class="modal-content">
                <div class="modal-header bg-transparent text-center">
                    <p class="modal-title fw-bolder text-dark w-100 h3" id="loginModalLabel" style="font-weight: 800;">Log In</p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @if(session('message'))
                <div class=" mx-1 my-1 alert alert-{{session('class')}}" role="alert">
                    {{ session('message') }}
                </div>
                @endif
                <div id="login-flash-message"></div>

                <div class="modal-body">

                 {{--   <div class="d-flex justify-content-center">
                            <a href="{{ route('customer.social.login','facebook') }}" class="btn btn-facebook mx-1"><span class="fa fa-facebook">acebook</span></a>
                            <a href="{{ route('customer.social.login','google') }}" class="btn btn-google mx-1"><span class="fa fa-google">oogle</span></a>
                    </div>
                    <hr class="hr-text mt-0" data-content="or" /> --}}

                    <div class="form-group">
                        <label class="form-label" for="register-email">Email</label>
                        <input type="email" name="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" required placeholder="{{ trans('global.login_email') }}" value="{{ old('email', null) }}">
                        <div class="invalid-feedback">
                            Please enter your email id
                        </div>
                    </div>

                    <div class="form-group mb-0">
                        <label class="form-label" for="register-password">Password</label>
                        <div class="input-group input-group-merge form-password-toggle">
                            <input type="password" name="password" id="lpassword" minlength="6" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" required placeholder="{{ trans('global.login_password') }}" autocomplete="off">
                            <div class="input-group-append">
                                <span class="input-group-text cursor-pointer">
                                    <i class="fa fa-eye togglePassword" data-id="lpassword" aria-hidden="true"></i>
                                </span>
                            </div>
                            <div class="invalid-feedback">
                                Please enter your password
                            </div>
                        </div>
                    </div>

                    <!-- <div class="form-group mb-0">
                <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" name="remember" id="remember" type="checkbox" tabindex="4" />
                    <label class="custom-control-label" for="remember">Remember Me</label>
                    </div>
            </div>  -->
                <input type="hidden" name="slink" value="{{$sLink ?? ''}}">
                </div>

                <button type="submit" id='login' class="btn btn-primary mr-1 ml-1" tabindex="5">LOG IN</button>
                <a class="btn btn-link p-1" href="javascript:void(0);" id='forgot_password' style="padding-inline: 0px;padding-block:6px;"><span>&nbsp;Forgot password?</span></a>

                <h6 class="text-center"><span>New to our website?</span>
                    <a href="javascript:void(0);" class="btn btn-danger" id='registerLink' ><span>Sign Up</span></a>
                </h6>

            </div>
        </form>
    </div>
</div>


<!-- reset modal -->
<div class="modal fade" id="forgotPasswordModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="forgotPasswordModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm ">
        <form method="POST" id="forgotPasswordform" action="javascript:void(0);" enctype="multipart/form-data">
            @csrf
            <div class="modal-content">
                <div class="modal-header bg-transparent text-center">
                    <p class="modal-title fw-bolder text-dark w-100 h3" id="loginModalLabel" style="font-weight: 800;">{{ trans('global.reset_password') }}</p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="forgotPassword-flash-message"></div>


                <div class="modal-body">

                    <p class="">Please enter your email address and we'll send you instructions on how to reset your password.</p>

                    <input type="hidden" name="store_id" value="{{isset($campaign)? $campaign->team_id : $userSettings->user->team_id}}">
                    <div class="form-label-group">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" required autocomplete="email" autofocus placeholder="{{ trans('global.login_email') }}">

                        <label for="inputEmail">Email</label>
                    </div>

                    <a href="javascript:void(0);" class="btn btn-link btn-sm p-1 btn-outline-primary float-left" id="forgotPwModal_loginLink">Back to Login</a>

                    <button type="submit" id="resetPassword" class="btn btn-primary btn-sm p-1 float-right">Recover Password</button>
                </div>
            </div>
        </form>
    </div>
</div>