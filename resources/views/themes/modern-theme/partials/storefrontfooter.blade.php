<!-- Internet Connection Status-->
<div class="internet-connection-status" id="internetStatus"></div>

@php
  if($userSettings->is_start == 1) 
  {
    if(!empty($userSettings->customdomain))
    {
      $homeLink = '//'.$userSettings->customdomain;
    }
    else if(!empty($userSettings->subdomain)) 
    {
        $homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
    }
    else
    {
      $homeLink = route('mystore.published',[$userSettings->public_link]);
    }
  }
  else
  {
    $homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
  }
    
  if(!empty($meta_data['settings']['whatsapp'])){
    $chat = "https://api.whatsapp.com/send?phone={{$meta_data['settings']['whatsapp']}}";
  }else{
    $chat = "";
  } 
@endphp

<!-- Footer Nav-->
<div class="footer-nav-area" id="footerNav">
  <div class="container h-100 px-0">
    <div class="suha-footer-nav h-100 mt-2">
      <span class="float-md-left d-block d-md-block mt-2">COPYRIGHT &copy; {{ date('Y') }}
        <a class="text-bold-800 grey darken-2" href="{{$homeLink}}">{{ $meta_data['storefront']['storename'] ?? "Store" }},</a>
        All rights Reserved. Created on <a class="text-bold-800 grey darken-2" href="{{env('APP_URL')}}" target="_blank">Simpli Sell</a>
      </span>
      <span class="float-md-right d-md-block ">
        <button type="button" class="btn btn-icon btn-xl  mr-1 mb-1" style="height:35px; width:60px;" onclick='share("fb","")'><i class="lni lni-facebook"></i></button>
        <button type="button" class="btn btn-icon btn-circle  mr-1 mb-1" style="height:35px; width:60px;" onclick='share("twitter","")'><i class="lni lni-twitter"></i></button>
        <button type="button" class="btn btn-icon btn-circle  mr-1 mb-1" style="height:35px; width:60px;" onclick='share("whatsapp","")'><i class="lni lni-whatsapp"></i></button>
      </span>
    </div>
  </div>
</div>
