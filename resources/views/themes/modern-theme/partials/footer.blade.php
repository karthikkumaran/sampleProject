<!-- Internet Connection Status-->
<div class="internet-connection-status" id="internetStatus"></div>

@php
  if($userSettings->is_start == 1) 
  {
    if(!empty($userSettings->customdomain))
    {
      $homeLink = '//'.$userSettings->customdomain;
    }
    else if(!empty($userSettings->subdomain)) 
    {
        $homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
    }
    else
    {
      $homeLink = route('mystore.published',[$userSettings->public_link]);
    }
  }
  else
  {
    $homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
  }
    
  if($campaign->is_start == 1) 
  {
    $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
  }
  else
  {
    $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
  }
 
  if(!empty($meta_data['settings']['whatsapp'])){
    $chat = "https://api.whatsapp.com/send?phone={{$meta_data['settings']['whatsapp']}}";
  }else{
    $chat = "";
  } 
@endphp

<!-- Footer Nav-->
<div class="footer-nav-area" id="footerNav">
      <div class="container h-100 px-0">
        <div class="suha-footer-nav h-100">
          <ul class=" foot h-100 d-flex align-items-center justify-content-between pl-0">
            <li class="{{ request()->is('s') || request()->is('s/*') ? 'active' : '' }}"><a   href="{{ $homeLink }}"><i class="lni lni-home"></i>Home</a></li>
            <li><a href="{{$chat}}"><i class="lni lni-whatsapp"></i>Chat</a></li>
            <li class="{{ request()->is('c') || request()->is('c/*') ? 'active' : '' }}"><a  href="{{ $checkoutLink }}"><span class="badge badge-primary cart-item-count position-absolute" ></span><i class="lni lni-shopping-basket"></i>Cart</a></li>               
          </ul>
        </div>
    </div>
</div>


