<style>
        #loading-bg {
            width: 100%;
            height: 100%;

            background: {{ $meta_data['splash']['bg_color'] ?? '#FFF' }};
            display: block;
            position: absolute;
            z-index: 19;
        }

        .loading-logo {
            position: absolute;
            left: calc(50% - 73px);
            top: 40%;
        }

        .loading-logo img {
            width: 150px;
            max-width: 150px;
        }

        .loading1 {
            position: absolute;
            left: calc(50% - 35px);
            top: 50%;
            width: 55px;
            height: 55px;
            /* border-radius: 50%;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            border: 3px solid transparent; */
        }

        .loading1 .effect-1,
        .loading1 .effect-2 {
            position: absolute;
            width: 100%;
            height: 100%;
            border: 3px solid transparent;
            border-left: 3px solid rgba(121, 97, 249, 1);
            border-radius: 50%;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }

        .loading1 .effect-1 {
            animation: rotate 1s ease infinite;
        }

        .loading1 .effect-2 {
            animation: rotateOpacity 1s ease infinite .1s;
        }

        .loading1 .effect-3 {
            position: absolute;
            width: 100%;
            height: 100%;
            border: 3px solid transparent;
            border-left: 3px solid rgba(121, 97, 249, 1);
            -webkit-animation: rotateOpacity 1s ease infinite .2s;
            animation: rotateOpacity 1s ease infinite .2s;
            border-radius: 50%;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }

        .loading1 .effects {
            transition: all .3s ease;
        }

        @keyframes rotate {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(1turn);
                transform: rotate(1turn);
            }
        }

        @keyframes rotateOpacity {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
                opacity: .1;
            }

            100% {
                -webkit-transform: rotate(1turn);
                transform: rotate(1turn);
                opacity: 1;
            }
        }

    </style>
<!-- Preloader-->
<div class="preloader" id="preloader">
  <div id="loading-bg">
        <div class="loading-logo">
        @if(!empty($meta_data['splash']))    
        @if($meta_data['splash']['brand_type'] == 2 && $meta_data['splash']['brand_name'] != "")
            <h1 style="color: {{ $meta_data['splash']['brand_color'] ?? '' }}">{{$meta_data['splash']['brand_name']}}</h1>
        @elseif(empty($userSettings->getFirstMediaUrl('splashlogo')) && $meta_data['splash']['brand_name'] != "")
            <h1 style="color: {{ $meta_data['splash']['brand_color'] ?? '' }}">{{$meta_data['splash']['brand_name']}}</h1>
        @elseif(!empty($userSettings->getFirstMediaUrl('splashlogo')))
            <img src="{{ $userSettings->getFirstMediaUrl('splashlogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                onerror="this.onerror=null;this.src='{{asset('XR/app-assets/images/logo/logo.png')}}';" alt="Logo">
        @endif
        @elseif(!empty($userSettings->getFirstMediaUrl('splashlogo')))
            <img src="{{ $userSettings->getFirstMediaUrl('splashlogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                onerror="this.onerror=null;this.src='{{asset('XR/app-assets/images/logo/logo.png')}}';" alt="Logo">
        @endif
        </div>
        <!-- <div class="loading1">
            <div class="effect-1 effects"></div>
            <div class="effect-2 effects"></div>
            <div class="effect-3 effects"></div>
        </div> -->
        <div class="loading1">
            <!-- <div class="spinner-grow text-danger w-100 h-100" role="status">
                <span class="sr-only">Loading...</span>
            </div> -->
            <div class="spinner-{{ $meta_data['splash']['loader_type'] ?? 'border' }} w-100 h-100"
                style="color: {{ $meta_data['splash']['loader_color'] ?? '' }}" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
   </div>
</div>
@php
  if($userSettings->is_start == 1)
    {
        //dd($userSettings);
        if(!empty($userSettings->customdomain))
        {
            $homeLink = '//'.$userSettings->customdomain;
        }
        else if(!empty($userSettings->subdomain)) 
        {
            $homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
        }
        else
        {
            $homeLink = route('mystore.published',[$userSettings->public_link]);
         
        }
    }
    else
    {
        $homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
        
    }
    if(!empty($meta_data['settings']['whatsapp'])){
      $chat = "https://api.whatsapp.com/send?phone={{$meta_data['settings']['whatsapp']}}";
    }else{
      $chat = "";
    }
@endphp
<!-- Header Area-->
<div class="header-area" id="headerArea">
        <div class="container h-100 d-flex align-items-center justify-content-between">
            <!-- Back Button-->
            <a href="{{ $homeLink }}"><i class="lni lni-home"></i></a>
<!-- Navbar Toggler-->
<div class="suha-navbar-toggler d-flex justify-content-between flex-wrap" id="suhaNavbarToggler"><span></span><span></span><span></span></div>
      </div>
    </div>
    <!-- Sidenav Black Overlay-->
    <div class="sidenav-black-overlay"></div>
    <!-- Side Nav Wrapper-->
    <div class="suha-sidenav-wrapper" id="sidenavWrapper">
      <!-- Sidenav Profile-->
      <div class="sidenav-profile">
      <div class="user-profile">
                    @if(!empty($meta_data['splash']))
                      @if($meta_data['splash']['brand_type'] == 2 && $meta_data['splash']['brand_name'] != "")
                        <h1 style="color: {{ $meta_data['splash']['brand_color'] ?? '' }}">{{$meta_data['splash']['brand_name']}}</h1>
                      @else
                        <img  src="{{ $userSettings->getFirstMediaUrl('splashlogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                            onerror="this.onerror=null;this.src='{{asset('XR/app-assets/images/logo/logo.png')}}';" alt="Logo" style="height: 90px;">
                      @endif
                    @else
                        <img class=" lazyload" loading="lazy" src="{{ $userSettings->getFirstMediaUrl('splashlogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                            onerror="this.onerror=null;this.src='{{asset('XR/app-assets/images/logo/logo.png')}}';" alt="Logo" style="height: 90px;">
                    @endif
          <!-- <img  src="{{asset('XR/app-assets/images/logo/logo.png')}}" alt=""> -->
        </div>
        <br>
        <div class="user-info">
          <h6 class="user-name mb-0">{{ $meta_data['storefront']['storename'] ?? 'Store' }}</h6>
          
        </div>
      </div>
      <!-- Sidenav Nav-->
    <ul class="sidenav-nav pl-0">
        <li class="active"><a href="{{ $homeLink }}"><i class="lni lni-home"></i>Home</a></li>
        <li><a href="https://api.whatsapp.com/send?phone={{$meta_data['settings']['whatsapp'] ?? ''}}&text={{$meta_data['settings']['enquiry_text'] ?? ''}}" target="_blank"><i class="lni lni-whatsapp"></i>Chat</a></li>
        
        <li><a><i class="lni lni-night"></i>Night mode&nbsp;&nbsp;
            <div class="single-settings d-flex align-items-center justify-content-between data-content">
                <div class="toggle-button-cover">
                    <div class="button r">
                        <input class="checkbox" id="darkSwitch" type="checkbox">
                            <div class="knobs"></div>
                            <div class="layer"></div>
                        </div>
                    </div>
                </div>
            </div>
        </a></li>
    </ul>
        </div> 
</div>

