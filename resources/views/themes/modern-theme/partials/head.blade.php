<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, viewport-fit=cover, shrink-to-fit=no">
    <meta name="description" content="Suha - Multipurpose Ecommerce Mobile HTML Template">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="theme-color" content="#100DD1">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <!-- The above tags *must* come first in the head, any other head content must come *after* these tags-->
    <!-- Title-->
    <title>{{ $meta_data['storefront']['storename'] ?? 'Store' }}</title>
    <!-- <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon.png') }}"> -->
    <!-- Favicon-->
    <!-- <link rel="icon" href="{{ asset('favicon.png') }}"> -->
    <!-- Apple Touch Icon-->
    <link rel="apple-touch-icon" href="{{ asset('themes/'.$meta_data['themes']['name'].'/img/icons/icon-96x96.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('themes/'.$meta_data['themes']['name'].'/img/icons/icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="167x167" href="{{ asset('themes/'.$meta_data['themes']['name'].'/img/icons/icon-167x167.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('themes/'.$meta_data['themes']['name'].'/img/icons/icon-180x180.png') }}">
    <!-- Stylesheet-->
    <link rel="stylesheet" href="{{ asset('themes/'.$meta_data['themes']['name'].'/style.css') }}" >
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('themes/'.$meta_data['themes']['name'].'/css/toastr.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('themes/'.$meta_data['themes']['name'].'/css/sweetalert2.min.css')}}">
    <!-- Web App Manifest-->
    <link rel="manifest" href="{{ asset('themes/'.$meta_data['themes']['name'].'/manifest.json') }}">