
@php
  
  if($userSettings->is_start == 1)
    {
          if(empty($catalog))
          {
              if(!empty($userSettings->customdomain))
              {
                  $homeLink = '//'.$userSettings->customdomain;
              }
              else if(!empty($userSettings->subdomain)) 
              {
                  $homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
              }
              else
              {
                  $homeLink = route('mystore.published',[$campaign->public_link]);
              }
          }    
          else
          {
              if(!empty($userSettings->customdomain))
              {   
                  $homeLink = '//'.$userSettings->customdomain;
              }
              else if(!empty($userSettings->subdomain)) 
              {
                  $homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
              }
              else
              {
                $homeLink = route('admin.mystore.preview',[$campaign->preview_link]);
              }
          }
    }
    else
    {
      $homeLink = route('admin.mystore.preview',[$campaign->preview_link]);
    }
  
      
    if($campaign->is_start == 1) 
    {
      $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
    }
    else
    {
      $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
    }

    if(!empty($meta_data['settings']['whatsapp'])){
      $chat = "https://api.whatsapp.com/send?phone={{$meta_data['settings']['whatsapp']}}";
    }else{
      $chat = "";
    }
   
  @endphp

<!-- Navbar Toggler-->
<div class="suha-navbar-toggler d-flex justify-content-between flex-wrap" id="suhaNavbarToggler"><span></span><span></span><span></span></div>
      </div>
    </div>
    <!-- Sidenav Black Overlay-->
    <div class="sidenav-black-overlay"></div>
    <!-- Side Nav Wrapper-->
    <div class="suha-sidenav-wrapper" id="sidenavWrapper">
      <!-- Sidenav Profile-->
      <div class="sidenav-profile">
      <div class="user-profile">
                    @if(!empty($meta_data['splash']))
                      @if($meta_data['splash']['brand_type'] == 2 && $meta_data['splash']['brand_name'] != "")
                        <h1 style="color: {{ $meta_data['splash']['brand_color'] ?? '' }}">{{$meta_data['splash']['brand_name']}}</h1>
                      @elseif(empty($userSettings->getFirstMediaUrl('splashlogo')) && $meta_data['splash']['brand_name'] != "")
                        <h1 style="color: {{ $meta_data['splash']['brand_color'] ?? '' }}">{{$meta_data['splash']['brand_name']}}</h1>
                      @elseif(empty($userSettings->getFirstMediaUrl('splashlogo')))
                        <img  src="{{ $userSettings->getFirstMediaUrl('splashlogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                            onerror="this.onerror=null;this.src='{{asset('XR/app-assets/images/logo/logo.png')}}';" alt="Logo" style="height: 90px;">
                      @endif
                    @elseif(empty($userSettings->getFirstMediaUrl('splashlogo')))
                        <img class=" lazyload" loading="lazy" src="{{ $userSettings->getFirstMediaUrl('splashlogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                            onerror="this.onerror=null;this.src='{{asset('XR/app-assets/images/logo/logo.png')}}';" alt="Logo" style="height: 90px;">
                    @endif
          <!-- <img  src="{{asset('XR/app-assets/images/logo/logo.png')}}" alt=""> -->
        </div>
        <br>

        <div class="user-info">
          <h6 class="user-name mb-0">{{ $meta_data['storefront']['storename'] ?? 'Store' }}</h6>
          
        </div>
      </div>
      <!-- Sidenav Nav-->
    <ul class="sidenav-nav pl-0">
        <li class="active"><a href="{{ $homeLink }}"><i class="lni lni-home"></i>Home</a></li>
        <li><a href="{{$chat}}"><i class="lni lni-whatsapp"></i>Chat</a></li>
        <li><a href="{{ $checkoutLink }}"><i class="lni lni-shopping-basket"></i>Cart&nbsp;<span class="badge badge-menu badge-warning cart-item-count"></span></a></li>
        <li><a><i class="lni lni-night"></i>Night mode&nbsp;&nbsp;
            <div class="single-settings d-flex align-items-center justify-content-between data-content">
                <div class="toggle-button-cover">
                    <div class="button r">
                        <input class="checkbox" id="darkSwitch" type="checkbox">
                            <div class="knobs"></div>
                            <div class="layer"></div>
                        </div>
                    </div>
                </div>
            </div>
        </a></li>
    </ul>