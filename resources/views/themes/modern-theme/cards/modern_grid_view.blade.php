@php
if(empty($user_meta_data['settings']['currency']))
        $currency = "₹";
else
    if($user_meta_data['settings']['currency_selection'] == "currency_symbol")
        $currency = App\User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
    else
        $currency = $user_meta_data['settings']['currency'];
@endphp
@foreach ($data as $key => $card)
                        @php
                            $product = $card->product;
                            if($product->discount != null)
                            {
                                $discount_price = $product->price * ($product->discount/100);
                                $discounted_price = round(($product->price - $discount_price),2);
                            }
                            if(count($product->photo) > 0)
                            {
                                $img = $product->photo[0]->getUrl('thumb');
                                $objimg = asset('XR/assets/images/placeholder.png');
                                $obj_id = "";
                            }
                            else if(count($product->arobject) > 0)
                            {
                                if(!empty($product->arobject[0]->object))
                                if(!empty($product->arobject[0]->object->getUrl('thumb')))
                                {
                                    $img = $product->arobject[0]->object->getUrl('thumb');
                                }
                            }
                            else
                            {
                                $img = asset('XR/assets/images/placeholder.png');
                                $objimg = asset('XR/assets/images/placeholder.png');
                            }

                            if(count($product->arobject) > 0)
                            {
                                if(!empty($product->arobject[0]->object))
                                if(!empty($product->arobject[0]->object->getUrl('thumb')))
                                {
                                    $objimg = $product->arobject[0]->object->getUrl('thumb');
                                    $obj_id = $product->arobject[0]->id;
                                }
                            }

                            if($campaign->is_start == 1)
                            {
                                $catalogId = $campaign->public_link;
                                $tryonLink = route('campaigns.publishedTryOn',$campaign->public_link)."?sku=".$product->id;
                                $catalogDetailsLink = route('campaigns.publishedCatalogDetails',[$product->id,$campaign->public_link]);

                            }
                            else
                            {
                                $catalogId = $campaign->preview_link;
                                $tryonLink = route('admin.campaigns.previewTryOn',$campaign->preview_link)."?sku=".$product->id;
                                $catalogDetailsLink = route('admin.campaigns.previewCatalogDetails',[$product->id,$campaign->preview_link]);

                            }
                        @endphp

                        <div class="col-6 col-md-4 col-lg-3">
                            <div class="card top-product-card">
                                <div class="card-body">
                                    @if($product->discount != null)
                                        <span class="badge badge-primary">{{$product->discount}}% OFF</span>
                                    @endif
                                    <a class="product-thumbnail d-block" " href="{{$catalogDetailsLink}}" ><img class="image-size" src="{{ $img }}" alt=""/></a>
                                    <a class="product-title d-block" href="{{$catalogDetailsLink}}">{!! $product->name ?? '&nbsp;' !!}</a>
                                    <p class="sale-price">{!! $currency ?? '&nbsp;'!!}
                                            @if($product->discount != null)
                                                {{$discounted_price}}
                                                <span>{{$product->price}}</span>
                                            @else
                                                {!! $product->price ?? '&nbsp;' !!}
                                            @endif
                                        </p>
                                    @if($product->out_of_stock != null)
                                    <div class="float-right">
                                        <h6 class="text-danger">Out of stock</h6>
                                    </div>
                                    @else
                                    <div class="cart">
                                        <a class=" add-to-cart btn btn-success btn-sm add2cart-notify" data-id="{{$product->id}}" data-name="{{$product->name ?? ''}}"
                                            data-price="{{$product->price ?? '0'}}" data-discounted_price="{{$product->discount ? $discounted_price:'0'}}" data-catalogid="{{$catalogId}}" data-catagory="{{(count($product->categories) > 0)?$product->categories[0]['name']:''}}" data-img="{{$img}}"><i class="lni lni-plus"></i> </a>

                                        <a class="view-in-cart btn btn-danger btn-sm d-none removefromcart-notify " data-id="{{$product->id}}" ><i class="lni lni-minus"></i></a>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach

@if( ($is_search == true) and (count($data) == 0))
<h3 class="text-center p-2 w-100">Searched product not found.</h3>
@endif
                    <script src="{{asset('themes/'.$user_meta_data['themes']['name'].'/js/cart.js')}}"></script>
