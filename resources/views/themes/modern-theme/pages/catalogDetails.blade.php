@extends('themes.'.$meta_data['themes']['name'].'.layouts.default')
@section('content')

@php
if($campaign->is_start == 1)
{
$catalogId = $campaign->public_link;
$tryonLink = route('campaigns.publishedTryOn',$campaign->public_link)."?sku=".$product->id;
$checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
$catalogLink = route('campaigns.published',[$campaign->public_link]);
}else
{
$catalogId = $campaign->preview_link;
$tryonLink = route('admin.campaigns.previewTryOn',$campaign->preview_link)."?sku=".$product->id;
$checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
$catalogLink = route('admin.campaigns.preview',[$campaign->preview_link]);
}

if($userSettings->is_start == 1)
{
if(!empty($userSettings->customdomain))
{
$homeLink = '//'.$userSettings->customdomain;
}
else if(!empty($userSettings->subdomain))
{
$homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
}
else
{
$homeLink = route('mystore.published',[$userSettings->public_link]);
}
$shopLink = route('campaigns.publishedAllCatalog',[$campaign->public_link]);

}
else
{
$homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
$shopLink = route('campaigns.publishedAllCatalog',[$campaign->preview_link]);
}

if(count($product->photo) > 0)
{
$img = $product->photo[0]->getUrl();
$objimg = asset('XR/assets/images/placeholder.png');
$obj_id = "";
}
else if(count($product->arobject) > 0)
{
$img = $product->arobject[0]->object->getUrl();
}

if(count($product->arobject) > 0)
{
$objimg = $product->arobject[0]->object->getUrl();
$obj_id = $product->arobject[0]->id;
}

if(empty($meta_data['settings']['currency']))
$currency = "₹";
else
if($meta_data['settings']['currency_selection'] == "currency_symbol")
$currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
else
$currency = $meta_data['settings']['currency'];

if($product->discount != null)
{
$discount_price = $product->price * ($product->discount/100);
$discounted_price = round(($product->price - $discount_price),2);
}
@endphp

@section('left')
<!-- Back Button-->
<div class="back-button"><a href="{{ $shopLink }}"><i class="lni lni-arrow-left"></i></a></div>
@endsection

@section('center')
<!-- Page Title-->
<div class="page-heading">
    <h6 class="mb-0">Product details</h6>
</div>
@endsection

@section('right')
@include('themes.'.$meta_data['themes']['name'].'.partials.menu')
@endsection

<div class="page-content-wrapper mt-0">
    <!-- Product Slides-->
    <div class="product-slides owl-carousel">
        <!-- Single Hero Slide-->
        @if(count($product->photo) >= 1)
        @foreach($product->photo as $photo)
        @if($loop->index < 3) <div class="single-product-slide {{$loop->index == 0?'active':''}}  " style="background-image: url({{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl():$objimg ?? ''}})" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
    </div>
    @endif
    @endforeach
    @else
    <div class="single-product-slide" style="background-image: url({{$img ?? ''}})" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';"></div>
    @endif
</div>

<div class="product-description pb-3">
    <!-- Product Title & Meta Data-->
    <div class="product-title-meta-data bg-white mb-3 py-3 mt-0">
        <div class="container d-flex justify-content-between">
            <div class="p-title-price">
                <h6 class="mb-1">{!! $product->name ?? '&nbsp;' !!}</h6>
                <p class="sale-price">
                    {!! $currency ?? '&nbsp;'!!}
                    @if($product->discount != null)
                    {{$discounted_price}}
                    <span>{{$product->price}}</span>
                    <br>
                    <h6 class="text-warning">{{$product->discount}}% OFF</h6>
                    @else
                    {!! $product->price ?? '&nbsp;' !!}
                    @endif
                </p>
                <div class=" float-right w-100">
                    @if($product->out_of_stock != null)
                    <h4 class="text-danger">Out of stock</h4>
                    @else
                    <form class="cart-form " action="#" method="">
                        <div class="order-plus-minus d-flex align-items-center">
                            <div class="quantity-button-handler">-</div>
                            <input class="form-control cart-quantity-input" type="text" step="1" name="quantity" value="1">
                            <div class="quantity-button-handler">+</div>&nbsp;&nbsp;&nbsp;
                        </div>
                        <div class="cart">
                            <a class=" add-to-cart btn btn-success add2cart-notify" data-id="{{$product->id}}" data-name="{{$product->name ?? ''}}" data-price="{{$product->price ?? '0'}}" data-discounted_price="{{$product->discount ? $discounted_price:'0'}}" data-catalogid="{{$catalogId}}" data-catagory="{{(count($product->categories) > 0)?$product->categories[0]['name']:''}}" data-img="{{$img}}"><i class="mr-1 lni lni-cart"></i>Add to cart</a>
                            <a class="view-in-cart btn btn-danger d-none removefromcart-notify " data-id="{{$product->id}}">Remove from cart</a>
                        </div>
                    </form>
                    @endif
                    <hr>
                    <button type="button" class="btn btn-icon btn-xl btn-circle btn-outline-primary mr-1 mb-1" style="height:35px; width:60px;" <?php if ($campaign->is_private == 1) { ?> disabled <?php   } ?> onclick='share("fb","")'><i class="lni lni-facebook"></i></button>
                    <button type="button" class="btn btn-icon btn-circle btn-outline-info mr-1 mb-1" style="height:35px; width:60px;" <?php if ($campaign->is_private == 1) { ?> disabled <?php   } ?> onclick='share("twitter","")'><i class="lni lni-twitter"></i></button>
                    <button type="button" class="btn btn-icon btn-circle btn-outline-primary mr-1 mb-1" style="height:35px; width:60px;" <?php if ($campaign->is_private == 1) { ?> disabled <?php   } ?> onclick='share("whatsapp","")'><i class="lni lni-whatsapp"></i></button>
                    <!-- <button type="button" class="btn btn-icon rounded-circle btn-outline-primary mr-1 mb-1" style="height:35px; width:60px;"><i class="lni lni-instagram"></i></button> -->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Product Specification-->
<div class="p-specification bg-white mb-3 py-3">
    <div class="container">
        <h6>Product Description</h6>
        <p>{!! $product->description ?? 'No description' !!}</p>
    </div>
</div>
</div>
</div>
@endsection
@section('scripts')
@parent
<script src="{{asset('themes/'.$meta_data['themes']['name'].'/js/cart.js')}}"></script>
<script>
    window.onload = function() {
        url = window.location.pathname;
        slink = url.slice(url.lastIndexOf('/') + 1);
        productId = $('.add-to-cart').data("id");
        track_events("product_details", productId, slink);
    }


    function share(share_name, msg) {
        var u = "{{Request::fullUrl()}}";
        var t = "I found this product interesting, open the link and check";
        switch (share_name) {
            case 'fb':
                window.open('http://www.facebook.com/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t), 'sharer', 'toolbar=0,status=0,width=626,height=436');
                break;
            case 'whatsapp':
                var message = encodeURIComponent(u) + " " + t;
                // var whatsapp_url = "whatsapp://send?text=" + message;
                // window.location.href = whatsapp_url;

                var whatsapp_url = "https://wa.me/?text=" + message;
                openInNewTab(whatsapp_url);
                break;
            case 'twitter':
                window.open("https://twitter.com/intent/tweet?text=" + t + "&url=" + u, 'sharer', 'toolbar=0,status=0,width=626,height=436')
                break;
        }
    }
</script>

@endsection
