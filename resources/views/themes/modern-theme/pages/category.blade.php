@extends('themes.'.$meta_data['themes']['name'].'.layouts.default')
@section('content')

@php
    if($userSettings->is_start == 1)
    {
        if(!empty($userSettings->customdomain))
        {
            $homeLink = '//'.$userSettings->customdomain;
        }
        else if(!empty($userSettings->subdomain))
        {
            $homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
        }
        else
        {
            $homeLink = route('mystore.published',[$userSettings->public_link]);
        }
    }
    else
    {
        $homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
    }

    if($campaign->is_start == 1){
        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
    }
    else{
        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
    }

    if(empty($meta_data['settings']['currency']))
        $currency = "₹";
else
    if($meta_data['settings']['currency_selection'] == "currency_symbol")
        $currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
    else
        $currency = $meta_data['settings']['currency'];
@endphp

@section('left')
    <!-- Back Button-->
    <div class="back-button"><a href="{{ $homeLink }}"><i class="lni lni-arrow-left"></i></a></div>
@endsection

@section('center')
    <!-- Page Title-->
    <div class="page-heading">
        <h6 class="mb-0">All products</h6>
    </div>
@endsection

@section('right')
    @include('themes.'.$meta_data['themes']['name'].'.partials.filter')
@endsection

<head>
    <style>
        #mtabs li
        {
            display: inline-block;
        }

        #mtabs li.active a
        {
            color: #ffffff;
            background-color: #ea4c62;
        }
    </style>
</head>

<div class="page-content-wrapper">
    <!-- Top Products-->
    <div class="top-products-area py-3">
        <div class="container">
            <div class="section-heading d-flex align-items-center justify-content-between">
                <h6 class="ml-1">All Products</h6>
                <!-- Layout Options-->
                <div id="mtabs">
                    <ul>
                        <li class="active"> <div class="layout-options"><a href="#tab1" rel="tab1"><i class="lni lni-grid-alt"></i></a></div></li>
                        <li ><div class="layout-options"><a  href="#tab4" rel="tab4"><i class="lni lni-radio-button"></i></a></div</li>
                    </ul>
                </div>
            </div>

            <div id="mtabs_content_container">
                <div id="tab1" class="mtab_content"  style="display: block;">
                    <div class="row g-3">
                    @foreach ($data as $key => $card)
                        @php
                            $product = $card->product;
                            if($product->discount != null)
                            {
                                $discount_price = $product->price * ($product->discount/100);
                                $discounted_price = round(($product->price - $discount_price),2);
                            }
                            if(count($product->photo) > 0)
                            {
                                $img = $product->photo[0]->getUrl('thumb');
                                $objimg = asset('XR/assets/images/placeholder.png');
                                $obj_id = "";
                            }
                            else if(count($product->arobject) > 0)
                            {
                                if(!empty($product->arobject[0]->object))
                                if(!empty($product->arobject[0]->object->getUrl()))
                                {
                                    $img = $product->arobject[0]->object->getUrl();
                                }
                            }
                            else
                            {
                                $img = asset('XR/assets/images/placeholder.png');
                                $objimg = asset('XR/assets/images/placeholder.png');
                            }

                            if(count($product->arobject) > 0)
                            {
                                if(!empty($product->arobject[0]->object))
                                if(!empty($product->arobject[0]->object->getUrl()))
                                {
                                    $objimg = $product->arobject[0]->object->getUrl();
                                    $obj_id = $product->arobject[0]->id;
                                }
                            }

                            if($campaign->is_start == 1)
                            {
                                $catalogId = $campaign->public_link;
                                $tryonLink = route('campaigns.publishedTryOn',$campaign->public_link)."?sku=".$product->id;
                                $catalogDetailsLink = route('campaigns.publishedCatalogDetails',[$product->id,$campaign->public_link]);

                            }
                            else
                            {
                                $catalogId = $campaign->preview_link;
                                $tryonLink = route('admin.campaigns.previewTryOn',$campaign->preview_link)."?sku=".$product->id;
                                $catalogDetailsLink = route('admin.campaigns.previewCatalogDetails',[$product->id,$campaign->preview_link]);

                            }
                        @endphp

                        <div class="col-6 col-md-4 col-lg-3">
                            <div class="card top-product-card">
                                <div class="card-body">
                                    @if($product->discount != null)
                                        <span class="badge badge-primary">{{$product->discount}}% OFF</span>
                                    @endif
                                    <a class="product-thumbnail d-block" " href="{{$catalogDetailsLink}}" ><img class="image-size" src="{{ $img }}" alt=""/></a>
                                    <a class="product-title d-block" href="{{$catalogDetailsLink}}">{!! $product->name ?? '&nbsp;' !!}</a>
                                    <p class="sale-price">{!! $currency ?? '&nbsp;'!!}
                                            @if($product->discount != null)
                                                {{$discounted_price}}
                                                <span>{{$product->price}}</span>
                                            @else
                                                {!! $product->price ?? '&nbsp;' !!}
                                            @endif
                                        </p>
                                    @if($product->out_of_stock != null)
                                    <div class="float-right">
                                        <h6 class="text-danger">Out of stock</h6>
                                    </div>
                                    @else
                                    <div class="cart">
                                        <a class=" add-to-cart btn btn-success btn-sm add2cart-notify" data-id="{{$product->id}}" data-name="{{$product->name ?? ''}}"
                                            data-price="{{$product->price ?? '0'}}" data-discounted_price="{{$product->discount ? $discounted_price:'0'}}" data-catalogid="{{$catalogId}}" data-catagory="{{(count($product->categories) > 0)?$product->categories[0]['name']:''}}" data-img="{{$img}}"><i class="lni lni-plus"></i> </a>

                                        <a class="view-in-cart btn btn-danger btn-sm d-none removefromcart-notify" data-id="{{$product->id}}" ><i class="lni lni-minus"></i></a>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>

                <div id="tab4" class="mtab_content" style="display: none;">
                    <div class="row g-3">
                    @foreach ($data as $key => $card)
                        @php
                            $product = $card->product;
                            if($product->discount != null)
                            {
                                $discount_price = $product->price * ($product->discount/100);
                                $discounted_price = $product->price - $discount_price;
                            }
                            if(count($product->photo) > 0)
                            {
                                $img = $product->photo[0]->getUrl('thumb');
                                $objimg = asset('XR/assets/images/placeholder.png');
                                $obj_id = "";
                            }
                            else if(count($product->arobject) > 0)
                            {
                                if(!empty($product->arobject[0]->object))
                                if(!empty($product->arobject[0]->object->getUrl()))
                                {
                                    $img = $product->arobject[0]->object->getUrl();
                                }
                            }
                            else
                            {
                                $img = asset('XR/assets/images/placeholder.png');
                                $objimg = asset('XR/assets/images/placeholder.png');
                            }

                            if(count($product->arobject) > 0)
                            {
                                if(!empty($product->arobject[0]->object))
                                if(!empty($product->arobject[0]->object->getUrl()))
                                {
                                    $objimg = $product->arobject[0]->object->getUrl();
                                    $obj_id = $product->arobject[0]->id;
                                }
                            }

                            if($campaign->is_start == 1)
                            {
                                $catalogId = $campaign->public_link;
                                $tryonLink = route('campaigns.publishedTryOn',$campaign->public_link)."?sku=".$product->id;
                                $catalogDetailsLink = route('campaigns.publishedCatalogDetails',[$product->id,$campaign->public_link]);

                            }
                            else
                            {
                                $catalogId = $campaign->preview_link;
                                $tryonLink = route('admin.campaigns.previewTryOn',$campaign->preview_link)."?sku=".$product->id;
                                $catalogDetailsLink = route('admin.campaigns.previewCatalogDetails',[$product->id,$campaign->preview_link]);

                            }
                        @endphp

                        <div class="col-12 col-md-6">
                            <div class="card weekly-product-card">
                                <div class="card-body d-flex align-items-center">
                                    <div class="product-thumbnail-side" >
                                    @if($product->discount != null)
                                        <span class="badge badge-primary">{{$product->discount}}% OFF</span>
                                    @endif
                                        <a class="product-thumbnail d-block" href="{{$catalogDetailsLink}}"><img class="image-size" src="{{$img}}" alt="" ></a>
                                    </div>
                                    <div class="product-description">
                                        <a class="product-title d-block" href="{{$catalogDetailsLink}}">{!! $product->name ?? '&nbsp;' !!}</a>
                                        <p class="sale-price">{!! $currency ?? '&nbsp;'!!}
                                            @if($product->discount != null)
                                                {{$discounted_price}}
                                                <span>{{$product->price}}</span>
                                            @else
                                                {!! $product->price ?? '&nbsp;' !!}
                                            @endif
                                        </p>
                                        @if($product->out_of_stock != null)
                                            <div>
                                                <h6 class="text-danger">Out of stock</h6>
                                            </div>
                                        @else
                                        <div class="cart">
                                            <a class=" add-to-cart btn btn-success btn-sm add2cart-notify" data-id="{{$product->id}}" data-name="{{$product->name ?? ''}}"
                                                data-price="{{$product->price ?? '0'}}" data-discounted_price="{{$product->discount ? $discounted_price:'0'}}" data-catalogid="{{$catalogId}}" data-catagory="{{(count($product->categories) > 0)?$product->categories[0]['name']:''}}" data-img="{{$img}}"><i class="mr-1 lni lni-cart"></i>Add to cart</a>

                                            <a class="view-in-cart btn btn-danger btn-sm d-none text-white removefromcart-notify " data-id="{{$product->id}}">Remove from cart</a>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
@parent
<script src="{{asset('themes/'.$meta_data['themes']['name'].'/js/cart.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

    //  When user clicks on tab, this code will be executed
    $("#mtabs li").click(function() {
        loadCart();
        //  First remove class "active" from currently active tab

        $("#mtabs li").removeClass('active');
        //  Now add class "active" to the selected/clicked tab
        $(this).addClass("active");
        //  Hide all tab content
        $(".mtab_content").hide();
        //  Here we get the href value of the selected tab
        var selected_tab = $(this).find("a").attr("href");
        //  Show the selected tab content
        $(selected_tab).fadeIn();
        //  At the end, we add return false so that the click on the link is not executed
        return false;
    });



});
</script>
@endsection
