@extends('themes.'.$meta_data['themes']['name'].'.layouts.store')
@section('content')

<div class="page-content-wrapper mt-4">
    <!-- Single Hero Slide-->
    @if(count($userSettings->bannerimg) > 0)
    @foreach($userSettings->bannerimg as $bannerimg)
    @if($loop->index == (count($userSettings->bannerimg) - 1))
    <div class="hero-slides owl-carousel {{$loop->index == 0?'active':'active'}}">
        <div class="single-hero-slide" style="background-image: url({{ $bannerimg->getUrl() }}); height:280px; border-radius: 5px;">
            <div class="slide-content h-100 d-flex align-items-center">
            </div>
        </div>
    </div>
    @endif
    @endforeach
    @endif

    <div class="top-products-area clearfix py-3">
        <div class="container">
            <div class="row g-3">
                @foreach ($catalog as $campaign)
                @php
                if($campaign->is_start == 1) {
                $catalogDetailsLink = route('campaigns.published',$campaign->public_link);
                }else{
                $catalogDetailsLink = route('admin.campaigns.preview',$campaign->preview_link);
                }
                $entitycards = $campaign->campaignEntitycards;
                $n = count($entitycards);
                @endphp
                <div class="col-6 col-md-4 col-lg-3">
                    <div class="card top-product-card">
                        <a href="{{$catalogDetailsLink}}">
                            <div class="card-body" style="height: 250px;">
                                <div class="product-thumbnail d-block">
                                    <div class="item-img text-center" style="min-height: 12.85rem;">
                                        @if(count($entitycards) <= 3) <div class="avatar mr-1 avatar-xl" style="position:absolute;left:10px;top:90px;">
                                            @php
                                            if(count($entitycards) > 2){
                                            $i = 2;
                                            }
                                            else if (count($entitycards) > 1){
                                            $i = 1;
                                            }
                                            else{
                                            $i = 0;
                                            }

                                            if(count($entitycards[$i]->product->photo) > 0){
                                            $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                                            }
                                            else{
                                            $img = asset('XR/assets/images/placeholder.png');
                                            }
                                            @endphp
                                            <img class="img-fluid lazyload" loading="lazy" src="{{ $img }}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="width:100px;height:100px;">
                                    </div>
                                    <div class="avatar mr-1 avatar-xl " style="z-index:1">
                                        @php
                                        if(count($entitycards) > 2){
                                        $i = 1;
                                        }
                                        else{
                                        $i = 0;
                                        }

                                        if(count($entitycards[$i]->product->photo) > 0){
                                        $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                                        }
                                        else{
                                        $img = asset('XR/assets/images/placeholder.png');
                                        }
                                        @endphp
                                        <img class="img-fluid lazyload" loading="lazy" src="{{ $img }}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="width:100px;height:100px;">
                                    </div>
                                    <div class="avatar mr-1 avatar-xl" style="position:absolute;right:10px;top:90px;">
                                        @php
                                        if(count($entitycards) > 2){
                                        if(count($entitycards[0]->product->photo) > 0){
                                        $img = $entitycards[0]->product->photo[0]->getUrl('thumb');
                                        }
                                        else{
                                        $img = asset('XR/assets/images/placeholder.png');
                                        }
                                        }
                                        else{
                                        $img = asset('XR/assets/images/placeholder.png');
                                        }
                                        @endphp
                                        <img class="img-fluid lazyload" loading="lazy" src="{{ $img }}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="width:100px;height:100px;">
                                    </div>
                                    @else
                                    <div class="avatar mr-1 avatar-xl" style="position:absolute;left:10px;top:90px;">
                                        @php
                                        if(count($entitycards[$n-2]->product->photo) > 0)
                                        $img = $entitycards[$n-2]->product->photo[0]->getUrl('thumb');
                                        else
                                        $img = asset('XR/assets/images/placeholder.png');
                                        @endphp
                                        <img class="img-fluid lazyload" loading="lazy" src="{{ $img }}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="width:100px;height:100px;">
                                    </div>
                                    <div class="avatar mr-1 avatar-xl " style="z-index:1">
                                        @php
                                        if(count($entitycards[$n-1]->product->photo) > 0)
                                        $img = $entitycards[$n-1]->product->photo[0]->getUrl('thumb');
                                        else
                                        $img = asset('XR/assets/images/placeholder.png');
                                        @endphp
                                        <img class="img-fluid lazyload" loading="lazy" src="{{ $img }}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="width:100px;height:100px;">
                                    </div>
                                    <div class="avatar mr-1 avatar-xl" style="position:absolute;right:10px;top:90px;">
                                        @php
                                        if(count($entitycards[$n-3]->product->photo) > 0)
                                        $img = $entitycards[$n-3]->product->photo[0]->getUrl('thumb');
                                        else
                                        $img = asset('XR/assets/images/placeholder.png');
                                        @endphp
                                        <img class="img-fluid lazyload" loading="lazy" src="{{ $img }}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="width:100px;height:100px;">
                                    </div>
                                    @endif

                                    @if(count($entitycards)==0)
                                    <img class="image-size" src="{{asset('XR/assets/images/placeholder.png')}}" style="height:100%;" alt="" />
                                    @endif


                                </div>

                            </div>
                            <a class="product-title d-block text-center mt-4 " href="{{$catalogDetailsLink}}">{!! $campaign->name == "Untitled"?"&nbsp;":$campaign->name !!}</a>
                    </div>
                    </a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
</div>

<script>
    window.onload = function() {

        // if (document.cookie.indexOf('store_event') == -1) {
        url = window.location.pathname;
        slink = url.slice(url.lastIndexOf('/') + 1);
        if (slink == '') {
            slink = window.location.host;
        }
        track_events("store", null, slink);
        // document.cookie = 'store_event=1';
        // }

    };


    function track_events(event_name, product_id, slink) {
        $.ajax({
            url: "{{route('admin.stats.ajaxEvents')}}",
            type: "post",
            data: {
                "_token": "{{ csrf_token() }}",
                "event": event_name,
                "slink": slink,
                "product_id": product_id,
            },
        });
    }
</script>