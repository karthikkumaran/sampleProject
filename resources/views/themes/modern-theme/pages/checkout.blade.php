@extends('themes.'.$meta_data['themes']['name'].'.layouts.default')
@section('content')

@php
    if($userSettings->is_start == 1)
    {
        //dd($userSettings);
        if(!empty($userSettings->customdomain))
        {
            $homeLink = '//'.$userSettings->customdomain;
        }
        else if(!empty($userSettings->subdomain))
        {
            $homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
        }
        else
        {
            $homeLink = route('mystore.published',[$userSettings->public_link]);
        }
        $shopLink = route('campaigns.publishedAllCatalog',[$campaign->public_link]);

    }
    else
    {
        $homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
        $shopLink = route('campaigns.publishedAllCatalog',[$campaign->preview_link]);
    }

    if($campaign->is_start == 1)
    {
      $catalogLink = route('campaigns.published',[$campaign->public_link]);
      $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
    }
    else
    {
      $catalogLink = route('admin.campaigns.preview',[$campaign->preview_link]);
      $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
    }

    if(empty($meta_data['settings']['contactnumber']))
      $contactNumber = "";
    else
      $contactNumber = $meta_data['settings']['contactnumber'];

    if(empty($meta_data['settings']['emailid']))
      $emailFrom = $campaign->user->email;
    else
      $emailFrom = $meta_data['settings']['emailid'];

      if(empty($meta_data['settings']['currency']))
        $currency = "₹";
else
    if($meta_data['settings']['currency_selection'] == "currency_symbol")
        $currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
    else
        $currency = $meta_data['settings']['currency'];
@endphp

@section('left')
    <!-- Back Button-->
    <div class="back-button"><a href="{{$shopLink}}"><i class="lni lni-arrow-left"></i></a></div>
@endsection

@section('center')
    <!-- Page Title-->
    <div class="page-heading">
        <h6 class="mb-0">My Cart</h6>
    </div>
@endsection

@section('right')
    @include('themes.'.$meta_data['themes']['name'].'.partials.menu')
@endsection

<head>
  <style>
    #Div2
    {
      display: none;
    }
  </style>
</head>

<div class="page-content-wrapper">
  <div class="container">

    <!-- Cart Wrapper-->
    <div class="cart-wrapper-area py-3 " id="Div1">
      <div class="cart-table card mb-3">
        <div class="table-responsive card-body">
        </div>
      </div>

      <!-- Cart Amount Area-->
      <div class="card cart-amount-area">
        <div class="card-body d-flex align-items-center justify-content-between">
          <h5 class="total-price mb-0">{!! $currency ?? '&nbsp;'!!}<span class="total-amt"></span></h5><a class="btn btn-warning" onclick="switchCheckout();">Checkout Now</a>
        </div>
      </div>
    </div>

    <div class="checkout-wrapper-area py-3" id="Div2">
      <!-- Billing Address-->
      <a style="color: blue;" onclick="switchCart();">Back</a>
      <div class="billing-information-card mb-3">
        <div class="card billing-information-title-card bg-danger">
          <div class="card-body">
            <h6 class="text-center mb-0 text-white">Enter Your Details</h6>
          </div>
        </div>
        <form action="#" name="checkout_form" class="checkoutDetails">
          @csrf
          <input type="hidden" value="{{$campaign->public_link}}" name="key"/>
        <div class="card user-data-card">
          <div class="card-body">
            <div class="single-profile-data d-flex align-items-center justify-content-between">
              <div class="title d-flex align-items-center"><i class="lni lni-user"></i><span><label for="checkout-name">Full Name:</label></span></div>
              <div class="data-content"><input type="text" id="checkout-name" class="form-control required" name="fname"></div>
            </div>
            <div class="single-profile-data d-flex align-items-center justify-content-between">
              <div class="title d-flex align-items-center"><i class="lni lni-envelope"></i><span><label for="checkout-email">Email:</label></span></div>
              <div class="data-content"><input type="email" id="checkout-email" class="form-control required" name="email"></div>
            </div>
            <div class="single-profile-data d-flex align-items-center justify-content-between">
              <div class="title d-flex align-items-center"><i class="lni lni-phone"></i><span><label for="checkout-number">Mobile Number:</label></span></div>
              <div class="data-content"><input type="number" id="checkout-mnumber" class="form-control required" name="mnumber"></div>
            </div>
            <div class="single-profile-data d-flex align-items-center justify-content-between">
              <div class="title d-flex align-items-center"><i class="lni lni-phone"></i><span><label for="checkout-number">Alternate Mobile Number:</label></span></div>
              <div class="data-content"><input type="number" id="checkout-amnumber" class="form-control required" name="amnumber"></div>
            </div>
            <div class="single-profile-data d-flex align-items-center justify-content-between">
              <div class="title d-flex align-items-center"><i class="lni lni-map-marker"></i><span><label for="checkout-apt_number">Flat, House No, Street:</label></span></div>
              <input type="text" id="checkout-apt_number" class="form-control required" name="apt_number">
            </div>
            <div class="single-profile-data d-flex align-items-center justify-content-between">
              <div class="title d-flex align-items-center"><i class="lni lni-map-marker"></i><span><label for="checkout-landmark">Landmark e.g. near apollo hospital:</label></span></div>
              <div class="data-content"><input type="text" id="checkout-landmark" class="form-control" name="landmark"></div>
            </div>
            <div class="single-profile-data d-flex align-items-center justify-content-between">
              <div class="title d-flex align-items-center"><i class="lni lni-map-marker"></i><span><label for="checkout-city">Town/City:</label></span></div>
              <input type="text" id="checkout-city" class="form-control required" name="city">
            </div>
            <div class="single-profile-data d-flex align-items-center justify-content-between">
              <div class="title d-flex align-items-center"><i class="lni lni-map-marker"></i><span><label for="checkout-pincode">Pincode:</label></span></div>
              <div class="data-content"><input type="number" id="checkout-pincode" class="form-control required" name="pincode"></div>
            </div>
            <div class="single-profile-data d-flex align-items-center justify-content-between">
              <div class="title d-flex align-items-center"><i class="lni lni-map-marker"></i><span><label for="checkout-state">State:</label></span></div>
              <div class="data-content"><input type="text" id="checkout-state" class="form-control required" name="state"></div>
            </div>
            <div class="single-profile-data d-flex align-items-center justify-content-between">
              <div class="title d-flex align-items-center"><i class="lni lni-map-marker"></i><span><label for="add-type">Address Type:</label></span></div>
              <div class="data-content"><select class="form-control" id="add-type" name="addresstype">
                <option>Home</option>
                <option>Work</option>
                </select></div>
            </div>

          </div>

        </div>

      </div>

      <!-- Shipping Method Choose-->
      <div class="shipping-method-choose mb-3">
        <div class="card shipping-method-choose-title-card bg-success">
          <div class="card-body">
            <h6 class="text-center mb-0 text-white">Payment Method</h6>
          </div>
        </div>
        <div class="card shipping-method-choose-card">
          <div class="card-body">
            <div class="shipping-method-choose">
              <ul class="pl-0">
                <li>
                  <input id="fastShipping" type="radio" name="selector" checked>
                  <label for="fastShipping">Cash on Delivery</label>
                  <div class="check"></div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!-- Cart Amount Area-->
      <div class="card cart-amount-area">
        <div class="card-body d-flex align-items-center justify-content-between">
          <div class="row">
            <div class="col-lg-6">
              <h5 class="total-price mt-1 mb-0">{!! $currency ?? '&nbsp;'!!}<span class="total-amt"></span></h5>
            </div>
            <div class="col-lg-6 ">
              <a class="btn btn-warning delivery-address float-right" onclick="placeOrder();">Confirm &amp; Pay</a>
              <button type="reset" class="btn btn-danger mr-1 float-right">CLEAR FORM</button>
            </div>
          </div>
        </div>
      </div>
      </form>
    </div>
  </div>
  @endsection

  @section('scripts')
  @parent
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  <script src="{{asset('themes/'.$meta_data['themes']['name'].'/js/jquery.bootstrap-touchspin.js')}}"></script>
  <script src="{{asset('themes/'.$meta_data['themes']['name'].'/js/jquery.steps.min.js')}}"></script>
  <script src="{{asset('themes/'.$meta_data['themes']['name'].'/js/cart.js')}}"></script>
  <script src="{{asset('themes/'.$meta_data['themes']['name'].'/js/jquery.validate.min.js')}}"></script>
  <script src="{{asset('themes/'.$meta_data['themes']['name'].'/js/sweetalert2.all.min.js')}}"></script>



  <script>
    function switchCheckout() {
      document.getElementById('Div1').style.display = 'none';
      document.getElementById('Div2').style.display = 'block';
      document.checkout_form.fname.value = getCookie("fname");
      document.checkout_form.email.value = getCookie("email");
      document.checkout_form.mnumber.value = getCookie("mnumber");
      document.checkout_form.amnumber.value = getCookie("amnumber");
      document.checkout_form.apt_number.value = getCookie("apt_number");
      document.checkout_form.landmark.value = getCookie("landmark");
      document.checkout_form.city.value = getCookie("city");
      document.checkout_form.pincode.value = getCookie("pincode");
      document.checkout_form.state.value = getCookie("state");
      document.checkout_form.addresstype.value = getCookie("addresstype");
    }

    function switchCart() {
      document.getElementById('Div1').style.display = 'block';
      document.getElementById('Div2').style.display = 'none';
    }

    function placeOrder() {

      if (Number($(".form-control.required").val().length) < 1) {
        toastr.warning('Error', 'Please Enter Valid Details', {
          "positionClass": "toast-bottom-right"
        });
      }
      var checkout = $(".checkoutDetails");
      checkout.validate();
      if (checkout.valid()) {
        track_events('confirm_order', null, slink);

        var $this = $('.delivery-address');
        var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> loading...';
        setCookie("fname", document.checkout_form.fname.value);
        setCookie("email", document.checkout_form.email.value);
        setCookie("mnumber", document.checkout_form.mnumber.value);
        setCookie("amnumber", document.checkout_form.amnumber.value);
        setCookie("apt_number", document.checkout_form.apt_number.value);
        setCookie("landmark", document.checkout_form.landmark.value);
        setCookie("city", document.checkout_form.city.value);
        setCookie("pincode", document.checkout_form.pincode.value);
        setCookie("state", document.checkout_form.state.value);
        setCookie("addresstype", document.checkout_form.addresstype.value);

        if ($this.html() !== loadingText) {
          $this.data('original-text', $this.html());
          $this.html(loadingText);

          //console.log($('form').serialize());
          formSubmit($('form').serialize(), $this);
        }
        // setTimeout(function() {
        // $this.html($this.data('original-text'));
        // }, 2000);
      }

    }

    var cart = [];
    var obj = {};

    $(document).ready(function() {

      // :: Cart Quantity Button Handler
      $(".quantity-button-handler").on("click", function() {
        var value = $(this).parent().find("input.cart-quantity-input").val();
        //console.log(this);
        if ($(this).text() == "+") {
          var newVal = Number(value) + 1;
        } else {
          if (value > 1) {
            var newVal = Number(value) - 1;
          } else {
            newVal = 1;
          }
        }
        $(this).parent().find("input").val(newVal);
        obj.setCountForItem(Number($(this).parent().find("input").data('id')), $(this).parent().find("input").val());
        var $this = $(this).parent().find("input");
        $("#product_qty_" + $(this).parent().find("input").data('id')).val($(this).parent().find("input").val());
        if (Number($this.data('discounted_price')) == 0) {
          $('.item-price-' + Number($this.data('id'))).html('{!! $currency !!} ' + $this.val() * Number($this.data('price')));
        } else {
          $('.item-price-' + Number($this.data('id'))).html('{!! $currency !!} ' + $this.val() * Number($this.data('discounted_price')) + '&nbsp;<span class="text-primary"><s>' + $this.val() * Number($this.data('price')) + '</s></span>');
        }

      });

      // remove items from wishlist page
      $(".remove-product").on("click", function() {
        $(this).closest(".ecom").remove();
        obj.removeItemFromCart(Number($(this).data('id')));
        obj.totalCart();
        track_events("remove_product", $(this).data('id'), slink);
      })
    });

    // Set count from item
    obj.setCountForItem = function(id, qty) {
      //console.log("set count "+ id,qty);
      for (var i in cart) {
        if (cart[i].id === id) {
          cart[i].qty = qty;
          break;
        }
      }
      saveCart();
      obj.totalCart();
    };

    // Remove item from cart
    obj.removeItemFromCart = function(id) {
      for (var item in cart) {
        if (cart[item].id === id) {
          //   cart[item].qty --;
          //   if(cart[item].qty === 0) {
          cart.splice(item, 1);
          //   }
          //   break;
        }
      }
      saveCart();
    }

    // Count cart
    obj.totalCount = function() {
      var totalCount = 0;
      for (var item in cart) {
        totalCount += cart[item].qty;
      }
      return totalCount;

      //console.log(totalCount);
    }

    // Total cart
    obj.totalCart = function() {
      var totalCart = 0;
      for (var item in cart) {
        if (cart[item].discounted_price != 0) {
          totalCart += cart[item].discounted_price * cart[item].qty;
          //console.log(totalCart);
        } else {
          totalCart += cart[item].price * cart[item].qty;
          //console.log(totalCart);
        }
      }
      $('.total-amt').html(Number(totalCart.toFixed(2)));
      //console.log(totalCart)
      if (cart.length > 0) {
        $(".cart-item-count").html(cart.length);
        $(".price-items").html("Price of " + cart.length + " items");
      } else {
        $(".cart-item-count").html("");
        $(".cart-wrapper-area").empty();
        $(".cart-wrapper-area").html("<h2 class='text-center' style='color: black;'>No item in cart</h2>");
      }
      return Number(totalCart.toFixed(2));
    }

    // Clear cart
    obj.clearCart = function() {
      cart = [];
      saveCart();
    }
    // Remove all items from cart
    obj.removeItemFromCartAll = function(name) {
      for (var item in cart) {
        if (cart[item].name === name) {
          cart.splice(item, 1);
          break;
        }
      }
      saveCart();
    }
    // Save cart
    function saveCart() {
      const now = new Date();
      var time = now.getTime() + 1000 * 60;
      localStorage.setItem('shoppingCart', JSON.stringify(cart));
      localStorage.setItem('expiretime', JSON.stringify(time));
    }

    function loadCart() {
      cart = JSON.parse(localStorage.getItem('shoppingCart'));
      var cartItemCount = count = cart.length;
      //console.log(cart);
      $(".table-responsive").empty();
      if (cart.length > 0) {
        //console.log("works");
        $(".cart-item-count").html(count);
        var products = [];
        var catalogs = [];
        for (var item in cart) {
          // console.log(cart[item].discounted_price);
          var html_1 = '<table class="table mb-0 ecom"><tbody><th scope="row"><a class="remove-product" data-id="' + cart[item].id + '"><i class="lni lni-close"></i></a></th><td><div class="item-img text-center"><a href="#"><img src="' + cart[item].img + '"></a></div></td><td style="max-width: 90px;"><a class="prod"href="javascript:void(0);" data-id="' + cart[item].id + '" data-catalog_id="' + cart[item].catalogid + '" onclick="catalogDetailsPage(this)"  >' + cart[item].name + '</a>';
          if (cart[item].discounted_price == 0) {
            var html_2 = '<div class="item-cost" ><h6 class="item-price item-price-' + cart[item].id + '">  {!! $currency ?? ' & nbsp;
            '!!} ' + cart[item].price + ' </h6></div></td>';
          } else {
            var html_2 = '<div class="item-cost" ><h6 class="item-price item-price-' + cart[item].id + '" > {!! $currency ?? ' & nbsp;
            '!!} ' + cart[item].discounted_price + ' &nbsp;<span class="text-primary"><s>' + cart[item].price + '</s></span></h6></div></td>';
          }
          var html_3 = '<td><div class="cart-form"><div class="order-plus-minus d-flex align-items-center"><div class="quantity-button-handler">-</div><input class="form-control cart-quantity-input" type="text" step="1" name="quantity" data-id="' + cart[item].id + '" data-price="' + cart[item].price + '" data-discounted_price="' + cart[item].discounted_price + '" value="' + cart[item].qty + '"><div class="quantity-button-handler">+</div></div></div></td></tr></tbody></table>';
          var html = html_1 + html_2 + html_3;
          $(".table-responsive").append(html);
          products.push(cart[item].id);
          var id = cart[item].id,
            qty = cart[item].qty,
            catalogid = cart[item].catalogid;
          $("form").append("<input type='hidden' id='product_qty_" + id + "' name='products_qty[" + id + "]' value='" + qty + "'>");
          for (var catalog in cart) {
            if (catalogs.indexOf(catalogid) == -1)
              catalogs.push(catalogid);
          }
          obj.totalCart();
        }
        $("form").append("<input type='hidden' name='products' value='" + products + "'>");
        $("form").append("<input type='hidden' name='catalogs' value='" + catalogs + "'>");
      } else {
        $(".cart-item-count").html("");
        $(".cart-wrapper-area").empty();
        $(".cart-wrapper-area").html("<h2 class='text-light text-center'>No item in cart</h2>");
        //console.log("Empty");
      }
    }

    if (localStorage.getItem("shoppingCart") != "[]") {
      const now = new Date();
      var now_time = now.getTime();
      var expire_time = localStorage.getItem("expiretime");
      // console.log(now_time);
      // console.log(expire_time);
      if (now_time > expire_time) {
        // console.log("Exceeded");
        obj.removeItemFromCartAll();
        obj.totalCart();
      } else {
        // console.log(" NOt Exceeded");
        loadCart();
      }
    } else {
      obj.totalCart();
    }

    function formSubmit(data, $this) {
      $.ajax({
        url: "{{route('orders.place')}}",
        type: "POST",
        data: data,
        success: function(response_sub) {
          //console.log(response_sub);
          // if(response_sub.code == 200){
          $this.html($this.data('original-text'));
          obj.clearCart();
          Swal.fire({
            type: "success",
            title: 'Order Confirmed!',
            //text: 'Thanks for placing an order with us, our team will get in touch with you shortly to process the order. Meanwhile if you want to reach us please feel free to reach us at {{$emailFrom}}{{empty($contactNumber)?"":" or call us at Store ".$contactNumber}} ',
            html: '<p style="line-height: 2;color: black;">Thanks for placing an order with us, our team will get in touch with you shortly to process the order. Meanwhile if you want to reach us please feel free to reach us at {{$emailFrom}}{{empty($contactNumber)?"":" or call us at Store ".$contactNumber}} or <a class="" href="https://api.whatsapp.com/send?phone={{$meta_data['settings ']['whatsapp '] ?? ''}}&text={{$meta_data['settings ']['enquiry_text '] ?? '            '}}" target="_blank"><i class="ficon fa fa-whatsapp btn-order text-white" style="padding: 8px !important;border-radius: 8px;">&nbsp;Contact us on whatsapp</i></p>',
            confirmButtonClass: 'btn btn-primary',

          }).then((result) => {
            window.location = "{{$catalogLink}}";
          });
          // }
        }
      });

    }
    var today = new Date();
    var expiry = new Date(today.getTime() + 30 * 24 * 3600 * 1000); // plus 30 days

    function setCookie(name, value) {
      document.cookie = name + "=" + escape(value) + "; path=/; expires=" + expiry.toGMTString();
      //console.log(document.cookie);
    }

    function getCookie(name) {
      var re = new RegExp(name + "=([^;]+)");
      var value = re.exec(document.cookie);
      //console.log(value);
      return (value != null) ? unescape(value[1]) : null;
    }

    function catalogDetailsPage(that) {
      var product_id = that.getAttribute("data-id");
      var catalog_id = that.getAttribute("data-catalog_id");
      var url = '{{ route('
      campaigns.publishedCatalogDetails ',[":product_id",":catalog_id"]) }}';
      url = url.replace(':product_id', product_id);
      url = url.replace(':catalog_id', catalog_id);
      console.log(url);
      window.location.href = url;
    }

    window.onload = function() {
      url = window.location.pathname;
      slink = url.slice(url.lastIndexOf('/') + 1)
      track_events('checkout_page', null, slink);
    };
  </script>
  @endsection
