@extends('themes.'.$meta_data['themes']['name'].'.layouts.default')
@section('content')

@php
    if($userSettings->is_start == 1) 
    {
        if(!empty($userSettings->customdomain))
        {
            $homeLink = '//'.$userSettings->customdomain;
        }
        else if(!empty($userSettings->subdomain)) 
        {
            $homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
        }
        else
        {
            $homeLink = route('mystore.published',[$userSettings->public_link]);
        }
    }
    else
    {
        $homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
    }

    if($campaign->is_start == 1){
        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
    }else{
        $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
    }

    if(empty($meta_data['settings']['currency']))
        $currency = "₹";
else
    if($meta_data['settings']['currency_selection'] == "currency_symbol")
        $currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
    else
        $currency = $meta_data['settings']['currency'];
@endphp

@section('left')
    <!-- Back Button-->
    <div class="back-button"><a href="{{ $homeLink }}"><i class="lni lni-arrow-left"></i></a></div>
@endsection

@section('center')
    @include('themes.'.$meta_data['themes']['name'].'.partials.searchbar')
@endsection

@section('right')
    @include('themes.'.$meta_data['themes']['name'].'.partials.menu')
@endsection

<head>
    <style>
        #mtabs li 
        {
            display: inline-block;
        }

        #mtabs li.active a 
        {
            color: #ffffff;
            background-color: #ea4c62;
        }
    </style>
</head>

<div class="page-content-wrapper">
    <!-- Top Products-->
    <div class="top-products-area py-3">
        <div class="container">
            <div class="section-heading d-flex align-items-center justify-content-between">
                <h6 class="ml-1">All Products</h6>
                <!-- Layout Options-->
                <div id="mtabs">
                    <ul>
                        <li class="active"> <div class="layout-options"><a href="#tab1" name="grid" rel="tab1"><i class="lni lni-grid-alt"></i></a></div></li>
                        <li ><div class="layout-options"><a  href="#tab4" name="list" rel="tab4"><i class="lni lni-radio-button"></i></a></div</li>
                    </ul>
                </div>          
            </div>
    
            <div id="mtabs_content_container">
                <div id="tab1" class="mtab_content"  style="display: block;">
                    <div id="catalog_prod_grid" class="row g-3">
                    
                    </div>
                </div>
    
                <div id="tab4" class="mtab_content" style="display: none;">
                    <div id="catalog_prod_list" class="row g-3">
                    
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>

    
@endsection

@section('scripts')
@parent
<script src="{{asset('themes/'.$meta_data['themes']['name'].'/js/cart.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript">
var q = "";
    var searchData = "";
    var sort = "";
    var page = 0;
    var filter = [];
    var filterData = "";
    var preview = 0;
    var campaign_id = {{ $campaign->id }} ;
    var meta = @json($meta_data);
    var theme = meta['themes']['name'];
    //var view = $("#mtabs li.active").find("a").attr("name");
    var page_url=window.location.pathname;
    // shareable_link=page_url.slice(page_url.lastIndexOf('/')+1);
    var shareable_link = "{{($userSettings->is_start == 1)?$campaign->public_link:$campaign->preview_link}}";
    var u = window.location.href;
    if(u.includes("preview"))
    {
        preview = 1;
    }

    getProducts(0);
    
    $("#CatalogProductSearch").on('input', function(){
        searchData = $(this).val();
        if(searchData.length >= 3 || searchData == "")
            //console.log(searchData);
            getProducts(0);
    });

    $("#ecommerce-productscatalog").on('click', '.pagination a', function (event) {
        event.preventDefault();

        $('li').removeClass('active');
        $(this).parent('li').addClass('active');

        var myurl = $(this).attr('href');
        var page = $(this).attr('href').split('page=')[1];

        getProducts(page);
    });


    function getProducts(page,view){
        q = 'q=' + searchData + '&s=' + sort + '&page=' + page + '&preview=' + preview + '&filterData=' + filterData + '&shareable_link=' + shareable_link + '&theme=' + theme + '&filter=' + JSON.stringify(filter);
        //console.log(view);
        $('.loading1').show();
        $.ajax({
            url: "{{route('admin.campaigns.ajaxProductData')}}" + '?' + q,
            type: "get",
            datatype: "html"
        }).done(function (data) {
            //console.log(data);
            //console.log("sucess");  
            var view = $("#mtabs li.active").find("a").attr("name");

            //console.log(view);
            if(view == "grid")
            {
                $("#catalog_prod_grid").empty().html(data);
            }
            else if(view == "list")
            {
                $("#catalog_prod_list").empty().html(data);
            }
            //$("#product_list").append(data);
            $('.loading1').hide();
        }).fail(function (jqXHR, ajaxOptions, thrownError) {
            $('.loading1').hide();
            // alert('No response from server');
        });
    }
$(document).ready(function(){
    
    //  When user clicks on tab, this code will be executed
    $("#mtabs li").click(function(){
        // console.log("Change view");
        loadCart();
        /*if (localStorage.getItem("shoppingCart") != null){
            loadCart();
        }*/

        //  First remove class "active" from currently active tab
        $("#mtabs li").removeClass('active');
        //  Now add class "active" to the selected/clicked tab
        $(this).addClass("active");
        //  Hide all tab content
        $(".mtab_content").hide();
        //  Here we get the href value of the selected tab
        var selected_tab = $(this).find("a").attr("href");
        var view = $(this).find("a").attr("name");
        //console.log(view);
        getProducts(page,view);
        //  Show the selected tab content
        $(selected_tab).fadeIn();
        //  At the end, we add return false so that the click on the link is not executed
        return false;
    });
});
</script>
@endsection