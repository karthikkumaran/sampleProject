@extends('themes.'.$meta_data['themes']['name'].'.layouts.default')
@section('content')

@php

if($userSettings->is_start == 1)
{
//dd($userSettings);
if(!empty($userSettings->customdomain))
{
$homeLink = '//'.$userSettings->customdomain;
}
else if(!empty($userSettings->subdomain))
{
$homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
}
else
{
$homeLink = route('mystore.published',[$userSettings->public_link]);
}
$shopLink = route('campaigns.publishedAllCatalog',[$campaign->public_link]);
}
else
{
$homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
$shopLink = route('campaigns.publishedAllCatalog',[$campaign->preview_link]);
}

if($campaign->is_start == 1)
{
$checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
}
else
{
$checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
}

@endphp

@section('left')
<a href="{{ $homeLink }}"><i class="lni lni-home"></i></a>
@endsection

@section('center')
@include('themes.'.$meta_data['themes']['name'].'.partials.searchbar')
@endsection

@section('right')
@include('themes.'.$meta_data['themes']['name'].'.partials.menu')
@endsection

<div class="page-content-wrapper mt-4">
    <!-- Single Hero Slide-->
    <div class="hero-slides owl-carousel">
        @if(count($userSettings->bannerimg) > 0)
        @foreach($userSettings->bannerimg as $bannerimg)
        @if($loop->index <= (count($userSettings->bannerimg)-1))
            <div class="single-hero-slide" style="background-image: url({{ $bannerimg->getUrl() }});max-height:280px;">
                <div class="slide-content h-100 d-flex align-items-center"></div>
            </div>
            @endif
            @endforeach
            @endif
    </div>
    <!-- Product Catagories-->
    @if(count($categories)>0)
    <div class="product-catagories-wrapper py-3">
        <div class="container">
            <div class="section-heading">
                <h6 class="ml-1">Product Category</h6>
            </div>

            <div class="product-catagory-wrap">
                <div class="row g-4">
                    @foreach($categories as $key => $category)
                    @php

                    if(!empty($category->photo))
                    $img = $category->photo->getUrl('thumb');
                    else
                    $img = asset('XR/assets/images/placeholder.png');

                    if($campaign->is_start == 1)
                    {
                    $categoryLink = route('campaigns.publishedProductByCategory',[$category->catagory_id,$campaign->public_link]);
                    }
                    else
                    {
                    $categoryLink = route('campaigns.publishedProductByCategory',[$category->catagory_id,$campaign->preview_link]);
                    }
                    @endphp
                    <!-- Single Catagory Card-->
                    <div class="col-6 col-md-4 col-lg-3">
                        <div class="card catagory-card">
                            <div class="card-body">
                                <!-- <a href="{{ $categoryLink }}"><img  class="image-size-2" src="{{ $img }}" alt=""/></a> -->
                                <!-- <a class="product-title d-block" href="{{ $categoryLink }}"><span>{!! $category->name ?? '&nbsp;' !!}</span></a> -->
                                <a class="product-thumbnail d-block" href="{{$categoryLink}}"><img class="image-size-2" src="{{ $img }}" alt="" /></a>
                                <a class="product-title d-block" href="{{$categoryLink}}">{!! $category->name ?? '&nbsp;' !!}</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @endif

    <!-- Top Products-->
    @if(count($data)>0)
    <div class="top-products-area clearfix py-3">
        <div class="container">
            <div class="section-heading d-flex align-items-center justify-content-between">
                <h6 class="ml-1">Products</h6>
                <a class="btn btn-danger btn-sm" href="{{$shopLink}}">View All</a>
            </div>

            <div id="catalog_prod" class="row g-3">

            </div>
        </div>
    </div>
    @endif
</div>
@endsection

@section('scripts')
@parent
<script src="{{asset('themes/'.$meta_data['themes']['name'].'/js/cart.js')}}"></script>
<script>
    window.onload = function() {
        //  slink=url.slice(url.lastIndexOf('/')+1);
        slink = "{{($userSettings->is_start == 1)?$campaign->public_link:$campaign->preview_link}}";
        track_events("catalog", null, slink);
    };

    var q = "";
    var searchData = "";
    var sort = "";
    var page = 0;
    var filter = [];
    var filterData = "";
    var preview = 0;
    var campaign_id = {
        {
            $campaign - > id
        }
    };
    var meta = @json($meta_data);
    var theme = meta['themes']['name'];
    var u = window.location.href;
    if (u.includes("preview")) {
        preview = 1;
    }
    var page_url = window.location.pathname;
    shareable_link = page_url.slice(page_url.lastIndexOf('/') + 1);
    // var shareable_link = "{{($userSettings->is_start == 1)?$campaign->public_link:$campaign->preview_link}}";

    getProducts(0);

    $("#CatalogProductSearch").on('input', function() {
        searchData = $(this).val();
        if (searchData.length >= 3 || searchData == "")
            //console.log(searchData);
            getProducts(0);
    });

    $("#ecommerce-productscatalog").on('click', '.pagination a', function(event) {
        event.preventDefault();

        $('li').removeClass('active');
        $(this).parent('li').addClass('active');

        var myurl = $(this).attr('href');
        var page = $(this).attr('href').split('page=')[1];

        getProducts(page);
    });


    function getProducts(page) {
        q = 'q=' + searchData + '&s=' + sort + '&page=' + page + '&preview=' + preview + '&filterData=' + filterData + '&shareable_link=' + shareable_link + '&theme=' + theme + '&filter=' + JSON.stringify(filter);
        //console.log(q);
        $('.loading1').show();
        $.ajax({
            url: "{{route('admin.campaigns.ajaxProductData')}}" + '?' + q,
            type: "get",
            datatype: "html"
        }).done(function(data) {
            //console.log(data);
            //console.log("sucess");
            $("#catalog_prod").empty().html(data);
            //$("#product_list").append(data);
            $('.loading1').hide();
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            $('.loading1').hide();
            // alert('No response from server');
        });
    }
</script>
@endsection
