<html>
<head>
    @include('themes.'.$meta_data['themes']['name'].'.partials.head')
    @yield('styles')
</head>
<body>
<div class="container">
   <header class="row">
       @include('themes.'.$meta_data['themes']['name'].'.partials.storefrontheader')
   </header>
   <div id="main" class="row">
           @yield('content')
   </div>
   <footer class="row">
       @include('themes.'.$meta_data['themes']['name'].'.partials.storefrontfooter')
   </footer>
</div>

<!-- All JavaScript Files-->
<script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/jquery.min.js') }}"></script>
<script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/waypoints.min.js') }}"></script>
<script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/jquery.easing.min.js') }}"></script>
<script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/jquery.counterup.min.js') }}"></script>
<script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/jquery.countdown.min.js') }}"></script>
<script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/default/jquery.passwordstrength.js') }}"></script>
<script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/wow.min.js') }}"></script>
<script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/jarallax.min.js') }}"></script>
<script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/jarallax-video.min.js') }}"></script>
<script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/default/dark-mode-switch.js') }}"></script>
<script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/default/no-internet.js') }}"></script>
<script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/default/active.js') }}"></script>
<script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/pwa.js') }}"></script>
<script>
function share(share_name,msg) {
            var u="{{Request::fullUrl()}}";
            var t=msg;
            // console.log(t);
            switch (share_name) {
                case 'fb':
                    window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t),'sharer','toolbar=0,status=0,width=626,height=436');
                    break;
                case 'whatsapp':
                    var message = encodeURIComponent(u);
                    // var whatsapp_url = "whatsapp://send?text=" + message;
                    // window.location.href = whatsapp_url;

                    var whatsapp_url = "https://wa.me/?text=" + message;
                    openInNewTab(whatsapp_url);
                    break;
                case 'twitter':
                    window.open("https://twitter.com/intent/tweet?text="+t+"&url="+u,'sharer','toolbar=0,status=0,width=626,height=436')
                    break;
            }
        }
</script>
@yield('scripts')
</body>
</html>