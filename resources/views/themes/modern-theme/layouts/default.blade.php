<html>

<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('themes.'.$meta_data['themes']['name'].'.partials.head')
    yield('styles')
</head>

<body>
    <div class="container">
        <header class="row">
            @include('themes.'.$meta_data['themes']['name'].'.partials.header')
        </header>
        <div id="main" class="row">
            @yield('content')
        </div>
        <footer class="row">
            @include('themes.'.$meta_data['themes']['name'].'.partials.footer')
        </footer>
        <div class="loading1" style="display:none">
            <!-- <div class="spinner-grow text-danger w-100 h-100" role="status">
            <span class="sr-only">Loading...</span>
        </div> -->
            <div class="spinner-{{ $meta_data['splash']['loader_type'] ?? 'border' }} w-100 h-100" style="color: {{ $meta_data['splash']['loader_color'] ?? '' }}" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>

    <!-- All JavaScript Files-->
    <script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/jquery.min.js') }}"></script>
    <script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/waypoints.min.js') }}"></script>
    <script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/default/jquery.passwordstrength.js') }}"></script>
    <script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/wow.min.js') }}"></script>
    <script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/jarallax.min.js') }}"></script>
    <script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/jarallax-video.min.js') }}"></script>
    <script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/default/dark-mode-switch.js') }}"></script>
    <script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/default/no-internet.js') }}"></script>
    <script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/default/active.js') }}"></script>
    <script src="{{ asset('themes/'.$meta_data['themes']['name'].'/js/pwa.js') }}"></script>
    <script>
        function track_events(event_name, product_id, slink) {

            $.ajax({
                url: "{{route('admin.stats.ajaxEvents')}}",
                type: "post",
                data: {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    "event": event_name,
                    "slink": slink,
                    "product_id": product_id,
                },
            });
        }
    </script>
    @yield('scripts')
</body>

</html>