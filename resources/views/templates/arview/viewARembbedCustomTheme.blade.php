@extends('layouts.viewar')
@section ('styles')
@parent
@php
if($campaign->is_start == 1) {
    $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
    $homeLink = route('campaigns.published',[$campaign->public_link]);
}else{
    $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
    $homeLink = route('admin.campaigns.preview',[$campaign->preview_link]);
}
$product = $data->product;
$obj_3d = "";
$obj_3d_ios = "";
$obj_id = "";
foreach ($product->arobject as $key => $arobject) {                        
    if($arobject->ar_type == "surface") {
        if(!empty($arobject->object_3d)) {
            $obj_3d = $arobject->object_3d->getUrl();
            $obj_id = $arobject->id;
        }
        if(!empty($arobject->object_3d_ios)) {
            $obj_3d_ios = $arobject->object_3d_ios->getUrl();
            $obj_id = $arobject->id;
        }      
    }                        
}
$product_meta_data = json_decode($product->scripts->meta_data ?? '',true);
@endphp
<style>
{!! $product->scripts->script_css ?? '' !!}
</style>
@endsection
@section('content')
<div id="loader-wrapper">
  <div id="mirrar-loader" class="text-center">
    <svg style="width: 200px" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 3044 745.74"><defs><style>.cls-1{fill:url(#linear-gradient);}.cls-2{fill:url(#linear-gradient-2);}.cls-3{fill:#231f20;}</style><linearGradient id="linear-gradient" x1="2228.65" y1="428.32" x2="2686.47" y2="428.32" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#a13c84"/><stop offset="1" stop-color="#5170d0"/></linearGradient><linearGradient id="linear-gradient-2" x1="1656.71" y1="426.77" x2="2163.6" y2="426.77" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#ef0a3b"/><stop offset="0.19" stop-color="#e80f42"/><stop offset="0.46" stop-color="#d41b54"/><stop offset="0.79" stop-color="#b43073"/><stop offset="1" stop-color="#9c3f89"/></linearGradient></defs><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><g id="AR_ICON_SVG"><path class="cls-1" d="M2686.47,656.15h-120l-32.26-43.39-9.32-12.53-.15-.2-13.35-19.63h0l-48.28-71H2339.18v28.24c0,.73,0,1.44,0,2.16V656.15H2230.23v-125h0V411.12h298.8c7,0,12.25-1.39,15.56-4.17a37.69,37.69,0,0,0,8.1-9.31,70,70,0,0,0,3.73-14.13V341.13q0-10.28-4.35-16.06a28.41,28.41,0,0,0-9.34-8.34,46.06,46.06,0,0,0-13.7-3.21H2343.26a115,115,0,0,1-79.06-31.23c-.67-.61-1.32-1.24-2-1.88h0c-2.65-2.62-5.18-5.38-7.57-8.24-.4-.48-.79-.95-1.18-1.44-.87-1.09-1.73-2.19-2.57-3.31l-.84-1.14a111.46,111.46,0,0,1-21.4-65.78h327.79a101.74,101.74,0,0,1,42.33,9A111.77,111.77,0,0,1,2657,269.85a114.31,114.31,0,0,1,8.41,43.67v84.12a109.91,109.91,0,0,1-6.84,38.84,125.23,125.23,0,0,1-18.37,33.08,103.58,103.58,0,0,1-27.71,24.71,107.26,107.26,0,0,1-34.85,13.81L2591,527.72Z" transform="translate(0 -26.74)"/><path class="cls-2" d="M2153.46,270.4a120.26,120.26,0,0,0-27.2-36.2A128.44,128.44,0,0,0,2086,209.64a134.61,134.61,0,0,0-49.33-9H1886.61v-.09h-45.35a113.83,113.83,0,0,0-113.83,113.83h53.5l2,0H2016.4q6.52,1.29,13.41,7.11t6.88,18.75v30.38h-253.8a136.4,136.4,0,0,0-49,8.71,129.34,129.34,0,0,0-40.25,24.24,113.9,113.9,0,0,0-27.19,35.88,99.26,99.26,0,0,0-9.79,43.62v56.87a101.43,101.43,0,0,0,9.79,44,112.93,112.93,0,0,0,27.19,36.2,128.91,128.91,0,0,0,40.25,24.22,135.76,135.76,0,0,0,49,8.74h257.88a134.78,134.78,0,0,0,60.89-14.26,123.17,123.17,0,0,0,26-17.56c22.2-19.64,35.95-46.78,35.95-76.77V314.35A98.42,98.42,0,0,0,2153.46,270.4ZM2036.69,511.47a62,62,0,0,1-4.34,14.22,45.32,45.32,0,0,1-9.44,9.7q-5.79,4.53-18.11,4.52h-190a38.65,38.65,0,0,1-16-3.23,31.83,31.83,0,0,1-10.88-8.4q-5.09-5.84-5.07-16.81a28.55,28.55,0,0,1,3.63-14.22,30.16,30.16,0,0,1,9.43-9.69q6.51-4.51,18.85-4.52h221.89Z" transform="translate(0 -26.74)"/><rect class="cls-3" x="2746.36" y="74.5" width="45.93" height="132.84" transform="translate(5538.64 255.1) rotate(-180)"/><rect class="cls-3" x="2702.9" y="31.04" width="45.93" height="132.84" transform="translate(2628.4 2796.59) rotate(-90)"/><rect class="cls-3" x="2702.9" y="683.1" width="45.93" height="132.84" transform="translate(1976.34 3448.64) rotate(-90)"/><rect class="cls-3" x="2746.36" y="612.9" width="45.93" height="132.84"/><rect class="cls-3" x="1583.44" y="47.76" width="45.93" height="132.84"/><rect class="cls-3" x="1626.9" y="31.04" width="45.93" height="132.84" transform="translate(1552.4 1720.59) rotate(-90)"/><rect class="cls-3" x="1626.9" y="683.1" width="45.93" height="132.84" transform="translate(900.35 2372.64) rotate(-90)"/><rect class="cls-3" x="1583.44" y="639.64" width="45.93" height="132.84" transform="translate(3212.82 1385.38) rotate(-180)"/></g><path d="M2884.81,45.28h-27.54V26.74H2935V45.28h-27.54V124.5h-22.63Z" transform="translate(0 -26.74)"/><path d="M2950.59,26.74h24.68l16.24,43.44c2,5.77,3.75,12,5.76,18h.61c2-6,3.7-12.28,5.71-18l15.73-43.44H3044V124.5h-20.6V88.62c0-9.53,1.82-23.75,2.89-33.21h-.61l-8.21,23.14L3003.32,116h-12.56l-14.17-37.45-8-23.14H2968c1.07,9.46,2.89,23.68,2.89,33.21V124.5h-20.29Z" transform="translate(0 -26.74)"/><path class="cls-3" d="M540,574.47V297.72a75.12,75.12,0,0,1,32.81,7.38,84.72,84.72,0,0,1,26.79,20,98.86,98.86,0,0,1,18.09,29.52,95.63,95.63,0,0,1,6.76,35.85V666.72a78.51,78.51,0,0,1-33.3-7.12,81.19,81.19,0,0,1-26.78-19.77,95.17,95.17,0,0,1-17.86-29.51A98.73,98.73,0,0,1,540,574.47Z" transform="translate(0 -26.74)"/><path class="cls-3" d="M710.94,662.22H804.8L830,627.94l7.29-9.9.12-.15,10.44-15.51v0l37.75-56.1h96.88v116h85.19V563.49h0V468.62H834q-8.26,0-12.16-3.29a29.36,29.36,0,0,1-6.33-7.35,55.75,55.75,0,0,1-2.92-11.17V413.33c0-5.41,1.12-9.64,3.4-12.69a22.23,22.23,0,0,1,7.3-6.59A35.53,35.53,0,0,1,834,391.52H979.32a89.41,89.41,0,0,0,61.82-24.68c.52-.48,1-1,1.55-1.49h0q3.11-3.11,5.92-6.51c.32-.37.62-.75.93-1.13.68-.86,1.35-1.74,2-2.62l.65-.9a88.78,88.78,0,0,0,16.74-52H812.62a78.93,78.93,0,0,0-33.11,7.1A87.93,87.93,0,0,0,734,357a91.16,91.16,0,0,0-6.58,34.51V458a87.52,87.52,0,0,0,5.35,30.68,99,99,0,0,0,14.37,26.14,81.37,81.37,0,0,0,21.66,19.53,83.72,83.72,0,0,0,27.26,10.91l-10.44,15.51Z" transform="translate(0 -26.74)"/><path class="cls-3" d="M1474.17,662.22H1380.3l-25.22-34.28-7.29-9.9-.12-.15-10.44-15.51v0l-37.75-56.1H1202.6v22.32c0,.57,0,1.13,0,1.7v91.94h-85.18V563.49h0V468.62h233.65q8.28,0,12.17-3.29a29.36,29.36,0,0,0,6.33-7.35,55.75,55.75,0,0,0,2.92-11.17V413.33q0-8.11-3.4-12.69a22.23,22.23,0,0,0-7.3-6.59,35.53,35.53,0,0,0-10.72-2.53H1205.79A89.41,89.41,0,0,1,1144,366.84c-.52-.48-1-1-1.55-1.49h0c-2.07-2.07-4.06-4.24-5.92-6.51-.32-.37-.62-.75-.93-1.13-.68-.86-1.35-1.74-2-2.62l-.65-.9a88.59,88.59,0,0,1-16.74-52h256.32a79,79,0,0,1,33.11,7.1A87.93,87.93,0,0,1,1451.11,357a91.16,91.16,0,0,1,6.58,34.51V458a87.52,87.52,0,0,1-5.35,30.68A99,99,0,0,1,1438,514.8a81.37,81.37,0,0,1-21.66,19.53,83.72,83.72,0,0,1-27.26,10.91l10.44,15.51Z" transform="translate(0 -26.74)"/><path class="cls-3" d="M439.65,320c-22.53-26.95-60.75-28.79-85.38-4.12L206.05,464.46,88.38,323.69l-6.06-7.25c-20-23.94-53.94-25.57-75.8-3.66L3,316.26H3c-1.43,1.43-2.25,5.59-2.25,5.59L0,663.51c48.38.11,87.69-42.74,87.82-95.71l.25-110.39,57,68.17,55.8,66.74,61-61.12,92.09-92.28-.52,228.16c48.37.11,87.69-42.74,87.81-95.71l.57-245.32a9.46,9.46,0,0,0-2.13-6Z" transform="translate(0 -26.74)"/><line class="cls-3" x1="3.04" y1="289.52" x2="0.79" y2="291.77"/><polygon class="cls-3" points="200.85 565.59 200.76 565.67 144.96 498.93 145.05 498.84 200.85 565.59"/></g></g></svg>
    <br /><br />
    <div class="loader4"></div>
  </div>
  <div class="loader-section section-left"></div>
  <div class="loader-section section-right"></div>
</div>
<div style="display: flex; flex-flow: column; height: 100%">
  <div class="row" style="flex: 1 1 auto">       
    <mirrar-viewar id="mirrar-viewar-demo" src="{{ $obj_3d }}" 
      ios-src="{{ $obj_3d_ios }}" 
      poster="{{$product->photo[0]->getUrl()}}"
      ar="ar" auto-rotate="auto-rotate" 
      camera-controls="camera-controls" 
      {{!empty($product_meta_data['disable_zoom'])? 'disable-zoom' : ''}}
      ar-scale="{{!empty($product_meta_data['disable_zoom'])? 'fixed' : 'auto'}}"
      slot="myprogress-bar"
      quick-look-browsers="safari chrome" class="col-10 col-centered mirrar-viewar-demo">       
      {!! $product->scripts->script_html ?? '' !!}
    </mirrar-viewar>
  </div>
@endsection
@section('scripts')
@parent
 <script>
 $("#loading-bg").remove();
 {!! $product->scripts->script_js ?? '' !!}
</script>
@endsection