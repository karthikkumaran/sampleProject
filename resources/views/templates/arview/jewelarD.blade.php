@extends('layouts.cam')
@section ('styles')
@parent
<link rel="stylesheet" type="text/css" href="{{ asset('XR/themes/jewelar/css/desktop.css') }}">
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
<style>
</style>
@endsection
@section('content')
@php
if($campaign->is_start == 1) {
    $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
    $homeLink = route('campaigns.published',[$campaign->public_link]);
}else{
    $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
    $homeLink = route('admin.campaigns.preview',[$campaign->preview_link]);
}
@endphp
<section id="content-desktop" class="camScreen">
    <div class="desktop-container" style="top:0;">
        <div class="col-lg-12 col-sm-12">
            <div class="row">
                <div class="col-lg-4 col-sm-4">
                    <h2 class="text-center m-2 w-100">Products</h2>
                </div>
                <div class="col-lg-4 col-sm-4">
                    <h2 class="text-center m-2 w-100">Virtual Tryon</h2>
                </div>
                <div class="col-lg-4 col-sm-4">
                    <h2 class="text-center m-2 w-100">Details</h2>                    
                </div>
            </div>
            <div class="row">
                    <!---- left panel ----->
                    <div class="col-lg-4 col-sm-4">
    <div class="container-fluid">
        <div class="row">
            <div class="scrollbar1 niceScroll" id="style-3">
                <div class="force-overflow mb-5">
                    <div class="row">
                        @foreach ($data as $key => $card)
                        @php
                        $product = $card->product;
                        $img_url = array();
                        $obj_url = array();
                        $obj_position = "";
                        $obj_position1 = "";
                        $obj_position2 = "";
                        if(count($product->photo) > 0){
                            for($i=0;$i < count($product->photo);$i++)
                                array_push($img_url,$product->photo[$i]->getUrl());
                        } else if(count($product->arobject)>0){
                            array_push($img_url,$product->arobject[0]->object->getUrl());
                        }
                        for($i=0;$i < count($product->arobject);$i++) {    
                            //array_push($obj_url,$product->arobject[$i]->object->getUrl());
                                if($product->arobject[$i]->position == "jwl-nck") {
                                    $obj_position = "NECK";
                                    $obj_position1 = "NECK";
                                    $obj_url["NECK"] = $product->arobject[$i]->object->getUrl();
                                } else if($product->arobject[$i]->position == "jwl-ear-l"){
                                    $obj_position = "EAR";
                                    $obj_position2 = "EAR";
                                    $obj_url["EAR"] = $product->arobject[$i]->object->getUrl();
                                } else if($product->arobject[$i]->position == "jwl-ear-r"){
                                    $obj_position = "EAR";
                                    $obj_position2 = "EAR";
                                    $obj_url["EAR"] = $product->arobject[$i]->object->getUrl();
                                }
                                if(!empty($obj_position1) && !empty($obj_position2))
                                    $obj_position = "SET";
                        }
                        $product_name= $product->name;
                        //$selected_product['img_url'] = $img_url;
                        //$selected_product['name'] = empty($product_name)?'No Title':$product_name;
                        $product_json = array('id' => $product->id,
                        'name' =>$product->name ?? '&nbsp;',
                        'img_url' => $img_url,
                        'obj_url' => $obj_url,
                        'description' =>htmlentities($product->description),
                        'price' => empty($product->price)?"":"₹". ($product->price ?? '&nbsp;'),
                        'price_amt' => ($product->price ?? ''),
                        'sku' =>$product->sku ?? '',
                        'discount_price' =>$product->discount_price,
                        'url' => $product->url ?? '#');

                        $product_json = json_encode($product_json,true);
                        @endphp
                        @if(count($obj_url)>0)
                        @if(isset($_GET['sku']) && $_GET['sku'] == $product->id ||
                        $loop->index == 0)
                        <script>
                            var product_json_selected = '<?php echo $product_json;?>';
                            var product_id_selected = '<?php echo $product->id;?>';
                            var product_position_selected = '<?php echo $obj_position;?>';
                            // console.log("selected",product_json_selected);
                        </script>
                        @endif
                        <div class="col-sm-6 col-lg-6 mb-2 pl-0">
                            <div class="card ecommerce-card" id="sku_{{$product->id}}" data-product="{{$product_json ?? ''}}"
                                onclick="init_vto.selImg(this,'{{$obj_position}}','','sku_{{$product->id}}')">
                                <div class="card-content p-1">
                                    <div class="item-img text-center" style="min-height: 12.85rem;">
                                        <a href="#">
                                                <img class="img-fluid lazyload" loading="lazy" data-src="{{ $img_url[0] ?? asset('XR/assets/images/placeholder.png') }}"
                                            onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="width: 150px; max-height:150px;"/></a>

                                    </div>
                                    <div class="card-body">
                                        <div class="item-wrapper">
                                            <div class="item-rating hidden">
                                            </div>
                                            <div>
                                                <h6 class="item-price text-left">
                                                {{empty($product->price)?"":"₹"}} {{$product->price ?? ''}}
                                                </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="position-absolute h-100 w-100 mypanel-center bg-light startTryon" style="opacity:0.3">
                    </div>
</div>
<!--- end left panel --->    
<!--- center panel --->
                    <div class="col-lg-4 col-sm-4 p-0">
                        <div class="container-fluid p-0">
                            <!-- <canvas width="350" height="250" id='video'></canvas> -->
                            <div id="VTO">
                            <img style="display:none" src="" id="myimg" />
        <video class="vto" style="z-index:-10;" id="video" crossorigin="*" autoplay muted loop
        playsinline></video>
    <canvas class="vto" id="vtovideo"></canvas>
    <canvas class="vto" id="vtooverlay"></canvas>
    <canvas id="vtophoto"></canvas>
    </div>
                            <div class="position-absolute h-100 w-100 mypanel-center">
                                <div class="float-left text-white" style="margin: 4px;">
                                    <h6 class="text-white">Powered by</h6>
                                    <h4 class="text-white">MirrAR</h4>
                                </div>
                                <div class="float-right" style="max-width: 150px;max-height: 150px; margin: 2px;">
                                    <!-- <img class="img-fluid bg-white" src="{{asset('XR/app-assets/images/logo/logo.png')}}"> -->
                                    @if(!empty($meta_data['splash']))
                                    @if($meta_data['splash']['brand_type'] == 2 && $meta_data['splash']['brand_name'] != "")
                                        <h1 class="bg-white" style="padding: 3px;">{{$meta_data['splash']['brand_name']}}</h1>
                                    @else
                                        <img class="img-fluid bg-white" src="{{ $campaign->getFirstMediaUrl('splashlogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                                                onerror="this.onerror=null;this.src='{{asset('XR/app-assets/images/logo/logo.png')}}';" />
                                    @endif
                                    @else
                                        <img class="img-fluid bg-white" src="{{ $campaign->getFirstMediaUrl('splashlogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                                            onerror="this.onerror=null;this.src='{{asset('XR/app-assets/images/logo/logo.png')}}';" />
                                    @endif
                                </div>
                                <div class="w-100 h-100 position-absolute mypanel-center">
                                <h4 class="m-4 p-1" id="toomanyfacemsg">Too many people there, please choose one person photo</h4>
                                <h4 class="m-4 p-1" id="nofacemsg">No face detected, please choose a photo with human face</h4>
                                </div>
                                <img class="cph position-absolute mypanel-center" src="{{ asset('XR/core/faceholder.png') }}" style="width: 200px;">
                                <div class="camicon position-absolute">
                                    <img onclick="init_vto.takePicture(mycallback)" src="{{ asset('XR/themes/jewelar/img/capture.png')}}"
                                        style="width:75px;/*position:absolute;*/">
                                    {{-- <a href="#" style="font-size:12px;position:absolute;margin-top:-65px;cursor:pointer;right:5px;">
                                        <img class="img-fluid text-center" id="upload-photo-img" src="{{ asset('XR/themes/jewelar/img/upload.png')}}" style="width: 60px;">
                                        <img class="img-fluid text-center" id="change-photo-img" src="{{ asset('XR/themes/jewelar/img/changeupload.png')}}" style="width: 60px;display:none;">
                                        <input type="file" class="upload-photo" id="uploadphoto" accept="image/*"/>
                                    </a>
                                    <img onclick="livemode()" id="livemode" src="{{ asset('XR/themes/jewelar/img/cameraLive.png')}}"
                                        style="width:72px;margin-top:-215px;visibility: hidden;"> --}}
                                </div>
                            </div>
                            <div class="position-absolute h-100 w-100 mypanel-center bg-light startTryon" style="display: none">
                                <div class="position-absolute mypanel-center h-100 p-2">
                              <h4>GET READY!</h4>
                                <div class="startTryon-logo mr-2">
                                @if(!empty($meta_data['splash']))    
                                @if($meta_data['splash']['brand_type'] == 2 && $meta_data['splash']['brand_name'] != "")
                                    <h1 style="color: {{ $meta_data['splash']['brand_color'] ?? '' }}">{{$meta_data['splash']['brand_name']}}</h1>
                                @else
                                    <img src="{{ $campaign->getFirstMediaUrl('splashlogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                                        onerror="this.onerror=null;this.src='{{asset('XR/app-assets/images/logo/logo.png')}}';" alt="Logo">
                                @endif
                                @else
                                    <img src="{{ $campaign->getFirstMediaUrl('splashlogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                                        onerror="this.onerror=null;this.src='{{asset('XR/app-assets/images/logo/logo.png')}}';" alt="Logo">
                                @endif
                               <p class="mt-1"><input type="checkbox" id="iagree"> I agree <br>
                               to the processing of my image by MirrAR as set out in the <a href="{{route('landing.privacyPolicy')}}" target="blank">privacy policy.</a></p>
                                <button type="button" class="btn btn-white btn-lg mt-0" id="liveCamera" style="width: 90px;font-size:1em;" disabled ><i class="feather icon-camera"></i> Start</button><br><br>
                                {{-- <button type="button" class="btn btn-white btn-lg mt-0" id="photoMode" style="width: 180px;font-size:1em;" disabled><i class="feather icon-upload"></i> upload photo</button> --}}
                                
                            </div>
                                </div>
                                <!-- <img class="position-absolute mypanel-center" src="{{ asset('XR/core/faceholder.png') }}"> -->
                            </div>
                        </div>
                    </div>
<!--- end center panel --->
<!--- right panel --->
<div class="col-lg-4 col-sm-4 scrollbar1 niceScroll" id="style-3">
<h2 class="text-center text-capitalize product_name">Products</h2>
<div class="force-overflow mb-5">
    <!--Carousel Wrapper-->
    <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-interval="false" data-ride="carousel">
        <!--Slides-->
        <div class="carousel-inner" id="product_details_img">            
        </div>
        <ol class="carousel-indicators" id="product_items_img">
        </ol>

        <!--/.Slides-->
        <!--Controls-->
        <a class="carousel-control-prev" href="#carousel-thumb" role="button" data-slide="prev">
                    <span class="feather icon-chevron-left text-light font-large-2" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel-thumb" role="button" data-slide="next">
                    <span class="feather icon-chevron-right text-light font-large-2" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
        <!--/.Controls-->
        


    </div>

    <h2 class="text-left mb-2">
        <!-- <a href="#" class="btn btn-danger float-right buynow">{{ trans('cruds.product.fields.order') }}</a>
            <span class="product_price"></span> -->
        <!-- </h2> -->
            <div class="cart btn btn-danger float-right">
                    <i class="feather icon-shopping-cart"></i> 
                    <span class="add-to-cart" style="cursor:pointer;">Add to cart</span> 
                    <a href="{{ $checkoutLink }}" target="_blank" class="view-in-cart d-none text-white">View In Cart</a>
            </div>
            <span class="product_price"></span>
                    </h2>
    <div class="scrollbar2 text-left niceScroll" id="style-3">
        <div class="mb-2"><p class="product_desc"></p>
        </div>
    </div>
    <div class="position-absolute h-100 w-100 mypanel-center bg-light startTryon" style="opacity:0.3">
                    </div>
</div>
<!-- end right panel -->
<!-- The Modal -->

<div class="modal overlay pl-0" style="overflow: hidden;" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal body -->
            <div class="modal-body" style="padding: 0rem;">
                <div class="hero-container">

                    <!--Carousel Wrapper-->
                    <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">
                        <!--Indicators-->

                        <DIV class="carousel-indicators" style="overflow-X: scroll;margin: 0px;">

                            <div class="row flex-row flex-nowrap">

                                <div class="card card-block selected">
                                    <img data-target="#multi-item-example" data-slide-to="0" class="active"
                                        src="{{ asset('XR/themes/jewelar/img/neck.png')}}" alt="img"
                                        style="WIDTH:75px">

                                </div>
                                <div class="card card-block selected">
                                    <img data-target="#multi-item-example" data-slide-to="1"
                                        src="{{ asset('XR/themes/jewelar/img/neck.png')}}" style="WIDTH:75px">

                                </div>
                                <div class="card card-block selected">
                                    <img data-target="#multi-item-example" data-slide-to="2"
                                        src="{{ asset('XR/themes/jewelar/img/neck.png')}}" style="WIDTH:75px">

                                </div>
                                <div class="card card-block selected">
                                    <img data-target="#multi-item-example" data-slide-to="3"
                                        src="{{ asset('XR/themes/jewelar/img/neck.png')}}" style="WIDTH:75px">

                                </div>
                                <div class="card card-block selected">
                                    <img data-target="#multi-item-example" data-slide-to="4"
                                        src="{{ asset('XR/themes/jewelar/img/neck.png')}}" style="WIDTH:75px">

                                </div>
                                <div class="card card-block selected">
                                    <img data-target="#multi-item-example" data-slide-to="4"
                                        src="{{ asset('XR/themes/jewelar/img/neck.png')}}" style="WIDTH:75px">

                                </div>
                            </div>

                        </DIV>


                        <!--Slides-->
                        <div class="carousel-inner" role="listbox" style="height: 100vh;padding-top: 100px;">

                            <!--First slide-->
                            <div class="carousel-item active">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card mb-2 " style="border:unset">
                                            <img class="card-img-top"
                                                src="{{ asset('XR/themes/jewelar/img/neck.png')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!--Second slide-->
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card mb-2" style="border:unset">
                                            <img class="card-img-top"
                                                src="{{ asset('XR/themes/jewelar/img/neck.png')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!--Third slide-->
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card mb-2" style="border:unset">
                                            <img class="card-img-top"
                                                src="{{ asset('XR/themes/jewelar/img/neck.png')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!--/.fourth slide-->
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card mb-2" style="border:unset">
                                            <img class="card-img-top"
                                                src="{{ asset('XR/themes/jewelar/img/neck.png')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!--/.fifth slide-->
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card mb-2" style="border:unset">
                                            <img class="card-img-top"
                                                src="{{ asset('XR/themes/jewelar/img/neck.png')}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--close botton -->
                            <div class="overlay" style="overflow: hidden;">
                                <h2 class="product_name position-absolute p-1"></h2>
                                <a href="#" class="icon" style="color: black;">
                                    <img src="{{ asset('XR/themes/jewelar/img/close.png')}}" data-dismiss="modal"
                                        style="width: 30px;">
                                </a>
                            </div> 


                        </div>
                        <!--/.Carousel Wrapper-->


                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</div>
<!--- end right panel --->


    </div>
</div>
       


        
        

    </div>

    

    <!-- Modal -->
    <div class="modal fade p-0" id="MirrAR_preview" role="dialog">
        <div class="modal-dialog" style="max-width: 100%;">

            <!-- Modal content-->
            <div class="modal-content rounded-0">

                <div class="modal-body p-0" style="height: 99vh;">

                    <div class="desktop-container position-absolute" style="bottom:auto;left:3px;right:3px;top:3px;">
                        <img id="preimg" src="" style="width: 100%;height: 90vh;">
                        <div class="overlay">
                            <a href="#" class="icon">
                                <img src="{{ asset('XR/themes/jewelar/img/close1.png')}}" data-dismiss="modal"
                                    style="width: 30px;">
                            </a>
                        </div>
                    </div>

                    <div class="desktop-container  position-absolute">
                        <a data-toggle="collapse" data-parent="#accordion" href="#share">
                            <img class="share-footer" src="{{ asset('XR/themes/jewelar/img/share.svg')}}"
                                style="float: left;"></a>

                        <a href="#" onclick="init_vto.savePicture()"><img class="share-footer"
                                src="{{ asset('XR/themes/jewelar/img/download.svg')}}" style="float: right;">
                        </a>
                    </div>
                    <div class="desktop-container position-absolute d-inline">


                        <div class="panel-group" id="accordion" style=" background-color: #e4eef5;border-radius: 20px;">
                            <div class="panel panel-default">

                                <div id="share" class="panel-collapse collapse in"
                                    style=" background-color: rgb(255, 255, 255);border-top-right-radius: 20px;border-top-left-radius: 20px;">
                                    <div class="container" style=" padding-bottom: 26px;">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#share"><img
                                                src="{{ asset('XR/themes/jewelar/img/min.png')}}"></a>
                                        <div class="clesrfix"></div>
                                        <a class="btn btn-lg btn-social btn-facebook bstyle" href="#"
                                            onclick='share("fb","")'>
                                            <i class="fa fa-facebook fa-fw"></i> Share on Facebook&nbsp;
                                        </a><br>
                                        <a class="btn btn-lg btn-social btn-whatsapp bstyle" href="#"
                                            onclick='share("whatsapp","")'>
                                            <i class="fa fa-whatsapp fa-fw"></i> Share on whatsapp
                                        </a><br>
                                        <a class="btn btn-lg btn-social btn-twitter bstyle" href="#"
                                            onclick='share("twitter","")'>
                                            <i class="fa fa-twitter fa-fw"></i> Share on twitter&nbsp;&nbsp;&nbsp;
                                        </a><br>
                                        <a class="btn btn-lg btn-social btn-twitter bstyle" href="#"
                                            onclick='share("copy","")'>
                                            <i class="fa fa-copy fa-fw"></i> Copy Share Link&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </a><br>
                                        <!-- <a class="btn btn-lg btn-social btn-pin" style="color:white;font-size:16px;padding:12px 47px!important;">
                              <i class="fa fa-pinterest fa-fw"></i> Share on pinterest
                            </a><br>        -->

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>



</section>
<!-- End Section -->
<!-- error -->
<div id="camError" class="text-center p-2 h-100"></div>

@endsection
@section('scripts')
@parent
<script src="{{asset('XR/assets/js/cart.js')}}"></script>

<script src="{{ asset('XR/core/finit.min.js') }}"></script>
<script src="{{ asset('XR/core/fbackend.min.js') }}"></script>
<script src="{{ asset('XR/core/fdetector.min.js') }}"></script>
<script src="{{ asset('XR/core/init.min.js') }}"></script>


<script>

// $('img').on("error", function() {
//         $(this).attr('src', '{{asset('XR/assets/images/placeholder.png')}}');
//         $(this).css('width', '95%');
//         $(this).css('max-height', '100%');
//     });

    var mycallback = function (img) {
        var selectedProducts = {
            "photo": img,
            "cid": "{{$campaign->id}}",
            "pid": product_id_selected
        };
        $.ajax({
            type: "POST",
            url: "{{ route("captures.store") }}",
            data: selectedProducts,
            success: function (data) {
                share_url = data;
                $('#share').collapse('show');
            }
        });
    }


    $(document).ready(function () {
    if (localStorage.getItem("shoppingCart") != null) {
    loadCart();
  }
});




 
</script>
@endsection
