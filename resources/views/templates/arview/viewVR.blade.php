@php

if($userSettings->is_start == 1) {
    $sharableLink=$userSettings->public_link;
    $shoppableLink =  route('mystore.publishedVideo',$sharableLink);
    if(!empty($userSettings->customdomain)){
        $homeLink = '//'.$userSettings->customdomain;
        $sLink=$userSettings->customdomain;

    }else if(!empty($userSettings->subdomain)) {
        $homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');
        $sLink=$userSettings->subdomain;

    }else{
        $homeLink = route('mystore.published',[$userSettings->public_link]);
        $sLink=$userSettings->public_link;
    }
    }else{
        $homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
        $shoppableLink =  route('mystore.publishedVideo',$userSettings->preview_link);
        $sLink=$userSettings->preview_link;
    }

    if($campaign->is_start == 1) {

$checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
$catalogLink = route('campaigns.published',[$campaign->public_link]);

if(!empty($userSettings->customdomain)){

$homeLink = '//'.$userSettings->customdomain;

}else if(!empty($userSettings->subdomain)) {

$homeLink = '//'. $userSettings->subdomain.".".env('SITE_URL');

}else{

$homeLink = route('mystore.published',[$userSettings->public_link]);

}

}else{
    
$checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
$checkoutLink = route('admin.campaigns.preview',[$campaign->preview_link]);
$homeLink = route('admin.mystore.preview',[$userSettings->preview_link]);
$sLink = $userSettings->preview_link;
}

@endphp

@extends('themes.default-theme.layouts.viewvrstore')
@section('content')
<div class="p-0 m-0" style="position: relative; width: 100vw; height: 100vh;">
  <iframe class="m-0 p-0" src="{{ env('VR_STORE_URL') . $vr_store_url  }}" title="Mirrar VR" id="mirrar-vr" style="position: absolute; height: 100%; width: 100%; top: 0;"></iframe>
  @include('themes.'.$meta_data['themes']['name'].'.partials.vr_store_header', ['checkoutlink' => $checkoutLink ])
  <div class="position-absolute" id="mirrar-vr-cart" style="position: absolute; height: 80%; width: 80%; top: 50%; left: 50%; transform: translate(-50%, -50%); display: none;">
    <iframe class="m-0 p-0 w-100 h-100" src="{{ $checkoutLink  }}" id="mirrar-vr-cart-iframe" title="Mirrar VR Cart"></iframe>
    <img class="position-absolute" src="{{ asset('XR/themes/vrview/img/close.svg') }}" style="top: -6px; right: -6px;" width="30" onclick="toggleCart()" >
  </div>
</div>
@endsection