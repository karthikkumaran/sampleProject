@extends('layouts.viewar')
@section ('styles')
@parent
@php
if($campaign->is_start == 1) {
    $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
    $homeLink = route('campaigns.published',[$campaign->public_link]);
}else{
    $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
    $homeLink = route('admin.campaigns.preview',[$campaign->preview_link]);
}
$product = $data->product;
$obj_3d = "";
$obj_3d_ios = "";
$obj_id = "";
foreach ($product->arobject as $key => $arobject) {                        
    if($arobject->ar_type == "surface") {
        if(!empty($arobject->object_3d)) {
            $obj_3d = $arobject->object_3d->getUrl();
            $obj_id = $arobject->id;
        }
        if(!empty($arobject->object_3d_ios)) {
            $obj_3d_ios = $arobject->object_3d_ios->getUrl();
            $obj_id = $arobject->id;
        }      
    }                        
}
$product_meta_data = json_decode($product->scripts->meta_data ?? '',true);
@endphp
<style>
{!! $product->scripts->script_css ?? '' !!}
</style>
@endsection
@section('content')
  <div id="loader-wrapper">
    <div id="mirrar-loader" class="text-center">
      <!-- <img style="width:200px" src="{{ asset('mirrar-viewar/icons/logo_color.svg') }}"><br><br> -->
      <h4 class="text-black">Loading...</h4>
      <div class="loader4"></div>
    </div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
  </div>
  <div style="display:flex;flex-flow:column;height:100%">
    <div id="top-banner">
      <div style="margin-top:3px;" class="text-center"><span class="col top-banner-text">{{ $product->name ?? "Our Product" }}</span></div>
    </div>
    <div class="row" style="flex:1 1 auto">
      <div class="col-lg-9 col-md-12 col-sm-12 col-12">
        <div class="row">
          <div class="logo-container p-1 "><span style="color:#9b9b9b;font-size:14px;font-weight:700">Powered
              By&nbsp;</span> <img style="width:150px" src="{{ asset('mirrar-viewar/icons/logo_white_color.svg') }}"></div>
        </div>        
          <mirrar-viewar id="mirrar-viewar-demo" src="{{ $obj_3d }}" 
            ios-src="{{ $obj_3d_ios }}" 
            poster="{{$product->photo[0]->getUrl()}}"
            ar="ar" auto-rotate="auto-rotate" 
            camera-controls="camera-controls" 
            {{!empty($product_meta_data['disable_zoom'])? 'disable-zoom' : ''}}
            ar-scale="{{!empty($product_meta_data['disable_zoom'])? 'fixed' : 'auto'}}"
            slot="myprogress-bar"
            quick-look-browsers="safari chrome" class="col-10 col-centered mirrar-viewar-demo">
             <!-- Full Screen Btn -->
          <div id="full-screen-container" class="d-inline-flex" style="position: absolute; left: 20px;">
            <div id="full-screen-btn" style="left:0;top:20px;cursor:pointer;" onclick="fullScreen()">
              <img id="full-screen-img" height="20px" src="{{ asset('mirrar-viewar/icons/full-screen.svg') }}" />
              <img id="exit-full-screen-img" height="20px" src="{{ asset('mirrar-viewar/icons/exit-full-screen.svg') }}" style="display: none" />
            </div>
            <div id="share-btn" style="position: absolute; left: 40px;top: 20px;cursor:pointer;">
              <img height="20px" src="{{ asset('mirrar-viewar/icons/share.png') }}" onclick="onShare()" />
            </div>
          </div>
          <div class="ar-brand bg-white p-1 d-none" style="position:absolute;bottom:5px;left:10px;width:200px"><span
              style="font-size:10px;font-weight:700">Powered By &nbsp;&nbsp;</span> <img
              style="position:absolute;width:120px" src="{{ asset('mirrar-viewar/icons/logo_color.svg') }}"></div><button slot="ar-button"
            class="custom-ar-btn">Tap here to place in your space</button>
          <div slot="progress-bar" class="custom-progress-bar text-center">
            <h4>3D Object Loading...</h4>
            <div class="progress">
              <div class="progress-done" data-done="0">0%</div>
            </div>
          </div>          
            {!! $product->scripts->script_html ?? '' !!}
        </mirrar-viewar>
      </div>
      <div class="col-lg-3 col-md-4 d-none d-sm-none d-md-none d-lg-block my-auto mx-auto">
      <div id="scan-qr-code-text">Scan to view in your mobile</div><img height="200px" id="qr-code-gen"
          style="background-color:#fff;margin:auto"
          src="http://chart.apis.google.com/chart?chs=177x177&amp;cht=qr&amp;chl=mirrar&amp;choe=UTF-8" alt="my alt">          
      </div>
    </div>
  </div>
@endsection
@section('scripts')
@parent
 <script>
 $("#loading-bg").remove();
 {!! $product->scripts->script_js ?? '' !!}
</script>
@endsection