@extends('layouts.cam')
@section ('styles')
@parent
<link rel="stylesheet" type="text/css" href="{{ asset('XR/themes/jewelar/css/style.css') }}">
<style>

</style>
@endsection
@section('content')
@php
if($campaign->is_start == 1) {
    $checkoutLink = route('campaigns.publishedCheckout',[$campaign->public_link]);
    $homeLink = route('campaigns.published',[$campaign->public_link]);
}else{
    $checkoutLink = route('campaigns.publishedCheckout',[$campaign->preview_link]);
    $homeLink = route('admin.campaigns.preview',[$campaign->preview_link]);
}
@endphp

<section id="hero" class="wow fadeIn camScreen">
    <div class="hero-container" style="top:0">
        <!-- <canvas width="350" height="250" id='video'></canvas> -->
        <div id="VTO">
            <img style="display:none;width:100%;" src="" id="myimg" />
            <video class="vto" style="z-index:-10;" id="video" crossorigin="*" autoplay muted loop playsinline></video>
            <canvas class="vto" id="vtovideo"></canvas>
            <canvas class="vto" id="vtooverlay"></canvas>
            <canvas id="vtophoto"></canvas>
        </div>
        <div class="position-absolute w-100 mypanel-center mt-0" style="height: 80%;" id="myswipe">
                                <div class="float-left text-white" style="margin: 4px;">
                                    <h6 class="text-white">Powered by</h6>
                                    <h4 class="text-white">MirrAR</h4>
                                </div>
                                <div class="float-right" style="max-width: 150px;max-height: 150px; margin: 2px;">
                                    <!-- <img class="img-fluid bg-white" src="{{asset('XR/app-assets/images/logo/logo.png')}}"> -->
                                    @if(!empty($meta_data['splash']))
                                    @if($meta_data['splash']['brand_type'] == 2 && $meta_data['splash']['brand_name'] != "")
                                        <h1 class="bg-white" style="padding: 3px;">{{$meta_data['splash']['brand_name']}}</h1>
                                    @else
                                        <img class="img-fluid bg-white" src="{{ $campaign->getFirstMediaUrl('splashlogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                                                onerror="this.onerror=null;this.src='{{asset('XR/app-assets/images/logo/logo.png')}}';" />
                                    @endif
                                    @else
                                        <img class="img-fluid bg-white" src="{{ $campaign->getFirstMediaUrl('splashlogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                                            onerror="this.onerror=null;this.src='{{asset('XR/app-assets/images/logo/logo.png')}}';" />
                                    @endif
                                </div>
                                <div class="w-100 h-100 position-absolute mypanel-center">
                                    <h4 class="m-4 p-1" id="toomanyfacemsg">Too many people there, please choose one person photo</h4>
                                    <h4 class="m-4 p-1" id="nofacemsg">No face detected, please choose a photo with human face</h4>
                                </div>
                                <img class="cph position-absolute mypanel-center" src="{{ asset('XR/core/faceholder.png') }}" style="width: 200px;">
                                <!-- <div class="camicon position-absolute">
                                    <img onclick="takePicture(mycallback)" src="{{ asset('XR/themes/jewelar/img/capture.png')}}"
                                        style="width:75px">
                                </div> -->
                            </div>
    </div>
    <div class="hero-container">
        <div class="camicon">
            <!-- <img onclick="flipCamera()" src="{{ asset('XR/themes/jewelar/img/cameraflip.png')}}" style="width: 75px;"><BR><BR> -->
            <img onclick="init_vto.takePicture(mycallback)" src="{{ asset('XR/themes/jewelar/img/capture.png')}}"
                style="width:75px;margin-right:-20px">
            {{-- <a href="#" style="font-size:12px;position:absolute;margin-top:-65px;cursor:pointer;right:5px;">
                <img class="img-fluid text-center" id="upload-photo-img" src="{{ asset('XR/themes/jewelar/img/upload.png')}}" style="width: 60px;">
                <img class="img-fluid text-center" id="change-photo-img" src="{{ asset('XR/themes/jewelar/img/changeupload.png')}}" style="width: 60px;display:none;">
                <input type="file" class="upload-photo" id="uploadphoto" accept="image/*"/>
            </a>
            <img onclick="livemode()" id="livemode" src="{{ asset('XR/themes/jewelar/img/cameraLive.png')}}"
                style="width:72px;position:absolute;margin-top:-135px;right:0px;visibility: hidden;"> --}}
        </div>
        <div class="panel-group box2" id="accordion">
            <div class="panel panel-default">
                <div id="collapse1" class="panel-collapse collapse show in box1">
                    <div class="container">
                        <a onclick="productList();">
                            <img src="{{ asset('XR/themes/jewelar/img/min.png')}}"></a>
                        <span style="text-align:left">
                            <h3 class="product_name text-capitalize text-truncate pb-1" id="product_title_btn"></h3>
                        </span>
                        <span style="float: right;">&nbsp;<div class="cart btn btn-danger float-right">
                    <i class="feather icon-shopping-cart"></i> 
                    <span class="add-to-cart" style="cursor:pointer;">Add to cart</span> 
                    <a href="{{ $checkoutLink }}" target="_blank" class="view-in-cart d-none text-white">View In Cart</a>
            </div></span>
                        <span style="float: right;" data-toggle="collapse" id="product_detail_btn" data-parent="#accordion" href="#details"
                            onclick="productDetails()">&nbsp;<button style="box-shadow: none;background-color: #5e77a8;"
                                type="button" class="btn btn-danger">{{ trans('cruds.product.fields.details') }}</button></span>
                                <span style="float: right; display:none;" id="product_back_btn" data-toggle="collapse" data-parent="#accordion" href="#details"
                            onclick="productDetails()">&nbsp;<button style="box-shadow: none;background-color: #5e77a8;"
                                type="button" class="btn btn-danger">Back</button></span>
                        <h4 class="product_price text-justify"></h4><br>

                        <div class="row" id="product_list" style="background-color:#f3faff; display: inline-block; width: 100%;">
                                            <div id="product_details" class="product-details pb-1 pt-1">
                                                <div class="row flex-nowrap">
                                                    @foreach ($data as $key => $card)
                                                    @php
                                                    $product = $card->product;
                                                    $img_url = array();
                                                    $obj_url = array();
                                                    $obj_position = "";
                                                    $obj_position1 = "";
                                                    $obj_position2 = "";
                                                    if(count($product->photo) > 0){
                                                        for($i=0;$i < count($product->photo);$i++)
                                                            array_push($img_url,$product->photo[$i]->getUrl());
                                                    } else if(count($product->arobject)>0){
                                                        array_push($img_url,$product->arobject[0]->object->getUrl());
                                                    }
                                                    for($i=0;$i < count($product->arobject);$i++) {    
                                                        //array_push($obj_url,$product->arobject[$i]->object->getUrl());
                                                            if($product->arobject[$i]->position == "jwl-nck") {
                                                                $obj_position = "NECK";
                                                                $obj_position1 = "NECK";
                                                                $obj_url["NECK"] = $product->arobject[$i]->object->getUrl();
                                                            } else if($product->arobject[$i]->position == "jwl-ear-l"){
                                                                $obj_position = "EAR";
                                                                $obj_position2 = "EAR";
                                                                $obj_url["EAR"] = $product->arobject[$i]->object->getUrl();
                                                            } else if($product->arobject[$i]->position == "jwl-ear-r"){
                                                                $obj_position = "EAR";
                                                                $obj_position2 = "EAR";
                                                                $obj_url["EAR"] = $product->arobject[$i]->object->getUrl();
                                                            }
                                                            if(!empty($obj_position1) && !empty($obj_position2))
                                                                $obj_position = "SET";
                                                    }
                                                    $product_name= $product->name;
                                                    $selected_product['img_url'] = $img_url;
                                                    $selected_product['name'] = empty($product_name)?'No
                                                    Title':$product_name;
                                                    $product_json = array('id' => $product->id,
                                                    'name' =>$product->name ?? '&nbsp;',
                                                    'img_url' => $img_url,
                                                    'obj_url' => $obj_url,
                                                    'description' =>htmlentities($product->description),
                                                    'price' => empty($product->price)?"":"₹". ($product->price ?? ' '),
                                                    'sku' =>$product->sku ?? '',
                                                    'discount_price' =>$product->discount_price,
                                                    'url' =>$product->url);

                                                    $product_json = json_encode($product_json,true);
                                                    @endphp
                                                    @if(count($obj_url)>0)
                                                    @if(isset($_GET['sku']) && $_GET['sku'] == $product->id ||
                                                    $loop->index == 0)
                                                    <script>
                                                        var product_json_selected = '<?php echo $product_json;?>';
                                                        var product_id_selected = '<?php echo $product->id;?>';
                                                        var product_position_selected = '<?php echo $obj_position;?>';

                                                    </script>
                                                    @endif
                                                    <div class="col-sm-4">
                                                        <div class="card card-block" id="sku_{{$product->id}}" data-product="{{$product_json ?? ''}}">
                                                            <img data-toggle="collapse"
                                                                onclick="init_vto.selImg(this,'{{$obj_position}}','','sku_{{$product->id}}')"
                                                                src="{{$img_url[0] ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}'"
                                                                class="scimg tryonimg">
                                                            <span style="display: none">
                                                                <div class="fa fa-heart hearthidden"></div>
                                                                <div class="fa fa-heart hearts"></div>
                                                                <div>&nbsp;</div>
                                                                <div class="fa fa-share shares"></div>
                                                            </span>
                                                        </div>
                                                  </div>
                                                    @endif
                                                    @endforeach
                                                    <!-- Left and right controls -->
                                                    <a class="carousel-control-prev" href="#demo" data-slide="prev" style="top:auto;bottom:50px !important;">
                                                        <span class="carousel-control-prev-icon"
                                                            style="background-color: black;margin-left: -36px;"></span>
                                                    </a>
                                                    <a class="carousel-control-next" href="#demo" data-slide="next" style="top:auto;bottom:50px !important;">
                                                        <span class="carousel-control-next-icon"
                                                            style="background-color: black;margin-right: -36px;"></span>
                                                    </a>
                                                <!-- </div>
                                            </div> -->
                                        </div>
                                    </div>
                                    <div id="details" class="collapse w-100" data-parent="#accordion">
                                    <div class="p-1" style="overflow-y: scroll;height: 50vh;">
                                        
                                        <div id="carousel-thumb" class="carousel slide" data-interval="false" data-ride="carousel">

                                                <!-- Indicators -->
                                                <ul class="carousel-indicators mb-0" id="product_items_img">
                                                </ul>

                                                <!-- The slideshow -->
                                                <div class="carousel-inner" id="product_details_img">
                                                    
                                                </div>
                                                
                                            </div>
                                            

                                            <pre class="product_desc mt-1 bg-transparent text-left h5"></pre>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                <!-- </div> -->
            </div>
    </div>




    <!-- Trigger the modal with a button -->


    <!-- Modal -->
    <div class="modal fade p-0" id="MirrAR_preview" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" style="max-width: 100%;">

            <!-- Modal content-->
            <div class="modal-content rounded-0">
                <div class="modal-body" style="height: auto;">
                    <div>
                        <img id="preimg" src="" style="width: 100%;min-height: 350px;">
                        <!-- <div class="overlay"> -->
                            <a href="#" class="icon">
                                <img src="{{ asset('XR/themes/jewelar/img/close1.png')}}" data-dismiss="modal"
                                    style="width: 30px;">
                            </a>
                        <!-- </div> -->
                    </div>

                    <div class="bg-light" style="height: 60px;">
                        <a data-toggle="collapse" data-parent="#accordion" href="#share">
                            <img class="neck" src="{{ asset('XR/themes/jewelar/img/share.svg')}}"
                                style="float: left;"></a>

                        <a href="#" onclick="init_vto.savePicture()"><img class="neck"
                                src="{{ asset('XR/themes/jewelar/img/download.svg')}}" style="float: right;">
                        </a>
                    </div>
                    <div class="hero-container ml-1 mr-1">


                        <div class="panel-group" id="accordion" style=" background-color: #e4eef5;border-radius: 20px;">
                            <div class="panel panel-default">

                                <div id="share" class="panel-collapse collapse in"
                                    style=" background-color: rgb(255, 255, 255);border-top-right-radius: 20px;border-top-left-radius: 20px;">
                                    <div class="container" style=" padding-bottom: 26px;">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#share"><img
                                                src="{{ asset('XR/themes/jewelar/img/min.png')}}"></a>
                                        <div class="clesrfix"></div>
                                        <a class="btn btn-lg btn-social btn-facebook bstyle" href="#"
                                            onclick='share("fb","")'>
                                            <i class="fa fa-facebook fa-fw"></i> Share on Facebook&nbsp;
                                        </a><br>
                                        <a class="btn btn-lg btn-social btn-whatsapp bstyle" href="#"
                                            onclick='share("whatsapp","")'>
                                            <i class="fa fa-whatsapp fa-fw"></i> Share on whatsapp
                                        </a><br>
                                        <a class="btn btn-lg btn-social btn-twitter bstyle" href="#"
                                            onclick='share("twitter","")'>
                                            <i class="fa fa-twitter fa-fw"></i> Share on twitter&nbsp;&nbsp;&nbsp;
                                        </a><br>
                                        <a class="btn btn-lg btn-social btn-twitter bstyle" href="#"
                                            onclick='share("copy","")'>
                                            <i class="fa fa-copy fa-fw"></i> Copy Share Link&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        </a><br>
                                        <!-- <a class="btn btn-lg btn-social btn-pin" style="color:white;font-size:16px;padding:12px 47px!important;">
                              <i class="fa fa-pinterest fa-fw"></i> Share on pinterest
                            </a><br>        -->

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>

    <!--model slider-->
    <div class="modal fade" id="myModal1" role="dialog">
        <div class="modal-dialog" style="max-width: 100%;">

            <!-- Modal content-->
            <div class="modal-content">

                <div class="modal-body" style="height: 100vh;">

                    <div class="hero-container">



                        <hr>

                        <!--Carousel Wrapper-->
                        <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">

                            <!--Controls-->
                            <!-- <div class="controls-top">
                    <a class="btn-floating" href="#multi-item-example" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
                    <a class="btn-floating" href="#multi-item-example" data-slide="next"><i class="fa fa-chevron-right"></i></a>
                  </div> -->
                            <!--/.Controls-->

                            <!--Indicators-->
                            <DIV class="carousel-indicators" style="overflow: scroll;margin: 0px;">
                                <div class="row flex-row flex-nowrap">

                                    <!-- <div class="card card-block selected" >                            
                      <img data-target="#multi-item-example"  data-slide-to="0" class="active" src="{{ asset('XR/themes/jewelar/img/neck.png')}}" alt="img" style="WIDTH:75px" >
                       
                    </div>    
                    <div class="card card-block selected" >                            
                      <img data-target="#multi-item-example"  data-slide-to="1"  src="{{ asset('XR/themes/jewelar/img/neck.png')}}" alt="img" style="WIDTH:75px" >
                       
                    </div>
                    <div class="card card-block selected" >                            
                      <img data-target="#multi-item-example"  data-slide-to="2"  src="{{ asset('XR/themes/jewelar/img/neck.png')}}" alt="img" style="WIDTH:75px" >
                       
                    </div>  
                    <div class="card card-block selected" >                            
                      <img data-target="#multi-item-example"  data-slide-to="3"  src="{{ asset('XR/themes/jewelar/img/neck.png')}}" alt="img" style="WIDTH:75px" >
                       
                    </div>  
                    <div class="card card-block selected" >                            
                      <img data-target="#multi-item-example"  data-slide-to="4"  src="{{ asset('XR/themes/jewelar/img/neck.png')}}" alt="img" style="WIDTH:75px" >
                       
                    </div>      -->
                                </div>

                                <!-- <li><img data-target="#multi-item-example" data-slide-to="0" class="active" src="{{ asset('XR/themes/jewelar/img/neck.png')}}"  style="width: 75px;"></li><BR><BR> -->
                                <!-- <li> <img data-target="#multi-item-example" src="{{ asset('XR/themes/jewelar/img/neck.png')}}"  style="width:75px"></li>
                      <img  data-target="#multi-item-example" data-slide-to="2" src="{{ asset('XR/themes/jewelar/img/neck.png')}}"  style="width:75px">
                      <img data-target="#multi-item-example" data-slide-to="3" src="{{ asset('XR/themes/jewelar/img/neck.png')}}"  style="width:75px">
                      <img  data-target="#multi-item-example" data-slide-to="2" src="{{ asset('XR/themes/jewelar/img/neck.png')}}"  style="width:75px">
                      <img data-target="#multi-item-example" data-slide-to="3" src="{{ asset('XR/themes/jewelar/img/neck.png')}}"  style="width:75px"> -->


                            </DIV>
                            <!--/.Indicators-->

                            <!--Slides-->
                            <div class="carousel-inner" role="listbox" style="height: 100vh;padding-top: 100px;">

                                <!--First slide-->
                                <div class="carousel-item active">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="card mb-2 " style="border:unset">
                                                <!-- <img class="card-img-top" src="{{ asset('XR/themes/jewelar/img/neck.png')}}"
                              alt="Card image cap"> -->
                                                <!-- <div class="card-body">
                              <h4 class="card-title">Card title</h4>
                              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                                card's content.</p>
                              <a class="btn btn-primary">Button</a>
                            </div> -->
                                            </div>
                                        </div>

                                        <div class="col-md-4 clearfix d-none d-md-block">
                                            <div class="card mb-2" style="border:unset">
                                                <img class="card-img-top"
                                                    src="{{ asset('XR/themes/jewelar/img/neck.png')}}"
                                                    alt="Card image cap">

                                            </div>
                                        </div>

                                        <div class="col-md-4 clearfix d-none d-md-block">
                                            <div class="card mb-2" style="border:unset">
                                                <img class="card-img-top"
                                                    src="{{ asset('XR/themes/jewelar/img/neck.png')}}"
                                                    alt="Card image cap">

                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!--/.First slide-->

                                <!--Second slide-->
                                <div class="carousel-item">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="card mb-2" style="border:unset">
                                                <img class="card-img-top"
                                                    src="{{ asset('XR/themes/jewelar/img/neck.png')}}"
                                                    alt="Card image cap">

                                            </div>
                                        </div>



                                    </div>

                                </div>
                                <!--/.Second slide-->

                                <!--Third slide-->
                                <div class="carousel-item">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="card mb-2" style="border:unset">
                                                <img class="card-img-top"
                                                    src="{{ asset('XR/themes/jewelar/img/neck.png')}}"
                                                    alt="Card image cap">
                                            </div>
                                        </div>


                                    </div>

                                </div>
                                <div class="carousel-item">

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="card mb-2" style="border:unset">
                                                <!-- <img class="card-img-top" src="{{ asset('XR/themes/jewelar/img/neck.png')}}" 
                              alt="Card image cap"> -->

                                            </div>
                                        </div>
                                    </div>
                                    <!--/.Third slide-->

                                </div>
                                <!--/.Slides-->
                                <div class="overlay">
                                    <a href="#" class="icon" title="User Profile" style="color: black;">
                                        <img src="{{ asset('XR/themes/jewelar/img/close.png')}}" data-dismiss="modal"
                                            style="width: 30px;">
                                    </a>
                                </div>
                            </div>
                            <!--/.Carousel Wrapper-->


                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    
</section><!-- End Hero Section -->
<div id="camError" class="text-center p-2 h-100" style="display:none"></div>

<div class="landscape-mode" style="text-align:center">
		<img src="{{ asset('XR/assets/images/rotate.png') }}"></img>
		<br />
		<p style="font-size: 25px;">Sorry landscape mode is not suitable for this application.!!
			<br />
			Kindly turn your phone to continue.</p>

	</div>
    <div class="position-absolute h-100 w-100 mypanel-center text-center bg-light startTryon" style="display: none">
        <div class="position-absolute mypanel-center h-100 p-2">
        <h4>GET READY!</h4>
        <div class="startTryon-logo mr-2">
        @if(!empty($meta_data['splash']))    
        @if($meta_data['splash']['brand_type'] == 2 && $meta_data['splash']['brand_name'] != "")
            <h1 style="color: {{ $meta_data['splash']['brand_color'] ?? '' }}">{{$meta_data['splash']['brand_name']}}</h1>
        @else
            <img src="{{ $campaign->getFirstMediaUrl('splashlogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                onerror="this.onerror=null;this.src='{{asset('XR/app-assets/images/logo/logo.png')}}';" alt="Logo">
        @endif
        @else
            <img src="{{ $campaign->getFirstMediaUrl('splashlogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                onerror="this.onerror=null;this.src='{{asset('XR/app-assets/images/logo/logo.png')}}';" alt="Logo">
        @endif
        <p class="mt-1"><input type="checkbox" id="iagree"> I agree <br>
        to the processing of my image by MirrAR as set out in the <a href="{{route('landing.privacyPolicy')}}" target="blank">privacy policy.</a></p>
        <button type="button" class="btn btn-white btn-lg mt-0" id="liveCamera" style="width: 90px;font-size:1em;" disabled ><i class="feather icon-camera"></i> Start</button><br><br>
        {{-- <button type="button" class="btn btn-white btn-lg mt-0" id="photoMode" style="width: 180px;font-size:1em;" disabled><i class="feather icon-upload"></i> upload photo</button> --}}
        
    </div>
        </div>

    @endsection
@section('scripts')
@parent

<script src="{{ asset('XR/themes/jewelar/js/main.js') }}"></script>
<script src="{{asset('XR/assets/js/cart.js')}}"></script>

<script src="{{ asset('XR/core/finit.min.js') }}"></script>
<script src="{{ asset('XR/core/fbackend.min.js') }}"></script>
<script src="{{ asset('XR/core/fdetector.min.js') }}"></script>
<script src="{{ asset('XR/core/init.min.js') }}"></script>

<script>
// $('img').on("error", function() {
//         $(this).attr('src', '{{asset('XR/assets/images/placeholder.png')}}');
//         $(this).css('width', '95%');
//         $(this).css('max-height', '100%');
//     });
    

    var mycallback = function (img) {
        var selectedProducts = {
            "photo": img,
            "cid": "{{$campaign->id}}",
            "pid": product_id_selected
        };
        $.ajax({
            type: "POST",
            url: "{{ route("captures.store") }}",
            data: selectedProducts,
            success: function (data) {
                share_url = data;
                $('#share').collapse('show');
            }
        });
    }
   
</script>
@endsection
