<!DOCTYPE html>
<html class="loaded" lang="en" data-textdirection="ltr">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $meta_data['splash']['brand_name'] ?? trans('panel.site_title') }}</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.min.css"
		integrity="sha512-k78e1fbYs09TQTqG79SpJdV4yXq8dX6ocfP0bzQHReQSbEghnS6AQHE2BbZKns962YaqgQL16l7PkiiAHZYvXQ=="
		crossorigin="anonymous" referrerpolicy="no-referrer" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/themes/base/jquery-ui.min.css"
		integrity="sha512-okE4owXD0kfXzgVXBzCDIiSSlpXn3tJbNodngsTnIYPJWjuYhtJ+qMoc0+WUwLHeOwns0wm57Ka903FqQKM1sA=="
		crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/extensions/toastr.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/themes/tryonv4_03/css/MirrAR-style-bigscreen.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/themes/tryonv4_03/css/MirrAR-Loader.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />  <script async src="https://www.googletagmanager.com/gtag/js?id=G-65PERJ2GMK"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-65PERJ2GMK');
    </script>
      @if(!empty($meta_data['analytic_settings']['google_analytics_id']))
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-{{$meta_data['analytic_settings']['google_analytics_id']}}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '{{$meta_data['analytic_settings']['google_analytics_id']}}');
        </script>
      @endif
    @if(!empty($meta_data['custom_script_settings']['custom_script_header']))
    {!! $meta_data['custom_script_settings']['custom_script_header'] !!}
    @endif

    @php
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')   
         $url = "https://";   
    else  
         $url = "http://";   
    // Append the host(domain name, ip) to the URL.   
    $url.= $_SERVER['HTTP_HOST'];   
    
    // Append the requested resource location to the URL   
    $url.= $_SERVER['REQUEST_URI'];   
    $components = parse_url($url);
    parse_str($components['query'], $results);
    
    $path = explode('/',$components['path']);
    \Debugbar::disable();
  
@endphp
<style>

:root {
  @if(!empty($meta_data['tryon_settings']['tryon_primary_color']))
    --tryon_primary_color: {{ $meta_data['tryon_settings']['tryon_primary_color'] }};
  @else
    --tryon_primary_color: #633194;
  @endif
  @if(!empty($meta_data['tryon_settings']['tryon_secondary_color']))
    --tryon_secondary_color: {{ $meta_data['tryon_settings']['tryon_secondary_color'] }}; 
  @else
  --tryon_secondary_color: #ead4ff;
  @endif
  @if(!empty($meta_data['tryon_settings']['tryon_tertiary_color']))
    --tryon_tertiary_color: {{ $meta_data['tryon_settings']['tryon_tertiary_color'] }};
  @else
  --tryon_tertiary_color: #ff0087;
  @endif
}

div#MirrAR-modal {
    z-index: inherit;
}
.share_btn{
  display: none;
  margin: 0;
  position: absolute;
  top:90%;
  left: 30%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
  z-index: 9;
}

.btn-facebook {
  color: #fff;
  background-color: #3b5998;
  border-color: rgba(0, 0, 0, 0.2);
}

.btn-lg {
  margin-top: 12px;
}

.btn-whatsapp {
  color: #fff;
  background-color: #4CAF50;
  border-color: rgba(0, 0, 0, 0.2);
}

.btn-twitter {
  color: #fff;
  background-color: rgb(29, 161, 242);
  border-color: rgba(0, 0, 0, 0.2);
}

.btn-copyshare{
  color: #fff;
  background-color: rgb(29, 161, 242);
  border-color: rgba(0, 0, 0, 0.2);
}

.btn-pin {
  color: #fff;
  background-color: red;
  border-color: rgba(0, 0, 0, 0.2);
}

.btn-social {
  width: 255px;
  position: relative;
  padding-left: 44px;
  text-align: left;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}

.btn-social:hover {
  color: #eee;
}

.bstyle {
  color: white !important;
  font-size: 16px;
  padding: 12px 47px !important;
}

.modal-myshare {
    min-height: calc(100vh - 60px);
    display: flex;
    flex-direction: column;
    justify-content: center;
    overflow: auto;
}
@media(max-width: 768px) {
  .modal-myshare {
    min-height: calc(100vh - 20px);
  }
}

@media (max-width: 576px) {
  .shareopt {
          margin: 0;
          position: absolute;
          top: 70%;
          margin-top: 5px;
          left: 15%;
          max-width : 370px;
          background-color: white;
          -ms-transform: translateY(-50%);
          transform: translateY(-50%);
          z-index: 9;
  }
}
@media (min-width: 576px){
      .vertical-center {
          margin: 0;
          position: absolute;
          top: 75%;
          margin-top: 5px;
          left: 25%;
          max-width : 370px;
          background-color: white;
          -ms-transform: translateY(-50%);
          transform: translateY(-50%);
          z-index: 9;
        }
}
    </style>
</head>

<body>
   <!-- Google Tag Manager (noscript) -->
   <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N67BLVX" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @if(!empty($meta_data['analytic_settings']['google_analytics_gtm_id']))
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{$meta_data['analytic_settings']['google_analytics_gtm_id']}}" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    @endif
    @php 
      $photo = $initialProduct->product->photo;
      $img_url = array();
      $img_thumb = array();
      $obj_neck = array();
      $obj_ear = array();
      $obj_ring = array();
      $obj_watch = array();
        foreach($photo as $key => $value){
          $img = $initialProduct->product->photo[$key]->getFullUrl();
          array_push($img_url, $img);

          $thumb = $initialProduct->product->photo[$key]->getFullUrl('thumb');
          array_push($img_thumb, $thumb);

        }

        $arobject = $initialProduct->arobject;
        foreach($arobject as $key => $value) {    
            if($arobject[$key]->position == "jwl-nck") {
                $neck = $arobject[$key]->object->getFullUrl();
            } else if($arobject[$key]->position == "jwl-ear-l"){
                $ear = $arobject[$key]->object->getFullUrl();
            } else if($arobject[$key]->position == "jwl-ear-r"){                
                $ear = $arobject[$key]->object->getFullUrl();
            } else if($arobject[$key]->position == "ring"){                
                $ring = $arobject[$key]->object->getFullUrl();
            } else if($arobject[$key]->position == "watch"){                
                $watch = $arobject[$key]->object->getFullUrl();
            } else if($arobject[$key]->position == "apparel"){                
                $apparel = $arobject[$key]->object->getFullUrl();
            } 
        }
        
    @endphp
	<!-- Mobile and Tablet LandScape View  -->
	<div class="landscape-mode" style="text-align: center">
		<img src="{{ asset('XR/themes/tryonv4_03/img/rotate.png') }}" />
		<br />
		<p style="font-size: 25px; color: aliceblue">
			Sorry landscape mode is not suitable for this application.!!
			<br />
			Kindly turn your phone to continue.
		</p>
	</div>
	<!-- Desktop View -->
	<div class="desktop">
		<div class="full-screen-camera-view">
			<div class="preloader-wrap">
				<div class="loading">
					<img src="{{ asset('XR/themes/tryonv4_03/img/logo_mixed.svg') }}" />
					<span><i></i><i></i><i></i></span>
				</div>
			</div>
			<div id="VTOtryonDesktop" class="vto" style="left: 0; right: 0; margin: auto">

			</div>
		</div>
	</div>
	<div class="vto vto-loading" style="background-color: black; width: 100%; height: 100%;">
		<div class="text-white text-center"
			style="position: absolute; left: 0; right: 0; margin: auto; text-align: center; top: 50%; transform: translateY(-50%);">
			<img src="{{ asset('XR/themes/tryonv4_03/img/logo_mixed.svg') }}" /><br>
			Loading...<br>
			Please Wait
		</div>
	</div>
	<div class="header-area">
		<button class="btn btn-primary btn-icon-only rounded-circle float-right" id="modalProductBtn" style="display: none"
			onclick="showHideProductList(this)">
			<svg width="50" height="50" viewBox="0 0 70 70" fill="none" xmlns="http://www.w3.org/2000/svg">
        <circle cx="35" cy="35" r="35" style="fill: var(--tryon_primary_color);"/>
        <path d="M30.8333 16.25H18.3333C17.7808 16.25 17.2509 16.4695 16.8602 16.8602C16.4695 17.2509 16.25 17.7808 16.25 18.3333V30.8333C16.25 31.3859 16.4695 31.9158 16.8602 32.3065C17.2509 32.6972 17.7808 32.9167 18.3333 32.9167H30.8333C31.3859 32.9167 31.9158 32.6972 32.3065 32.3065C32.6972 31.9158 32.9167 31.3859 32.9167 30.8333V18.3333C32.9167 17.7808 32.6972 17.2509 32.3065 16.8602C31.9158 16.4695 31.3859 16.25 30.8333 16.25ZM28.75 28.75H20.4167V20.4167H28.75V28.75ZM51.6667 16.25H39.1667C38.6141 16.25 38.0842 16.4695 37.6935 16.8602C37.3028 17.2509 37.0833 17.7808 37.0833 18.3333V30.8333C37.0833 31.3859 37.3028 31.9158 37.6935 32.3065C38.0842 32.6972 38.6141 32.9167 39.1667 32.9167H51.6667C52.2192 32.9167 52.7491 32.6972 53.1398 32.3065C53.5305 31.9158 53.75 31.3859 53.75 30.8333V18.3333C53.75 17.7808 53.5305 17.2509 53.1398 16.8602C52.7491 16.4695 52.2192 16.25 51.6667 16.25ZM49.5833 28.75H41.25V20.4167H49.5833V28.75ZM30.8333 37.0833H18.3333C17.7808 37.0833 17.2509 37.3028 16.8602 37.6935C16.4695 38.0842 16.25 38.6141 16.25 39.1667V51.6667C16.25 52.2192 16.4695 52.7491 16.8602 53.1398C17.2509 53.5305 17.7808 53.75 18.3333 53.75H30.8333C31.3859 53.75 31.9158 53.5305 32.3065 53.1398C32.6972 52.7491 32.9167 52.2192 32.9167 51.6667V39.1667C32.9167 38.6141 32.6972 38.0842 32.3065 37.6935C31.9158 37.3028 31.3859 37.0833 30.8333 37.0833ZM28.75 49.5833H20.4167V41.25H28.75V49.5833ZM45.4167 37.0833C40.8208 37.0833 37.0833 40.8208 37.0833 45.4167C37.0833 50.0125 40.8208 53.75 45.4167 53.75C50.0125 53.75 53.75 50.0125 53.75 45.4167C53.75 40.8208 50.0125 37.0833 45.4167 37.0833ZM45.4167 49.5833C43.1187 49.5833 41.25 47.7146 41.25 45.4167C41.25 43.1187 43.1187 41.25 45.4167 41.25C47.7146 41.25 49.5833 43.1187 49.5833 45.4167C49.5833 47.7146 47.7146 49.5833 45.4167 49.5833Z" fill="white"/>
      </svg>
		</button>
	</div>
	</div>
	<!-- <div> -->

	<div id="model-view-camera-only" style="display: none">
		<div class="card camera-only-modal-card">
			<div class="card-header camera-modal-header bg-black">
				<div id="main">
					<div id="VTOtryonDesktopModalCamOnly" class="vto m-2"
						style="left: 0; right: 0; margin: auto; height: calc(90%); border-radius: 10px; width: calc(95% - 5px)">
					</div>
					<div class="vto vto-loading"
						style="background-color: black; width: calc(95% - 5px); height: 100%; border-top-left-radius: 1.5rem;	border-bottom-left-radius: 1.5rem;">
						<div class="text-white text-center"
							style="position: absolute; left: 0; right: 0; margin: auto; text-align: center; top: 50%; transform: translateY(-50%);">
							<img src="{{ asset('XR/themes/tryonv4_03/img/logo_mixed.svg') }}" /><br>
							Loading...<br>
							Please Wait
						</div>
					</div>
				</div>
			</div>

			<div
				style="background-color: black; position: absolute; top: 8px; left: 8px; bottom: 5px; right: 5px; width: calc(100% - 55px); border-radius: 10px; display: none;z-index:2;"
				id="preimgscrDesktopModalcamonly">
				<img class="vto" src="#" id="preimgDesktopModalcamonly"
					style="width: 100%; height: 100%; border-radius: 10px" />
				<div class="footer-area text-center">
					<button class="btn btn-lg btn-primary float-left" onclick="closePreimgscr()">
						<img src="{{ asset('XR/themes/tryonv4_03/img/arrow-left.svg') }}" width="30" /> Go back
					</button>
					<button class="btn btn-lg btn-primary" data-toggle="modal" data-target="#myShare">
						<img src="{{ asset('XR/themes/tryonv4_03/img/share.svg') }}" width="30" /> Share
					</button>
					<button class="btn btn-lg btn-primary float-right" onclick="downloadTakePicture()">
						<img src="{{ asset('XR/themes/tryonv4_03/img/download.svg') }}" width="30" /> Download
					</button>
				</div>
			</div>

			<!-- Camera Button Card Footer  --p>
			<div class="card-footer camera-modal-footer bg-white d-flex align-items-center justify-content-center">
				<!-- Close Product Details card Button -->
				<button class="btn btn-primary btn-icon-only rounded-circle" onclick="takePicture()">
					<img src="{{ asset('XR/themes/tryonv4_03/img/Camera_Take_Photo_bigger.svg') }}" width="45" />
				</button>
			</div>
		</div>
	</div>

	<div class="preloader-wrap">
		<div class="loading">
			<img src="{{ asset('XR/themes/tryonv4_03/img/logo_mixed.svg') }}" />
			<span><i></i><i></i><i></i></span>
		</div>
	</div>
	<div class="wrapper-product-details-modal" style="z-index: 1">
		<!-- Product List Modal -->
		<div class="card product-modal" id="modalProduct">
			<div class="card-header bg-white product-modal-header">
				<div class="menu-wrapper d-flex p-2">
					<!-- Top Navbar -- List of Categories -->
					<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist"></div>

					<!-- Categories Navbar Left - Right Paddle -->
					<div class="paddles">
						<button class="btn btn-icon-only left-paddle paddle">
            <svg width="25" height="25" viewBox="0 0 107 113" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g opacity="0.6">
                  <ellipse cx="53.3525" cy="56.5165" rx="53.0732" ry="56.295" transform="rotate(-180 53.3525 56.5165)" style="fill: var(--tryon_secondary_color)" />
                  <path d="M61.4766 40.2676L45.2275 56.5167L61.4766 72.7658" stroke-width="3.06" stroke-linecap="round" stroke-linejoin="round" style="stroke: var(--tryon_primary_color);" />
                </g>
              </svg>
						</button>
						<button class="btn btn-icon-only right-paddle paddle">
              <svg width="25" height="25" viewBox="0 0 114 113" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g opacity="0.6">
                  <ellipse rx="56.2954" ry="56.295" transform="matrix(1 0 0 -1 57.1001 56.5165)" style="fill: var(--tryon_secondary_color)"/>
                  <path d="M48.9766 41.1978L65.2255 56.5167L48.9766 71.8356" stroke-width="3.06" stroke-linecap="round" stroke-linejoin="round" style="stroke: var(--tryon_primary_color)" />
                </g>
              </svg>
						</button>
					</div>
				</div>

				<!-- Close Button  -->
				<button class="btn btn-primary btn-icon-only rounded-circle float-right product-modal-close-btn"
					onclick="showHideProductList()">
					<svg width="25" height="25" viewBox="0 0 56 56" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="28" cy="28" r="28" style="fill: var(--tryon_primary_color)"/>
            <path d="M39 18L19 38" stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
            <path d="M19 18L39 38" stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
          </svg>
				</button>
			</div>

      <div class="d-flex justify-content-center my-3" style="width: 100%">
        <form class="d-flex" id="search-form" style="width:95%" >
          <input class="form-control" id="search" placeholder="Search" aria-label="Search" style="border-top-right-radius: 0;border-bottom-right-radius: 0;font-size: 12px" />
          <button class="btn btn-primary" style="margin-left: 0px;padding: 5px;border-top-left-radius: 0; border-bottom-left-radius: 0;font-size: 12px;"><i class="fas fa-search" style="margin-right: 5px;"></i> Search</button>
        </form>
      </div>

			<!-- Content of Product List for Eeach Category -->
			<div class="card-body product-modal-body scrollbar">
				<div class="loader-wrapper" style="display: none;">
					<div class="display-1 loading-text">Loading...</div>
				</div>
				<div class="tab-content clearfix" id="tab-content">
					
				</div>
			</div>

			<!-- Product List Card Footer -->
			<div class="card-footer product-modal-footer">
            <div class="w-100 m-2">
							<h6 class="text-center d-flex justify-content-center align-items-center" style="font-size: 14px;">Powered by <a href="https://mirrar.co" class="ml-2 pink"><img src="{{ asset('XR/themes/tryonv4_03/img/logo_color.svg') }}" style="width:100px;"/></a></h6>
						</div>
				<!-- <div class="menu-wrapper p-2">
					<div class="nav nav-tabs nav-fill d-flex mr-0" role="tablist">
						<div class="w-100 mt-2">
							<h6 class="text-right">Powered by <a href="https://mirrar.co" class="pink"><img src="{{ asset('XR/themes/tryonv4_03/img/logo_color.svg') }}" style="width:100px;"/></a></h6>
						</div>
						<a class="nav-item nav-link active d-none" id="nav-home-tab" data-toggle="tab" href="#nav-necklace"
							role="tab" aria-controls="" aria-selected="true"><img src="{{ asset('XR/themes/tryonv4_03/img/camera.svg') }}"
								class="footer-menu-icon" />Camera</a>
						<a class="nav-item nav-link disabled d-none" id="nav-profile-tab" data-toggle="tab" href="#nav-models"
							role="tab" aria-controls="" aria-selected="false"><img src="{{ asset('XR/themes/tryonv4_03/img/image_disable.svg') }}"
								class="footer-menu-icon" />Model</a>
					</div>
				</div> -->
			</div>

			<!-- Product Detail Side View -->
			<div class="card product-modal-detail-side" id="productDetailSideView" style="z-index: 1">
				<div class="card-header product-modal-header-side">
					Product Details
					<button class="btn btn-primary btn-icon-only rounded-circle float-left" style="top: 0; right: 0"
						onclick="toggleDetailsSideView()" id="product-modal-detail-side-back-btn">
						<img src="{{ asset('XR/themes/tryonv4_03/img/arrow-left.svg') }}" />
					</button>
				</div>
				<div class="card-body product-modal-detail-side-body">
					<div class="row h-100">
						<div class="col-12">
							<div class="">
								<div id="custCarousel-side" class="carousel slide custCarousel" data-ride="carousel"
									data-interval="false" align="center">
									<!-- slides -->
									<div class="carousel-inner product-details-carousel-image-group-side"></div>
									<!-- Left right -->
									<a class="carousel-control-prev" href="#custCarousel-side" data-slide="prev">
										<span class="carousel-control-prev-icon"></span>
									</a>
									<a class="carousel-control-next" href="#custCarousel-side" data-slide="next">
										<span class="carousel-control-next-icon"></span>
									</a>
									<!-- Thumbnails -->
									<ol class="carousel-indicators list-inline product-details-side"></ol>
								</div>
							</div>
						</div>
						<!-- Product details -->
						<div class="col-12">
							<div class="product-modal-detail-side-add-to-cart-btn">
								<button class="btn btn-md add-to-cart-btn" onclick="addtocart(this,event)">Add to cart</button>
								<button class="btn btn-md remove-from-cart-btn" style="display: none;"
									onclick="removefromcart(this,event)">Remove from cart</button>
							</div>
							<div class="product-modal-detail scrollbar" style="overflow-y: auto">
								<div>
									<h3 class="display-1 product-modal-detail-title-side">Product Title</h3>
								</div>
								<div>
									<h4 class="display-4 product-modal-detail-price-side">&#x20b9; 99999</h4>
								</div>
								<div class="lead product-modal-detail-product-details-side scrollbar">Product Details</div>
								<div id="product-detail"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-footer product-modal-footer">
					<div class="menu-wrapper p-2">
						<div class="nav nav-tabs nav-fill d-flex mr-0" role="tablist">
							<div class="w-100 mt-2">
								<h6 class="text-right">Powered by <a href="https://mirrar.co" class="pink">MirrAR</a></h6>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- Modal Tryon Camera -->
		<div class="card camera-modal-card">
			<div class="card-header camera-modal-header bg-white">
				<div id="main">
					<div id="VTOtryonDesktopModal" class="vto m-2"
						style="left: 0; right: 0; margin: auto; height: calc(90%); border-radius: 10px; width: calc(95% - 5px)">
						</div>
						<div class="vto vto-loading"
							style="background-color: black; width: calc(95% - 5px); height: 100%; border-top-left-radius: 1.5rem;	border-bottom-left-radius: 1.5rem;">
							<div class="text-white text-center"
								style="position: absolute; left: 0; right: 0; margin: auto; text-align: center; top: 50%; transform: translateY(-50%);">
								<img src="{{ asset('XR/themes/tryonv4_03/img/logo_mixed.svg') }}" /><br>
								Loading...<br>
								Please Wait
							</div>
						</div>
					</div>
				</div>

				<div style="background-color: black; position: absolute; top: 8px; left: 8px; bottom: 5px; right: 5px; width: calc(100% - 55px); border-radius: 10px; display: none; z-index: 2;" id="preimgscrDesktopModal">
					<img class="vto" id="preimgDesktopModal" src="#" style="width: 100%; height: 100%; border-radius: 10px" />
					<div class="footer-area text-center">
						<button class="btn btn-lg btn-primary float-left" onclick="closePreimgscr()">
							<img src="{{ asset('XR/themes/tryonv4_03/img/arrow-left.svg') }}" width="30" /> Go back
						</button>
						<button class="btn btn-lg btn-primary" data-toggle="modal" data-target="#myShare">
							<img src="{{ asset('XR/themes/tryonv4_03/img/share.svg') }}" width="30" /> Share
						</button>
						<button class="btn btn-lg btn-primary float-right" onclick="downloadTakePicture()">
							<img src="{{ asset('XR/themes/tryonv4_03/img/download.svg') }}" width="30" /> Download
						</button>
					</div>
				</div>

				<!-- Camera Button Card Footer  -->
				<div class="card-footer camera-modal-footer bg-white d-flex align-items-center justify-content-center">
					<!-- Close Product Details card Button -->
					<button class="btn btn-primary btn-icon-only rounded-circle" onclick="takePicture()">
						<img src="{{ asset('XR/themes/tryonv4_03/img/Camera_Take_Photo_bigger.svg') }}" width="45" />
					</button>
				</div>
			</div>
		</div>
		<!-- </div> -->

		<!-- Product Details Card -->
		<div class="card product-modal-detail-card" id="modalProductDetail" style="z-index: 1">
			<div class="card-header product-modal-detail-header bg-white">
				<div class="menu-wrapper"></div>
			</div>
			<div class="card-body">
				<div class="row h-100">
					<div class="col-6">
						<div class="product-modal-detail-imgs w-100">
							<div id="custCarousel" class="carousel slide custCarousel" data-ride="carousel" data-interval="false"
								align="center">
								<!-- slides -->
								<div class="carousel-inner product-details-carousel-image-group"></div>
								<!-- Left right -->
								<a class="carousel-control-prev" href="#custCarousel" data-slide="prev">
									<span class="carousel-control-prev-icon"></span>
								</a>
								<a class="carousel-control-next" href="#custCarousel" data-slide="next">
									<span class="carousel-control-next-icon"></span>
								</a>
								<!-- Thumbnails -->
								<ol class="carousel-indicators list-inline product-details"></ol>
							</div>
						</div>
					</div>
					<!-- Product details -->
					<div class="col-6">
						<div class="product-modal-detail scrollbar">
							<div>
								<h3 class="display-1 product-modal-detail-title">Product Title</h3>
							</div>

							<div class="ml-2 row d-flex align-items-center">
								<h4 class="display-4 product-modal-detail-price">&#x20b9; 99999</h4>
								<div class="d-flex ml-4">
									<button class="btn btn-md add-to-cart-btn" onclick="addtocart(this,event)">Add to cart</button>
									<button class="btn btn-md remove-from-cart-btn" style="display: none;"
										onclick="removefromcart(this,event)">Remove from cart</button>
								</div>
							</div>

							<div class="lead product-modal-detail-product-details scrollbar">Product Details</div>
							<div id="product-detail"></div>
						</div>
					</div>
				</div>
			</div>

			<!-- Product Details Card Footer  -->
			<div class="card-footer product-modal-detail-footer bg-white d-flex align-items-center justify-content-center">
				<!-- Close Product Details card Button -->
				<button class="btn btn-lg btn-primary float-center product-modal-detail-close-btn"
					onclick="closeproductDetail()">
					<div class="d-flex align-items-center justify-content-center display-4 product-modal-detail-close-btn-text">
						Close</div>
				</button>
			</div>
		</div>
		<div style="background-color: black; position: absolute; top: 0; bottom: 0; width: 100%; display: none; z-index: 2"
			class="webcam" id="preimgscrDesktop">
			<img class="vto" src="#" id="preimgDesktop" style="width: 100%; top: 0; bottom: 0; margin: auto" />
			<div class="footer-area text-center ">
				<button class="btn btn-lg btn-primary float-left" onclick="closePreimgscr()">
					<img src="{{ asset('XR/themes/tryonv4_03/img/arrow-left.svg') }}" width="30" /> Go back
				</button>
				<button class="btn btn-lg btn-primary" data-toggle="modal" data-target="#myShare">
					<img src="{{ asset('XR/themes/tryonv4_03/img/share.svg') }}" width="30" /> Share
				</button>
				<button class="btn btn-lg btn-primary float-right" onclick="downloadTakePicture()">
					<img src="{{ asset('XR/themes/tryonv4_03/img/download.svg') }}" width="30" /> Download
				</button>
			</div>
		</div>
	</div>

	<!-- Mobile View -->
	<div class="mobile">
		<div class="preloader-wrap">
			<div class="loading">
				<img src="{{ asset('XR/themes/tryonv4_03/img/logo_mixed.svg') }}" />
				<span><i></i><i></i><i></i></span>
			</div>
		</div>
		<div class="card camera-mobile">
      <a style="left: 20px; top: 5%; transform: translateY(-70%); text-decoration: none; z-index: 2; position: absolute;" href="{{ url()->previous() }}" >
        <img class="mobile-back-btn d-none" src="{{ asset('XR/themes/tryonv4_03/img/arrow-left.svg') }}" style="height:20px" >
      </a>
      <div class="portrait-top-header" style="display: none; height: 10%; width: 100%;background-color: #4A2849;">
        <div class="d-flex align-items-center justify-content-center" style="height: 100%;">
          <div>
            <img class="mobile-store-logo" src="{{ !empty($userSettings->getFirstMedia('storetryonlogo'))? $userSettings->getFirstMedia('storetryonlogo')->getFullUrl() : '' }}" style="height: 40px" >
          </div>
          <div></div>
        </div>
      </div>
			<div id="VTOtryonMobile" class="vto" style="border: 0; padding: 0; height: 100%; width: 100%"></div>
			<div class="vto vto-loading" style="background-color: black; width: 100%; height: 100%;">
				<div class="text-white text-center"
					style="position: absolute; left: 0; right: 0; margin: auto; text-align: center; top: 50%; transform: translateY(-50%);">
					<img src="{{ asset('XR/themes/tryonv4_03/img/logo_mixed.svg') }}" /><br>
					Loading...<br>
					Please Wait
				</div>
			</div>
			<div
				style="background-color: black; position: absolute; top: 0; bottom: 0; width: 100%; display: none; z-index: 2"
				class="webcam" id="preimgscrMobile">
				<img class="vto" src="#" id="preimgMobile" style="width: 100%; top: 0; bottom: 0; margin: auto" />
				<div class="footer-area text-center">
					<button class="btn btn-lg btn-primary float-left" onclick="closePreimgscr()">
						<img src="{{ asset('XR/themes/tryonv4_03/img/arrow-left.svg') }}" width="30" /> Go back
					</button>
					<button class="btn btn-lg btn-primary float-right" onclick="downloadTakePicture()">
						<img src="{{ asset('XR/themes/tryonv4_03/img/download.svg') }}" width="30" /> Download
					</button>
					<button class="btn btn-lg btn-primary mt-2" data-toggle="modal" data-target="#myShare">
						<img src="{{ asset('XR/themes/tryonv4_03/img/share.svg') }}" width="30" /> Share
					</button>
				</div>
			</div>

			<!-- Camera Button Card Footer  -->
			<div class="align-items-center justify-content-center h-100" style="z-index: 1">
				<!-- Close Product Details card Button -->
				<div class="bottom-sheet-scroll">
          <div class="position-absolute d-none" style="width: 100%;top: 20%;transform: translateY(-20%);">
            <svg width="25" height="25" viewBox="0 0 71 70" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M70.3242 50C70.3242 61.0457 61.3699 70 50.3242 70L0.324219 70L0.324213 6.11959e-06L50.3242 1.74846e-06C61.3699 7.8281e-07 70.3242 8.95431 70.3242 20L70.3242 50Z" fill="#633194"/>
              <path d="M35.4082 43.4165L45.8249 32.9998L35.4082 22.5832" stroke="white" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"/>
              <path d="M20.8242 43.4165L31.2409 32.9998L20.8242 22.5832" stroke="white" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
            <svg width="25" height="25" viewBox="0 0 56 56" fill="none" xmlns="http://www.w3.org/2000/svg">
              <circle cx="28" cy="28" r="28" style="fill: var(--tryon_primary_color)"/>
              <path d="M39 18L19 38" stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
              <path d="M19 18L39 38" stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
          </div>
          <div class="position-absolute d-none justify-content-between prev-next-paddle" style="width: 100%;top: 35%;transform: translateY(-35%);">
            <svg viewBox="0 0 107 113" fill="none" xmlns="http://www.w3.org/2000/svg">
              <g opacity="0.6">
                <ellipse cx="53.3525" cy="56.5165" rx="53.0732" ry="56.295" transform="rotate(-180 53.3525 56.5165)" style="fill: var(--tryon_secondary_color)" />
                <path d="M61.4766 40.2676L45.2275 56.5167L61.4766 72.7658" stroke-width="3.06" stroke-linecap="round" stroke-linejoin="round" style="stroke: var(--tryon_primary_color)" />
              </g>
            </svg>
            <svg viewBox="0 0 114 113" fill="none" xmlns="http://www.w3.org/2000/svg">
              <g opacity="0.6">
                <ellipse rx="56.2954" ry="56.295" transform="matrix(1 0 0 -1 57.1001 56.5165)" style="fill: var(--tryon_secondary_color);"/>
                <path d="M48.9766 41.1978L65.2255 56.5167L48.9766 71.8356" stroke-width="3.06" stroke-linecap="round" stroke-linejoin="round" style="stroke: var(--tryon_primary_color);" />
              </g>
            </svg>
          </div>
          
					<div class="bottom-sheet-wrapper">
            {{-- <div onclick="closeBottomSheet()" id="tap-to-close-container"></div> --}}
						<div class="camera-mobile-footer">
							<button class="btn btn-primary btn-icon-only rounded-circle d-none" onclick="takePicture()">
								<img src="{{ asset('XR/themes/tryonv4_03/img/Camera_Take_Photo_bigger.svg') }}" width="45" />
							</button>
              <div class="d-flex justify-content-between align-items-center bottom-sheet-features" >
                <div class="bottom-sheet-option-left">
                  <img class="d-none" src="{{ asset('XR/themes/tryonv4_03/img/split-screen.svg') }}" onclick="compareScreen()" />
                  <img class="d-none" src="{{ asset('XR/themes/tryonv4_03/img/reset_icon.svg') }}" />
                </div>
                <div class="bottom-sheet-option-center">
                  <svg viewBox="0 0 147 148" fill="none" xmlns="http://www.w3.org/2000/svg" style="cursor: pointer" onclick="captureBtnMobile(event)" >
                    <circle cx="77" cy="70.8296" r="51" fill="white"/>
                    <g filter="url(#filter0_dd_631_2297)">
                      <circle cx="77" cy="70.8296" r="40" fill="white"/>
                    </g>
                    <g clip-path="url(#clip0_631_2297)">
                      <path d="M95.6667 82.4963C95.6667 83.3803 95.3155 84.2282 94.6904 84.8533C94.0652 85.4784 93.2174 85.8296 92.3333 85.8296H62.3333C61.4493 85.8296 60.6014 85.4784 59.9763 84.8533C59.3512 84.2282 59 83.3803 59 82.4963V64.1629C59 63.2789 59.3512 62.431 59.9763 61.8059C60.6014 61.1808 61.4493 60.8296 62.3333 60.8296H69L72.3333 55.8296H82.3333L85.6667 60.8296H92.3333C93.2174 60.8296 94.0652 61.1808 94.6904 61.8059C95.3155 62.431 95.6667 63.2789 95.6667 64.1629V82.4963Z" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" style="stroke: var(--tryon_primary_color)" />
                      <path d="M76.6667 79.1629C80.3486 79.1629 83.3333 76.1782 83.3333 72.4963C83.3333 68.8144 80.3486 65.8296 76.6667 65.8296C72.9848 65.8296 70 68.8144 70 72.4963C70 76.1782 72.9848 79.1629 76.6667 79.1629Z" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" style="stroke: var(--tryon_primary_color)" />
                    </g>
                    <defs>
                    <filter id="filter0_dd_631_2297" x="0" y="0.82959" width="147" height="147" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                      <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                      <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                      <feOffset dx="-7"/>
                      <feGaussianBlur stdDeviation="15"/>
                      <feComposite in2="hardAlpha" operator="out"/>
                      <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.18 0"/>
                      <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_631_2297"/>
                      <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                      <feOffset dy="7"/>
                      <feGaussianBlur stdDeviation="15"/>
                      <feComposite in2="hardAlpha" operator="out"/>
                      <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.18 0"/>
                      <feBlend mode="normal" in2="effect1_dropShadow_631_2297" result="effect2_dropShadow_631_2297"/>
                      <feBlend mode="normal" in="SourceGraphic" in2="effect2_dropShadow_631_2297" result="shape"/>
                    </filter>
                    <clipPath id="clip0_631_2297">
                      <rect width="40" height="40" fill="white" transform="translate(57 50.8296)"/>
                    </clipPath>
                    </defs>
                  </svg>
                </div>
                <div class="bottom-sheet-option-right">
                  <img class="d-none settings-btn-icon" src="{{ asset('XR/themes/tryonv4_03/img/Setting.svg') }}" style="cursor: pointer" >
                  <img class="d-none" src="{{ asset('XR/themes/tryonv4_03/img/camera_flip.svg') }}" id="flip-camera-btn-mob" onclick="flipCameraMobile(event)" />
                  <svg class="zoom-in-mob d-none" width="55" height="55" viewBox="0 0 131 131" fill="none" xmlns="http://www.w3.org/2000/svg" onclick="camZoom('zoomIn')" >
                    <circle cx="69.3164" cy="62.1211" r="40" fill="white"/>
                    <g filter="url(#filter0_dd_724_7429)">
                      <circle cx="69.3169" cy="62.1211" r="31.3725" fill="white"/>
                    </g>
                    <path d="M69.1431 55.6714H66.3226V59.9022H62.0918V62.7227H66.3226V66.9535H69.1431V62.7227H73.3739V59.9022H69.1431V55.6714Z" style="fill: var(--tryon_primary_color)"/>
                    <path d="M67.7323 50.0303C61.5116 50.0303 56.4502 55.0917 56.4502 61.3124C56.4502 67.5331 61.5116 72.5946 67.7323 72.5946C70.2355 72.594 72.6666 71.7561 74.6384 70.214L80.838 76.4136L82.8321 74.4195L76.6325 68.2199C78.1754 66.2479 79.0139 63.8163 79.0145 61.3124C79.0145 55.0917 73.953 50.0303 67.7323 50.0303ZM67.7323 69.774C63.0658 69.774 59.2707 65.979 59.2707 61.3124C59.2707 56.6458 63.0658 52.8508 67.7323 52.8508C72.3989 52.8508 76.194 56.6458 76.194 61.3124C76.194 65.979 72.3989 69.774 67.7323 69.774Z" style="fill: var(--tryon_primary_color)"/>
                    <defs>
                      <filter id="filter0_dd_724_7429" x="0.944336" y="0.748535" width="129.745" height="129.745" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                        <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                        <feOffset dx="-7"/>
                        <feGaussianBlur stdDeviation="15"/>
                        <feComposite in2="hardAlpha" operator="out"/>
                        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.18 0"/>
                        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_724_7429"/>
                        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                        <feOffset dy="7"/>
                        <feGaussianBlur stdDeviation="15"/>
                        <feComposite in2="hardAlpha" operator="out"/>
                        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.18 0"/>
                        <feBlend mode="normal" in2="effect1_dropShadow_724_7429" result="effect2_dropShadow_724_7429"/>
                        <feBlend mode="normal" in="SourceGraphic" in2="effect2_dropShadow_724_7429" result="shape"/>
                      </filter>
                    </defs>
                  </svg>
                  <svg class="zoom-out-mob d-none" width="55" height="55" viewBox="0 0 131 131" fill="none" xmlns="http://www.w3.org/2000/svg" onclick="camZoom('zoomOut')">
                    <circle cx="69.3164" cy="62.1211" r="40" fill="white"/>
                      <g filter="url(#filter0_dd_724_7435)">
                    <circle cx="69.3169" cy="62.1211" r="31.3725" fill="white"/>
                    </g>
                    <path d="M67.7323 50.0303C61.5116 50.0303 56.4502 55.0917 56.4502 61.3124C56.4502 67.5331 61.5116 72.5946 67.7323 72.5946C70.2355 72.594 72.6666 71.7561 74.6384 70.214L80.838 76.4136L82.8321 74.4195L76.6325 68.2199C78.1754 66.2479 79.0139 63.8163 79.0145 61.3124C79.0145 55.0917 73.953 50.0303 67.7323 50.0303ZM67.7323 69.774C63.0658 69.774 59.2707 65.979 59.2707 61.3124C59.2707 56.6458 63.0658 52.8508 67.7323 52.8508C72.3989 52.8508 76.194 56.6458 76.194 61.3124C76.194 65.979 72.3989 69.774 67.7323 69.774Z" style="fill: var(--tryon_primary_color);"/>
                    <rect x="61.6299" y="60.21" width="11.7438" height="3.01218" style="fill: var(--tryon_primary_color);"/>
                    <defs>
                      <filter id="filter0_dd_724_7435" x="0.944336" y="0.748535" width="129.745" height="129.745" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                        <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                        <feOffset dx="-7"/>
                        <feGaussianBlur stdDeviation="15"/>
                        <feComposite in2="hardAlpha" operator="out"/>
                        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.18 0"/>
                        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_724_7435"/>
                        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                        <feOffset dy="7"/>
                        <feGaussianBlur stdDeviation="15"/>
                        <feComposite in2="hardAlpha" operator="out"/>
                        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.18 0"/>
                        <feBlend mode="normal" in2="effect1_dropShadow_724_7435" result="effect2_dropShadow_724_7435"/>
                        <feBlend mode="normal" in="SourceGraphic" in2="effect2_dropShadow_724_7435" result="shape"/>
                      </filter>
                    </defs>
                  </svg>
                </div>
              </div>
						</div>
						<div class="card bottom-sheet-mobile">
							<div class="card-header bg-white product-modal-header" style="border-bottom: none;">
								<div class="bottom-sheet-top-indicator-div" style="display: flex; justify-content: center; margin-top: 8px;">
                  <img src="{{ asset('XR/themes/tryonv4_03/img/dropdown_up.svg') }}" width="40px" onclick="ShowHideBottomSheet(event)" id="drop-down-indicator" >
								</div>
								<div class="menu-wrapper d-flex">
									<!-- Top Navbar -- List of Categories -->
									<div class="nav nav-tabs nav-fill" id="nav-tab-mob" role="tablist"></div>

									<!-- Categories Navbar Left - Right Paddle -->
									<div class="paddles">
										<button class="btn btn-icon-only left-paddle paddle">
                      <svg viewBox="0 0 107 113" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <g opacity="0.6">
                            <ellipse cx="53.3525" cy="56.5165" rx="53.0732" ry="56.295" transform="rotate(-180 53.3525 56.5165)" style="fill: var(--tryon_secondary_color);"/>
                            <path d="M61.4766 40.2676L45.2275 56.5167L61.4766 72.7658" stroke-width="3.06" stroke-linecap="round" stroke-linejoin="round" style="stroke: var(--tryon_primary_color);" />
                          </g>
                        </svg>
										</button>
										<button class="btn btn-icon-only right-paddle paddle">
                      <svg viewBox="0 0 114 113" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g opacity="0.6">
                          <ellipse rx="56.2954" ry="56.295" transform="matrix(1 0 0 -1 57.1001 56.5165)" style="fill: var(--tryon_secondary_color)" />
                          <path d="M48.9766 41.1978L65.2255 56.5167L48.9766 71.8356" stroke-width="3.06" stroke-linecap="round" stroke-linejoin="round" style="stroke: var(--tryon_primary_color);" />
                        </g>
                      </svg>
										</button>
									</div>
								</div>
                <div class="d-flex justify-content-center">
                  <form class="d-flex" id="search-form_mob">
                    <input class="form-control" id="search_mob" placeholder="Search" aria-label="Search" />
                    <button class="btn btn-primary d-flex align-items-center" id="search_btn_mob">
                    <i class="fas fa-search" style="margin-right: 5px;"></i>Search</button>
                  </form>
                </div>
							</div>

							<!-- Content of Product List for Eeach Category -->
							<div class="card-body product-modal-body-mob" style="z-index: 1">
								<div class="loader-wrapper" style="display: none;">
									<h1 class="display-1 loading-text">Loading...</h1>
								</div>

								<div class="tab-content clearfix tab-content-mob" id="tab-content-mob"></div>
							</div>

              <div class="card-footer bottom-sheet-footer">
                <div class="d-flex justify-content-center align-items-end">
                  <div style="margin-right: 5px">Powered by</div>
                  <img src="{{ asset('XR/themes/tryonv4_03/img/logo_color.svg') }}" style="width: 20%; margin-left: 5px;"></div>
              </div>
						</div>
					</div>
				</div>
			</div>
			<div class="product-details-mob" style="z-index: 2">
				<div class="card-header product-modal-detail-header-mob">
					<button class="btn btn-primary btn-icon-only rounded-circle float-left" style="top: 0; right: 0"
						id="product-details-close-btn-mob" onclick="showHideDetailsMobile()">
						<img src="{{ asset('XR/themes/tryonv4_03/img/arrow-left.svg') }}" />
					</button>
					<button class="btn btn-primary btn-icon-only rounded-circle float-right" style="top: 0; right: 0"
						id="product-details-minimize-btn-mob" onclick="minimizeDetailsMob()">
						<img height="20px" src="{{ asset('XR/themes/tryonv4_03/img/minimize.png') }}" />
					</button>
					<div class="display-4 product-details-header-title-mob">Product Details</div>
				</div>
				<div class="row mob-details-content-scroll">
					<div class="col-12" class="product-details-mob-images">
						<div class="">
							<div id="custCarousel-mob" class="carousel slide custCarousel" data-ride="carousel" data-interval="false"
								align="center">
								<!-- slides -->
								<div class="carousel-inner product-details-carousel-image-group-mob"></div>
								<!-- Left right -->
								<a class="carousel-control-prev" href="#custCarousel-mob" data-slide="prev">
									<span class="carousel-control-prev-icon"></span>
								</a>
								<a class="carousel-control-next" href="#custCarousel-mob" data-slide="next">
									<span class="carousel-control-next-icon" style="color: black"></span>
								</a>
								<!-- Thumbnails -->
								<!-- <ol class="carousel-indicators list-inline product-carousel-thumbnail-mob"></ol> -->
							</div>
						</div>
					</div>

					<div class="col-12">
						<div class="product-modal-detail scrollbar">
							<div>
								<h3 class="display-1 product-modal-detail-title product-modal-detail-title-mob">Product Title</h3>
							</div>
							<div class="ml-1 row d-flex align-items-center">
								<h4 class="display-4 product-modal-detail-price product-modal-detail-price-mob">&#x20b9; 99999</h4>
								<div class="d-flex ml-4">
									<button class="btn btn-sm add-to-cart-btn" onclick="addtocart(this,event)">Add to cart</button>
									<button class="btn btn-md remove-from-cart-btn" style="display: none;"
										onclick="removefromcart(this,event)">Remove from cart</button>
								</div>
							</div>
							<div>
								<p class="lead product-modal-detail-product-details product-modal-detail-product-details-mob scrollbar" style="z-index: 1">
									Product Details</p>
							</div>
							<div id="product-detail"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Template For Product Card -->
	<template id="product-card">
		<div class="col-12 mt-4 col-sm-12 col-md-6 col-lg-6 product-card-div">
			<div class="card product-card product-card-desktop" style="cursor: pointer" onclick="tryon(this,event)">
				<img class="product-card-img" src="{{ asset('XR/themes/tryonv4_03/img/sample_img.jpg') }}" width="50px" alt="Product Image" />
				<input type="hidden" class="img-urls" name="img-urls" value="" />
				<input type="hidden" class="tryonNeck" name="tryonNeck" value="" />
				<input type="hidden" class="tryonApparel" name="tryonApparel" value="" />
				<input type="hidden" class="tryonEarL" name="tryonEarL" value="" />
				<input type="hidden" class="tryonEarR" name="tryonEarR" value="" />
				<input type="hidden" class="tryonWatch" name="tryonWatch" value="" />
				<input type="hidden" class="tryonRing" name="tryonRing" value="" />
				<input type="hidden" class="tryonBracelet" name="tryonBracelet" value="" />
				<input type="hidden" class="product-sku" name="productSKU" value="" />
				<div class="card-body card-product-name-div">
					<h2 class="product-card-title text-truncate">product name</h2>
				</div>
				<div class="card-footer product-card-btn" onclick="viewDetails(this,event)">
					View Details <input type="hidden" class="product-details-data" name="product-details-data" value="" />
				</div>
			</div>
		</div>
	</template>

	<template id="product-card-mob">
		<div class="col-12 mt-2 col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12 product-card-div">
			<div class="card product-card product-card-mob" style="cursor: pointer;" onclick="tryon(this,event)">
				<div class="product-card-div-img">
					<img class="product-card-img product-card-img-mob" src="{{ asset('XR/themes/tryonv4_03/img/sample_img.jpg') }}" width="50px" alt="Product Image" />
				</div>
				<input type="hidden" class="img-urls" name="img-urls" value="" />
				<input type="hidden" class="tryonNeck" name="tryonNeck" value="" />
				<input type="hidden" class="tryonApparel" name="tryonApparel" value="" />
				<input type="hidden" class="tryonEarL" name="tryonEarL" value="" />
				<input type="hidden" class="tryonEarR" name="tryonEarR" value="" />
				<input type="hidden" class="tryonWatch" name="tryonWatch" value="" />
				<input type="hidden" class="tryonRing" name="tryonRing" value="" />
				<input type="hidden" class="tryonBracelet" name="tryonBracelet" value="" />
				<input type="hidden" class="product-sku" name="productSKU" value="" />
				<div class="card-body card-product-name-div">
					<h2 class="product-card-title text-truncate">product name</h2>
				</div>
        <div class="card-footer product-card-btn" onclick="viewDetailsMobile(this,event)">
          View Details <input type="hidden" class="product-details-data" name="product-details-data" value="" />
        </div>
      </div>
		</div>
	</template>

  <template id="prev-next-template">
    <div class="position-absolute justify-content-between previous-next-btn" style="display: flex; z-index: 2; top: 50%; width: calc(65% - 25px);transform: translateY(-50%);">
      <svg width="30" height="30" viewBox="0 0 107 113" fill="none" xmlns="http://www.w3.org/2000/svg" style="margin-left: 5px;" onclick="prevNextProduct('previous')">
        <g opacity="0.6">
          <ellipse cx="53.3525" cy="56.5165" rx="53.0732" ry="56.295" transform="rotate(-180 53.3525 56.5165)" style="fill: var(--tryon_secondary_color)"/>
          <path d="M61.4766 40.2676L45.2275 56.5167L61.4766 72.7658" stroke-width="3.06" stroke-linecap="round" stroke-linejoin="round" style="stroke: var(--tryon_primary_color)" />
        </g>
      </svg>
      <svg width="30" height="30" viewBox="0 0 114 113" fill="none" xmlns="http://www.w3.org/2000/svg" style="margin-right: 5px;" onclick="prevNextProduct('next')" >
        <g opacity="0.6">
          <ellipse rx="56.2954" ry="56.295" transform="matrix(1 0 0 -1 57.1001 56.5165)" style="fill: var(--tryon_secondary_color)"/>
          <path d="M48.9766 41.1978L65.2255 56.5167L48.9766 71.8356" stroke-width="3.06" stroke-linecap="round" stroke-linejoin="round" style="stroke: var(--tryon_primary_color)" />
        </g>
      </svg>
    </div>
  </template>

  <template id="side-compare-template">
    <div class="position-absolute side-compare-tray" style="z-index: 2; display: none;" >
      <div class="side-compare-tray-container">
        <div class="card side-tray-card position-relative">
          <div class="w-100 text-center font-weight-bold my-2 card-title" style="font-size: 14px;" >Selected Items</div>
          <div class="side-tray-scroll position-relative card-body p-0 mb-2"></div>
        </div>
        <svg class="position-absolute" width="25" height="25" viewBox="0 0 71 70" fill="none" xmlns="http://www.w3.org/2000/svg" style="right: -25px; top:0;" onclick="sideTrayToggle()" >
          <path d="M70.3242 50C70.3242 61.0457 61.3699 70 50.3242 70L0.324219 70L0.324213 6.11959e-06L50.3242 1.74846e-06C61.3699 7.8281e-07 70.3242 8.95431 70.3242 20L70.3242 50Z" style="fill: var(--tryon_primary_color)"/>
          <path d="M31.2402 43.4165L20.8236 32.9998L31.2402 22.5832" stroke="white" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"/>
          <path d="M45.8242 43.4165L35.4076 32.9998L45.8242 22.5832" stroke="white" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>
      </div>
      <svg class="position-absolute side-tray-open-icon" width="25" height="25" viewBox="0 0 71 70" fill="none" xmlns="http://www.w3.org/2000/svg" style="display:none; right: -25px; top: -85px;" onclick="sideTrayToggle()">
        <path d="M70.3242 50C70.3242 61.0457 61.3699 70 50.3242 70L0.324219 70L0.324213 6.11959e-06L50.3242 1.74846e-06C61.3699 7.8281e-07 70.3242 8.95431 70.3242 20L70.3242 50Z" style="fill: var(--tryon_primary_color)"/>
        <path d="M35.4082 43.4165L45.8249 32.9998L35.4082 22.5832" stroke="white" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M20.8242 43.4165L31.2409 32.9998L20.8242 22.5832" stroke="white" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"/>
      </svg>
    </div>
  </template>

  <template id="footer-menu-template">
    <div class="footer-area d-flex flex-column" style="z-index:1;">
      <div class="d-flex justify-content-between reset-btn-container" style="width: calc(65% - 20px); position: relative;">
        <!-- <button class="btn btn-primary float-left d-flex align-items-center" onclick="reset()" style="border-radius: 25px;font-size: 12px;padding: 6px 20px;width: 100px;">
          <img src="{{ asset('XR/themes/tryonv4_03/img/system-uicons_reset-temporary.svg') }}" width="14" style="margin-right:6px;" /> Reset
        </button> -->
        <div class="d-flex flex-column position-absolute" style="right:0px; bottom: 15px;" >
          <span class="flex-column compare-screen-toggle-btns" style="display: flex;" >
          <svg width="55" height="55" viewBox="0 0 131 131" fill="none" xmlns="http://www.w3.org/2000/svg" onclick="cameraZoomIn()" >
              <circle cx="69.3164" cy="62.1211" r="40" fill="white"/>
              <g filter="url(#filter0_dd_724_7429)">
                <circle cx="69.3169" cy="62.1211" r="31.3725" fill="white"/>
              </g>
              <path d="M69.1431 55.6714H66.3226V59.9022H62.0918V62.7227H66.3226V66.9535H69.1431V62.7227H73.3739V59.9022H69.1431V55.6714Z" style="fill: var(--tryon_primary_color)"/>
              <path d="M67.7323 50.0303C61.5116 50.0303 56.4502 55.0917 56.4502 61.3124C56.4502 67.5331 61.5116 72.5946 67.7323 72.5946C70.2355 72.594 72.6666 71.7561 74.6384 70.214L80.838 76.4136L82.8321 74.4195L76.6325 68.2199C78.1754 66.2479 79.0139 63.8163 79.0145 61.3124C79.0145 55.0917 73.953 50.0303 67.7323 50.0303ZM67.7323 69.774C63.0658 69.774 59.2707 65.979 59.2707 61.3124C59.2707 56.6458 63.0658 52.8508 67.7323 52.8508C72.3989 52.8508 76.194 56.6458 76.194 61.3124C76.194 65.979 72.3989 69.774 67.7323 69.774Z" style="fill: var(--tryon_primary_color)"/>
              <defs>
                <filter id="filter0_dd_724_7429" x="0.944336" y="0.748535" width="129.745" height="129.745" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                  <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                  <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                  <feOffset dx="-7"/>
                  <feGaussianBlur stdDeviation="15"/>
                  <feComposite in2="hardAlpha" operator="out"/>
                  <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.18 0"/>
                  <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_724_7429"/>
                  <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                  <feOffset dy="7"/>
                  <feGaussianBlur stdDeviation="15"/>
                  <feComposite in2="hardAlpha" operator="out"/>
                  <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.18 0"/>
                  <feBlend mode="normal" in2="effect1_dropShadow_724_7429" result="effect2_dropShadow_724_7429"/>
                  <feBlend mode="normal" in="SourceGraphic" in2="effect2_dropShadow_724_7429" result="shape"/>
                </filter>
              </defs>
            </svg>
            <svg width="55" height="55" viewBox="0 0 131 131" fill="none" xmlns="http://www.w3.org/2000/svg" onclick="cameraZoomOut()">
              <circle cx="69.3164" cy="62.1211" r="40" fill="white"/>
                <g filter="url(#filter0_dd_724_7435)">
              <circle cx="69.3169" cy="62.1211" r="31.3725" fill="white"/>
              </g>
              <path d="M67.7323 50.0303C61.5116 50.0303 56.4502 55.0917 56.4502 61.3124C56.4502 67.5331 61.5116 72.5946 67.7323 72.5946C70.2355 72.594 72.6666 71.7561 74.6384 70.214L80.838 76.4136L82.8321 74.4195L76.6325 68.2199C78.1754 66.2479 79.0139 63.8163 79.0145 61.3124C79.0145 55.0917 73.953 50.0303 67.7323 50.0303ZM67.7323 69.774C63.0658 69.774 59.2707 65.979 59.2707 61.3124C59.2707 56.6458 63.0658 52.8508 67.7323 52.8508C72.3989 52.8508 76.194 56.6458 76.194 61.3124C76.194 65.979 72.3989 69.774 67.7323 69.774Z" style="fill: var(--tryon_primary_color);"/>
              <rect x="61.6299" y="60.21" width="11.7438" height="3.01218" style="fill: var(--tryon_primary_color);"/>
              <defs>
                <filter id="filter0_dd_724_7435" x="0.944336" y="0.748535" width="129.745" height="129.745" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                  <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                  <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                  <feOffset dx="-7"/>
                  <feGaussianBlur stdDeviation="15"/>
                  <feComposite in2="hardAlpha" operator="out"/>
                  <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.18 0"/>
                  <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_724_7435"/>
                  <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                  <feOffset dy="7"/>
                  <feGaussianBlur stdDeviation="15"/>
                  <feComposite in2="hardAlpha" operator="out"/>
                  <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.18 0"/>
                  <feBlend mode="normal" in2="effect1_dropShadow_724_7435" result="effect2_dropShadow_724_7435"/>
                  <feBlend mode="normal" in="SourceGraphic" in2="effect2_dropShadow_724_7435" result="shape"/>
                </filter>
              </defs>
            </svg>
            <svg id="toggle-btn-active" width="55" height="55" viewBox="0 0 131 131" fill="none" xmlns="http://www.w3.org/2000/svg" onclick="ActivateLayer(this)" >
              <circle cx="69" cy="62" r="40" fill="white"/>
                <g filter="url(#filter0_dd_941_811)">
              <circle cx="69.0005" cy="62" r="31.3725" fill="white"/>
              </g>
              <path d="M86 61.999C86 61.1908 85.5714 60.4543 84.8515 60.0271L82.9583 58.9133C83.6876 58.4803 84.3375 58.094 84.853 57.7873C85.5714 57.3586 86 56.6221 86 55.8169C86 55.0117 85.5714 54.2751 84.853 53.8457C82.4865 52.4383 74.4619 47.6696 70.5062 45.4019C69.5901 44.8759 68.4763 44.8548 67.4862 45.4064C67.4862 45.4064 56.7284 51.7252 53.1484 53.8451C52.4286 54.2721 52 55.0086 52 55.8169C52 56.6221 52.4286 57.3586 53.147 57.7881C53.6624 58.0946 54.3122 58.4808 55.0414 58.9136L53.1485 60.0271C52.4286 60.4543 52 61.1908 52 61.999C52 62.8043 52.4286 63.5408 53.147 63.9702C53.6624 64.2767 54.3122 64.6629 55.0414 65.0958L53.1485 66.2093C52.4286 66.6364 52 67.373 52 68.1812C52 68.9864 52.4286 69.7229 53.147 70.1524C56.2092 71.9733 63.7358 76.4417 67.4938 78.5962C67.9541 78.8604 68.4763 79 69 79C69.5237 79 70.046 78.8604 70.5078 78.5954C74.2643 76.4416 81.7908 71.9733 84.8531 70.1516C85.5715 69.7229 86.0001 68.9864 86.0001 68.1812C86.0001 67.373 85.5714 66.6364 84.8516 66.2093L82.9583 65.0955C83.6877 64.6624 84.3376 64.2762 84.8531 63.9694C85.5714 63.5408 86 62.8043 86 61.999ZM53.9379 56.4618C53.8201 56.3909 53.5454 56.1871 53.5454 55.8182C53.5454 55.4476 53.8186 55.2455 53.9363 55.176C57.5117 53.0585 68.2545 46.7492 68.2529 46.7499C68.745 46.4729 69.2822 46.4835 69.738 46.7446C73.6831 49.0062 81.7002 53.7693 84.0621 55.1745C84.1799 55.2454 84.4546 55.4492 84.4546 55.8181C84.4546 56.1871 84.1799 56.3909 84.0621 56.461C81.0045 58.2797 73.487 62.7433 69.7395 64.8909C69.2747 65.1566 68.7238 65.1558 68.262 64.8916C64.513 62.7433 56.9955 58.2797 53.9379 56.4618ZM84.0637 67.5396C84.1814 67.609 84.4546 67.8112 84.4546 68.1818C84.4546 68.5508 84.1799 68.7545 84.0621 68.8247C81.0045 70.6433 73.487 75.1069 69.7395 77.2546C69.2747 77.5202 68.7238 77.5194 68.262 77.2553C64.513 75.1069 56.9955 70.6433 53.9379 68.8254C53.8201 68.7545 53.5454 68.5507 53.5454 68.1818C53.5454 67.8112 53.8186 67.6091 53.9363 67.5396L56.5587 65.9965C60.1032 68.0974 64.7788 70.8579 67.4938 72.4144C67.9541 72.6785 68.4763 72.8181 69 72.8181C69.5237 72.8181 70.046 72.6785 70.5078 72.4136C73.2216 70.8577 77.8968 68.0976 81.4411 65.9964L84.0637 67.5396ZM84.0621 62.6429C81.0045 64.4615 73.487 68.9251 69.7395 71.0728C69.2747 71.3384 68.7238 71.3376 68.262 71.0735C64.513 68.9251 56.9955 64.4615 53.9379 62.6436C53.8201 62.5727 53.5454 62.3689 53.5454 62C53.5454 61.6294 53.8186 61.4273 53.9363 61.3578L56.5587 59.8147C60.1032 61.9156 64.7788 64.6761 67.4938 66.2326C67.9541 66.4967 68.4763 66.6363 69 66.6363C69.5237 66.6363 70.046 66.4967 70.5078 66.2318C73.2216 64.6759 77.8968 61.9158 81.4411 59.8146L84.0637 61.3578C84.1815 61.4272 84.4546 61.6294 84.4546 62C84.4546 62.3689 84.1799 62.5727 84.0621 62.6429Z" style="fill: var(--tryon_primary_color)"/>
              <defs>
                <filter id="filter0_dd_941_811" x="0.62793" y="0.627441" width="129.745" height="129.745" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                  <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                  <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                  <feOffset dx="-7"/>
                  <feGaussianBlur stdDeviation="15"/>
                  <feComposite in2="hardAlpha" operator="out"/>
                  <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.18 0"/>
                  <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_941_811"/>
                  <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                  <feOffset dy="7"/>
                  <feGaussianBlur stdDeviation="15"/>
                  <feComposite in2="hardAlpha" operator="out"/>
                  <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.18 0"/>
                  <feBlend mode="normal" in2="effect1_dropShadow_941_811" result="effect2_dropShadow_941_811"/>
                  <feBlend mode="normal" in="SourceGraphic" in2="effect2_dropShadow_941_811" result="shape"/>
                </filter>
              </defs>
            </svg>
            <svg class="toggle-layer-btn" id="toggle-btn-disable" width="55" height="55" viewBox="0 0 131 131" fill="none" xmlns="http://www.w3.org/2000/svg" style="display: none;" onclick="ActivateLayer(this)">
              <circle cx="69" cy="62" r="40" fill="white"/>
                <g filter="url(#filter0_dd_944_827)">
                  <circle cx="69.0005" cy="62" r="31.3725" fill="white"/>
                </g>
              <path d="M86 61.999C86 61.1908 85.5714 60.4543 84.8515 60.0271L82.9583 58.9133C83.6876 58.4803 84.3375 58.094 84.853 57.7873C85.5714 57.3586 86 56.6221 86 55.8169C86 55.0117 85.5714 54.2751 84.853 53.8457C82.4865 52.4383 74.4619 47.6696 70.5062 45.4019C69.5901 44.8759 68.4763 44.8548 67.4862 45.4064C67.4862 45.4064 56.7284 51.7252 53.1484 53.8451C52.4286 54.2721 52 55.0086 52 55.8169C52 56.6221 52.4286 57.3586 53.147 57.7881C53.6624 58.0946 54.3122 58.4808 55.0414 58.9136L53.1485 60.0271C52.4286 60.4543 52 61.1908 52 61.999C52 62.8043 52.4286 63.5408 53.147 63.9702C53.6624 64.2767 54.3122 64.6629 55.0414 65.0958L53.1485 66.2093C52.4286 66.6364 52 67.373 52 68.1812C52 68.9864 52.4286 69.7229 53.147 70.1524C56.2092 71.9733 63.7358 76.4417 67.4938 78.5962C67.9541 78.8604 68.4763 79 69 79C69.5237 79 70.046 78.8604 70.5078 78.5954C74.2643 76.4416 81.7908 71.9733 84.8531 70.1516C85.5715 69.7229 86.0001 68.9864 86.0001 68.1812C86.0001 67.373 85.5714 66.6364 84.8516 66.2093L82.9583 65.0955C83.6877 64.6624 84.3376 64.2762 84.8531 63.9694C85.5714 63.5408 86 62.8043 86 61.999ZM53.9379 56.4618C53.8201 56.3909 53.5454 56.1871 53.5454 55.8182C53.5454 55.4476 53.8186 55.2455 53.9363 55.176C57.5117 53.0585 68.2545 46.7492 68.2529 46.7499C68.745 46.4729 69.2822 46.4835 69.738 46.7446C73.6831 49.0062 81.7002 53.7693 84.0621 55.1745C84.1799 55.2454 84.4546 55.4492 84.4546 55.8181C84.4546 56.1871 84.1799 56.3909 84.0621 56.461C81.0045 58.2797 73.487 62.7433 69.7395 64.8909C69.2747 65.1566 68.7238 65.1558 68.262 64.8916C64.513 62.7433 56.9955 58.2797 53.9379 56.4618ZM84.0637 67.5396C84.1814 67.609 84.4546 67.8112 84.4546 68.1818C84.4546 68.5508 84.1799 68.7545 84.0621 68.8247C81.0045 70.6433 73.487 75.1069 69.7395 77.2546C69.2747 77.5202 68.7238 77.5194 68.262 77.2553C64.513 75.1069 56.9955 70.6433 53.9379 68.8254C53.8201 68.7545 53.5454 68.5507 53.5454 68.1818C53.5454 67.8112 53.8186 67.6091 53.9363 67.5396L56.5587 65.9965C60.1032 68.0974 64.7788 70.8579 67.4938 72.4144C67.9541 72.6785 68.4763 72.8181 69 72.8181C69.5237 72.8181 70.046 72.6785 70.5078 72.4136C73.2216 70.8577 77.8968 68.0976 81.4411 65.9964L84.0637 67.5396ZM84.0621 62.6429C81.0045 64.4615 73.487 68.9251 69.7395 71.0728C69.2747 71.3384 68.7238 71.3376 68.262 71.0735C64.513 68.9251 56.9955 64.4615 53.9379 62.6436C53.8201 62.5727 53.5454 62.3689 53.5454 62C53.5454 61.6294 53.8186 61.4273 53.9363 61.3578L56.5587 59.8147C60.1032 61.9156 64.7788 64.6761 67.4938 66.2326C67.9541 66.4967 68.4763 66.6363 69 66.6363C69.5237 66.6363 70.046 66.4967 70.5078 66.2318C73.2216 64.6759 77.8968 61.9158 81.4411 59.8146L84.0637 61.3578C84.1815 61.4272 84.4546 61.6294 84.4546 62C84.4546 62.3689 84.1799 62.5727 84.0621 62.6429Z" style="fill: var(--tryon_primary_color)"/>
              <rect x="51" y="44.7046" width="3.83112" height="45.6118" transform="rotate(-44.9051 51 44.7046)" style="fill: var(--tryon_primary_color)"/>
              <defs>
                <filter id="filter0_dd_944_827" x="0.62793" y="0.627441" width="129.745" height="129.745" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                  <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                  <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                  <feOffset dx="-7"/>
                  <feGaussianBlur stdDeviation="15"/>
                  <feComposite in2="hardAlpha" operator="out"/>
                  <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.18 0"/>
                  <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_944_827"/>
                  <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                  <feOffset dy="7"/>
                  <feGaussianBlur stdDeviation="15"/>
                  <feComposite in2="hardAlpha" operator="out"/>
                  <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.18 0"/>
                  <feBlend mode="normal" in2="effect1_dropShadow_944_827" result="effect2_dropShadow_944_827"/>
                  <feBlend mode="normal" in="SourceGraphic" in2="effect2_dropShadow_944_827" result="shape"/>
                </filter>
              </defs>
            </svg>
          </span>
          <img class="d-none" src="{{ asset('XR/themes/tryonv4_03/img/split-screen.svg') }}" width="55px" onclick="compareScreen(this)" />
        </div>
      </div>
      
      <div class="d-flex justify-content-center camera-btn-container" style="width: calc(65% - 20px)">
        <!-- <button type="button" class="btn btn-primary mr-1 prev-next-btn"><i class="fas fa-arrow-left mr-2"></i>Previous</button> -->
        <svg class="position-absolute camera-btn-icon" width="70" height="70" viewBox="0 0 147 148" fill="none" xmlns="http://www.w3.org/2000/svg" onclick="takePicture()" style="top: -70px" >
          <circle cx="77" cy="70.8296" r="51" fill="white"/>
          <g filter="url(#filter0_dd_631_2297)">
            <circle cx="77" cy="70.8296" r="40" fill="white"/>
          </g>
          <g clip-path="url(#clip0_631_2297)">
            <path d="M95.6667 82.4963C95.6667 83.3803 95.3155 84.2282 94.6904 84.8533C94.0652 85.4784 93.2174 85.8296 92.3333 85.8296H62.3333C61.4493 85.8296 60.6014 85.4784 59.9763 84.8533C59.3512 84.2282 59 83.3803 59 82.4963V64.1629C59 63.2789 59.3512 62.431 59.9763 61.8059C60.6014 61.1808 61.4493 60.8296 62.3333 60.8296H69L72.3333 55.8296H82.3333L85.6667 60.8296H92.3333C93.2174 60.8296 94.0652 61.1808 94.6904 61.8059C95.3155 62.431 95.6667 63.2789 95.6667 64.1629V82.4963Z" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" style="stroke: var(--tryon_primary_color)" />
            <path d="M76.6667 79.1629C80.3486 79.1629 83.3333 76.1782 83.3333 72.4963C83.3333 68.8144 80.3486 65.8296 76.6667 65.8296C72.9848 65.8296 70 68.8144 70 72.4963C70 76.1782 72.9848 79.1629 76.6667 79.1629Z"  stroke-width="3" stroke-linecap="round" stroke-linejoin="round" style="stroke: var(--tryon_primary_color)" />
          </g>
          <defs>
            <filter id="filter0_dd_631_2297" x="0" y="0.82959" width="147" height="147" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
              <feFlood flood-opacity="0" result="BackgroundImageFix"/>
              <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
              <feOffset dx="-7"/>
              <feGaussianBlur stdDeviation="15"/>
              <feComposite in2="hardAlpha" operator="out"/>
              <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.18 0"/>
              <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_631_2297"/>
              <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
              <feOffset dy="7"/>
              <feGaussianBlur stdDeviation="15"/>
              <feComposite in2="hardAlpha" operator="out"/>
              <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.18 0"/>
              <feBlend mode="normal" in2="effect1_dropShadow_631_2297" result="effect2_dropShadow_631_2297"/>
              <feBlend mode="normal" in="SourceGraphic" in2="effect2_dropShadow_631_2297" result="shape"/>
            </filter>
            <clipPath id="clip0_631_2297">
              <rect width="40" height="40" fill="white" transform="translate(57 50.8296)"/>
            </clipPath>
          </defs>
        </svg>
        <!-- <button type="button" class="btn btn-primary ml-1 prev-next-btn">Next<i class="fas fa-arrow-right ml-2" style=""></i></button> -->
      </div>
      
    </div>
  </template>

  <div class="modal" id="myShare">
    <div class="modal-dialog modal-myshare">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title text-center">Share</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body text-center">
          <div class="qr-code-container" >
            <img id="qr-code-gen" src="{{asset('XR/assets/images/blank.gif')}}" height="177px" width="177px">
            <div style="font-size: 13px; font-weight: 700; color: #000;">Scan QR to view in mobile</div>
          </div>

          <a class="btn btn-lg btn-social btn-facebook bstyle text-center" href="#"
              onclick='share("fb","")'>
              <i class="fa fa-facebook fa-fw"></i> Share on Facebook
          </a><br>
          <a class="btn btn-lg btn-social btn-whatsapp bstyle text-center" href="#"
              onclick='share("whatsapp","")'>
              <i class="fa fa-whatsapp fa-fw"></i> Share on whatsapp
          </a><br>
          <a class="btn btn-lg btn-social btn-twitter bstyle text-center" href="#"
              onclick='share("twitter","")'>
              <i class="fa fa-twitter fa-fw"></i> Share on twitter
          </a><br>
          <a class="btn btn-lg btn-social btn-copyshare bstyle text-center" href="#"
              onclick='share("copy","")'>
              <i class="fa fa-copy fa-fw"></i> Copy Share Link
          </a><br>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

      </div>
    </div>
  </div>

  <div class="settings-pannel-container">
    <div class="card settings-pannel">
      <div class="card-header">
        <div class="card-title">Camera Settings</div>
        <svg width="20" height="20" viewBox="0 0 56 56" fill="none" xmlns="http://www.w3.org/2000/svg" style="position: absolute; top: -8px; right: -8px;" onclick="closeSettings()" >
          <circle cx="28" cy="28" r="28" style="fill: var(--tryon_primary_color)"/>
          <path d="M39 18L19 38" stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
          <path d="M19 18L39 38" stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>
      </div>
      <div class="card-body d-flex flex-row justify-content-start align-items-start w-100 mt-3">
        <div style="margin-right: 10px;">Select Camera</div>
        <div class="btn-group">
          <button type="button" class="btn btn-primary dropdown-toggle select-cam-dropdown-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Action
          </button>
          <div class="dropdown-menu select-camera-dropdown-menu"></div>
        </div>
      </div>
    </div>
  </div>
</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
	integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
	crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"
	integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA=="
	crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://hammerjs.github.io/dist/hammer.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" 
  integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" 
  crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" 
  integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" 
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/aws-sdk/2.1046.0/aws-sdk.min.js"
	integrity="sha512-VIjLqYYZk88+H3w0D2M3NtF4OdeZcWSNAQqgtQg7CCpeCvMPIwiISF9GXIHvsqnBxl4NG15L91s/+iBxwI10+A=="
	crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/amplitudejs/5.3.2/amplitude.min.js" 
  integrity="sha512-Q/0FAciD7lgeXgWUoL6/GuhRxRLP7KaAEag7TjKyx5hGXKsQc7Sf6dAL2Fg62Od8oXUYgyKNIBbwA2SwNSQi+A==" 
  crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="{{ asset('XR/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
<script src="https://cdn.mirrar.in/v4.0.3/js/init.min.js"></script>

<script>
const BigScreen = "BIG-SCREEN"

const SingleDetailView = "SINGLE";
const SideDetailViewInit = "SIDE-INIT";
const SideDetailView = "SIDE";
const DefaultDetailView = "DEFAULT";

const FaceTracking = "FACE";
const HandTracking = "HAND";

var DetailSideView = true;
var DetailsViewBackBtn = true;

var Mode = BigScreen;
var View = DefaultDetailView;
var Tracking = FaceTracking;
const TrackingSwitch = new Event("tracking-switch");

var current_page = 1; // Current Page to be loaded
var previous_id = ""; // Previously loaded category
var page_loaded; // The Page till which the loading is completed
var limit = 10; // Number of products to be loaded for every API Request
var query_string = "";
var previous_query = "";
var last_page = 1;
var clear_data = 0;
var category_id;
var isCompare = false;
var activeCam = 0;
var isTesting = false;

var trayActive = false;
var trayElements = ['necklace', 'earring', 'watch', 'ring', 'bracelet'];

var selectedScreen = 'screen1';

var allCameras = []
var selectedCameraIndex = null;

var product_details_data = "";

var SwipeActive = true;

var activeProductIndex = 0;
var panGesture = "none";

var api = {
  //api-optional
  endpoint: "{{ URL::to('/') }}/api/v2",
  method: "GET",
  data: {},
  @if(isset($meta_data['tryon_settings']['tryon_ui_product_list_title'])) 
    @if($meta_data['tryon_settings']['tryon_ui_product_list_title'] == "catagory") 
        catagory: "/public/product-categories",
    @else
        catagory: '',
    @endif
  @endif
  products: "/public/products?link={{$path[2]}}",
  searchParam: "q",
  headers: { Authorization:  "Bearer {{ $access_token }}"  },
}

var mode = {
  // Optional
  displayMode: "BIG-SCREEN", // Optional (Default: CAMH)
  detailView: "{{$meta_data['tryon_settings']['tryon_product_detail_mode'] ?? 'NORM'}}", // instant/single/normal (optional) (Default: NORM)
}

var layer_products = {
  "necklace": { "category_name": "NECK", "tryon_url": "", "sku": "" },
  "earring": { "category_name": "EAR", "tryon_url": "", "sku": "" },
  "watch": { "category_name": "WATCH", "tryon_url": "", "sku": "" },
  "ring": { "category_name": "RING", "tryon_url": "", "sku": "" },
  "bracelet": { "category_name": "BRACELET", "tryon_url": "", "sku": "" }
}

var set_products = {
  "necklace": { "category_name": "NECK", "tryon_url": "", "sku": "" },
  "earring": { "category_name": "EAR", "tryon_url": "", "sku": "" },
  "watch": { "category_name": "WATCH", "tryon_url": "", "sku": "" },
  "ring": { "category_name": "RING", "tryon_url": "", "sku": "" },
  "bracelet": { "category_name": "BRACELET", "tryon_url": "", "sku": "" }
}

var visible_layers = {
  "necklace": { "hidden": false },
  "earring": { "hidden": false },
  "watch": { "hidden": false },
  "ring": { "hidden": false },
  "bracelet": { "hidden": false }
}

var tryon_payload = {
  "_id": "",
  "product_category": "SET",
  "product_name": "Product",
  "product_id": "",
  "thumbnail": "",
  "ear_wearing": "",
  "neck_wearing": "",
  "hand_wearing": "",
  "ppu_ear": null,
  "ppu_neck": null,
  "ppu_hand": null,
  "x_ear": null,
  "x_neck": null,
  "y_ear": null,
  "y_neck": null,
  "x_hand": null,
  "y_hand": null,
  "ear_shining": false,
  "neck_shining": true,
}

/* ----------------------------------------Anto Changes start---------------------------------- */
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);

var disableCategory = true;
var product_id_selected = urlParams.get('sku');
var product_item_selected = null;
var takePictureData = null;
var share_url;

document.addEventListener("filterAddRemove", (data) => {
  // console.log("aftertryon",data.detail)      
  product_id_selected = data.detail.data._id;
      if(data.detail.data._id != null)
      window.history.replaceState(null, null, "?sku="+data.detail.data._id);
      });

      document.addEventListener("init", (data) => {
        if(product_item_selected != null) {
            product_item_selected.click()
        }
      });
      
      document.addEventListener("takephoto", (data) => {
        // console.log("takephoto",data.detail.data)
        if(product_id_selected != null) {
            takePictureData = data.detail.data;
            umami('tryon-camera-button-clicked');
            $("#preimgscr").fadeIn("slow").show();
            $("#takePicture").hide();
            $("#preimg").attr("src", data.detail.data);
              var selectedProducts = {
                  "photo": data.detail.data,
                  "cid": "{{$campaign->id}}",
                  "pid": product_id_selected,
                  "_token": "{{ csrf_token() }}",
              };              
              $.ajax({
                type: "POST",
                url: "{{ route('captures.store') }}",
                data: selectedProducts,
                success: function (data) {
                    share_url = data;
                    $("#myShare").modal('show')
                    $("#qr-code-gen").attr(
                      "src",
                      "http://chart.apis.google.com/chart?chs=177x177&amp;&cht=qr&amp;&chl=" + share_url + "&amp;&choe=UTF-8"
                    );
                    
                }, error: function(data){
                  // console.log(JSON.stringify(data));
                }
              });
        }
        
      })

      function closePreimgscr() {
        umami('tryon-go-back-button-clicked');
        $("#preimgscr").fadeOut("slow").hide(500);
        $("#takePicture").show();
      }

      function downloadTakePicture() {
        umami('tryon-download-button-clicked');
        const link = document.createElement('a')
        link.setAttribute('href', takePictureData)
        link.setAttribute('download', 'MirrAR' + Date.now() + '.png')
        link.click()
      }

      function share(share_name,msg) {
          // var u="{{Request::fullUrl()}}";
          
        $('.collapse').collapse();
        var u=share_url;
        var t="I tried this jewellery on me using MirrAR Virtual TryOn. I recommend you to give it a Try. Click on the Try Now button and Allow Camera access to experience the design. @mirrarofficial";
        var hashtags = "MirrAR";
        switch (share_name) {
            case 'fb':
              umami('tryon-facebook-share-button-clicked');
                window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t +" #"+ hashtags),'sharer','toolbar=0,status=0,width=626,height=436');
                break;
            case 'whatsapp':
              umami('tryon-whatsapp-share-button-clicked');
                var message = encodeURIComponent(u) + " " + t + " #" +hashtags;
                // var whatsapp_url = "web.whatsapp://send?text=" + message;
                var whatsapp_url = "whatsapp://send?text=" + message;
                window.location.href = whatsapp_url;
                break;
            case 'twitter':
              umami('tryon-twitter-share-button-clicked');
                window.open("https://twitter.com/intent/tweet?text="+t+"&url="+u+"&hashtags="+hashtags,'sharer','toolbar=0,status=0,width=626,height=436')
                break;
            case 'copy':
              umami('tryon-link-copy-button-clicked');
                var $temp = $("<input>");
                $("#myShare").append($temp);
                $temp.val(u);
                $temp.select();
                document.execCommand("copy");
                $temp.remove();
                $('#demo').collapse('hide');
                $('#MirrAR_preview').hide();
                toastr.success('Share Link', 'Copied!');
                break;
        }
      }

var options = {
  key: "{{$meta_data['tryon_settings']['tryon_key'] ?? ''}}",
  ExID: "VTO",
  brandLogo: "{{ !empty($userSettings->getFirstMedia('storetryonlogo'))? $userSettings->getFirstMedia('storetryonlogo')->getFullUrl() : '' }}",
}


$(document).ready(async function () {
  // $(".vto-loading").hide();
  // $(".preloader-wrap").fadeOut(300);
  Mode = mode.displayMode;
  View = mode.detailView;
  sizeMode();
  await getAllCameras();
  init(options);
  if(!isTesting){
    start();
  }
  Mode = mode.displayMode;
  View = mode.detailView;
  $(".vto-loading").hide();
  // if((Mode !== BigScreen && !$(".mobile").is(":visible")) || ( Mode === BigScreen && window.innerHeight < window.innerWidth )){
  //   featuresActivate();
  // }

  if(window.innerHeight < window.innerWidth){
    featuresActivate();
  }

  disableCategory = "{{$meta_data['tryon_settings']['tryon_ui_product_list_title']}}" == "catagory" ? false : true;
  window.loadCatagory();

  $(".preloader-wrap").fadeOut(300);

  if(typeof(umami) == 'undefined'){
    var s = document.createElement("script");
    s.type = "text/javascript";
    s.src = "https://stats.weavexr.com/umami.js";
    s.setAttribute('data-website-id',"768cbc7c-afdc-4d85-83ab-2b89ef2e3b67");
    $("head").append(s);
  }
  sizeMode();
})

$(window).resize(function() {
  sizeMode();
});

$(document).on('screenSelection', (e) =>{
  selectedScreen = e.detail.data;
})

$(document).on("tracking-switch", () => {
  if(Tracking === FaceTracking){
    showHideItem('watch', 'hide');
    layer_products.watch.tryon_url = "";
    visible_layers.watch.hidden = true;

    showHideItem('ring', 'hide');
    layer_products.ring.tryon_url = "";
    visible_layers.ring.hidden = true;

    showHideItem('bracelet', 'hide');
    layer_products.bracelet.tryon_url = "";
    visible_layers.bracelet.hidden = true;

    showHideItem('necklace', 'show');
    visible_layers.necklace.hidden = false;

    showHideItem('earring', 'show');
    visible_layers.earring.hidden = false;
  }else{
    showHideItem('watch', 'show');
    visible_layers.watch.hidden = false;

    showHideItem('ring', 'show');
    visible_layers.ring.hidden = false;

    showHideItem('bracelet', 'show');
    visible_layers.bracelet.hidden = false;

    showHideItem('necklace', 'hide');
    layer_products.necklace.tryon_url = "";
    visible_layers.necklace.hidden = true;

    showHideItem('earring', 'hide');
    layer_products.earring.tryon_url = "";
    visible_layers.earring.hidden = true;
  }
});

/* ----------------------------------------Anto Changes End---------------------------------- */

window.sizeMode = function () {
  if(window.innerHeight > window.innerWidth){
    $(".desktop").css("display", "none");
    $(".mobile").css("display", "block");
    $("#VTOtryonMobile").attr("id", "VTO");
    $("#preimgscrMobile").attr("id", "preimgscr");
    $("#preimgMobile").attr("id", "preimg");
    $(".product-modal").hide();
    // Mode == CamOnly ? $(".bottom-sheet-mobile").hide() : null;
    $(".bottom-sheet-top-indicator-div").hide();
    $(".portrait-top-header").show();
    var cameraContainerHeight = (window.innerHeight * 0.9) - $(".bottom-sheet-mobile").height() + 10;
    $("#VTO").css({"height": cameraContainerHeight + "px", "top": window.innerHeight * 0.09 + "px"});
    
    let bottomSheetContainerHeight = window.innerHeight -( $('.bottom-sheet-mobile').height() );
    $(".bottom-sheet-wrapper").css({ "height": `${ bottomSheetContainerHeight }px` ,"position": "absolute", "top": 0, "width": "100%" });
  }else{
    $(".desktop").css("display", "block");
    $(".mobile").css("display", "none");
    $("#VTOtryonDesktop").attr("id", "VTO");
    $(".camera-modal-card").hide();
    $(".camera-btn-icon").show();
    $("#preimgscrDesktop").attr("id", "preimgscr");
    $("#preimgDesktop").attr("id", "preimg");
    $("#VTO").css({"height": "100%", "top":"0px", "width": window.innerWidth - $(".product-modal").width() + "px", "left": "0px", "margin": "" });
    $(".product-modal-close-btn").hide();
    $(".product-modal").css({"height": "100%", "top": "0px", "right": "0px", "bottom": "0px", "border-radius": "0px"})
    $(".reset-btn-container").css("width", "100%");
    $(".camera-btn-container").css("width", "100%");
    $(".previous-next-btn").css("width", "100%");
  }

  if (View == DefaultDetailView) {
    DetailSideView = false;
    DetailsViewBackBtn = true;
  } else if (View == SingleDetailView) {
    DetailSideView = true;
    DetailsViewBackBtn = false;
    if ($(".mobile").is(":visible")) {
      $(".product-details-mob").show("slide", {
        direction: "down"
      }, 0);

      viewDetailsMobile();
    } else {
      $("#productDetailSideView").show();

      viewDetails();
    }
  } else if (View == SideDetailViewInit || View == SideDetailView) {
    DetailSideView = true;
    DetailsViewBackBtn = true;
  }

  //HammerJS

  try {
    if (SwipeActive) {
      var myElement = document.getElementById("VTO");

      myElement.onmouseup = () => {
        panGesture = "none";
      };

      myElement.ontouchend = () => {
        panGesture = "none";
      };

      var mc = new Hammer(myElement);

      mc.get("pan").set({
        direction: Hammer.DIRECTION_ALL,
        threshold: 100
      });

      // listen to events...
      mc.on("panleft panright panup pandown", function (ev) {
        // console.log(ev.type);
        let products = $(".mobile").is(":visible") ? Array.from($(".product-card-mob")) : Array.from($(".product-card-desktop"));
        // console.log("products", products)
        switch (ev.type) {
          case "panleft":
            if (panGesture != "panleft" && (products.length - 1) >= activeProductIndex) {
              activeProductIndex++;
              tryon(products[activeProductIndex]);
              // console.log(products,activeProductIndex, products[activeProductIndex]);
              panGesture = ev.type;
            }
            break;
          case "panright":
            if (panGesture != "panright" && activeProductIndex >= 0) {
              activeProductIndex--;
              tryon(products[activeProductIndex]);
              // console.log(activeProductIndex, products[activeProductIndex]);
              panGesture = ev.type;
            }
            break;
          case "panup":
            // console.log("Event Called")
            if ($(".mobile").is(":visible")) {
              $(".bottom-sheet-scroll")
                .stop()
                .animate({
                  scrollTop: $(".bottom-sheet-wrapper").height()
                }, 800, "swing");
            }
            break;
          case "pandown":
            if ($(".mobile").is(":visible")) {
              $(".product-details-mob").hide("slide", {
                direction: "down"
              }, 0);
              $(".bottom-sheet-scroll").stop().animate({
                scrollTop: 0
              }, 800, "swing");
            }
            break;
          default:
            break;
        }
      });
    }

  } catch (e) {

  }
}

function compareScreen ($this){
  let splitScreenIcon = $($this);
  umami('tryon-compare-screen-button-clicked');
  if(!isCompare){
    isCompare = true;
    multiCam();
    splitScreenIcon.attr("src", "{{ asset('XR/themes/tryonv4_03/img/split-screen-active.svg') }}");
    $(".previous-next-btn").hide();
    $(".compare-screen-toggle-btns").hide();
    if(trayActive){
      $(".toggle-layer-btn").click();
    }
  }else{
    isCompare = false;
    multiCamDisable();
    splitScreenIcon.attr("src", "{{ asset('XR/themes/tryonv4_03/img/split-screen.svg') }}")
    selectedScreen = "screen1";
    $(".previous-next-btn").css("display", "flex");
    $(".compare-screen-toggle-btns").css("display", "flex");
  }
}

function flipCamera(){
  if(activeCam === 0){
    switchCamera(1);
    activeCam = 1;
  }else{
    switchCamera(0);
    activeCam = 0;
  }
}

function flipCameraMobile(event){
  // umami('flip-camera-mobile-button-clicked');
  stopClickPropagation(event)
  flipCamera();
}

function captureBtnMobile(event){
  // umami('capture-button-mobile-clicked');
  stopClickPropagation(event);
  takePicture();
}

function sideTrayToggle(){
  $(".side-compare-tray-container").toggle("slide", {
      direction: "left",
    }, 800);
  if(!$(".side-tray-open-icon").is(":visible")){
    setTimeout(() => $(".side-tray-open-icon").toggle(), 800);
  }else{
    $(".side-tray-open-icon").toggle()
  }
}

function stopClickPropagation(event) {
  event.stopPropagation();
}

$(".settings-btn-icon").on("click", () => {
  $(".settings-pannel-container").fadeIn(500);
  $(".select-camera-dropdown-menu").empty();
  for(let i = 0; i < allCameras.length; i++){
    $(".select-camera-dropdown-menu").append(`<i class="dropdown-item" onclick="cameraSelected(${i})">${allCameras[i].deviceName}</i>`)
  }
  selectedCameraIndex === null ? $(".select-cam-dropdown-btn").text(allCameras[0].deviceName) : $(".select-cam-dropdown-btn").text(allCameras[selectedCameraIndex].deviceName) ;
  
})

function closeSettings(){
  $(".settings-pannel-container").fadeOut(500);
}

function cameraSelected(index){
  $(".select-cam-dropdown-btn").text(allCameras[index].deviceName);
  selectedCameraIndex = index;
  switchCamera(index);
}

async function getAllCameras() {
  var allDevices = await navigator.mediaDevices.enumerateDevices();
  for (const deviceInfo of allDevices) {
      if (deviceInfo.kind === 'videoinput') {
          allCameras.push({"deviceId": deviceInfo.deviceId, "deviceName": deviceInfo.label});
      }
  }
}

function prevNextProduct(type){
  let products = $(".mobile").is(":visible") ? Array.from($(".product-card-mob")) : Array.from($(".product-card-desktop"));
  switch (type) {
    case "next":
      if ( (products.length - 1) > activeProductIndex) {
        activeProductIndex++;
        tryon(products[activeProductIndex]);
      }
      break;
    case "previous":
      if (activeProductIndex > 0) {
        activeProductIndex--;
        tryon(products[activeProductIndex]);
      }
      break;
    default: break;
  }
}

function updateLayerProduct(){
  // jQuery.each(tryon_payload.product_category === "SET" ? set_products : layer_products, (i, val) => {
  jQuery.each(layer_products, (i, val) => {
    if(val.tryon_url == "") {
      $('#tray-'+i+'-card').css('display', 'none');
    }
    else{
      $('#tray-'+i+'-card').css('display', 'flex');
      $('#tray-'+i+'-card-img').attr('src', val.tryon_url );
      // if(tryon_payload.product_category === "SET"){
      //   $('#show-hide-icon-' + i).attr('src', visible_layers[i]['hidden'] ? "{{ asset('XR/themes/tryonv4_03/img/eye.svg') }}" : "{{ asset('XR/themes/tryonv4_03/img/eye-off.svg') }}" );
      //   $('#show-hide-btn-' + i)[0].childNodes[2].nodeValue = visible_layers[i]['hidden'] ? "Show" : "Hide";
      // }else{
        $('#show-hide-icon-' + i).attr('src', visible_layers[i]['hidden'] ? "{{ asset('XR/themes/tryonv4_03/img/eye.svg') }}" : "{{ asset('XR/themes/tryonv4_03/img/eye-off.svg') }}" );
        $('#show-hide-btn-' + i)[0].childNodes[2].nodeValue = visible_layers[i]['hidden'] ? "Show" : "Hide";
      // }
    }
  });
}

function removeLayer(layer_type){
    umami('tryon-remove-layer-button-clicked');
    switch(layer_type){
      case "necklace": 
        layer_products.necklace.tryon_url = "";  
        filterAddRemove('remove', {"product_category": "NECK"}); 
        visible_layers.necklace.hidden = false; break;
      case "earring": 
        layer_products.earring.tryon_url = ""; 
        filterAddRemove('remove', {"product_category": "EAR"}); 
        visible_layers.earring.hidden = false; break;
      case "watch": 
        layer_products.watch.tryon_url = "";  
        filterAddRemove('remove', {"product_category": "WATCH"}); 
        visible_layers.watch.hidden = false; break;
      case "ring": 
        layer_products.ring.tryon_url = "";  
        filterAddRemove('remove', {"product_category": "RING"}); 
        visible_layers.ring.hidden = false; break;
      case "bracelet": 
        layer_products.bracelet.tryon_url = ""; 
        filterAddRemove('remove', {"product_category": "BRACELET"}); 
        visible_layers.bracelet.hidden = false; break;
      default: break;
    }
  // }
  updateLayerProduct();
}

function showHideLayer(layer_type){
  // if(tryon_payload.product_category === "SET"){
  //   switch(layer_type){
  //   case "necklace": visible_layers.necklace.hidden = !visible_layers.necklace.hidden; break;
  //   case "earring": visible_layers.earring.hidden = !visible_layers.earring.hidden; break;
  //   case "watch": visible_layers.watch.hidden = !visible_layers.watch.hidden; break;
  //   case "ring": visible_layers.ring.hidden = !visible_layers.ring.hidden; break;
  //   case "bracelet": visible_layers.bracelet.hidden = !visible_layers.bracelet.hidden; break;
  //   default: break;
  // }
  //   $('#show-hide-icon-' + layer_type).attr('src', visible_layers[layer_type]['hidden'] ? "{{ asset('XR/themes/tryonv4_03/img/eye.svg') }}" : "{{ asset('XR/themes/tryonv4_03/img/eye-off.svg') }}" );
  //   $('#show-hide-btn-' + layer_type)[0].childNodes[2].nodeValue = visible_layers[layer_type]['hidden'] ? "Show" : "Hide";
  // }else{
  switch(layer_type){
    case "necklace": visible_layers.necklace.hidden = !visible_layers.necklace.hidden; break;
    case "earring": visible_layers.earring.hidden = !visible_layers.earring.hidden; break;
    case "watch": visible_layers.watch.hidden = !visible_layers.watch.hidden; break;
    case "ring": visible_layers.ring.hidden = !visible_layers.ring.hidden; break;
    case "bracelet": visible_layers.bracelet.hidden = !visible_layers.bracelet.hidden; break;
    default: break;
  }
    $('#show-hide-icon-' + layer_type).attr('src', visible_layers[layer_type]['hidden'] ? "{{ asset('XR/themes/tryonv4_03/img/eye.svg') }}" : "{{ asset('XR/themes/tryonv4_03/img/eye-off.svg') }}" );
    $('#show-hide-btn-' + layer_type)[0].childNodes[2].nodeValue = visible_layers[layer_type]['hidden'] ? "Show" : "Hide";
    visible_layers[layer_type]['hidden'] ? umami('tryon-show-button-clicked') : umami('tryon-hide-button-clicked');
  // }

  showHideItem(layer_type, visible_layers[layer_type]["hidden"] ? 'hide': 'show');
}

function hideAllLayers(){
  if(tryon_payload.product_category === "SET"){
    trayElements.forEach(elm => {
      if(!visible_layers[elm]["hidden"]){
        visible_layers[elm]["hidden"] = true;
        showHideItem(elm);
      }
    })
  }else{
    trayElements.forEach(elm => {
      if(!visible_layers[elm]["hidden"]){
        visible_layers[elm]["hidden"] = true;
        showHideItem(elm);
      }
    })
  }
}

function ActivateLayer($this){
  umami('tryon-active-layer-button-clicked');
  if(trayActive){
    trayActive = false;
    $(".side-compare-tray").hide();
    $("#toggle-btn-active").show();
    $("#toggle-btn-disable").hide();
  }else{
    trayActive = true;
    $(".side-compare-tray").show();
    $("#toggle-btn-active").hide();
    $("#toggle-btn-disable").show();
  }
  updateLayerProduct();
}

function layerProductsTryonReload(){
  let products = $(".mobile").is(":visible") ? Array.from($(".product-card-mob")) : Array.from($(".product-card-desktop"));
  
  products.forEach((elm, i) => {
    if(tryon_payload.product_category === "SET"){
      if( $(elm).find(".product-sku").val() === layer_products.necklace.sku ||
        $(elm).find(".product-sku").val() === layer_products.earring.sku ||
        $(elm).find(".product-sku").val() === layer_products.watch.sku ||
        $(elm).find(".product-sku").val() === layer_products.ring.sku ||
        $(elm).find(".product-sku").val() === layer_products.bracelet.sku){
          tryon(products[i]);
      }
    }
  });
  updateLayerProduct();
}

function resetLayerProducts(){
  layer_products = { 
    "necklace": { "category_name": "NECK", "tryon_url": "", "sku": "" },
    "earring": { "category_name": "EAR", "tryon_url": "", "sku": "" },
    "watch": { "category_name": "WATCH", "tryon_url": "", "sku": "" },
    "ring": { "category_name": "RING", "tryon_url": "", "sku": "" },
    "bracelet": { "category_name": "BRACELET", "tryon_url": "", "sku": "" }
  }
}

function faceHandTrackingSwitch(){
  if(Tracking === FaceTracking){
    showHideItem('watch', 'hide');
    visible_layers.watch.hidden = true;

    showHideItem('ring', 'hide');
    visible_layers.ring.hidden = true;

    showHideItem('bracelet', 'hide');
    visible_layers.bracelet.hidden = true;

    showHideItem('necklace', 'show');
    visible_layers.necklace.hidden = false;

    showHideItem('earring', 'show');
    visible_layers.earring.hidden = false;

  }else if(Tracking === HandTracking){
    showHideItem('necklace', 'hide');
    visible_layers.necklace.hidden = true;

    showHideItem('earring', 'hide');
    visible_layers.earring.hidden = true;

    showHideItem('watch', 'show');
    visible_layers.watch.hidden = false;

    showHideItem('ring', 'show');
    visible_layers.ring.hidden = false;

    showHideItem('bracelet', 'show');
    visible_layers.bracelet.hidden = false;
  }
}

function featuresActivate(){
  let prev_next_template = document.querySelector("#prev-next-template");
  let prev_next_clone = prev_next_template.content.cloneNode(true);
  $("#VTO").append(prev_next_clone);

  
  let side_tray_template = document.querySelector("#side-compare-template");
  let side_tray_clone = side_tray_template.content.cloneNode(true);
  $("#VTO").append(side_tray_clone);

  let footer_menu_template = document.querySelector("#footer-menu-template");
  let footer_menu_clone = footer_menu_template.content.cloneNode(true);
  $("#VTO").append(footer_menu_clone);
  
  trayElements.forEach(elm => {
    $(".side-tray-scroll").append(`<div class="card mt-1 mb-3 mx-2 align-items-center" id="tray-${elm}-card" style="display: none; width: 125px;border: 2px solid #BB97DE; border-radius: 15px;">
      <img class="my-2" src="{{ asset('XR/themes/tryonv4_03/img/blank.gif') }}" id="tray-${elm}-card-img" style="height:75px; width:100px; object-fit: contain;" >
      <button class="btn btn-primary float-left d-flex align-items-center mb-2" style="border-radius: 25px;font-size: 12px;padding: 2px 10px;width: fit-content;" id="show-hide-btn-${elm}" onclick="showHideLayer('${elm}')">
        <img src="{{ asset('XR/themes/tryonv4_03/img/eye-off.svg') }}" width="14" id="show-hide-icon-${elm}" style="margin-right:6px;" /> Hide
      </button>
      <svg class="position-absolute" width="14" height="14" viewBox="0 0 56 56" fill="none" xmlns="http://www.w3.org/2000/svg" style="top: -4px; right: -4px; cursor: pointer;" onclick="removeLayer('${elm}')" >
        <circle cx="28" cy="28" r="28" style="fill: var(--tryon_primary_color)"/>
        <path d="M39 18L19 38" stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
        <path d="M19 18L39 38" stroke="white" stroke-width="3" stroke-linecap="round" stroke-linejoin="round"/>
      </svg>
    </div>`);
  })

  trayActive ? updateLayerProduct(): $(".side-compare-tray").hide();
  
}

function applyTryon(){
  filterAddRemove('add', tryon_payload);
}

window.RequestData = function (id_name, category) {
  // Checking if the values are already loaded for a particular category.
  // console.log("RequestData", previous_id, id_name);
  category_id = category;
  if (previous_id !== id_name) {
    // console.log("RequestData loading1",previous_id, id_name);
    document.getElementById("search").value = "";
    query_string = "";
    let loadMorebtn = Array.from(document.getElementById(id_name).children);
    let loadMoreBtnPresent = (loadMorebtn.length !== 0) ? (loadMorebtn[loadMorebtn.length - 1].firstChild.textContent === "Load More" ? 1 : 0) : 0;
    if (document.getElementById(id_name).hasChildNodes()) {
      // console.log("RequestData loading2", previous_id, id_name);
      if (current_page !== Math.ceil((document.getElementById(id_name).childElementCount - loadMoreBtnPresent) / limit)) {
        //if the category already have products listed ----- then checing till which page the products are loaded
        current_page = Math.ceil((document.getElementById(id_name).childElementCount - loadMoreBtnPresent) / limit);
        page_loaded = current_page;
        previous_id = id_name;
        return;
      }
    } else {

      //When loading for the first time
      current_page = 1;
      page_loaded = 0;
    }
  } else {
    // console.log("RequestData loading3",previous_id, id_name);

    //To avoid loading when we already reached the bottom of the page
    if (current_page !== page_loaded + 1) {
      // console.log("RequestData loading3.1", previous_id, id_name);

      return;
    }
  }
  previous_id = id_name;
  var request_data;
  // console.log("RequestData loading4", previous_id, id_name, current_page, page_loaded, last_page);

  if (current_page !== page_loaded && View !== SingleDetailView && current_page <= last_page) {
    $(".loader-wrapper").show();
    // console.log("RequestData loading5", previous_id, id_name);

    //API Reques to get the products ----- filtered as per category_id
    $.ajax({
        url: api.endpoint +
          api.products +
          "&category_id=" +
          category_id +
          "&page=" +
          current_page +
          "&limit=" +
          limit +
          "&product_id_selected=" +
          product_id_selected +
          "&q=" +
          query_string,
        method: api.method,
        beforeSend: function (request) {
          if (api.headers != null)
            Object.keys(api.headers).forEach(function (header_key) {
              request.setRequestHeader(header_key, api.headers[header_key]);
            });
        },
        complete: function () {
          $(".loader-wrapper").hide();
        },
      })
      .done(function (data) {
        // JSON Data From API
        if (data.status === "success") {
          try {
            request_data = data;
            last_page = request_data.data["page"].last_page;
            
            // Selecting the Required components
            var template = document.querySelector("#product-card");
            var template_mob = document.querySelector("#product-card-mob");
            var product_body = document.getElementById(id_name);
            var product_body_mob = document.getElementById(id_name + "-mob");
            
            // Removing the OLD data (if any)
            if (
              (document.getElementById(id_name).childElementCount !== request_data.data["data"].length ||
                current_page <= request_data.data["page"].last_page) &&
              page_loaded !== current_page
            ) {
              if (previous_query !== query_string) {
                previous_query = query_string;
                clear_data = 0;
                if (document.getElementById(id_name).hasChildNodes()) {
                  Array.from(document.getElementById(id_name).children).forEach((element) => {
                    element.remove();
                  });
                }
                if (document.getElementById(id_name + "-mob").hasChildNodes()) {
                  Array.from(document.getElementById(id_name + "-mob").children).forEach((element) => {
                    element.remove();
                  });
                }
              }

              page_loaded = current_page;
              // Instiating template values for each product in API Data
              for (let i = 0; i < request_data.data["data"].length; i++) {
                //Storing raw image details data of all products in img_uls
                var img_urls = request_data.data["data"][i].imgUrls;
                var tryon_img_urls = request_data.data["data"][i].tryonImg;
                var meta_data = request_data.data["data"][i].meta_data;
                meta_data = (meta_data != null) ? JSON.parse(meta_data) : null;
                // Storing all the image_urls as a single string (note: delimiter = ",")
                var complete_img_url = "";
                for (let j = 0; j < img_urls.length; j++) {
                  complete_img_url += img_urls[j];
                  if (j < img_urls.length - 1) {
                    complete_img_url += ",";
                  }
                }
                // Cloning the template and updating the values
                var clone = template.content.cloneNode(true);
                var mobClone = template_mob.content.cloneNode(true);

                var product_name = clone.querySelector(".product-card-title");
                var product_sku = clone.querySelector(".product-sku");
                var product_img = clone.querySelector(".product-card-img");
                var image_urls = clone.querySelector(".img-urls");
                var tryon_img_neck = clone.querySelector(".tryonNeck");
                var tryon_img_apparel = clone.querySelector(".tryonApparel");
                var tryon_img_ear_l = clone.querySelector(".tryonEarL");
                var tryon_img_ear_r = clone.querySelector(".tryonEarR");
                var tryon_img_watch = clone.querySelector(".tryonWatch");
                var tryon_img_ring = clone.querySelector(".tryonRing");
                var tryon_img_bracelet = clone.querySelector(".tryonBracelet");
                var product_data = clone.querySelector(".product-details-data");
                // console.log(product_data);
                
                var product_name_mob = mobClone.querySelector(".product-card-title");
                var product_sku_mob = mobClone.querySelector(".product-sku");
                var product_img_mob = mobClone.querySelector(".product-card-img");
                var image_urls_mob = mobClone.querySelector(".img-urls");
                var tryon_img_neck_mob = mobClone.querySelector(".tryonNeck");
                var tryon_img_apparel_mob = mobClone.querySelector(".tryonApparel");
                var tryon_img_ear_l_mob = mobClone.querySelector(".tryonEarL");
                var tryon_img_ear_r_mob = mobClone.querySelector(".tryonEarR");
                var tryon_img_watch_mob = mobClone.querySelector(".tryonWatch");
                var tryon_img_ring_mob = mobClone.querySelector(".tryonRing");
                var tryon_img_bracelet_mob = clone.querySelector(".tryonBracelet");
                var product_data_mob = mobClone.querySelector(".product-details-data");

                if(meta_data != null) {
                  if(meta_data.tryon_settings != null) {
                      $(tryon_img_neck).attr('ppu',meta_data.tryon_settings.necklace_size);
                      $(tryon_img_neck).attr('xpos',meta_data.tryon_settings.necklace_h_position);
                      $(tryon_img_neck).attr('ypos',meta_data.tryon_settings.necklace_v_position);
                      $(tryon_img_ear_l).attr('ppu',meta_data.tryon_settings.ear_left_size);
                      $(tryon_img_ear_l).attr('xpos',meta_data.tryon_settings.ear_left_h_position);
                      $(tryon_img_ear_l).attr('ypos',meta_data.tryon_settings.ear_left_v_position);
                      $(tryon_img_ear_r).attr('ppu',meta_data.tryon_settings.ear_right_size);
                      $(tryon_img_ear_r).attr('xpos',meta_data.tryon_settings.ear_right_h_position);
                      $(tryon_img_ear_r).attr('ypos',meta_data.tryon_settings.ear_right_v_position);
                      $(tryon_img_watch).attr('ppu',meta_data.tryon_settings.watch_size);
                      $(tryon_img_watch).attr('xpos',meta_data.tryon_settings.watch_h_position);
                      $(tryon_img_watch).attr('ypos',meta_data.tryon_settings.watch_v_position);
                      $(tryon_img_ring).attr('ppu',meta_data.tryon_settings.ring_size);
                      $(tryon_img_ring).attr('xpos',meta_data.tryon_settings.ring_h_position);
                      $(tryon_img_ring).attr('ypos',meta_data.tryon_settings.ring_v_position);
                      $(tryon_img_apparel).attr('ppu',meta_data.tryon_settings.apparel_size);
                      $(tryon_img_apparel).attr('xpos',meta_data.tryon_settings.apparel_h_position);
                      $(tryon_img_apparel).attr('ypos',meta_data.tryon_settings.apparel_v_position);
                      $(tryon_img_bracelet).attr('ppu',meta_data.tryon_settings.bracelet_size);
                      $(tryon_img_bracelet).attr('xpos',meta_data.tryon_settings.bracelet_h_position);
                      $(tryon_img_bracelet).attr('ypos',meta_data.tryon_settings.bracelet_v_position);

                      $(tryon_img_neck_mob).attr('ppu',meta_data.tryon_settings.necklace_size);
                      $(tryon_img_neck_mob).attr('xpos',meta_data.tryon_settings.necklace_h_position);
                      $(tryon_img_neck_mob).attr('ypos',meta_data.tryon_settings.necklace_v_position);
                      $(tryon_img_ear_l_mob).attr('ppu',meta_data.tryon_settings.ear_left_size);
                      $(tryon_img_ear_l_mob).attr('xpos',meta_data.tryon_settings.ear_left_h_position);
                      $(tryon_img_ear_l_mob).attr('ypos',meta_data.tryon_settings.ear_left_v_position);
                      $(tryon_img_ear_r_mob).attr('ppu',meta_data.tryon_settings.ear_right_size);
                      $(tryon_img_ear_r_mob).attr('xpos',meta_data.tryon_settings.ear_right_h_position);
                      $(tryon_img_ear_r_mob).attr('ypos',meta_data.tryon_settings.ear_right_v_position);
                      $(tryon_img_watch_mob).attr('ppu',meta_data.tryon_settings.watch_size);
                      $(tryon_img_watch_mob).attr('xpos',meta_data.tryon_settings.watch_h_position);
                      $(tryon_img_watch_mob).attr('ypos',meta_data.tryon_settings.watch_v_position);
                      $(tryon_img_ring_mob).attr('ppu',meta_data.tryon_settings.ring_size);
                      $(tryon_img_ring_mob).attr('xpos',meta_data.tryon_settings.ring_h_position);
                      $(tryon_img_ring_mob).attr('ypos',meta_data.tryon_settings.ring_v_position);
                      $(tryon_img_apparel_mob).attr('ppu',meta_data.tryon_settings.apparel_size);
                      $(tryon_img_apparel_mob).attr('xpos',meta_data.tryon_settings.apparel_h_position);
                      $(tryon_img_apparel_mob).attr('ypos',meta_data.tryon_settings.apparel_v_position);
                      $(tryon_img_bracelet_mob).attr('ppu',meta_data.tryon_settings.bracelet_size);
                      $(tryon_img_bracelet_mob).attr('xpos',meta_data.tryon_settings.bracelet_h_position);
                      $(tryon_img_bracelet_mob).attr('ypos',meta_data.tryon_settings.bracelet_v_position);
                      
                  }
                   
                }

                product_name.textContent = request_data.data["data"][i].name;
                product_sku.value = request_data.data["data"][i].id;
                product_img.src = request_data.data["data"][i].thumbUrls[0];
                image_urls.value = complete_img_url;

                Object.entries(tryon_img_urls).forEach(([key, value]) => {
                  if (value != null)                  
                    Object.entries(value).forEach(([key1, value1]) => {
                      if (key1 == "neck") {
                        tryon_img_neck.value = value1;
                        tryon_img_neck_mob.value = value1;
                      }
                      if (key1 == "apparel") {
                        tryon_img_apparel.value = value1;
                        tryon_img_apparel_mob.value = value1;
                      }
                      if (key1 == "ear") {
                        tryon_img_ear_l.value = value1;
                        tryon_img_ear_l_mob.value = value1;
                      }
                      if (key1 == "ear") {
                        tryon_img_ear_r.value = value1;
                        tryon_img_ear_r_mob.value = value1;
                      }
                      if (key1 == "watch") {
                        tryon_img_watch.value = value1;
                        tryon_img_watch_mob.value = value1;
                      }
                      if (key1 == "ring") {
                        tryon_img_ring.value = value1;
                        tryon_img_ring_mob.value = value1;
                      }
                      if (key1 == "bracelet"){
                        tryon_img_bracelet.value = value1;
                        tryon_img_bracelet_mob.val = value1;
                      }
                    });
                });

                product_data.value = JSON.stringify(request_data.data["data"][i]);
                product_body.appendChild(clone);

                product_name_mob.textContent = request_data.data["data"][i].name;
                product_sku_mob.value = request_data.data["data"][i].id;
                product_img_mob.src = request_data.data["data"][i].thumbUrls[0];
                image_urls_mob.value = complete_img_url;
                product_data_mob.value = JSON.stringify(request_data.data["data"][i]);
                product_body_mob ? product_body_mob.appendChild(mobClone) : null
              }

              if (View == SideDetailViewInit && !$(".mobile").is(":visible")) {
                let productBodyArray = Array.from(product_body.children[0].children[0].children);
                productBodyArray[productBodyArray.length - 1].click();
                // console.log(productBodyMobArray[productBodyMobArray.length - 1]);
              } else if (View == SideDetailViewInit && $(".mobile").is(":visible")) {
                let productBodyMobArray = Array.from(product_body_mob.children[0].children[0].children);
                productBodyMobArray[productBodyMobArray.length - 1].click();
                // console.log(productBodyMobArray[productBodyMobArray.length - 1]);
              }

              let items = $(".mobile").is(":visible") ? Array.from($(".product-card-mob")) : Array.from($(".product-card-desktop"));
              items.forEach(function (item) {
                let selected_item_sku = $(item).find(".product-sku").attr("value");
                if(product_id_selected == selected_item_sku){
                  product_item_selected = item;
                }
              });

              //Creating the load more button if the page loaded is not the last one
              if (current_page < request_data.data["page"].last_page) {
                $(`<div class="load-more-btn-div col-12 text-center"><button class="btn btn-primary load-more-btn" onclick="loadMore('` + id_name + `','${category_id}')">Load More</button></div>`).appendTo("#" + id_name);
                $(`<div class="load-more-btn-div col-12 text-center load-more-btn-mob"><button class="btn btn-primary load-more-btn" onclick="loadMore('` + id_name + `','${category_id}')">Load More</button></div>`).appendTo("#" + id_name + "-mob");
                // console.log(id_name,current_page,request_data.data["page"].last_page);

              }
            }
          } catch (err) {
            console.log(err);
          }
        } else {
          $("#" + id_name).html(`<div class="col-12 text-center text-muted mt-4">No Product Found</div>`)
          $("#" + id_name + "-mob").html(`<div class="col-12 text-center text-muted mt-4">No Product Found</div>`)
          $("#" + id_name + "-mob").removeClass("flex-row")
        }
      })
      .fail(function (err) {
        // On API Request Fail
        console.log(err);
      });
  }
}

window.loadCatagory = function () {
  if (View !== SingleDetailView) {
    if ((api.catagory != undefined && api.catagory != "") && !disableCategory) {
      $(".loader-wrapper").show();
      $.ajax({
        url: api.endpoint + api.catagory,
        method: api.method,
        beforeSend: function (request) {
          if (api.headers != null)
            Object.keys(api.headers).forEach(function (header_key) {
              request.setRequestHeader(header_key, api.headers[header_key]);
            });
        },
        complete: function () {
          $(".loader-wrapper").hide();
        },
      }).done(function (data) {
        // console.log(data)
        try {
          // let active_indicator = true;

          $('<a class="nav-item nav-link docs-creator active" id="nav-all-tab" data-toggle="tab" href="#nav-all" role="tab" aria-controls="nav-all" aria-selected="true" onclick="RequestData("all-body", "")">All</a>').appendTo("#nav-tab");
          $('<div class="tab-pane active " id="nav-all"><div class="row" id="all-body"></div></div>').appendTo("#tab-content");
          $("#nav-tab a.active").trigger("click");

          $(`<a class="nav-item nav-link docs-creator active" id="nav-all-tab-mob" data-toggle="tab" href="#nav-all-mob" role="tab" aria-controls="nav-all-mob" aria-selected="true" onclick="RequestData('all-body', '')">All</a>`).appendTo("#nav-tab-mob");
          $('<div class="tab-pane active " id="nav-all-mob"><div class="row flex-row flex-nowrap" id="all-body-mob"></div></div>').appendTo("#tab-content-mob");
          $("#nav-tab-mob a.active").trigger("click");

          if (data.data.length !== 0) {
            data.data.forEach(function (category) {
              // let active = active_indicator ? "active" : "";
              // active_indicator = false;
              let active = "";
              $(
                '<a class="nav-item nav-link docs-creator text-capitalize ' +
                active +
                '" id="nav-' +
                category.name +
                '-tab" data-toggle="tab" href="#nav-' +
                category.name +
                '" role="tab" aria-controls="nav-' +
                category.name +
                '" aria-selected="true" onclick="RequestData(' +
                `'${category.name}-body',${category.id.toString()}` +
                ')" >' +
                category.name +
                "</a>"
              ).appendTo("#nav-tab");

              $(
                '<a class="nav-item nav-link docs-creator text-capitalize ' +
                active +
                '" id="nav-' +
                category.name +
                '-tab-mob" data-toggle="tab" href="#nav-' +
                category.name +
                '-mob" role="tab" aria-controls="nav-' +
                category.name +
                '" aria-selected="true" onclick="RequestData(' +
                `'${category.name}-body',${category.id.toString()}` +
                ')" >' +
                category.name +
                "</a>"
              ).appendTo("#nav-tab-mob");

              $(
                '<div class="tab-pane ' +
                active +
                '" id="nav-' +
                category.name +
                '"><div class="row" id="' +
                category.name +
                '-body"></div></div>'
              ).appendTo("#tab-content");
              $(
                '<div class="tab-pane ' +
                active +
                '" id="nav-' +
                category.name +
                '-mob"><div class="row flex-row flex-nowrap" id="' +
                category.name +
                '-body-mob"></div></div>'
              ).appendTo("#tab-content-mob");
            });
          }
          topNavScroll();
        } catch (err) {
          console.log(err);
        }
      });
    } else {
      $(`<a class="nav-item nav-link docs-creator active" id="nav-all-tab" data-toggle="tab" href="#nav-all" role="tab" aria-controls="nav-all" aria-selected="true" onclick="RequestData('all-body', '')">All</a>`).appendTo("#nav-tab");
      $('<div class="tab-pane active " id="nav-all"><div class="row" id="all-body"></div></div>').appendTo("#tab-content");
      $("#nav-tab a.active").trigger("click");

      $(`<a class="nav-item nav-link docs-creator active" id="nav-all-tab-mob" data-toggle="tab" href="#nav-all-mob" role="tab" aria-controls="nav-all-mob" aria-selected="true" onclick="RequestData('all-body', '')">All</a>`).appendTo("#nav-tab-mob");
      $('<div class="tab-pane active " id="nav-all-mob"><div class="row flex-row flex-nowrap" id="all-body-mob"></div></div>').appendTo("#tab-content-mob");
      $("#nav-tab-mob a.active").trigger("click");

    }
  }
}

window.showHideProductList = function ($this) {
  $("#modalProductBtn").toggle();
  $("#modalProductDetail").hide();
  $(".reset-btn-container").hide();
  $(".camera-btn-container").hide();
  if (Mode != CamFview && Mode != 'mode2' && Mode != 'mode4') {
    $(".camera-modal-card").toggle();
  }
  if($("#modalProduct").is(":visible")){
    $(".reset-btn-container").css("width", "100%");
    $(".camera-btn-container").css("width", "100%");
    $(".previous-next-btn").css("width", "100%");  
  }else{
    $(".reset-btn-container").css("width", "calc(65% - 20px)");
    $(".camera-btn-container").css("width", "calc(65% - 20px)");
    $(".previous-next-btn").css("width", "calc(65% - 20px)");  
  }
  $("#modalProduct").fadeToggle(500);
  $(".reset-btn-container").fadeIn(1000);
  $(".camera-btn-container").fadeIn(1000);
  $("#productDetailSideView").hide();
}

$(".product-details-mob").hide();

window.showHideDetailsMobile = function () {
  $(".product-details-mob").toggle(
    "slide", {
      direction: "down",
    },
    1000
  );
}

window.toggleDetailsSideView = function () {
  $("#productDetailSideView").toggle("slide", {
    direction: "right"
  }, 1000);
}

window.viewDetailsMobile = function ($this, event) {
  var data = JSON.parse($this.children[0].value);
  var  product_id = data.id;
  umami('tryon-view-details-button-clicked-mobile-sku-'+product_id);
  if (event != undefined)
    event.stopImmediatePropagation();
  $(".product-details-mob").show("slide", {
    direction: "down"
  }, 1000);
  if (!DetailsViewBackBtn) {
    $("#product-details-close-btn-mob").hide();
    $("#product-details-minimize-btn-mob").show();
  } else {
    $("#product-details-minimize-btn-mob").hide();
  }

  try {
    //Retriving the stringified JSON Data of the Particular Product
    product_details_data = View === SingleDetailView ? myoptions.initProduct : JSON.parse($this.children[0].value);
    // Select and Assign required components
    var product_title = document.querySelector(".product-modal-detail-title-mob");
    var product_price = document.querySelector(".product-modal-detail-price-mob");
    var product_details = document.querySelector(".product-modal-detail-product-details-mob");
    var carousel_body = document.querySelector(".product-details-carousel-image-group-mob");
    var carousel_thumbnail_body = document.querySelector(".product-carousel-thumbnail-mob");

    //Removing the Existing Carousel Images of previous product (if any)
    if (carousel_body.childElementCount != 0) {
      Array.from(carousel_body.children).forEach(function (caros_img) {
        caros_img.remove();
      });
    }
    var image_details = product_details_data.imgUrls;
    for (let i = 0; i < image_details.length; i++) {
      //Main Carousel Images
      let active_class = i == 0 ? "active" : "";
      $(
        '<div class="carousel-item product-detail-carousel-img-mob ' +
        active_class +
        '"><img class="img-fluid" src="' +
        image_details[i] +
        '" alt="Hills" /></div>'
      ).appendTo(".product-details-carousel-image-group-mob");
    }

    // Updating values for the product detail components
    product_title.innerHTML = product_details_data.name;
    product_price.innerHTML = (product_details_data.price != "" && product_details_data.price != null) ? "&#x20b9;" + product_details_data.price : "";
    product_details.innerHTML = product_details_data.description;
  } catch (err) {
    console.log(err);
  }
  window.parent.postMessage({
      action: "productDetail",
      msg: "",
      data: product_details_data,
    },
    "*"
  );
  amplitude.logEvent("Detail Product-" + product_details_data.id, {
    finished_flow: false,
  });
}

window.viewDetails = function ($this, event) {
  var data = JSON.parse($this.children[0].value);
  var  product_id = data.id;
  umami('tryon-view-details-button-clicked-sku-'+product_id);

  if (event != undefined)
    event.stopImmediatePropagation();
  if (!DetailSideView && View !== SingleDetailView) {
    //Slider Animation for details Card

    $("#modalProductDetail").show(
      "slide", {
        direction: "right",
      },
      1000
    );

    $(".product-modal").css("border-top-left-radius", "0.25rem");
    $(".product-modal").css("border-bottom-left-radius", "0.25rem");
    $(".product-modal-footer").css("border-bottom-left-radius", "0.25em");
    $(".product-modal-detail-footer").css("border-bottom-left-radius", "25px");

    try {
      //Retriving the stringified JSON Data of the Particular Product
      product_details_data = JSON.parse($this.children[0].value);

      // Select and Assign required components
      var product_title = document.querySelector(".product-modal-detail-title");
      var product_price = document.querySelector(".product-modal-detail-price");
      var product_details = document.querySelector(".product-modal-detail-product-details");
      var carousel_body = document.querySelector(".product-details-carousel-image-group");
      var carousel_thumbnail_body = document.querySelector(".product-details");

      //Removing the Existing Carousel Images of previous product (if any)
      if (carousel_body.childElementCount != 0) {
        Array.from(carousel_body.children).forEach(function (caros_img) {
          caros_img.remove();
        });
      }
      //Removing the Existing Carousel Thumbnail Images of previous product (if any)
      if (carousel_thumbnail_body.childElementCount != 0) {
        Array.from(carousel_thumbnail_body.children).forEach(function (caros_img) {
          caros_img.remove();
        });
      }

      var image_details = product_details_data.imgUrls;
      var thumbnails = product_details_data.thumbUrls;

      for (let i = 0; i < image_details.length; i++) {
        //Main Carousel Images
        let active_class = i == 0 ? "active" : "";
        $(
          '<div class="carousel-item product-detail-carousel-img ' +
          active_class +
          '"><img class="img-fluid" src="' +
          image_details[i] +
          '" alt="Hills" /></div>'
        ).appendTo(".product-details-carousel-image-group");

        // Carousel Thumbnail images
        $(
          '<li class="list-inline-item cursor-pointer ' +
          active_class +
          '"><a id="carousel-selector-' +
          i +
          '" class="selected" data-slide-to="' +
          i +
          '" data-target="#custCarousel"><img height="60px" src="' +
          thumbnails[i] +
          '" class="img-fluid" /></a></li>'
        ).appendTo(".product-details");
      }

      // Updating values for the product detail components
      product_title.innerHTML = product_details_data.name;
      product_price.innerHTML = (product_details_data.price != "" && product_details_data.price != null ) ? "&#x20b9;" + product_details_data.price : "";
      product_details.innerHTML = product_details_data.description;
    } catch (err) {
      console.log(err);
    }
  } else {
    $("#productDetailSideView").show(
      "slide", {
        direction: "right",
      },
      1000
    );
    DetailsViewBackBtn ? $("#product-modal-detail-side-back-btn").show() : $("#product-modal-detail-side-back-btn").hide();
    $(".product-modal").css("border-top-left-radius", "0.25rem");
    $(".product-modal").css("border-bottom-left-radius", "0.25rem");
    $(".product-modal-footer").css("border-bottom-left-radius", "0.25em");

    try {
      //Retriving the stringified JSON Data of the Particular Product
      product_details_data = View === SingleDetailView ? myoptions.initProduct : JSON.parse($this.children[0].value);

      var product_title = document.querySelector(".product-modal-detail-title-side");
      var product_price = document.querySelector(".product-modal-detail-price-side");
      var product_details = document.querySelector(".product-modal-detail-product-details-side");
      var carousel_body = document.querySelector(".product-details-carousel-image-group-side");
      var carousel_thumbnail_body = document.querySelector(".product-details-side");

      if (carousel_body.childElementCount != 0) {
        Array.from(carousel_body.children).forEach(function (caros_img) {
          caros_img.remove();
        });
      }
      //Removing the Existing Carousel Thumbnail Images of previous product (if any)
      if (carousel_thumbnail_body.childElementCount != 0) {
        Array.from(carousel_thumbnail_body.children).forEach(function (caros_img) {
          caros_img.remove();
        });
      }

      var image_details = product_details_data.imgUrls;
      var thumbnails = product_details_data.thumbUrls;

      for (let i = 0; i < image_details.length; i++) {
        //Main Carousel Images
        let active_class = i == 0 ? "active" : "";
        $(
          '<div class="carousel-item product-detail-carousel-img ' +
          active_class +
          '"><img class="img-fluid" src="' +
          image_details[i] +
          '" alt="Hills" /></div>'
        ).appendTo(".product-details-carousel-image-group-side");

        // Carousel Thumbnail images
        $(
          '<li class="list-inline-item cursor-pointer ' +
          active_class +
          '"><a id="carousel-selector-' +
          i +
          '" class="selected" data-slide-to="' +
          i +
          '" data-target="#custCarousel-side"><img height="60px" src="' +
          thumbnails[i] +
          '" class="img-fluid" /></a></li>'
        ).appendTo(".product-details-side");
      }
      $(".carousel-item").addClass("product-detail-carousel-img-side");
      // Updating values for the product detail components
      product_title.innerHTML = product_details_data.name;
      product_price.innerHTML = (product_details_data.price != "" && product_details_data.price != null) ? "&#x20b9;" + product_details_data.price : "";
      product_details.innerHTML = product_details_data.description;
    } catch (err) {
      console.log(err);
    }
  }

  amplitude.logEvent("Detail Product-" + product_details_data["id"], {
    finished_flow: false,
  });
  // console.log("view detail", product_details_data)
  window.parent.postMessage({
      action: "productDetail",
      msg: "",
      data: product_details_data
    },
    "*"
  );
}

window.tryon = function ($this, event) {
// console.log("tryon",$this);
  if(!trayActive){
    filterAddRemove("remove", tryon_payload, selectedScreen);
    resetLayerProducts();
  }
  // tryon_payload.product_category === "SET" ? filterAddRemove("remove", tryon_payload, selectedScreen):null;
  try {
    if (event != undefined)
      event.stopImmediatePropagation();
    var sku = $($this).find(".product-sku").val();
    var tryonImg = {};
    var tryonImgNeck = $($this).find(".tryonNeck").val();
    var tryonApparel = $($this).find(".tryonApparel").val();
    var tryonImgEarL = $($this).find(".tryonEarL").val();
    var tryonImgEarR = $($this).find(".tryonEarR").val();
    var tryonImgWatch = $($this).find(".tryonWatch").val();
    var tryonImgRing = $($this).find(".tryonRing").val();
    var tryonImgBracelet = $($this).find(".tryonBracelet").val();
    var product_detail_btn = $($this).find(".product-details-data");
    
    var tryonNeckPpu = $($this).find(".tryonNeck").attr("ppu");
    var tryonNeckXpos = $($this).find(".tryonNeck").attr("xpos");
    var tryonNeckYpos = $($this).find(".tryonNeck").attr("ypos");
    var tryonApparelPpu = $($this).find(".tryonApparel").attr("ppu");
    var tryonApparelXpos = $($this).find(".tryonApparel").attr("xpos");
    var tryonApparelYpos = $($this).find(".tryonApparel").attr("ypos");
    var tryonImgEarLPpu = $($this).find(".tryonEarL").attr("ppu");
    var tryonImgEarLXpos = $($this).find(".tryonEarL").attr("xpos");
    var tryonImgEarLYpos = $($this).find(".tryonEarL").attr("ypos");
    var tryonImgEarRPpu = $($this).find(".tryonEarR").attr("ppu");
    var tryonImgEarRXpos = $($this).find(".tryonEarR").attr("xpos");
    var tryonImgEarRYpos = $($this).find(".tryonEarR").attr("ypos");
    var tryonImgWatchPpu = $($this).find(".tryonWatch").attr("ppu");
    var tryonImgWatchXpos = $($this).find(".trytryon_payloadonWatch").attr("xpos");
    var tryonImgWatchYpos = $($this).find(".tryonWatch").attr("ypos");
    var tryonImgRingPpu = $($this).find(".tryonRing").attr("ppu");
    var tryonImgRingXpos = $($this).find(".tryonRing").attr("xpos");
    var tryonImgRingYpos = $($this).find(".tryonRing").attr("ypos");
    var tryonImgBraceletPpu = $($this).find(".tryonBracelet").attr("ppu");
    var tryonImgBraceletXpos = $($this).find(".tryonBracelet").attr("xpos");
    var tryonImgBraceletYpos = $($this).find(".tryonBracelet").attr("ypos");
    tryon_payload._id = sku;
    tryon_payload.product_id = sku;
    product_details_data = JSON.parse(product_detail_btn[0].value);
    
    if (tryonImgNeck != "" && tryonImgEarL != "" && tryonImgEarR != "") {
      tryon_payload.product_category = "SET",
      tryon_payload.neck_wearing = tryonImgNeck;
      tryon_payload.ear_wearing = tryonImgEarL;
      tryon_payload.thumbnail = tryonImgNeck;
           
      tryon_payload.ppu_ear = (tryonImgEarLPpu != null)?tryonImgEarLPpu : null,
      tryon_payload.x_ear = (tryonImgEarLXpos != null)?tryonImgEarLXpos : null,
      tryon_payload.y_ear = (tryonImgEarLYpos != null)?tryonImgEarLYpos : null,
      tryon_payload.ppu_neck = (tryonNeckPpu != null)?tryonNeckPpu : null,
      tryon_payload.x_neck = (tryonNeckXpos != null)?tryonNeckXpos : null,
      tryon_payload.y_neck = (tryonNeckYpos != null)?tryonNeckYpos : null,
      tryonImg = {
        neck: tryonImgNeck,
        ear: tryonImgEarL,
      };
      if(selectedScreen === "screen1"){
        layer_products.necklace.tryon_url = tryonImgNeck;
        layer_products.earring.tryon_url = tryonImgEarL;
        layer_products.necklace.sku = sku;
        layer_products.earring.sku = sku;
      }
      showHideItem("necklace", "show");
      showHideItem("earring", "show");

      layer_products.necklace.hidden = false;
      layer_products.earring.hidden = false;

      if(Tracking !== FaceTracking){
        Tracking = FaceTracking;
        $(document).trigger("tracking-switch");
      }
    } else if (tryonImgNeck != "" && tryonImgEarL != "") {
      tryon_payload.product_category = "SET"
      tryon_payload.neck_wearing = tryonImgNeck;
      tryon_payload.ear_wearing = tryonImgEarL;
      tryon_payload.thumbnail = tryonImgNeck;      
      tryon_payload.ppu_ear = (tryonImgEarLPpu != null)?tryonImgEarLPpu : null,
      tryon_payload.x_ear = (tryonImgEarLXpos != null)?tryonImgEarLXpos : null,
      tryon_payload.y_ear = (tryonImgEarLYpos != null)?tryonImgEarLYpos : null,
      tryon_payload.ppu_neck = (tryonNeckPpu != null)?tryonNeckPpu : null,
      tryon_payload.x_neck = (tryonNeckXpos != null)?tryonNeckXpos : null,
      tryon_payload.y_neck = (tryonNeckYpos != null)?tryonNeckYpos : null,
      tryonImg = {
        neck: tryonImgNeck,
        ear: tryonImgEarL,
      };
      if(selectedScreen === "screen1"){
        layer_products.necklace.tryon_url = tryonImgNeck;
        layer_products.earring.tryon_url = tryonImgEarL;
        layer_products.necklace.sku = sku;
        layer_products.earring.sku = sku;
      }

      showHideItem("necklace", "show");
      showHideItem("earring", "show");

      layer_products.necklace.hidden = false;
      layer_products.earring.hidden = false;

      if(Tracking !== FaceTracking){
        Tracking = FaceTracking;
        $(document).trigger("tracking-switch");
      }
    } else if (tryonImgNeck != "" && tryonImgEarR != "") {
      tryon_payload.product_category = "SET"
      tryon_payload.neck_wearing = tryonImgNeck;
      tryon_payload.ear_wearing = tryonImgEarR;
      tryon_payload.thumbnail = tryonImgNeck;
      tryon_payload.ppu_ear = (tryonImgEarRPpu != null)?tryonImgEarRPpu : null,
      tryon_payload.x_ear = (tryonImgEarRXpos != null)?tryonImgEarRXpos : null,
      tryon_payload.y_ear = (tryonImgEarRYpos != null)?tryonImgEarRYpos : null,
      tryon_payload.ppu_neck = (tryonNeckPpu != null)?tryonNeckPpu : null,
      tryon_payload.x_neck = (tryonNeckXpos != null)?tryonNeckXpos : null,
      tryon_payload.y_neck = (tryonNeckYpos != null)?tryonNeckYpos : null,
      tryonImg = {
        neck: tryonImgNeck,
        ear: tryonImgEarR,
      };
      if(selectedScreen === "screen1"){
        layer_products.necklace.tryon_url = tryonImgNeck;
        layer_products.earring.tryon_url = tryonImgEarR;
        layer_products.necklace.sku = sku;
        layer_products.earring.sku = sku;
      }
      showHideItem("necklace", "show");
      showHideItem("earring", "show");

      layer_products.necklace.hidden = false;
      layer_products.earring.hidden = false;

      if(Tracking !== FaceTracking){
        Tracking = FaceTracking;
        $(document).trigger("tracking-switch");
      }
    } else if (tryonImgNeck != "") {
      tryon_payload.product_category = "NECK"
      tryon_payload.neck_wearing = tryonImgNeck; 
      tryon_payload.thumbnail = tryonImgNeck;
      tryon_payload.ppu_neck = (tryonNeckPpu != null)?tryonNeckPpu : null,
      tryon_payload.x_neck = (tryonNeckXpos != null)?tryonNeckXpos : null,
      tryon_payload.y_neck = (tryonNeckYpos != null)?tryonNeckYpos : null,
      tryonImg = {
        neck: tryonImgNeck,
      };
      if(selectedScreen === "screen1"){
        layer_products.necklace.tryon_url = tryonImgNeck;
        layer_products.necklace.sku = sku;
      }
      showHideItem("necklace", "show");

      layer_products.necklace.hidden = false;

      if(Tracking !== FaceTracking){
        Tracking = FaceTracking;
        $(document).trigger("tracking-switch");
      }
    } else if (tryonImgEarL != "" && tryonImgEarR != "") {
      tryon_payload.product_category = "EAR"
      tryon_payload.ear_wearing = tryonImgEarL;
      tryon_payload.thumbnail = tryonImgEarL;
      tryon_payload.ppu_ear = (tryonImgEarLPpu != null)?tryonImgEarLPpu : null,
      tryon_payload.x_ear = (tryonImgEarLXpos != null)?tryonImgEarLXpos : null,
      tryon_payload.y_ear = (tryonImgEarLYpos != null)?tryonImgEarLYpos : null,
      tryonImg = {
        ear: tryonImgEarL,
      };
      if(selectedScreen === "screen1"){
        layer_products.earring.tryon_url = tryonImgEarL;
        layer_products.earring.sku = sku;
      }
      showHideItem("earring", "show");

      layer_products.earring.hidden = false;

      if(Tracking !== FaceTracking){
        Tracking = FaceTracking;
        $(document).trigger("tracking-switch");
      }
    } else if (tryonImgEarL != "") {
      tryon_payload.product_category = "EAR"
      tryon_payload.ear_wearing = tryonImgEarL;
      tryon_payload.thumbnail = tryonImgEarL;
      tryon_payload.ppu_ear = (tryonImgEarLPpu != null)?tryonImgEarLPpu : null,
      tryon_payload.x_ear = (tryonImgEarLXpos != null)?tryonImgEarLXpos : null,
      tryon_payload.y_ear = (tryonImgEarLYpos != null)?tryonImgEarLYpos : null,
      tryonImg = {
        ear: tryonImgEarL,
      };
      if(selectedScreen === "screen1"){
        layer_products.earring.tryon_url = tryonImgEarL;
        layer_products.earring.sku = sku;
      }
      showHideItem("earring", "show");

      layer_products.earring.hidden = false;

      if(Tracking !== FaceTracking){
        Tracking = FaceTracking;
        $(document).trigger("tracking-switch");
      }
    } else if (tryonImgEarR != "") {
      tryon_payload.product_category = "EAR"
      tryon_payload.ear_wearing = tryonImgEarR;
      tryon_payload.thumbnail = tryonImgEarR;
      tryon_payload.ppu_ear = (tryonImgEarRPpu != null)?tryonImgEarRPpu : null,
      tryon_payload.x_ear = (tryonImgEarRXpos != null)?tryonImgEarRXpos : null,
      tryon_payload.y_ear = (tryonImgEarRYpos != null)?tryonImgEarRYpos : null,
      tryonImg = {
        ear: tryonImgEarR,
      };
      if(selectedScreen === "screen1"){
        layer_products.earring.tryon_url = tryonImgEarR;
        layer_products.earring.sku = sku;
      }
      showHideItem("earring", "show");

      layer_products.earring.hidden = false;

      if(Tracking !== FaceTracking){
        Tracking = FaceTracking;
        $(document).trigger("tracking-switch");
      }
    } else if (tryonImgWatch != "" && tryonImgRing != "") {
      tryon_payload.product_category = "WATCH"
      tryon_payload.hand_wearing = tryonImgWatch;
      tryon_payload.thumbnail = tryonImgWatch;
      tryon_payload.ppu_hand = (tryonImgWatchPpu != null)?tryonImgWatchPpu : null
      tryon_payload.x_hand = (tryonImgWatchXpos != null)?tryonImgWatchXpos : null
      tryon_payload.y_hand = (tryonImgWatchYpos != null)?tryonImgWatchYpos : null
      tryonImg = {
        watch: tryonImgWatch,
        ring: tryonImgRing
      }
      if(selectedScreen === "screen1"){
        layer_products.watch.tryon_url = tryonImgWatch;
        layer_products.ring.tryon_url  = tryonImgRing;
        layer_products.watch.sku = sku;
        layer_products.ring.sku = sku;
      }
      showHideItem("watch", "show");
      showHideItem("ring", "show");

      layer_products.watch.hidden = false;
      layer_products.ring.hidden = false;

      if(Tracking !== HandTracking){
        Tracking = HandTracking;
        $(document).trigger("tracking-switch");
      }
    } else if (tryonImgWatch != "") {
      tryon_payload.product_category = "WATCH"
      tryon_payload.hand_wearing = tryonImgWatch;
      tryon_payload.thumbnail = tryonImgWatch;
      tryon_payload.ppu_hand = (tryonImgWatchPpu != null)?tryonImgWatchPpu : null
      tryon_payload.x_hand = (tryonImgWatchXpos != null)?tryonImgWatchXpos : null
      tryon_payload.y_hand = (tryonImgWatchYpos != null)?tryonImgWatchYpos : null
      tryonImg = {
        watch: tryonImgWatch,
      }
      if(selectedScreen === "screen1"){
        layer_products.watch.tryon_url = tryonImgWatch;
        layer_products.watch.sku = sku;
      }
      showHideItem("watch", "show");

      layer_products.watch.hidden = false;

      if(Tracking !== HandTracking){
        Tracking = HandTracking;
        $(document).trigger("tracking-switch");
      }
    } else if (tryonImgRing != "") {
      tryon_payload.product_category = "RING"
      tryon_payload.hand_wearing = tryonImgRing;
      tryon_payload.thumbnail = tryonImgRing;
      tryon_payload.ppu_hand = (tryonImgRingPpu != null)?tryonImgRingPpu : null
      tryon_payload.x_hand = (tryonImgRingXpos != null)?tryonImgRingXpos : null
      tryon_payload.y_hand = (tryonImgRingYpos != null)?tryonImgRingYpos : null
      tryonImg = {
        ring: tryonImgRing
      }
      if(selectedScreen === "screen1"){
        layer_products.ring.tryon_url = tryonImgRing;
        layer_products.ring.sku = sku;
      }
      showHideItem("ring", "show");

      layer_products.ring.hidden = false;

      if(Tracking !== HandTracking){
        Tracking = HandTracking;
        $(document).trigger("tracking-switch");
      }
    } else if(tryonImgBracelet !=""){
      tryon_payload.product_category = "BRACELET"
      tryon_payload.hand_wearing = tryonImgBracelet;
      tryon_payload.thumbnail = tryonImgBracelet;
      tryon_payload.ppu_hand = (tryonImgBraceletPpu != null)?tryonImgBraceletPpu : null
      tryon_payload.x_hand = (tryonImgBraceletXpos != null)?tryonImgBraceletXpos : null
      tryon_payload.y_hand = (tryonImgBraceletYpos != null)?tryonImgBraceletYpos : null
      if(selectedScreen === "screen1"){
        layer_products.bracelet.tryon_url = tryonImgBracelet;
        layer_products.bracelet.sku = sku;
      }
      showHideItem("bracelet", "show");

      layer_products.bracelet.hidden = false;

      if(Tracking !== HandTracking){
        Tracking = HandTracking;
        $(document).trigger("tracking-switch");
      }
    }
    else if (tryonApparel != "") {
      tryon_payload.product_category = "NECK"
      tryon_payload.neck_wearing = tryonApparel; 
      tryon_payload.thumbnail = tryonApparel;
      tryon_payload.ppu_neck = (tryonApparelPpu != null)?tryonApparelPpu : null,
      tryon_payload.x_neck = (tryonApparelXpos != null)?tryonApparelXpos : null,
      tryon_payload.y_neck = (tryonApparelYpos != null)?tryonApparelYpos : null,

      tryonImg = {
        neck: tryonApparel
      }
      if(selectedScreen === "screen1"){
        layer_products.necklace.tryon_url = tryonApparel;
        layer_products.necklace.sku = sku;
      }
      showHideItem("necklace", "show");

      layer_products.necklace.hidden = false;

      if(Tracking !== FaceTracking){
        Tracking = FaceTracking;
        $(document).trigger("tracking-switch");
      }
    }

    faceHandTrackingSwitch();
    // if((Mode !== BigScreen && !$(".mobile").is(":visible")) || ( Mode === BigScreen && window.innerHeight < window.innerWidth )) updateLayerProduct();
    
    filterAddRemove("add", tryon_payload, selectedScreen);
    //removing borders from other cards
    // console.log("tryon",tryon_payload,selectedScreen);

    let products = $(".mobile").is(":visible") ? Array.from($(".product-card-mob")) : Array.from($(".product-card-desktop"));

    // alert($this);
    // alert(products[0])

    products.forEach((card, i) => {
      $(card).css("border", "2px solid var(--tryon_primary_color)");
      if (card == $this || $(card) == $this) {
        $(card).css("border", "2px solid var(--tryon_tertiary_color)")
        activeProductIndex = i;
      }
    })

    //Giving border to the selected Card
    $($this).css("border", "2px solid var(--tryon_tertiary_color)"); //more efficient
    if ($("#modalProductDetail").is(":visible") || $("#productDetailSideView").is(":visible") || $(".product-details-mob").is(":visible")) {
      if ($this.children) {
        let cardChildren = Array.from($this.children);
        if ($(".mobile").is(":visible")) {
          viewDetailsMobile(cardChildren[cardChildren.length - 1], event);
        } else {
          viewDetails(cardChildren[cardChildren.length - 1], event);
        }
      }
    } else {
      window.parent.postMessage({
          action: "tryon",
          msg: "",
          data: product_details_data,
        },
        "*"
      );
    }
  } catch (e) {
    console.log(e)
  }

}

window.addtocart = function ($this, event) {
  var cartId =product_details_data.id;
  umami('tryon-add-to-cart-button-clicked-sku-'+cartId);
  
  $($this).hide();
  $($this).siblings('.remove-from-cart-btn').show();
  window.parent.postMessage({
      action: "addtocart",
      msg: "",
      data: product_details_data,
    },
    "*"
  );
}

window.removefromcart = function ($this, event) {
  var cartId =product_details_data.id;
  // alert(cartId);
  umami('tryon-remove-from-cart-button-clicked-sku-'+cartId);
  $($this).hide();
  $($this).siblings('.add-to-cart-btn').show();
  window.parent.postMessage({
      action: "removefromcart",
      msg: "",
      data: product_details_data,
    },
    "*"
  );
}

$("#modalProductDetail").hide();

window.closeproductDetail = function () {
  $("#modalProductDetail").hide(
    "slide", {
      direction: "right",
    },
    1000
  );
  if (Mode == CamFview) {
    $(".product-modal").css("border-top-left-radius", "25px");
    $(".product-modal").css("border-bottom-left-radius", "25px");
    $(".product-modal-footer").css("border-bottom-left-radius", "25px");
    $(".product-modal-detail-footer").css("border-bottom-left-radius", "25px");
  }
}

window.topNavScroll = function () {
  // duration of scroll animation
  var scrollDuration = 300;
  // paddles
  var leftPaddle = document.getElementsByClassName("left-paddle");
  var rightPaddle = document.getElementsByClassName("right-paddle");
  // get items dimensions
  var navItems = $(".nav-item");
  var menuSize = 0;
  for (let i = 0; i < navItems.length; i++) {
    menuSize += navItems[i].offsetWidth;
  }

  var itemsLength = $(".nav-item").length;
  var itemSize = $(".nav-item").outerWidth(true);
  // get some relevant size for the paddle triggering point
  var paddleMargin = 10;

  // get wrapper width
  var getMenuWrapperSize = function () {
    return $(".menu-wrapper").outerWidth();
  };
  var menuWrapperSize = getMenuWrapperSize();
  // console.log(menuWrapperSize);
  // the wrapper is responsive
  $(window).on("resize", function () {
    menuWrapperSize = getMenuWrapperSize();
  });
  // size of the visible part of the menu is equal as the wrapper size
  var menuVisibleSize = menuWrapperSize;

  // get total width of all menu items
  var getMenuSize = function () {
    return itemsLength * itemSize;
  };
  // var menuSize = getMenuSize();
  // get how much of menu is invisible
  var menuInvisibleSize = menuSize - menuWrapperSize;

  // get how much have we scrolled to the left
  var getMenuPosition = function () {
    return $(".nav-tabs").scrollLeft();
  };

  // finally, what happens when we are actually scrolling the menu
  $(".nav-tabs").on("scroll", function () {
    // get how much of menu is invisible
    menuInvisibleSize = menuSize - menuWrapperSize;
    // get how much have we scrolled so far
    var menuPosition = getMenuPosition();

    var menuEndOffset = menuInvisibleSize - paddleMargin;

    // show & hide the paddles
    // depending on scroll position
    if (menuPosition <= paddleMargin) {
      // $(leftPaddle).addClass('hidden');
      // $(rightPaddle).removeClass('hidden');
    } else if (menuPosition < menuEndOffset) {
      // show both paddles in the middle
      // $(leftPaddle).removeClass('hidden');
      // $(rightPaddle).removeClass('hidden');
    } else if (menuPosition >= menuEndOffset) {
      // $(leftPaddle).removeClass('hidden');
      // $(rightPaddle).addClass('hidden');
    }

    // print important values
    $("#print-wrapper-size span").text(menuWrapperSize);
    $("#print-menu-size span").text(menuSize);
    $("#print-menu-invisible-size span").text(menuInvisibleSize);
    $("#print-menu-position span").text(menuPosition);
  });

  var offsetSize = 0;

  // scroll to left
  $(rightPaddle).on("click", function (event) {
    event.stopPropagation()
    offsetSize = offsetSize < menuSize ? offsetSize + menuSize / 3 : offsetSize;
    $(".nav-tabs").animate({
        scrollLeft: offsetSize,
      },
      scrollDuration
    );
  });

  // scroll to right
  $(leftPaddle).on("click", function (event) {
    event.stopPropagation()
    offsetSize = offsetSize > 0 ? offsetSize - menuSize / 3 : offsetSize;
    $(".nav-tabs").animate({
        scrollLeft: offsetSize,
      },
      scrollDuration
    );
  });
}

//Search Bar
var search_form = document.getElementById("search-form");
var search_input_field = document.getElementById("search");

var search_form_mob = document.getElementById("search-form_mob");
var search_input_field_mob = document.getElementById("search_mob");

//Search Bar on submit funtion
window.submitted = function (event) {
  event.preventDefault();
  event.stopPropagation();
  query_string = search_input_field.value;
  // console.log(document.getElementById("search").value);
  if (previous_query !== query_string) {
    current_page = 1;
    page_loaded = 0;
    RequestData(previous_id, category_id);
  }
}

window.submitted_mob = function (event) {
  event.preventDefault();
  event.stopPropagation();
  query_string = search_input_field_mob.value;
  // console.log(document.getElementById("search").value);
  if (previous_query !== query_string) {
    current_page = 1;
    page_loaded = 0;
    RequestData(previous_id, category_id);
  }
}

search_form.addEventListener("submit", submitted);
search_form_mob.addEventListener("submit", submitted_mob)

//load More buton
window.loadMore = function (id_name, category_id) {
  var product_body_mob = document.getElementById(id_name + "-mob");
  let LoadMoreBtnMob = Array.from(product_body_mob.children);
  var product_body = document.getElementById(id_name);
  // console.log(LoadMoreBtnMob);
  if (LoadMoreBtnMob[LoadMoreBtnMob.length - 1].firstChild.textContent === "Load More") {
    LoadMoreBtnMob[LoadMoreBtnMob.length - 1].remove();
  }
  if (product_body.hasChildNodes()) {
    let LoadMoreBtn = Array.from(product_body.children);
    if (LoadMoreBtn[LoadMoreBtn.length - 1].firstChild.textContent === "Load More") {
      LoadMoreBtn[LoadMoreBtn.length - 1].remove();
    }
  }
  current_page++;
  RequestData(id_name, category_id);
}

//Scroll to load more added
$(".product-modal-body").scroll(function () {
  const CardBorderWidth = 0.5;
  if ($(".product-modal-body").scrollTop() > $(".tab-content").height() - $(".product-modal-body").height() - CardBorderWidth) {
    // loadMore(previous_id, category_id);
  }
});

$(".product-modal-body-mob").scroll(function () {
  // console.log("hello")
  // console.log($(".product-modal-body-mob").scrollLeft(), $(".tab-content-mob").width());
});

$(".bottom-sheet-scroll").scroll(function () {
  if($(".bottom-sheet-scroll").scrollTop() === 0){
    $("#drop-down-indicator").attr("src", "{{ asset('XR/themes/tryonv4_03/img/dropdown_up.svg') }}")
  }else{
    $("#drop-down-indicator").attr("src", "{{ asset('XR/themes/tryonv4_03/img/dropdown_down.svg') }}")
  }
})

$(".bottom-sheet-scroll").scrollTop($(".bottom-sheet-wrapper").height());

window.ShowHideBottomSheet = function (event) {
  stopClickPropagation(event)
  if (View !== SingleDetailView) {
    $(".bottom-sheet-scroll").animate({
      scrollTop: $(".bottom-sheet-scroll").scrollTop() === 0 ? $(".bottom-sheet-wrapper").height() : 0,
    });
  } else {
    $(".bottom-sheet-scroll").animate({
      scrollTop: $(".bottom-sheet-scroll").scrollTop() === 0 ? $(".bottom-sheet-wrapper").height() : 50,
    });
    $(".product-details-mob").show("slide", {
      direction: "down"
    }, 200);
    $("#nav-paddles-mob").hide();
  }
}

window.closeBottomSheet = function() {
  $("#drop-down-indicator").attr("src", "{{ asset('XR/themes/tryonv4_03/img/dropdown_up.svg') }}");
  $(".bottom-sheet-scroll").animate({scrollTop: 0});
}

window.minimizeDetailsMob = function () {
  $(".product-details-mob").hide("slide", {
    direction: "down"
  }, 0);
  $(".bottom-sheet-scroll").scrollTop(0);
  $("#nav-paddles-mob").hide();
  $(".bottom-sheet-wrapper").css("height", "calc(100% - 70px)");

}
//events tracking functions
    
    $("#zoomIn").click(function(){
      umami('tryon-zoom-in-button-clicked');
    }
    );
    $("#zoomOut").click(function(){
      umami('tryon-zoom-out-button-clicked');
    }
    );
</script>

</html>