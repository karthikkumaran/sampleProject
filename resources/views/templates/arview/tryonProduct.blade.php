
<!DOCTYPE html>
<html class="loaded" lang="en" data-textdirection="ltr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $meta_data['splash']['brand_name'] ?? trans('panel.site_title') }}</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/crypto-js.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/themes/jewelar/css/desktop.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-65PERJ2GMK"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-65PERJ2GMK');
    </script>
      @if(!empty($meta_data['analytic_settings']['google_analytics_id']))
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-{{$meta_data['analytic_settings']['google_analytics_id']}}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '{{$meta_data['analytic_settings']['google_analytics_id']}}');
        </script>
      @endif
    @if(!empty($meta_data['custom_script_settings']['custom_script_header']))
    {!! $meta_data['custom_script_settings']['custom_script_header'] !!}
    @endif

    @php

if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')   
         $url = "https://";   
    else  
         $url = "http://";   
    // Append the host(domain name, ip) to the URL.   
    $url.= $_SERVER['HTTP_HOST'];   
    
    // Append the requested resource location to the URL   
    $url.= $_SERVER['REQUEST_URI'];   
    $components = parse_url($url);
    parse_str($components['query'], $results);
    
    $path = explode('/',$components['path']);
    \Debugbar::disable();
  
@endphp
<style>
  div#MirrAR-modal {
    z-index: inherit;
}
.share_btn{
  margin: 0;
  position: absolute;
  top:90%;
  left: 30%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
  z-index: 9;
}
@media (max-width: 576px) {
  .shareopt {
          margin: 0;
          position: absolute;
          top: 70%;
          margin-top: 5px;
          left: 15%;
          max-width : 370px;
          background-color: white;
          -ms-transform: translateY(-50%);
          transform: translateY(-50%);
          z-index: 9;
  }
}
@media (min-width: 576px){
      .vertical-center {
          margin: 0;
          position: absolute;
          top: 75%;
          margin-top: 5px;
          left: 25%;
          max-width : 370px;
          background-color: white;
          -ms-transform: translateY(-50%);
          transform: translateY(-50%);
          z-index: 9;
        }
}
    </style>
  </head>
  <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N67BLVX" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @if(!empty($meta_data['analytic_settings']['google_analytics_gtm_id']))
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{$meta_data['analytic_settings']['google_analytics_gtm_id']}}" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    @endif
    @php 
      $photo = $initialProduct->product->photo;
      $img_url = array();
      $img_thumb = array();
      $obj_neck = array();
      $obj_ear = array();
      $obj_ring = array();
      $obj_watch = array();
        foreach($photo as $key => $value){
          $img = $initialProduct->product->photo[$key]->getFullUrl();
          array_push($img_url, $img);

          $thumb = $initialProduct->product->photo[$key]->getFullUrl('thumb');
          array_push($img_thumb, $thumb);

        }

        $arobject = $initialProduct->arobject;
        foreach($arobject as $key => $value) {    
            if($arobject[$key]->position == "jwl-nck") {
                $neck = $arobject[$key]->object->getFullUrl();
            } else if($arobject[$key]->position == "jwl-ear-l"){
                $ear = $arobject[$key]->object->getFullUrl();
            } else if($arobject[$key]->position == "jwl-ear-r"){                
                $ear = $arobject[$key]->object->getFullUrl();
            } else if($arobject[$key]->position == "ring"){                
                $ring = $arobject[$key]->object->getFullUrl();
            } else if($arobject[$key]->position == "watch"){                
                $watch = $arobject[$key]->object->getFullUrl();
            } else if($arobject[$key]->position == "apparel"){                
                $apparel = $arobject[$key]->object->getFullUrl();
            } 
        }
        
    @endphp
   
  <div class="div ">
    <div class="share_btn "><a href="#demo" class="btn btn-primary "  data-toggle="collapse"><b><i class="fa fa-share-alt" aria-hidden="true"></i></b>share</a></div>
        <div id="demo" class="collapse ">
          <div class="panel-group" id="accordion" style=" background-color: #e4eef5;border-radius: 20px;">
            <div class="panel panel-default">
                <div id="share" class="panel-collapse collapse in" style=" background-color: rgb(255, 255, 255);border-top-right-radius: 20px;border-top-left-radius: 20px;">
                <div class="container rounded vertical-center shareopt pt-2" style=" padding-bottom: 10px;">
                <div class="row">
                      <div class="col-sm">
                        
                      </div>
                      <div class="col-sm float-right">
                      <h3>Share</h3>
                      </div>
                      <div class="col-sm float-right p-1">
                      <a data-toggle="collapse" style="color:black;" class="float-right"  href="#demo" ><i class="fa fa-times fa-2x" aria-hidden="true"></i>
                                          </a>
                      </div>
                    </div>

                    <div class="clesrfix"></div>
                      <a class="btn btn-lg btn-social btn-facebook bstyle w-100" href="#"
                          onclick='share("fb","")'>
                          <i class="fa fa-facebook fa-fw"></i> Share on Facebook
                      </a><br>
                      <a class="btn btn-lg btn-social btn-whatsapp bstyle w-100" href="#"
                          onclick='share("whatsapp","")'>
                          <i class="fa fa-whatsapp fa-fw"></i> Share on whatsapp
                      </a><br>
                      <a class="btn btn-lg btn-social btn-twitter bstyle w-100" href="#"
                          onclick='share("twitter","")'>
                          <i class="fa fa-twitter fa-fw"></i> Share on twitter
                      </a><br>
                      <a class="btn btn-lg btn-social btn-twitter bstyle w-100" href="#"
                          onclick='share("copy","")'>
                          <i class="fa fa-copy fa-fw"></i> Copy Share Link
                      </a><br>
                      

                  </div>
                </div>  
            </div>
          </div>
        </div>
    </div>
  </body>
  <script src="https://cdn.mirrar.co/v3.0.1/js/init.min.js"></script>
  <script>
    
        $('.share_btn').css('display','none');
        var share_url = "";
        var  product_id_selected = '{{$initialProduct->product->id}}';
    (function ($) {
      "use strict";
      $(document).ready(function () {

        document.addEventListener("tryon", (data) => {
          // alert(JSON.stringify(data.detail.data));
          product_id_selected = data.detail.data.id;
        window.history.replaceState(null, null, "?sku="+data.detail.data.id);
        });
    
        
        document.addEventListener("takephoto", (data) => {
          var selectedProducts = {
              "photo": data.detail.data,
              "cid": "{{$campaign->id}}",
              "pid": product_id_selected,
              "_token": "{{ csrf_token() }}",
          };
          
          $.ajax({
            type: "POST",
            url: "{{ route('captures.store') }}",
            data: selectedProducts,
            success: function (data) {
                share_url = data;
                $('.collapse').collapse();
                
            }, error: function(data){
              console.log(JSON.stringify(data));
            }
          });
        })
        var options = {
          key: "{{$meta_data['tryon_settings']['tryon_key'] ?? ''}}",
          mode: {
            displayMode: "{{$meta_data['tryon_settings']['tryon_mode'] ?? ''}}",
            detailView: "{{$meta_data['tryon_settings']['tryon_product_detail_mode'] ?? ''}}",
          },
          api: {
            endpoint: "https://simplisell.co/api/v1",
            method: "GET",
            data: {},
            @if(isset($meta_data['tryon_settings']['tryon_ui_product_list_title'])) 
              @if($meta_data['tryon_settings']['tryon_ui_product_list_title'] == "catagory") 
                 catagory: "/public/product-categories",
              @else
                  catagory: '',
              @endif
            @endif
            products: "/public/products?link={{$path[2]}}",
            searchParam: "q",
            // headers: { Authorization:  "Bearer 25|3K53z5e3h91e691ujZl4oBIp0U7pm3nwC2IOZVvY"  },
          },
          initProduct: {
            id: "{{$initialProduct->product->id}}",
            name: "{{$initialProduct->product->name}}",
            sku: "{{$initialProduct->product->sku}}",
            imgUrls: <?php echo json_encode($img_url); ?>,
            thumbUrls: <?php echo json_encode($img_thumb); ?>,
            description:'{!! $initialProduct->product->description !!}',
            price: "{{$initialProduct->product->price}}",
            tryonImg: { neck: "{{$neck ?? ''}}", apparel: "{{$apparel ?? ''}}",
                ear: "{{$ear ?? ''}}", ring: "{{$ring ?? ''}}", watch: "{{$watch ?? ''}}"},
        },
          isFloatingBtn: {{$meta_data['tryon_settings']['tryon_floating_mode'] ?? 'false'}}, //default false
        };
        init(options);
        start();
      });
    
                    document.addEventListener('closePreview', ()=>{
                        // $('.share_btn').css('display','none');
                        $('#demo').collapse('hide');
                        // console.log("Start event triggered closePreview")
                    });
                    document.addEventListener('downloadImage', ()=>{
                        // $('.share_btn').css('display','none');
                        $('#demo').collapse('hide');
                        // console.log("Start event triggered downloadImage")
                    });
                    document.addEventListener('shareImage', ()=>{
                        $('#demo').collapse('show');
                        // console.log("Start event triggered shareImage")
                    });
                    document.addEventListener('CloseTryon', ()=>{
                        history.back();
                    });
    
    })(jQuery);
   
  

  

            
function share(share_name,msg) {
      // var u="{{Request::fullUrl()}}";
      var u=share_url;
    var t="I tried this jewellery on me using MirrAR Virtual TryOn. I recommend you to give it a Try. Click on the Try Now button and Allow Camera access to experience the design. @mirrarofficial";
    var hashtags = "MirrAR";
    switch (share_name) {
        case 'fb':
            window.open('http://www.facebook.com/sharer.php?u='+encodeURIComponent(u)+'&t='+encodeURIComponent(t +" #"+ hashtags),'sharer','toolbar=0,status=0,width=626,height=436');
             break;
        case 'whatsapp':
            var message = encodeURIComponent(u) + " " + t + " #" +hashtags;
            // var whatsapp_url = "web.whatsapp://send?text=" + message;
            var whatsapp_url = "whatsapp://send?text=" + message;
            window.location.href = whatsapp_url;
            break;
        case 'twitter':
            window.open("https://twitter.com/intent/tweet?text="+t+"&url="+u+"&hashtags="+hashtags,'sharer','toolbar=0,status=0,width=626,height=436')
            break;
        case 'copy':
            var $temp = $("<input>");
            $("#share").append($temp);
            $temp.val(u);
            $temp.select();
            document.execCommand("copy");
            $temp.remove();
            $('#demo').collapse('hide');
            $('#MirrAR_preview').hide();
            toastr.success('Share Link', 'Copied!');
            break;
    }
  }
  </script>
   @if(!empty($meta_data['custom_script_settings']['custom_script_footer']))
    {!! $meta_data['custom_script_settings']['custom_script_footer'] !!}
    @endif
</html>