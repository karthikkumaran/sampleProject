@extends('layouts.cam')
@section('styles')
@parent
  <!-- Google Fonts -->
  <meta property="og:image" content="{{$capture->photo->getUrl()}}"/>
<link href='https://fonts.googleapis.com/css?family=Capriola' rel='stylesheet'>
<link rel="stylesheet" type="text/css" href="{{ asset('XR/themes/sharescreen/css/style.css') }}">

@endsection
@section('content')

<section id="hero">
  <div class="card" style="-webkit-box-shadow: 0px 0px 50px 0px #e020595e;
    -moz-box-shadow: 0px 0px 50px 0px #e020595e;box-shadow: 0px 0px 50px 0px #e020595e;
      border: 0.5px solid #e02059;">

      <div style="position: relative">
        <img src="{{ $capture_img }}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}'" class="tryon-image" alt="..." />
        <a onclick='onShare()'>
          <img src="{{ asset('XR/themes/sharescreen/img/share.svg') }}" height="30px" class="share-icon" />
        </a> 
        <a onclick='downloadTakePicture()'>
          <img src="{{ asset('XR/themes/sharescreen/img/download.svg') }}" height="30px" class="share-icon" style="bottom: 10px; top: unset;" />
        </a>
        {{-- <div class="store-container">
          <img src="{{ $logo_url }}" onerror="this.onerror=null;this.style='display:none'" class="logo-image img-fluid mb-1">
          <a href="{{$store_url}}" class="visit-store-button"><img src="{{ asset('XR/themes/sharescreen/img/store.svg') }}" height="20px"
              style="padding-right: 10px" />Visit Store</a>
        </div> --}}
      </div>

      <div class="row" style="margin: 1px; padding: 0.5rem">
        <div class="col-4 product-image-container">
          <img src="{{ $product_img }}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}'" class="product-img img-fluid" alt="..." />
        </div>
        <div class="col-8 product-details">
          <div class="row product-title">{{ $product->name }}</div>
          <div class="row product-shop-name">by {{ $store_info["storefront"]["storename"] }}</div>
          <div class="row tryon-button-container" style="margin-top: 0.5rem">
            <div class="col-auto p-0 m-0 mt-1">
              <a href="{{$tryon_link}}" class="tryon-button  text-center" style="width: 120px;">
                <img src="{{ asset('XR/themes/sharescreen/img/camera.svg') }}" height="20px" style="padding-right: 10px" />Try On</a>
            </div>
            <div class="col-auto p-0 m-0 mt-1"><a href="{{$product_details_link}}" class="view-details-button text-center" style="width: 120px;">View Details</a></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


@endsection
@section('scripts')
@parent
<script> 
  setTimeout(removeLoader, 3000);
  function downloadTakePicture() {
        const link = document.createElement('a')
        link.setAttribute('href', '{{ $capture_img }}')
        link.setAttribute('download', 'MirrAR' + Date.now() + '.png')
        link.click()
      }
  window.addEventListener("load", function() {
      setTimeout(function() {
          window.scrollTo(0, 1);
      }, 4000);
  });
  /* Get the documentElement (<html>) to display the page in fullscreen */
  var elem = document.documentElement;
  openFullscreen();
  /* View in fullscreen */
  function openFullscreen() {
      // if (elem.requestFullscreen) {
      //   elem.requestFullscreen();
      // } else 
      if (elem.mozRequestFullScreen) {
          /* Firefox */
          elem.mozRequestFullScreen();
      } else if (elem.webkitRequestFullscreen) {
          /* Chrome, Safari and Opera */
          elem.webkitRequestFullscreen();
      } else if (elem.msRequestFullscreen) {
          /* IE/Edge */
          elem.msRequestFullscreen();
      }
  }

  /* Close fullscreen */
  function closeFullscreen() {
      if (document.exitFullscreen) {
          document.exitFullscreen();
      } else if (document.mozCancelFullScreen) {
          /* Firefox */
          document.mozCancelFullScreen();
      } else if (document.webkitExitFullscreen) {
          /* Chrome, Safari and Opera */
          document.webkitExitFullscreen();
      } else if (document.msExitFullscreen) {
          /* IE/Edge */
          document.msExitFullscreen();
      }
  }

  function readmore() {
      var dots = document.getElementById("dots");
      var moreText = document.getElementById("more");
      var btnText = document.getElementById("readmore");

      if (dots.style.display === "none") {
          dots.style.display = "inline";
          btnText.innerHTML = "Read more";
          moreText.style.display = "none";
      } else {
          dots.style.display = "none";
          btnText.innerHTML = "Read less";
          moreText.style.display = "inline";
      }
  }

  function onShare() {
      console.log(navigator);
      if (navigator.share) {
          navigator
              .share({
                  title: "MirrAR",
                  text: "I tried this jewellery on me using MirrAR Virtual TryOn. I recommend you to give it a Try. Click on the Try Now button and Allow Camera access to experience the design. @mirrarofficial",
                  url: window.location.href,
              })
              .then(() => console.log("Successful share"))
              .catch((error) => console.log("Error sharing", error));
      }
  }
</script>  
@endsection