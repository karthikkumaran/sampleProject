@extends('layouts.admin')
@section('content')
@php
//$token=auth()->user()->createToken('auth-token')->accessToken; 
$token=auth()->user()->createToken('auth-token')->plainTextToken; 
@endphp
<style>

</style>
<div class="card">
  <div class="card-body">
    <div class="row">
        <div class="col ml-3">
            <h4 class="fw-normal mb-1"><b>To use SimpliSell on your mobile:</b></h4>
            <p class="">1. Open <b>SimpliSell</b> on your phone</p>
            <p class="">2. Scan the <b>QrCode</b></p>
            <p class="">3. Automatically sync with mobile</p>

        </div>
        <div class="col">
            <div class="" id="qrcode" data-token="{{'SIMPLISELL'.$token}}" style="width:fit-content; margin: 0 auto;"></div>
        </div>
    </div>
  </div>
</div>

@endsection
@section('scripts')
@parent
<script>

var qrcode = new QRCode(document.getElementById('qrcode'), 
            {
                text: $('#qrcode').attr('data-token'),
                // width: 240,
                // height: 240,
                // colorDark: "#000000",
                // colorLight: "#ffffff",
                logo: "{{ asset('XR/app-assets/images/logo/logo-mini.png') }}",
                correctLevel: QRCode.CorrectLevel.H
            });
</script>
@endsection