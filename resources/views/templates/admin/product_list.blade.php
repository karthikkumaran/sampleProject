@extends('layouts.admin')
@section('content')
@section('styles')
@parent
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/plugins/file-uploaders/dropzone.css')}}">
<style>
.dropzone .dz-message {
    height: auto !important;
}
</style>
@endsection
@php
$agent=new \Jenssegers\Agent\Agent();
if(empty($user_meta_data['settings']['currency']))
        $currency = "₹";
    else
        $currency = App\User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
@endphp
@include('modals.addnewproduct')
@include('modals.add_products_to_catalogs')
@include('modals.edit_product')
@include('modals.edit_productvariant')
@include('csvImport.modal', ['model' => 'Product', 'route' => 'admin.products.parseCsvImport'])
<div class="ecommerce-application">
<div class="row mb-2">
        <div class="col-lg-12">

@if($agent->isMobile())
    <div class="row mb-1">
        <div class="col-sm-12 p-0">
@endif
            <button type="button" class="btn btn-success pull-right ml-1 mr-1" onclick="addProductsToCatalog()">
            <i class="feather icon-plus"></i> Add Products to Calatog
            </button>
@if($agent->isMobile())
        </div>
    </div><br>
@endif
@if(!session('subscription_exhausted'))
<button type="button" class="btn btn-primary pull-right " onclick="addnewproduct()">
<!-- <i class="feather icon-cube"></i> -->
<i class="feather icon-plus"></i>
 Add Product</button>
@endif
@if ($agent->isMobile())
    <div class="row mb-1">
        <div class="col-md-12">
@endif
    <button class="btn btn-success text-white pull-right mr-2" data-toggle="modal" data-target="#csvImportModal">
        <i class="feather icon-upload"></i> Import Product
    </button>
@if ($agent->isMobile())
        </div>
    </div><br>
@endif
    </div>
</div>
@if($product_enable == "false")
    <div class="row mb-2">
        <div class="col-lg-12">
            <h5 class="alert alert-danger" role="alert">
                <i class="feather icon-info mr-1 align-middle"></i>
                Disable a product if you want to enable any other product.
            </h5>
        </div>
    </div>
@else
    @if($remaining_products != "false" && $remaining_products <= 10)
        <div class="row mb-2">
            <div class="col-lg-12">
                <h5 class="alert alert-danger" role="alert">
                    <i class="feather icon-info mr-1 align-middle"></i>
                    You can upload only {{$remaining_products}} more products.
                </h5>
            </div>
        </div>
    @endif
@endif

<section id="ecommerce-searchbar">
    <div class="row mt-1">
        <div class="col-11">
            <fieldset class="form-group position-relative">
                <input type="text" class="form-control search-product" id="ModalProductSearch"
                    placeholder="Search here">
                <div class="form-control-position">
                    <i class="feather icon-search"></i>
                </div>
            </fieldset>
        </div>
        <div class="col-1 " id="show_my_porducts_filter_btn" onclick="show_my_products_filter()" style="cursor:pointer;">
            <button class="navbar-toggler shop-sidebar-toggler w-100 h-100 d-flex justify-content-center" type="button" data-toggle="collapse" onclick="show_my_products_filter()">
                <span class="navbar-toggler-icon d-block d-lg-block d-md-block d-block pr-2"><i class="feather icon-filter" style="font-size:21px;"></i></span>
            </button>
        </div>
    </div>
</section>





    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="product-all-tab" data-toggle="tab" href="#product-all"
                aria-controls="product-all" role="tab" aria-selected="true">All Products(<span id="all_products_count"></span>)</a>
        </li>
        <!-- <li class="nav-item">
            <a class="nav-link" id="product-edit-tab" data-toggle="tab" href="#product-edit"
                aria-controls="product-edit" role="tab" aria-selected="false">Unedited Products</a>
        </li> -->
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="product-all" aria-labelledby="product-all-tab" role="tabpanel">
            <section id="ecommerce-productslist" class="list-view d-block mb-4">
                <div class="" id="product_list_1">

                </div>

            </section>
        </div>

        <div class="tab-pane" id="product-edit" aria-labelledby="product-edit-tab" role="tabpanel">
            <section class="list-view d-block mb-4">
                <div class="" id="product_list">
                    @foreach ($data as $key => $product)
                    @if(empty($product->name))
                    @php
                    $img = $objimg = asset('XR/assets/images/placeholder.png');
                    $obj_id = "";
                    if(count($product->photo) > 0){
                    $img = $product->photo[0]->getUrl('thumb');
                    $objimg = asset('XR/assets/images/placeholder.png');
                    $obj_id = "";
                    } else if(count($product->arobject) > 0){
                        if(!empty($product->arobject[0]->object))
                        if(!empty($product->arobject[0]->object->getUrl())) {
                            $img = $product->arobject[0]->object->getUrl();
                        }
                    }
                    if(count($product->arobject) > 0){
                        if(!empty($product->arobject[0]->object))
                        if(!empty($product->arobject[0]->object->getUrl())) {
                            $objimg = $product->arobject[0]->object->getUrl();
                            $obj_id = $product->arobject[0]->id;
                        }
                    }
                    @endphp
                    <div class="card ecommerce-card" id="product_list_{{$product->id}}">
                        <div class="card-content">
                        <div class="item-img text-center d-inline p-1" style="min-height: 12.85rem;">
                                @if(count($product->photo) > 1)
                                <!--Carousel Wrapper-->
                                <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails"
                                    data-interval="false" data-ride="carousel">
                                    <!--Slides-->
                                    <div class="carousel-inner" role="listbox">
                                        @foreach($product->photo as $photo)
                                        @if($loop->index < 3)
                                        <div class="carousel-item {{$loop->index == 0?'active':''}}">
                                            <img class="img-fluid" style="width: 150px; max-height:150px;min-height:150px;"
                                                src="{{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl():$product->arobject->object->getUrl() ?? ''}}"
                                                onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                        </div>
                                        @endif
                                        @endforeach
                                    </div>
                                    <!--/.Slides-->
                                    <ol class="carousel-indicators position-relative ">
                                        @foreach($product->photo as $photo)
                                        @if($loop->index < 3)
                                        <li data-target="#carousel-thumb" data-slide-to="{{$loop->index}}" class=" {{$loop->index == 0?'active':''}}">
                                            <img class="d-block img-fluid" style="width:30px; height:30px;"
                                                src="{{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl():$product->arobject->object->getUrl() ?? ''}}"
                                                onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                        </li>
                                        @endif
                                        @endforeach

                                    </ol>
                                </div>
                                <!--/.Carousel Wrapper-->
                                @else
                                <img class="img-fluid" src="{{$img}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';"
                                        style="width: 150px; max-height:150px;">
                                @endif
                            </div>
                            <div class="card-body">
                                <div class="item-name">
                                <input type="hidden" class="tryonobj" value="{{$objimg}}"/>
                                    <input type="hidden" class="obj_id" value="{{$obj_id}}"/>
                                    <a href="#" type="button" class="update" data-pk="{{$product->id}}"
                                        data-url="{{route('admin.product_list.update',$product->id)}}" data-type="text"
                                        data-title="Enter name" data-name="name">{{$product->name ?? 'Untitled'}}</a>
                                    <p class="item-company">
                                        @if(count($product->categories) > 0)
                                        <div class="badge badge-pill badge-glow badge-primary mr-1">
                                            {{$product->categories[0]['name']}}
                                        </div>
                                        @endif
                                    </p>
                                </div>
                                <div>
                                <a href="#" type="button" class="update" data-pk="{{$product->id}}"
                                        data-url="{{route('admin.product_list.update',$product->id)}}" data-type="text"
                                        data-title="Enter the sku" data-name="sku">{{$product->sku ?? 'No SKU'}}</a>
                                </div>
                                <!-- <div>
                                    <p class="item-description update" data-pk="{{$product->id}}"
                                        data-url="{{route('admin.product_list.update',$product->id)}}" data-type="textarea"
                                        data-title="Enter description" data-name="description">
                                        {{$product->description ?? 'No Description'}}
                                    </p>
                                </div> -->
                                <div>
                                <!-- <a href="#" type="button" class="update" data-pk="{{$product->id}}"
                                        data-url="{{route('admin.product_list.update',$product->id)}}" data-type="text"
                                        data-title="Enter Product Url" data-name="url">{{$product->url ?? 'No url'}}</a> -->
                                </div>
                            </div>
                            <div class="item-options text-center">
                                <div class="item-wrapper">
                                    <div class="item-rating">
                                        <a href="#" onclick="deleteProduct($(this))" data-id="{{$product->id}}"
                                            class="badge badge-pill badge-glow badge-danger"><i
                                                class="feather icon-trash-2 m-0"></i></a>
                                    </div>
                                    <div class="item-cost">
                                        <h6 class="item-price">
                                            ₹ <span class="update" data-pk="{{$product->id}}"
                                                data-url="{{route('admin.product_list.update',$product->id)}}"
                                                data-type="text" data-title="Enter price"
                                                data-name="price">{{$product->price ?? '00.00'}}</span>
                                        </h6>
                                    </div>
                                </div>
                                <div class="wishlist bg-white">
                                    <!-- <i class="fa fa-heart-o mr-25"></i> Wishlist -->
                                </div>
                                <span class="cart p-0">
                                @if(!empty($obj_id))
                                    <!-- <button type="button" class="btn btn-block text-white btn-primary edittryon"
                                        data-color="primary">Edit Try On Image
                                    </button> -->
                                @else
                                <!-- <button type="button" class="btn btn-block text-white btn-primary edittryon"
                                        data-color="primary">Add Try On Image
                                    </button> -->
                                @endif
                                <a href="{{route('admin.products.edit',$product->id)}}" class="btn btn-block text-white btn-primary"
                                        data-color="primary">Edit
                                </a>
                                </span>

                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                    @if(count($data) == 0)
                    <h3 class="text-light text-center p-2">No Products added yet, please add</h3>
                    @endif
                </div>
                {!! $data->render() !!}
            </section>
        </div>

    </div>

    <!-- </div>-->

</div>

<!-- Add New Product Modal -->
<div class="modal fade ecommerce-application" id="productEditModal" tabindex="-1" role="dialog"
    aria-labelledby="productEditModalTitle" aria-hidden="true">
    <form method="POST" action="{{route('admin.arobjects.objupload')}}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="obj_id" value="" id="obj_id" />
        <input type="hidden" name="product_id" value="" id="product_id" />
        <input type="hidden" name="is_product" value="" id="is_product" />
        <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="productSelectModalTitle"><span id="tryontitle"></span> - Try On Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                                    <div class="card ecommerce-card mb-0" style="min-height:350px;">
                                        <div class="card-content">
                                            <div class="item-img text-center" style="min-height: 12.85rem;">
                                                <a href="#">
                                                    <img class="img-fluid" id="tryonimg" src="" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';"
                                                        style="max-height:350px;"></a>
                                            </div>

                                        </div>
                                    </div>

                        </div>
                        <div class="col-sm-6">
                            <div class="dropzone dropzone-area {{ $errors->has('photo') ? 'is-invalid' : '' }}"
                                id="uploadtryonimage">
                                <input type="hidden" class="form-control" name="object" value="" required data-validate="true"/>
                                <div class="dz-message">Upload Try On Image</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"
                        aria-label="Close">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>

        </div>
    </form>
</div>
<!-- end Add New Product Modal -->

<!-- My Products Filter Modal -->
<div class="modal modal-sm fade" id="myProductsFilterModal" tabindex="-1" role="dialog" aria-labelledby="myProductsFilterModalTitle" aria-hidden="true" @if($agent->isMobile()) @else style="margin-left:35%" @endif>
    <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myProductsFilterModalTitle">Filters</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card" style="padding-bottom: 0;">
                    <div class="card-body" style="padding-bottom: 2vh;">
                        <li class="d-flex justify-content-between align-items-center py-25">
                            <span class="vs-checkbox-con vs-checkbox-primary">
                                <input type="checkbox" name="out_of_stock_products_only" value="out_of_stock_products_only">
                                <span class="vs-checkbox">
                                    <span class="vs-checkbox--check">
                                        <i class="vs-icon feather icon-check"></i>
                                    </span>
                                </span>
                                <span class="">Show out of stock products only</span>
                            </span>
                        </li>
                        <div class="form-group row mt-1">
                            <span class="ml-1 mb-1">Show products in stock range</span>
                            <div class="form-group col-6 w-100">
                                <input class="form-control" style="width: 100%" type="number" name="stock_filter_min" id="stock_filter_min" placeholder="Enter the minimum stock value">
                            </div>
                            <div class="form-group col-6 w-100">
                                <input class="form-control" style="width: 100%" type="number" name="stock_filter_max" id="stock_filter_max" placeholder="Enter the maximum stock value">
                            </div>
                            <div class="alert alert-warning" id="error_min" style="display:none;" role="alert">Minimum value should be less than maximum value.</div>
                            <div class="alert alert-warning" id="error_max" style="display:none;" role="alert">Maximum value should be greater than minimum value.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="clear_my_products_filter()">Clear filters</button>
                <button type="button" id="apply_my_products_filter" class="btn btn-primary" onclick="apply_my_products_filter()">Apply filters</button>
            </div>
        </div>
    </div>
    </form>
</div>
<!-- End My Products Filter Modal -->

@endsection

@section('scripts')
@parent
<script>
    $('.update').editable({
        mode: "inline"
    });

    function publishCard(id, status) {
        $('#loading-bg').show();
        var url = "{{route('admin.campaigns.publish')}}";
        url = url.replace(':id', id);
        var txt = (status == 1) ? "Published" : "Unpublished";
        $.ajax({
            url: url,
            type: "POST",
            data: {
                '_method': 'POST',
                'id': id,
                'status': status
            },
            success: function (response_sub) {
                $('#loading-bg').show();
                Swal.fire({
                    type: "success",
                    title: txt + '!',
                    text: 'Your campaign has been ' + txt + '.',
                    confirmButtonClass: 'btn btn-success',
                }).then((result) => {
                    $('#loading-bg').hide();
                    window.location.reload();
                });
            }
        });

    }

    function deleteProduct(ss) {
        // console.log(ss.attr('data-id'));
        $('#loading-bg').show();
        var id = ss.attr('data-id');
        var url = "{{route('admin.products.destroy',':id')}}";
        url = url.replace(':id', id);
        swal.fire({
            title: 'Are you sure to delete this proudct?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, Delete it!',
            cancelButtonText: 'No, Cancel!',
            confirmButtonClass: 'btn btn-danger',
            cancelButtonClass: 'btn btn-light',
        }).then(function (result) {
            // console.log(result);
            if (result.value) {
                $('#loading-bg').show();
                // console.log(result.value);
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                    success: function (response_sub) {
                        $('#loading-bg').show();
                        // console.log(response_sub)
                        if(response_sub.status == "success") {
                            Swal.fire({
                                type: "success",
                                title: 'Delete!',
                                text: 'Product has been deleted.',
                                confirmButtonClass: 'btn btn-success',
                            }).then((result) => {
                                // window.location.reload();
                                $('#product_list_' + id).remove();
                                $('#loading-bg').hide();
                            });
                        } else {
                            Swal.fire({
                            type: "error",
                            title: 'Delete!',
                            text: "This product is added to one of the catalog, please remove it from the catalog to delete it.",
                            confirmButtonClass: 'btn btn-danger',
                        }).then((result) => {
                            $('#loading-bg').show();
                            window.location.reload();
                        });
                        }

                    },error: function (res) {
                        Swal.fire({
                            type: "error",
                            title: 'Delete!',
                            text: "This product is added to one of the catalog, please remove it from the catalog to delete it.",
                            confirmButtonClass: 'btn btn-danger',
                        }).then((result) => {
                            // window.location.reload();
                        });
                    }
                });


            }
        })

    }

    // editModal();

    function editModal() {
        $('#productEditModal').modal('show');
    }

    $('.edittryon').on('click',function(event){

        if ($(event.target).parents('.card').length > 0) {
            // console.log($(event.target).parents('.card').children().children('.card-body').children('.item-name').children('a'));
            var p = $(event.target).parents('.card');
            // console.log(p.find('img').attr('src'));
            $('#tryontitle').html(p.find('.update').html());
            // $('#tryonimg').attr('src', p.find('img').attr('src'));
            $('#tryonimg').attr('src', p.find('.tryonobj').val());
            $('#obj_id').val(p.find('.obj_id').val());
            $('#product_id').val(p.find('.update').attr('data-pk'));
            $('#productEditModal').modal('show');
        }
    });

</script>
<script>
    Dropzone.autoDiscover = false;
    myDropzone = new Dropzone('#uploadtryonimage', {
        url: '{{ route('admin.arobjects.storeMedia') }}',
    maxFilesize: 40, // MB
    maxFiles: 1,
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 40
    },
    success: function (file, response) {
      $('form').find('input[name="object"]').remove()
      $('form').append('<input type="hidden" name="object" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="object"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }

    });

    $(document).ready(function(){
        setTimeout(function(){
            getList(0);
        }, 1500);
    });

    $("#ModalProductSearch").on('input', function(){
        searchData = $(this).val();
        if(searchData.length >= 3 || searchData == "")
            getList(0);
    });

    $("#ecommerce-productslist").on('click', '.pagination a', function (event) {
        event.preventDefault();

        $('li').removeClass('active');
        $(this).parent('li').addClass('active');

        var myurl = $(this).attr('href');
        var page = $(this).attr('href').split('page=')[1];

        getList(page);
    });

    var q = "";
    var searchData = "";
    var sort = "";
    var page = 0;
    var filterData = "";
    var filter = [];
    var stock_filter_min = document.getElementById('stock_filter_min');
    var stock_filter_max = document.getElementById('stock_filter_max');

    function getList(page){
        $('#loading-bg').show();
        q = 'q=' + searchData + '&s=' + sort + '&page=' + page + '&filterData=' + filterData + '&filter=' + JSON.stringify(filter);
        $.ajax({
            url: "{{route('admin.products.product_list')}}" + '?' + q,
            type: "get",
            datatype: "html"
        }).done(function (data) {
            //console.log(data);
            //console.log("sucess");
            $("#product_list_1").empty().html(data);
            //$("#product_list").append(data);
            $('#loading-bg').hide();
        }).fail(function (jqXHR, ajaxOptions, thrownError) {
            $('#loading-bg').hide();
            // alert('No response from server');
        });
    }

    function show_my_products_filter(){
        $("#myProductsFilterModal").modal('show');
    }

    function apply_my_products_filter() {
        filter = [];
        var stock_values_array = [];

        if ($('input[name="out_of_stock_products_only"]').is(":checked") == true) {
            filter.push({
                type: 'out_of_stock_products_only',
                value: $('input[name="out_of_stock_products_only"]').val()
            });
        }

        if ((stock_filter_min.value == "") && (stock_filter_max.value == "")) {

        } else if ((stock_filter_min.value != "") && (stock_filter_max.value == "")) {
            filter.push({
                type: 'stock',
                value: stock_filter_min.value
            });
        } else if ((stock_filter_min.value == "") && (stock_filter_max.value != "")) {
            stock_values_array.push(0);
            stock_values_array.push(stock_filter_max.value);
            var stock_values = stock_values_array.toString();
            filter.push({
                type: 'stock',
                value: stock_values
            });
        } else {
            stock_values_array.push(stock_filter_min.value);
            stock_values_array.push(stock_filter_max.value);
            var stock_values = stock_values_array.toString();
            filter.push({
                type: 'stock',
                value: stock_values
            });
        }

        $("#myProductsFilterModal").modal('hide');
        getList(0);
    }

    function clear_my_products_filter(){
        filter = [];
        $("[name=out_of_stock_products_only]").prop("checked", false);
        stock_filter_min.value = "";
        stock_filter_max.value = ""
        $("#myProductsFilterModal").modal('hide');
        getList(0);
    }

    $("#stock_filter_min").on('input', function() {
        var val = parseInt($(this).val());
        var max = parseInt($("#stock_filter_max").val());
        if (val > max) {
            $('#error_min').show();
            $('#apply_my_products_filter').attr('disabled', true);
        } else {
            $('#error_max').hide();
            $('#error_min').hide();
            $('#apply_my_products_filter').attr('disabled', false);
        }
    });

    $("#stock_filter_max").on('input', function() {
        var val = parseInt($(this).val());
        var min = parseInt($("#stock_filter_min").val());
        if (val < min) {
            $('#error_max').show();
            $('#apply_my_products_filter').attr('disabled', true);
        } else {
            $('#error_max').hide();
            $('#error_min').hide();
            $('#apply_my_products_filter').attr('disabled', false);
        }

    });
</script>
@endsection
