@extends('layouts.admin')
@section('styles')
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/plugins/forms/validation/form-validation.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/pickers/pickadate/pickadate.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-user.css')}}">
@endsection
@section('content') 

                <!-- users edit start -->
                <section class="users-edit">
                    <div class="card">
                        <div class="card-content">
                            <div class="card-body">
                                <ul class="nav nav-tabs mb-3" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center active" id="account-tab" data-toggle="tab" href="#account" aria-controls="account" role="tab" aria-selected="true">
                                            <i class="feather icon-user mr-25"></i><span class="d-none d-sm-block">Account</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center" id="information-tab" data-toggle="tab" href="#information" aria-controls="information" role="tab" aria-selected="false">
                                            <i class="feather icon-info mr-25"></i><span class="d-none d-sm-block">User Info</span>
                                        </a>
                                    </li>
                                    <!-- <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center" id="company-tab" data-toggle="tab" href="#company" aria-controls="company" role="tab" aria-selected="false">
                                            <i class="feather icon-info mr-25"></i><span class="d-none d-sm-block">Company Info</span>
                                        </a>
                                    </li> -->
                                    <li class="nav-item">
                                        <a class="nav-link d-flex align-items-center" id="change-password-tab" data-toggle="tab" href="#change-password" aria-controls="change-password" role="tab" aria-selected="false">
                                            <i class="fa fa-key mr-25"></i><span class="d-none d-sm-block">Change Password</span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="account" aria-labelledby="account-tab" role="tabpanel">
                                        <!-- users edit media object start -->
                                        <div class="media mb-2">
                                            <a class="mr-2 my-25" href="#">
                                                <img src="{{asset('XR/assets/images/profile.png')}}" alt="users avatar" class="users-avatar-shadow rounded" height="90" width="90">
                                            </a>
                                            <div class="media-body mt-50">
                                                <h4 class="media-heading">{{$user->name}}</h4>
                                            </div>
                                        </div>
                                        <!-- users edit media object ends -->
                                        <!-- users edit account form start -->
                                        <form method="POST" action="{{ route("admin.users.accountSettingsUpdate", [$user->id]) }}" enctype="multipart/form-data">
                                            @method('PUT')
                                            @csrf
                                            <div class="row">
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>Name</label>
                                                            <input type="text" class="form-control" placeholder="name" name="name" id="name" value="{{ old('name', $user->name) }}" required data-validation-required-message="This username field is required">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>E-mail</label>
                                                            <input type="hidden" value="{{ old('email', $user->email) }}" name="email">
                                                            <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" placeholder="Email" type="text" value="{{ old('email', $user->email) }}" disabled>
                                                        </div>
                                                    </div>
                                                   
                                                </div>
                                                
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>Mobile</label>
                                                            <input type="text" class="form-control" value="{{ old('mobile', $user->mobile) }}" id="mobile" name="mobile" pattern="[0-9]{10}" maxlength="10" required placeholder="Mobile number here..." data-validation-required-message="This mobile number is required">
                                                        </div>
                                                    </div>
                                                </div>
                                               
                                                <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                                                    <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">Update
                                                        Changes</button>
                                                    <button type="reset" class="btn btn-outline-warning">Reset</button>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- users edit account form ends -->
                                    </div>
                                    <div class="tab-pane" id="information" aria-labelledby="information-tab" role="tabpanel">
                                        <!-- users edit Info form start -->
                                        <form method="POST" action="{{ route('admin.user-infos.update', [$userInfo->id ?? '']) }}" enctype="multipart/form-data">
                                            @method('PUT')
                                            @csrf
                                            <input type="hidden" name="user_id" value="{{$user->id}}"/>
                                            <div class="row mt-1">
                                                <div class="col-12 col-sm-12"> 
                                                    <h5 class="mb-1"><i class="feather icon-user mr-25"></i>Personal Information</h5>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                            <div class="controls">
                                                                    <label>Birth date</label>
                                                                    <input type="text" class="form-control birthdate-picker" name="dob" value="{{ old('dob', $userInfo->dob  ?? '') }}" placeholder="Birth date">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>Gender</label>
                                                        <ul class="list-unstyled mb-0">
                                                        @foreach(App\UserInfo::GENDER_RADIO as $key => $label)
                                                            <li class="d-inline-block mr-2">
                                                                <fieldset>
                                                                    <div class="vs-radio-con">
                                                                    <input class="form-check-input" type="radio" id="gender_{{ $key }}" name="gender" value="{{ $key }}" {{ old('gender', $userInfo->gender ?? '') === (string) $key ? 'checked' : '' }}>
                                                                        <span class="vs-radio">
                                                                            <span class="vs-radio--border"></span>
                                                                            <span class="vs-radio--circle"></span>
                                                                        </span>
                                                                        {{ $label }}
                                                                    </div>
                                                                </fieldset>
                                                            </li>
                                                        @endforeach
                                                            
                                                        </ul>
                                                    </div>
                                                   
                                                   
                                                    
                                                   
                                                   

                                                </div>
                                                <div class="col-12 col-sm-6">
                                                <div class="form-group">
                                                        <div class="controls">
                                                            <label>Designation</label>
                                                            <input type="text" name="designation" class="form-control" required placeholder="Designation" value="{{ old('designation', $userInfo->designation ?? '') }}" data-validation-required-message="This Designation field is required">
                                                        </div>
                                                    </div>
                                                    <!-- <div class="form-group">
                                                        <div class="controls">
                                                            <label>Mobile</label>
                                                            <input type="text" class="form-control" value="{{ old('mobile', $userInfo->mobile ?? '') }}" name="mobile" placeholder="Mobile number here..." data-validation-required-message="This mobile number is required">
                                                        </div>
                                                    </div> -->


                                                </div>
                                                <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                                                    <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">Update
                                                        Changes</button>
                                                    <button type="reset" class="btn btn-outline-warning">Reset</button>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- users edit Info form ends -->
                                    </div>
                                   

                                    <div class="tab-pane" id="company" aria-labelledby="company-tab" role="tabpanel">
                                        <!-- users edit Info form start -->
                                        <form method="POST" action="{{ route("admin.company-infos.update", [$companyInfo->id ?? '']) }}" enctype="multipart/form-data">
                                        @method('PUT')
                                        @csrf
                                         <input type="hidden" name="user_id" value="{{$user->id}}"/>
                                            <div class="row mt-1">
                                                <div class="col-12 col-sm-12">
                                                    <h5 class="mb-1"><i class="feather icon-user mr-25"></i>Company Information</h5>
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <div class="controls">
                                                                    <label>Business Name</label>
                                                                    <input type="text" class="form-control" name="name" id="name" value="{{ old('name', $companyInfo->name ?? '') }}" required placeholder="Business Name" data-validation-required-message="This field is required">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>E-mail</label>
                                                            <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" placeholder="Company Email" type="text" name="email" id="email" value="{{ old('email', $companyInfo->email ?? '') }}" required data-validation-required-message="This field is required">
                                                        </div>
                                                    </div>
                                                    

                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>Website</label>
                                                            <input type="text" class="form-control" required placeholder="Website here..." value="" data-validation-required-message="This Website field is required">
                                                        </div>
                                                    </div>
                                                        </div>
                                                    <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>Phone no.</label>
                                                            <input class="form-control" placeholder="Phone number here..." type="text" name="phoneno" id="phoneno" value="{{ old('phoneno', $companyInfo->phoneno ?? '') }}" data-validation-required-message="This phone number is required">
                                                         </div>
                                                    </div>

                                                    <!-- <div class="form-group">
                                                        <div class="controls">
                                                            <label>Subdomain</label>
                                                            <fieldset>
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" placeholder="subdomain" aria-describedby="basic-addon2" name="subdomain" id="subdomain" value="{{ old('subdomain', $companyInfo->subdomain ?? '') }}" required data-validation-required-message="This is required">
                                                                    <div class="input-group-append">
                                                                        <span class="input-group-text" id="basic-addon2">.{{env('SITE_URL')}}.com</span>
                                                                    </div>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                    </div> -->
                                                   

                                                </div>
                                                <div class="col-12 col-sm-12">
                                                    <h5 class="mb-1 mt-2 mt-sm-0"><i class="feather icon-map-pin mr-25"></i>Address</h5>
                                                </div>
                                                <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>Address Line 1</label>
                                                            <input type="text" class="form-control" name="address" value="{{ old('address', $companyInfo->address ?? '') }}" placeholder="Address Line 1" data-validation-required-message="This Address field is required">
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>Postcode</label>
                                                            <input type="text" class="form-control" placeholder="postcode" value="" data-validation-required-message="This Postcode field is required">
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>State</label>
                                                            <select class="form-control select2 {{ $errors->has('state') ? 'is-invalid' : '' }}" name="state_id" id="state_id">
                                                                @foreach($states as $id => $state)
                                                                    <option value="{{ $id }}" {{ ($companyInfo->state ? $companyInfo->state->id : old('state_id')) == $id ? 'selected' : '' }}>{{ $state }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                        </div>
                                                        <div class="col-12 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>Address Line 2</label>
                                                            <input type="text" class="form-control"  placeholder="Address Line 2" data-validation-required-message="This field is required">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>City</label>
                                                            <select class="form-control select2 {{ $errors->has('city') ? 'is-invalid' : '' }}" name="city_id" id="city_id">
                                                                @foreach($cities as $id => $city)
                                                                    <option value="{{ $id }}" {{ ($companyInfo->city ? $companyInfo->city->id : old('city_id')) == $id ? 'selected' : '' }}>{{ $city }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>Country</label>
                                                            <select class="form-control select2 {{ $errors->has('country') ? 'is-invalid' : '' }}" name="country_id" id="country_id" required data-validation-required-message="This Time Zone field is required">
                                                                @foreach($countries as $id => $country)
                                                                    <option value="{{ $id }}" {{ ($companyInfo->country ? $companyInfo->country->id : old('country_id')) == $id ? 'selected' : '' }}>{{ $country }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                                                    <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">Save
                                                        Changes</button>
                                                    <button type="reset" class="btn btn-outline-warning">Reset</button>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- users edit Info form ends -->
                                    </div>
                                    <div class="tab-pane" id="change-password" aria-labelledby="change-password-tab" role="tabpanel">
                                        <!-- users edit socail form start -->
                                        <form method="POST" action="{{ route('admin.users.changePassword',$user->id) }}" enctype="multipart/form-data">
                                        @csrf
                                            <div class="row">
                                            @if(app('impersonate')->isImpersonating())
                                            <div class="col-12 col-sm-6">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>Create Password</label>
                                                        <input type="password" class="form-control" name="create_password" autocomplete="reate-password" required data-validation-required-message="This field is required">
                                                    </div>
                                                </div>
                                            </div>
                                            @else
                                            <div class="col-12 col-sm-6">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>Current Password</label>
                                                        <input type="password" class="form-control" name="current_password" autocomplete="current-password" required data-validation-required-message="This field is required">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>New Password</label>
                                                        <input type="password" class="form-control" name="new_password" autocomplete="current-password" required data-validation-required-message="This field is required">
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>New Confirm Password</label>
                                                        <input type="password" class="form-control" name="new_confirm_password" autocomplete="current-password" required data-validation-required-message="This field is required">
                                                    </div>
                                                </div>
                                                @endif
                                                <div class="col-12 d-flex flex-sm-row flex-column justify-content-end mt-1">
                                                    <button type="submit" class="btn btn-primary glow mb-1 mb-sm-0 mr-0 mr-sm-1">Save
                                                        Changes</button>
                                                    <button type="reset" class="btn btn-outline-warning">Reset</button>
                                                </div>
                                            </div>
                                            
                                        </form>
                                        <!-- users edit socail form ends -->
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- users edit ends -->



@endsection
@section('scripts')
@parent
<script src="{{asset('XR/app-assets/vendors/js/forms/validation/jqBootstrapValidation.js')}}"></script>
<script src="{{asset('XR/app-assets/vendors/js/pickers/pickadate/picker.js')}}"></script>
<script src="{{asset('XR/app-assets/vendors/js/pickers/pickadate/picker.date.js')}}"></script>
<script src="{{asset('XR/app-assets/js/scripts/pages/app-user.js')}}"></script>
    @endsection