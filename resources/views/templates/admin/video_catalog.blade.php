@extends('layouts.admin')
@section('content')
@php
if(empty($user_meta_data['settings']['currency']))
$currency = "₹";
else
$currency = App\User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
@endphp
@if($product_enable == "false")
    <div class="row mb-2">
        <div class="col-lg-12">
            <h5 class="alert alert-danger" role="alert">
                <i class="feather icon-info mr-1 align-middle"></i>
                Disable a product if you want to enable any other product.
            </h5>
        </div>
    </div>
@else
    @if($remaining_products != "false" && $remaining_products <= 10)
        <div class="row mb-2"> 
            <div class="col-lg-12">
                <h5 class="alert alert-danger" role="alert">
                    <i class="feather icon-info mr-1 align-middle"></i>
                    You can upload only {{$remaining_products}} more products.
                </h5>
            </div>
        </div>
    @endif
@endif
<div class="text-right w-100 mb-1">
    <button type="button" class="btn btn-primary btn-icon" onclick="addnewvideo()">
        <i class="feather icon-plus"></i> Add Video
    </button>
    {{-- <a href="{{route('admin.campaigns.preview',$campaign->preview_link)}}" target="__blank" class="btn btn-warning btn-icon"><i class="feather icon-eye"></i></a> --}}

    <a href="#" onclick="publishCard({{$campaign->id}},{{$campaign->is_start == 1?0:1}})" id="publish_catalog_icon_{{$campaign->id}}" type="button"
        class="btn btn-info btn-icon" {{$campaign->is_start == 0 ? '':'hidden'}} {{ ($campaign->downgradeable == 1 && $catalogs_publishable == "false") ? 'hidden' :''}}><i class="feather icon-link"></i>
        Publish</a>

    <button type="button" class="btn btn-info btn-icon sharebtn" id="share_public_icon_{{$campaign->id}}" data-link="{{$campaign->public_link}}" data-qrcode="{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'catalog','',true)}}" data-title="{{$campaign->name}}" data-download="false" data-pdf="false" data-sharelink="{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'video_catalog')}}" {{$campaign->is_start == 1 ? '':'hidden'}}>
    <img src="{{ asset('XR\app-assets\images\icons\share.png') }}" style="height: 21px;width: 21px; margin-top:-10px; margin-right:5px;" > Share Catalog
    </button>
    
    <button type="button" class="btn btn-primary btn-icon" id="catalogSettingsBtn_{{$campaign->id}}" onclick="catalog_settings($(this))" data-id="{{$campaign->id}}" data-is_start="{{$campaign->is_start}}" data-is_private="{{$campaign->is_private}}" data-private_password="{{$campaign->private_password}}" data-is_expired="{{empty($campaign->public_shareable->expires_at) ? 'false':'checked'}}" data-expired_date="{{ empty($campaign->public_shareable->expires_at)?'':date('Y-m-d\TH:i', strtotime($campaign->public_shareable->expires_at)) }}" data-is_added_home="{{$campaign->is_added_home}}" data-toggle="modal" data-target="CatalogSettingsModal" data-catalogs_publishable="" data-catalog_downgradeable="{{$campaign->downgradeable}}">
        <i class="feather icon-settings"></i>
    </button>
</div>
<div class="card ecommerce-application">
    <div class="card-header d-flex justify-content-between">
        @if($campaign->is_start == 1)

        @else
        @if(count($campaign->campaignEntitycards)>0)
        <h5 class="alert alert-danger mt-1 alert-validation-msg w-100" role="alert">
            <i class="feather icon-info mr-1 align-middle"></i>
            <span> Your catalog is not yet published. Once you are done with adding the product to your catalog then hit the publish button.</span>
        </h5>
        @else

        @endif
        @endif

        <h4 class="mb-1"><span class="update" data-pk="{{$campaign->id}}" data-url="{{route('admin.campaigns.update',$campaign->id)}}" data-type="text" data-title="Enter Catalog name" data-name="name">{{ ($campaign->name == "Untitled")?"Untitled Catalog": $campaign->name}}</span> <span class="h3 text-primary"><i class="feather icon-edit-1"></i></span></h4>
        @include('modals.published1')
        <!-- Ecommerce Search Bar Starts -->
        <section id="ecommerce-searchbar" style="width:100%">
            <div class="row">
                <div class="col-sm-12">
                    <fieldset class="form-group position-relative">
                        <input type="text" class="form-control search-product" id="CatalogEntityCardSearch" placeholder="Search from {{count($campaign->campaignEntitycards->whereNull('product_id'))}} product here">
                        <div class="form-control-position">
                            <i class="feather icon-search"></i>
                        </div>
                    </fieldset>
                </div>
            </div>
        </section>
        <!-- Ecommerce Search Bar Ends -->
    </div>

</div>

<section class="grid-view d-block mb-4" id="catalog_entitycard">
    <div class="row" id="entitycard_gird"></div>
</section>
@include('modals.product')
@include('modals.addnewvideo')
@include('modals.edit_video')
@include('modals.campaign_settings')


@endsection

@section('scripts')
@parent
<script>
    var open_video_id;
    if(window.location.hash.includes("#open")){
        open_video_id = window.location.hash.replace("#open-","")
    }
    var catalogs_publishable = "{{$catalogs_publishable}}";
    var allowed_catalogs = "{{$allowed_catalogs}}";
    var set = $('[id^="catalogSettingsBtn_"]');
    set.data('catalogs_publishable',catalogs_publishable);
    // $('img').on("error", function() {
    //         $(this).attr('src', '{{asset('XR/assets/images/placeholder.png')}}');
    //         $(this).css('width', '95%');
    //         $(this).css('max-height', '100%');
    //     });

    // $(window).on('hashchange', function () {
    //         if (window.location.hash) {
    //             page = window.location.hash.replace('#', '');
    //             if (page == Number.NaN || page <= 0) {
    //                 return false;
    //             } else {
    //                 getEntityData(page);
    //             }
    //         }
    //     });

    var q = "";
    var searchData = "";
    var sort = "";
    var page = 0;
    var filterData = "";

    $("#CatalogEntityCardSearch").on('input', function() {
        searchData = $(this).val();
        if (searchData.length >= 3 || searchData == "")
            getEntityData(0);
    });

    $("#catalog_entitycard").on('click', '.pagination a', function(event) {
        event.preventDefault();

        $('li').removeClass('active');
        $(this).parent('li').addClass('active');

        var myurl = $(this).attr('href');
        var page = $(this).attr('href').split('page=')[1];

        getEntityData(page);
    });

    // $(document).ready(function () {
    // setTimeout(function(){  }, 5000);
    getEntityData(page);
    // });
    function getEntityData(page) {
        $('#loading-bg').show();
        // if(searchData == "")
        //     q = 'page=' + page;
        // else 
        q = 'q=' + searchData + '&s=' + sort + '&page=' + page + '&filterDatas=' + filterData +
            '&campaign_id={{$campaign->id}}';

        $.ajax({
            url: "{{route('admin.campaigns.ajaxVideoEntityData')}}" + '?' + q,
            type: "get",
            datatype: "html"
        }).done(function(data) {
            // console.log(data);
            $("#entitycard_gird").empty().html(data);
            // console.log("video loaded");
            if(open_video_id){
                getVideoDetail(open_video_id);
            }
            // $("#product_list").append(data);
            location.hash = page++;
            $('#loading-bg').hide();
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            // console.log("error")
            $('#loading-bg').hide();
            // alert('No response from server');
        });
    }

    $('.update').editable({});

    function publishCard(id, status) {
        $('#loading-bg').show();
        var url = "{{route('admin.campaigns.publish')}}";
        url = url.replace(':id', id);
        var txt = (status == 1) ? "Published" : "Unpublished";
        $.ajax({
            url: url,
            type: "POST",
            data: {
                '_method': 'POST',
                'id': id,
                'status': status
            },
            success: function(response_sub) {
                Swal.fire({
                    type: "success",
                    title: txt + '!',
                    text: 'Your catalog has been ' + txt + '.',
                    confirmButtonClass: 'btn btn-success',
                }).then((result) => {
                    window.location.reload();
                    $('#loading-bg').hide();
                });
            },
            error: function(response_sub) {
                Swal.fire({
                    type: "error",
                    title: "Error",
                    text: response_sub.responseJSON.msg,
                    confirmButtonClass: 'btn btn-danger',
                });
            }
        });

    }

    function deleteCard(id) {
        $('#loading-bg').show();
        var url = "{{route('admin.entitycards.destroy',':id')}}";
        url = url.replace(':id', id);
        swal.fire({
            title: 'Are you sure to remove this proudct from this Catalog?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, Remove it!',
            cancelButtonText: 'No, Cancel!',
            confirmButtonClass: 'btn btn-danger',
            cancelButtonClass: 'btn btn-light',
        }).then(function(result) {
            $('#loading-bg').show();
            // console.log(result);
            if (result.value) {
                // console.log(result.value);
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                    success: function(response_sub) {
                        Swal.fire({
                            type: "success",
                            title: 'Removed!',
                            text: 'Product has been removed from this Catalog.',
                            confirmButtonClass: 'btn btn-success',
                        }).then((result) => {
                            $('#loading-bg').hide();
                            // window.location.reload();
                            $('#entitycard_' + id).remove();
                            getEntityData(page);
                        });

                    }
                });


            }
        })

    }

    function WhatsAppShare(url, msg) {
        var t = "I found this product interesting, open the link and check";
        var hashtags = "{{ isset($meta_data['storefront']['storename'])?$meta_data['storefront']['storename'] : ''}}";
        var message = encodeURIComponent(url) + " " + t + " #" + hashtags;
        // var whatsapp_url = "whatsapp://send?text=" + message;
        // window.location.href = whatsapp_url;
        var whatsapp_url = "https://wa.me/?text=" + message;
        openInNewTab(whatsapp_url);
    }
   
</script>
@endsection