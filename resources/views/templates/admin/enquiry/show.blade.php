@extends('layouts.admin')
@section('styles')
@parent
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
@endsection
@section('content')
@php
$emeta_data = json_decode($enquiry->meta_data, true);
@endphp

<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <a href="{{route('admin.enquiry.index')}}">
                    <h4 class="text-primary"><i class="feather icon-arrow-left"></i> Back to Enquiries</h4>
                </a>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <h3>Enquiry Details</h3>

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <h4><b>Enquiry #{{$enquiry->enquiry->enquiry_id}}</b></h4>

                        <h4>No. Products : <b>{{ count($enquiry->enquiryProducts) }}</b></h4>
                        <h4>Total Price : <b>{!! $emeta_data['currency'] !!}{{ $emeta_data['product_total_price'] }}</b></h4>
                    </div>
                    <div class="col-md-4 d-flex flex-md-row flex-column justify-content-md-center">
                    <a href="https://wa.me/{{$enquiry->country_code ? $enquiry->country_code.$enquiry->mobileno : '91'.$enquiry->mobileno}}" class="mr-1 mb-1" target="_blank">
                            <i style="color:#25D366" class="fa fa-whatsapp fa-2x"></i></a>
                        <a href="tel:{{$enquiry->mobileno}}" class="mr-1 mb-1">
                            <i style="color:#2F91F3" class="fa fa-phone fa-2x"></i></a>

                    </div>
                    <div class="col-md-4 text-md-right">
                        <div class="row justify-content-end pb-1">
                            <select class="form-control" style="width:200px;" name="enquiry_status" id='enquiry_status'>
                                @foreach(App\Enquiry::STATUS as $key => $label)
                                <option value="{{ $key }}" {{ old('enquiry_status', $enquiry->enquiry->status ) ==  $key ? 'selected' : '' }}> {{$label}}</option>
                                @endforeach
                            </select>
                        </div>
                        <b>{{date('d-m-Y h:i:s A', strtotime($enquiry->created_at))}}</b>

                        @if($enquiry->enquiry->notes)
                        <div style="border: 1px solid black;background: #e7e9ec;border-radius:5px;padding: 5px;">
                            <h4>Notes : </h4>
                            <p>{!! $enquiry->enquiry->notes !!}</p>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="col-md-12">
        <h3>Customer Details</h3>
        <div class="card">
            <div class="card-header">
            </div>
            <div class="card-body">
                <div class="row no-gutters">
                    <div class="col-md-6  order-1 no-gutters">
                        <div class="row no-gutters">
                            <div class="col col-md-2 no-gutters">
                                Name
                            </div>
                            <div class="col col-md-5 no-gutters">
                                <b> {{$enquiry->fname ?? "" }}</b>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col col-md-2  no-gutters">
                                Address
                            </div>
                            <div class="col col-md-5 no-gutters">
                                <b>
                                    {{$enquiry->address1 ?? "" }}<br>
                                    {{$enquiry->address2 ?? "" }}<br>
                                    {{$enquiry->city?? "" }}<br>
                                    {{$enquiry->pincode?? "" }}<br>
                                    {{$enquiry->state ?? "" }}<br>
                                    {{$enquiry->country ?? "" }}<br>
                                </b>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 order-2 no-gutters">
                        <div class="row no-gutters">
                            <div class="col col-md-5 no-gutters">
                                Email
                            </div>
                            <div class="col-6 col-md-6 no-gutters">
                                <b>{{ $enquiry->email ?? "" }}</b>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col col-md-5 no-gutters">
                                Mobile No
                            </div>
                            <div class="col col-md-5 no-gutters">
                                <b>{{ $enquiry->mobileno ?? "" }}</b>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col col-md-5 no-gutters">
                                Alternative Mobile No
                            </div>
                            <div class="col-6 col-md-5 no-gutters">
                                <b>{{ $enquiry->amobileno ?? "" }}</b>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col col-md-5 no-gutters">
                                Address Type
                            </div>
                            <div class="col col-md-5 no-gutters">
                                <b>{{ $enquiry->addresstype?? "" }}</b>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>
</div>
<div class="ecommerce-application">
    <h3>Enquired Product List</h3>

    <section class="list-view d-block ">
        <div class="" id="product_list">

            @foreach ($enquiry->enquiryProducts as $oproduct)
            @php
            $pmeta_data = json_decode($oproduct['meta_data'], true);
            $pmeta_data['campaign_name'];
            $product_categories = $pmeta_data['category'];
            $product = $oproduct->product;

            if($oproduct['discount'] != null)
            {
            $discount_price = $oproduct['price'] * ($oproduct['discount']/100);
            $discounted_price = $oproduct['price'] - $discount_price;
            }
            $img = $objimg = asset('XR/assets/images/placeholder.png');
            $obj_id = "";
            if(count($product->photo) > 0) {
            $img = $product->photo[0]->getUrl('thumb');
            $objimg = asset('XR/assets/images/placeholder.png');
            $obj_id = "";
            } else if(count($product->arobject) > 0) {
            if(!empty($product->arobject[0]->object))
            if(!empty($product->arobject[0]->object->getUrl()))
            $img = $product->arobject[0]->object->getUrl();
            }
            if(count($product->arobject) > 0){
            if(!empty($product->arobject[0]->object))
            if(!empty($product->arobject[0]->object->getUrl())) {
            $objimg = $product->arobject[0]->object->getUrl();
            $obj_id = $product->arobject[0]->id;
            }
            }
            @endphp
            <div class="card ecommerce-card" id="product_list_{{$product->id}}">
                <div class="card p-3">
                    <div class="row mb-1 w-100 " style="min-height:13rem;">
                        <div class="col-lg-3 col-md-3 col-sm-12 d-flex justify-content-center p-1 rounded">
                            @if(count($product->photo) > 1)
                            <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails h-100 w-100" data-interval="false" data-ride="carousel">
                                <!--Slides-->
                                <div class="carousel-inner h-100 w-100" role="listbox">
                                    @foreach($product->photo as $photo)
                                    <div class="carousel-item {{$loop->index == 0?'active':''}}  h-100 w-100 pl-1 d-flex justify-content-center">
                                        <img class="img-fluid mw-100 h-100 rounded" src="{{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl('thumb'):$objimg ?? ''}}" style="min-height:13rem;" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                    </div>
                                    @endforeach
                                </div>
                                <div class="w-100 position-absolute d-flex niceScroll" style="width: 200px; overflow: hidden; display: inline-block; margin: 0px auto; padding: 3px; outline: none;" tabindex="0">
                                    <ol class="carousel-indicators  position-relative">
                                        @foreach($product->photo as $photo)
                                        <li data-target="#carousel-thumb" data-slide-to="{{$loop->index}}" class="rounded-circle mr-1 {{$loop->index == 0?'active':''}}" style="width:50px; height:50px">
                                            <img class="d-flex  " style="width:50px;height:50px" src="{{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl('thumb'):$objimg ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                        </li>
                                        @endforeach
                                    </ol>
                                </div>
                            </div>
                            @else
                            <img class="img-fluid mw-100 rounded h-100" src="{{$img ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="min-height:13rem;">
                            @endif
                            @if(!empty($obj_id))
                            <div class="position-absolute" style="top:0;left:0;">
                                <div class="badge badge-glow badge-info">
                                    <img class="img-fluid" src="{{asset('XR/assets/images/ar.png')}}" style="width: 32px; max-height:32px;">
                                    <h6 class="text-white m-0">Tryon</h6>
                                </div>
                            </div>
                            @endif
                        </div>
                        <!--/.Carousel Wrapper-->
                        
                    <div class="col-lg-6 col-md-6 col-6 mt-3 p-1">
                        <div class="item-name">
                            <input type="hidden" class="tryonobj" value="{{$objimg ?? ''}}" />
                            <input type="hidden" class="obj_id" value="{{$obj_id ?? ''}}" />
                            <span>{{$oproduct['name'] ?? 'Untitled'}}</span>
                            <p class="item-company">
                                @if(count($product_categories) > 0)
                                <div class="badge badge-pill badge-glow badge-secondary text-dark mr-1">
                                    {{$product_categories[0]['name']}}
                                </div>
                                @endif
                            </p>
                        </div>
                        <div>
                            <span>{{$oproduct['sku'] ?? ''}}</span>
                        </div>

                        <div>
                            <h4 class="text-dark">{!! $pmeta_data['campaign_name'] !!}</h4>
                        </div>
                        @if($oproduct->notes)
                        <div style="border: 1px solid black;background: #e7e9ec;border-radius:5px;padding: 5px;">
                            <h4 class="text-dark">Notes :</h4>
                            <p>{{" ".$oproduct->notes}}</p>
                        </div>

                        @endif
                        <!-- <div>
                                    <p class="item-description update" data-pk="{{$product->id}}"
                                        data-url="{{route('admin.product_list.update',$product->id)}}" data-type="textarea"
                                        data-title="Enter description" data-name="description">
                                        {{$product->description ?? 'No Description'}}
                                    </p>
                                </div> -->
                        <div>
                            <span>{{$product->url ?? ''}}</span>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-6 p-2 mt-3">
                       <div class="item-wrapper">
                            <div class="item-rating">
                                @if(!empty($obj_id))
                                <!-- <a href="#" data-id="{{$product->id}}"
                                            class="badge badge-pill badge-glow badge-success">Try On</a> -->
                                @endif
                                <!-- <a href="#" onclick="deleteProduct($(this))" data-id="{{$product->id}}"
                                            class="badge badge-pill badge-glow badge-danger"><i
                                                class="feather icon-trash-2 m-0"></i></a> -->
                            </div>
                            <!-- <div class="item-cost"> -->
                            <div class="item-cost text-right">
                                <h6 class="item-price">
                                    {!! $emeta_data['currency'] !!}
                                    @if($oproduct['discount'] != null)
                                    {{$discounted_price}}
                                    <span class="text-primary"><s>{{$oproduct['price']}}</s></span>
                                    <br><span class="text-warning" style="font-size: 80%;">{{$oproduct['discount'] ?? ''}}%</span>
                                    @else
                                    <span>{{$oproduct['price'] ?? '00.00'}}</span>
                                    @endif
                                </h6>
                            </div>
                            <div class="item-cost text-right">
                                <h6 class="item-price">
                                    Qty: <span>{{$oproduct['qty'] ?? ''}}</span>
                                </h6>

                            </div>
                            <!-- </div> -->
                        </div>
                        <div class="wishlist bg-white">
                            <!-- <i class="fa fa-heart-o mr-25"></i> Wishlist -->
                        </div>
                        <!-- <span class="cart p-0">
                            <a href="{{route('admin.products.edit',$product->id)}}" class="btn btn-block text-white btn-primary"
                                data-color="primary">Edit
                            </a>
                        </span> -->
                    </div>
                </div>
            </div>
            @endforeach
            @if(count($enquiry->enquiryProducts) == 0)
            <h3 class="text-light text-center p-2">No Products added yet, please add</h3>
            @endif
        </div>
    </section>
</div>
@endsection
@section('scripts')
@parent
<script>
    $('#enquiry_status').on('change', function() {
        // console.log($('#enquiry_status').val());
        $.ajax({
            url: "{{route('admin.enquiry.updateStatus')}}",
            method: 'POST',
            data: {
                enquiry_status: $('#enquiry_status').val(),
                enquiry_id: '{{$enquiry->enquiry->id}}'
            },
        })
    });

    $(".niceScroll").niceScroll({
        cursorcolor: "grey", // change cursor color in hex
        cursoropacitymin: 0, // change opacity when cursor is inactive (scrollabar "hidden" state), range from 1 to 0
        cursoropacitymax: 1, // change opacity when cursor is active (scrollabar "visible" state), range from 1 to 0
        cursorwidth: "5px", // cursor width in pixel (you can also write "5px")
        cursorborder: "1px solid #fff", // css definition for cursor border
        cursorborderradius: "5px", // border radius in pixel for cursor
        zindex: "auto", // change z-index for scrollbar div
        scrollspeed: 60, // scrolling speed
        mousescrollstep: 40, // scrolling speed with mouse wheel (pixel)
        touchbehavior: false, // enable cursor-drag scrolling like touch devices in desktop computer
        hwacceleration: true, // use hardware accelerated scroll when supported
        boxzoom: false, // enable zoom for box content
        dblclickzoom: true, // (only when boxzoom=true) zoom activated when double click on box
        gesturezoom: true, // (only when boxzoom=true and with touch devices) zoom activated when pinch out/in on box
        grabcursorenabled: true, // (only when touchbehavior=true) display "grab" icon
        autohidemode: true, // how hide the scrollbar works, possible values: 
        background: "", // change css for rail background
        iframeautoresize: true, // autoresize iframe on load event
        cursorminheight: 32, // set the minimum cursor height (pixel)
        preservenativescrolling: true, // you can scroll native scrollable areas with mouse, bubbling mouse wheel event
        railoffset: false, // you can add offset top/left for rail position
        bouncescroll: false, // (only hw accell) enable scroll bouncing at the end of content as mobile-like 
        spacebarenabled: true, // enable page down scrolling when space bar has pressed
        disableoutline: true, // for chrome browser, disable outline (orange highlight) when selecting a div with nicescroll
        horizrailenabled: true, // nicescroll can manage horizontal scroll
        railalign: "right", // alignment of vertical rail
        railvalign: "bottom", // alignment of horizontal rail
        enabletranslate3d: true, // nicescroll can use css translate to scroll content
        enablemousewheel: true, // nicescroll can manage mouse wheel events
        enablekeyboard: true, // nicescroll can manage keyboard events
        smoothscroll: true, // scroll with ease movement
        sensitiverail: true, // click on rail make a scroll
        enablemouselockapi: true, // can use mouse caption lock API (same issue on object dragging)
        cursorfixedheight: false, // set fixed height for cursor in pixel
        hidecursordelay: 400, // set the delay in microseconds to fading out scrollbars
        irectionlockdeadzone: 6, // dead zone in pixels for direction lock activation
        nativeparentscrolling: true, // detect bottom of content and let parent to scroll, as native scroll does
        enablescrollonselection: true, // enable auto-scrolling of content when selection text
        cursordragspeed: 0.3, // speed of selection when dragged with cursor
        rtlmode: "auto", // horizontal div scrolling starts at left side
        cursordragontouch: false, // drag cursor in touch / touchbehavior mode also
        oneaxismousemode: "auto",
        scriptpath: "", // define custom path for boxmode icons ("" => same script path)
        preventmultitouchscrolling: true, // prevent scrolling on multitouch events
        disablemutationobserver: false,
    });
</script>
@endsection