@extends('layouts.admin')
@section('styles')
@parent
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/plugins/file-uploaders/dropzone.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-user.css')}}">
<style>
.dropzone .dz-message {
height: auto !important;
}

.dz-image img {
    width: 150px;
}
</style>
@endsection
@section('content')
@if (session('error'))
<div class="alert alert-warning">{{ session('error') }}</div>
@endif

<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <a href="{{route('admin.campaigns.edit',$campaign->id)}}"><h4 class="text-primary"><i class="feather icon-arrow-left"></i> Back to Campaign</h4></a>
                <h2 class="content-header-title float-left mb-0">Campaign Options</h2>
            </div>
        </div>
    </div>
</div>

<!-- account setting page start -->
<section>
    <div class="row">
        <!-- left menu section -->
        <div class="col-md-3 mb-2 mb-md-0">
            <ul class="nav nav-pills flex-column mt-md-0 mt-1">
                <li class="nav-item">
                    <a class="nav-link d-flex py-75 active" id="campaign-pill-Splash" data-toggle="pill"
                        href="#splash-screen" aria-expanded="true">
                        <i class="feather icon-zap mr-50 font-medium-3"></i>
                        Splash Screen
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="storefront-pill" data-toggle="pill"
                        href="#storefront" aria-expanded="false">
                        <i class="fa fa-home mr-50 font-medium-3"></i>
                        Store Front
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="campaign-pill-settings" data-toggle="pill"
                        href="#campaign-settings" aria-expanded="false">
                        <i class="feather icon-settings mr-50 font-medium-3"></i>
                        Settings
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="account-pill-password" data-toggle="pill"
                        href="#others" aria-expanded="false">
                        <i class="feather icon-package mr-50 font-medium-3"></i>
                        Others
                    </a>
                </li>
               
            </ul>
        </div>
        <!-- right content section -->
        <div class="col-md-9">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="splash-screen"
                                aria-labelledby="campaign-pill-Splash" aria-expanded="true">
                                <form method="POST" id="formsplashscreen" action="{{ route("admin.campaignOption.optionsUpdate") }}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="hash" value="splash-screen"/>
                                <input type="hidden" name="id" value="{{$campaign->id}}"/>
                                <div class="dropzone dropzone-area {{ $errors->has('photo') ? 'is-invalid' : '' }}"
                                        id="splashlogo">
                                        <div class="dz-message">Upload Brand logo</div>
                                    </div>
                                <hr>
                               
                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label for="splash-brandname">Brand Name</label>
                                                    <input type="text" class="form-control" 
                                                        placeholder="Brand Name" value="{{ old('brand_name', $meta_data['splash']['brand_name'] ?? '') }}" name="brand_name">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label for="splash-brandnamecolor">Brand Name Color</label>
                                                    <input type="color" class="form-control" name="brand_color"
                                                        placeholder="Brand Name Color" value="{{ old('brand_color', $meta_data['splash']['brand_color'] ?? '')}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label for="splash-loader_type">Loader Type</label>
                                                    <select class="form-control {{ $errors->has('loader_type') ? 'is-invalid' : '' }}" name="loader_type">
                                                        @foreach(App\Campaign::LOADER_TYPE as $key => $label)
                                                            <option value="{{ $key }}" {{ old('loader_type', $meta_data['splash']['loader_type'] ?? 'border') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label for="splash-loader_color">Loader Color</label>
                                                    <input type="color" class="form-control" name="loader_color"
                                                        placeholder="Background Color" value="{{ old('loader_color', $meta_data['splash']['loader_color'] ?? '') }}">
                                                </div>
                                            </div>
                                        </div>
                                        

                                        <div class="col-12 col-sm-6">
                                        <div class="form-group">
                                                        <label>Show Brand name or logo</label>
                                                        <select class="form-control {{ $errors->has('brand_type') ? 'is-invalid' : '' }}" name="brand_type">
                                                        @foreach(App\Campaign::BRAND_TYPE as $key => $label)
                                                            <option value="{{ $key }}" {{ old('brand_type', $meta_data['splash']['brand_type'] ?? '1') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                                                        @endforeach
                                                    </select>
                                                        <!-- <ul class="list-unstyled mb-0">
                                                            <li class="d-inline-block mr-2">
                                                                <fieldset>
                                                                    <div class="vs-radio-con">
                                                                    <input class="form-check-input" type="radio" name="brand" value="1">
                                                                        <span class="vs-radio">
                                                                            <span class="vs-radio--border"></span>
                                                                            <span class="vs-radio--circle"></span>
                                                                        </span>Brand name
                                                                    </div>
                                                                </fieldset>
                                                            </li>
                                                            <li class="d-inline-block mr-2">
                                                                <fieldset>
                                                                    <div class="vs-radio-con">
                                                                    <input class="form-check-input" type="radio" name="brand" value="2">
                                                                        <span class="vs-radio">
                                                                            <span class="vs-radio--border"></span>
                                                                            <span class="vs-radio--circle"></span>
                                                                        </span>Brand logo
                                                                    </div>
                                                                </fieldset>
                                                            </li>
                                                        </ul> -->
                                                    </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label for="account-name">Background Color</label>
                                                    <input type="color" class="form-control" name="bg_color"
                                                        placeholder="Background Color" value="{{ old('bg_color', $meta_data['splash']['bg_color'] ?? '') }}" required
                                                        data-validation-required-message="This field is required">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                            <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save
                                                changes</button>
                                            <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="tab-pane fade " id="campaign-settings" role="tabpanel"
                                aria-labelledby="campaign-pill-settings" aria-expanded="false">
                                <form method="POST" action="{{ route("admin.campaignOption.optionsUpdate") }}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="hash" value="campaign-settings"/>
                                <input type="hidden" name="id" value="{{$campaign->id}}"/>
                                    <div class="row">
                                        <div class="col-6">
                                        <div class="form-group">
                                            <div class="controls">
                                                <label>Subdomain</label>
                                                <fieldset>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" placeholder="subdomain" aria-describedby="basic-addon2" name="subdomain" id="subdomain" value="{{ old('subdomain', $meta_data['settings']['subdomain'] ?? '') }}">
                                                        <div class="input-group-append">
                                                            <span class="input-group-text" id="basic-addon2">.{{env('SITE_URL')}}</span>
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                            
                                        </div>
                                        <div class="col-6">
                                        <div class="form-group">
                                            <div class="controls">
                                                <label>Custom domain</label>
                                                <fieldset>
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" placeholder="custom domain" name="customdomain" id="customdomain" value="{{ old('customdomain', $meta_data['settings']['customdomain'] ?? '') }}">
                                                    </div>
                                                </fieldset>
                                            </div>
                                        </div>
                                            
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label>Whatsapp Number</label>
                                                    <input type="text" class="form-control" value="{{ old('whatsapp', $meta_data['settings']['whatsapp'] ?? '') }}" name="whatsapp" placeholder="Whatsapp number here...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <hr class="hr-text" data-content="Email Setting" />
                                            <div class="row">
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>Email ID</label>
                                                        <input type="text" class="form-control" value="{{ old('emailid', $meta_data['settings']['emailid'] ?? '') }}" name="emailid" placeholder="Email ID here...">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>Email From Name</label>
                                                        <input type="text" class="form-control" value="{{ old('emailfromname', $meta_data['settings']['emailfromname'] ?? '') }}" name="emailfromname" placeholder="Email From Name here...">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>Contact Number</label>
                                                        <input type="mobile" class="form-control" value="{{ old('contactnumber', $meta_data['settings']['contactnumber'] ?? '') }}" name="contactnumber" placeholder="Contact Number here...">
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                            <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save
                                                changes</button>
                                            <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="tab-pane fade " id="storefront" role="tabpanel"
                                aria-labelledby="storefront-pill" aria-expanded="false">
                                <form method="POST" id="formstorefront" action="{{ route("admin.campaignOption.optionsUpdate") }}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="hash" value="storefront"/>
                                <input type="hidden" name="id" value="{{$campaign->id}}"/>
                                    <div class="row">
                                        <div class="col-12">
                                        <div class="dropzone dropzone-area" id="bannerimg">
                                          <div class="dz-message">Upload Banner Image</div>
                                        </div>
                                    </div>
                                        </div>
                                    <hr>
                                        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                            <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save
                                                changes</button>
                                            <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="tab-pane fade " id="others" role="tabpanel"
                                aria-labelledby="others-pill" aria-expanded="false">
                                <form novalidate>
                                    <div class="row">
                                        <div class="col-12">
                                        </div>
                                        <!-- <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                            <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save
                                                changes</button>
                                            <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                        </div> -->
                                    </div>
                                </form>
                            </div>
                           
                            
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- account setting page end -->
@endsection

@section('scripts')
@parent
<script>
    var hash = window.location.hash;
  hash && $('ul.nav a[href="' + hash + '"]').tab('show');

  $('.nav-item .nav-link').click(function (e) {
    $(this).tab('show');
    var scrollmem = $('body').scrollTop();
    window.location.hash = this.hash;
  });
    </script>
<script>
    Dropzone.autoDiscover = false;
    myDropzone = new Dropzone('#splashlogo', {
        url: '{{ route('admin.campaignOption.storeMedia') }}',
    maxFilesize: 40, // MB
    maxFiles: 1,
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 40
    },
    success: function (file, response) {
      $('#formsplashscreen').find('input[name="splashlogo"]').remove()
      $('#formsplashscreen').append('<input type="hidden" name="splashlogo" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('#formsplashscreen').find('input[name="splashlogo"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
        @if(isset($campaign) && $campaign->splashlogo)
            var file = {!! json_encode($campaign->splashlogo) !!}
                this.options.addedfile.call(this, file)
            file.previewElement.classList.add('dz-complete')
            $('#formsplashscreen').append('<input type="hidden" name="splashlogo" value="' + file.file_name + '">')
            this.options.maxFiles = this.options.maxFiles - 1
        @endif
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
    
});
var uploadedBannerimgMap = {}
var myDropzone1 = new Dropzone('#bannerimg', {
        url: '{{ route('admin.campaignOption.storeMedia') }}',
    maxFilesize: 40, // MB
    maxFiles: 5,
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 40,
      width: 4086,
      height: 4096
    },
    success: function (file, response) {
      $('#bannerimg').append('<input type="hidden" name="bannerimg[]" value="' + response.name + '">')
      uploadedBannerimgMap[file.name] = response.name
    },
    removedfile: function (file) {
    //   console.log(file)
      file.previewElement.remove()
      var name = ''
      if (typeof file.file_name !== 'undefined') {
        name = file.file_name
      } else {
        name = uploadedBannerimgMap[file.name]
      }
      $('#bannerimg').find('input[name="bannerimg[]"][value="' + name + '"]').remove()
    },
    init: function () {
@if(isset($campaign) && $campaign->bannerimg)
      var files =
        {!! json_encode($campaign->bannerimg) !!}
        var j = 0;
          for (var i in files) {
              j++;
          var file = files[i]
          if(files.length == j) {
              this.options.addedfile.call(this, file)
              this.options.thumbnail.call(this, file, file.url)
              file.previewElement.classList.add('dz-complete')
              $('#bannerimg').append('<input type="hidden" name="bannerimg[]" value="' + file.file_name + '">')
          }
        }
@endif
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
    
});
</script>
@endsection