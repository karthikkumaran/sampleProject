@extends('layouts.admin')
@section('styles')
@parent
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/plugins/file-uploaders/dropzone.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-user.css')}}">
<style>
    .dropzone .dz-message {
        height: auto !important;
    }

    .otp-instructions {
        font-size: 18px;
    }

    .dz-image img {
        width: 150px;
    }

    .image-area {
        position: relative;
        width: 100%;
        /* background: #333; */
        8
    }

    .image-area img {
        max-width: 100%;
        height: auto;
    }

    .remove-image {
        display: inline;
        position: absolute;
        top: -10px;
        right: -10px;
        border-radius: 10em;
        padding: 2px 6px 3px;
        text-decoration: none;
        font: 15px sans-serif;
        background: red;
        border: 3px solid #fff;
        color: white;
        box-shadow: 0 2px 6px rgba(0, 0, 0, 0.5), inset 0 2px 4px rgba(0, 0, 0, 0.3);
        text-shadow: 0 1px 2px rgba(0, 0, 0, 0.5);
        -webkit-transition: background 0.5s;
        transition: background 0.5s;
    }

    .remove-image:hover {
        color: red;
        background: white;
        padding: 3px 7px 5px;
        top: -11px;
        right: -11px;
    }

    .remove-image:active {
        background: red;
        top: -10px;
        right: -11px;
    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    td,
    th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }
</style>
@endsection
@php
$theme_name = $meta_data["themes"]["name"] ?? env('DEFAULT_STOREFRONT_THEME');
$default_color_path = "../resources/views/themes/" . $theme_name . "/partials/default_color.php";
if(file_exists($default_color_path)){
include($default_color_path);
}
if(empty($meta_data['settings']['currency'])){
$currency = "₹";
}
else{
if($meta_data['settings']['currency_selection'] == "currency_symbol")
$currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
else
$currency = $meta_data['settings']['currency'];
}
@endphp
@section('content')
@if (session('error'))
<div class="alert alert-warning">{{ session('error') }}</div>
@endif

<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <h2 class="content-header-title float-left mb-0">Settings</h2>
            </div>
        </div>
    </div>
</div>

<!-- account setting page start -->
<section>
    <div class="row">
        <!-- left menu section -->
        <div class="col-md-3 mb-2 mb-md-0">
            <ul class="nav nav-pills flex-column mt-md-0 mt-1">
                <li class="nav-item">
                    <a class="nav-link d-flex py-75 active" id="storefront-pill" data-toggle="pill" href="#storefront" aria-expanded="true">
                        <i class="fa fa-home mr-50 font-medium-3"></i>
                        Storefront
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="campaign-pill-Splash" data-toggle="pill" href="#splash-screen" aria-expanded="false">
                        <i class="feather icon-zap mr-50 font-medium-3"></i>
                        Splash Screen
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="storefront-pill-theme" data-toggle="pill" href="#themes" aria-expanded="false">
                        <i class="fa fa-paint-brush" aria-expanded="false"></i>&nbsp;
                        Themes
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="announcement-pill-settings" data-toggle="pill" href="#announcement-settings" aria-expanded="false">
                        <i class="fa fa-bullhorn mr-50 font-medium-3"></i>
                        Announcements
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="checkout-pill-settings" data-toggle="pill" href="#checkout-settings" aria-expanded="false">
                        <i class="feather icon-shopping-cart mr-50 font-medium-3"></i>
                        Checkout
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="order-template-pill-settings" data-toggle="pill" href="#order-template-setting" aria-expanded="false">
                        <i class="fa fa-file mr-50 font-medium-3"></i>
                        Orders Template
                    </a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="invoice-tempelate-pill-settings" data-toggle="pill" href="#invoice-tempelate-setting" aria-expanded="false">
                        <i class="fa fa-file mr-50 font-medium-3"></i>
                        Invoice Template
                    </a>
                </li> -->
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="invoice-pill-settings" data-toggle="pill" href="#invoice-settings" aria-expanded="false">
                        <i class="fa fa-file mr-50 font-medium-3"></i>
                        Invoice
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="payment-pill-settings" data-toggle="pill" href="#payment-settings" aria-expanded="false">
                        <i class="fa fa-credit-card mr-50 font-medium-3"></i>
                        Payment
                    </a>
                </li>
                <li class="nav-item" style="display:hide;">
                    <a class="nav-link d-flex py-75" id="shipping-pill-settings" data-toggle="pill" href="#shipping-settings" aria-expanded="false">
                        <i class="fa fa-truck mr-50 font-medium-3"></i>
                        Shipping
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="notification-pill-settings" data-toggle="pill" href="#notification-settings" aria-expanded="false">
                        <i class="fa fa-bell mr-50 font-medium-3"></i>
                        Notification
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="telegram-pill-settings" data-toggle="pill" href="#telegram-settings" aria-expanded="false">
                        <i class="fa fa-telegram mr-50 font-medium-3"></i>
                        Telegram
                    </a>
                </li>
                @if(session('subscribed_features'))
                    @if(array_key_exists("pos", session('subscribed_features')[0]))
                    <li class="nav-item">
                        <a class="nav-link d-flex py-75" id="pos-pill-settings" data-toggle="pill" href="#pos-settings" aria-expanded="false">
                            <i class="fa fa-file mr-50 font-medium-3"></i>
                            Point Of Sales
                        </a>
                    </li>
                    @endif
                @endif

                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="footer-pill-settings" data-toggle="pill" href="#footer-settings" aria-expanded="false">
                        <i class="feather icon-settings mr-50 font-medium-3"></i>
                        Footer
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="campaign-pill-settings" data-toggle="pill" href="#campaign-settings" aria-expanded="false">
                        <i class="feather icon-settings mr-50 font-medium-3"></i>
                        Store
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="content-pill-settings" data-toggle="pill" href="#content-settings" aria-expanded="false">
                        <i class="fa fa-file-text-o mr-50 font-medium-3"></i>
                        Content
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="try-on-pill-settings" data-toggle="pill" href="#try-on-settings" aria-expanded="false">
                        <i class="feather icon-settings mr-50 font-medium-3"></i>
                        Try On
                    </a>
                </li>
                
                @if ( app('impersonate')->isImpersonating())
                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="analytic-pill-settings" data-toggle="pill" href="#analytic-settings" aria-expanded="false">
                        <i class="feather icon-settings mr-50 font-medium-3"></i>
                        Analytics
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="custom-script-pill-settings" data-toggle="pill" href="#custom-script-settings" aria-expanded="false">
                        <i class="feather icon-settings mr-50 font-medium-3"></i>
                        Custom Script
                    </a>
                </li>
               @endcan

                <!-- <li class="nav-item">
                    <a class="nav-link d-flex py-75" id="account-pill-password" data-toggle="pill"
                        href="#others" aria-expanded="false">
                        <i class="feather icon-package mr-50 font-medium-3"></i>
                        Others
                    </a>
                </li> -->

            </ul>
        </div>
        <!-- right content section -->
        <div class="col-md-9">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                        <div class="tab-content">
                            <!-- splash screen settings start -->
                            <div role="tabpanel" class="tab-pane fade" id="splash-screen" aria-labelledby="campaign-pill-Splash" aria-expanded="false">
                                <form method="POST" id="formsplashscreen" action="{{ route("admin.userSettings.update") }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="hash" value="splash-screen" />
                                    <!-- <div class="dropzone dropzone-area {{ $errors->has('photo') ? 'is-invalid' : '' }}"
                                        id="splashlogo">
                                        <div class="dz-message">Upload Brand logo</div>
                                    </div> -->
                                    <label>Splash screen image</label>
                                    <div class="row">
                                        <div class="col-12 mt-1">
                                            @if($campaign->splashlogo != null)
                                            <input type="hidden" name='splashlogo' value="{{$campaign->splashlogo->file_name}}">
                                            <div class="image-area" id="logo_{{$campaign->splashlogo->id}}" style="width: 200px; height: 200px;">
                                                <div style="height:200px;">
                                                    <img class="d-block img-fluid" style="width:100%;min-height:50%;max-height:100%;" src="{{$campaign->splashlogo->getUrl() ?? asset('assets/images/placholder.png')}}">
                                                    <a class="remove-image" href="javascript:void(0)" onclick="removesplashlogoimg({{$campaign->splashlogo->id}},'{{$campaign->splashlogo->file_name}}')" style="display: inline;"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                            @endif
                                            <div style="height:85px;width: 120px;text-align: center;border-style:dashed;padding: 7px;" id="logo_btn" class="rounded mb-0">
                                                <a data-toggle="modal" data-target="#splashUploadimgModal" href="{{ route('admin.userSettings.storeMedia') }}" style="color: #626262;">
                                                    <div class="image">
                                                        <i class="feather icon-plus-circle font-large-1 "></i>
                                                    </div>
                                                    <span style="display: block;" class=' pb-1'>Upload image</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Splash upload Modal -->
                                    <div class="modal fade ecommerce-application" id="splashUploadimgModal" tabindex="-1" role="dialog" aria-labelledby="bannerSelectModalTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="splashSelectModalTitle">Splash screen logo upload image</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="dropzone dropzone-area {{ $errors->has('photo') ? 'is-invalid' : '' }}" id="splashlogo">
                                                        <div class="dz-message">Upload splash screen logo</div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancel</button>
                                                    <button type="button" class="btn btn-primary" id="splashLogoAdd" data-dismiss="modal" aria-label="Close" disabled="true">Add</button>
                                                    <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Splash Upload Modal -->

                                    <hr>

                                    <div class="row">
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label for="splash-brandname">Splash screen name</label>
                                                    <input type="text" class="form-control" placeholder="Store Name" value="{{ old('brand_name', $meta_data['splash']['brand_name'] ?? '') }}" name="brand_name" maxlength="35">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label for="splash-brandnamecolor">Splash screen color</label>
                                                    <input type="color" class="form-control" name="brand_color" placeholder="Brand Name Color" value="{{ old('brand_color', $meta_data['splash']['brand_color'] ?? '#000000')}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label for="splash-loader_type">Loader type</label>
                                                    <select class="form-control {{ $errors->has('loader_type') ? 'is-invalid' : '' }}" name="loader_type">
                                                        @foreach(App\Campaign::LOADER_TYPE as $key => $label)
                                                        <option value="{{ $key }}" {{ old('loader_type', $meta_data['splash']['loader_type'] ?? 'border') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label for="splash-loader_color">Loader color</label>
                                                    <input type="color" class="form-control" name="loader_color" placeholder="Background Color" value="{{ old('loader_color', $meta_data['splash']['loader_color'] ?? '#000000') }}">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <label>Show splash screen name or logo</label>
                                                <select class="form-control {{ $errors->has('brand_type') ? 'is-invalid' : '' }}" name="brand_type">
                                                    @foreach(App\Campaign::BRAND_TYPE as $key => $label)
                                                    <option value="{{ $key }}" {{ old('brand_type', $meta_data['splash']['brand_type'] ?? '1') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label for="account-name">Background color</label>
                                                    <input type="color" class="form-control" name="bg_color" placeholder="Background Color" value="{{ old('bg_color', $meta_data['splash']['bg_color'] ?? '#FFFFFF') }}" required data-validation-required-message="This field is required">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                            <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save
                                                changes</button>
                                            <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- splash screen settings end -->

                            <!-- themes settings start -->
                            <div class="tab-pane fade" id="themes" role="tabpanel" aria-labelledby="storefront-pill-theme" aria-expanded="false">
                                <form method="POST" id="formthemes" action="{{ route("admin.userSettings.update") }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="hash" value="themes" />
                                    <label for="themes">Themes</label>
                                    <hr>
                                    <section class="grid-view d-block mb-4">
                                        <div class="row mb-1">
                                            @foreach($dir as $fileinfo)
                                            @if($fileinfo->isDir() && !$fileinfo->isDot())
                                            @php
                                            $newpath = "../resources/views/themes/" . $fileinfo . "/";
                                            $img = "../public/themes/" . $fileinfo . "/screenshot.png";
                                            $layouts = "../resources/views/themes/" . $fileinfo . "/layouts";
                                            $pages = "../resources/views/themes/" . $fileinfo . "/pages";
                                            $partials = "../resources/views/themes/" . $fileinfo . "/partials";
                                            @endphp
                                            @if(file_exists($img) && file_exists($layouts) && file_exists($pages) && file_exists($partials) &&(($fileinfo->getFilename()!='rustic-theme' || app('impersonate')->isImpersonating() )))
                                            <div class="col-lg-3 justify-content-center">
                                                <div class="card ecommerce-card">
                                                    <div class="card-content">
                                                        <div class="item-img text-center" style="min-height: 5rem;">
                                                            <img src="{{asset('themes/'.$fileinfo.'/screenshot.png')}}" style="height:75px; width:110px;">
                                                        </div>
                                                        <div class="card-body p-0 pt-1 pb-1 text-center">
                                                            <div class="item-name pb-1 justify-content-center">
                                                                @php
                                                                $theme_name = $fileinfo->getFilename();
                                                                $default_color_path = "../resources/views/themes/" . $theme_name . "/partials/default_color.php";
                                                                if(file_exists($default_color_path))
                                                                $custom_color = true;
                                                                else
                                                                $custom_color = false;
                                                                @endphp
                                                                <input type="radio" name="name" value="{{$theme_name}}" data-custom_color="{{$custom_color}}" {{ old('name', $meta_data['themes']['name'] ?? env('DEFAULT_STOREFRONT_THEME')) ===  $theme_name ? 'checked' : '' }}>&nbsp;{{$theme_name}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            @endif

                                            @endforeach
                                        </div>
                                        <div class="row mb-1">
                                            <div class="col-12">
                                                <label for="">How do you want the product details to be shown</label>
                                                <hr>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group custom-control custom-switch custom-switch-success">
                                                    <span class="switch-label mr-1"><b>Modal popup</b></span>
                                                    <input type="checkbox" class="custom-control-input" value="new_page" id="product_details_display" name="product_details_display" @if(isset($meta_data['themes']['product_details_display'])) {{ old('product_details_display',$meta_data['themes']['product_details_display']) == "new_page" ? 'checked':''}} @endif>
                                                    <label class="custom-control-label" for="product_details_display"></label>
                                                    <span class="switch-label"><b>New page</b></span>
                                                </div>
                                            </div>
                                        </div>
                                        @if(isset($background_default_color) || isset($text_default_color))
                                        <div class="row mb-1" id="theme_custom_color">
                                            <div class="col-12">
                                                <label for="custom_color">Custom colour(These colours will be reset to default if you change the theme)</label>
                                                <hr>
                                            </div>
                                            @if(isset($background_default_color))
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <label class="m-1" for="background_color"> Button color</label>
                                                </div>
                                                <div class="row">
                                                    <div class="col-2">
                                                        <input type="color" class="form-control" name="theme_background_color" value="{{ old('theme_background_color', $meta_data['themes']['theme_background_color'] ?? $background_default_color)}}" style="width: 3rem; padding: 0.1rem;">
                                                    </div>
                                                    <div class="col-10">
                                                        <button type="button" class="btn btn-info" onclick="resetThemeBackgroundColor()">Reset to default</button>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            @if(isset($text_default_color))
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <label class="m-1" for="text_color"> Header colour</label>
                                                </div>
                                                <div class="row">
                                                    <div class="col-2">
                                                        <input type="color" class="form-control" name="theme_text_color" value="{{ old('theme_text_color', $meta_data['themes']['theme_text_color'] ?? $text_default_color)}}" style="width: 3rem; padding: 0.1rem;">
                                                    </div>
                                                    <div class="col-10">
                                                        <button type="button" class="btn btn-info" onclick="resetThemeTextColor()">Reset to default</button>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            @if(isset($bg_body_color))
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <label class="m-1" for="bg_body_color"> Body colour</label>
                                                </div>
                                                <div class="row">
                                                    <div class="col-2">
                                                        <input type="color" class="form-control" name="theme_body_color" value="{{ old('theme_body_color', $meta_data['themes']['theme_body_color'] ?? $bg_body_color)}}" style="width: 3rem; padding: 0.1rem;">
                                                    </div>
                                                    <div class="col-10">
                                                        <button type="button" class="btn btn-info" onclick="resetThemeBodyColor()">Reset to default</button>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            @if(isset($bg_footer_color))
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <label class="m-1" for="bg_footer_color">Footer colour</label>
                                                </div>
                                                <div class="row">
                                                    <div class="col-2">
                                                        <input type="color" class="form-control" name="theme_footer_color" value="{{ old('theme_footer_color', $meta_data['themes']['theme_footer_color'] ?? $bg_footer_color)}}" style="width: 3rem; padding: 0.1rem;">
                                                    </div>
                                                    <div class="col-10">
                                                        <button type="button" class="btn btn-info" onclick="resetThemeFooterColor()">Reset to default</button>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            @if(isset($bg_mobNav_color))
                                            <div class="col-lg-6">
                                                <div class="row">
                                                    <label class="m-1" for="bg_mobNav_color">Mobile Bottom Navigation colour</label>
                                                </div>
                                                <div class="row">
                                                    <div class="col-2">
                                                        <input type="color" class="form-control" name="theme_mobNav_color" value="{{ old('theme_mobNav_color', $meta_data['themes']['theme_mobNav_color'] ?? $bg_mobNav_color)}}" style="width: 3rem; padding: 0.1rem;">
                                                    </div>
                                                    <div class="col-10">
                                                        <button type="button" class="btn btn-info" onclick="resetThemeMobNavColor()">Reset to default</button>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            
                                            
                                        </div>
                                        @endif
                                        <div class="row d-flex flex-sm-row flex-column justify-content-end">
                                            <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save changes</button>
                                            <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                        </div>
                                    </section>

                                </form>
                            </div>
                            <!-- themes settings end -->

                            <!-- announcement settings start -->
                            <div class="tab-pane fade" id="announcement-settings" role="tabpanel" aria-labelledby="announcement-pill-theme" aria-expanded="false">
                                <form method="POST" id="formannouncement" action="{{ route("admin.userSettings.update") }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="hash" value="announcement-settings" />
                                    <hr class="hr-text mt-0" data-content="Announcement Settings" />
                                    <section class="grid-view d-block mb-4">
                                        <div class="row mb-1">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <div class="controls">
                                                        <label>Announcement Text</label>
                                                        <textarea type="text" class="form-control" name="announcement_text" value="" placeholder="Announcement text here..." maxlength="255">{{ old('announcement_text', $meta_data['announcement_settings']['announcement_text'] ?? '') }}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6  col-md-12">
                                                <div class="row">
                                                    <label class="m-1" for="announcement_background_color">Set text color</label>
                                                </div>
                                                <div class="row">
                                                    <div class="col-2">
                                                        <input type="color" class="form-control" name="announcement_text_color" value="{{ old('announcement_text_color', $meta_data['announcement_settings']['announcement_text_color'] ?? '#FFFFFF')}}" style="width: 3rem; padding: 0.1rem;">
                                                    </div>
                                                    <div class="col-10">
                                                        <button type="button" class="btn btn-info" onclick="resetAnnouncementTextColor()">Reset to default</button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-6  col-md-12">
                                                <div class="row">
                                                    <label class="m-1" for="announcement_background_color">Set background color</label>
                                                </div>
                                                <div class="row">
                                                    <div class="col-2">
                                                        <input type="color" class="form-control" name="announcement_background_color" value="{{ old('announcement_background_color', $meta_data['announcement_settings']['announcement_background_color'] ?? '#FF0000')}}" style="width: 3rem; padding: 0.1rem;">
                                                    </div>
                                                    <div class="col-10">
                                                        <button type="button" class="btn btn-info" onclick="resetAnnouncementBackgroundColor()">Reset to default</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row d-flex flex-sm-row flex-column justify-content-end">
                                            <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save changes</button>
                                            <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                        </div>
                                    </section>
                                </form>
                            </div>
                            <!-- announcement settings end -->

                            <!-- store settings start -->
                            <div class="tab-pane fade " id="campaign-settings" role="tabpanel" aria-labelledby="campaign-pill-settings" aria-expanded="false">
                                <form method="POST" action="{{ route("admin.userSettings.update") }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="hash" value="campaign-settings" />
                                    <hr class="hr-text mt-0" data-content="Domain Setting" />

                                    <div class="row">
                                        <div class="col-12 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label>Subdomain</label>
                                                    <fieldset>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" style="text-transform:lowercase" placeholder="subdomain" aria-describedby="basic-addon2" name="subdomain" id="subdomain" value="{{ old('subdomain', $meta_data['settings']['subdomain'] ?? '') }}">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text" id="basic-addon2">.{{env('SITE_URL')}}</span>
                                                            </div>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>

                                        </div>
                                        @can('custom_domain')
                                        <div class="col-12 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label>Custom domain</label>
                                                    <fieldset>
                                                        <div class="input-group">
                                                            <input type="text" class="form-control" style="text-transform:lowercase" placeholder="custom domain" name="customdomain" id="customdomain" value="{{ old('customdomain', $meta_data['settings']['customdomain'] ?? '') }}" pattern="(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]">
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>
                                        @endcan
                                        <div class="col-12">
                                            <hr class="hr-text" data-content="Enquiry Setting" />
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label for="whatsapp_country_code">Whatsapp Country Code</label>
                                                            <select id="whatsapp_country_code" class="form-control" name="whatsapp_country_code" required>
                                                                @foreach(App\Customer::MOBILE_COUNTRY_CODE as $key=>$value)
                                                                <option value="{{$key}}" {{ old('whatsapp_country_code', $meta_data['settings']['whatsapp_country_code'] ?? '91') ==  $key ? 'selected' : '' }}>{{$value}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-12">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>Whatsapp Number</label>
                                                            <input type="tel" class="form-control" value="{{ old('whatsapp', $meta_data['settings']['whatsapp'] ?? '') }}" name="whatsapp" placeholder="Enter your whatsapp number here with country code..." onkeypress="return isNumberKey(event,this)" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>Enquiry Text</label>
                                                            <input type="text" class="form-control" value="{{ old('enquiry_text', $meta_data['settings']['enquiry_text'] ?? 'Hello! I found your online store and I have few questions regarding online ordering. Are you free to chat now?') }}" name="enquiry_text" placeholder="Enquiry text here...">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <hr class="hr-text" data-content="Email Setting" />
                                            <div class="row">
                                                <div class="col-12 col-md-6 col-sm-12">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>Email ID</label>
                                                            <input type="email" class="form-control" value="{{ old('emailid', $meta_data['settings']['emailid'] ?? '') }}" name="emailid" placeholder="Email ID here...">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-6 col-sm-12">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>Email From Name</label>
                                                            <input type="text" class="form-control" value="{{ old('emailfromname', $meta_data['settings']['emailfromname'] ?? '') }}" name="emailfromname" placeholder="Email From Name here...">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-12">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label for="contactnumber_country_code">Contact Number Country Code</label>
                                                            <select id="contactnumber_country_code" class="form-control" name="contactnumber_country_code" required>
                                                                @foreach(App\Customer::MOBILE_COUNTRY_CODE as $key=>$value)
                                                                <option value="{{$key}}" {{ old('contactnumber_country_code', $meta_data['settings']['contactnumber_country_code'] ?? '91') ==  $key ? 'selected' : '' }}>{{$value}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-6 col-sm-12">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>Contact Number</label>
                                                            <input type="tel" class="form-control" value="{{ old('contactnumber', $meta_data['settings']['contactnumber'] ?? '') }}" name="contactnumber" placeholder="Contact Number here..." onkeypress="return isNumberKey(event,this)" required>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                        <hr class="hr-text" data-content="SEO" />
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="form-group">
                                                        <label for="seo_title" >SEO Title</label>
                                                        <input class="form-control" id="seo_title" name="seo_title" maxlength="120" value= "@if(isset($meta_data['seo_title']['seo_title'])) {{ old('seo_title',$meta_data['seo_title']['seo_title'])}} @endif">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="form-group">
                                                        <label for="seo_description" >SEO Description</label>
                                                        <textarea class="form-control" type="text" id="seo_description" name="seo_description" maxlength="160" >{{ old('seo_description', $meta_data['seo_description']['seo_description'] ?? '') }}</textarea>
                                                    </div>
                                                </div>
                                            
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="form-group">
                                                        <label for="seo_keyword" class=" h6">SEO Keyword</label>
                                                        <input class="form-control" id="seo_keyword" name="seo_keyword" value= "@if(isset($meta_data['seo_keyword']['seo_keyword'])) {{ old('seo_keyword',$meta_data['seo_keyword']['seo_keyword'])}} @endif">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <hr class="hr-text" data-content="Show SKU Setting" />
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12">
                                                    <label>Show SKU in Storefront</label>
                                                    <div class="form-group custom-control custom-switch custom-switch-success">
                                                        <span class="switch-label mr-1"><b>Enable</b></span>
                                                        <input type="checkbox" class="custom-control-input" value="1" id="storefront_show_sku" name="storefront_show_sku" @if(isset($meta_data['settings']['storefront_show_sku'])) {{ old('storefront_show_sku',$meta_data['settings']['storefront_show_sku']) == 1 ? 'checked':''}} @endif>
                                                        <label class="custom-control-label" for="storefront_show_sku"></label>
                                                        <span class="switch-label"><b>Disable</b></span>
                                                    </div>
                                                </div>
                                                @if(session('subscribed_features'))
                                                @if(array_key_exists("pos", session('subscribed_features')[0]))
                                                <div class="col-lg-6 col-md-12">
                                                    <label>Show SKU in PoS</label>
                                                    <div class="form-group custom-control custom-switch custom-switch-success">
                                                        <span class="switch-label mr-1"><b>Enable</b></span>
                                                        <input type="checkbox" class="custom-control-input" value="1" id="pos_show_sku" name="pos_show_sku" @if(isset($meta_data['settings']['pos_show_sku'])) {{ old('pos_show_sku',$meta_data['settings']['pos_show_sku']) == 1 ? 'checked':''}}@endif>
                                                        <label class="custom-control-label" for="pos_show_sku"></label>
                                                        <span class="switch-label"><b>Disable</b></span>
                                                    </div>
                                                </div>
                                                @endif
                                                @endif
                                            </div>
                                        </div>
                                        @if(session('subscribed_features'))
                                        @if(array_key_exists("inventory", session('subscribed_features')[0]))
                                        <div class="col-12">
                                            <hr class="hr-text" data-content="Show Inventory Setting" />
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12">
                                                    <label>Show Inventory in Storefront</label>
                                                    <div class="form-group custom-control custom-switch custom-switch-success">
                                                        <span class="switch-label mr-1"><b>Enable</b></span>
                                                        <input type="checkbox" class="custom-control-input" value="1" id="storefront_show_inventory" name="storefront_show_inventory" @if(isset($meta_data['settings']['storefront_show_inventory'])) {{ old('storefront_show_inventory',$meta_data['settings']['storefront_show_inventory']) == 1 ? 'checked':''}} @endif>
                                                        <label class="custom-control-label" for="storefront_show_inventory"></label>
                                                        <span class="switch-label"><b>Disable</b></span>
                                                    </div>
                                                </div>
                                                @if(array_key_exists("pos", session('subscribed_features')[0]))
                                                <div class="col-lg-6 col-md-12">
                                                    <label>Show Inventory in PoS</label>
                                                    <div class="form-group custom-control custom-switch custom-switch-success">
                                                        <span class="switch-label mr-1"><b>Enable</b></span>
                                                        <input type="checkbox" class="custom-control-input" value="1" id="pos_show_inventory" name="pos_show_inventory" @if(isset($meta_data['settings']['pos_show_inventory'])) {{ old('pos_show_inventory',$meta_data['settings']['pos_show_inventory']) == 1 ? 'checked':''}}@endif>
                                                        <label class="custom-control-label" for="pos_show_inventory"></label>
                                                        <span class="switch-label"><b>Disable</b></span>
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                        @endif
                                        @endif
                                        @if(session('subscribed_features'))
                                        @if(array_key_exists("Video catalog", session('subscribed_features')[0]))
                                        <div class="col-12">
                                            <hr class="hr-text" data-content="Show Shop TV" />
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12">
                                                    <label>Show Shop TV in Storefront</label>
                                                    <div class="form-group custom-control custom-switch custom-switch-success">
                                                        <span class="switch-label mr-1"><b>Enable</b></span>
                                                        <input type="checkbox" class="custom-control-input" value="1" id="storefront_shop_tv" name="storefront_shop_tv" @if(isset($meta_data['settings']['storefront_shop_tv'])) {{ old('storefront_shop_tv',$meta_data['settings']['storefront_shop_tv']) == 1 ? 'checked':''}} @endif>
                                                        <label class="custom-control-label" for="storefront_shop_tv"></label>
                                                        <span class="switch-label"><b>Disable</b></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        @endif
                                        <div class="col-12">
                                            <hr class="hr-text" data-content="Currency Setting" />
                                            <div class="row">
                                                <div class="col-12 col-md-6 col-sm-12">
                                                    <div class="form-group">
                                                        <label>Symbol</label>
                                                        <select class="form-control" name="currency" onChange="change_currency(this.value)">
                                                            @foreach(App\User::CURRENCY_SYMBOLS as $key => $label)
                                                            <option value="{{ $key }}" {{ old('currency', $meta_data['settings']['currency'] ?? 'INR') ===  $key ? 'selected' : '' }}> {{ $key }} ({{html_entity_decode($label)}})</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-6 col-sm-12">
                                                    <div class="form-group">
                                                        <label>Symbol Display</label>
                                                        <br>
                                                        <div class="row ml-1">
                                                            <div class="col-6">

                                                                <input class="form-check-input " type="radio" name="currency_selection" value="currency_symbol" @if(isset($meta_data['settings']['currency_selection'])) {{ old('currency_selection',$meta_data['settings']['currency_selection'] ?? '') == 'currency_symbol' ? 'checked':''}}@else checked @endif>
                                                                <span id="currency_symbol">&nbsp;
                                                                    Symbol ({!! isset($meta_data['settings']['currency'])? App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']] : '&#8377' !!})</span>
                                                            </div>
                                                            <div class="col-6">
                                                                <input class="form-check-input " type="radio" name="currency_selection" value="currency_label" @if(isset($meta_data['settings']['currency_selection'])) {{ old('currency_selection',$meta_data['settings']['currency_selection'] ?? '') == 'currency_label' ? 'checked':''}}@endif>
                                                                <span id="currency_label">&nbsp; Label ({!! isset($meta_data['settings']['currency_selection']) ? $meta_data['settings']['currency'] : 'INR' !!})</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <hr class="hr-text" data-content="Privacy Setting" />
                                        </div>

                                        <div class="col-12 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="p-0 custom-control custom-switch custom-switch-success">
                                                    <span class="switch-label pl-0">Private Store</span>
                                                    <input type="checkbox" class="custom-control-input private-store" id="private_store" name="private_store" @if(isset($meta_data['settings']['private_store'])){{old('private_store', $meta_data['settings']['private_store']=='on' ?? '') == true ? 'checked':''}} @endif>
                                                    <label class="custom-control-label  float-right" for="private_store">
                                                        <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                                        <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 col-md-6 col-sm-12">
                                            <div class="form-group" id="secret_password_div" style=@if(isset($meta_data['settings']['private_store'])){{old('private_store', $meta_data['settings']['private_store']=='on' ?? '') == true ? 'display:block;':'display:none;'}} @endif>
                                                <div class="controls">
                                                    <label>Password</label>
                                                    <fieldset>
                                                        <div class="input-group">
                                                            <input type="password" class="form-control" id="secret_password" placeholder="password" name="secret_password" autocomplete="off"/>
                                                        </div>
                                                    </fieldset>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <hr class="hr-text" data-content="VR Store" />
                                        </div>

                                        <div class="col-12 col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label for="basic-url">VR Store URL</label>
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text" id="vr-store-url">{{env("VR_STORE_URL")}}</span>
                                                        </div>
                                                        <input type="text" class="form-control" id="basic-url" name="vr-store-url" placeholder="Enter path" value="{{ old('vr-store-url', $meta_data['vr-store-url']['vr-store-url'] ?? '') }}" aria-describedby="vr-store-url">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                            <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save changes</button>
                                            <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- store settings end -->

                            <!-- store front settings start -->
                            <div class="tab-pane active" id="storefront" role="tabpanel" aria-labelledby="storefront-pill" aria-expanded="true">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="btn-group m-1 float-right">
                                            <a href="#" onclick="publishStore({{ auth()->user()->id }},{{auth()->user()->userInfo->is_start == 1?0:1}})" type="button" class="btn {{auth()->user()->userInfo->is_start == 1?'btn-danger':'btn-info'}}"><img class="mb-xs-1" src="{{ asset('XR\app-assets\images\icons\store_white.png') }}" style="height: 18px;width: 18px;"> {{auth()->user()->userInfo->is_start == 1?"Store Unpublish":"Publish Store"}}</a>
                                        </div>
                                    </div>
                                </div>
                                <form method="POST" id="formstorefront" action="{{ route("admin.userSettings.update") }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="hash" value="storefront" />
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label>Store Name</label>
                                                    <input type="text" class="form-control" value="{{ old('name', $meta_data['storefront']['storename'] ?? '') }}" name="storename" placeholder="Store Name here..." maxlength="35" required>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <!-- //store logo starts-->
                                    <label>Store logo</label>
                                    <br><br>
                                    <div class="row">
                                        <div class="col-12" id="store_logo_img">
                                            <div style="height:85px;width: 120px;text-align: center;border-style:dashed;padding: 7px;" id="store_logo_btn" class="rounded mb-0 ml-1">
                                                <a data-toggle="modal" data-target="#storeLogoUploadimgModal" href="{{ route('admin.userSettings.storeMedia') }}" style="color: #626262;">
                                                    <div class="image" id="addstorelogo">
                                                        <i class="feather icon-plus-circle font-large-1 "></i>
                                                    </div>
                                                    <span style="display: block;padding:2px;">Upload Image</span>
                                                </a>
                                            </div>
                                            <br>

                                            @if($campaign->storelogo != null)
                                            <!--/.Slides-->
                                            <input type="hidden" name='storelogo' value="{{$campaign->storelogo->file_name}}">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12">
                                                    <div class="image-area" id="storelogo_{{$campaign->storelogo->id}}" style="width: 200px; height: 200px;">
                                                        <div style="height:200px;">
                                                            <img class="d-block img-fluid" style="width:100%;min-height:50%;max-height:100%;" src="{{$campaign->storelogo->getUrl() ?? asset('assets/images/placholder.png')}}">
                                                            <a class="remove-image" href="javascript:void(0)" onclick="removestorelogoimg({{$campaign->storelogo->id}},'{{$campaign->storelogo->file_name}}')" style="display: inline;"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                    <br>
                                                </div>
                                            </div>
                                            @else
                                            <h3 class="p-4 text-light">No logo uploaded</h3>
                                            @endif
                                        </div>
                                    </div>
                                    <!-- store logo ends -->
                                    <!-- <div class="row">
                                        <div class="col-12">
                                        <div class="dropzone dropzone-area" id="bannerimg">
                                          <div class="dz-message">Upload Banner Image</div>
                                        </div>
                                    </div>
                                        </div> -->
                                        <div>
                                        <label>Show Store Name or Logo</label>
                                        <select class="form-control {{ $errors->has('logo_type') ? 'is-invalid' : '' }}" name="logo_type">
                                                    @foreach(App\Campaign::BRAND_TYPE as $key => $label)
                                                    <option value="{{ $key }}" {{ old('logo_type', $meta_data['storefront']['logo_type'] ?? '1') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                                                    @endforeach
                                                </select>
                                        </div>
                                    <br><br>
                                    <label>Manage Banners <b>[Recommended size: 1200 X 280]</b></label>
                                    <br><br>
                                    <div class="row">
                                        <div class="col-12" id="banner_img">
                                            <div style="height:85px;width: 120px;text-align: center;border-style:dashed;padding: 7px;" id="banner_btn" class="rounded mb-0 ml-1">
                                                <a data-toggle="modal" data-target="#bannerUploadimgModal" href="{{ route('admin.userSettings.storeMedia') }}" style="color: #626262;">
                                                    <div class="image" id="addban">
                                                        <i class="feather icon-plus-circle font-large-1"></i>
                                                    </div>
                                                    <span style="display: block;padding:2px;">Upload Image</span>
                                                </a>
                                            </div>
                                            <br>
                                            @if(count($campaign->bannerimg) > 0)
                                            <!--/.Slides-->
                                            <div class="row">
                                                @foreach($campaign->bannerimg as $photo)
                                                <input type="hidden" name="bannerimg[]" value={{$photo->file_name}}>
                                                <div class="col-lg-6 col-md-12">
                                                    <div class="image-area" id="banner_{{$photo->id}}">
                                                        <div style="min-height:90px;">
                                                            <img class="d-block img-fluid" style="min-width:100%;max-width:100%;height: 120px;max-height:100%;" src="{{$photo->getUrl() ?? asset('assets/images/placholder.png')}}">
                                                        </div>
                                                        <a class="remove-image" href="javascript:void(0)" onclick="removebannerimg({{$photo->id}},'{{$photo->file_name}}')" style="display: inline;"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                    </div>
                                                    <!-- <button class="btn badge btn-danger btn-sm text-white" href="#{{$photo->id}}" onclick="removebannerimg({{$photo->id}})" ><i class="feather icon-trash-2 m-0"></i></button> -->
                                                    <br>
                                                </div>
                                                @endforeach
                                            </div>
                                            @else
                                            <h3 class="p-4 text-light">No Banner Images Uploaded</h3>
                                            @endif
                                        </div>
                                    </div>

                                    <!--start store logo uploading modal -->
                                    <div class="modal fade ecommerce-application" id="storeLogoUploadimgModal" tabindex="-1" role="dialog" aria-labelledby="SelectstoreLogoModal" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="SelectstoreLogoModal">Upload store logo</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="dropzone dropzone-area" id="storelogoimg">
                                                        <div class="dz-message">Upload store logo</div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancel</button>
                                                    <button type="button" class="btn btn-primary" id="storeLogoAdd" data-dismiss="modal" aria-label="Close" disabled="true">Add</button>
                                                    <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end store logo uploading modal -->


                                    <!-- Banner upload Modal -->
                                    <div class="modal fade ecommerce-application" id="bannerUploadimgModal" tabindex="-1" role="dialog" aria-labelledby="bannerSelectModalTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="bannerSelectModalTitle">Banner Upload Image</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="dropzone dropzone-area" id="bannerimg">
                                                        <div class="dz-message">Upload Banner Image</div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancel</button>
                                                    <button type="button" class="btn btn-primary" id="bannerImgAdd" data-dismiss="modal" aria-label="Close" disabled="true">Add</button>
                                                    <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Banner Upload Modal -->

                                    <hr>
                                    <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                        <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save
                                            changes</button>
                                        <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                    </div>
                                </form>
                            </div>
                            <!-- store front settings end-->

                            <!-- shipping settings start -->
                            <div class="tab-pane fade " id="shipping-settings" role="tabpanel" aria-labelledby="shipping-pill-settings" aria-expanded="false">
                                <form method="POST" class="needs-validation" action="{{ route("admin.userSettings.update") }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="hash" value="shipping-settings" />
                                    <label for="shipping-settings">Shipping Settings</label>
                                    <hr>
                                    <div class="row">
                                        <div class="col-12 col-md-12 col-sm-12">
                                            <div class="form-group ">
                                                <div class="p-0 custom-control custom-switch custom-switch-success">

                                                    <span class="switch-label text-bold-700">
                                                        <!-- Shipping -->
                                                <img src="{{ asset('XR\app-assets\images\icons\shyplite.png') }}" alt="Shyplite" style="width: 102px;margin-left:-16px;">

                                                    </span>
                                                    <input type="checkbox" class="custom-control-input shipping-method" id="shipping" name="shipping" @if(isset($meta_data['shipping_settings']['shipping_method'])){{old('shipping', in_array("shipping",$meta_data['shipping_settings']['shipping_method']) ?? '') == true ? 'checked':''}} @endif>
                                                    <label class="custom-control-label  float-right" for="shipping">
                                                        <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                                        <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                                    </label>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row ml-1">
                                                    <div class="col-md-12 mb-1">
                                                        Please create an account with ShypLite by clicking here <a href="http://shyplite.com/?at=SimpliSell" target="blank">http://shyplite.com/?at=SimpliSell</a>
                                                    </div>
                                                </div>
                                    <div class="row ">
                                        <div class="col-12 col-md-12 col-sm-12">
                                            @php
                                            $shipping_method= isset($meta_data['shipping_settings']["shipping_method"]) ?  in_array("shipping",$meta_data['shipping_settings']["shipping_method"]) : FALSE;
                                            $shipping_settings= isset($meta_data['shipping_settings']['shipping'])? $meta_data['shipping_settings']['shipping'] : '';
                                            @endphp
                                            
                                            <div id="shipping-input-div">
                                                

                                                <div class="form-row ml-1">

                                                    <div class="col-md-4 mb-1">
                                                        <label for="public-key">Public Key</label>
                                                        <input type="text" class="form-control" id="public-key-id" name="public-key-id" value="{{ isset( $shipping_settings['public-key-id'] ) ? $shipping_settings['public-key-id']:'' }}" @if (!$shipping_method ) readonly @else required @endif>
                                                    </div>
                                                    <div class="col-md-4 mb-1">
                                                        <label for="sp-app">App Id</label>
                                                        <input type="text" class="form-control" id="sp-app-id" name="sp-app-id" value="{{ isset( $shipping_settings['sp-app-id'] ) ? $shipping_settings['sp-app-id']:'' }}" @if (!$shipping_method ) readonly @else required @endif>
                                                    </div>
                                                    <div class="col-md-4 mb-1">
                                                        <label for="sp-sell">Seller Id</label>
                                                        <input type="text" class="form-control" id="sp-sell-id" name="sp-sell-id" value="{{ isset( $shipping_settings['sp-sell-id'] ) ? $shipping_settings['sp-sell-id']:'' }}" @if (!$shipping_method ) readonly @else required @endif>
                                                    </div>
                                                    
                                                </div>
                                                <div class="form-row ml-1">

                                                    <div class="col-md-12 mb-1">
                                                        <label for="sp-private-key">Private Key</label>
                                                        <input type="text" class="form-control" id="sp-private-key" name="sp-private-key" value="{{ isset( $shipping_settings['sp-private-key'] ) ? $shipping_settings['sp-private-key']:'' }}" @if (!$shipping_method ) readonly @else required @endif>
                                                    </div>
                                                </div>
                                                <div class="form-row ml-1">

                                                    <div class="col-md-4 mb-1">
                                                        <label for="sp-private-key">Seller address Id</label>
                                                        <input type="text" class="form-control" id="sp-address-id" name="sp-address-id" value="{{ isset( $shipping_settings['sp-address-id'] ) ? $shipping_settings['sp-address-id']:'' }}" @if (!$shipping_method ) readonly @else required @endif>
                                                    </div>
                                                    <div class="col-md-4 mb-1">
                                                        <label for="sp-continuous">Sphylite continuous</label>
                                                        <input type="text" class="form-control" id="sp-continuous" name="sp-continuous" value="{{ isset( $shipping_settings['sp-continuous'] ) ? $shipping_settings['sp-continuous']:'' }}" @if (!$shipping_method ) readonly @else required @endif>
                                                    </div>
                                                </div>

                                                <hr class="hr-text" data-content="Shpping Parameter" />
                                                <div class="form-row ml-1">

                                                <div class="col-md-6 mb-1">
                                                    <label for="shyplite_length">Length</label>
                                                    <input type="text" class="form-control" id="shyplite_length" name="shyplite_length" value="{{ isset( $shipping_settings['shyplite_length'] ) ? $shipping_settings['shyplite_length']:'' }}" @if (!$shipping_method ) readonly @else  @endif>
                                                </div>
                                                <div class="col-md-6 mb-1">
                                                    <label for="shyplite_width">Width</label>
                                                    <input type="text" class="form-control" id="shyplite_width" name="shyplite_width" value="{{ isset( $shipping_settings['shyplite_width'] ) ? $shipping_settings['shyplite_width']:'' }}" @if (!$shipping_method ) readonly @else  @endif>
                                                </div>
                                                <div class="col-md-6 mb-1">
                                                    <label for="shyplite_height">Height</label>
                                                    <input type="text" class="form-control" id="shyplite_height" name="shyplite_height" value="{{ isset( $shipping_settings['shyplite_height'] ) ? $shipping_settings['shyplite_length']:'' }}" @if (!$shipping_method ) readonly @else  @endif>
                                                </div>
                                                <div class="col-md-6 mb-1">
                                                    <label for="shyplite_weight">Weight</label>
                                                    <input type="text" class="form-control" id="shyplite_weight" name="shyplite_weight" value="{{ isset( $shipping_settings['shyplite_weight'] ) ? $shipping_settings['shyplite_weight']:'' }}" @if (!$shipping_method ) readonly @else  @endif>
                                                </div>
                                                </div>
                                                

                                                
                                            </div>
                                        </div>
                                    </div>
                                    

                                    <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                        <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save changes</button>
                                        <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                    </div>
                                </form>
                            </div>

                            <!-- payment settings start -->
                            <div class="tab-pane fade " id="payment-settings" role="tabpanel" aria-labelledby="payment-pill-settings" aria-expanded="false">
                                <form method="POST" class="needs-validation" action="{{ route("admin.userSettings.update") }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="hash" value="payment-settings" />
                                    <label for="payment-settings">Payment Settings</label>
                                    <hr>

                                    <div class="row">
                                        <div class="col-12 col-md-12 col-sm-12">
                                            <div class="form-group ">
                                                <div class="p-0 custom-control custom-switch custom-switch-success">
                                                    <span class="switch-label text-bold-700">
                                                        Enquiry
                                                    </span>
                                                    <input type="checkbox" class="custom-control-input payment-method" id="enquiry" name="enquiry" @if(isset($meta_data['payment_settings']['payment_method'])){{old('enquiry', in_array("Enquiry",$meta_data['payment_settings']['payment_method']) ?? '') == true ? 'checked':''}} @endif>
                                                    <label class="custom-control-label  float-right" for="enquiry">
                                                        <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                                        <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                                    </label>
                                                </div>

                                            </div>
                                            <p class="px-2 w-75 text-break ">
                                                The Enquiry feature will enabled by default if you don't enable any payment option. Enquiry feature can be enabled along with any Payment option.
                                            </p>

                                        </div>
                                    </div>

                                    <hr class="hr-text" data-content="COD Settings" />

                                    <div class="row">

                                        <div class="col-12 col-md-12 col-sm-12">
                                            <div class="form-group ">
                                                <div class="p-0 custom-control custom-switch custom-switch-success">
                                                    <span class="switch-label text-bold-700">
                                                        <!-- <img src="{{ asset('XR\app-assets\images\icons\razorpay_logo.svg') }}" style="width: 102px;margin-left:-16px;"> -->
                                                        Cash on Delivery
                                                    </span>
                                                    <input type="checkbox" class="custom-control-input payment-method" id="cod" name="cod" @if(isset($meta_data['payment_settings']['payment_method'])){{old('cod', in_array("Cash on delivery",$meta_data['payment_settings']['payment_method']) ?? '') == true ? 'checked':''}} @endif>
                                                    <label class="custom-control-label  float-right" for="cod">
                                                        <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                                        <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                                    </label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row ">
                                        <div class="col-12 col-md-12 col-sm-12">
                                            @php

                                            $payment_method= isset($meta_data['payment_settings']["payment_method"]) ? in_array("Cash on delivery",$meta_data['payment_settings']["payment_method"]): FALSE;
                                            $payment_settings= isset($meta_data['payment_settings']['Cash on delivery']) ? $meta_data['payment_settings']['Cash on delivery']:'';
                                            @endphp
                                            <div class="form-row ml-1" id="cod-input-div">
                                                <div class="col-md-12 mb-1">
                                                    <label for="cod-text">Description Text</label>
                                                    <textarea type="text" class="form-control" id="cod-text" name="cod-text" @if (!$payment_method) readonly @else required @endif>{{old('cod-text', isset($payment_settings['desc-text'])?$payment_settings['desc-text']:'Make payment after you receive the product.' )}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <img src="{{ asset('XR\app-assets\images\icons\razorpay_logo.svg') }}" style="width: 102px;margin-left:-16px;"> -->

                                    <!-- <hr class="hr-text" data-content="UPI Settings" />

                                    <div class="row">
                                        <div class="col-12 col-md-12 col-sm-12">
                                            <div class="form-group ">
                                                <div class="p-0 custom-control custom-switch custom-switch-success">
                                                    <span class="switch-label text-bold-700">
                                                        UPI
                                                    </span>
                                                    <input type="checkbox" class="custom-control-input payment-method" id="upi" name="upi" @if(isset($meta_data['payment_settings']['payment_method'])){{ old('upi', in_array("UPI",$meta_data['payment_settings']['payment_method']) ?? '') == true ? 'checked':''}} @endif>
                                                    <label class="custom-control-label  float-right" for="upi">
                                                        <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                                        <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                                    </label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row ">
                                        <div class="col-12 col-md-12 col-sm-12">
                                            @php
                                            $payment_method= isset($meta_data['payment_settings']["payment_method"]) ? in_array("UPI",$meta_data['payment_settings']["payment_method"]) : FALSE;
                                            $payment_settings= isset($meta_data['payment_settings']['UPI'])?$meta_data['payment_settings']['UPI']: '';
                                            @endphp
                                            <div id="upi-input-div"> 

                                                <div class="form-row ml-1">
                                                    <div class="col-md-6 mb-1">
                                                        <label for="upi-name">UPI Name</label>
                                                        <input class="form-control" id="upi-name" name="upi-name" value="{{ isset( $payment_settings['upi-name'] ) ? $payment_settings['upi-name'] : '' }}" @if(!$payment_method) readonly @else required @endif>
                                                    </div>

                                                    <div class="col-md-6 mb-1">
                                                        <label for="upi-id">UPI Id</label>
                                                        <input class="form-control" id="upi-id" name="upi-id" value="{{isset( $payment_settings['upi-id'] ) ? $payment_settings['upi-id']:'' }}" @if(!$payment_method) readonly @else required @endif>
                                                    </div>

                                                    <div class="col-md-12 mb-1">
                                                        <p class="text-muted">Notes: Enter the exact UPI name associated with your UPI Id</p>
                                                    </div>
                                                </div>

                                                <div class="form-row ml-1">
                                                    <div class="col-md-12 mb-1">
                                                        <label for="cod-text">Description Text</label>
                                                        <textarea class="form-control" id="upi-text" name="upi-text" @if(!$payment_method) readonly @else required @endif>{{old('upi-text', isset($payment_settings['desc-text']) ? $payment_settings['desc-text']:'Pay to our UPI ID and send the transaction screenshot to our WhatsApp number.' )}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->

                                    <hr class="hr-text" data-content="Razorpay Settings" />

                                    <div class="row">

                                        <div class="col-12 col-md-12 col-sm-12">
                                            <div class="form-group ">
                                                <div class="p-0 custom-control custom-switch custom-switch-success">
                                                    <span class="switch-label text-bold-700">
                                                        <img src="{{ asset('XR\app-assets\images\icons\razorpay_logo.svg') }}" alt="RazorPay" style="width: 102px;margin-left:-16px;">
                                                        <!-- RazorPay -->
                                                    </span>
                                                    <input type="checkbox" class="custom-control-input payment-method" id="razorpay" name="razorpay" @if(isset($meta_data['payment_settings']['payment_method'])){{old('razorpay', in_array('Razorpay',$meta_data['payment_settings']['payment_method']) ?? '') == true ? 'checked':''}} @endif>
                                                    <label class="custom-control-label  float-right" for="razorpay">
                                                        <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                                        <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                                    </label>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="col-12 pb-3 ml-2">
                                            If you don't have a razorpay account, <a href="https://rzp.io/i/XbIxhKR">click here to signup</a>
                                        </div>
                                    </div>

                                    <div class="row ">
                                        <div class="col-12 col-md-12 col-sm-12">
                                            @php
                                            $payment_method= isset($meta_data['payment_settings']["payment_method"]) ?  in_array("Razorpay",$meta_data['payment_settings']["payment_method"]) : FALSE;
                                            $payment_settings= isset($meta_data['payment_settings']['Razorpay'])? $meta_data['payment_settings']['Razorpay'] : '';
                                            @endphp
                                            <div id="razorpay-input-div">

                                                <div class="form-row ml-1">

                                                    <div class="col-md-4 mb-1">
                                                        <label for="rp-key">Razorpay Key</label>
                                                        <input type="text" class="form-control" id="rp-key-id" name="rp-key-id" value="{{ isset( $payment_settings['rp-key-id'] ) ? $payment_settings['rp-key-id']:'' }}" @if (!$payment_method ) readonly @else required @endif>
                                                    </div>
                                                    <div class="col-md-4 mb-1">
                                                        <label for="rp-secret-key">Razorpay Secret Key </label>
                                                        <input type="text" class="form-control" id="rp-secret-key" name="rp-secret-key" value="{{ isset($payment_settings['rp-secret-key']) ? $payment_settings['rp-secret-key'] : '' }}" @if (!$payment_method) readonly @else required @endif>
                                                    </div>
                                                    <div class="col-md-4 mb-1">
                                                        <label for="rp-webhook-secret-key">Razorpay Webhook Secret Key</label>
                                                        <input type="text" class="form-control" id="rp-webhook-secret-key" name="rp-webhook-secret-key" value="{{ isset($payment_settings['rp-webhook-secret-key']) ? $payment_settings['rp-webhook-secret-key']:'' }}" @if (!$payment_method) readonly @endif>
                                                    </div>
                                                </div>
                                                <div class="form-row ml-1">

                                                    <div class="col-md-12 mb-1">
                                                        <label for="rp-text">Description Text</label>
                                                        <textarea class="form-control" id="rp-text" name="rp-text" @if (!$payment_method) readonly @else required @endif>{{ isset($payment_settings['desc-text']) ? $payment_settings['desc-text']:'Make an online payment using RazorPay. You can use any choice of your payment like Credit Card, Debit Card, Online Banking, UPI, Wallets.'}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    {{--<div class="row">
                                        <div class="col-12 col-md-12 col-sm-12 ">
                                            <div class="form-group">
                                                <div class="custom-control custom-switch custom-switch-success">
                                                    <span class="switch-label text-bold-700">
                                                        Paypal
                                                    </span>

                                                    <input type="checkbox" class="custom-control-input payment-method" id="paypal" name="paypal" @if(isset($meta_data['payment_settings']['payment_method']) ){{old('paypal',in_array('Paypal',$meta_data['payment_settings']['payment_method']) ?? '') == true ? 'checked':''}} @endif>
                                                    <label class="custom-control-label   float-right " for="paypal">
                                                        <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                                        <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                                    </label>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        @php
                                        $payment_method= isset($meta_data['payment_settings']["payment_method"]) ?  in_array("Paypal",$meta_data['payment_settings']["payment_method"]) : '';
                                        $payment_settings= isset($meta_data['payment_settings']['Paypal']) ?$meta_data['payment_settings']['Paypal']: '';
                                        @endphp
                                        <div class="col-12 col-md-12 col-sm-12 ">
                                            <div id="paypal-input-div">

                                                <div class="form-row ml-1">
                                                    <div class="col-md-4 mb-1">
                                                        <label for="paypal-client-id">Paypal Client Id</label>
                                                        <input type="text" class="form-control" id="paypal-client-id" name="paypal-client-id" value="{{old('paypal-client-id', isset($payment_settings['paypal-client-id'])?$payment_settings['paypal-client-id']:'' )}}" @if (!$payment_method) readonly @else required @endif>
                                                    </div>
                                                    <div class="col-md-4 mb-1">
                                                        <label for="paypal-secret">Paypal Secret</label>
                                                        <input type="text" class="form-control" id="paypal-secret" name="paypal-secret" value="{{old('paypal-secret', isset($payment_settings['paypal-secret'])?$payment_settings['paypal-secret']:'' )}}" @if (!$payment_method) readonly @else required @endif>
                                                    </div>
                                                    <div class="col-md-4 mb-1">
                                                        <label for="paypal-env">Select Environment</label>
                                                        <select class="custom-select form-control" id="paypal-env" name="paypal-env" @if (!$payment_method) readonly @else required @endif>
                                                            <option value=''>select</option>
                                                            <option value="sandbox" @if( $payment_settings!='' && $payment_settings['paypal-env']=='sandbox' ) selected @endif>Sandbox</option>
                                                            <option value="live" @if( $payment_settings!='' && $payment_settings['paypal-env']=='live' ) selected @endif>Live</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-row ml-1">

                                                    <div class="col-md-12 mb-1">
                                                        <label for="paypal-text">Description Text</label>
                                                        <textarea class="form-control" id="paypal-text" name="paypal-text" @if ($payment_method ) readonly @else required @endif>{{old('paypal-text', isset($payment_settings['desc-text']) ? $payment_settings['desc-text']:'Make online payment using Paypal. You can use any choice of your payment like Credit Card, Debit Card, Online Banking,' )}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-12 col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <div class="custom-control custom-switch custom-switch-success">
                                                    <span class="switch-label text-bold-700">Stripe
                                                        <!-- <img for="stripe" src="{{ asset('XR\app-assets\images\icons\stripe.svg') }}" style=" width: 87px;margin-top:-72px;margin-left:-26px;"> -->
                                                    </span>
                                                    <!-- <div style="margin-left:158px;margin-top:-55px;"> -->
                                                    <input type="checkbox" class="custom-control-input payment-method" id="stripe" name="stripe" @if(isset($meta_data['payment_settings']['payment_method']) ){{old('stripe',in_array('Stripe',$meta_data['payment_settings']['payment_method']) ?? '') == 'true' ? 'checked':''}} @endif>
                                                    <label class="custom-control-label  float-right" for="stripe">
                                                        <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                                        <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                                    </label>
                                                    <!-- </div> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        @php

                                        $payment_method= isset($meta_data['payment_settings']["payment_method"]) ? in_array("Stripe",$meta_data['payment_settings']["payment_method"]) : '';
                                        $payment_settings=$meta_data['payment_settings']['Stripe'] ?? '';

                                        @endphp
                                        <div class="col-12 col-md-12 col-sm-12">
                                            <div id="stripe-input-div">

                                                <div class="form-row ml-1">
                                                    <div class="col-md-4 mb-1">
                                                        <label for="stripe-client-id">Stripe Client Id</label>
                                                        <input type="text" class="form-control" id="stripe-client-id" name="stripe-client-id" value="{{old('stripe-client-id', isset($payment_settings['stripe-client-id'])?$payment_settings['stripe-client-id']:'' )}}" @if (!$payment_method) readonly @else required @endif>
                                                    </div>
                                                    <div class="col-md-4 mb-1">
                                                        <label for="stripe-secret">Stripe Secret</label>
                                                        <input type="text" class="form-control" id="stripe-secret" name="stripe-secret" value="{{old('stripe-secret', isset($payment_settings['stripe-secret'])?$payment_settings['stripe-secret']:'' )}}" @if (!$payment_method) readonly @else required @endif>
                                                    </div>

                                                    <div class="col-md-4 mb-1">
                                                        <label for="stripe-webhook-secret-key">Stripe Webhook Secret Key</label>
                                                        <input type="text" class="form-control" id="stripe-webhook-secret-key" name="stripe-webhook-secret-key" value="{{old('stripe-webhook-secret-key', isset($payment_settings['stripe-secret'])?$payment_settings['stripe-webhook-secret-key']:'' )}}" @if (!$payment_method) readonly @else required @endif>
                                                    </div>

                                                </div>
                                                <div class="form-row ml-1">

                                                    <div class="col-md-12 mb-1">
                                                        <label for="stripe-text">Description Text</label>
                                                        <textarea class="form-control" id="stripe-text" name="stripe-text" @if (!$payment_method) readonly @else required @endif>{{old('stripe-text', isset($payment_settings['desc-text']) ? $payment_settings['desc-text']:'Make online payment using Stripe. You can use any choice of your payment like Credit Card, Debit Card, Online Banking,' )}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>--}}

                                    <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                        <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save changes</button>
                                        <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                    </div>
                                </form>
                            </div>
                            <!-- payment settings end-->

                            <!-- checkout settings start -->
                            <div class="tab-pane fade " id="checkout-settings" role="tabpanel" aria-labelledby="checkout-pill-settings" aria-expanded="false">
                                <form method="POST" action="{{ route("admin.userSettings.update") }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="hash" value="checkout-settings" />
                                    <div class="row">
                                        <div class="col-12">
                                            <hr class="hr-text" data-content="Coupon Setting" />
                                            <div class="col-12 p-0">
                                                <div class="form-group">
                                                    <div class="p-0 custom-control custom-switch custom-switch-success">
                                                        <span class="switch-label p-0">
                                                            Enable coupons in store checkout
                                                        </span>
                                                        <input type="checkbox" class="custom-control-input guest-user" id="display_coupons" name="display_coupons" @if(isset($meta_data['checkout_settings']['display_coupons'])){{old('display_coupons', $meta_data['checkout_settings']['display_coupons']=='on' ?? '') == true ? 'checked':''}} @endif>
                                                        <label class="custom-control-label  float-right" for="display_coupons">
                                                            <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                                            <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr class="hr-text" data-content="Guest User Setting" />
                                            <div class="col-12 p-0">
                                                <div class="form-group">
                                                    <div class="p-0 custom-control custom-switch custom-switch-success">
                                                        <span class="switch-label p-0">
                                                            Allow guest user to checkout in store
                                                        </span>
                                                        <input type="checkbox" class="custom-control-input guest-user" id="guest_user" name="guest_user" @if(isset($meta_data['checkout_settings']['guest_user'])){{old('guest_user', $meta_data['checkout_settings']['guest_user']=='on' ?? '') == true ? 'checked':''}} @endif>
                                                        <label class="custom-control-label  float-right" for="guest_user">
                                                            <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                                            <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr class="hr-text" data-content="Order Setting" />
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>Order confirmation Title</label>
                                                            <input type="text" class="form-control" value="{{ old('order_confirmation_title', $meta_data['checkout_settings']['order_confirmation_title'] ?? '') }}" name="order_confirmation_title" placeholder="Order confirmation title here..." maxlength="100">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>Order Confirmation Description</label>
                                                            <textarea class="form-control" name="order_confirmation_description" placeholder="Order confirmation description here..." maxlength="1000">{{ old('order_confirmation_description', $meta_data['checkout_settings']['order_confirmation_description'] ?? '') }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr class="hr-text" data-content="Delivery/Shipping Cost" />
                                            <div class="col-12 p-0">
                                                <div class="row text-center m-0">
                                                    <div class="col-4 p-0">
                                                        <input type="radio" name="delivery_charge" value="no_charge" @if(isset($meta_data['checkout_settings']['delivery_charge'])) {{ old('delivery_charge',$meta_data['checkout_settings']['delivery_charge'] ?? '') == 'no_charge' ? 'checked':''}}@else checked @endif>&nbsp;No shipping charge
                                                    </div>
                                                    <div class="col-4 p-0">
                                                        <input type="radio" name="delivery_charge" value="charge_per_order" @if(isset($meta_data['checkout_settings']['delivery_charge'])) {{ old('delivery_charge',$meta_data['checkout_settings']['delivery_charge'] ?? '') == 'charge_per_order' ? 'checked':''}}@endif>&nbsp;Charge per order
                                                    </div>
                                                    <div class="col-4 p-0">
                                                        <input type="radio" name="delivery_charge" value="charge_per_product" @if(isset($meta_data['checkout_settings']['delivery_charge'])) {{ old('delivery_charge',$meta_data['checkout_settings']['delivery_charge'] ?? '') == 'charge_per_product' ? 'checked':''}}@endif>&nbsp;Charge per product
                                                    </div>
                                                </div>
                                                @php
                                                $delivery_charge='false';
                                                if( isset($meta_data['checkout_settings']['delivery_charge']) && $meta_data['checkout_settings']['delivery_charge'] != "no_charge")
                                                $delivery_charge='true';
                                                @endphp
                                                <div class="row mt-1" id="delivery_charge_settings" @if($delivery_charge !='true' ) style="display: none;" @endif>
                                                    <div class="col-12" id="delivery_charge_per_order_div" @if( $delivery_charge=='true' ) @if($meta_data['checkout_settings']['delivery_charge']=="charge_per_product" ) style="display: none;" @endif @endif>
                                                        <div class="form-group">
                                                            <div class="controls">
                                                                <label>Delivery charge per order<span class="text-danger">*</span></label>
                                                                <fieldset>
                                                                    <div class="input-group">
                                                                        <div class="input-group-append">
                                                                            <span class="input-group-text">{!! $currency !!}</span>
                                                                        </div>
                                                                        <input type="number" class="form-control" name="delivery_charge_per_order" value="{{ old('delivery_charge_per_order', $meta_data['checkout_settings']['delivery_charge_settings']['delivery_charge_per_order'] ?? '') }}" placeholder="Enter the delivery charge per order here...">
                                                                    </div>
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12" id="delivery_charge_per_product_div" @if( $delivery_charge=='true' ) @if($meta_data['checkout_settings']['delivery_charge']=="charge_per_order" ) style="display: none;" @endif @endif>
                                                        <div class="form-group">
                                                            <div class="controls">
                                                                <label>Delivery charge per product<span class="text-danger">*</span></label>
                                                                <fieldset>
                                                                    <div class="input-group">
                                                                        <div class="input-group-append">
                                                                            <span class="input-group-text">{!! $currency !!}</span>
                                                                        </div>
                                                                        <input type="number" class="form-control" name="delivery_charge_per_product" value="{{ old('delivery_charge_per_product', $meta_data['checkout_settings']['delivery_charge_settings']['delivery_charge_per_product'] ?? '') }}" placeholder="Enter the delivery charge per product here...">
                                                                    </div>
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <div class="controls">
                                                                <label>Free delivery above<span class="text-danger">*</span></label>
                                                                <fieldset>
                                                                    <div class="input-group">
                                                                        <div class="input-group-append">
                                                                            <span class="input-group-text">{!! $currency !!}</span>
                                                                        </div>
                                                                        <input type="number" class="form-control" name="free_delivery_above" value="{{ old('free_delivery_above', $meta_data['checkout_settings']['delivery_charge_settings']['free_delivery_above'] ?? '') }}" placeholder="Enter the amount above which delivery will be made free here...">
                                                                    </div>
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr class="hr-text" data-content="Tax Settings" />
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="p-0 custom-control custom-switch custom-switch-success">
                                                            <span class="switch-label text-bold-700 pl-0">Tax Settings</span>
                                                            <input type="checkbox" class="custom-control-input tax-settings" value="1" id="tax_settings" name="tax_settings" @if(isset($meta_data['checkout_settings']['tax_settings'])) {{ old('tax_settings',$meta_data['checkout_settings']['tax_settings']) == 1 ? 'checked':''}} @endif>
                                                            <label class="custom-control-label float-right" for="tax_settings">
                                                                <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                                                <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr class="m-0">
                                            @php
                                                $tax_settings='false';
                                                if( isset($meta_data['checkout_settings']['tax_settings']) && $meta_data['checkout_settings']['tax_settings'] == "1")
                                                    $tax_settings='true';
                                            @endphp
                                            <div class="row" id="tax_settings-input-div">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>PAN Number</label>
                                                            <input type="text" class="form-control" name="pan_number" value="{{$meta_data['checkout_settings']['tax_setting_details']['pan_number'] ?? ''}}" placeholder="Enter your PAN number here here..." minlength="10" maxlength="10" @if ($tax_settings=='false' ) readonly @else required @endif>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>Tax / GST Number</label>
                                                            <input type="text" class="form-control" name="gst_number" value="{{$meta_data['checkout_settings']['tax_setting_details']['gst_number'] ?? '' }}" placeholder="Enter your GST number here..." minlength="15" maxlength="15" @if ($tax_settings=='false' ) readonly @else required @endif>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>Tax / GST Percentage</label>
                                                            <input type="text" class="form-control" name="gst_percentage" id="gst_percentage" value="{{$meta_data['checkout_settings']['tax_setting_details']['gst_percentage'] ?? '' }}" onkeypress="return isNumberKeyPercentage(event,this)" placeholder="Enter GST percentage within 100" @if ($tax_settings=='false' ) readonly @else required @endif>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr class="hr-text m-0" data-content="Create Extra Charges" />
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="btn-group mt-2">
                                                        <span class="switch-label text-bold-700">Create Extra Charges</span>
                                                    </div>
                                                    <div class="btn-group m-1 float-lg-right">
                                                        <button type="button" class="btn btn-success" id="create_extra_charges_btn" onclick="createNewExtraCharge()">Create new extra charge</button>
                                                    </div>
                                                </div>
                                                <hr class="m-0">
                                                @if(isset($meta_data['checkout_settings']['extra_charges']) && count($meta_data['checkout_settings']['extra_charges']) > 0)
                                                    <div class="col-md-12">
                                                        @foreach($meta_data['checkout_settings']['extra_charges'] as $key => $value)
                                                            @php
                                                            //dd($value);
                                                                $extra_charge_id = $value['extra_charge_id'];
                                                            @endphp
                                                            <hr class="hr-text m-0" data-content="{!! $value['extra_charge_name'] !!}" />
                                                            <div class="row" id="extra_charge_{{$extra_charge_id}}" >
                                                                <input type="hidden" name="extra_charge_id[]" value="{{$extra_charge_id}}">
                                                                <div class="col-12">
                                                                    <div class="form-group">
                                                                        <div class="p-0 custom-control custom-switch custom-switch-success">
                                                                            <span class="switch-label text-bold-700 pl-0">{!! $value['extra_charge_name'] !!}</span>
                                                                            <input type="checkbox" class="custom-control-input extra-charges" value="1" id="extra_charge_enabled_{{$extra_charge_id}}" data-extra_charge_id="{{$extra_charge_id}}" name="extra_charge_enabled_{{$extra_charge_id}}" {{ old('extra_charge_enabled_',$value['extra_charge_enabled']) == 1 ? 'checked':''}}>
                                                                            <label class="custom-control-label float-right" for="extra_charge_enabled_{{$extra_charge_id}}">
                                                                                <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                                                                <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12">
                                                                    <input type="radio" name="extra_charge_type_{{$extra_charge_id}}" id="extra_charge_type_percentage_{{$extra_charge_id}}" data-extra_charge_id="{{$extra_charge_id}}" value="percentage" {{ old('extra_charge_type_', $value['extra_charge_type'] ?? '') == 'percentage' ? 'checked' : '' }}>&nbsp;Percentage
                                                                    <input type="radio" name="extra_charge_type_{{$extra_charge_id}}" id="extra_charge_type_flat_charge_{{$extra_charge_id}}" data-extra_charge_id="{{$extra_charge_id}}" value="flat_charge" {{ old('extra_charge_type_', $value['extra_charge_type'] ?? '') == 'flat_charge' ? 'checked' : '' }}>&nbsp;Flat charge
                                                                    <button type="button" class="btn btn-icon btn-danger float-right" id="delete_extra_charge_{{$extra_charge_id}}" onclick="delete_extra_charge({{$extra_charge_id}})"><i class="fa fa-trash fa-2x"></i></button>
                                                                </div>
                                                                <div class="col-12">
                                                                <div class="row" id="extra_charge_enabled_{{$extra_charge_id}}-input-div">
                                                                <div class="col-12">
                                                                    <div class="form-group">
                                                                        <div class="controls">
                                                                            <label>Charge Name<span class="text-danger">*</span></label>
                                                                            <input type="text" class="form-control" name="extra_charge_name[]" value="{{ old('extra_charge_name', $value['extra_charge_name'] ?? '') }}" placeholder="Enter the amount above which delivery will be made free here..." @if ($value['extra_charge_enabled'] == "0" ) readonly @else required @endif>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12" id="extra_charge_percentage_div_{{$extra_charge_id}}" @if($value['extra_charge_type']=='flat_charge' ) style="display: none;" @endif>
                                                                    <div class="form-group">
                                                                        <div class="controls">
                                                                            <label>Charges in percentage<span class="text-danger">*</span></label>
                                                                            <fieldset>
                                                                                <div class="input-group">
                                                                                    <input type="number" class="form-control" name="extra_charge_percentage[]" id="extra_charge_percentage_{{$extra_charge_id}}" value="{{ old('extra_charge_percentage', $value['extra_charge_percentage'] ?? '') }}" placeholder="Enter the extra charge percentage here..." step="any" @if ($value['extra_charge_enabled'] == "0" ) readonly @else @if($value['extra_charge_type'] == 'percentage') required @endif @endif>
                                                                                    <div class="input-group-append">
                                                                                        <span class="input-group-text">%</span>
                                                                                    </div>
                                                                                </div>
                                                                            </fieldset>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-12" id="extra_charge_amount_div_{{$extra_charge_id}}" @if($value['extra_charge_type']=='percentage' ) style="display: none;" @endif>
                                                                    <div class="form-group">
                                                                        <div class="controls">
                                                                            <label>Flat Charges<span class="text-danger">*</span></label>
                                                                            <fieldset>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-append">
                                                                                        <span class="input-group-text">{!! $currency !!}</span>
                                                                                    </div>
                                                                                    <input type="number" class="form-control" name="extra_charge_amount[]" id="extra_charge_amount_{{$extra_charge_id}}" value="{{ old('extra_charge_amount', $value['extra_charge_amount'] ?? '') }}" placeholder="Enter the extra charge amount here..." step="any" @if ($value['extra_charge_enabled'] == "0" ) readonly @else @if($value['extra_charge_type'] == 'flat_charge') required @endif @endif>
                                                                                </div>
                                                                            </fieldset>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                </div>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                @endif
                                                <div class="col-12" id="extra_charges_section">

                                                </div>
                                            </div>
                                            <hr class="hr-text" data-content="Minimum Checkout Value" />
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="form-group">
                                                        <div class="controls">
                                                            <label>Minimum Checkout Value</label>
                                                            <input type="number" class="form-control" name="minimum_checkout_value" value="{{ old('minimum_checkout_value', $meta_data['checkout_settings']['minimum_checkout_value'] ?? '') }}" placeholder="Enter the minimum value to place an order here...">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                            <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save changes</button>
                                            <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- checkout settings end -->
                            <!-- order tempelate -->
                            <div class="tab-pane fade " id="order-template-setting" role="tabpanel" aria-labelledby="order-template-pill-settings" aria-expanded="false">
                                <form method="POST" id="formOrderTemplateSettings" action="{{ route("admin.userSettings.update") }}" enctype="multipart/form-data">
                                @csrf
                                    <input type="hidden" name="hash" value="order-template-setting" />
                                    <hr class="hr-text" data-content="Orders Template" />
                                    <div class="row">
                                        <div class="col-4 m-1">
                                            <img class="mb-xs-1" src="{{ asset('XR\app-assets\images\invtemplate\default_template.png') }}" style="height: 100px;width: 100px;"/> 
                                        </div>
                                        <div class="col-4 m-1">
                                            <img class="mb-xs-1" src="{{ asset('XR\app-assets\images\invtemplate\default_template.png') }}" style="height: 100px;width: 100px;"/> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4 m-1">
                                            <input type="radio" id="order_template" name="order_template" value="default_template" @if(isset($meta_data['order_template']['order_template'])) {{ old('order_template',$meta_data['order_template']['order_template']) == 'default_template' ? 'checked':''}} @else checked @endif>
                                            <label for="order_template">Template 1 </label><br>
                                        </div>
                                        <div class="col-4 m-1">
                                            <input type="radio" id="order_template" name="order_template" value="modern_template" @if(isset($meta_data['order_template']['order_template'])) {{ old('order_template',$meta_data['order_template']['order_template']) == 'modern_template' ? 'checked':''}} @endif>
                                            <label for="order_template">Template 2</label><br>
                                        </div> 
                                        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                            <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save changes</button>
                                            <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                        </div>
                                    </div>
                                    
                                    </form> 
                                </div>       
                                <!-- Order tempelate ends--> 
                            <!-- invoice tempelate -->
                            <!-- <div class="tab-pane fade " id="invoice-tempelate-setting" role="tabpanel" aria-labelledby="invoice-tempelate-pill-settings" aria-expanded="false">
                                <form method="POST" id="formInvoiceTempelateSettings" action="{{ route("admin.userSettings.update") }}" enctype="multipart/form-data">
                                @csrf
                                    <input type="hidden" name="hash" value="invoice-tempelate-setting" />
                                    <hr class="hr-text" data-content="Invoice Template" />
                                    <div class="row">
                                        <div class="col-4 m-1">
                                            <img class="mb-xs-1" src="{{ asset('XR\app-assets\images\invtemplate\default_template.png') }}" style="height: 100px;width: 100px;"/> 
                                        </div>
                                        <div class="col-4 m-1">
                                            <img class="mb-xs-1" src="{{ asset('XR\app-assets\images\invtemplate\default_template.png') }}" style="height: 100px;width: 100px;"/> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4 m-1">
                                            <input type="radio" id="invoice_template" name="invoice_template" value="default_template" @if(isset($meta_data['invoice_template']['invoice_template'])) {{ old('invoice_template',$meta_data['invoice_template']['invoice_template']) == 'default_template' ? 'checked':''}} @else checked @endif>
                                            <label for="invoice_template">Template 1</label><br>
                                        </div>
                                        <div class="col-4 m-1">
                                            <input type="radio" id="invoice_template" name="invoice_template" value="modern_template" @if(isset($meta_data['invoice_template']['invoice_template'])) {{ old('invoice_template',$meta_data['invoice_template']['invoice_template']) == 'modern_template' ? 'checked':''}} @endif>
                                            <label for="invoice_template">Template 2</label><br>
                                        </div> 
                                        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                            <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save changes</button>
                                            <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                        </div>
                                    </div>
                                    
                                    </form> 
                                </div>        -->
                                <!-- invoice tempelate ends-->            



                            <!-- invoice settings start -->
                            <div class="tab-pane fade " id="invoice-settings" role="tabpanel" aria-labelledby="invoice-pill-settings" aria-expanded="false">
                                <form method="POST" id="formInvoiceSettings" action="{{ route("admin.userSettings.update") }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="hash" value="invoice-settings" />
                                    <hr class="hr-text" data-content="Invoice Setting" />
                                    <label>Upload signature</label>
                                    <div class="row">
                                        <div class="col-12 mt-1">
                                            @if($campaign->invoice_signature != null)
                                            <input type="hidden" name='invoice_signature' value="{{$campaign->invoice_signature->file_name}}">
                                            <div class="image-area" id="invoice_signature_{{$campaign->invoice_signature->id}}" style="width: 200px; height: 200px;">
                                                <div style="height:200px;">
                                                    <img class="d-block img-fluid" style="width:100%;min-height:50%;max-height:100%;" src="{{$campaign->invoice_signature->getUrl() ?? asset('assets/images/placholder.png')}}">
                                                    <a class="remove-image" href="javascript:void(0)" onclick="removeInvoiceSignatureimg({{$campaign->invoice_signature->id}},'{{$campaign->invoice_signature->file_name}}')" style="display: inline;"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                            @endif
                                            <div style="height:85px;width: 120px;text-align: center;border-style:dashed;padding: 7px;" id="signature_btn" class="rounded mb-0">
                                                <a data-toggle="modal" data-target="#invoiceSignatureUploadimgModal" href="{{ route('admin.userSettings.storeMedia') }}" style="color: #626262;">
                                                    <div class="image">
                                                        <i class="feather icon-plus-circle font-large-1 "></i>
                                                    </div>
                                                    <span style="display: block;" class=' pb-1'>Upload image</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Invoice Signature upload Modal -->
                                    <div class="modal fade ecommerce-application" id="invoiceSignatureUploadimgModal" tabindex="-1" role="dialog" aria-labelledby="invoiceSignatureUploadimgModalTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="invoiceSignatureUploadimgModalTitle">Invoice signature</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="dropzone dropzone-area {{ $errors->has('photo') ? 'is-invalid' : '' }}" id="invoice_signature">
                                                        <div class="dz-message">Upload invoice signature image</div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancel</button>
                                                    <button type="button" class="btn btn-primary" id="invoiceSignatureAdd" data-dismiss="modal" aria-label="Close" disabled="true">Add</button>
                                                    <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Invoice Signature Upload Modal -->
                                    <hr>
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <div class="p-0 custom-control custom-switch custom-switch-success">
                                                    <span class="switch-label p-0">
                                                        Show signature in Order Summary
                                                    </span>
                                                    <input type="checkbox" class="custom-control-input" value="1" id="show_signature_in_order_summary" name="show_signature_in_order_summary" @if(isset($meta_data['invoice_settings']['show_signature_in_order_summary'])) {{ old('show_signature_in_order_summary',$meta_data['invoice_settings']['show_signature_in_order_summary']) == 1 ? 'checked':''}} @endif>
                                                    <label class="custom-control-label float-right" for="show_signature_in_order_summary">
                                                        <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                                        <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="hr-text" data-content="Invoice Template" />
                                    <div class="row">
                                        <div class="col-4 m-1">
                                            <img class="mb-xs-1" src="{{ asset('XR\app-assets\images\invtemplate\default_template.png') }}" style="height: 100px;width: 100px;"/> 
                                        </div>
                                        <div class="col-4 m-1">
                                            <img class="mb-xs-1" src="{{ asset('XR\app-assets\images\invtemplate\default_template.png') }}" style="height: 100px;width: 100px;"/> 
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-4 m-1">
                                            <input type="radio" id="invoice_template" name="invoice_template" value="default_template" @if(isset($meta_data['invoice_template']['invoice_template'])) {{ old('invoice_template',$meta_data['invoice_template']['invoice_template']) == 'default_template' ? 'checked':''}} @else checked @endif>
                                            <label for="invoice_template">Template 1</label><br>
                                        </div>
                                        <div class="col-4 m-1">
                                            <input type="radio" id="invoice_template" name="invoice_template" value="modern_template" @if(isset($meta_data['invoice_template']['invoice_template'])) {{ old('invoice_template',$meta_data['invoice_template']['invoice_template']) == 'modern_template' ? 'checked':''}} @endif>
                                            <label for="invoice_template">Template 2</label><br>
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                            <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save changes</button>
                                            <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- invoice settings end -->


                            <!-- notification settings start -->
                            <div class="tab-pane fade " id="notification-settings" role="tabpanel" aria-labelledby="notification-pill-settings" aria-expanded="false">
                                <form method="POST" class="needs-validation" action="{{ route("admin.userSettings.update") }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="hash" value="notification-settings" />
                                    <label for="notification-settings">Notification Settings</label>
                                    <hr>

                                    <div class="row">
                                        <div class="col-12 col-md-12 col-sm-12">
                                            <div class="form-group ">
                                                <div class="p-0 custom-control custom-switch custom-switch-success">
                                                    <span class="switch-label text-bold-700">
                                                        Order Notification
                                                    </span>
                                                    <input type="checkbox" class="custom-control-input notify" id="notification" name="notification" @if(isset($meta_data['notification_settings']['order_notification'])){{old('notification', $meta_data['notification_settings']['order_notification'] =="on" ?? '') == true ? 'checked':''}} @endif>
                                                    <label class="custom-control-label  float-right" for="notification">
                                                        <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                                        <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                                    </label>
                                                </div>

                                            </div>

                                            <p class="px-2 w-75 text-break">Enable this if you want to recieve notifications upon order in your store.</p>
                                            <input type="hidden" id="order_notification" name='order_notification' value=''>
                                        </div>
                                    </div>
                                    <div class="row">
                                        
                                        <div class="col-9 col-md-9 col-sm-9">
                                            <p class="py-2 px-2">Enable this if you want to recieve browser notifications in your store.</p>
                                        </div>
                                        <div class="col-3 col-md-3 col-sm-3  text-right">
                                            <div class="pt-2" style="margin-left: -35px!important">
                                                <a class="p-1 btn btn-danger text-white" onclick="initFirebaseMessagingRegistration()">Allow Notification</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                        <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save changes</button>
                                        <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                    </div>

                                </form>
                            </div>
                            <!-- notification settings end -->

                            <!-- telegram settings start -->
                            <div class="tab-pane fade " id="telegram-settings" role="tabpanel" aria-labelledby="telegram-pill-settings" aria-expanded="false">
                                <div class="row">
                                    <div class="col-md-12" >
                                        <div class="btn-group mt-2 ml-2">
                                            <!-- <h3>Connect to Telegram</h3> -->
                                            <img src="{{ asset('XR\app-assets\images\icons\telegram.png') }}" alt="Telegram" style="width: 102px;margin-left:-16px;">
                                        </div>
                                        @if(count($bot_accounts) <=5) 
                                            <!-- <div class="btn-group m-1 float-right">
                                                <button class="btn btn-success" id="add_telegram_account_btn" onclick="generateTelegramOtp()" {{ !empty($telegram_otp) ? 'disabled':'' }}>Add account</button>
                                            </div> -->
                                        @endif 

                                        @if(count($bot_accounts) >5) 
                                            <div class="alert alert-danger" role="alert">
                                                You can have a maximum of 5 accounts. Delete an account to add more.
                                            </div>
                                        @endif 
                                    </div>
                                </div><br>
                                @if(count($bot_accounts) == 0) 
                                <div class="row">
                                    <div class="col-md-12 p-1">
                                        <center>
                                   <h4> You can receive order notification to your Telegram Messenger account by adding your  Telegram mobile number.</h4>
                                    </center>
                                </div>
                                </div><br>
                                @endif
                                <div class="row">
                                    <div class="col-md-12" align="center">
                                    @if(count($bot_accounts) <=5) 
                                            <div class="btn-group m-1 ">
                                                <button class="btn btn-success" id="add_telegram_account_btn" onclick="generateTelegramOtp()" {{ !empty($telegram_otp) ? 'disabled':'' }}>Add account</button>
                                            </div>
                                        @endif 
                                    </div>
                                </div>
                                <div class="row card" style="display: none;" id="telegram_otp_instructions">
                                    <div class="col-md-12 text-center">
                                        <h2>Your OTP is <strong><span id="telegram_otp"></span></strong></h2>
                                    </div>
                                    <div class="col-md-12">
                                        <h5><strong>Instructions:</strong></h5>
                                        <ol>
                                            <li class="otp-instructions">Open your Telegram account and search: SimpliSellSeller_bot</li>
                                            <li class="otp-instructions">Start a conversation by typing /start</li>
                                            <li class="otp-instructions">Send the verification code as a text message</li>
                                            <li class="otp-instructions">Wait for a confirmation message (This process may take up to 5 min)</li>
                                        </ol>
                                    </div>
                                </div>
                                @if(count($bot_accounts) > 0)
                                <div class="row">
                                    <div class="col-md-12">
                                        <h4>Your accounts:</h4>
                                        <table class="">
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Telegram Username</th>
                                                <th>First Name</th>
                                                <th>Delete account</th>
                                            </tr>
                                            @foreach($bot_accounts as $account)
                                            <tr id="subscriber_{{$account->telegram_chat_id}}">
                                                <td>{{ $loop->index + 1 }}</td>
                                                <td>{{ $account->username }}</td>
                                                <td>{{ $account->first_name }}</td>
                                                <td><a href="#" onclick="deleteTelegramSubscriber({{ $account->telegram_chat_id }})">Delete</a></td>
                                            </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <!-- telegram settings end -->

                            <!-- pos setting start -->
                            <div class="tab-pane fade " id="pos-settings" role="tabpanel" aria-labelledby="pos-pill-settings" aria-expanded="false">
                                <form method="POST" action="{{ route("admin.userSettings.update") }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="hash" value="pos-settings" />
                                    <div class="row">
                                        <div class="col-12">
                                            <hr class="hr-text" data-content="Customer Sign Setting" />
                                            <div class="col-12 p-0">
                                                <div class="form-group">
                                                    <div class="p-0 custom-control custom-switch custom-switch-success">
                                                        <span class="switch-label p-0">
                                                            Enable Customer Sign in POS
                                                        </span>
                                                        <input type="checkbox" class="custom-control-input guest-user" id="display_customer_sign" name="display_customer_sign" @if(isset($meta_data['pos_setting']['display_customer_sign'])){{old('display_customer_sign', $meta_data['pos_setting']['display_customer_sign']=='on' ?? '') == true ? 'checked':''}} @endif>
                                                        <label class="custom-control-label  float-right" for="display_customer_sign">
                                                            <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                                            <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                                <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save changes</button>
                                                <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                            <!-- content settings start -->
                            <div class="tab-pane fade " id="content-settings" role="tabpanel" aria-labelledby="content-pill-settings" aria-expanded="false">
                                <form method="POST" action="#" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="hash" value="content-settings" />
                                    <hr class="hr-text mt-0" data-content="Content Settings" />
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="p-0 custom-control custom-switch custom-switch-success">
                                                    <span class="switch-label text-bold-700">About us</span>
                                                    <input type="checkbox" class="custom-control-input" value="1" id="show_about_us" name="show_about_us" @if(isset($meta_data['content_settings']['show_about_us'])) {{ old('show_about_us',$meta_data['content_settings']['show_about_us']) == 1 ? 'checked':''}} @endif>
                                                    <label class="custom-control-label float-right" for="show_about_us">
                                                        <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                                        <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                                    </label>
                                                </div>
                                                <hr>
                                                <textarea class="form-control pb-1 hidden" rows="5" name="about_us" id="about_us">@if(isset($meta_data['content_settings']['about_us'])){{ old('about_us', $meta_data['content_settings']['about_us']) ?? "" }}@endif</textarea>
                                                <div id="about_us-container" style="height: 250px;"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="p-0 custom-control custom-switch custom-switch-success">
                                                    <span class="switch-label text-bold-700">Privacy Policy</span>
                                                    <input type="checkbox" class="custom-control-input" value="1" id="show_privacy_policy" name="show_privacy_policy" @if(isset($meta_data['content_settings']['show_privacy_policy'])) {{ old('show_privacy_policy',$meta_data['content_settings']['show_privacy_policy']) == 1 ? 'checked':''}} @endif>
                                                    <label class="custom-control-label float-right" for="show_privacy_policy">
                                                        <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                                        <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                                    </label>
                                                </div>
                                                <hr>
                                                <textarea class="form-control pb-1 hidden" rows="5" name="privacy_policy" id="privacy_policy">@if(isset($meta_data['content_settings']['privacy_policy'])){{ old('privacy_policy', $meta_data['content_settings']['privacy_policy']) ?? "" }}@endif</textarea>
                                                <div id="privacy_policy-container" style="height: 250px;"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="p-0 custom-control custom-switch custom-switch-success">
                                                    <span class="switch-label text-bold-700">Terms & Conditions</span>
                                                    <input type="checkbox" class="custom-control-input" value="1" id="show_terms_and_conditions" name="show_terms_and_conditions" @if(isset($meta_data['content_settings']['show_terms_and_conditions'])) {{ old('show_terms_and_conditions',$meta_data['content_settings']['show_terms_and_conditions']) == 1 ? 'checked':''}} @endif>
                                                    <label class="custom-control-label float-right" for="show_terms_and_conditions">
                                                        <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                                        <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                                    </label>
                                                </div>
                                                <hr>
                                                <textarea class="form-control pb-1 hidden" rows="5" name="terms_and_conditions" id="terms_and_conditions">@if(isset($meta_data['content_settings']['terms_and_conditions'])){{ old('terms_and_conditions', $meta_data['content_settings']['terms_and_conditions']) ?? "" }}@endif</textarea>
                                                <div id="terms_and_conditions-container" style="height: 250px;"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="p-0 custom-control custom-switch custom-switch-success">
                                                    <span class="switch-label text-bold-700">Refund & Return Policy</span>
                                                    <input type="checkbox" class="custom-control-input" value="1" id="show_refund_and_return_policy" name="show_refund_and_return_policy" @if(isset($meta_data['content_settings']['show_refund_and_return_policy'])) {{ old('show_refund_and_return_policy',$meta_data['content_settings']['show_refund_and_return_policy']) == 1 ? 'checked':''}} @endif>
                                                    <label class="custom-control-label float-right" for="show_refund_and_return_policy">
                                                        <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                                        <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                                    </label>
                                                </div>
                                                <hr>
                                                <textarea class="form-control pb-1 hidden" rows="5" name="refund_and_return_policy" id="refund_and_return_policy">@if(isset($meta_data['content_settings']['refund_and_return_policy'])){{ old('refund_and_return_policy', $meta_data['content_settings']['refund_and_return_policy']) ?? "" }}@endif</textarea>
                                                <div id="refund_and_return_policy-container" style="height: 250px;"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="p-0 custom-control custom-switch custom-switch-success">
                                                    <span class="switch-label text-bold-700">Contact us</span>
                                                    <input type="checkbox" class="custom-control-input" value="1" id="show_contact_us" name="show_contact_us" @if(isset($meta_data['content_settings']['show_contact_us'])) {{ old('show_contact_us',$meta_data['content_settings']['show_contact_us']) == 1 ? 'checked':''}} @endif>
                                                    <label class="custom-control-label float-right" for="show_contact_us">
                                                        <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                                        <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                                    </label>
                                                </div>
                                                <hr>
                                                <textarea class="form-control pb-1 hidden" rows="5" name="contact_us" id="contact_us">@if(isset($meta_data['content_settings']['contact_us'])){{ old('contact_us', $meta_data['content_settings']['contact_us']) ?? "" }}@endif</textarea>
                                                <div id="contact_us-container" style="height: 250px;"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="p-0 custom-control custom-switch custom-switch-success">
                                                    <span class="switch-label text-bold-700">Shipping Policy</span>
                                                    <input type="checkbox" class="custom-control-input" value="1" id="show_shipping_policy" name="show_shipping_policy" @if(isset($meta_data['content_settings']['show_shipping_policy'])) {{ old('show_shipping_policy',$meta_data['content_settings']['show_shipping_policy']) == 1 ? 'checked':''}} @endif>
                                                    <label class="custom-control-label float-right" for="show_shipping_policy">
                                                        <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                                        <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                                    </label>
                                                </div>
                                                <hr>
                                                <textarea class="form-control pb-1 hidden" rows="5" name="shipping_policy" id="shipping_policy">@if(isset($meta_data['content_settings']['shipping_policy'])){{ old('contact_us', $meta_data['content_settings']['shipping_policy']) ?? "" }}@endif</textarea>
                                                <div id="shipping_policy-container" style="height: 250px;"></div>
                                            </div>
                                        </div>
                                        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                            <button type="button" onclick="updatePolicySettings()" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save changes</button>
                                            <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- content settings end -->

                            <!-- Footer Settings start -->
                            <div class="tab-pane fade " id="footer-settings" role="tabpanel" aria-labelledby="footer-pill-settings" aria-expanded="false">
                                <form method="POST" action="{{ route('admin.userSettings.update') }}" enctype="multipart/form-data">


                                    @csrf
                                    <input type="hidden" name="hash" value="footer-settings" />
                                    <hr class="hr-text mt-0" data-content="Social Links" />

                                    @php
                                    $footer_settings=[];
                                    if(isset($meta_data['footer_settings']))
                                    $footer_settings=$meta_data['footer_settings'];
                                    @endphp

                                    <div class="row ">
                                        <div class="col-12 col-md-12 col-sm-12">
                                            <div id="facebook-input-div">
                                                <div class="form-row ml-1">
                                                    <div class="col-12 mb-1">
                                                        <label for="facebook-id" class="text-bold-700 h6">Facebook Id</label>
                                                        <input class="form-control" id="facebook-id" name="facebook_id" value="{{old('facebook_id', isset($footer_settings['facebook_id']) ? $footer_settings['facebook_id']:'#' )}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- <div class="row ">
                                        <div class="col-12 col-md-12 col-sm-12">
                                            <div id="whatsapp-input-div">
                                                <div class="form-row ml-1">
                                                    <div class="col-12 mb-1">
                                                        <label for="whatsapp-num" class="text-bold-700 h6">Whatsapp Number</label>
                                                        <input class="form-control" id="whatsapp-num" name="whatsapp_num" value="{{old('whatsapp_num', isset($footer_settings['whatsapp_num']) ? $footer_settings['whatsapp_num']:'#' )}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row ">
                                        <div class="col-12 col-md-12 col-sm-12">
                                            <div id="google-input-div">
                                                <div class="form-row ml-1">
                                                    <div class="col-12 mb-1">
                                                        <label for="google-link" class="text-bold-700 h6">Google Site Url</label>
                                                        <input class="form-control" id="google-link" name="google_link" value="{{old('google_link', isset($footer_settings['google_link']) ? $footer_settings['google_link']:'#' )}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> -->
                                    <div class="row ">
                                        <div class="col-12 col-md-12 col-sm-12">
                                            <div id="instagram-input-div">
                                                <div class="form-row ml-1">
                                                    <div class="col-12 mb-1">
                                                        <label for="instagram-link" class="text-bold-700 h6">Instagram Id</label>
                                                        <input class="form-control" id="instagram-link" name="instagram_link" value="{{old('instagram_link', isset($footer_settings['instagram_link']) ? $footer_settings['instagram_link']:'#' )}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row ">
                                        <div class="col-12 col-md-12 col-sm-12">
                                            <div id="pininterest-input-div">
                                                <div class="form-row ml-1">
                                                    <div class="col-12 mb-1">
                                                        <label for="pininterest-link" class="text-bold-700 h6">PinInterset Id</label>
                                                        <input class="form-control" id="pininterest-link" name="pininterest_link" value="{{old('pininterest_link', isset($footer_settings['pininterest_link']) ? $footer_settings['pininterest_link']:'#' )}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row ">
                                        <div class="col-12 col-md-12 col-sm-12">
                                            <div id="twitter-input-div">
                                                <div class="form-row ml-1">
                                                    <div class="col-12 mb-1">
                                                        <label for="twitter-id" class="text-bold-700 h6">Twitter Id</label>
                                                        <input class="form-control" id="twitter-id" name="twitter_id" value="{{old('twitter_id', isset($footer_settings['twitter_id']) ? $footer_settings['twitter_id']:'#' )}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row ">
                                        <div class="col-12 col-md-12 col-sm-12">
                                            <div id="youtube-input-div">
                                                <div class="form-row ml-1">
                                                    <div class="col-12 mb-1">
                                                        <label for="youtube-link" class="text-bold-700 h6">Youtube Link</label>
                                                        <input class="form-control" id="youtube-link" name="youtube_link" value="{{old('youtube_link', isset($footer_settings['youtube_link']) ? $footer_settings['youtube_link']:'#' )}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <hr class="hr-text mt-0" data-content="Store Address" />

                                    <div class="row ">
                                        <div class="col-12 col-md-12 col-sm-12">
                                            <div id="address1-input-div">
                                                <div class="form-row ml-1">
                                                    <div class="col-6 mb-1">
                                                        <label for="address1" class="text-bold-700 h6">Address Line 1</label>
                                                        <input class="form-control" id="address1" name="address1" value="{{old('address1', isset($footer_settings['address1']) ? $footer_settings['address1']:'' )}}">
                                                    </div>
                                                    <div class="col-6 mb-1">
                                                        <label for="address2" class="text-bold-700 h6">Address Line 2</label>
                                                        <input class="form-control" id="address2" name="address2" value="{{old('address2', isset($footer_settings['address2']) ? $footer_settings['address2']:'' )}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row ">
                                        <div class="col-12 col-md-12 col-sm-12">
                                            <div id="city-input-div">
                                                <div class="form-row ml-1">
                                                    <div class="col-6 mb-1">
                                                        <label for="city" class="text-bold-700 h6">City</label>
                                                        <input class="form-control" id="city" name="city" value="{{old('city', isset($footer_settings['city']) ? $footer_settings['city']:'' )}}">
                                                    </div>
                                                    <div class="col-6 mb-1">
                                                        <label for="address1" class="text-bold-700 h6">State</label>
                                                        <input class="form-control" id="state" name="state" value="{{old('state', isset($footer_settings['state']) ? $footer_settings['state']:'' )}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row ">
                                        <div class="col-12 col-md-12 col-sm-12">
                                            <div id="country-input-div">
                                                <div class="form-row ml-1">
                                                    <div class="col-6 mb-1">
                                                        <label for="country" class="text-bold-700 h6">Country</label>
                                                        <input class="form-control" id="country" name="country" value="{{old('country', isset($footer_settings['country']) ? $footer_settings['country']:'' )}}">
                                                    </div>
                                                    <div class="col-6 mb-1">
                                                        <label for="pincode" class="text-bold-700 h6">Pincode</label>
                                                        <input class="form-control" id="pincode" name="pincode" value="{{old('pincode', isset($footer_settings['pincode']) ? $footer_settings['pincode']:'' )}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>




                                    <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                        <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save changes</button>
                                        <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                    </div>

                                </form>
                            </div>
                            <!-- Footer Settings end -->
                            <!-- tryon settings -->
                            <div class="tab-pane fade " id="try-on-settings" role="tabpanel" aria-labelledby="try-on-pill-settings" aria-expanded="false">
                                <form method="POST" id="formTryOnSettings" action="{{ route("admin.userSettings.update") }}" enctype="multipart/form-data">
                                @csrf
                                    <input type="hidden" name="hash" value="try-on-settings" />
                                    <hr class="hr-text" data-content="MirrAR TryOn" />
                                    <div class="row">
                                        <div class="col-12 text-right">
                                            <a href="http://developer.mirrar.co/" target="_blank" class="btn btn-link">Click here to get API key</a>
                                        </div>
                                        <div class="col-12 mb-1">
                                            <label for="tryon_key" class="text-bold-700 h6">API Key</label>
                                            <input class="form-control" id="tryon_key" name="tryon_key" value= "@if(isset($meta_data['tryon_settings']['tryon_key'])) {{  isset($meta_data['tryon_settings']['tryon_key'])?old('tryon_key',$meta_data['tryon_settings']['tryon_key']):''}} @endif">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 mb-1">
                                            <label for="tryon_secretKey" class="text-bold-700 h6">Secret Key</label>
                                            <input class="form-control" id="tryon_secretKey" name="tryon_secretKey" value= "@if(isset($meta_data['tryon_settings']['tryon_secretKey'])) {{ isset($meta_data['tryon_settings']['tryon_secretKey'])?old('tryon_secretKey',$meta_data['tryon_settings']['tryon_secretKey']):''}} @endif">
                                        </div>
                                    </div>
                                    <hr class="hr-text" data-content="TryOn UI Logo" />
                                    <!-- //store tryon logo starts-->
                                    <label>TryOn logo</label>
                                    <br><br>
                                    <div class="row">
                                        <div class="col-12" id="store_tryon_logo_img">
                                            <div style="height:85px;width: 120px;text-align: center;border-style:dashed;padding: 7px;" id="store_tryon_btn" class="rounded mb-0 ml-1">
                                                <a data-toggle="modal" data-target="#storeTryonUploadimgModal" href="{{ route('admin.userSettings.storeMedia') }}" style="color: #626262;">
                                                    <div class="image" id="addstoretryonlogo">
                                                        <i class="feather icon-plus-circle font-large-1 "></i>
                                                    </div>
                                                    <span style="display: block;padding:2px;">Upload Image</span>
                                                </a>
                                            </div>
                                            <br>
                                            
                                            @if($campaign->storetryonlogo != null)
                                            <!--/.Slides-->
                                            <input type="hidden" name='storetryonlogo' value="{{$campaign->storetryonlogo->file_name}}">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12">
                                                    <div class="image-area" id="storetryonlogo_{{$campaign->storetryonlogo->id}}" style="width: 200px; height: 200px;">
                                                        <div style="height:200px;">
                                                            <img class="d-block img-fluid" style="width:100%;min-height:50%;max-height:100%;" src="{{$campaign->storetryonlogo->getUrl() ?? asset('assets/images/placholder.png')}}">
                                                            <a class="remove-image" href="javascript:void(0)" onclick="removestoretryonlogoimg({{$campaign->storetryonlogo->id}},'{{$campaign->storetryonlogo->file_name}}')" style="display: inline;"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                                        </div>
                                                    </div>
                                                    <br>
                                                </div>
                                            </div>
                                            @else
                                            <h3 class="p-4 text-light">No Tryon logo uploaded</h3>
                                            @endif
                                        </div>
                                    </div>
                                    <hr class="hr-text" data-content="TryOn UI Settings" />
                                    <div class="row">
                                        <div class="col-12 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label for="tryon_mode">Virtual TryOn Mode</label>
                                                    <select class="form-control {{ $errors->has('tryon_mode') ? 'is-invalid' : '' }}" name="tryon_mode">
                                                        <option value="mode1" {{ old('tryon_mode', $meta_data['tryon_settings']['tryon_mode'] ?? 'mode1') === "mode1" ? 'selected' : '' }}>Products Side View</option>
                                                        <option value="mode2" {{ old('tryon_mode', $meta_data['tryon_settings']['tryon_mode'] ?? 'mode2') === "mode2" ? 'selected' : '' }}>Full Screen</option>
                                                        <option class="d-none value="mode3" {{ old('tryon_mode', $meta_data['tryon_settings']['tryon_mode'] ?? 'mode3') === "mode3" ? 'selected' : '' }}>Mode 3</option>
                                                        <option class="d-none" value="mode4" {{ old('tryon_mode', $meta_data['tryon_settings']['tryon_mode'] ?? 'mode4') === "mode4" ? 'selected' : '' }}>Mode 4</option>
                                                        <option value="BIG-SCREEN" {{ old('tryon_mode', $meta_data['tryon_settings']['tryon_mode'] ?? 'BIG-SCREEN') === "BIG-SCREEN" ? 'selected' : '' }}>TV Screen (Large Screen)</option>
                                                        <!-- <option value="mode5" {{ old('tryon_mode', $meta_data['tryon_settings']['tryon_mode'] ?? 'mode5') === "mode5" ? 'selected' : '' }}>Mode 5</option>
                                                        <option value="mode6" {{ old('tryon_mode', $meta_data['tryon_settings']['tryon_mode'] ?? 'mode6') === "mode6" ? 'selected' : '' }}>Mode 6</option> -->
                                                       
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label for="tryon_product_detail_mode">Product Detail View Mode</label>
                                                    <select class="form-control {{ $errors->has('tryon_product_detail_mode') ? 'is-invalid' : '' }}" name="tryon_product_detail_mode">
                                                        <option value="DEFAULT" {{ old('tryon_product_detail_mode', $meta_data['tryon_settings']['tryon_product_detail_mode'] ?? 'DEFAULT') === "DEFAULT" ? 'selected' : '' }}>DEFAULT</option>
                                                        <option value="SIDEVIEW" {{ old('tryon_product_detail_mode', $meta_data['tryon_settings']['tryon_product_detail_mode'] ?? 'SIDEVIEW') === "SIDEVIEW" ? 'selected' : '' }}>SIDE VIEW</option>
                                                        <option value="SIDEVIEWINIT" {{ old('tryon_product_detail_mode', $meta_data['tryon_settings']['tryon_product_detail_mode'] ?? 'SIDEVIEWINIT') === "SIDEVIEWINIT" ? 'selected' : '' }}>SIDE VIEW INIT</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        @if(app('impersonate')->isImpersonating())
                                        <div class="col-12 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label for="tryon_version">Tryon Version</label>
                                                    <select class="form-control {{ $errors->has('tryon_version') ? 'is-invalid' : '' }}" name="tryon_version">
                                                        <option value="3.0.1" {{ old('tryon_version', $meta_data['tryon_settings']['tryon_version'] ?? '3.0.1') === "3.0.1" ? 'selected' : '' }}>3.0.1</option>
                                                        <option value="4.0.0" {{ old('tryon_version', $meta_data['tryon_settings']['tryon_version'] ?? '4.0.0') === "4.0.0" ? 'selected' : '' }}>4.0.0</option>
                                                        <option value="4.0.2" {{ old('tryon_version', $meta_data['tryon_settings']['tryon_version'] ?? '4.0.0') === "4.0.2" ? 'selected' : '' }}>4.0.2</option>
                                                        <option value="4.0.3" {{ old('tryon_version', $meta_data['tryon_settings']['tryon_version'] ?? '4.0.3') === "4.0.3" ? 'selected' : '' }}>4.0.3</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6 col-sm-12">
                                        </div>
                                        @endif
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label for="tryon_primary_color">Tryon Primary color</label>
                                                    <input type="color" class="form-control" name="tryon_primary_color" placeholder="Tryon Primary Color" value="{{ old('tryon_primary_color', $meta_data['tryon_settings']['tryon_primary_color'] ?? '#633194')}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label for="tryon_secondary_color">Tryon Secondary color</label>
                                                    <input type="color" class="form-control" name="tryon_secondary_color" placeholder="Tryon Secondary Color" value="{{ old('tryon_secondary_color', $meta_data['tryon_settings']['tryon_secondary_color'] ?? '#ead4ff')}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-sm-6">
                                            <div class="form-group">
                                                <div class="controls">
                                                    <label for="tryon_tertiary_color">Tryon Tertiary color</label>
                                                    <input type="color" class="form-control" name="tryon_tertiary_color" placeholder="Tryon Tertiary Color" value="{{ old('tryon_tertiary_color', $meta_data['tryon_settings']['tryon_tertiary_color'] ?? '#ff0087')}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="tryon_ui_product_list_title" class="text-bold-700 h6">Product Listing Option</label>
                                                <br>
                                                <div class="row ml-1">
                                                    <div class="col-6">
                                                        <input class="form-check-input " type="radio" name="tryon_ui_product_list_title" value="all" @if(isset($meta_data['tryon_settings']['tryon_ui_product_list_title'])) {{ old('tryon_ui_product_list_title',$meta_data['tryon_settings']['tryon_ui_product_list_title'] ?? '') == 'all' ? 'checked':''}}@else checked @endif>
                                                        <span id="tryon_ui_product_list_title_all">&nbsp; All</span>
                                                    </div>
                                                    <div class="col-6">
                                                        <input class="form-check-input " type="radio" name="tryon_ui_product_list_title" value="catagory" @if(isset($meta_data['tryon_settings']['tryon_ui_product_list_title'])) {{ old('tryon_ui_product_list_title',$meta_data['tryon_settings']['tryon_ui_product_list_title'] ?? '') == 'catagory' ? 'checked':''}}@endif>
                                                        <span id="tryon_ui_product_list_title_catagory">&nbsp; Catagory Wise</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="tryon_floating_mode" class="text-bold-700 h6">Virtual TryOn Floating mode (Enable/Disable)</label>
                                                <br>
                                                <div class="row ml-1">
                                                    <div class="col-6">
                                                        <input class="form-check-input " type="radio" name="tryon_floating_mode" value="true" @if(isset($meta_data['tryon_settings']['tryon_floating_mode'])) {{ old('tryon_floating_mode',$meta_data['tryon_settings']['tryon_floating_mode'] ?? '') == 'true' ? 'checked':''}}@else checked @endif>
                                                        <span>&nbsp; Yes</span>
                                                    </div>
                                                    <div class="col-6">
                                                        <input class="form-check-input " type="radio" name="tryon_floating_mode" value="false" @if(isset($meta_data['tryon_settings']['tryon_floating_mode'])) {{ old('tryon_floating_mode',$meta_data['tryon_settings']['tryon_floating_mode'] ?? '') == 'false' ? 'checked':''}}@endif>
                                                        <span>&nbsp; No</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row"> 
                                        <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                            <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save changes</button>
                                            <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                        </div>
                                    </div>
                                    
                                    </form> 
                                </div>    
                                <!-- tryon setting end -->
                                 <!--start store Tryon logo uploading modal -->
                                 <div class="modal fade ecommerce-application" id="storeTryonUploadimgModal" tabindex="-1" role="dialog" aria-labelledby="SelectstoreTryonModal" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="SelectstoreTryonModal">Upload store logo</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="dropzone dropzone-area" id="storetryonimg">
                                                        <div class="dz-message">Upload Tryon logo</div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancel</button>
                                                    <button type="button" class="btn btn-primary" id="storeTryonAdd" data-dismiss="modal" aria-label="Close" disabled="true">Add</button>
                                                    <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end store tryon logo uploading modal -->
                                <!-- seo setting start -->
                                <div class="tab-pane fade " id="analytic-settings" role="tabpanel" aria-labelledby="analytic-pill-settings" aria-expanded="false">
                                    <form method="POST" id="formAnalyticSettings" action="{{ route("admin.userSettings.update") }}" enctype="multipart/form-data">
                                    @csrf
                                        <input type="hidden" name="hash" value="analytic-settings" />
                                        <hr class="hr-text" data-content="Google Analytics" />
                                        <div class="row">
                                            <div class="col-6 mb-1">
                                                <label for="google_analytics_id" class="text-bold-700 h6">Google Analytics Id</label>
                                                <input class="form-control" id="google_analytics_id" name="google_analytics_id" value= "@if(isset($meta_data['analytic_settings']['google_analytics_id'])) {{ old('google_analytics_id',$meta_data['analytic_settings']['google_analytics_id'])}} @endif" required>
                                            </div>
                                            <div class="col-6 mb-1">
                                                <label for="google_analytics_gtm_id" class="text-bold-700 h6">Google Analytics GTM Id</label>
                                                <input class="form-control" id="google_analytics_gtm_id" name="google_analytics_gtm_id" value= "@if(isset($meta_data['analytic_settings']['google_analytics_gtm_id'])) {{ old('google_analytics_gtm_id',$meta_data['analytic_settings']['google_analytics_gtm_id'])}} @endif" required>
                                            </div>
                                        </div>
                                        <div class="row"> 
                                            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                                <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save changes</button>
                                                <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                            </div>
                                        </div>
                                    </form> 
                                </div>

                                <div class="tab-pane fade " id="custom-script-settings" role="tabpanel" aria-labelledby="custom-script-pill-settings" aria-expanded="false">
                                    <form method="POST" id="formCustomScriptSettings" action="{{ route("admin.userSettings.update") }}" enctype="multipart/form-data">
                                    @csrf
                                        <input type="hidden" name="hash" value="custom-script-settings" />
                                        <hr class="hr-text" data-content="Header Script" />
                                        <div class="row">
                                            <div class="col-12">
                                                <label for="custom_script_header" class="text-bold-700 h6">Header Script</label>
                                                <textarea type="text" class="form-control" name="custom_script_header" value="" placeholder="Custom js/css here...">{{ old('custom_script_header', $meta_data['custom_script_settings']['custom_script_header'] ?? '') }}</textarea>
                                            </div>
                                        </div>
                                        <hr class="hr-text" data-content="Footer Script" />
                                        <div class="row">
                                            <div class="col-12 mb-1">
                                                <label for="custom_script_footer" class="text-bold-700 h6">Footer Script</label>
                                                <textarea type="text" class="form-control" name="custom_script_footer" value="" placeholder="Custom js/css here..." >{{ old('custom_script_footer', $meta_data['custom_script_settings']['custom_script_footer'] ?? '') }}</textarea>
                                            </div>
                                        </div>
                                        <div class="row"> 
                                            <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                                <button type="submit" class="btn btn-primary mr-sm-1 mb-1 mb-sm-0">Save changes</button>
                                                <button type="reset" class="btn btn-outline-warning">Cancel</button>
                                            </div>
                                        </div>
                                    </form> 
                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- account setting page end -->
@endsection

@section('scripts')

@parent

<script>

    enquiry();

    $('.payment-method').on('click', function() {
        var div_id = $(this).attr('id') + '-input-div';
        // console.log($('.payment-method:checked').length);
        if ($(this).prop('checked')) {
            checked = true;
            disable_tag('#' + div_id, false);
            require_tag('#' + div_id, true);
        } else {
            checked = false;
            disable_tag('#' + div_id, true);
            require_tag('#' + div_id, false);
        }

        enquiry();

    });

    $('.tax-settings').on('click', function() {
        var div_id = $(this).attr('id') + '-input-div';
        if ($(this).prop('checked')) {
            checked = true;
            disable_tag('#' + div_id, false);
            require_tag('#' + div_id, true);
        } else {
            checked = false;
            disable_tag('#' + div_id, true);
            require_tag('#' + div_id, false);
        }
    });

    $('.extra-charges').on('click', function() {
        var div_id = $(this).attr('id') + '-input-div';
        // var extra_charge_id = $(this).data('extra_charge_id');
        if ($(this).prop('checked')) {
            checked = true;
            disable_tag('#' + div_id, false);
            require_tag('#' + div_id, true);
            // if($("#extra_charge_type_percentage_" + extra_charge_id).prop('checked') == true) {
            //     var extra_charge_type = "percentage";
            // }
            // else if($("#extra_charge_type_flat_charge_" + extra_charge_id).prop('checked') == true) {
            //     var extra_charge_type = "flat_charge";
            // }
            // checkExtraChargeType(extra_charge_type);
        } else {
            checked = false;
            disable_tag('#' + div_id, true);
            require_tag('#' + div_id, false);
        }
    });

    function enquiry() {
        if ($('.payment-method:checked').length == 0)
            $('#enquiry').prop('checked', true);
    }
    function disable_tag(id, flag) {
        $(id).find("textarea").prop('readonly', flag);
        $(id).find("input").prop('readonly', flag);
        $(id).find("input[type='radio']").prop('disabled', flag);
        $(id).find("select").prop('readonly', flag);
    }

    function require_tag(id, flag) {
        if ($(id).find("input").attr('id') != "rp-webhook-secret-key")
            $(id).find("input").prop('required', flag);
        $(id).find("select").prop('required', flag);
        $(id).find("textarea").prop('required', flag);
    }

    var hash = window.location.hash;
    // console.log(hash)
    hash && $('ul.nav a[href="' + hash + '"]').tab('show');

    $('.nav-item .nav-link').click(function(e) {
        $(this).tab('show');
        var scrollmem = $('body').scrollTop();
        window.location.hash = this.hash;
    });
</script>
<script>
    Dropzone.autoDiscover = false;
    myDropzone = new Dropzone('#storelogoimg', {
        url: "{{ route('admin.userSettings.storeMedia') }}",
        maxFilesize: 40, // MB
        maxFiles: 1,
        acceptedFiles: '.jpeg,.jpg,.png,.gif',
        addRemoveLinks: true,
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        params: {
            size: 40
        },
        success: function(file, response) {
            $('#formstorefront').append('<input type="hidden" name="storelogo" value="' + response.name + '">')
            this.options.maxFiles = this.options.maxFiles - 1
        },
        removedfile: function(file) {
            file.previewElement.remove()
            if (file.status !== 'error') {
                $('#formstorefront').find('input[name="storelogo"]').remove()
                this.options.maxFiles = this.options.maxFiles + 1
            }
        },
        init: function() {
            var submitButton = document.querySelector("#storeLogoAdd");
            myDropzone = this; // closure
            submitButton.addEventListener("click", function() {
                if (myDropzone.getUploadingFiles().length === 0 && myDropzone.getQueuedFiles().length === 0) {} else {
                    myDropzone.processQueue();
                }
            });
            this.on("dragend, processingmultiple", function(file) {
                $("#storeLogoAdd").prop('disabled', true);
            });
            this.on("queuecomplete", function(file) {
                if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                    $("#storeLogoAdd").prop('disabled', false);
                } else {
                    $("#storeLogoAdd").prop('disabled', true);
                }
            });
            @if(isset($campaign) && $campaign->storelogo)
            // var file = {!! json_encode($campaign->storelogo) !!}
            // // //     this.options.addedfile.call(this, file)
            // // // file.previewElement.classList.add('dz-complete')
            // $('#formstorefront').append('<input type="hidden" name="storelogo" value="' + file.file_name + '">')
            // this.options.maxFiles = this.options.maxFiles - 1
            @endif
        },
        error: function(file, response) {
            if ($.type(response) === 'string') {
                var message = response //dropzone sends it's own error messages in string
            } else {
                var message = response.errors.file
            }
            file.previewElement.classList.add('dz-error')
            _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
            _results = []
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                node = _ref[_i]
                _results.push(node.textContent = message)
            }

            return _results
        }

    });

    Dropzone.autoDiscover = false;
    myDropzone = new Dropzone('#storetryonimg', {
        url: "{{ route('admin.userSettings.storeMedia') }}",
        maxFilesize: 40, // MB
        maxFiles: 1,
        acceptedFiles: '.jpeg,.jpg,.png,.gif',
        addRemoveLinks: true,
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        params: {
            size: 40
        },
        success: function(file, response) {
            $('#formTryOnSettings').append('<input type="hidden" name="storetryonlogo" value="' + response.name + '">')
            this.options.maxFiles = this.options.maxFiles - 1
        },
        removedfile: function(file) {
            file.previewElement.remove()
            if (file.status !== 'error') {
                $('#formTryOnSettings').find('input[name="storetryonlogo"]').remove()
                this.options.maxFiles = this.options.maxFiles + 1
            }
        },
        init: function() {
            var submitButton = document.querySelector("#storeTryonAdd");
            myDropzone = this; // closure
            submitButton.addEventListener("click", function() {
            console.log('submit');
                if (myDropzone.getUploadingFiles().length === 0 && myDropzone.getQueuedFiles().length === 0) {} else {
                    myDropzone.processQueue();
                }
            });
            this.on("dragend, processingmultiple", function(file) {
                $("#storeTryonAdd").prop('disabled', true);
            });
            this.on("queuecomplete", function(file) {
                if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                    $("#storeTryonAdd").prop('disabled', false);
                } else {
                    $("#storeTryonAdd").prop('disabled', true);
                }
            });
            @if(isset($campaign) && $campaign->storelogo)
            // var file = {!! json_encode($campaign->storelogo) !!}
            // // //     this.options.addedfile.call(this, file)
            // // // file.previewElement.classList.add('dz-complete')
            // $('#formstorefront').append('<input type="hidden" name="storelogo" value="' + file.file_name + '">')
            // this.options.maxFiles = this.options.maxFiles - 1
            @endif
        },
        error: function(file, response) {
            if ($.type(response) === 'string') {
                var message = response //dropzone sends it's own error messages in string
            } else {
                var message = response.errors.file
            }
            file.previewElement.classList.add('dz-error')
            _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
            _results = []
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                node = _ref[_i]
                _results.push(node.textContent = message)
            }

            return _results
        }

    });
</script>
<script>
    Dropzone.autoDiscover = false;
    myDropzone = new Dropzone('#splashlogo', {
        url: "{{ route('admin.userSettings.storeMedia') }}",
        maxFilesize: 40, // MB
        maxFiles: 1,
        acceptedFiles: '.jpeg,.jpg,.png,.gif',
        addRemoveLinks: true,
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        params: {
            size: 40
        },
        success: function(file, response) {
            $('#formsplashscreen').find('input[name="splashlogo"]').remove()
            $('#formsplashscreen').append('<input type="hidden" name="splashlogo" value="' + response.name + '">')
        },
        removedfile: function(file) {
            file.previewElement.remove()
            if (file.status !== 'error') {
                $('#formsplashscreen').find('input[name="splashlogo"]').remove()
                this.options.maxFiles = this.options.maxFiles + 1
            }
        },
        init: function() {
            var submitButton = document.querySelector("#splashLogoAdd");
            myDropzone = this; // closure
            submitButton.addEventListener("click", function() {
                if (myDropzone.getUploadingFiles().length === 0 && myDropzone.getQueuedFiles().length === 0) {} else {
                    myDropzone.processQueue();
                }
            });
            this.on("dragend, processingmultiple", function(file) {
                $("#splashLogoAdd").prop('disabled', true);
            });
            this.on("queuecomplete", function(file) {
                if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                    $("#splashLogoAdd").prop('disabled', false);
                } else {
                    $("#splashLogoAdd").prop('disabled', true);
                }
            });
            @if(isset($campaign) && $campaign->splashlogo)
            /*var file = {!! json_encode($campaign->splashlogo) !!}
                this.options.addedfile.call(this, file)
            file.previewElement.classList.add('dz-complete')
            $('#formsplashscreen').append('<input type="hidden" name="splashlogo" value="' + file.file_name + '">')
            this.options.maxFiles = this.options.maxFiles - 1*/
            @endif
        },
        error: function(file, response) {
            if ($.type(response) === 'string') {
                var message = response //dropzone sends it's own error messages in string
            } else {
                var message = response.errors.file
            }
            file.previewElement.classList.add('dz-error')
            _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
            _results = []
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                node = _ref[_i]
                _results.push(node.textContent = message)
            }

            return _results
        }

    });

    myDropzone = new Dropzone('#invoice_signature', {
        url: "{{ route('admin.userSettings.storeMedia') }}",
        maxFilesize: 40, // MB
        maxFiles: 1,
        acceptedFiles: '.jpeg,.jpg,.png,.gif',
        addRemoveLinks: true,
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        params: {
            size: 40
        },
        success: function(file, response) {
            $('#formInvoiceSettings').find('input[name="invoice_signature"]').remove()
            $('#formInvoiceSettings').append('<input type="hidden" name="invoice_signature" value="' + response.name + '">')
        },
        removedfile: function(file) {
            file.previewElement.remove()
            if (file.status !== 'error') {
                $('#formInvoiceSettings').find('input[name="invoice_signature"]').remove()
                this.options.maxFiles = this.options.maxFiles + 1
            }
        },
        init: function() {
            var submitButton = document.querySelector("#invoiceSignatureAdd");
            myDropzone = this; // closure
            submitButton.addEventListener("click", function() {
                if (myDropzone.getUploadingFiles().length === 0 && myDropzone.getQueuedFiles().length === 0) {} else {
                    myDropzone.processQueue();
                }
            });
            this.on("dragend, processingmultiple", function(file) {
                $("#invoiceSignatureAdd").prop('disabled', true);
            });
            this.on("queuecomplete", function(file) {
                if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                    $("#invoiceSignatureAdd").prop('disabled', false);
                } else {
                    $("#invoiceSignatureAdd").prop('disabled', true);
                }
            });
            @if(isset($campaign) && $campaign->invoice_signature)
            var file = {!! json_encode($campaign->invoice_signature) !!}
            this.options.addedfile.call(this, file)
            file.previewElement.classList.add('dz-complete');
            $('#formInvoiceSettings').append('<input type="hidden" name="invoice_signature" value="' + file['file_name'] + '">')
            this.options.maxFiles = this.options.maxFiles - 1
            @endif
        },
        error: function(file, response) {
            if ($.type(response) === 'string') {
                var message = response //dropzone sends it's own error messages in string
            } else {
                var message = response.errors.file
            }
            file.previewElement.classList.add('dz-error')
            _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
            _results = []
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                node = _ref[_i]
                _results.push(node.textContent = message)
            }

            return _results
        }

    });

    var ct = {!!count($campaign->bannerimg) !!};
    //var ct = max_photos.length;
    // console.log(ct);

    var max_files = 5 - ct;
    // console.log(max_files);
    // document.getElementById('addban').onclick = function() {}
    var uploadedBannerimgMap = {}
    var myDropzone1 = new Dropzone('#bannerimg', {
        url: "{{route('admin.userSettings.storeMedia')}}",
        maxFilesize: 40, // MB
        maxFiles: max_files,
        acceptedFiles: '.jpeg,.jpg,.png,.gif',
        addRemoveLinks: true,
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        params: {
            size: 40,
            width: 4086,
            height: 4096
        },
        success: function(file, response) {
            $('#formstorefront').append('<input type="hidden" name="bannerimg[]" value="' + response.name + '">')
            uploadedBannerimgMap[file.name] = response.name
            //console.log(uploadedBannerimgMap);
        },
        removedfile: function(file) {

            file.previewElement.remove()
            var name = ''
            if (typeof file.file_name !== 'undefined') {
                name = file.file_name
            } else {
                name = uploadedBannerimgMap[file.name]
            }

            $('#formstorefront').find("input[name='bannerimg[]'][value=" + file.file_name + "]").remove()
        },
        init: function() {
            var submitButton = document.querySelector("#bannerImgAdd");
            myDropzone = this; // closure
            submitButton.addEventListener("click", function() {
                if (myDropzone.getUploadingFiles().length === 0 && myDropzone.getQueuedFiles().length === 0) {} else {
                    myDropzone.processQueue();
                }
            });
            this.on("dragend, processingmultiple", function(file) {
                $("#bannerImgAdd").prop('disabled', true);
            });
            this.on("queuecomplete", function(file) {
                if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                    $("#bannerImgAdd").prop('disabled', false);
                } else {
                    $("#bannerImgAdd").prop('disabled', true);
                }
            });
            @if(isset($campaign) && count($campaign->bannerimg) > 0)
            //    var files = {!! json_encode($campaign->bannerimg) !!}
            // //    console.log(files)
            //    files.forEach((file,index)=>{
            //         $('#bannerimg').append('<input type="hidden" name="bannerimg[]" value="' + file.file_name + '">')
            //     //    console.log(value,index);
            //    })
            // var j = 0;
            // for (var i in files) {
            // j++;
            // console.log(j);
            // var file = files[i]
            //         if(files.length == j) 
            //         {
            //             this.options.addedfile.call(this, file)
            //             this.options.thumbnail.call(this, file, file.url)
            //             file.previewElement.classList.add('dz-complete')
            //             $('#bannerimg').append('<input type="hidden" name="bannerimg[]" value="' + file.file_name + '">')
            //         }
            //     }
            @endif
        },
        error: function(file, response) {
            if ($.type(response) === 'string') {
                var message = response //dropzone sends it's own error messages in string
            } else {
                var message = response.errors.file
            }
            file.previewElement.classList.add('dz-error')
            _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
            _results = []
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                node = _ref[_i]
                _results.push(node.textContent = message)
            }
            return _results;
        }
    });

    $(document).ready(function() {
        @if($campaign->splashlogo != null)
        $('#logo_btn').hide();
        $('#logo_').show();
        @else
        $('#logo_btn').show();
        $('#logo_').hide();
        @endif
        @if(count($campaign->bannerimg) == 5)
        $('#banner_btn').hide();
        @endif

        @if(isset($campaign->storelogo) && $campaign->storelogo != null)
        $('#store_logo_btn').hide();
        @endif

        @if(isset($campaign->storetryonlogo) && $campaign->storetryonlogo != null)
        $('#store_tryon_btn').hide();
        @endif

        @if(!empty($telegram_otp))
        $('#telegram_otp_instructions').show();
        var data = "{{$telegram_otp}}";
        $("#telegram_otp").empty().html(data);
        @endif

        @if($campaign->invoice_signature != null)
        $('#signature_btn').hide();
        $('#invoice_signature_').show();
        @else
        $('#signature_btn').show();
        $('#invoice_signature_').hide();
        @endif

        $("#formthemes input[name=name]").on('change', function() {
            if ($(this).data("custom_color") == 1) {
                $("#theme_custom_color").show();
            } else {
                $("#theme_custom_color").hide();
            }
        });

        $('input[name="delivery_charge"]').on('change', function() {
            var delivery_charge_type = $(this).val();
            if (delivery_charge_type == "no_charge") {
                $("#delivery_charge_settings").hide();
                $('input[name="delivery_charge_per_product"]').prop('required', false);
                $('input[name="delivery_charge_per_order"]').prop('required', false);
                $('input[name="free_delivery_above"]').prop('required', false);
                $('input[name="delivery_charge_per_product"]').prop('disabled', true);
                $('input[name="delivery_charge_per_order"]').prop('disabled', true);
                $('input[name="free_delivery_above"]').prop('disabled', true);
            } else {
                $('input[name="free_delivery_above"]').prop('disabled', false);
                $("#delivery_charge_settings").show();
                $('input[name="free_delivery_above"]').prop('required', true);
                if (delivery_charge_type == "charge_per_order") {
                    $('input[name="delivery_charge_per_order"]').prop('disabled', false);
                    $('#delivery_charge_per_product_div').hide();
                    $('#delivery_charge_per_order_div').show();
                    $('input[name="delivery_charge_per_product"]').prop('required', false);
                    $('input[name="delivery_charge_per_order"]').prop('required', true);
                } else if (delivery_charge_type == "charge_per_product") {
                    $('input[name="delivery_charge_per_product"]').prop('disabled', false);
                    $('#delivery_charge_per_order_div').hide();
                    $('#delivery_charge_per_product_div').show();
                    $('input[name="delivery_charge_per_order"]').prop('required', false);
                    $('input[name="delivery_charge_per_product"]').prop('required', true);
                }
            }
        });
    });

    function removebannerimg(id, file_name) {
        $('#loading-bg').show();
        if (confirm("{{ trans('global.areYouSure')}}")) {
            var url = "{{route('admin.banner.removebannerimg')}}";
            // url = url.replace(':id', id);
            $.ajax({
                url: url,
                type: "post",
                data: {
                    'id': id
                },
            }).done(function(data) {
                //alert('Removed image');
                //location.reload();
                // console.log( $('#banner_' + id));
                $('#banner_' + id).remove();
                $('#loading-bg').hide();
                $('#banner_btn').show();

                ct -= 1;
                max_files = 5 - ct;
                // console.log(max_files);
                myDropzone1.options.maxFiles = max_files;

                // console.log($('#formstorefront').find("input[name='bannerimg[]'][value='" + file_name+ "']").remove());
                $('#formstorefront').find("input[name='bannerimg[]'][value='" + file_name + "']").remove()
                // $("#banner_img").load("#banner_img > *");
            }).fail(function(jqXHR, ajaxOptions, thrownError) {
                // alert('No response from server');
            });
        }
    }

    function removeInvoiceSignatureimg(id, file_name) {
        if (confirm("{{ trans('global.areYouSure') }}")) {
            $('#loading-bg').show();
            var url = "{{route('admin.banner.removebannerimg')}}";
            $.ajax({
                url: url,
                type: "post",
                data: {
                    'id': id
                },
            }).done(function(data) {
                $('#invoice_signature_' + id).remove();
                $('#loading-bg').hide();
                $('#signature_btn').show();
                $('#formInvoiceSettings').find("input[name='invoice_signature'][value='" + file_name + "']").remove();
            }).fail(function(jqXHR, ajaxOptions, thrownError) {
                // alert('No response from server');
            });
        }
    }

    function removesplashlogoimg(id, file_name) {
        if (confirm("{{ trans('global.areYouSure') }}")) {
            $('#loading-bg').show();
            var url = "{{route('admin.banner.removebannerimg')}}";
            // url = url.replace(':id', id);
            $.ajax({
                url: url,
                type: "post",
                data: {
                    'id': id
                },
            }).done(function(data) {
                //alert('Removed image');
                //location.reload();
                $('#logo_' + id).remove();
                $('#loading-bg').hide();
                $('#logo_btn').show();

                // console.log( $('#formsplashscreen').find("input[name='splashlogo'][value='" + file_name + "']"));
                $('#formsplashscreen').find("input[name='splashlogo'][value='" + file_name + "']").remove()

            }).fail(function(jqXHR, ajaxOptions, thrownError) {
                // alert('No response from server');
            });
        }
    }

    function removestorelogoimg(id, file_name) {
        if (confirm("{{ trans('global.areYouSure') }}")) {
            $('#loading-bg').show();
            var url = "{{route('admin.banner.removebannerimg')}}";
            $.ajax({
                url: url,
                type: "post",
                data: {
                    'id': id,
                },
            }).done(function(data) {
                //alert('Removed image');
                //location.reload();
                $('#storelogo_' + id).remove();
                $('#loading-bg').hide();
                $('#store_logo_btn').show();

                $('#formstorefront').find("input[name='storelogo'][value='" + file_name + "']").remove()

            }).fail(function(jqXHR, ajaxOptions, thrownError) {
                // alert('No response from server');
            });
        }
    }

    function removestoretryonlogoimg(id, file_name) {
        if (confirm("{{ trans('global.areYouSure') }}")) {
            $('#loading-bg').show();
            var url = "{{route('admin.banner.removebannerimg')}}";
            $.ajax({
                url: url,
                type: "post",
                data: {
                    'id': id,
                },
            }).done(function(data) {
                //alert('Removed image');
                //location.reload();
                $('#storetryonlogo_' + id).remove();
                $('#loading-bg').hide();
                $('#store_tryon_btn').show();

                $('#formTryOnSettings').find("input[name='storetryonlogo'][value='" + file_name + "']").remove()

            }).fail(function(jqXHR, ajaxOptions, thrownError) {
                // alert('No response from server');
            });
        }
    }

    function change_currency(value) {
        var s;
        var label;
        var val = {!!json_encode(App\ User::CURRENCY_SYMBOLS) !!};
        for (label in val) {
            if (label == value) {
                s = (val[label]);
            }
        }
        document.getElementById("currency_label").innerHTML = "Label (" + value + ")";
        document.getElementById("currency_symbol").innerHTML = "Symbol (" + s + ")";
    }

    $('#subdomain').keypress(function(e) {
        var key = e.which;
        return ((key >= 48 && key <= 57) || (key==45) || (key >= 65 && key <= 90) || (key >= 97 && key <= 122));
    });

    $('#customdomain').keypress(function(e) {
        var key = e.which;
        return ((key >= 48 && key <= 57) || (key >= 65 && key <= 90) || (key >= 97 && key <= 122) || (key == 46) || (key == 45));
    });


    notification();

    $('#notification').on('click', function() {
        notification()
    })

    function notification() {
        if ($("#notification").prop('checked') == true)
            $('#order_notification').val("on")
        else
            $('#order_notification').val("off")
    }

    // console.log($('#order_notification').val());

    function generateTelegramOtp() {
        $('#telegram_otp_instructions').show();
        $("#add_telegram_account_btn").prop('disabled', true);
        $.ajax({
            url: "{{route('telegramBot.generateOtp')}}",
            type: "get",
            datatype: "html"
        }).done(function(data) {
            $("#telegram_otp").empty().html(data);
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            // alert('No response from server');
        });
    }

    function deleteTelegramSubscriber(chat_id) {
        var url = "{{route('telegramBot.deleteTelegramBotSuscriber')}}";
        $.ajax({
            url: url,
            type: "post",
            data: {
                'chat_id': chat_id,
            },
        }).done(function(data) {
            $('#subscriber_' + chat_id).remove();
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            // alert('No response from server');
        });
    }

    function resetThemeBackgroundColor() {
        var default_theme_bg_color = "{{isset($background_default_color) ? $background_default_color : ''}}";
        $("input[name=theme_background_color]").val(default_theme_bg_color);
    }

    function resetThemeTextColor() {
        var default_theme_text_color = "{{isset($text_default_color) ? $text_default_color : ''}}";
        $("input[name=theme_text_color]").val(default_theme_text_color);
    }

    function resetThemeMobNavColor(){
        var default_theme_mobNav_color = "{{isset($bg_mobNav_color) ? $bg_mobNav_color : ''}}";
        $("input[name=theme_mobNav_color]").val(default_theme_mobNav_color);
    }

    function resetThemeFooterColor(){
        var default_theme_footer_color = "{{isset($bg_footer_color) ? $bg_footer_color : ''}}";
        $("input[name=theme_footer_color]").val(default_theme_footer_color);
    }

    function resetThemeBodyColor(){
        var default_theme_body_color = "{{isset($bg_body_color) ? $bg_body_color : ''}}";
        $("input[name=theme_body_color]").val(default_theme_body_color);
    }

    function resetAnnouncementBackgroundColor() {
        var default_announcement_bg_color = "#FF0000";
        $("input[name=announcement_background_color]").val(default_announcement_bg_color);
    }

    function resetAnnouncementTextColor() {
        var default_announcement_text_color = "#FFFFFF";
        $("input[name=announcement_text_color]").val(default_announcement_text_color);
    }

    var about_us_quill = new Quill('#about_us-container', {
        modules: {
            'toolbar': [
                [{
                    'font': []
                }, {
                    'size': []
                }],
                ['bold', 'italic', 'underline', 'strike'],
                [{
                    'color': []
                }, {
                    'background': []
                }],
                [{
                    'script': 'super'
                }, {
                    'script': 'sub'
                }],
                [{
                    'header': '1'
                }, {
                    'header': '2'
                }, 'blockquote', 'code-block'],
                [{
                    'list': 'ordered'
                }, {
                    'list': 'bullet'
                }, {
                    'indent': '-1'
                }, {
                    'indent': '+1'
                }],
                ['direction', {
                    'align': []
                }],
                ['link', 'image', 'video', 'formula'],
                ['clean']
            ],
        },
        placeholder: 'About us...',
        theme: 'snow'
    });
    about_us_quill.root.innerHTML = $("#about_us").val();

    var privacy_policy_quill = new Quill('#privacy_policy-container', {
        modules: {
            'toolbar': [
                [{
                    'font': []
                }, {
                    'size': []
                }],
                ['bold', 'italic', 'underline', 'strike'],
                [{
                    'color': []
                }, {
                    'background': []
                }],
                [{
                    'script': 'super'
                }, {
                    'script': 'sub'
                }],
                [{
                    'header': '1'
                }, {
                    'header': '2'
                }, 'blockquote', 'code-block'],
                [{
                    'list': 'ordered'
                }, {
                    'list': 'bullet'
                }, {
                    'indent': '-1'
                }, {
                    'indent': '+1'
                }],
                ['direction', {
                    'align': []
                }],
                ['link', 'image', 'video', 'formula'],
                ['clean']
            ],
        },
        placeholder: 'Privacy policy...',
        theme: 'snow'
    });
    privacy_policy_quill.root.innerHTML = $("#privacy_policy").val();

    var terms_and_conditions_quill = new Quill('#terms_and_conditions-container', {
        modules: {
            'toolbar': [
                [{
                    'font': []
                }, {
                    'size': []
                }],
                ['bold', 'italic', 'underline', 'strike'],
                [{
                    'color': []
                }, {
                    'background': []
                }],
                [{
                    'script': 'super'
                }, {
                    'script': 'sub'
                }],
                [{
                    'header': '1'
                }, {
                    'header': '2'
                }, 'blockquote', 'code-block'],
                [{
                    'list': 'ordered'
                }, {
                    'list': 'bullet'
                }, {
                    'indent': '-1'
                }, {
                    'indent': '+1'
                }],
                ['direction', {
                    'align': []
                }],
                ['link', 'image', 'video', 'formula'],
                ['clean']
            ],
        },
        placeholder: 'Terms & Conditions...',
        theme: 'snow'
    });
    terms_and_conditions_quill.root.innerHTML = $("#terms_and_conditions").val();

    var refund_and_return_policy_quill = new Quill('#refund_and_return_policy-container', {
        modules: {
            'toolbar': [
                [{
                    'font': []
                }, {
                    'size': []
                }],
                ['bold', 'italic', 'underline', 'strike'],
                [{
                    'color': []
                }, {
                    'background': []
                }],
                [{
                    'script': 'super'
                }, {
                    'script': 'sub'
                }],
                [{
                    'header': '1'
                }, {
                    'header': '2'
                }, 'blockquote', 'code-block'],
                [{
                    'list': 'ordered'
                }, {
                    'list': 'bullet'
                }, {
                    'indent': '-1'
                }, {
                    'indent': '+1'
                }],
                ['direction', {
                    'align': []
                }],
                ['link', 'image', 'video', 'formula'],
                ['clean']
            ],
        },
        placeholder: 'Refund and Return Policy...',
        theme: 'snow'
    });
    refund_and_return_policy_quill.root.innerHTML = $("#refund_and_return_policy").val();

    var contact_us_quill = new Quill('#contact_us-container', {
        modules: {
            'toolbar': [
                [{
                    'font': []
                }, {
                    'size': []
                }],
                ['bold', 'italic', 'underline', 'strike'],
                [{
                    'color': []
                }, {
                    'background': []
                }],
                [{
                    'script': 'super'
                }, {
                    'script': 'sub'
                }],
                [{
                    'header': '1'
                }, {
                    'header': '2'
                }, 'blockquote', 'code-block'],
                [{
                    'list': 'ordered'
                }, {
                    'list': 'bullet'
                }, {
                    'indent': '-1'
                }, {
                    'indent': '+1'
                }],
                ['direction', {
                    'align': []
                }],
                ['link', 'image', 'video', 'formula'],
                ['clean']
            ],
        },
        placeholder: 'Contact us...',
        theme: 'snow'
    });
    contact_us_quill.root.innerHTML = $("#contact_us").val();

    var shipping_policy_quill = new Quill('#shipping_policy-container', {
        modules: {
            'toolbar': [
                [{
                    'font': []
                }, {
                    'size': []
                }],
                ['bold', 'italic', 'underline', 'strike'],
                [{
                    'color': []
                }, {
                    'background': []
                }],
                [{
                    'script': 'super'
                }, {
                    'script': 'sub'
                }],
                [{
                    'header': '1'
                }, {
                    'header': '2'
                }, 'blockquote', 'code-block'],
                [{
                    'list': 'ordered'
                }, {
                    'list': 'bullet'
                }, {
                    'indent': '-1'
                }, {
                    'indent': '+1'
                }],
                ['direction', {
                    'align': []
                }],
                ['link', 'image', 'video', 'formula'],
                ['clean']
            ],
        },
        placeholder: 'Shipping Policy...',
        theme: 'snow'
    });
    shipping_policy_quill.root.innerHTML = $("#shipping_policy").val();


    function updatePolicySettings() {
        $("#about_us").val(about_us_quill.root.innerHTML);
        $("#privacy_policy").val(privacy_policy_quill.root.innerHTML);
        $("#terms_and_conditions").val(terms_and_conditions_quill.root.innerHTML);
        $("#refund_and_return_policy").val(refund_and_return_policy_quill.root.innerHTML);
        $("#contact_us").val(contact_us_quill.root.innerHTML);
        $("#shipping_policy").val(shipping_policy_quill.root.innerHTML);
        $('#loading-bg').show();
        $('#productEditorModal').modal('hide');
        $.ajax({
            url: "{{ route('admin.userSettings.update') }}",
            type: "POST",
            data: $('form').serializeArray(),
        }).done(function(data) {
            $('#loading-bg').hide();
            $(".modal-backdrop").hide();
            window.location.reload();
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            $('#loading-bg').hide();
            // console.log('No response from server');
        });
    }

    $('#private_store').click(function() {
        if ($(this).is(':checked')) {
            $('#secret_password_div').css('display', 'block');
        } else {
            $('#secret_password_div').css('display', 'none');
        }
    });

    function isNumberKey(event) {
        // Allow only backspace and delete
        if (event.keyCode == 8) {
            // let it happen, don't do anything
        } else {
            // Ensure that it is a number and stop the keypress
            if (event.keyCode < 48 || event.keyCode > 57) {
                event.preventDefault();
            }
        }
    }

    $("#gst_percentage").on('input', function() {
        var num = $(this).val();
        if ($(this).val() > 100) {
            alert("Enter a percentage value below 100.");
            $(this).val('100');
        }
    });

    function isNumberKeyPercentage(evt, element) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57) && !(charCode == 46 || charCode == 8))
            return false;
        else {
            var len = $(element).val().length;
            var index = $(element).val().indexOf('.');
            if (index > 0 && charCode == 46) {
                return false;
            }
            if (index > 0) {
                var CharAfterdot = (len + 1) - index;
                if (CharAfterdot > 3) {
                    return false;
                }
            }
        }
        return true;
    }

    @if(isset($meta_data['checkout_settings']['extra_charges']))
        var extra_charge_id = {{count($meta_data['checkout_settings']['extra_charges'])}};
    @else
        var extra_charge_id = 0;
    @endif
    
    function createNewExtraCharge() {
        extra_charge_id += 1;
        url = "{{ route('admin.userSettings.createExtraCharge', ':extra_charge_id') }}";
        url = url.replace(':extra_charge_id', extra_charge_id);
        var extra_charges_row = $('<div>').load(url, function(row) {
            $('#extra_charges_section').append(row);
        });
    }
        
    function delete_extra_charge(extra_charge_id) {
        var x = confirm("Are you sure you want to the extra charge?");
        if (x)
            $("#extra_charge_" + extra_charge_id).remove();
        else
            return false;
    }
    
    $('input[name^="extra_charge_type"]').on('change', function() {
        var extra_charge_type = $(this).val();
        var extra_charge_id = $(this).data('extra_charge_id');
        checkExtraChargeType(extra_charge_type);
    });

    function checkExtraChargeType(extra_charge_type)
    {
        if (extra_charge_type == "percentage") {
            $('#extra_charge_amount_div_' + extra_charge_id).hide();
            $('#extra_charge_percentage_div_' + extra_charge_id).show();
            $('#extra_charge_percentage_' + extra_charge_id).prop('required', true);
            $('#extra_charge_amount_' + extra_charge_id).prop('required', false);
        } else if (extra_charge_type == "flat_charge") {
            $('#extra_charge_percentage_div_' + extra_charge_id).hide();
            $('#extra_charge_amount_div_' + extra_charge_id).show();
            $('#extra_charge_percentage_' + extra_charge_id).prop('required', false);
            $('#extra_charge_amount_' + extra_charge_id).prop('required', true);
        }
    }
    // $('#default_template').on('click',function(){
    //     $("#modern_template").attr('disabled', true);
    //     $("#default_template").attr('disabled', false);
    // })
    // $('#modern_template').on('click',function(){
    //     $("#modern_template").attr('disabled', false);
    //     $("#default_template").attr('disabled', true);
    // })
    $('#shipping').on('click',function(){
        if($('#shipping').val()=='on'){
            $('#public-key-id').prop('readonly', function(i, v) { return !v; });
            $('#sp-app-id').prop('readonly', function(i, v) { return !v; });
            $('#sp-sell-id').prop('readonly', function(i, v) { return !v; });
            $('#sp-private-key').prop('readonly', function(i, v) { return !v; });
            $('#sp-address-id').prop('readonly', function(i, v) { return !v; });
            $('#sp-continuous').prop('readonly', function(i, v) { return !v; });
            $('#shyplite_height').prop('readonly', function(i, v) { return !v; });
            $('#shyplite_weight').prop('readonly', function(i, v) { return !v; });
            $('#shyplite_length').prop('readonly', function(i, v) { return !v; });
            $('#shyplite_width').prop('readonly', function(i, v) { return !v; });
        //    $('#public-key-id').removeAttr('readonly');
            
        }
    });
</script>
@endsection