@extends('layouts.admin')
@section('content')
@section('styles')
@parent
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
@endsection
    @livewire('admin.customer.customer-order-list',['id'=>$id])
@endsection
