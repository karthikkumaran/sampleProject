@extends('layouts.admin')
@section('styles')
@parent
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.6.2/viewer.min.css">
@endsection
@section('content')
    @livewire('admin.order.order-details',['id'=>$id])
@endsection