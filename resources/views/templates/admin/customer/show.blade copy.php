@extends('layouts.admin')
@section('styles')
@parent
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.6.2/viewer.min.css">

@endsection
@section('content')
@php
$cmeta_data = json_decode($customer->meta_data, true);
@endphp

<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <a href="{{ url()->previous()}}">
                    <h4 class="text-primary"><i class="feather icon-arrow-left"></i> Back to Customers</h4>
                </a>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">

                <div class="row">
                    <div class="col-md-4 text-left">
                        <h3>Order Details</h3>
                        <h4><b>Order #{{$customer->order->id}}</b></h4>
                        <h4>No. Products : <b>{{ count($customer->orderProducts) }}</b></h4>
                        <h4>Total Price : <b>{!! $cmeta_data['currency'] !!}{{ $cmeta_data['product_total_price'] }}</b></h4>
                    </div>
                    <div class="col-md-4 text-md-center p-1">
                        <a href="https://wa.me/{{$customer->country_code ? $customer->country_code.$customer->mobileno : '91'.$customer->mobileno}}" class="mr-1 mb-1" target="_blank">
                            <i style="color:#25D366" class="fa fa-whatsapp fa-2x"></i></a>
                        <a href="tel:{{$customer->mobileno}}" class="mr-1 mb-1">
                            <i style="color:#2F91F3" class="fa fa-phone fa-2x"></i></a>
                        <a href="{{ route('admin.export_order_pdf', $customer->order_id) }}" class="mr-1 mb-1">
                            <i style="color:red" class="fa fa-file-pdf-o fa-2x"></i></a>
                    </div>
                    <div class="col-md-4 text-md-right ">
                        <h4>Change order status:</h4>
                        <div class="row justify-content-end pb-1">
                            <select class="form-control" style="width:200px;" name="order_status" id='order_status'>
                                @foreach(App\orders::STATUS as $key => $label)
                                <option value="{{ $key }}" {{ old('order_status', $customer->order->status ) ==  $key ? 'selected' : '' }}> {{$label}}</option>
                                @endforeach
                            </select>
                        </div>
                        @if($customer->order->order_mode)
                            <h4>Order mode: <b>{{$customer->order->order_mode=="Online-Storefront" ? "Online" : $customer->order->order_mode}}</b></h4>
                        @endif
                        <b>{{date('d-m-Y h:i:s A', strtotime($customer->created_at))}}</b>

                        @if($customer->order->notes)
                        <div style="border: 1px solid black;border-radius:5px;padding: 5px;background: #e7e9ec;">
                            <h4>Notes : </h4>
                            <p>{!! $customer->order->notes !!}</p>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if($transaction!=null)
    <div class="col-md-12">
        <section id="collapsible">
            <div class="collapse-default collapse-icon">
                <div class="card">
                    <div id="headingCollapse4" class="card-header pb-1 cursor-pointer" data-toggle="collapse" role="button" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">

                        <span class="lead collapse-title text-dark h3">Transaction Details - #{{$transaction->transaction_id}}</span>
                    </div>
                    <div id="collapse4" role="tabpanel" aria-labelledby="headingCollapse4" class="collapse" aria-expanded="false">
                        <div class="card-body">
                            <div class="row">
                                <div class="col col-md-4 order-1 cursor-pointer">
                                    <h5><b>{{$transaction->payment_mode}}</b></h5>
                                    <h5>{{$transaction->payment_method}}</h5>

                                    @if($transaction->payment_mode == "UPI" && !empty($transaction->photo->getUrl('thumb')))
                                    <img id="upi-ss" class="img-fluid p-1" style="max-width: 150px; max-height:150px;" src="{{$transaction->photo->getUrl('thumb')}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                    @endif
                                </div>
                                <div class="col col-md-4 order-2 d-flex flex-md-row flex-column justify-content-md-center">

                                    <a href="#" class="mr-1 mb-1">Order #{{$transaction->order_id}}</a>


                                </div>
                                <div class="col col-md-4 order-3 text-md-right cursor-pointer">

                                    <h5>{{ date('d-m-Y', strtotime($transaction->created_at)) }}</h5>
                                    <h5>{{ date('h:i:s A', strtotime($transaction->created_at)) }}</h5>

                                    <h5><b>{!! $transaction['amount']==0?" ":$cmeta_data['currency'].''.$transaction['amount'] !!}</b></h5>

                                    <h4><b>{{$transaction->status}}</b></h4>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @endif
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3>Customer Details</h3>
            </div>
            <div class="card-body">
                <div class="row no-gutters">
                    <div class="col-md-6  order-1 no-gutters">
                        <div class="row no-gutters">
                            <div class="col col-md-2 no-gutters">
                                Name
                            </div>
                            <div class="col col-md-5 no-gutters">
                                <b> {{$customer->fname ?? "" }}</b>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col col-md-2  no-gutters">
                                Address
                            </div>
                            <div class="col col-md-5 no-gutters">
                                <b>
                                    {{$customer->address1 ?? "" }}<br>
                                    {{$customer->address2 ?? "" }}<br>
                                    {{$customer->city?? "" }}<br>
                                    {{$customer->pincode?? "" }}<br>
                                    {{$customer->state ?? "" }}<br>
                                    {{$customer->country ?? "" }}<br>
                                </b>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 order-2 no-gutters">
                        <div class="row no-gutters">
                            <div class="col col-md-5 no-gutters">
                                Email
                            </div>
                            <div class="col-6 col-md-6 no-gutters">
                                <b>{{ $customer->email ?? "" }}</b>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col col-md-5 no-gutters">
                                Mobile No
                            </div>
                            <div class="col col-md-5 no-gutters">
                                <b>{{ $customer->mobileno ?? "" }}</b>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col col-md-5 no-gutters">
                                Alternative Mobile No
                            </div>
                            <div class="col-6 col-md-5 no-gutters">
                                <b>{{ $customer->amobileno ?? "" }}</b>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col col-md-5 no-gutters">
                                Address Type
                            </div>
                            <div class="col col-md-5 no-gutters">
                                <b>{{ $customer->addresstype?? "" }}</b>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>
</div>
<div class="ecommerce-application">
    <h3>Ordered Product List</h3>

    <section class="list-view d-block mb-4">
        <div class="" id="product_list">

            @foreach ($customer->orderProducts as $oproduct)
            @php
            $pmeta_data = json_decode($oproduct['meta_data'], true);
            $pmeta_data['campaign_name'];
            $product_categories = $pmeta_data['category'];
            $product = $oproduct->product;

            if($oproduct['discount'] != null)
            {
            $discount_price = $oproduct['price'] * ($oproduct['discount']/100);
            $discounted_price = $oproduct['price'] - $discount_price;
            }
            $img = $objimg = asset('XR/assets/images/placeholder.png');
            $obj_id = "";
            if(count($product->photo) > 0) {
            $img = $product->photo[0]->getUrl('thumb');
            $objimg = asset('XR/assets/images/placeholder.png');
            $obj_id = "";
            } else if(count($product->arobject) > 0) {
            if(!empty($product->arobject[0]->object))
            if(!empty($product->arobject[0]->object->getUrl()))
            $img = $product->arobject[0]->object->getUrl();
            }
            if(count($product->arobject) > 0){
            if(!empty($product->arobject[0]->object))
            if(!empty($product->arobject[0]->object->getUrl())) {
            $objimg = $product->arobject[0]->object->getUrl();
            $obj_id = $product->arobject[0]->id;
            }
            }
            @endphp
            <div class="card ecommerce-card" id="product_list_{{$product->id}}">
                <div class="card-content">
                    <div class="item-img text-center d-inline p-1" style="min-height: 12.85rem;">

                        @if(count($product->photo) > 1)
                        <!--Carousel Wrapper-->
                        <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-interval="false" data-ride="carousel">
                            <!--Slides-->
                            <div class="carousel-inner" role="listbox">
                                @foreach($product->photo as $photo)
                                <div class="carousel-item {{$loop->index == 0?'active':''}}">
                                    <img class="img-fluid" style="max-width: 150px; max-height:150px;" src="{{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl('thumb'):$objimg ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                </div>
                                @endforeach
                            </div>
                            <!--/.Slides-->
                            <div class="text-center niceScroll" style="width:200px;overflow-x: scroll; display: inline-block; margin: 0 auto; padding: 3px;">
                                <ol class="carousel-indicators position-relative ">
                                    @foreach($product->photo as $photo)
                                    <li data-target="#carousel-thumb" data-slide-to="{{$loop->index}}" class=" {{$loop->index == 0?'active':''}}" style="width:50px; height:50px;">
                                        <img class="d-block img-fluid" style="max-width:50px;max-height: 50px;" src="{{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl('thumb'):$objimg ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                    </li>
                                    @endforeach
                                </ol>
                            </div>
                        </div>
                        <!--/.Carousel Wrapper-->
                        @else
                        <img class="img-fluid" src="{{$img ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="width: 150px; max-height:150px;">
                        @endif
                        @if(!empty($obj_id))
                        <div class="position-absolute" style="top:0;left:0;">
                            <div class="badge badge-glow badge-info">
                                <img class="img-fluid" src="{{asset('XR/assets/images/ar.png')}}" style="width: 32px; max-height:32px;">
                                <h6 class="text-white m-0">Tryon</h6>
                            </div>
                        </div>
                        @endif
                    </div>

                    <div class="card-body">
                        <div class="item-name">
                            <input type="hidden" class="tryonobj" value="{{$objimg ?? ''}}" />
                            <input type="hidden" class="obj_id" value="{{$obj_id ?? ''}}" />
                            <span>{{$oproduct['name'] ?? 'Untitled'}}</span>
                            <p class="item-company">
                                @if(count($product_categories) > 0)
                                <div class="badge badge-pill badge-glow badge-secondary text-dark mr-1">
                                    {{$product_categories[0]['name']}}
                                </div>
                                @endif
                            </p>
                        </div>
                        <div>
                            <span>{{$oproduct['sku'] ?? ''}}</span>
                        </div>

                        <div>
                            <h4 class="text-dark">{!! $pmeta_data['campaign_name'] !!}</h4>
                        </div>
                        @if($oproduct->notes)
                        <div style="border: 1px solid black;background: #D5CCCB;border-radius:5px;padding: 5px;">
                            <h4 class="text-dark">Note:{{" ".$oproduct->notes}}</h4>
                        </div>

                        @endif
                        <!-- <div>
                                    <p class="item-description update" data-pk="{{$product->id}}"
                                        data-url="{{route('admin.product_list.update',$product->id)}}" data-type="textarea"
                                        data-title="Enter description" data-name="description">
                                        {{$product->description ?? 'No Description'}}
                                    </p>
                                </div> -->
                        <div>
                            <span>{{$product->url ?? ''}}</span>
                        </div>

                    </div>
                    <div class="item-options text-center">
                        <div class="item-wrapper">
                            <div class="item-rating">
                                @if(!empty($obj_id))
                                <!-- <a href="#" data-id="{{$product->id}}"
                                            class="badge badge-pill badge-glow badge-success">Try On</a> -->
                                @endif
                                <!-- <a href="#" onclick="deleteProduct($(this))" data-id="{{$product->id}}"
                                            class="badge badge-pill badge-glow badge-danger"><i
                                                class="feather icon-trash-2 m-0"></i></a> -->
                            </div>
                            <!-- <div class="item-cost"> -->
                            <div class="item-cost text-right">
                                <h6 class="item-price">
                                    {!! $cmeta_data['currency'] !!}
                                    @if($oproduct['discount'] != null)
                                    {{$discounted_price}}
                                    <span class="text-primary"><s>{{$oproduct['price']}}</s></span>
                                    <br><span class="text-warning" style="font-size: 80%;">{{$oproduct['discount'] ?? ''}}%</span>
                                    @else
                                    <span>{{$oproduct['price'] ?? '00.00'}}</span>
                                    @endif
                                </h6>
                            </div>
                            <div class="item-cost text-right">
                                <h6 class="item-price">
                                    Qty: <span>{{$oproduct['qty'] ?? ''}}</span>
                                </h6>

                            </div>
                            <!-- </div> -->
                        </div>
                        <div class="wishlist bg-white">
                            <!-- <i class="fa fa-heart-o mr-25"></i> Wishlist -->
                        </div>
                        <!-- <span class="cart p-0">
                                <a href="{{route('admin.products.edit',$product->id)}}" class="btn btn-block text-white btn-primary"
                                        data-color="primary">Edit
                                </a>
                                </span> -->

                    </div>
                </div>
            </div>
            @endforeach
            @if(count($customer->orderProducts) == 0)
            <h3 class="text-light text-center p-2">No Products added yet, please add</h3>
            @endif
        </div>
    </section>

</div>


@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.6.2/viewer.min.js" integrity="sha512-VzJLwaOOYyQemqxRypvwosaCDSQzOGqmBFRrKuoOv7rF2DZPlTaamK1zadh7i2FRmmpdUPAE/VBkCwq2HKPSEQ==" crossorigin="anonymous"></script>

@parent

<script>
    // document.getElementById("upi-ss").onclick = function() {
    //     $('#upi-ss-modal').modal('toggle');
    // }

    $('#order_status').on('change', function() {
        // console.log($('#order_status').val());
        $.ajax({
            url: "{{route('admin.orders.updateStatus')}}",
            method: 'POST',
            data: {
                order_status: $('#order_status').val(),
                order_id: '{{$customer->order->id}}'
            },
        })
    });
    document.getElementById("upi-ss").onmousemove = function() {

        let toolbar = {
            zoomOut: 3,
            zoomIn: 3,
            prev: 3,
            play: 3,
            next: 3,
            reset: 3,
        }
        const viewer = new Viewer(this, {
            inline: false,
            toolbar: toolbar,
            title: false,
            zoomOnTouch: true,
            slideOnTouch: true,
            movable: false,
            className: "bg-dark",
        });
    }

    $(".niceScroll").niceScroll({
        cursorcolor: "grey", // change cursor color in hex
        cursoropacitymin: 0, // change opacity when cursor is inactive (scrollabar "hidden" state), range from 1 to 0
        cursoropacitymax: 1, // change opacity when cursor is active (scrollabar "visible" state), range from 1 to 0
        cursorwidth: "5px", // cursor width in pixel (you can also write "5px")
        cursorborder: "1px solid #fff", // css definition for cursor border
        cursorborderradius: "5px", // border radius in pixel for cursor
        zindex: "auto", // change z-index for scrollbar div
        scrollspeed: 60, // scrolling speed
        mousescrollstep: 40, // scrolling speed with mouse wheel (pixel)
        touchbehavior: false, // enable cursor-drag scrolling like touch devices in desktop computer
        hwacceleration: true, // use hardware accelerated scroll when supported
        boxzoom: false, // enable zoom for box content
        dblclickzoom: true, // (only when boxzoom=true) zoom activated when double click on box
        gesturezoom: true, // (only when boxzoom=true and with touch devices) zoom activated when pinch out/in on box
        grabcursorenabled: true, // (only when touchbehavior=true) display "grab" icon
        autohidemode: true, // how hide the scrollbar works, possible values: 
        background: "", // change css for rail background
        iframeautoresize: true, // autoresize iframe on load event
        cursorminheight: 32, // set the minimum cursor height (pixel)
        preservenativescrolling: true, // you can scroll native scrollable areas with mouse, bubbling mouse wheel event
        railoffset: false, // you can add offset top/left for rail position
        bouncescroll: false, // (only hw accell) enable scroll bouncing at the end of content as mobile-like 
        spacebarenabled: true, // enable page down scrolling when space bar has pressed
        disableoutline: true, // for chrome browser, disable outline (orange highlight) when selecting a div with nicescroll
        horizrailenabled: true, // nicescroll can manage horizontal scroll
        railalign: "right", // alignment of vertical rail
        railvalign: "bottom", // alignment of horizontal rail
        enabletranslate3d: true, // nicescroll can use css translate to scroll content
        enablemousewheel: true, // nicescroll can manage mouse wheel events
        enablekeyboard: true, // nicescroll can manage keyboard events
        smoothscroll: true, // scroll with ease movement
        sensitiverail: true, // click on rail make a scroll
        enablemouselockapi: true, // can use mouse caption lock API (same issue on object dragging)
        cursorfixedheight: false, // set fixed height for cursor in pixel
        hidecursordelay: 400, // set the delay in microseconds to fading out scrollbars
        irectionlockdeadzone: 6, // dead zone in pixels for direction lock activation
        nativeparentscrolling: true, // detect bottom of content and let parent to scroll, as native scroll does
        enablescrollonselection: true, // enable auto-scrolling of content when selection text
        cursordragspeed: 0.3, // speed of selection when dragged with cursor
        rtlmode: "auto", // horizontal div scrolling starts at left side
        cursordragontouch: false, // drag cursor in touch / touchbehavior mode also
        oneaxismousemode: "auto",
        scriptpath: "", // define custom path for boxmode icons ("" => same script path)
        preventmultitouchscrolling: true, // prevent scrolling on multitouch events
        disablemutationobserver: false,
    });
</script>
@endsection