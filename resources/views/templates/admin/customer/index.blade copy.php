@extends('layouts.admin')
@section('content')
@section('styles')
@parent
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
@endsection
<div class="ecommerce-application">

                    <section id="ecommerce-searchbar">
                        <div class="row justify-content-end">
                          <div class="col">
                             <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
        
            <a class="nav-link active" id="product-all-tab" data-toggle="tab" href="#product-all"
                aria-controls="product-all" role="tab" aria-selected="true">Customers  (<span id="customer_count"></span>)</a>
        </li>
    </ul>
                          </div>
                            <div class="col-md-4 col-sm-12">
                            <!-- {{-- <div class="col-sm-12 "> --}} -->
                                <fieldset class="form-group position-relative">
                                    <input type="text" class="form-control search-product" id="ModalProductSearch"
                                        placeholder="Search here">
                                    <div class="form-control-position">
                                        <i class="feather icon-search"></i>
                                    </div>
                                </fieldset>
                            <!-- </div> -->
                        </div>
                    </section>
 </div>

 {{-- <div class="tab-content"> --}}
     {{-- <div class="tab-pane active" id="order-all" aria-labelledby="order-all-tab" role="tabpanel"> --}}
            <section class="list-view d-block mb-4">
            <div id="customers">
            <div class="" id="customer_list">
            
            </div>
            </div>
            </section>
     {{-- </div>        --}}
{{-- </div> --}}




@endsection
@section('scripts')
@parent

<script>
    var q = "";
    var searchData = "";
    var sort = "";
    var page = 0;
    var filterData = "";


    $(document).ready(function(){
        setTimeout(function(){
            getcustomerData(0);
        }, 1500);  
    });

    // alert("hello");
   


  


 
  $("#ModalProductSearch").on('input', function(){
    // console.log("hi");
         searchData = $(this).val();
        if(searchData.length >= 3 || searchData == "")
            getcustomerData(0);
    });


    function getcustomerData(page) {
         $('.loading').show();

        q = 'q=' + searchData + '&s=' + sort + '&page=' + page + '&filterData=' + filterData;

        $.ajax({
            url: "{{route('admin.customers.ajaxData')}}" + '?' + q,
            type: "get",
            datatype: "html"
        }).done(function (data) {
            $("#customer_list").empty().html(data);
            // $("#customer_list").append(data);
            location.hash = page++;
            // $('.loading').hide();
        }).fail(function (jqXHR, ajaxOptions, thrownError) {
            $('.loading').hide();
            // alert('No response from server');
        });
    }

      $("#customers").on('click', '.pagination a', function (event) {
        event.preventDefault();
        $('li').removeClass('active');
        $(this).parent('li').addClass('active');
        var myurl = $(this).attr('href');
        var page = $(this).attr('href').split('page=')[1];
        getcustomerData(page);
    });
</script>
@endsection
