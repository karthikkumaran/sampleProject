@extends('layouts.admin')
@section('content')
@section('styles')
@parent
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
@endsection
@php
    if(empty($user_meta_data['settings']['currency'])){
        $currency = "₹";
    }
    else{
        if($user_meta_data['settings']['currency_selection'] == "currency_symbol")
            $currency = App\User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
        else
            $currency = $user_meta_data['settings']['currency'];
    }
@endphp
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <a href="{{ route('admin.customers.index')}}"><h4 class="text-primary"><i class="feather icon-arrow-left"></i>Back to Customers</h4></a>
             </div>
        </div>
    </div>
</div>

<section id="ecommerce-searchbar">
                        <div class="row justify-content-end">
                          <div class="col">
                         <h2>Customer Details</h2>

                          </div>
                            <div class="col-md-4 col-sm-12">
                            {{-- <div class="col-sm-12 "> --}}
                                <fieldset class="form-group position-relative">
                                    <input type="text" class="form-control search-product" id="ModalProductSearch"
                                        placeholder="Search here">
                                    <div class="form-control-position">
                                        <i class="feather icon-search"></i>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
</section>


<div class="card ecommerce-card" id="customer_list_{{$customer[0]->id}}">
                
                    <div class="card-body">
                     <div class="row">
                         <div class="col col-md-4 order-1"  style="cursor:pointer;" >
                           
                        
                        
                          <h4><b>{{$customer[0]->fname}} {{$customer[0]->lname ?? ""}}</b></h4>  
                          
                          <h4><b>{{$customer[0]->mobileno ?? ""}}</b></h4> 

                          <h4><b>{{$customer[0]->email ?? ""}}</b></h4>
                           
                        </div>
                         <div  class="col col-md-4 order-2 d-flex flex-md-row flex-column justify-content-md-center justify-content-end">
                            
                           <p><a href="https://wa.me/{{$customer[0]->country_code ? $customer[0]->country_code.$customer[0]->mobileno : '91'.$customer[0]->mobileno}}" target="_blank"><i style="color:#25D366"class="fa fa-whatsapp fa-2x"></i></a></p>&nbsp;&nbsp;&nbsp;&nbsp;
                            <p><a href="tel:{{$customer[0]->mobileno}}" style="padding-left:5px"><i style="color:#2F91F3"class="fa fa-phone fa-2x"></i></a></p>&nbsp;&nbsp;&nbsp;&nbsp;
                            <!-- <p><a href="#" onclick='' style="padding-left:5px"><i style="color:red" class="fa fa-file-pdf-o fa-2x"></i></a></p> -->
            
                         </div>
                          <div  class="col col-md-4 order-3 text-md-right"  style="cursor:pointer;" >

                         <h4>No of Orders : <b>{{$customer[0]->total}}</b></h4> 
                         <h4>Total Amount: <b>{!! $currency.''.$customer_total_price !!}</b></h4> 
                          </div>

                     </div>
                    </div>

</div> 

<section id="ecommerce-products" class="list-view d-block mb-4">
 <h2>Order Details</h2>
<div class="row" id="order_list">
            
</div>
</section>



@endsection
@section('scripts')
@parent
<script>
    var q = "";
    var searchData = "";
    var id = "";
    var page = 0;
    var sort='';
    var filterData='';


$(document).ready(function(){
   setTimeout(function(){
   url=window.location.href.split('/');
   id=url[url.length-1];
   if(id)
   getordersData(0);
   },1400);

});
  
  $("#ModalProductSearch").on('input', function(){
         searchData = $(this).val();
         getordersData(0);
    });


    function getordersData(page) {
         
         $('.loading').show();
        q ='?'+'customerid='+id+'&q=' + searchData + '&s=' + sort + '&page=' + page + '&filterData=' + filterData;

        var url = '{{ route("admin.customers.order") }}'+q;
        // url = url.replace(':id', id);

        //  console.log(url);

        $.ajax({
            url: url,
            type: "get",
            datatype: "html"
        }).done(function (data) {

            // $("order_list").load(function(){
            $("#order_list").empty().html(data);
            $('.loading').hide();
            // });

        }).fail(function (jqXHR, ajaxOptions, thrownError) {
            $('.loading').hide();
            // alert('No response from server');
        });
    }

 
    $("#order_list").on('click', '.pagination a', function (event) {
        event.preventDefault();
        $('li').removeClass('active');
        $(this).parent('li').addClass('active');
        var myurl = $(this).attr('href');
        var page = $(this).attr('href').split('page=')[1];
        getordersData(page);
    }); 
</script>
@endsection
