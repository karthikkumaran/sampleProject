@extends('layouts.admin')
@section('content')
@section('styles')
@parent
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
@endsection
<div class="ecommerce-application">
    <section class="invoice-list-wrapper">
        <div class="card-header">Transactions <span class="float-right">Total Amount:{{ $subscription_payments->sum('plan_price') }}</span></div>    
        <div class="card p-2">
            <div class="card-datatable table-responsive">
                <table class="invoice-list-table table datatable-UserPayments">
                @if(count($subscription_payments) > 0)
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Plan Name</th>
                            <th>Price</th>
                            <th class="text-truncate">Subscribed Date</th>
                            <th class="text-truncate">Expiry Date</th>
                            <th class="cell-fit">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($subscription_payments as $subscription_payment)
                            @php 
                                $payment_metadata = unserialize($subscription_payment->meta_data);
                            @endphp
                            <tr>
                                <td>
                                    {!! $subscription_payment->id ?? "" !!}
                                </td>
                                <td>
                                    {!! $subscription_payment->plan_name ?? "" !!}
                                </td>
                                <td>
                                    {!! $payment_metadata->currency ?? $payment_metadata["currency"] !!} {{$subscription_payment->plan_price}}
                                </td>
                                <td>
                                    {{$subscription_payment->created_at ?? "" }}
                                </td>
                                <td>
                                    {{$subscription_payment->plan_expiry ?? ""}}
                                </td>
                                <td>
                                    <a class="btn btn-xs btn-primary btn-icon" href="{{ route('admin.user_payments.show', $subscription_payment->id) }}">
                                        <i class="feather icon-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                @else
                    <h4 class="text-center">You haven't made any payments yet, once you make any payment you can see the details here.</h4>
                @endif
                </table>
            </div>
        </div>
    </section>
</div>
@endsection