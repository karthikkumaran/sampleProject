@php
if(empty($meta_data['settings']['currency'])){
        $currency = "₹";
    }
    else{
        if($meta_data['settings']['currency_selection'] == "currency_symbol")
            $currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
        else
        $currency = $meta_data['settings']['currency'];
    }
@endphp
<div class="row" id="extra_charge_{{$extra_charge_id}}">
    <input type="hidden" name="extra_charge_enabled_{{$extra_charge_id}}" value="1">
    <input type="hidden" name="extra_charge_id[]" value="{{$extra_charge_id}}">
    <div class="col-12">
        <input type="radio" class="" name="extra_charge_type_{{$extra_charge_id}}" data-extra_charge_id="{{$extra_charge_id}}" value="percentage" checked>&nbsp;Percentage
        <input type="radio" class="" name="extra_charge_type_{{$extra_charge_id}}" data-extra_charge_id="{{$extra_charge_id}}" value="flat_charge" >&nbsp;Flat charge
        <button type="button" class="btn btn-icon btn-danger float-right" id="delete_extra_charge_{{$extra_charge_id}}" onclick="delete_extra_charge({{$extra_charge_id}})"><i class="fa fa-trash fa-2x"></i></button>
    </div>
    <div class="col-12">
        <div class="row mt-1">
            <div class="col-12">
                <div class="form-group">
                    <div class="controls">
                        <label>Charge Name<span class="text-danger">*</span></label>
                        <input type="text" class="form-control" name="extra_charge_name[]" value=""  placeholder="Enter the amount above which delivery will be made free here...">
                    </div>
                </div>
            </div>
            <div class="col-12" id="extra_charge_percentage_div_{{$extra_charge_id}}">
                <div class="form-group">
                    <div class="controls">
                        <label>Charges in percentage<span class="text-danger">*</span></label>
                        <fieldset>
                            <div class="input-group">
                                <input type="number" class="form-control" name="extra_charge_percentage[]" id="extra_charge_percentage" value=""  placeholder="Enter the extra charge percentage here..." step="any">
                                <div class="input-group-append">
                                    <span class="input-group-text">%</span>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            <div class="col-12" id="extra_charge_amount_div_{{$extra_charge_id}}" style="display: none;">
                <div class="form-group">
                    <div class="controls">
                        <label>Flat Charges<span class="text-danger">*</span></label>
                        <fieldset>
                            <div class="input-group">
                                <div class="input-group-append">
                                    <span class="input-group-text">{!! $currency !!}</span>
                                </div>
                                <input type="number" class="form-control" name="extra_charge_amount[]" id="extra_charge_amount" value=""  placeholder="Enter the extra charge amount here..." step="any">
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('input[name^="extra_charge_type"]').on('change', function() {
        var extra_charge_type = $(this).val();
        var extra_charge_id = $(this).data('extra_charge_id');
        if (extra_charge_type == "percentage") {
            $('#extra_charge_amount_div_' + extra_charge_id).hide();
            $('#extra_charge_percentage_div_' + extra_charge_id).show();
            $('input[name="extra_charge_percentage"]').prop('required', true);
            $('input[name="extra_charge_amount"]').prop('required', false);
        } else if (extra_charge_type == "flat_charge") {
            $('#extra_charge_percentage_div_' + extra_charge_id).hide();
            $('#extra_charge_amount_div_' + extra_charge_id).show();
            $('input[name="extra_charge_percentage"]').prop('required', false);
            $('input[name="extra_charge_amount"]').prop('required', true);
        }
    });
</script>