@extends('layouts.admin')
@section('content')
@section('styles')
@parent
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
@endsection
@php
if(empty($user_meta_data['settings']['currency'])){
$currency = "₹";
}
else{
if($user_meta_data['settings']['currency_selection'] == "currency_symbol")
$currency = App\User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
else
$currency = $user_meta_data['settings']['currency'];
}


$cmeta_data = json_decode($customer->meta_data, true);

@endphp
<div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <a href="{{ route('admin.transactions.index')}}">
                    <h4 class="text-primary"><i class="feather icon-arrow-left"></i>Back to Transactions</h4>
                </a>
            </div>
        </div>
    </div>
</div>
<section id="ecommerce-searchbar">
    <div class="row justify-content-end">
        <div class="col">
            <h3>Transaction Details</h3>

        </div>
        <div class="col-md-4 col-sm-12">
            <fieldset class="form-group position-relative">
                <input type="text" class="form-control search-product" id="modalTransactionSearch" placeholder="Search here">
                <div class="form-control-position">
                    <i class="feather icon-search"></i>
                </div>
            </fieldset>
        </div>
    </div>
    </div>
</section>


<div class="card ecommerce-card" id="transactions_list_{{$transaction->id}}">
    <div class="card-body">
        <div class="row">
            <div class="col col-md-4 order-1 cursor-pointer">

                <h4><b>#{{$transaction->transaction_id}}</b></h4>


                <h5 class="mb-1 mt-1"><b>{{$transaction->payment_mode}}</b></h5>


                <h5>{{$transaction->payment_method}}</h5>


                <!-- <h5></h5> -->

            </div>
            <div class="col col-md-4 order-2 d-flex flex-md-row flex-column justify-content-md-center">

                <a href="{{route('admin.orders.show',$customer->order_id)}}" class="mr-1 mb-1 text-center">
                    <h4>Order Details</h4>
                    <i class="feather icon-list fa-2x"></i></a>

            </div>
            <div class="col col-md-4 order-3 text-md-right cursor-pointer">

                <h5>{{ date('d-m-Y', strtotime($transaction->created_at)) }}</h5>
                <h5>{{ date('h:i:s A', strtotime($transaction->created_at)) }}</h5>

                <h5><b>{!! $transaction['amount']==0?" ":$currency.''.$transaction['amount'] !!}</b></h5>

                <h4><b>{{$transaction->status}}</b></h4>

            </div>
        </div>
    </div>

</div>

<h3>Order Details</h3>
<div class="card ecommerce-card" id="order_{{$customer->order_id}}">
    <div class="card-body">
        <div class="row">

            <div class="col col-md-4 order-1 cursor-pointer">

                <h4><b>Order #{{$customer->order->id}}</b></h4>


                <h5 class="mb-1 mt-1"><b>{{$customer->fname}} {{$customer->lname ?? ""}}</b></h5>


                <h5>{{$customer->mobileno ?? ""}}</h5>

                <h5>{{$order->email ?? ""}}</h5>

            </div>
            <div class="col col-md-4 order-2 d-flex flex-md-row flex-column justify-content-md-center">

                <a href="https://wa.me/{{$customer->mobileno}}" class="mr-1 mb-1">
                    <i style="color:#25D366" class="fa fa-whatsapp fa-2x"></i></a>
                <a href="tel:{{$customer->mobileno}}" class="mr-1 mb-1">
                    <i style="color:#2F91F3" class="fa fa-phone fa-2x"></i></a>
                <a href="{{ route('admin.export_order_pdf', $customer->order_id) }}" class="mr-1 mb-1">
                    <i style="color:red" class="fa fa-file-pdf-o fa-2x"></i></a>

            </div>
            <div class="col col-md-4 order-3 text-md-right cursor-pointer">

                <h5>{{ date('d-m-Y', strtotime($customer->created_at)) }}</h5>
                <h5>{{ date('h:i:s A', strtotime($customer->created_at)) }}</h5>

                <h5><b>No. of Products: {{count($customer->orderProducts)}}</b></h5>

                <h4><b>{!! $cmeta_data['product_total_price']==0?" ":$currency.''.$cmeta_data['product_total_price'] !!}</b></h4>

            </div>
        </div>
    </div>

</div>

<div class="ecommerce-application">
    <h3>Ordered Product List</h3>

    <section class="list-view d-block mb-4">
        <div class="" id="product_list">

            @foreach ($customer->orderProducts as $oproduct)
            @php
            $pmeta_data = json_decode($oproduct['meta_data'], true);
            $pmeta_data['campaign_name'];
            $product_categories = $pmeta_data['category'];
            $product = $oproduct->product;

            if($oproduct['discount'] != null)
            {
            $discount_price = $oproduct['price'] * ($oproduct['discount']/100);
            $discounted_price = $oproduct['price'] - $discount_price;
            }
            $img = $objimg = asset('XR/assets/images/placeholder.png');
            $obj_id = "";
            if(count($product->photo) > 0) {
            $img = $product->photo[0]->getUrl('thumb');
            $objimg = asset('XR/assets/images/placeholder.png');
            $obj_id = "";
            } else if(count($product->arobject) > 0) {
            if(!empty($product->arobject[0]->object))
            if(!empty($product->arobject[0]->object->getUrl()))
            $img = $product->arobject[0]->object->getUrl();
            }
            if(count($product->arobject) > 0){
            if(!empty($product->arobject[0]->object))
            if(!empty($product->arobject[0]->object->getUrl())) {
            $objimg = $product->arobject[0]->object->getUrl();
            $obj_id = $product->arobject[0]->id;
            }
            }
            @endphp
            <div class="card ecommerce-card" id="product_list_{{$product->id}}">
                <div class="card-content">
                    <div class="item-img text-center d-inline p-1" style="min-height: 12.85rem;">

                        @if(count($product->photo) > 1)
                        <!--Carousel Wrapper-->
                        <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-interval="false" data-ride="carousel">
                            <!--Slides-->
                            <div class="carousel-inner" role="listbox">
                                @foreach($product->photo as $photo)
                                <div class="carousel-item {{$loop->index == 0?'active':''}}">
                                    <img class="img-fluid" style="max-width: 150px; max-height:150px;" src="{{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl('thumb'):$objimg ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                </div>
                                @endforeach
                            </div>
                            <!--/.Slides-->
                            <div class="text-center niceScroll" style="width:200px;overflow-x: scroll; display: inline-block; margin: 0 auto; padding: 3px;">
                                <ol class="carousel-indicators position-relative ">
                                    @foreach($product->photo as $photo)
                                    <li data-target="#carousel-thumb" data-slide-to="{{$loop->index}}" class=" {{$loop->index == 0?'active':''}}" style="width:50px; height:50px;">
                                        <img class="d-block img-fluid" style="max-width:50px;max-height: 50px;" src="{{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl('thumb'):$objimg ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                    </li>
                                    @endforeach
                                </ol>
                            </div>
                        </div>
                        <!--/.Carousel Wrapper-->
                        @else
                        <img class="img-fluid" src="{{$img ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="width: 150px; max-height:150px;">
                        @endif
                        @if(!empty($obj_id))
                        <div class="position-absolute" style="top:0;left:0;">
                            <div class="badge badge-glow badge-info">
                                <img class="img-fluid" src="{{asset('XR/assets/images/ar.png')}}" style="width: 32px; max-height:32px;">
                                <h6 class="text-white m-0">Tryon</h6>
                            </div>
                        </div>
                        @endif
                    </div>

                    <div class="card-body">
                        <div class="item-name">
                            <input type="hidden" class="tryonobj" value="{{$objimg ?? ''}}" />
                            <input type="hidden" class="obj_id" value="{{$obj_id ?? ''}}" />
                            <span>{{$oproduct['name'] ?? 'Untitled'}}</span>
                            <p class="item-company">
                                @if(count($product_categories) > 0)
                                <div class="badge badge-pill badge-glow badge-secondary text-dark mr-1">
                                    {{$product_categories[0]['name']}}
                                </div>
                                @endif
                            </p>
                        </div>
                        <div>
                            <span>{{$oproduct['sku'] ?? ''}}</span>
                        </div>

                        <div>
                            <h4 class="text-dark">{!! $pmeta_data['campaign_name'] !!}</h4>
                        </div>
                        @if($oproduct->notes)
                        <div style="border: 1px solid black;border-radius:5px;padding: 5px;background: #e7e9ec;">
                            <h4 class="text-dark">Notes :</h4>
                            <p>{{" ".$oproduct->notes}}</p>
                        </div>

                        @endif
                        <!-- <div>
                                    <p class="item-description update" data-pk="{{$product->id}}"
                                        data-url="{{route('admin.product_list.update',$product->id)}}" data-type="textarea"
                                        data-title="Enter description" data-name="description">
                                        {{$product->description ?? 'No Description'}}
                                    </p>
                                </div> -->
                        <div>
                            <span>{{$product->url ?? ''}}</span>
                        </div>

                    </div>
                    <div class="item-options text-center">
                        <div class="item-wrapper">
                            <div class="item-rating">
                                @if(!empty($obj_id))
                                <!-- <a href="#" data-id="{{$product->id}}"
                                            class="badge badge-pill badge-glow badge-success">Try On</a> -->
                                @endif
                                <!-- <a href="#" onclick="deleteProduct($(this))" data-id="{{$product->id}}"
                                            class="badge badge-pill badge-glow badge-danger"><i
                                                class="feather icon-trash-2 m-0"></i></a> -->
                            </div>
                            <!-- <div class="item-cost"> -->
                            <div class="item-cost text-right">
                                <h6 class="item-price">
                                    {!! $cmeta_data['currency'] !!}
                                    @if($oproduct['discount'] != null)
                                    {{$discounted_price}}
                                    <span class="text-primary"><s>{{$oproduct['price']}}</s></span>
                                    <br><span class="text-warning" style="font-size: 80%;">{{$oproduct['discount'] ?? ''}}%</span>
                                    @else
                                    <span>{{$oproduct['price'] ?? '00.00'}}</span>
                                    @endif
                                </h6>
                            </div>
                            <div class="item-cost text-right">
                                <h6 class="item-price">
                                    Qty: <span>{{$oproduct['qty'] ?? ''}}</span>
                                </h6>

                            </div>
                            <!-- </div> -->
                        </div>
                        <div class="wishlist bg-white">
                            <!-- <i class="fa fa-heart-o mr-25"></i> Wishlist -->
                        </div>
                        <!-- <span class="cart p-0">
                                <a href="{{route('admin.products.edit',$product->id)}}" class="btn btn-block text-white btn-primary"
                                        data-color="primary">Edit
                                </a>
                                </span> -->

                    </div>
                </div>
            </div>
            @endforeach
            @if(count($customer->orderProducts) == 0)
            <h3 class="text-light text-center p-2">No Products added yet, please add</h3>
            @endif
        </div>
    </section>
</div>


@endsection