@extends('layouts.admin')
@section('content')
@section('styles')
@parent
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">

@endsection
@php
// dd($user_meta_data);
if(empty($user_meta_data['settings']['currency']))
$currency = "₹";
else
if($user_meta_data['settings']['currency_selection'] == "currency_symbol")
$currency = App\User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
else
$currency = $user_meta_data['settings']['currency'];
@endphp

<div class="ecommerce-application">
    <section id="ecommerce-searchbar">
        <div class="row justify-content-end">
            <div class="col">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="transactions-all-tab" data-toggle="tab" href="#transactions-all" aria-controls="transactions-all" role="tab" aria-selected="true">Transactions ({{ $transactions->total()}})</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4 col-sm-12">
                <fieldset class="form-group position-relative">
                    <input type="text" class="form-control search-transaction" id="modalTransactionSearch" placeholder="Search here">
                    <div class="form-control-position">
                        <i class="feather icon-search"></i>
                    </div>
                </fieldset>
            </div>
        </div>
    </section>
</div>
<div class="tab-content">
    <div class="tab-pane active" id="transactions-all" aria-labelledby="transactions-all-tab" role="tabpanel">
        <section class="list-view d-block mb-4">
            <div id="transactions">
                <div class="" id="transactions_list">

                </div>
            </div>
        </section>
    </div>
</div>


@endsection
@section('scripts')
@parent
<script>
    var q = "";
    var searchData = "";
    var sort = "";
    var page = 0;
    var filterData = "";




    geTransactionsData(0);

    $("#modalTransactionSearch").on('input', function() {
        // console.log("hi");
        searchData = $(this).val();
        // if( searchData != "")
        geTransactionsData(0);
    });


    function geTransactionsData(page) {
        $('#loading-bg').show();
        q = 'q=' + searchData + '&s=' + sort + '&page=' + page + '&filterData=' + filterData;
        $.ajax({
            url: "{{route('admin.transactions.ajaxData')}}" + '?' + q,
            type: "get",
            datatype: "html"
        }).done(function(data) {
            $("#transactions_list").empty().html(data);
            // $("#customer_list").append(data);
            // location.hash = page++;
            $('#loading-bg').hide();
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            $('#loading-bg').hide();


            // alert('No response from server');
        });
    }

    $("#transactions").on('click', '.pagination a', function(event) {

        event.preventDefault();
        $('li').removeClass('active');
        $(this).parent('li').addClass('active');
        var myurl = $(this).attr('href');
        var page = $(this).attr('href').split('page=')[1];
        geTransactionsData(page);

    });
</script>
@endsection