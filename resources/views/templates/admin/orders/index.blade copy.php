@extends('layouts.admin')
@section('content')
@section('styles')
@parent
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">

@endsection
@php
if(empty($user_meta_data['settings']['currency']))
$currency = "₹";
else
if($user_meta_data['settings']['currency_selection'] == "currency_symbol")
$currency = App\User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
else
$currency = $user_meta_data['settings']['currency'];
@endphp

<div class="ecommerce-application">
    <section id="ecommerce-searchbar">
        <div class="row justify-content-end">
            <div class="col">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="product-all-tab" data-toggle="tab" href="#product-all" aria-controls="product-all" role="tab" aria-selected="true">Orders ({{$customers->total()}})</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-10">
                    <select name="" class="form-control search-product mb-1" id="order_status">
                            <option value="*" class="form-control" selected>All</option>
                            @foreach(App\orders::STATUS as $key => $label)
                                <option value="{{ $key }}" class="form-control"> {{$label}}</option>
                            @endforeach
                    </select>
            </div>
            <div class="col-md-4 col-sm-12">
                <fieldset class="form-group position-relative">
                    <input type="text" class="form-control search-product" id="ModalProductSearch" placeholder="Search here">
                    <div class="form-control-position">
                        <i class="feather icon-search"></i>
                    </div>
                </fieldset>
            </div>
        </div>
    </section>
</div>
<div class="tab-content">
    <div class="tab-pane active" id="order-all" aria-labelledby="order-all-tab" role="tabpanel">
        <section class="list-view d-block mb-4">
            <div id="orders">
                <div class="" id="order_list">

                </div>
            </div>
        </section>
    </div>
</div>


@endsection
@section('scripts')
@parent
<script>
    var q = "";
    var searchData = "";
    var sort = "";
    var page = 0;
    var filterData = "";
    var status="*";




    getordersData(0);



    $("#ModalProductSearch").on('input', function() {
        // console.log("hi");
        searchData = $(this).val();
        // if( searchData != "")
        getordersData(0);
    });


    function getordersData(page) {
        $('#loading-bg').show();

        q = 'q=' + searchData + '&s=' + sort + '&page=' + page + '&filterData=' + filterData + '&status=' + status;


        $.ajax({
            url: "{{route('admin.orders.ajaxData')}}" + '?' + q,
            type: "get",
            datatype: "html"
        }).done(function(data) {
            $("#order_list").empty().html(data);
            // $("#customer_list").append(data);
            // location.hash = page++;
            $('#loading-bg').hide();
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            $('#loading-bg').hide();


            // alert('No response from server');
        });
    }

    $("#orders").on('click', '.pagination a', function(event) {
        event.preventDefault();
        $('li').removeClass('active');
        $(this).parent('li').addClass('active');
        var myurl = $(this).attr('href');
        var page = $(this).attr('href').split('page=')[1];
        getordersData(page);
    });

    $('#order_status').on('change', function() {
        status=$(this).val()
        getordersData(0);
    });
</script>
@endsection