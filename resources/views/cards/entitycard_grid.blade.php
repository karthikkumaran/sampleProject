<style>
#overlay {
    border-radius: 0.5rem;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0,0,0,0.5);
  z-index: 2;
}

#text{
  position: absolute;
  top: 50%;
  left: 50%;
  color: white;
  transform: translate(-50%,-50%);
  -ms-transform: translate(-50%,-50%);
}
</style>
@php
if(empty($user_meta_data['settings']['currency']))
$currency = "₹";
else
if($user_meta_data['settings']['currency_selection'] == "currency_symbol")
$currency = App\User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
else
$currency = $user_meta_data['settings']['currency'];
@endphp
@foreach ($data as $key => $card)
@php
$product = $card->product;
$obj_id = "";
$obj_3d = "";

if(isset($product->discount) && $product->discount != null)
{
$discount_price = (float)$product->price * ((float)$product->discount/100);
$discounted_price = round(($product->price - $discount_price),2);
}
$img_share = $img = $objimg = asset('XR/assets/images/placeholder.png');
$obj_id = "";
if(!isset($product->id)){
continue;
}
if(isset($product->photo) && count($product->photo) > 0){
//$img = $product->photo[0]->getUrl('thumb');
$img = $product->photo[0]->getUrl('thumb');
$img_share = $product->photo[0]->getUrl();
$objimg = asset('XR/assets/images/placeholder.png');
$obj_id = "";
} else if(isset($product->arobject) && count($product->arobject) > 0){
if(!empty($product->arobject[0]->object))
if(!empty($product->arobject[0]->object->getUrl('thumb'))) {
$img = $product->arobject[0]->object->getUrl('thumb');
}
}

foreach ($product->arobject as $key => $arobject) {                        
    if(!empty($arobject->object)){
        if($arobject->ar_type == "face") {
            if(!empty($arobject->object)){
                if(!empty($arobject->object->getUrl('thumb'))) {
                    $objimg = $arobject->object->getUrl('thumb');
                    $obj_id = $arobject->id;
                }
            }                        
        }                               
    }    
    if($arobject->ar_type == "surface") {                                
        if(!empty($arobject->object_3d)) {
            $obj_3d = true;
        }        
        if(!empty($arobject->object_3d_ios)) {
            $obj_3d = true;
        }                
    }                        
}



@endphp
<div class="col-lg-4 col-md-6 col-sm-12" id="entitycard_{{$card->id}}">
    <div class="card" style="box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.3)">
        <div class="card-header">
        <h5 class="card-title text-truncate" style="max-width: 250px;"><b><a href="#" type="button" class="update " data-pk="{{$product->id}}" data-url="{{route('admin.product_list.update',$product->id)}}" data-type="text" data-title="Enter name" data-name="name">{{$product->name ?? 'Click here to enter the Title'}}</b></a></h5>
        <a href="javascript:void(0);" class="text-danger ml-1 float-right" onclick="deleteCard({{$card->id}})"><i class="fa fa-trash" style="font-size:18px;"></i></a>
        
    </div>

    <div class="card-body" >
        @if($product->new != 0)
            <img src="{{asset('XR/app-assets/images/icons/new.png')}}" style="position: absolute;z-index:10;width:5vw;height:5vw;"/>
        @endif
        @if($product->featured != 0)
            <img src="{{asset('XR/app-assets/images/icons/featured1.png')}}" style="position: absolute;z-index: 10;left:25.2%;width:5vw;height:5vw;"/>
        @endif
        <div class="d-inline-flex rounded" style="height:180px;width:100%;">
            <div class="h-100 rounded-left d-flex justify-content-center" style="width:45%"><img src="{{$img}}"  class="mw-100 mh-100 rounded-left img-responsive"></div>
            <div class="h-100" style="width:55%">
                <div class="w-100 p-1" style="height:80%;">
                    <h6><a href="#" type="button" class="update" data-pk="{{$product->id}}" data-url="{{route('admin.product_list.update',$product->id)}}" data-type="text" data-title="Enter the sku" data-name="sku"><b>{{$product->sku ?? 'Click here to enter SKU'}}</b></a></h6>
                    <h6 class="text-info"><b>{!!$currency!!} <span class="update" data-pk="{{$product->id}}" data-url="{{route('admin.product_list.update',$product->id)}}" data-type="number" data-title="Enter price" data-name="price">{{$product->price ?? '00.00'}}</b></span></h6>
                    @if(isset($product->discount) && $product->discount != null)
                    <h6><div class="badge badge-pill badge-glow badge-primary mr-1 text-truncate" style="max-width:45%;"><b>{{$product->discount}}% OFF</b></div></h6>
                        <h6 class="text-primary"><b>{!!$currency!!} <span>{{$discounted_price}}</span></b></h6>
                    @endif
                    @if(!empty($product->downgradeable) || $product->downgradeable == "0")
                        @if ($product->downgradeable == 1)
                            <div class="badge badge-pill badge-glow badge-danger mr-1 text-truncate" style="max-width:45%;">Inactive</div>
                        @endif
                    @endif
                    @if(isset($product->categories) && count($product->categories) > 0)
                    <div class="badge badge-pill badge-glow badge-primary mr-1 text-truncate" style="max-width:45%;">
                        {{$product->categories[0]['name']}}
                    </div>
                    @else
                    <div class="" style="max-width:45%;">
                        &nbsp;
                    </div>
                    @endif
                </div>
                <div class="w-100 d-inline-flex" style="height:20%;">
                    <div class="w-50 h-100 float-left pt-1 pl-1"><i class="feather icon-eye" style="font-size: 15px;"> {{$card->views}} </i></div>
                    <div class="w-50 h-100 d-flex justify-content-around pt-1">
                        <div>
                            <a onclick="getProductDetail({{$product->id}})" class="text-dark" data-toggle="modal" data-target="#productEditorModal">
                                <i class="fa fa-pencil" style="font-size: 18px;"></i>
                            </a>
                        </div>
                        <div class="catalog_share_card" {{$campaign->is_start == 1 ? '':'hidden'}} > 
                            <a href="javascript:void(0);" onclick="linkShare($(this))" class="text-primary ml-1" style="color:#6610F2 !important;" data-id="{{$product->id}}" data-name="{{$product->name}}" data-sku="{{$product->sku}}" 
                            data-currency="{!! $currency !!}" 
                            data-price="{{$product->price}}" 
                             data-discount="{{$product->discount}}" 
                             data-discounted_price="{{$discounted_price ?? ''}}" 
                             data-tryonlink="{{!empty($obj_id)? (App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'tryon',$product->id)) :''}}" 
                             data-viewarlink="{{!empty($obj_3d)? (App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'viewar',$product->id)) :''}}"
                              data-img="{{$img_share}}" 
                              data-link="{{$campaign->public_link}}" 
                              data-qrcode="{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'product',$product->id,true)}}"
                               data-title="{{$product->name}}" 
                               data-sharelink="{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'product',$product->id)}}" 
                               data-pdf="false"
                               data-download="false"
                               data-entity="product">
                                <i class="fa fa-share-alt" style="font-size: 18px;"></i> 
                            </a>
                        </div>
                        <div class="catalog_noshare_card" {{$campaign->is_start == 1 ? 'hidden':''}}>
                            <a href="javascript:void(0);" class="text-light ml-1">
                                <i class="feather icon-share-2" style="font-size: 18px;"></i>
                            </a>
                        </div>
                        <div>
                            <a href="javascript:void(0);" class="text-success ml-1 mr-1" style="color:#28C76F !important" onclick="WhatsAppShare('{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'product',$product->id)}}','')">
                                <b><i class="fa fa-whatsapp" style="font-size: 18px;"></i></b>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
        @if(($product->downgradeable == 1) and ($product_enable == "false"))
        <div id="overlay">
          <div id="text"><b>Product Disabled</b></div>
        </div>
        @endif
    </div>

</div>
@endforeach
@if( ($is_search == false) and (count($data) == 0))
<!-- <i class="fa fa-list fa-4x nav-icon text-light text-center p-2 w-100"></i> -->
<h3 class="text-light text-center p-2 w-100"><img src="{{asset('/XR/app-assets/images/icons/AddProduct.png')}}" style="height: 200px;"></h3>
<h3 class="text-light text-center p-2 w-100">This is an empty catalog.</h3>
<h3 class="text-light text-center p-2 w-100">Add products from your camera, photo gallery or my products to build your catalog.</h3>
<div style="border-style:dashed;cursor:pointer;" onclick="addnewproduct()" class="rounded mb-0 m-1 text-center p-2 text-dark w-100">
    <div class="image">
        <i class="feather icon-plus-circle font-large-1"></i>
    </div>
    <span style="display: block;padding:2px;">Add Product</span>
</div>
@endif
@if( ($is_search == true) and (count($data) == 0))
<h3 class="text-light text-center p-2 w-100">Searched product not found.</h3>
@endif
{!! $data->render() !!}
<script>
    $('.update').editable({
        mode: "inline"
    });
</script>