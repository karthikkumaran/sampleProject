@foreach ($data as $key => $product)
    <div class="card ecommerce-card">
        <div class="card-content">
            <div class="item-img text-center" style="min-height: 12.85rem;">
                <a href="#">
                    <img class="img-fluid" src="{{(count($product->photo) > 0)?$product->photo[0]->getUrl('thumb'):$product->arobject->object->getUrl('thumb')}}" alt="img-placeholder"
                    onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="width: 150px; max-height:150px;"></a>
            </div>
            <div class="card-body">
                <div class="item-wrapper">
                    <div class="item-rating">
                        @if(count($product->categories) > 0)
                        <div class="badge badge-pill badge-glow badge-primary mr-1">{{$product->categories[0]['name']}}
                        </div>
                        @endif
                    </div>
                    <div>
                        <h6 class="item-price">
                            ₹ {{$product->price ?? '00.00'}}
                        </h6>
                    </div>
                </div>
                <div class="item-name">
                    <a href="#" type="button" class="addedupdate" data-pk="{{$product->id}}" data-url="{{route('admin.products.update',$product->id)}}" data-type="text" data-title="Enter name" data-name="name">{{$product->name ?? 'No Title'}}</a>
                    <p class="item-company">By <span class="company-name">Amazon</span></p>
                </div>
                <div>
                    <p class="item-description">
                        No Description
                    </p>
                </div>
            </div>
            <div class="item-options text-center">
                <div class="item-wrapper">
                    <div class="item-rating">
                        <!-- <div class="badge badge-primary badge-md">
                                                <span>{{count($product->categories) > 0 ?$product->categories[0]['name']:'' }}</span> <i class="feather icon-star"></i>
                                            </div> -->
                        @if(count($product->categories) > 0)
                        <div class="badge badge-pill badge-glow badge-primary mr-1">{{$product->categories[0]['name']}}
                        </div>
                        @endif
                    </div>
                    <div class="item-cost">
                        <h6 class="item-price">
                            ₹ {{$product->price ?? '00.00'}}
                        </h6>
                    </div>
                </div>
                <div class="wishlist bg-white">
                    <!-- <i class="fa fa-heart-o mr-25"></i> Wishlist -->
                </div>
                <span class="button-checkbox cart p-0">
                    <button type="button" class="btn btn-block text-white btn-primary"
                        data-color="primary">Select</button>
                    <input type="checkbox" name="product_id[]" value="{{$product->id}}" class="hidden" />
                </span>
              
            </div>
        </div>
    </div>
@endforeach

<script>
     $('.addedupdate').editable({
        type: 'text',
     });
    $('#productSelectModalTotal').html('{{$data->total()}}');
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'fa fa-check'
                },
                off: {
                    icon: 'fa fa-unchecked'
                }
            };
            var product_val = $widget.children('input').val();
        // Event Handlers
        $button.on('click', function () {
            
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            var isChecked = $checkbox.is(':checked');
            var selectedProducts = new Array(); 
            if(localStorage.getItem( 'selectedProducts' ) !== null && localStorage.getItem( 'selectedProducts' ) !== "")
                    selectedProducts = JSON.parse(localStorage.getItem( 'selectedProducts' ));
            
            if (isChecked) {
                
                // console.log(localStorage.getItem( 'selectedProducts' ));
                selectedProducts.push(product_val);
                localStorage.setItem("selectedProducts", JSON.stringify(selectedProducts));
                var date = new Date();
                date.setTime(date.getTime() + (15 * 60 * 1000));
                
                localStorage.setItem("expiretime", date);
                if(localStorage.getItem( 'expiretime' ) < Date()) {
                    localStorage.setItem("selectedProducts", "");
                }
            } else {
                selectedProducts = jQuery.grep(selectedProducts, function(value) {
                                return value != product_val;
                                });
                localStorage.setItem("selectedProducts", JSON.stringify(selectedProducts));
            }
            
            updateDisplay();
        });
        $checkbox.on('change', function () {
            // updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            var selectedProducts = new Array(); 
            if(localStorage.getItem( 'selectedProducts' ) !== null && localStorage.getItem( 'selectedProducts' ) !== "")
                    selectedProducts = JSON.parse(localStorage.getItem( 'selectedProducts' ));
            if(selectedProducts.indexOf(product_val) >= 0){
                isChecked = true;
                $checkbox.prop('checked', isChecked);
                // $checkbox.triggerHandler('change');
                
            }
               
            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

                
            // Update the button's color
            if (isChecked) {
                $button
                    // .removeClass('btn-default')
                    .addClass('btn-' + color + ' active')
                    .html('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> Selected');
                // .prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            } else {
                $button
                    // .removeClass('btn-' + color + ' active')
                    .addClass('btn-default')
                    .html('<i class="feather icon-shopping-cart"></i> Select');
                // .prepend('');

            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
</script>
{!! $data->render() !!}
