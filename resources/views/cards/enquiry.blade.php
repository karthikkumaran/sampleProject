@php
if(empty($user_meta_data['settings']['currency']))
$currency = "₹";
else
if($user_meta_data['settings']['currency_selection'] == "currency_symbol")
$currency = App\User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
else
$currency = $user_meta_data['settings']['currency'];
@endphp
@foreach ($enquiries as $key => $enquiry)
@php

$enquiry_meta_data= json_decode($enquiry->meta_data, true);

@endphp
<div class="card ecommerce-card" id="enquiry_list_{{$enquiry->enquiry_id}}">
    <div class="card-body">
        <div class="row">
            <div class="col col-md-4 order-1 cursor-pointer" onclick="location.href='{{route('admin.enquiry.show',$enquiry->enquiry_id)}}';">

                <h4><b>Enquiry #{{$enquiry->enquiry->enquiry_id}}</b></h4>


                <h5 class="mb-1 mt-1"><b>{{$enquiry->fname}} {{$enquiry->lname ?? ""}}</b></h5>


                <h5>{{$enquiry->mobileno ?? ""}}</h5>


                <h5>{{$enquiry->email ?? ""}}</h5>

                @php
                if(App\Enquiry::STATUS[$enquiry->enquiry->status] == 'Enquiry'):
                $class="text-primary";
                elseif (App\Enquiry::STATUS[$enquiry->enquiry->status] == 'Lead'):
                $class="text-info";
                elseif (App\Enquiry::STATUS[$enquiry->enquiry->status] == 'Contacted'):
                $class="text-success";
                endif;
                @endphp

                <h5>Status: <span class='{{$class}}'>{{App\Enquiry::STATUS[$enquiry->enquiry->status]}}</span></h5>

            </div>
            <div class="col col-md-4 order-2 d-flex flex-md-row flex-column justify-content-md-center">

                <a href="https://wa.me/{{$enquiry->country_code ? $enquiry->country_code.$enquiry->mobileno : '91'.$enquiry->mobileno}}" class="mr-1 mb-1" target="_blank">
                    <i style="color:#25D366" class="fa fa-whatsapp fa-2x"></i></a>
                <a href="tel:{{$enquiry->mobileno}}" class="mr-1 mb-1">
                    <i style="color:#2F91F3" class="fa fa-phone fa-2x"></i></a>

            </div>
            <div class="col col-md-4 order-3 text-md-right cursor-pointer" onclick="location.href='{{route('admin.enquiry.show',$enquiry->enquiry_id)}}';">

                <h5>{{ date('d-m-Y', strtotime($enquiry->created_at)) }}</h5>
                <h5>{{ date('h:i:s A', strtotime($enquiry->created_at)) }}</h5>

                <h5><b>No. of Products: {{count($enquiry->enquiryProducts)}}</b></h5>

                <h4><b>{!! $enquiry_meta_data['product_total_price']==0?" ":$currency.''.$enquiry_meta_data['product_total_price']!!}</b></h4>

            </div>
        </div>
    </div>

</div>

@endforeach
@if(count($enquiries) == 0)
<h3 class="text-light text-center p-2 w-100">No Enquiries exist</h3>
@endif
{!! $enquiries->render() !!}