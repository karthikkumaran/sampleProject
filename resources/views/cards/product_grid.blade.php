@php
    if(empty($user_meta_data['settings']['currency'])){
        $currency = "₹";
    }
    else{
        if($user_meta_data['settings']['currency_selection'] == "currency_symbol")
            $currency = App\User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
        else
            $currency = $user_meta_data['settings']['currency'];
    }@endphp

@foreach ($data as $key => $product)
    @php
        $img = $objimg = asset('XR/assets/images/placeholder.png');
        $obj_id = "";
        if(count($product->photo) > 0){
            $img = $product->photo[0]->getUrl('thumb');
            $objimg = asset('XR/assets/images/placeholder.png');
            $obj_id = "";
        } else if(count($product->arobject) > 0) {
            if(!empty($product->arobject[0]->object))
                if(!empty($product->arobject[0]->object->getUrl('thumb'))) {
                    $img = $product->arobject[0]->object->getUrl('thumb');
                }
        }
        if(count($product->arobject) > 0){
            if(!empty($product->arobject[0]->object))
                if(!empty($product->arobject[0]->object->getUrl('thumb'))) {
                    $objimg = $product->arobject[0]->object->getUrl('thumb');
                    $obj_id = $product->arobject[0]->id;
                }
        }
    @endphp 
    <div class="col-lg-4 col-md-6 col-sm-12" >
        <div class="card" style="box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.3) !important;padding:3px;" id="card_{{$product->id}}">
            <div class="card-content">
                <div class="rounded float-left" style="background:#dee1e4;margin:5px 15px 0px 5px;padding:5px 5px 0px 5px;min-height:100px;max-height:100px;">
                    <img src="{{$img}}" width="100" class="" style="max-height:90px;">
                </div>
                <div class="card-body p-0" style="margin:5px;">
                    <h5 class="card-title text-truncate"><b>{{$product->name ?? 'Untitled'}}</b></h5>
                    <h6><b>{{$product->sku ?? 'No SKU'}}</b></h6>
                    <h6 class="text-info"><b>{!!$currency!!} {{$product->price ?? '00.00'}}</b></h6>
                    @if(isset($product->categories) && count($product->categories) > 0)
                        <div class="badge badge-pill badge-glow badge-primary mr-1 text-truncate" style="max-width:45%;">
                            {{$product->categories[0]['name']}}
                        </div>
                    @else
                        <div class="" style="max-width:45%;">
                            &nbsp;
                        </div>
                    @endif
                    <!-- <div class="card-text mb-25 text-truncate" style="width:50%;">{!! strip_tags($product->description ?? '&nbsp;') !!}</div> -->
                </div>  
                <div class="d-flex justify-content-between float-right">
                    <span class="button-checkbox cart p-0">
                        <button type="button" class="btn btn-sm btn-block text-white btn-primary" data-catagory_id="{{ (count($product->categories)>0)?$product->categories[0]->id:''}}"
                            data-color="primary"  data-product_id="{{$product->id}}">Select</button>
                        <input type="checkbox" value="{{$product->id}}" class="hidden" />
                        <input type="checkbox" value="{{$videoid}}" name="videoid" class="hidden" />
                    </span>   
                </div>                       
            </div>
        </div>
    </div>
@endforeach
@if( ($is_search == false and $is_filter == false) and (count($data) == 0))
    <h3 class="text-light text-center p-2 w-100">No Products added yet, please add</h3>
@endif

@if( ($is_search == true or $is_filter == true) and (count($data) == 0))
<h3 class="text-light text-center p-2 w-100">Searched product not found.</h3>
@endif
<script>
    $('.update').editable({
        mode: "inline"
    });

$('#productSelectModalTotal').html('{{$data->total()}}');
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'fa fa-check'
                },
                off: {
                    icon: 'fa fa-unchecked'
                }
            };
            var product_val = $widget.children('input').val();
            var catagory_id = $button.data("catagory_id");
            
        // Event Handlers
        $button.on('click', function () {
            
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            var isChecked = $checkbox.is(':checked');
            var selectedProducts = new Array(); 
            var productData = new Array();
            if(localStorage.getItem( 'selectedProducts' ) !== null && localStorage.getItem( 'selectedProducts' ) !== "")
                    selectedProducts = JSON.parse(localStorage.getItem( 'selectedProducts' ));
            
            if (isChecked) {
                
                // console.log(localStorage.getItem( 'selectedProducts' ));
                
                productData.push(product_val);
                productData.push(catagory_id);
                selectedProducts.push(productData);
                localStorage.setItem("selectedProducts", JSON.stringify(selectedProducts));
                var date = new Date();
                date.setTime(date.getTime() + (15 * 60 * 1000));
                
                localStorage.setItem("expiretime", date);
                if(localStorage.getItem( 'expiretime' ) < Date()) {
                    localStorage.setItem("selectedProducts", "");
                }
            } else {
                selectedProducts = selectedProducts.filter(ar => ar[0] != product_val);
                // selectedProducts = jQuery.grep(selectedProducts, function(value) {
                //                 return value != product_val;
                //                 });
                localStorage.setItem("selectedProducts", JSON.stringify(selectedProducts));
            }
            
            updateDisplay();
        });
        $checkbox.on('change', function () {
            // updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            var selectedProducts = new Array(); 
            if(localStorage.getItem( 'selectedProducts' ) !== null && localStorage.getItem( 'selectedProducts' ) !== "")
                    selectedProducts = JSON.parse(localStorage.getItem( 'selectedProducts' ));
            if(selectedProducts.length > 0){
                $("#add_btn").attr('disabled', false);
            }
            else{
                $("#add_btn").attr('disabled', true);
            }
            let productSelected = selectedProducts.some((product) => product[0] == product_val);
            if(productSelected == true){
                isChecked = true;
                $checkbox.prop('checked', isChecked);
                // $checkbox.triggerHandler('change');
                
            }
               
            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

                
            // Update the button's color
            if (isChecked) {
                $('#card_'+$button.data('product_id')).addClass('border-primary');
                $button
                    // .removeClass('btn-default')
                    .removeClass('btn-primary'  + ' active')
                    .addClass('btn-warning' + ' active')
                    .html('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> Selected');
                // .prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            } else {
                $('#card_'+$button.data('product_id')).removeClass('border-primary');

                $button
                    .addClass('btn-primary' + ' active')
                    .removeClass('btn-warning'  + ' active')
                    .addClass('btn-default')
                    .html('Select');
                // .prepend('');

            }
        }

        // Initialization
        function init() {
            localStorage.setItem("selectedProducts", "");
            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });

    function selectAllMyProductsToCatalog(){
        $("#select_all_my_products").hide();
        $("#deselect_all_my_products").show();
        $('.button-checkbox').each(function () {
            var $widget = $(this),
                $button = $widget.find('button'),
                $checkbox = $widget.find('input:checkbox');
            var isChecked = $checkbox.is(':checked');
            if(!isChecked){
                $button.click();
            }
        });
    }
    function deselectAllMyProductsToCatalog(){
        $("#deselect_all_my_products").hide();
        $("#select_all_my_products").show();
        $('.button-checkbox').each(function () {
            var $widget = $(this),
                $button = $widget.find('button');
            $button.click();
        });
    }
</script>
{!! $data->render() !!}
