@php
if(empty($user_meta_data['settings']['currency']))
        $currency = "₹";
else
    if($user_meta_data['settings']['currency_selection'] == "currency_symbol")
        $currency = App\User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
    else
        $currency = $user_meta_data['settings']['currency']
@endphp
@foreach ($data as $key => $card)
    @php
        $product = $card->product;
        if(!isset($product->id)){
            continue;
        }

        if($product->discount != null){
            $discount_price = $product->price * ($product->discount/100);
            $discounted_price = round(($product->price - $discount_price),2);
        }

        if(count($product->photo) > 0){
            $img = $product->photo[0]->getUrl('thumb');
            $objimg = asset('XR/assets/images/placeholder.png');
            $obj_id = "";
        } else if(count($product->arobject) > 0){
            if(!empty($product->arobject[0]->object))
                if(!empty($product->arobject[0]->object->getUrl('thumb'))) {
                    $img = $product->arobject[0]->object->getUrl('thumb');
                }
        } else {
            $img = asset('XR/assets/images/placeholder.png');
            $objimg = asset('XR/assets/images/placeholder.png');
        }

        if(count($product->arobject) > 0){
            if(!empty($product->arobject[0]->object))
                if(!empty($product->arobject[0]->object->getUrl('thumb'))) {
                    $objimg = $product->arobject[0]->object->getUrl('thumb');
                    $obj_id = $product->arobject[0]->id;
                }
        }
    @endphp

    @if((new \Jenssegers\Agent\Agent())->isDesktop())
        <div class="col-lg-4 col-md-6 col-12 rounded" id="entitycard_{{$product->id}}">
    @else
        <div class="col-md-4 col-12 rounded" id="entitycard_{{$product->id}}">
    @endif
        <div class="card rounded mb-1" style="box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.3) !important;">
            <div class="card-content">
                @if($product->discount != null)
                    <span class="badge badge-primary badge-pill" style="position: absolute;top:5px;left:5px;z-index: 10;">{{$product->discount}}% OFF</span>
                @endif
                <div class="w-100 d-flex justify-content-center" style="text-align: center;"><img src="{{$img}}" class="w-100 rounded-top" style="height:180px;"></div>
                <div class="w-100 bg-white" style="padding:10px;">
                    @if(count($product->categories) > 0)
                        <div class="badge badge-pill badge-glow badge-primary text-white w-50 text-truncate mr-1 " style="position: absolute;right: 0;margin-top:-18px;">{{$product->categories[0]['name']}}</div>
                    @endif
                        <span class="h-50 text-primary">
                            <strong>
                                {!!$currency!!}
                            </strong>
                        </span>
                    @if($product->discount != null)
                        <strong>
                            <span class="text-primary">
                                {{$discounted_price}}
                            </span>
                            <span class="text-dark">
                                <s>{{$product->price}}</s>
                            </span>
                        </strong>
                    @else
                        <span class="h-50 text-primary"><strong>{{$product->price ?? '00.00'}}</strong></span>
                    @endif
                    <div class="h-50 text-truncate"><strong>{{$product->name ?? 'Untitled'}}</strong></div>
                    @if(isset($user_meta_data['settings']['pos_show_sku']) && $user_meta_data['settings']['pos_show_sku'] == 1)
                        @if(isset($product->sku))
                            <div class="h-50 text-truncate"><strong>SKU: {!! $product->sku ?? '' !!}</strong></div>
                        @else
                            <div class="h-50 text-truncate"><strong><br></strong></div>
                        @endif
                    @endif
                    @if(isset($user_meta_data['settings']['pos_show_inventory']) && $user_meta_data['settings']['pos_show_inventory'] == 1)
                        @if($product->out_of_stock == null && $product->stock != null)
                        <div class="h-50 text-truncate"><strong>Stock: {{$product->stock}}</strong></div>
                    @else
                        <br>
                    @endif
                    @endif
                </div>

                @if($product->out_of_stock != null)
                    <div class="w-100 rounded-bottom text-white text-center font-weight-bold bg-warning" style="padding:10px;">
                        <span> OUT OF STOCK </span>
                    </div>
                @else

                    <div class="cart cart_{{ $product->sku }} w-100 rounded-bottom text-white text-center font-weight-bold bg-primary" style="cursor: pointer;padding:10px;" id="product_{{$product->sku}}">
                    @if(!empty($product->shop) && $product->external_shop == 1)
                     <a href="{{$product->link}}" style="color:white;" target="_blank">{{$product->shop}}</a>
                        @else
                        @if($product->has_variants == 1)
                        <a>
                            <div class="item-wrapper d-inline-flex  h-100" onclick="getPosProductDetails({{$card->id}},{{$campaign->id}})" data-toggle="modal" data-target="#posproductDetailsModal" style="cursor:pointer;"><i class="feather icon-box"></i>View Product</div>
                        </a>
                        @else
                        <span class="add-to-cart"data-id="{{$product->id}}" data-name="{{$product->name ?? ''}}" data-description="{{$product->description ?? ''}}"
                            data-price="{{$product->price ?? '0'}}" data-discount="{{$product->discount ?? '0'}}" data-discounted_price="{{$product->discount ? $discounted_price:'0'}}" data-catalogid="{{$card->campaign_id}}" data-catagory="{{(count($product->categories) > 0)?$product->categories[0]['name']:''}}"
                            data-quantity="{{$product->minimum_quantity ?? '1'}}" data-minimum_quantity="{{$product->minimum_quantity ?? '1'}}" data-maximum_quantity="{{ $product->maximum_quantity ?? '10000' }}"
                            data-stock="{{$product->stock ?? '0'}}" data-img="{{$img}}"  data-sku="{{ $product->sku }}">
                            <i class="feather icon-shopping-cart"></i> ADD TO CART
                        </span>
                        <span class="remove-from-cart d-none" id="product_view_{{$product->sku}}">
                            <i class="fa fa-trash-o"></i> REMOVE
                        </span>
                    @endif
                    @endif
                    </div>
                @endif
        </div>
    </div>
</div>
<div class="modal fade ecommerce-application" id="posproductDetailsModal" tabindex="-1" role="dialog" aria-labelledby="posproductDetailsModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="productDetailsModalTitle">Product Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="row" id="posproductDetails">

                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
@if( (($is_search == false) and ($is_filter == false)) and (count($data) == 0))
    <h3 class="text-light text-center p-2 w-100">"No products in your store"</h3>
@endif
@if( (($is_search == true) or ($is_filter == true)) and (count($data) == 0))
    <h3 class="text-light text-center p-2 w-100">"No products found matching your search or filter criteria"</h3>
@endif
{!! $data->render() !!}

<script>
var preview = 0;
    var u = window.location.href;
    if (u.includes("preview")) {
        preview = 1;
    }

    function getPosProductDetails(card_id,campaign_id,video_id="") {
        $('.loading1').show();
        var q = "";
        q = '&card_id= ' + card_id + '&preview=' +preview+ '&campaign_id=' + campaign_id + '&video_id=' + video_id;
        $.ajax({
            url: "{{route('admin.campaigns.ajaxPosProductDetails')}}" + '?' + q,
            type: "get",
            datatype: "html"
        }).done(function(data) {
            $("#posproductDetails").empty().html(data);
            if (localStorage.getItem("posCart") != null) {
                const now = new Date();
                var now_time = now.getTime();
                var expire_time = localStorage.getItem("expiretime");
                if (now_time > expire_time) {
                    obj.removeItemFromCartAll();
                    obj.totalCart();
                } else {
                    loadCart();
                }
            } else {
                document.getElementById("place_order").style.pointerEvents = "none";
                document.getElementById("hold_cart").style.pointerEvents = "none";
                $(".total-discount").html("0");
                $(".total-amt").html("{!! $currency !!} 0");
                $(".cart-item-count").html("0");
                $(".checkout-items").empty();
                $(".checkout-items").html("<h2 class='text-light text-center'><i class='pt-1 ficon feather icon-shopping-cart fa-3x'></i><br>No item in cart</h2>");
            }
            $('.loading1').hide();
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            $('.loading1').hide();
            // alert('No response from server');
        });
    }

    function getPosSimpleVariantDetails(cardid,campaignid,variantid){
        $('.loading1').show();
        var q = "";
        q = '&card_id= ' + cardid + '&preview=' + preview +  '&campaign_id=' + campaignid + '&productvariantid=' + variantid;
        // $("#posproductDetailsModal").modal('show');
        $.ajax({
            url: "{{route('admin.campaigns.ajaxPosProductVariantDetails')}}" + '?' + q,
            type: "get",
            datatype: "html"
        }).done(function(data) {
            $("#posproductDetails").empty().html(data);
            if (localStorage.getItem("posCart") != null) {
                const now = new Date();
                var now_time = now.getTime();
                var expire_time = localStorage.getItem("expiretime");
                if (now_time > expire_time) {
                    obj.removeItemFromCartAll();
                    obj.totalCart();
                } else {
                    loadCart();
                }
            } else {
                document.getElementById("place_order").style.pointerEvents = "none";
                document.getElementById("hold_cart").style.pointerEvents = "none";
                $(".total-discount").html("0");
                $(".total-amt").html("{!! $currency !!} 0");
                $(".cart-item-count").html("0");
                $(".checkout-items").empty();
                $(".checkout-items").html("<h2 class='text-light text-center'><i class='pt-1 ficon feather icon-shopping-cart fa-3x'></i><br>No item in cart</h2>");
            }
            $('.loading1').hide();
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            $('.loading1').hide();
            // alert('No response from server');
        });
    }

    function getPosCombinationalVariantDetails(cardid,campaignid,key,value,previousproductid)
    {
        // alert("hi");

        $('.loading1').show();
        var q = "";
        q = '&card_id= ' + cardid + '&preview=' + preview + '&campaign_id=' + campaignid + '&key=' + key + '&value=' + value + '&previousproductid=' + previousproductid;
        $.ajax({
            url: "{{route('admin.campaigns.ajaxPosCombinationalProductVariantDetails')}}" + '?' + q,
            type: "get",
            datatype: "html"
        }).done(function(data) {
            // addProductToCart($(this));
            $("#posproductDetails").empty().html(data);
            if (localStorage.getItem("posCart") != null) {
                const now = new Date();
                var now_time = now.getTime();
                var expire_time = localStorage.getItem("expiretime");
                if (now_time > expire_time) {
                    obj.removeItemFromCartAll();
                    obj.totalCart();
                } else {
                    loadCart();
                }
            } else {
                document.getElementById("place_order").style.pointerEvents = "none";
                document.getElementById("hold_cart").style.pointerEvents = "none";
                $(".total-discount").html("0");
                $(".total-amt").html("{!! $currency !!} 0");
                $(".cart-item-count").html("0");
                $(".checkout-items").empty();
                $(".checkout-items").html("<h2 class='text-light text-center'><i class='pt-1 ficon feather icon-shopping-cart fa-3x'></i><br>No item in cart</h2>");
            }
            $('.loading1').hide();
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            $('.loading1').hide();
            // alert('No response from server');
        }) ;
    }
</script>
<script src="{{asset('XR/assets/js/pos_cart.js')}}"></script>

