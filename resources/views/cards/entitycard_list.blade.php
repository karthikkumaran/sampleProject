@php
if(empty($user_meta_data['settings']['currency']))
$currency = "₹";
else
$currency = App\User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
@endphp

@foreach ($data as $key => $product)
<div class="col-lg-3 col-md-6 col-sm-12">
    <div class="card ecommerce-card">
        <div class="card-content">
            <div class="item-img text-center" style="min-height: 12.85rem;">
                <a href="#">
                    @php
                    $img = $objimg = asset('XR/assets/images/placeholder.png');
                    $obj_id = "";
                    if(count($product->photo) > 0){
                    $img = $product->photo[0]->getUrl('thumb');
                    $objimg = asset('XR/assets/images/placeholder.png');
                    $obj_id = "";
                    } else if(count($product->arobject) > 0) {
                    if(!empty($product->arobject[0]->object))
                    if(!empty($product->arobject[0]->object->getUrl('thumb'))) {
                    $img = $product->arobject[0]->object->getUrl('thumb');
                    }
                    }

                    if(count($product->arobject) > 0){
                    if(!empty($product->arobject[0]->object))
                    if(!empty($product->arobject[0]->object->getUrl('thumb'))) {
                    $objimg = $product->arobject[0]->object->getUrl('thumb');
                    $obj_id = $product->arobject[0]->id;
                    }
                    }
                    @endphp
                    <img class="img-fluid" src="{{$img}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="width: 150px; max-height:150px;"></a>

                @if(!empty($obj_id))
                <div class="position-absolute" style="top:0;left:0;">
                    <div class="badge badge-glow badge-info">
                        <img class="img-fluid" src="{{asset('XR/assets/images/ar.png')}}" style="width: 32px; max-height:32px;">
                        <h6 class="text-white m-0">Tryon</h6>
                    </div>
                </div>
                @endif
            </div>
            <div class="card-body">
                <div class="item-wrapper">
                    <div class="item-rating">
                        @if(!empty($obj_id))
                        <!-- <a href="#" data-id="{{$product->id}}"
                                class="badge badge-pill badge-glow badge-success">Try On</a> -->
                        @endif
                        @if(count($product->categories) > 0)
                        <div class="badge badge-pill badge-glow badge-primary mr-1">{{$product->categories[0]['name']}}
                        </div>
                        @endif
                    </div>
                    <div>
                        <h6 class="item-price">
                            {!! $currency !!}<span class="update" data-pk="{{$product->id}}" data-url="{{route('admin.product_list.update',$product->id)}}" data-type="text" data-title="Enter price" data-name="price">{{$product->price ?? '00.00'}}</span>
                        </h6>
                    </div>
                </div>
                <div class="item-name pb-1">
                    <a href="#" type="button" class="update" data-pk="{{$product->id}}" data-url="{{route('admin.product_list.update',$product->id)}}" data-type="text" data-title="Enter name" data-name="name">{{$product->name ?? 'Untitled'}}</a>
                    <!-- <p class="item-company">By <span class="company-name">Amazon</span></p> -->
                </div>
                <div>
                    <!-- <p class="item-description update" data-pk="{{$product->id}}"
                                        data-url="{{route('admin.product_list.update',$product->id)}}" data-type="textarea"
                                        data-title="Enter description" data-name="description">
                                        No Description
                                    </p> -->
                </div>
            </div>
            <div class="item-options text-center">
                <div class="item-wrapper">
                    <div class="item-rating">
                        <!-- <div class="badge badge-primary badge-md">
                                                <span>{{count($product->categories) > 0 ?$product->categories[0]['name']:'' }}</span> <i class="feather icon-star"></i>
                                            </div> -->
                        @if(!empty($obj_id))
                        <a href="#" data-id="{{$product->id}}" class="badge badge-pill badge-glow badge-success">Try On</a>
                        @endif
                        @if(count($product->categories) > 0)
                        <div class="badge badge-pill badge-glow badge-primary mr-1">{{$product->categories[0]['name']}}
                        </div>
                        @endif
                    </div>
                    <div class="item-cost">
                        <h6 class="item-price">
                            {!! $currency !!}{{$product->price ?? '00.00'}}
                        </h6>
                    </div>
                </div>
                <div class="wishlist bg-white d-none">
                    <!-- <i class="fa fa-heart-o mr-25"></i> Wishlist -->
                </div>
                <span class="button-checkbox cart p-0">
                    <button type="button" class="btn btn-block text-white btn-primary" data-catagory_id="{{ (count($product->categories)>0)?$product->categories[0]->id:''}}" data-color="primary">Select</button>
                    <input type="checkbox" value="{{$product->id}}" class="hidden" />
                </span>
                <!-- <div class="cart">
                                        <i class="feather icon-shopping-cart mr-25"></i> <span class="add-to-cart">Add to cart</span> <a href="app-ecommerce-checkout.html" class="view-in-cart d-none">View In Cart</a>
                                    </div> -->
                <!-- <div class="item-wrapper">
                                            <div class="item-rating">
                                            @if(count($product->categories) > 0)
                                                <div class="badge badge-pill badge-glow badge-primary mr-1">{{$product->categories[0]['name']}}</div>
                                                @endif
                                            </div>
                                            <div class="item-cost">
                                                <h6 class="item-price">
                                                ₹ {{$product->price ?? '__.__'}}
                                                </h6>
                                            </div>
                                        </div>
                                        <span class="button-checkbox cart p-0">
                                            <button type="button" class="btn btn-block text-white btn-primary"
                                                data-color="primary">Select</button>
                                            <input type="checkbox" name="product_id[]" value="{{$product->id}}" class="hidden" />
                                        </span> -->
                <!-- <div class="cart"> 
                                        <i class="feather icon-shopping-cart"></i> <span class="add-to-cart">Add to cart</span>
                                       
                                    </div> -->
            </div>
        </div>
    </div>
</div>
@endforeach
@if(count($data) == 0)
<h3 class="text-light text-center p-2 w-100">No Products added yet, please add</h3>
@endif

<script>
    $('.update').editable({
        mode: "inline"
    });

    $('#productSelectModalTotal').html('{{$data->total()}}');
    $('.button-checkbox').each(function() {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'fa fa-check'
                },
                off: {
                    icon: 'fa fa-unchecked'
                }
            };
        var product_val = $widget.children('input').val();
        var catagory_id = $button.data("catagory_id");

        // Event Handlers
        $button.on('click', function() {

            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            var isChecked = $checkbox.is(':checked');
            var selectedProducts = new Array();
            var productData = new Array();
            if (localStorage.getItem('selectedProducts') !== null && localStorage.getItem('selectedProducts') !== "")
                selectedProducts = JSON.parse(localStorage.getItem('selectedProducts'));

            if (isChecked) {

                // console.log(localStorage.getItem( 'selectedProducts' ));
                productData.push(product_val);
                productData.push(catagory_id);
                selectedProducts.push(productData);
                localStorage.setItem("selectedProducts", JSON.stringify(selectedProducts));
                var date = new Date();
                date.setTime(date.getTime() + (15 * 60 * 1000));

                localStorage.setItem("expiretime", date);
                if (localStorage.getItem('expiretime') < Date()) {
                    localStorage.setItem("selectedProducts", "");
                }
            } else {
                selectedProducts = jQuery.grep(selectedProducts, function(value) {
                    return value != product_val;
                });
                localStorage.setItem("selectedProducts", JSON.stringify(selectedProducts));
            }

            updateDisplay();
        });
        $checkbox.on('change', function() {
            // updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            var selectedProducts = new Array();
            if (localStorage.getItem('selectedProducts') !== null && localStorage.getItem('selectedProducts') !== "")
                selectedProducts = JSON.parse(localStorage.getItem('selectedProducts'));
            if (selectedProducts.indexOf(product_val) >= 0) {
                isChecked = true;
                $checkbox.prop('checked', isChecked);
                // $checkbox.triggerHandler('change');

            }

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);


            // Update the button's color
            if (isChecked) {
                $button
                    // .removeClass('btn-default')
                    .addClass('btn-' + color + ' active')
                    .html('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> Selected');
                // .prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            } else {
                $button
                    // .removeClass('btn-' + color + ' active')
                    .addClass('btn-default')
                    .html('<i class="feather icon-shopping-cart"></i> Select');
                // .prepend('');

            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
</script>
{!! $data->render() !!}