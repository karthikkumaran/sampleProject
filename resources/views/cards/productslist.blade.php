<style>
    #overlay {
        border-radius: 0.5rem;
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: rgba(0,0,0,0.5);
        z-index: 2;
    }
    #text{
        position: absolute;
        top: 50%;
        left: 50%;
        font-size: 30px;
        color: white;
        transform: translate(-50%,-50%);
        -ms-transform: translate(-50%,-50%);
    }
</style>
@php
    if(empty($user_meta_data['settings']['currency'])){
        $currency = "₹";
    }
    else{
        if($user_meta_data['settings']['currency_selection'] == "currency_symbol")
            $currency = App\User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
        else
            $currency = $user_meta_data['settings']['currency'];
    }
@endphp
@foreach ($data as $key => $product)
    @php
        $img = $objimg = asset('XR/assets/images/placeholder.png');
        $obj_id = "";
        $obj_3d = false;
        if(count($product->photo) > 0){
            $img = $product->photo[0]->getUrl('thumb');
            $objimg = asset('XR/assets/images/placeholder.png');
            $obj_id = "";
            $obj_3d = false;
        } else if(count($product->arobject) > 0){
            if(!empty($product->arobject[0]->object))
                if(!empty($product->arobject[0]->object->getUrl('thumb'))) {
                    $img = $product->arobject[0]->object->getUrl('thumb');
                }
            } else {                        
                $img = asset('XR/assets/images/placeholder.png');
                $objimg = asset('XR/assets/images/placeholder.png');
            }
                    
            foreach ($product->arobject as $key => $arobject) {                        
                if(!empty($arobject->object)){
                    if($arobject->ar_type == "face") {
                        if(!empty($arobject->object)){
                            if(!empty($arobject->object->getUrl('thumb'))) {
                                $objimg = $arobject->object->getUrl('thumb');
                                $obj_id = $arobject->id;
                            }
                        }                        
                    }                               
                }    
                //echo $arobject->ar_type; 
                if($arobject->ar_type == "surface") {                                
                    if(!empty($arobject->object_3d)) {
                        $obj_3d = true;
                    }        
                    if(!empty($arobject->object_3d_ios)) {
                        $obj_3d = true;
                    }                
                }                        
            }
    @endphp
    <div class="card d-flex justify-content-center" id="product_list_{{$product->id}}">
        @if(($product->downgradeable == 1) and ($product_enable == "false"))
            <div id="overlay">
                <div id="text">Product Disabled</div>
            </div>
        @endif
        <div class="row m-0 w-100 rounded my-1 bg-white" style="min-height:13.0rem;">
            <div class="col-lg-3 col-md-4 d-flex justify-content-center rounded">
                @if($product->new != 0)
                    <span><img src="{{asset('XR/app-assets/images/icons/new.png')}}" style="position:absolute;z-index:10;width:70px;height:70px;top:0.01px;left:0.01px;"/></span>
                @endif
                @if($product->featured != 0)
                    <span><img src="{{asset('XR/app-assets/images/icons/featured1.png')}}" style="position:absolute;z-index: 10;width:70px;height:70px;right:0.01px;top:0.010px;"/></span>
                @endif
                @if(count($product->photo) > 1)
                    <div  id="carousel-thumb_{{$product->id}}" class="carousel slide carousel-fade carousel-thumbnails h-100 w-100" data-interval="false" data-ride="carousel">
                        <div class="carousel-inner d-flex" role="listbox">
                            @foreach($product->photo as $photo)
                                <div class="carousel-item {{$loop->index == 0?'active':''}}  h-100 w-100 d-flex justify-content-center">
                                    <img class="img-fluid rounded" style="height:13.0rem" src="{{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl('thumb'):$objimg ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                </div>
                            @endforeach
                        </div>
                        <div class="d-inline-flex niceScroll mx-auto justify-content-start h-25 w-100" style="overflow-x:scroll;overflow-y:hidden !important;">
                            <ul class="carousel-indicators position-relative">
                                @foreach($product->photo as $photo)
                                    <li data-target="#carousel-thumb_{{$product->id}}" data-slide-to="{{$loop->index}}" class="mr-1 bg-none {{$loop->index == 0?'active':''}}" style="width:40px;height:30px;border-radius:50px;">
                                        <img class="d-block rounded" style="width:40px;height:30px;border-radius:25px;" src="{{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl('thumb'):$objimg ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @else
                    <img class="img-fluid mw-100 rounded h-100" src="{{$img ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="min-height:13rem;">
                @endif
                @if(!empty($obj_id))
                    <div class="position-absolute" style="top:0;left:0;">
                        <div class="badge badge-glow badge-info">
                            <img class="img-fluid" src="{{asset('XR/assets/images/ar.png')}}" style="width: 32px; max-height:32px;">
                            <h6 class="text-white m-0">Tryon</h6>
                        </div>
                    </div>
                @endif
                @if($obj_3d == true)
                    <div class="position-absolute" style="top: 0;right: 0;">                                    
                        <div class="badge badge-glow badge-info">
                            <img class="img-fluid" src="{{asset('XR/assets/images/ar.png')}}" style="width: 32px; max-height:32px;">
                            <h6 class="text-white m-0">3D/AR View</h6>
                        </div>
                    </div>
                @endif
                <a href="#!">
                    <div class="mask rgba-white-slight"></div>
                </a>
            </div>
            <div class="col-lg-9 col-md-8">
                <div class="w-100 p-1 h-75">
                    <div class="position-absolute" style="right:30px;">
                        <a href="#" onclick="deleteProduct($(this))" data-id="{{$product->id}}" class="badge badge-pill badge-glow badge-danger">
                            <i class="feather icon-trash-2 m-0"></i>
                        </a>
                    </div>
                    <div class="item-name">
                        <input type="hidden" class="tryonobj" value="{{$objimg ?? ''}}"/>
                        <input type="hidden" class="obj_id" value="{{$obj_id ?? ''}}"/>
                        <a href="#" type="button" class="update" data-pk="{{$product->id}}" data-url="{{route('admin.product_list.update',$product->id)}}" data-type="text" data-title="Enter name" data-name="name">{{$product->name ?? 'Untitled'}}</a>
                        <p class="item-company">
                            @if(count($product->categories) > 0)
                                <div class="badge badge-pill badge-glow badge-primary">{{$product->categories[0]['name']}}</div>
                            @endif
                        </p>
                        <p class="item-company">
                            @if(!empty($product->downgradeable) || $product->downgradeable == "0")
                                @if ($product->downgradeable == 1)
                                    <div class="badge badge-pill badge-glow badge-danger mr-1 text-truncate" style="max-width:45%;">Inactive</div>
                                @endif
                            @endif
                        </p>
                    </div>
                    <div class="item-cost mb-1 mt-1">
                        <h6 class="item-price">
                            <a href="#" type="button" class="update" data-pk="{{$product->id}}" data-url="{{route('admin.product_list.update',$product->id)}}" data-type="text" data-title="Enter the sku" data-name="sku">{{$product->sku ?? 'No SKU'}}</a>
                        </h6>
                    </div>
                    <div class="item-cost mb-1 mt-1">
                        <h6 class="item-price">
                            {!! $currency !!}<span class="update" data-pk="{{$product->id}}" data-url="{{route('admin.product_list.update',$product->id)}}" data-type="number" data-title="Enter price" data-name="price">{{$product->price ?? '00.00'}}</span>
                        </h6>
                    </div>
                    <!-- <div>
                        <a href="#" type="button" class="update" data-pk="{{$product->id}}" data-url="{{route('admin.product_list.update',$product->id)}}" data-type="text" data-title="Enter the sku" data-name="sku">{{$product->sku ?? 'No SKU'}}</a>
                    </div> -->
                    <p class="item-company">
                        @if(count($product->catalogs) > 0)
                            @foreach ($product->catalogs as $catalog)
                                @if(!empty($catalog) && !empty($catalog->campagin))
                                    <div class="badge badge-pill badge-glow badge-dark mr-1" onclick="location.href='{{route('admin.campaigns.edit',$catalog->campagin->id)}}'" style="cursor: pointer;">{{$catalog->campagin->name}}</div>
                                @endif
                            @endforeach
                        @endif
                    </p>
                </div>
                <div class="w-100 col-lg-12 d-flex justify-content-lg-end justify-content-md-end justify-content-sm-center">
                        <button type="button" class="btn btn-primary col-lg-4 col-md-5 col-sm-12" onclick="getProductDetail({{$product->id}})" data-toggle="modal" data-target="#productEditorModal" style="background-color: #5E50EE !important;">
                            <i class="fa fa-pencil" aria-hidden="true"></i> Edit
                        </button>
                </div>
            </div>
        </div>
    </div>
@endforeach

@if(count($data) == 0)
    @if($is_search == true || $is_filter == true)
        <h3 class="text-light text-center p-2 w-100">No products found matching the search or filter criteria.</h3>
    @elseif($is_search == false && $is_search == false)
        <h3 class="text-light text-center p-2">No Products added yet, please add</h3>
    @endif
@endif
{!! $data->render() !!}
<script>
    $('#all_products_count').empty().html({{$data->total()}});
    $('.update').editable({
        mode: "inline"
    });
    
    $(".niceScroll").niceScroll({
    cursorcolor: "grey", // change cursor color in hex
    cursoropacitymin: 0, // change opacity when cursor is inactive (scrollabar "hidden" state), range from 1 to 0
    cursoropacitymax: 1, // change opacity when cursor is active (scrollabar "visible" state), range from 1 to 0
    cursorwidth: "5px", // cursor width in pixel (you can also write "5px")
    cursorborder: "1px solid #fff", // css definition for cursor border
    cursorborderradius: "5px", // border radius in pixel for cursor
    zindex: "auto", // change z-index for scrollbar div
    scrollspeed: 60, // scrolling speed
    mousescrollstep: 40, // scrolling speed with mouse wheel (pixel)
    touchbehavior: false, // enable cursor-drag scrolling like touch devices in desktop computer
    hwacceleration: true, // use hardware accelerated scroll when supported
    boxzoom: false, // enable zoom for box content
    dblclickzoom: true, // (only when boxzoom=true) zoom activated when double click on box
    gesturezoom: true, // (only when boxzoom=true and with touch devices) zoom activated when pinch out/in on box
    grabcursorenabled: true, // (only when touchbehavior=true) display "grab" icon
    autohidemode: true, // how hide the scrollbar works, possible values: 
    background: "", // change css for rail background
    iframeautoresize: true, // autoresize iframe on load event
    cursorminheight: 32, // set the minimum cursor height (pixel)
    preservenativescrolling: true, // you can scroll native scrollable areas with mouse, bubbling mouse wheel event
    railoffset: false, // you can add offset top/left for rail position
    bouncescroll: false, // (only hw accell) enable scroll bouncing at the end of content as mobile-like 
    spacebarenabled: true, // enable page down scrolling when space bar has pressed
    disableoutline: true, // for chrome browser, disable outline (orange highlight) when selecting a div with nicescroll
    horizrailenabled: true, // nicescroll can manage horizontal scroll
    railalign: "right", // alignment of vertical rail
    railvalign: "bottom", // alignment of horizontal rail
    enabletranslate3d: true, // nicescroll can use css translate to scroll content
    enablemousewheel: true, // nicescroll can manage mouse wheel events
    enablekeyboard: true, // nicescroll can manage keyboard events
    smoothscroll: true, // scroll with ease movement
    sensitiverail: true, // click on rail make a scroll
    enablemouselockapi: true, // can use mouse caption lock API (same issue on object dragging)
    cursorfixedheight: false, // set fixed height for cursor in pixel
    hidecursordelay: 400, // set the delay in microseconds to fading out scrollbars
    irectionlockdeadzone: 6, // dead zone in pixels for direction lock activation
    nativeparentscrolling: true, // detect bottom of content and let parent to scroll, as native scroll does
    enablescrollonselection: true, // enable auto-scrolling of content when selection text
    cursordragspeed: 0.3, // speed of selection when dragged with cursor
    rtlmode: "auto", // horizontal div scrolling starts at left side
    cursordragontouch: false, // drag cursor in touch / touchbehavior mode also
    oneaxismousemode: "auto", 
    scriptpath: "", // define custom path for boxmode icons ("" => same script path)
    preventmultitouchscrolling: true,// prevent scrolling on multitouch events
    disablemutationobserver: false,
  });
</script>
