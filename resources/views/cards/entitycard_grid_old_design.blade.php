@php
if(empty($user_meta_data['settings']['currency']))
        $currency = "₹";
    else
        $currency = App\User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
@endphp
@foreach ($data as $key => $card)
                @php
                $product = $card->product;
                $img = $objimg = asset('XR/assets/images/placeholder.png');
                $obj_id = "";
                if(count($product->photo) > 0){
                    $img = $product->photo[0]->getUrl('thumb');
                    $objimg = asset('XR/assets/images/placeholder.png');
                    $obj_id = "";
                    } else if(count($product->arobject) > 0){
                        if(!empty($product->arobject[0]->object))
                        if(!empty($product->arobject[0]->object->getUrl('thumb'))) {
                            $img = $product->arobject[0]->object->getUrl('thumb');
                        }
                    }
                    if(count($product->arobject) > 0){
                        if(!empty($product->arobject[0]->object))
                        if(!empty($product->arobject[0]->object->getUrl('thumb'))) {
                            $objimg = $product->arobject[0]->object->getUrl('thumb');
                            $obj_id = $product->arobject[0]->id;
                        }
                    }
                @endphp
                <div class="col-lg-3 col-md-6 col-sm-12" id="entitycard_{{$card->id}}">
                    <div class="card ecommerce-card">
                        <div class="card-content">
                            <div class="item-img text-center" style="min-height: 12.85rem;">
                                <a href="#">
                                    <img class="img-fluid" src="{{$img}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';"
                                        style="width: 150px; max-height:150px;"></a>
                                        @if(!empty($obj_id))
                                <div class="position-absolute" style="top:0;left:0;">
                                    <div class="badge badge-glow badge-info">
                                        <img class="img-fluid" src="{{asset('XR/assets/images/ar.png')}}"
                                                            style="width: 32px; max-height:32px;">
                                        <h6 class="text-white m-0">Tryon</h6>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="card-body">
                                <div class="item-wrapper">
                                    <div class="item-rating">
                                    @if(!empty($obj_id))
                                        <!-- <a href="#" data-id="{{$product->id}}"
                                            class="badge badge-pill badge-glow badge-success">Try On</a> -->
                                    @endif
                                        @if(count($product->categories) > 0)
                                        <div class="badge badge-pill badge-glow badge-primary mr-1">
                                            {{$product->categories[0]['name']}}
                                            </div>
                                        @endif
                                    </div>
                                    <div>
                                    @if($campaign->is_start == 1)
                                        <button type="button" class="btn btn-primary btn-icon sharebtn" data-link="{{$campaign->public_link}}" data-qrcode="{{route('admin.campaigns.previewQrCode',$campaign->public_link)}}" data-title="{{$product->name}}" data-sharelink="{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'product',$product->id)}}" data-pdf="false">
                                            <i class="feather icon-share-2"></i>
                                        </button>
                                    @endif
                                        <h6 class="item-price">
                                            {!!$currency!!}{{$product->price ?? '00.00'}}
                                        </h6>
                                    </div>
                                </div>
                                <div class="item-name">
                                    <a href="#">{{$product->name ?? 'Untitled'}}</a>
                                </div>
                                <div>
                                    <p class="item-description">
                                    </p>
                                </div>
                            </div>
                            <div class="item-options text-center">
                                <div class="item-wrapper">
                                    <div class="item-rating">
                                    @if(!empty($obj_id))
                                        <a href="#" data-id="{{$product->id}}"
                                            class="badge badge-pill badge-glow badge-success">Try On</a>
                                    @endif
                                        @if(count($product->categories) > 0)
                                        <div class="badge badge-pill badge-glow badge-primary mr-1">
                                            {{$product->categories[0]['name']}}</div>
                                        @endif
                                    </div>
                                    <div class="item-cost">
                                        <h6 class="item-price">
                                            {!! $currency !!} {{$product->price ?? '00.00'}}
                                        </h6>
                                    </div>
                                </div>

                                <div class="cart bg-danger p-0">
                                    <a href="#" onclick="deleteCard({{$card->id}})"
                                        class="btn btn-block text-white bg-danger" data-color="primary">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
@endforeach
@if(count($data) == 0)
    <h3 class="text-light text-center p-2 w-100">No Products added yet, please add</h3>
@endif
{!! $data->render() !!}
