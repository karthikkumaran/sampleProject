<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <div class="card-body">
        <form method="POST" action="#" name="update_product_variant_form" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <input type="hidden" id="id" name="id" value="{{$product->id}}"/>
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="card collapse-icon">
                            <div class="card-body">
                                <div class="collapse-default">
                                    <div class="card p-0" style="border: #B8C2CC;border-style: solid;">
                                        <div id="headingCollapse1" class="card-header p-1" data-toggle="collapse" role="button" data-target="#collapse1" aria-expanded="false" aria-controls="collapse1">
                                            <span class="lead collapse-title"> Product Images </span>
                                        </div>
                                        <div id="collapse1" role="tabpanel" aria-labelledby="headingCollapse1" class="collapse">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <div class="item-img text-center d-flex mw-100 justify-content-center" id="prod_slide" style="min-height: 12.85rem;">
                                                      @if(isset($product->photo))
                                                        @if(count($product->photo) > 0)
                                                            <div id="carousel-thumb_{{$product->id}}" class="mw-100 carousel slide carousel-fade carousel-thumbnails carousel-thumb" data-interval="false" data-ride="carousel">
                                                                <div class="carousel-inner" role="listbox">
                                                                    @foreach($product->photo as $photo)
                                                                        <div class="carousel-item {{$loop->index == 0?'active':''}}" style="max-height:350px;">
                                                                            <img class="img-fluid rounded w-sm-100 mw-lg-50" id="image_{{$photo->id}}" style=" max-height:350px;" src="{{$photo->getUrl() ?? asset('assets/images/placholder.png')}}">
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                                <div class="w-100 mt-1 justify-content-lg-center d-inline-flex niceScroll" style="height:120px;overflow-x:auto;max-width:75vw;z-index:1000">
                                                                    @foreach($product->photo as $photo)
                                                                        <div data-target="#carousel-thumb_{{$product->id}}" id="product_{{$photo->id}}" data-slide-to="{{$loop->index}}" class="{{$loop->index == 0?'active':''}} mr-1" style="height:120px;width:90px;">
                                                                            <img class="d-flex rounded cursor-pointer" style="height:60px;width:90px;" src="{{$photo->getUrl() ?? asset('assets/images/placholder.png')}}">
                                                                            <button href="#{{$photo->id}}" id="delete_img" onclick="varremoveimg({{$photo->id}})" class="btn btn-danger btn-sm text-white mt-2"><i class="feather icon-trash-2 m-0"></i></button>
                                                                        </div>
                                                                    @endforeach
                                                                    <a data-toggle="modal" data-target="#varproductUploadimgModal" style="cursor:pointer;">
                                                                        <div style="min-height:120px;max-height:120px;min-width: 80px;max-width:80px;text-align: center;border-style:dashed;" class="rounded ml-1">
                                                                            <div class="image mt-2">
                                                                                <i class="feather icon-plus-circle font-large-1 " ></i>
                                                                            </div>
                                                                            <span style="display: block;padding: 4px;">Upload Image</span>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                                <!-- <div class="w-100 bg-success" style="max-width:75vw;">
                                                                    <ol class=" carousel-indicators overflow-hidden position-relative m-0 bg-warning w-100" style="z-index:1;">
                                                                        @foreach($product->photo as $photo)
                                                                            <li data-target="#carousel-thumb_{{$product->id}}" id="product_{{$photo->id}}" data-slide-to="{{$loop->index}}" class="{{$loop->index == 0?'active':''}} ml-1" style="min-width:90px;min-height:120px;">
                                                                                <div style="height:90px;object-fit: contain;">
                                                                                    <img class="d-block img-fluid rounded cursor-pointer w-100" style="height:60px;" src="{{$photo->getUrl() ?? asset('assets/images/placholder.png')}}">
                                                                                </div>
                                                                                <button href="#{{$photo->id}}" id="delete_img" onclick="removeimg({{$photo->id}})" class="btn btn-danger btn-sm text-white"><i class="feather icon-trash-2 m-0"></i></button>
                                                                            </li>
                                                                        @endforeach
                                                                        <a data-toggle="modal" data-target="#varproductUploadimgModal" style="cursor:pointer;">
                                                                        <div style="min-height:120px;max-height:120px;min-width: 80px;max-width:80px;text-align: center;border-style:dashed;" class="rounded my-1 ml-1">
                                                                            <div class="image mt-2">
                                                                                <i class="feather icon-plus-circle font-large-1 " ></i>
                                                                            </div>
                                                                            <span style="display: block;padding: 4px;">Upload Image</span>
                                                                        </div>
                                                                        </a>
                                                                    </ol>
                                                                </div> -->

                                                            </div>
                                                        @else
                                                            <div style="height:110px;max-width: 400px;border-style:dashed;position:relative;margin-left:30%;margin-right:30%;" class="rounded mb-0 text-center">
                                                                <div class="image mt-2">
                                                                    <i data-toggle="modal" data-target="#varproductUploadimgModal" class="feather icon-plus-circle font-large-1 "></i>
                                                                </div>
                                                                <span style="display: block;padding: 4px;">Upload Image</span>
                                                            </div>
                                                            <h1 class="p-4 text-light">No Product Images Uploaded</h1>
                                                        @endif
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
          <!--details-->

                                    <div class="card p-0" style="border: #B8C2CC;border-style: solid;">
                                        <div id="headingCollapse2" class="card-header collapse-header p-1" data-toggle="collapse" role="button" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
                                            <span class="lead collapse-title">Product Details</span>
                                        </div>
                                        <div id="collapse2" role="tabpanel" aria-labelledby="headingCollapse2" class="collapse" aria-expanded="false">
                                            <div class="card-body">
                                                <div class="col-12">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-12">
                                                            <div class="form-group">
                                                                <label for="name">{{ trans('cruds.product.fields.name') }}</label>
                                                                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $product->name) }}">
                                                                @if($errors->has('name'))
                                                                    <div class="invalid-feedback">
                                                                        {{ $errors->first('name') }}
                                                                    </div>
                                                                @endif
                                                                <span class="help-block">{{ trans('cruds.product.fields.name_helper') }}</span>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6 col-md-12">
                                                            <div class="form-group">
                                                                <label for="sku">{{ trans('cruds.product.fields.sku') }}</label>
                                                                <input class="form-control {{ $errors->has('sku') ? 'is-invalid' : '' }}" type="text" name="sku" id="sku" value="{{ old('sku', $product->sku) }}">
                                                                @if($errors->has('sku'))
                                                                    <div class="invalid-feedback">
                                                                        {{ $errors->first('sku') }}
                                                                    </div>
                                                                @endif
                                                                <span class="help-block">{{ trans('cruds.product.fields.sku_helper') }}</span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-12">
                                                            <div class="form-group">
                                                                <label for="price">{{ trans('cruds.product.fields.price') }}</label>
                                                                <input class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}" type="number" name="price" id="price" value="{{ old('price', $product->price) }}" step="0.01">
                                                                @if($errors->has('price'))
                                                                    <div class="invalid-feedback">
                                                                        {{ $errors->first('price') }}
                                                                    </div>
                                                                @endif
                                                                <span class="help-block">{{ trans('cruds.product.fields.price_helper') }}</span>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6 col-md-12">
                                                            <div class="form-group px-1">
                                                                <input class="form-check-input " type="checkbox" name="vardiscount_rate" id="vardiscount_rate"  value="1" {{ old('discount', $product->discount) ? 'checked="checked"' : '' }} />
                                                                <label class="form-check-label" id="ofr_discvar">Offer Discount</label>
                                                                <label class="form-check-label" style="display: none;" id="vardisc_per">{{ trans('cruds.product.fields.discount') }}(%)</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <input class="form-control {{ $errors->has('discount') ? 'is-invalid' : '' }}" style="display: none" type="number" name="vardiscount" id="vardiscount" value="{{ old('discount', $product->discount) }}" onkeypress="return isNumberKey(event,this)" placeholder="Enter discount percentage within 100">
                                                                <span id="errMsg"></span>
                                                                @if($errors->has('discount'))
                                                                    <div class="invalid-feedback">
                                                                        {{ $errors->first('discount') }}
                                                                    </div>
                                                                @endif
                                                                <span class="help-block">{{ trans('cruds.product.fields.discount_helper') }}</span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                     {{-- attribute div --}}
                                                     <div class="row">
                                                        @if(session('subscribed_features'))
                                                            @if(array_key_exists("inventory", session('subscribed_features')[0]))
                                                                <div class="col-lg-3 col-md-12">
                                                                    <div class="form-group pt-2 px-1">
                                                                        <input class="form-check-input" type="checkbox" id="stock_qty" name="stock_qty" value="1" {{ old('stock', $product->stock) ? 'checked="checked"' : '' }} />
                                                                        <label class="form-check-label" id="manage_inv">Manage Inventory</label>
                                                                        <label class="form-check-label" id="stock_qt" style="display: none;">Stock Quantity</label>
                                                                        <input class="form-control" style="display: none" type="number" name="stock" id="stock" value="{{ old('stock', $product->stock) }}">
                                                                        @if($errors->has('stock'))
                                                                            <div class="invalid-feedback">
                                                                                {{ $errors->first('stock') }}
                                                                            </div>
                                                                        @endif
                                                                        <span class="help-block">{{ trans('cruds.product.fields.stock_helper') }}</span>
                                                                    </div>
                                                                </div>

                                                                <div class="col-lg-3 col-md-12">
                                                                    <div class="px-1 pt-2  form-group">
                                                                        <input class="form-check-input" type="checkbox" id="out_of_stock" name="out_of_stock" value="1" {{ old('out_of_stock',$product->out_of_stock) ? 'checked="checked"' : '' }} />
                                                                        <label class="form-check-label" id="out_stock">{{ trans('cruds.product.fields.out_of_stock') }}</label>
                                                                        @if($errors->has('out_of_stock'))
                                                                            <div class="invalid-feedback">
                                                                                {{ $errors->first('out_of_stock') }}
                                                                            </div>
                                                                        @endif
                                                                        <span class="help-block">{{ trans('cruds.product.fields.out_of_stock_helper') }}</span>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @endif
                                                    </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="vardescription">{{ trans('cruds.product.fields.description') }}</label>
                                                        <textarea class="form-control pb-1 {{ $errors->has('description') ? 'is-invalid' : '' }} hidden" rows="5" name="vardescription" id="vardescription">{{ old('description', $product->description) }}</textarea>
                                                        <div id="vareditor-container" style="height: 250px;"></div>
                                                        @if($errors->has('description'))
                                                            <div class="invalid-feedback">
                                                                {{ $errors->first('description') }}
                                                            </div>
                                                        @endif
                                                        <span class="help-block">{{ trans('cruds.product.fields.description_helper') }}</span>
                                                    </div>
                                                </div>
                                                @if(!empty($product->downgradeable) || $product->downgradeable == 0)
                                                <div class="col-lg-6 col-md-12">
                                                    <div class="form-group">
                                                        <label class="form-check-label" for="downgradeable">Status</label>
                                                        <select class="custom-select form-control" name="downgradeable" id="downgradeable" required>
                                                            <option value="0" {{ old('downgradeable',$product->downgradeable == 0) ? 'selected' : '' }}>Active</option>
                                                            <option value="1" {{ old('downgradeable',$product->downgradeable == 1) ? 'selected' : '' }}>Inactive</option>
                                                        </select>
                                                        @if($errors->has('downgradeable'))
                                                            <div class="invalid-feedback">
                                                                {{ $errors->first('downgradeable') }}
                                                            </div>
                                                        @endif
                                                        <span class="help-block">{{ trans('cruds.product.fields.downgradeable_helper') }}</span>
                                                    </div>
                                                </div>
                                            @else
                                            @endif

                                                @if(count($attributes)>=1)
                                                    {{-- {!!dd($attributes)!!} --}}
                                                        <div class="col-lg-6 col-md-12">
                                                            <div class="px-1 form-group">
                                                                <input class="form-check-input" type="checkbox" id="attribute" name="attribute" value="1" {{ old('attribute', count($values)>0?true:false) ? 'checked' : '' }} />
                                                                <label class="form-check-label text-capitalize" for="attribute">Add Attributes</label>
                                                            </div>
                                                            {{-- attribute-list --}}
                                                            <div id="attribute_list">
                                                                @foreach ($attributes as $groupname=>$attributes)
                                                                    <label class=" bg-light w-100 mb-1 text-capitalize font-weight-bold" style="padding:5px;">{{$groupname}}</label>
                                                                    <div>
                                                                        @foreach($attributes as $attribute)
                                                                            @if ($attribute->type=="textbox")
                                                                                <div class="form-group ml-1">
                                                                                    <label class="text-capitalize font-weight-bold" for="{{$attribute->label}}">{{ $attribute->label }}</label>
                                                                                    <input class="form-control {{ $errors->has('$attribute->label') ? 'is-invalid' : '' }}" type="text" name="{{$attribute->slug}}" id="{{$attribute->id."_".$attribute->label}}" value="{{ old('$attribute->label', count($values)>0 &&  isset($values[$attribute->attribute_id]) ? $values[$attribute->attribute_id]: '') }}">
                                                                                </div>
                                                                            @elseif($attribute->type=="checkbox")
                                                                                <div class="form-group ml-1">
                                                                                    <label class="text-capitalize font-weight-bold" for="{{$attribute->label}}">{{ $attribute->label }}</label>
                                                                                    @foreach(explode(",",$attribute->values) as $value)
                                                                                        <div class="form-check form-check-inline">
                                                                                            <input class="form-check-input {{ $errors->has('$attribute->label') ? 'is-invalid' : '' }} " type="checkbox" id="{{$attribute->id."_".$value}}" name="{{$attribute->slug}}[]" value="{{$value}}" {{count($values)>0&&isset($values[$attribute->attribute_id]) && in_array($value ,explode(",",$values[$attribute->attribute_id]))? 'checked' : ''}}>
                                                                                            <label class="form-check-label" for="{{$attribute->id."_".$value}}">{{$value}}</label>
                                                                                        </div>
                                                                                    @endforeach
                                                                                </div>
                                                                            @elseif($attribute->type=="radio_button")
                                                                                <div class="form-group ml-1">
                                                                                    <label class="text-capitalize attr_radiogroup font-weight-bold {{$attribute->slug}}" data-id={{$attribute->id}} for="{{$attribute->label}}">{{ $attribute->label }}</label>
                                                                                    @foreach(explode(",",$attribute->values) as $value)
                                                                                        <div class="form-check form-check-inline">
                                                                                            <input class="form-check-input {{ $errors->has('$attribute->label') ? 'is-invalid' : '' }} attr_radiobtn{{$attribute->id}}   " type="radio" id="{{$attribute->id."_".$value}}" name="{{$attribute->slug}}" value="{{$value}}" {{-- onclick="check(this)"   --}} {{count($values)>0&&isset($values[$attribute->attribute_id]) && $values[$attribute->attribute_id]==$value ? 'checked' : ''}}>
                                                                                            <label class="form-check-label" for="{{$attribute->id."_".$value}}">{{$value}}</label>
                                                                                        </div>
                                                                                    @endforeach
                                                                                </div>
                                                                            @elseif($attribute->type=="dropdown")
                                                                                <div class="form-group ml-1">
                                                                                    <label class="text-capitalize font-weight-bold" for="{{$attribute->label}}">{{$attribute->label}}</label>
                                                                                    <select class="form-control custom-select {{ $errors->has('$attribute->label') ? 'is-invalid' : '' }} " name="{{$attribute->slug}}" id="{{$attribute->id}}">
                                                                                        <option value="">Choose..</option>
                                                                                        @foreach (explode(",",$attribute->values) as $value)
                                                                                            <option value="{{ $value }}" {{count($values)>0&&isset($values[$attribute->attribute_id])&& $values[$attribute->attribute_id]==$value ? 'selected' : ''}}>
                                                                                                {{ $value }}
                                                                                            </option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            @endif
                                                                        @endforeach
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                @endif
                                        </div>
                                    </div>
</div>

                </div>


                <div class="card p-0" style="border: #B8C2CC;border-style: solid;">
                                        <div id="headingCollapse3" class="card-header collapse-header p-1" data-toggle="collapse" role="button" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                            <span class="lead collapse-title"> Try On images </span>
                                        </div>
                                        @php
                                            $ar_necklace = null;
                                            $ar_ear_left = null;
                                            $ar_ear_right = null;
                                            $ar_3d_file = null;
                                            $ar_3d_ios_file = null;
                                            foreach($product->arobject as $arobject){
                                                if($arobject->ar_type == "face"){
                                                    if($arobject->position == "jwl-nck"){
                                                       $ar_necklace = $arobject;
                                                    }
                                                    else if($arobject->position == "jwl-ear-l"){
                                                        $ar_ear_left = $arobject;
                                                    }
                                                    else if($arobject->position == "jwl-ear-r"){
                                                        $ar_ear_right = $arobject;
                                                    }
                                                }
                                                else{
                                                    if(!empty($arobject->object_3d))
                                                    {
                                                        $ar_3d_file = $arobject;
                                                    }
                                                    if(!empty($arobject->object_3d_ios))
                                                    {
                                                        $ar_3d_ios_file = $arobject;
                                                    }
                                                }
                                            }
                                            //dd($ar_3d_file,$ar_necklace,$ar_ear_left,$ar_ear_right);
                                        @endphp
                                        <div id="collapse3" role="tabpanel" aria-labelledby="headingCollapse3" class="collapse" aria-expanded="false">
                                            <div class="card-body">
                                            @if(session('subscribed_features'))
                                                @if(array_key_exists("ar_object", session('subscribed_features')[0]))
                                                <div class="row">
                                                    <div class="col-lg-4 col-md-12 text-center mb-2">
                                                        @if(!empty($ar_necklace))
                                                            <div style="height:90px;object-fit: contain;text-align: -webkit-center;">
                                                                <h6>Necklace</h6>
                                                                <img class="d-block img-fluid" style="min-width:90px; min-height:70px;max-height:80px;" src="{{$ar_necklace->object->getUrl() ?? asset('assets/images/placholder.png')}}">
                                                            </div>
                                                            <button type="button" href="#" id="delete_img" onclick="removevartryonimg({{$ar_necklace->id}})" class="btn btn-danger btn-sm text-white"><i class="feather icon-trash-2 m-0"></i></button>
                                                        @else
                                                            <div style="height:120px;max-width: 400px;position:relative;margin-left:25%;margin-right:25%;display: none;" class="rounded mb-0 text-center" id="added_img_jwl-nck">
                                                                <h5>Necklace try on image uploaded</h5>
                                                            </div>
                                                            <div style="cursor: pointer;height:120px;max-width: 400px;border-style:dashed;position:relative;margin-left:25%;margin-right:25%;" class="rounded mb-0 text-center" id="add_img_jwl-nck" data-position="jwl-nck" onclick="open_tryon_var_image_upload($(this))">
                                                                <div class="image mt-2">
                                                                    <i class="feather icon-plus-circle font-large-1"></i>
                                                                </div>
                                                                <span style="display: block;padding: 4px;">Upload Tryon necklace Image</span>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 text-center mb-2">
                                                        @if(!empty($ar_ear_left))
                                                            <div style="height:90px;object-fit: contain;text-align: -webkit-center;">
                                                                <h6>Earring Left</h6>
                                                                <img class="d-block img-fluid" style="min-width:90px; min-height:70px;max-height:80px;" src="{{$ar_ear_left->object->getUrl() ?? asset('assets/images/placholder.png')}}">
                                                            </div>
                                                            <button type="button" href="#" id="delete_img" onclick="removevartryonimg({{$ar_ear_left->id}})" class="btn btn-danger btn-sm text-white"><i class="feather icon-trash-2 m-0"></i></button>
                                                        @else
                                                            <div style="height:120px;max-width: 400px;position:relative;margin-left:25%;margin-right:25%;display: none;" class="rounded mb-0 text-center" id="added_img_jwl-ear-l">
                                                                <h5>Earring Left try on image uploaded</h5>
                                                            </div>
                                                            <div style="cursor: pointer;height:120px;max-width: 400px;border-style:dashed;position:relative;margin-left:25%;margin-right:25%;" class="rounded mb-0 text-center" id="add_img_jwl-ear-l" data-position="jwl-ear-l" onclick="open_tryon_var_image_upload($(this))">
                                                                <div class="image mt-2">
                                                                    <i class="feather icon-plus-circle font-large-1"></i>
                                                                </div>
                                                                <span style="display: block;padding: 4px;">Upload Tryon earring left Image</span>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 text-center mb-2">
                                                        @if(!empty($ar_ear_right))
                                                            <h6>Earring Right</h6>
                                                            <div style="height:90px;object-fit: contain;text-align: -webkit-center;">
                                                                <img class="d-block img-fluid" style="min-width:90px; min-height:70px;max-height:80px;" src="{{$ar_ear_right->object->getUrl() ?? asset('assets/images/placholder.png')}}">
                                                            </div>
                                                            <button type="button" href="#" id="delete_img" onclick="removevartryonimg({{$ar_ear_right->id}})" class="btn btn-danger btn-sm text-white"><i class="feather icon-trash-2 m-0"></i></button>
                                                        @else
                                                            <div style="height:120px;max-width: 400px;position:relative;margin-left:25%;margin-right:25%;display: none;" class="rounded mb-0 text-center" id="added_img_jwl-ear-r">
                                                                <h5>Earring Right try on image uploaded</h5>
                                                            </div>
                                                            <div style="cursor: pointer;height:120px;max-width: 400px;border-style:dashed;position:relative;margin-left:25%;margin-right:25%;" class="rounded mb-0 text-center" id="add_img_jwl-ear-r" data-position="jwl-ear-r" onclick="open_tryon_var_image_upload($(this))">
                                                                <div class="image mt-2">
                                                                    <i class="feather icon-plus-circle font-large-1"></i>
                                                                </div>
                                                                <span style="display: block;padding: 4px;">Upload Tryon earring right Image</span>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                @else
                                                <div class="row">
                                                    <div class="col-12 text-center">
                                                        <div class="alert alert-danger" role="alert">
                                                            You do not have this feature. Please upgrade to a plan that has AR Object feature.
                                                            <a href="{{route('admin.subscriptions.index')}}" class="btn btn-info btn-xs">Upgrade plan</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                            @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card p-0" style="border: #B8C2CC;border-style: solid;">
                                        <div id="headingCollapse4" class="card-header p-1" data-toggle="collapse" role="button" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                            <span class="lead collapse-title"> 3D File </span>
                                        </div>
                                        <div id="collapse4" role="tabpanel" aria-labelledby="headingCollapse4" class="collapse" aria-expanded="false">
                                            <div class="card-body">
                                            @if(session('subscribed_features'))
                                                @if(array_key_exists("3d_object", session('subscribed_features')[0]))
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-12 text-center mb-2">
                                                        @if(!empty($ar_3d_file))
                                                            <div style="height:90px;object-fit: contain;text-align: -webkit-center;">
                                                                <p class="text-center">3D File Uploaded</p>
                                                                <button type="button" href="#" id="delete_img" onclick="remove3DVarFile({{$ar_3d_file->id}})" class="btn btn-danger btn-sm text-white"><i class="feather icon-trash-2 m-0"></i></button>
                                                            </div>
                                                        @else
                                                            <div class="rounded mb-0 text-center" id="file_3d_uploaded_android" style="height:110px;max-width:400px;position:relative;margin-left:30%;margin-right:30%;display: none;">
                                                                <h5>3D file uploaded</h5>
                                                            </div>
                                                            <div class="rounded mb-0 text-center" id="file_3d_upload_android" data-3d_type="android" onclick="open_3d_file_var_upload($(this))" style="cursor: pointer;height:110px;max-width: 400px;border-style:dashed;position:relative;margin-left:30%;margin-right:30%;">
                                                                <div class="image mt-2">
                                                                    <i  class="feather icon-plus-circle font-large-1"></i>
                                                                </div>
                                                                <span style="display: block;padding: 4px;">Upload 3D file</span>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 text-center mb-2">
                                                        @if(!empty($ar_3d_ios_file))
                                                            <div style="height:90px;object-fit: contain;text-align: -webkit-center;">
                                                                <p class="text-center">3D iOS File Uploaded</p>
                                                                <button type="button" href="#" id="delete_img" onclick="remove3DVarFile({{$ar_3d_ios_file->id}})" class="btn btn-danger btn-sm text-white"><i class="feather icon-trash-2 m-0"></i></button>
                                                            </div>
                                                        @else
                                                            <div class="rounded mb-0 text-center" id="file_3d_uploaded_iOS" style="height:110px;max-width:400px;position:relative;margin-left:30%;margin-right:30%;display: none;">
                                                                <h5>iOS 3D file uploaded</h5>
                                                            </div>
                                                            <div class="rounded mb-0 text-center" id="file_3d_upload_iOS" data-3d_type="iOS" onclick="open_3d_file_var_upload($(this))" style="cursor: pointer;height:110px;max-width: 400px;border-style:dashed;position:relative;margin-left:30%;margin-right:30%;">
                                                                <div class="image mt-2">
                                                                    <i  class="feather icon-plus-circle font-large-1"></i>
                                                                </div>
                                                                <span style="display: block;padding: 4px;">Upload 3D iOS file</span>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                @else
                                                <div class="row">
                                                    <div class="col-12 text-center">
                                                        <div class="alert alert-danger" role="alert">
                                                            You do not have this feature. Please upgrade to a plan that has 3D Object feature.
                                                            <a href="{{route('admin.subscriptions.index')}}" class="btn btn-info btn-xs">Upgrade plan</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                            @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card p-0" style="border: #B8C2CC;border-style: solid;">
                                        <div id="headingCollapse5" class="card-header p-1" data-toggle="collapse" role="button" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                            <span class="lead collapse-title"> Documents </span>
                                        </div>
                                        <div id="collapse5" role="tabpanel" aria-labelledby="headingCollapse5" class="collapse" aria-expanded="false">
                                            <div class="card-body">
                                                @if(count($product->docs) > 0)
                                                    <div style="height:90px;object-fit: contain;text-align: -webkit-center;">
                                                        <p class="text-center">{{count($product->docs)}} Documents Uploaded</p>
                                                    </div>
                                                @else
                                                <div class="rounded mb-0 text-center" id="varproduct_docs_uploaded" style="height:110px;max-width: 400px;position:relative;margin-left:30%;margin-right:30%;display: none;">
                                                    <h5>Product documents uploaded</h5>
                                                </div>
                                                    <div class="rounded mb-0 text-center" id="varproduct_docs_upload" data-toggle="modal" data-target="#varproductDocumentsUploadModal" style="cursor: pointer;height:110px;max-width: 400px;border-style:dashed;position:relative;margin-left:30%;margin-right:30%;">
                                                        <div class="image mt-2">
                                                            <i  class="feather icon-plus-circle font-large-1"></i>
                                                        </div>
                                                        <span style="display: block;padding: 4px;">Upload Documents</span>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                    </div>

                <div class="form-group">
                    <button class="btn btn-danger float-right" type="button" onclick="update_productvariant($(this))" data-modal_name="productVariantEditorModal">
                        {{ trans('global.save') }}
                    </button>
                </div>

            <!-- Product upload Modal -->
            <div class="modal fade ecommerce-application" id="varproductUploadimgModal" tabindex="-1" role="dialog" aria-labelledby="productSelectModalTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="productSelectModalTitle">Product Upload Image</h5>
                            <button type="button" class="close" onclick="close_product_image_upload()" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="dropzone dropzone-area {{ $errors->has('photo') ? 'is-invalid' : '' }}" id="productUploadimg3">
                                <div class="dz-message">Upload Product Image</div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" onclick="close_product_image_upload()" aria-label="Close">Cancel</button>
                            <button type="button" id="varproductImgSubmit" class="btn btn-primary" onclick="update_productvariant($(this))" disabled="true" data-modal_name="varproductUploadimgModal">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Product Upload Modal -->

            <!-- AR upload Modal -->
            <div class="modal fade ecommerce-application" id="arVarUploadimgModal" tabindex="-1" role="dialog" aria-labelledby="arVarUploadimgModalTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="arVarUploadimgModalTitle">Tryon Upload Images</h5>
                            <button type="button" class="close" onclick="close_tryon_var_image_upload()" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="required" for="arVarUploadimg">Tryon Image</label>
                                <div class="dropzone dropzone-area {{ $errors->has('arVarUploadimg') ? 'is-invalid' : '' }}" id="arVarUploadimg" style="min-height: 305px;">
                                    <div class="dz-message">Upload Tryon Image</div>
                                </div>
                                @if($errors->has('arVarUploadimg'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('arVarUploadimg') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" onclick="close_tryon_var_image_upload()" aria-label="Close">Cancel</button>
                            <button type="button" id="arVarImgSubmit" onclick="update_productvariant($(this))" class="btn btn-primary" disabled="true" data-modal_name="arVarUploadimgModal">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End AR Upload Modal -->

            <!-- 3D Image upload Modal -->
            <div class="modal fade ecommerce-application" id="file3dUploadVarModal" tabindex="-1" role="dialog" aria-labelledby="file3dUploadVarModalTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="file3dUploadVarModalTitle">3D Upload File</h5>
                            <button type="button" class="close" onclick="close_3d_file_var_upload()" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="dropzone dropzone-area {{ $errors->has('file3dUploadVar') ? 'is-invalid' : '' }}" id="file3dUploadVar" style="min-height: 305px;display: none;">
                                    <div class="dz-message">Upload 3D file</div>
                                </div>
                                <div class="dropzone dropzone-area {{ $errors->has('file3diOSVarUpload') ? 'is-invalid' : '' }}" id="file3diOSVarUpload" style="min-height: 305px;display: none;">
                                    <div class="dz-message">Upload 3D iOS file</div>
                                </div>
                                @if($errors->has('file3diOSVarUpload'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('file3diOSVarUpload') }}
                                    </div>
                                @endif
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" onclick="close_3d_file_var_upload()" aria-label="Close">Cancel</button>
                            <button type="button" id="file3dVarSubmit" onclick="update_productvariant($(this))" class="btn btn-primary" disabled="true" data-modal_name="file3dUploadVarModal">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End 3D Image Upload Modal -->

            <!-- Documents upload Modal -->
            <div class="modal fade ecommerce-application" id="varproductDocumentsUploadModal" tabindex="-1" role="dialog" aria-labelledby="varproductDocumentsUploadModalTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="varproductDocumentsUploadModalTitle">Upload Documents</h5>
                            <button type="button" class="close" onclick="close_varproduct_documents_upload()" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="dropzone dropzone-area {{ $errors->has('productDocumentsUpload') ? 'is-invalid' : '' }}" id="productDocumentsUpload1" style="min-height: 305px;">
                                    <div class="dz-message">Upload Documents</div>
                                </div>
                                @if($errors->has('productDocumentsUpload'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('productDocumentsUpload') }}
                                    </div>
                                @endif
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" onclick="close_varproduct_documents_upload()" aria-label="Close">Cancel</button>
                            <button type="button" id="varproductDocsSubmit" onclick="update_productvariant($(this))" class="btn btn-primary" disabled="true" data-modal_name="varproductDocumentsUploadModal">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Documents Upload Modal -->
        </form>
    </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>

<script>
        $('.arobjectPosition').editable({
            source: [
                @foreach(App\Arobject::POSITION_SELECT as $key => $label)
                {
                    value: '{{$key}}',
                    text: '{{$label}}'
                },
                @endforeach
            ]
        });

        var ar_position = "";
        var file_3d_type = "";

        function removevartryonimg(id){
            if(confirm('{{ trans('global.areYouSure') }}')) {
                var url = "{{route('admin.arobjects.destroy',':id')}}";
                url = url.replace(':id', id);
                $.ajax({
                    url: url,
                    type: "post",
                    data: {'_method': 'DELETE'},
                }).done(function (data) {
                    alert('Removed try on image');
                    $('#productVariantEditorModal').modal('hide');
                }).fail(function (jqXHR, ajaxOptions, thrownError) {
                    // alert('No response from server');
                });
            }
        }

        function remove3DVarFile(id){
            if(confirm('{{ trans('global.areYouSure') }}')) {
                var url = "{{route('admin.arobjects.destroy',':id')}}";
                url = url.replace(':id', id);
                $.ajax({
                    url: url,
                    type: "post",
                    data: {'_method': 'DELETE'},
                }).done(function (data) {
                    alert('Removed the 3D File');
                    $('#productVariantEditorModal').modal('hide');
                }).fail(function (jqXHR, ajaxOptions, thrownError) {
                    // alert('No response from server');
                });
            }
        }

        function close_product_image_upload() {
            $('#varproductUploadimgModal').modal('hide');
        }

        function close_tryon_var_image_upload() {
            ar_position = "";
            $('#arVarUploadimgModal').modal('hide');
        }

        function open_3d_file_var_upload($this){
            file_3d_type = $this.data('3d_type');
            if(file_3d_type == "android"){
                $("#file3diOSVarUpload").hide();
                $("#file3dUploadVar").show();
            }
            else{
                $("#file3dUploadVar").hide();
                $("#file3diOSVarUpload").show();
            }
            $('#file3dUploadVarModal').modal('show');
        }

        function close_3d_file_var_upload() {
            $('#file3dUploadVarModal').modal('hide');
        }

        function close_varproduct_documents_upload() {
            $('#varproductDocumentsUploadModal').modal('hide');
        }

        $('#varproductUploadimgModal').on('hidden.bs.modal', function() {
            // do something…
            $('#productVariantEditorModal').modal('show');
        })

        function open_tryon_var_image_upload($this) {
            ar_position = $this.data('position');
            $('#arVarUploadimgModal').modal('show');
        }

        function close_category_modal(){
            $('#categoryAddModal').modal('hide');
        }

        function update_productvariant($this){
            console.log('enter');
            $("#vardescription").val(quill.root.innerHTML);
            $('#loading-bg').show();
            var modal_name = $this.data('modal_name');
            $('#'+ modal_name).modal('hide');
            var update_url = "{{ route('admin.productVariants.update', ':product_id') }}";
            var product_id ="{{$product->id}}";
            console.log(product_id);
            update_url = update_url.replace(':product_id', product_id);
            $.ajax({
                url: update_url,
                type: "PUT",
                data: $('form[name=update_product_variant_form]').serializeArray(),
            }).done(function (data) {
                if(modal_name == "arVarUploadimgModal"){
                    console.log('tryon');
                    arImageDropzone.removeAllFiles();
                    $("#add_img_" + ar_position).hide();
                    $("#added_img_" + ar_position).show();
                    $('form[name=update_product_variant_form]').find('input[name="try_on_object"]').remove();
                    $('form[name=update_product_variant_form]').find('input[name="position"]').remove();
                    $('form[name=update_product_variant_form]').find('input[name="try_on_type"]').remove();
                }
                else if(modal_name == "file3dUploadVarModal"){
                    file3dDropzone.removeAllFiles();
                    file3DiOSDropzone.removeAllFiles();
                    $("#file_3d_upload_" + file_3d_type).hide();
                    $("#file_3d_uploaded_" + file_3d_type).show();
                    $('form[name=update_product_variant_form]').find('input[name="object_3d_ios"]').remove();
                    $('form[name=update_product_variant_form]').find('input[name="object_3d"]').remove();
                    $('form[name=update_product_variant_form]').find('input[name="type_3d"]').remove();
                }
                else if(modal_name == "varproductDocumentsUploadModal"){
                    productDocumentsDropzone.removeAllFiles();
                    $("#varproduct_docs_upload").hide();
                    $("#varproduct_docs_uploaded").show();
                    $('form[name=update_product_variant_form]').find('input[name="docs[]"]').remove();
                }
                else{
                    // console.log(data);
                    // var img_html ='<div data-target="#carousel-thumb_5" id="product_74" data-slide-to="1" class=" mr-1" style="height:120px;width:90px;"><img class="d-flex rounded cursor-pointer" style="height:60px;width:90px;" src="/storage/74/60cc6492299db_cou1.jpg"><button href="#74" id="delete_img" onclick="removeimg(74)" class="btn btn-danger btn-sm text-white mt-2"><i class="feather icon-trash-2 m-0"></i></button></div>';
                    // $('#productVariantEditorModal').modal('show');
                    // $('.niceScroll').append(img_html);
                    var producturl = "{{ route('admin.products.product_list')}}/#updat-"+ product_id;
                    window.location.href = producturl;
                    location.reload();


                }
                $('#loading-bg').hide();
                $(".modal-backdrop").hide();
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                $('#loading-bg').hide();
                console.log('No response from server');
            });
        }

        function create_category(){
            $('#loading-bg').show();
            $('#categoryAddModal').modal('hide');
            $('#productVariantEditorModal').modal('hide');
            $.ajax({
                url: "{{ route('admin.product-categories.store') }}",
                type: "POST",
                data: $('form[name=add_category_form]').serializeArray(),
            }).done(function (data) {
                $('#loading-bg').hide();
                $(".modal-backdrop").hide();
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                $('#loading-bg').hide();
                console.log('No response from server');
            });
        }


        if ($("#attribute").is(':checked')) {
            $("#attribute_list").show();
        } else {
            $("#attribute_list").hide();
        }



        $("#attribute").on('change', function() {
            if ($("#attribute").is(':checked')) {
                $("#attribute_list").show();
            } else {
                $("#attribute_list").hide();
            }
        });

        $('.attr_radiogroup').on('click', function() {

            id = $(this).data("id")
            $('.attr_radiobtn' + id).prop('checked', false);
        });


        if ($("#vardiscount_rate").is(':checked')) {
            $("#vardiscount").show();
            $('#vardisc_per').show();
            $('#ofr_discvar').hide();
        } else {
            $("#vardiscount").hide();
            $('#ofr_discvar').show();
            $('#vardisc_per').hide();
        }

        $("#vardiscount_rate").change(function() {
            if ($("#vardiscount_rate").is(':checked')) {
                $("#vardiscount").show();
                $('#vardisc_per').show();
                $('#ofr_discvar').hide();
            } else {
                $("#vardiscount").hide();
                $('#ofr_discvar').show();
                $('#vardisc_per').hide();
            }

        });
        if ($("#varstock_qty").is(":checked")) {
            $("#varstock").show();
            $("#varstock_qt").show();
            $("#varmanage_inv").hide();
        } else {
            $("#varstock").hide();
            $("#varstock_qt").hide();
            $("#varmanage_inv").show();
        }

        $("#varstock_qty").on('change', function() {
            if ($(this).is(":checked")) {
                $("#varstock").show();
                $("#varstock_qt").show();
                $("#varmanage_inv").hide();
            } else {
                $("#varstock").hide();
                $("#varstock_qt").hide();
                $("#varmanage_inv").show();
            }
        });

        $("#vardiscount").on('input', function() {
            var num = $(this).val();
            if ($(this).val() > 100) {
                alert("No numbers above 100");
                $(this).val('100');
            }
            // var format = /^[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*$/;

        });

        // $("#discount").ForceNumericOnly().show();
        // $('#discount').keypress(function(event) {
        //     var num = $(this).val();
        //     var format =/^[0-9]/;
        //         if(format.test(num)){
        //         // alert("only number is allowed");
        //         // $(this).val('');
        //         }
        //         else{
        //             alert('only number can filled');
        //         }
        // });
        function isNumberKey(evt, element) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57) && !(charCode == 46 || charCode == 8))
                return false;
            else {
                var len = $(element).val().length;
                var index = $(element).val().indexOf('.');
                if (index > 0 && charCode == 46) {
                    return false;
                }
                if (index > 0) {
                    var CharAfterdot = (len + 1) - index;
                    if (CharAfterdot > 3) {
                        return false;
                    }
                }
            }
            return true;
        }
        var quill = new Quill('#vareditor-container', {
            modules: {
                'toolbar': [
                    [{
                        'font': []
                    }, {
                        'size': []
                    }]
                    , ['bold', 'italic', 'underline', 'strike']
                    , [{
                        'color': []
                    }, {
                        'background': []
                    }]
                    , [{
                        'script': 'super'
                    }, {
                        'script': 'sub'
                    }]
                    , [{
                        'header': '1'
                    }, {
                        'header': '2'
                    }, 'blockquote', 'code-block']
                    , [{
                        'list': 'ordered'
                    }, {
                        'list': 'bullet'
                    }, {
                        'indent': '-1'
                    }, {
                        'indent': '+1'
                    }]
                    , ['direction', {
                        'align': []
                    }]
                    , ['link', 'image', 'video', 'formula']
                    , ['clean']
                ]
            , }
            , placeholder: 'description...'
            , theme: 'snow'
        });
        quill.root.innerHTML = $("#vardescription").val();

        var uploadedPhotoMap = {}
        Dropzone.autoDiscover = false;
        productImageDropzone = new Dropzone('#productUploadimg3', {
            url: '{{ route('admin.productVariants.storeMedia')}}',
            maxFilesize : 40,
            acceptedFiles: '.jpeg,.jpg,.png,.gif',
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            params: {
                size: 40
            },
            success: function(file, response) {
                $('form').append('<input type="hidden" name="photo[]" value="' + response.name + '">')
            },
            removedfile: function (file) {
                file.previewElement.remove()
                var name = ''
                if (typeof file.file_name !== 'undefined') {
                    name = file.file_name
                } else {
                    name = uploadedPhotoMap[file.name]
                }
                $('form').find('input[name="photo[]"][value="' + name + '"]').remove()
            },
            init: function () {
                var submitButton = document.querySelector("#varproductImgSubmit");
                myProductImageDropzone = this; // closure
                submitButton.addEventListener("click", function() {
                    if (myProductImageDropzone.getUploadingFiles().length === 0 && myProductImageDropzone.getQueuedFiles().length === 0) {} else {
                        myProductImageDropzone.processQueue();
                    }
                });
                this.on("dragend, processingmultiple", function(file) {
                    $("#varproductImgSubmit").prop('disabled', true);
                });
                this.on("queuecomplete", function(file) {
                    if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                        $("#varproductImgSubmit").prop('disabled', false);
                    } else {
                        $("#varproductImgSubmit").prop('disabled', true);
                    }
                })
            },
            error: function (file, response) {
                if ($.type(response) === 'string') {
                    var message = response //dropzone sends it's own error messages in string
                } else {
                    var message = response.errors.file
                }
                file.previewElement.classList.add('dz-error')
                _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
                _results = []
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    node = _ref[_i]
                    _results.push(node.textContent = message)
                }
                return _results
            }
        });

        arImageDropzone = new Dropzone('#arVarUploadimg', {
            url: '{{ route('admin.arobjects.storeMedia') }}',
            maxFilesize: 40, // MB
            maxFiles: 1,
            acceptedFiles: '.png',
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            params: {
                size: 40
            },
            success: function (file, response) {
                $('form').find('input[name="try_on_object"]').remove()
                $('form').append('<input type="hidden" name="try_on_object" value="' + response.name + '">')
                $('form').append('<input type="hidden" name="position" value="' + ar_position + '">')
                $('form').append('<input type="hidden" name="try_on_type" value="face">')
            },
            removedfile: function (file) {
                file.previewElement.remove()
                if (file.status !== 'error') {
                    $('form').find('input[name="try_on_object"]').remove()
                    this.options.maxFiles = this.options.maxFiles + 1
                }
            },
            init: function () {
                var submitButton = document.querySelector("#arVarImgSubmit");
                myARimageDropzone = this; // closure
                submitButton.addEventListener("click", function() {
                    if (myARimageDropzone.getUploadingFiles().length === 0 && myARimageDropzone.getQueuedFiles().length === 0) {} else {
                        myARimageDropzone.processQueue();
                    }
                });
                this.on("dragend, processingmultiple", function(file) {
                    $("#arVarImgSubmit").prop('disabled', true);
                });
                this.on("queuecomplete", function(file) {
                    if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                        $("#arVarImgSubmit").prop('disabled', false);
                    } else {
                        $("#arVarImgSubmit").prop('disabled', true);
                    }
                })
            },
            error: function (file, response) {
                if ($.type(response) === 'string') {
                    var message = response //dropzone sends it's own error messages in string
                } else {
                    var message = response.errors.file
                }
                file.previewElement.classList.add('dz-error')
                _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
                _results = []
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    node = _ref[_i]
                    _results.push(node.textContent = message)
                }

                 return _results
            }
        });

        file3dDropzone = new Dropzone('#file3dUploadVar', {
            url: '{{ route('admin.arobjects.storeMedia') }}',
            maxFilesize: 150, // MB
            maxFiles: 1,
            // acceptedFiles: '.glb,.fbx,.zip.vrx,.gltf',
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (file, response) {
                $('form').find('input[name="object_3d"]').remove()
                $('form').append('<input type="hidden" name="object_3d" value="' + response.name + '">')
                $('form').append('<input type="hidden" name="type_3d" value="surface">')
            },
            removedfile: function (file) {
                file.previewElement.remove()
                if (file.status !== 'error') {
                    $('form').find('input[name="object_3d"]').remove()
                    this.options.maxFiles = this.options.maxFiles + 1
                }
            },
            init: function () {
                var submitButton = document.querySelector("#file3dVarSubmit");
                myFile3dDropzone = this; // closure
                submitButton.addEventListener("click", function() {
                    if (myFile3dDropzone.getUploadingFiles().length === 0 && myFile3dDropzone.getQueuedFiles().length === 0) {} else {
                        myFile3dDropzone.processQueue();
                    }
                });
                this.on("dragend, processingmultiple", function(file) {
                    $("#file3dVarSubmit").prop('disabled', true);
                });
                this.on("queuecomplete", function(file) {
                    if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                        $("#file3dVarSubmit").prop('disabled', false);
                    } else {
                        $("#file3dVarSubmit").prop('disabled', true);
                    }
                })
            },
            error: function (file, response) {
                if ($.type(response) === 'string') {
                    var message = response //dropzone sends it's own error messages in string
                } else {
                    var message = response.errors.file
                }
                file.previewElement.classList.add('dz-error')
                _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
                _results = []
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    node = _ref[_i]
                    _results.push(node.textContent = message)
                }

                 return _results
            }
        });

        file3DiOSDropzone = new Dropzone('#file3diOSVarUpload', {
            url: '{{ route('admin.arobjects.storeMedia') }}',
            maxFilesize: 150, // MB
            maxFiles: 1,
            acceptedFiles: '.usdz',
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            params: {
                size: 150
            },
            success: function (file, response) {
                $('form').find('input[name="object_3d_ios"]').remove()
                $('form').append('<input type="hidden" name="object_3d_ios" value="' + response.name + '">')
                $('form').append('<input type="hidden" name="type_3d" value="surface">')
            },
            removedfile: function (file) {
                file.previewElement.remove()
                if (file.status !== 'error') {
                    $('form').find('input[name="object_3d_ios"]').remove()
                    this.options.maxFiles = this.options.maxFiles + 1
                }
            },
            init: function () {
                var submitButton = document.querySelector("#file3dVarSubmit");
                myFile3DiOSDropzone = this; // closure
                submitButton.addEventListener("click", function() {
                    if (myFile3DiOSDropzone.getUploadingFiles().length === 0 && myFile3DiOSDropzone.getQueuedFiles().length === 0) {} else {
                        myFile3DiOSDropzone.processQueue();
                    }
                });
                this.on("dragend, processingmultiple", function(file) {
                    $("#file3dVarSubmit").prop('disabled', true);
                });
                this.on("queuecomplete", function(file) {
                    if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                        $("#file3dVarSubmit").prop('disabled', false);
                    } else {
                        $("#file3dVarSubmit").prop('disabled', true);
                    }
                })
            },
            error: function (file, response) {
                if ($.type(response) === 'string') {
                    var message = response //dropzone sends it's own error messages in string
                } else {
                    var message = response.errors.file
                }
                file.previewElement.classList.add('dz-error')
                _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
                _results = []
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    node = _ref[_i]
                    _results.push(node.textContent = message)
                }

                 return _results
            }
        });

        productDocumentsDropzone = new Dropzone('#productDocumentsUpload1', {
            url: '{{ route('admin.arobjects.storeMedia') }}',
            maxFilesize: 15, // MB
            maxFiles: 5,
            acceptedFiles: '.pdf',
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            params: {
                size: 40
            },
            success: function (file, response) {
                $('form').append('<input type="hidden" name="docs[]" value="' + response.name + '">')
            },
            removedfile: function (file) {
                file.previewElement.remove()
                var name = ''
                if (typeof file.file_name !== 'undefined') {
                    name = file.file_name
                } else {
                    name = uploadedPhotoMap[file.name]
                }
                $('form').find('input[name="docs[]"][value="' + name + '"]').remove()
            },
            init: function () {
                var submitButton = document.querySelector("#varproductDocsSubmit");
                myProductDocumentsDropzone = this; // closure
                submitButton.addEventListener("click", function() {
                    if (myProductDocumentsDropzone.getUploadingFiles().length === 0 && myProductDocumentsDropzone.getQueuedFiles().length === 0) {} else {
                        myProductDocumentsDropzone.processQueue();
                    }
                });
                this.on("dragend, processingmultiple", function(file) {
                    $("#varproductDocsSubmit").prop('disabled', true);
                });
                this.on("queuecomplete", function(file) {
                    if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                        $("#varproductDocsSubmit").prop('disabled', false);
                    } else {
                        $("#varproductDocsSubmit").prop('disabled', true);
                    }
                })
            },
            error: function (file, response) {
                if ($.type(response) === 'string') {
                    var message = response //dropzone sends it's own error messages in string
                } else {
                    var message = response.errors.file
                }
                file.previewElement.classList.add('dz-error')
                _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
                _results = []
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    node = _ref[_i]
                    _results.push(node.textContent = message)
                }

                 return _results
            }
        });

        categoryDropzone = new Dropzone('#photo-dropzone', {
            url: '{{ route('admin.product-categories.storeMedia') }}',
            // maxFilesize: 2, // MB
            acceptedFiles: '.jpeg,.jpg,.png,.gif',
            maxFiles: 1,
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            params: {
                size: 2,
                width: 4086,
                height: 4096
            },
            success: function (file, response) {
                $('form').find('input[name="photo"]').remove()
                $('form').append('<input type="hidden" name="photo" value="' + response.name + '">')
            },
            removedfile: function (file) {
                file.previewElement.remove()
                if (file.status !== 'error') {
                    $('form').find('input[name="photo"]').remove()
                    this.options.maxFiles = this.options.maxFiles + 1
                }
            },
            init: function () {
                @if(isset($productCategory) && $productCategory->photo)
                    var file = {!! json_encode($productCategory->photo) !!}
                    this.options.addedfile.call(this, file)
                    this.options.thumbnail.call(this, file, '{{ $productCategory->photo->getUrl('thumb') }}')
                    file.previewElement.classList.add('dz-complete')
                    $('form').append('<input type="hidden" name="photo" value="' + file.file_name + '">')
                    this.options.maxFiles = this.options.maxFiles - 1
                @endif
            },
            error: function (file, response) {
                if ($.type(response) === 'string') {
                    var message = response //dropzone sends it's own error messages in string
                } else {
                    var message = response.errors.file
                }
                file.previewElement.classList.add('dz-error')
                _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
                _results = []
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    node = _ref[_i]
                    _results.push(node.textContent = message)
                }

                return _results
            }
        });

        $('.carousel-thumb').carousel({
            interval: 2000
        });
        //$("button").click(() => $(".carousel").carousel("next"));

        $(function () {
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons);
            $.extend(true, $.fn.dataTable.defaults, {
                pageLength: 10,
            });

            $('.datatable-product-variants-collection:not(.ajaxTable)').DataTable({ buttons: dtButtons });
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
            });
        });
</script>
