@php
if(empty($user_meta_data['settings']['currency']))
$currency = "₹";
else
if($user_meta_data['settings']['currency_selection'] == "currency_symbol")
$currency = App\User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
else
$currency = $user_meta_data['settings']['currency'];
$counter=0;
@endphp
@foreach ($customers as $key => $customer)
@php
$customer_meta_data= json_decode($customer->meta_data, true);
// dd($products);

@endphp

@if($status=='*')
@else
    @if($customer->order->status==$status)
        @php
            $counter=$counter+1;
        @endphp
    @else
        @continue
    @endif
@endif


<div class="card ecommerce-card" id="order_list_{{$customer->order_id}}">
    <div class="card-body">
        <div class="row">
            <div class="col col-md-4 order-1 cursor-pointer" onclick="location.href='{{route('admin.orders.show',$customer->order_id)}}';">

                <h4><b>Order #{{$customer->order->order_id ?? $customer->order_id}}</b></h4>


                <h5 class="mb-1 mt-1"><b>{{$customer->fname}} {{$customer->lname ?? ""}}</b></h5>


                <h5>{{$customer->mobileno ?? ""}}</h5>

                @php
                
                $class="text-primary";
                if(App\orders::STATUS[$customer->order->status ?? $customer->status] == 'Order Recieved'):
                $class="text-primary";
                elseif (App\orders::STATUS[$customer->order->status ?? $customer->status ] == 'Accepted'):
                $class="text-success";
                elseif (App\orders::STATUS[$customer->order->status ?? $customer->status] == 'Rejected'):
                $class="text-danger";
                elseif (App\orders::STATUS[$customer->order->status ?? $customer->status] == 'Cancelled'):
                $class="text-secondary"; 
                elseif (App\orders::STATUS[$customer->order->status ?? $customer->status ] == 'Delivered'):
                $class="text-success";
                elseif (App\orders::STATUS[$customer->order->status ?? $customer->status ] == 'Pending'):
                $class="text-secondary";
                elseif (App\orders::STATUS[$customer->order->status ?? $customer->status] == 'Shipped'):
                $class="text-success";
                elseif (App\orders::STATUS[$customer->order->status ?? $customer->status ] == 'Failed'):
                $class="text-danger";
                endif;
                @endphp

                <h5>Status: <span class='{{$class}}'>{{App\orders::STATUS[$customer->order->status ?? $customer->status ]}}</span></h5>

            </div>
            <div class="col col-md-4 order-2 d-flex flex-md-row flex-column justify-content-md-center">

                <a href="https://wa.me/{{$customer->country_code ? $customer->country_code.$customer->mobileno : '91'.$customer->mobileno}}" class="mr-1 mb-1" target="_blank">
                    <i style="color:#25D366" class="fa fa-whatsapp fa-2x"></i></a>
                <a href="tel:{{$customer->mobileno}}" class="mr-1 mb-1">
                    <i style="color:#2F91F3" class="fa fa-phone fa-2x"></i></a>
                <a href="{{ route('admin.export_order_pdf', $customer->order_id) }}" class="mr-1 mb-1">
                    <i style="color:red" class="fa fa-file-pdf-o fa-2x"></i></a>

            </div>
            <div class="col col-md-4 order-3 text-md-right cursor-pointer" onclick="location.href='{{route('admin.orders.show',$customer->order_id)}}';">

                <h5>{{ date('d-m-Y', strtotime($customer->created_at)) }}</h5>
                <h5>{{ date('h:i:s A', strtotime($customer->created_at)) }}</h5>

                <h5><b>No. of Products: {{count($customer->orderProducts)}}</b></h5>

                <h4><b>{!! $customer_meta_data['product_total_price']==0?" ":$currency.''.$customer_meta_data['product_total_price'] !!}</b></h4>

            </div>
        </div>
    </div>

</div>

@endforeach
@if(count($customers) == 0 || ($status!='*' && $counter == 0))
<h3 class="text-light text-center p-2 w-100">No Orders exist</h3>
@else
{!! $customers->render() !!}
@endif
