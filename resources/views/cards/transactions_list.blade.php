@php
if(empty($user_meta_data['settings']['currency']))
$currency = "₹";
else
if($user_meta_data['settings']['currency_selection'] == "currency_symbol")
$currency = App\User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
else
$currency = $user_meta_data['settings']['currency'];
@endphp

@foreach ($transactions as $key => $transaction)
<div class="card ecommerce-card" id="transactions_list_{{$transaction->id}}">
    <div class="card-body">
        <div class="row">
            <div class="col col-md-4 order-1 cursor-pointer" onclick="location.href='{{route('admin.transactions.show',$transaction->id)}}'">

                <h4><b>#{{$transaction->transaction_id}}</b></h4>


                <h5 class="mb-1 mt-1"><b>{{$transaction->payment_mode}}</b></h5>


                <h5>{{$transaction->payment_method}}</h5>


                <!-- <h5></h5> -->

            </div>
            <div class="col col-md-4 order-2 d-flex flex-md-row flex-column justify-content-md-center">

                <a href="#" class="mr-1 mb-1">Order #{{$transaction->order_id}}</a>
                

            </div>
            <div class="col col-md-4 order-3 text-md-right cursor-pointer" onclick="location.href='{{route('admin.transactions.show',$transaction->id)}}'">

                <h5>{{ date('d-m-Y', strtotime($transaction->created_at)) }}</h5>
                <h5>{{ date('h:i:s A', strtotime($transaction->created_at)) }}</h5>

                <h5><b>{!! $transaction['amount']==0?" ":$currency.''.$transaction['amount'] !!}</b></h5>

                <h4><b>{{$transaction->status}}</b></h4>

            </div>
        </div>
    </div>

</div>

@endforeach
@if(count($transactions) == 0)
<h3 class="text-light text-center p-2 w-100">No Transactions exist</h3>
@endif
{!! $transactions->render() !!}