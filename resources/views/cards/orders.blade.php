@php
if(empty($user_meta_data['settings']['currency'])){
$currency = "₹";
}
else{
if($user_meta_data['settings']['currency_selection'] == "currency_symbol")
$currency = App\User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
else
$currency = $user_meta_data['settings']['currency'];
}
@endphp
@foreach ($data as $customer_order)
@php
 $customer_order_meta_data= json_decode( $customer_order->meta_data, true);
@endphp
<div class="col-md-4 col-sm-12">

   <div class="card ecommerce-card" id="customer_list_{{ $customer_order->id}}">
      <div class="card-body d-flex">

         <div class="float-left">

            <h5 class="card-title">Order<b>#{{ $customer_order->order->order_id}}</b></h5>
            <br>

            <p><a href="{{ route('admin.export_order_pdf',  $customer_order->order_id) }}" onclick='' style="padding-left:5px"><i style="color:red" class="fa fa-file-pdf-o fa-2x"></i></a></p>

         </div>

         <div class="float-right text-right" style="cursor:pointer; width:75%; height:100%;" onclick="location.href='{{route('admin.customers.displayorder', $customer_order->order_id)}}';">

            <h6>{{ date('d-m-Y', strtotime( $customer_order->created_at)) }}</h6>
            <h6>{{ date('h:i:s A', strtotime( $customer_order->created_at)) }}</h6>

            <h4><b>No. of Products: {{count( $customer_order->orderProducts)}}</b></h4>

            <h4><b>{!!  $customer_order_meta_data['product_total_price']==0 ? ' ' : $currency.''. $customer_order_meta_data['product_total_price'] !!}</b></h4>

            @php
               if(App\orders::STATUS[ $customer_order->order->status] == 'Ordered')
                  $class="text-primary";
               elseif (App\orders::STATUS[ $customer_order->order->status] == 'Approved')
                  $class="text-success";
               elseif (App\orders::STATUS[ $customer_order->order->status] == 'Rejected')
                  $class="text-danger";
               elseif (App\orders::STATUS[ $customer_order->order->status] == 'Cancelled')
                  $class="text-secondary";
               elseif (App\orders::STATUS[ $customer_order->order->status] == 'Delivered')
                  $class="text-success";
               else
               $class="text-info";
            @endphp
            <h4>Status: <span class='{{$class}}'>{{App\orders::STATUS[ $customer_order->order->status]}}</span></h4>

            <!-- <h4><b>{!!  $customer_order_meta_data['product_total_discount']==0?" ":  $customer_order_meta_data['product_total_discount'].'%' !!}</b></h4> -->

         </div>


      </div>
   </div>
</div>


@endforeach
@if(count($data) == 0)
<h3 class="text-light text-center p-2 w-100">No Orders exist</h3>
@endif
{!! $data->render() !!}