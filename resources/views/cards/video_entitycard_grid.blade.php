<style>
#overlay {
    border-radius: 0.5rem;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0,0,0,0.5);
  z-index: 2;
}
.videoIcon{
        margin:auto;
        height:200px;
        bottom:0;
        top: 80px;
        left: 0;
        right: 0;        
    }

#text{
  position: absolute;
  top: 50%;
  left: 50%;
  color: white;
  transform: translate(-50%,-50%);
  -ms-transform: translate(-50%,-50%);
}
</style>
@php
if(empty($user_meta_data['settings']['currency']))
$currency = "₹";
else
if($user_meta_data['settings']['currency_selection'] == "currency_symbol")
$currency = App\User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
else
$currency = $user_meta_data['settings']['currency'];
@endphp
@foreach ($data as $key => $card)
@php
$video = $card->video;
if(!empty($video->thumbnail)){
    $img_share = $img = $video->thumbnail->getUrl('thumb');
}
else{
    $img_share = $img = $objimg = asset('XR/assets/images/video-placeholder.png');
}
$obj_id = "";
if(!isset($video->id)){
continue;
}

@endphp

<div class="col-lg-4 col-md-6 col-sm-12" id="entitycard_{{$card->id}}">
    <div class="card row-match" style="box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.3)">
        <div class="card-header">
        <h5 class="card-title text-truncate" style="width:80%"><b><a href="#" type="button" class="update" data-pk="{{$video->id}}" data-url="{{route('admin.video_list.update',$video->id)}}" data-type="text" data-title="Enter name" data-name="name">{{$video->title ?? 'Untitled'}}</b></a></h5>
            <a href="javascript:void(0);" class="text-danger ml-1 float-right"  onclick="deleteCard({{$card->id}})"><i class="fa fa-trash" style="font-size:18px;"></i></a>
                    @if(!empty($video->downgradeable) || $video->downgradeable == "0")
                        @if ($video->downgradeable == 1)
                            <div class="badge badge-pill badge-glow badge-danger mr-1 text-truncate" style="max-width:45%;">Inactive</div>
                        @endif
                    @endif
            </a>
        </div>
        <div class="card-body">
        <img src="{{$img}}"  class="mw-100 mh-100 rounded img-responsive" style="height: 200px;width: 420px;"><i class="feather icon-play-circle fa-5x position-absolute text-light d-flex justify-content-center videoIcon"  ></i>
        </div>
        <div class="card-footer" style="padding: 1rem;">
            <div class="w-50 h-100 float-left pt-1 pl-1"><b><i class="feather icon-eye" style="font-size: 20px;"> {{$card->views}} </i></b></div>
                    <div class="w-50 h-100 d-flex justify-content-around pt-1">
                        <div>
                            <a onclick="getVideoDetail({{$video->id}})" class="text-dark" data-toggle="modal" data-target="#videoEditorModal">
                                <i class="fa fa-pencil" style="font-size: 18px;"></i>
                            </a>
                        </div>
                        <div class="catalog_share_card" {{$campaign->is_start == 1 ? '':'hidden'}}>
                            <a href="javascript:void(0);" onclick="linkShare($(this))" class="text-primary ml-1" style="color:#6610F2 !important;" data-id="{{$video->id}}" data-name="{{$video->title}}"   data-img="{{$img_share}}" 
                            data-link="{{$campaign->public_link}}" 
                            data-qrcode="{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'product',$video->id,true)}}"
                            data-title="{{$video->name}}" 
                            data-sharelink="{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'videoproduct',$video->id)}}" 
                            data-pdf="false"
                            data-download="false"
                            data-entity="product">
                                <i class="fa fa-share-alt" style="font-size: 18px;"></i>
                            </a>
                        </div>
                        <div class="catalog_noshare_card" {{$campaign->is_start == 1 ? 'hidden':''}}>
                            <a href="javascript:void(0);" class="text-light ml-1">
                                <i class="feather icon-share-2" style="font-size: 18px;"></i>
                            </a>
                        </div>
                        <div>
                            <a href="javascript:void(0);" class="text-success ml-1 mr-1" style="color:#28C76F !important" onclick="WhatsAppShare('{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'videoproduct',$video->id)}}','')">
                                <b><i class="fa fa-whatsapp" style="font-size: 18px;"></i></b>
                            </a>
                        </div>
                    </div>
            </div>
        </div>    
       <!-- <div class="d-inline-flex rounded" style="height:180px;">

            <div class="h-100 rounded-left d-flex justify-content-center"  style="width:45%"><img src="{{$img}}"  class="mw-100 mh-100 rounded-left img-responsive"></div>
            <div class="h-100" style="width:55%">
                <div class="w-100 p-1" style="height:80%;">
                    <a href="javascript:void(0);" class="text-danger ml-1 float-right" onclick="deleteCard({{$card->id}})"><i class="fa fa-trash" style="font-size:18px;"></i></a>
                    <h5 class="card-title text-truncate"><b><a href="#" type="button" class="update" data-pk="{{$video->id}}" data-url="{{route('admin.video_list.update',$video->id)}}" data-type="text" data-title="Enter name" data-name="name">{{$video->title ?? 'Untitled'}}</b></a></h5>
                    @if(!empty($video->downgradeable) || $video->downgradeable == "0")
                        @if ($video->downgradeable == 1)
                            <div class="badge badge-pill badge-glow badge-danger mr-1 text-truncate" style="max-width:45%;">Inactive</div>
                        @endif
                    @endif
                </div>
                <div class="w-100 d-inline-flex" style="height:20%;">
                    <div class="w-50 h-100 float-left pt-1 pl-1"><i class="feather icon-eye" style="font-size: 15px;"> {{$card->views}} </i></div>
                    <div class="w-50 h-100 d-flex justify-content-around pt-1">
                        <div>
                            <a onclick="getVideoDetail({{$video->id}})" class="text-dark" data-toggle="modal" data-target="#videoEditorModal">
                                <i class="fa fa-pencil" style="font-size: 18px;"></i>
                            </a>
                        </div>
                        <div class="catalog_share_card" {{$campaign->is_start == 1 ? '':'hidden'}}>
                            <a href="javascript:void(0);" onclick="linkShare($(this))" class="text-primary ml-1" style="color:#6610F2 !important;" data-id="{{$video->id}}" data-name="{{$video->title}}"   data-img="{{$img_share}}" 
                              data-link="{{$campaign->public_link}}" 
                              data-qrcode="{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'product',$video->id,true)}}"
                               data-title="{{$video->name}}" 
                               data-sharelink="{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'videoproduct',$video->id)}}" 
                               data-pdf="false"
                               data-download="false"
                               data-entity="product">
                                <i class="fa fa-share-alt" style="font-size: 18px;"></i>
                            </a>
                        </div>
                        <div class="catalog_noshare_card" {{$campaign->is_start == 1 ? 'hidden':''}}>
                            <a href="javascript:void(0);" class="text-light ml-1">
                                <i class="feather icon-share-2" style="font-size: 18px;"></i>
                            </a>
                        </div>
                        <div>
                            <a href="javascript:void(0);" class="text-success ml-1 mr-1" style="color:#28C76F !important" onclick="WhatsAppShare('{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'product',$video->id)}}','')">
                                <b><i class="fa fa-whatsapp" style="font-size: 18px;"></i></b>
                            </a>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
        @if(($video->downgradeable == 1) and ($product_enable == "false"))
        <div id="overlay">
          <div id="text"><b>Product Disabled</b></div>
        </div>
        @endif
    </div>

</div>
@endforeach
@if( ($is_search == false) and (count($data) == 0))
<!-- <i class="fa fa-list fa-4x nav-icon text-light text-center p-2 w-100"></i> -->
<h3 class="text-light text-center p-2 w-100"><img src="{{asset('/XR/app-assets/images/icons/AddProduct.png')}}" style="height: 200px;"></h3>
<h3 class="text-light text-center p-2 w-100">This is an empty catalog.</h3>
<h3 class="text-light text-center p-2 w-100">Add products from your camera, photo gallery or my products to build your catalog.</h3>
<div style="border-style:dashed;cursor:pointer;" onclick="addnewvideo()" class="rounded mb-0 m-1 text-center p-2 text-dark w-100">
    <div class="image">
        <i class="feather icon-plus-circle font-large-1"></i>
    </div>
    <span style="display: block;padding:2px;">Add Video</span>
</div>
@endif
@if( ($is_search == true) and (count($data) == 0))
<h3 class="text-light text-center p-2 w-100">Searched product not found.</h3>
@endif
{!! $data->render() !!}
<script>
    $('.update').editable({
        mode: "inline"
    });
</script>