@php
  if(empty($user_meta_data['settings']['currency'])){
    $currency = "₹";
  }
  else{
    if($user_meta_data['settings']['currency_selection'] == "currency_symbol")
      $currency = App\User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
    else
      $currency = $user_meta_data['settings']['currency'];
  }
@endphp
 @foreach ($data as $customer)
                <div class="card ecommerce-card" id="customer_list_{{$customer->id}}">
                  {{-- @php
                   dd($data);   
                  @endphp --}}
                  @php 
                    $customer_total_price = 0;
                    $customer_meta_data = json_decode($customer->meta_data,true);
                    $orders = App\ordersCustomer::select('*')->where("email", $customer->email);
                    $customer_order_data = $orders->paginate();
                    foreach ($customer_order_data as $order){
                      $order_meta_data= json_decode($order->meta_data, true);
                      $customer_total_price += $order_meta_data['product_total_price'];
                    }
                  @endphp
                    <div class="card-body">
                         {{-- <h5 class="card-title">Customer <b>#{{$customer->id}}</b></h5> --}}
                     <div class="row ">
                         <div class="col-10 col-md-4 "  style="cursor:pointer;" onclick="location.href='{{route('admin.customers.details',$customer->id)}}';">
                           
                          <h4><b>{{$customer->fname}} {{$customer->lname ?? ""}}</b></h4>  
                          
                          <h5>{{$customer->mobileno ?? ""}}</h5> 

                          <h5>{{$customer->email ?? ""}}</h5>

                          <h4><b>Customer type: </b><span class="{{isset($customer->customer_id)?'text-success':'text-danger'}}">{{isset($customer->customer_id) ? 'Registered User' : 'Guest' }}</span></h4>
                           
                        </div>
                         <div  class="col col-md-4  d-flex flex-md-row flex-column justify-content-md-center justify-content-end">
                            
                          <p><a href="https://wa.me/{{$customer->country_code ? $customer->country_code.$customer->mobileno : '91'.$customer->mobileno}}" target="_blank"><i style="color:#25D366"class="fa fa-whatsapp fa-2x"></i></a></p>&nbsp;&nbsp;&nbsp;&nbsp;
                            <p><a href="tel:{{$customer->mobileno}}" style="padding-left:5px"><i style="color:#2F91F3"class="fa fa-phone fa-2x"></i></a></p>&nbsp;&nbsp;&nbsp;&nbsp;
                            {{-- <p><a href="#" onclick='' style="padding-left:5px"><i style="color:red" class="fa fa-file-pdf-o fa-2x"></i></a></p> --}}
            
                         </div>
                         <div  class="col-sm-12 col-md-4 text-md-right"  style="cursor:pointer;" onclick="location.href='{{route('admin.customers.details',$customer->id)}}';">
                                                     
                         <h4><b>No. of Orders: {{$customer->total}}</b></h4>
                         <h4><b>Total Amount: {!! $currency.''.$customer_total_price !!}</b></h4>
                         </div>

                     </div>
                    </div>

                </div>
@endforeach
    @if(count($data) == 0)
    <h3 class="text-light text-center p-2 w-100">No customers match your search</h3>
@endif
{!! $data->render() !!}
<script>
   $('#customer_count').empty().html({{$data->total()}});
</script>
