<style>
    @media (max-width: 576px) {
        .cardchanges{
            padding: 0.5rem !important;
        }
        .cardbox{
            box-shadow: 0 0px 0px 0 !important;
        }
    }
</style>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <div class="card-body">
        <form method="POST" action="#" name="update_product_form" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @php
            $product_meta_data = json_decode($product->scripts->meta_data ?? '', true);
        @endphp

                <div class="row">
                <div class="col-sm-12 col-md-12 mb-1">

                    <div class="form-group">
                        <button class="btn btn-danger float-right" type="button" onclick="update_product($(this))" data-modal_name="productEditorModal">
                            {{ trans('global.save') }}
                        </button>
                    </div>
                </div>
                    <div class="col-sm-12 col-md-12 p-0">

                        <div class="card cardbox collapse-icon mb-0">
                            <div class="card-body cardchanges">
                                <div class="collapse-default">

                                    <div class="card p-0" style="border: #B8C2CC;border-style: solid;">
                                        <div id="headingCollapse1" class="card-header p-1" data-toggle="collapse" role="button" data-target="#collapse1" aria-expanded="false" aria-controls="collapse1">
                                            <span class="lead collapse-title"> Product Images</span>
                                        </div>
                                        <div id="collapse1" role="tabpanel" aria-labelledby="headingCollapse1" class="collapse">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <div class="item-img text-center d-flex mw-100 justify-content-center" id="prod_slide" style="min-height: 12.85rem;">
                                                        @if(count($product->photo) > 0)
                                                            <div id="carousel-thumb_{{$product->id}}" class="mw-100 carousel slide carousel-fade carousel-thumbnails carousel-thumb" data-interval="false" data-ride="carousel">
                                                                <div class="carousel-inner" role="listbox">
                                                                    @foreach($product->photo as $photo)
                                                                        <div class="carousel-item {{$loop->index == 0?'active':''}}" style="max-height:350px;">
                                                                            <img class="img-fluid rounded w-sm-100 mw-lg-50" id="image_{{$photo->id}}" style=" max-height:350px;" src="{{$photo->getUrl() ?? asset('assets/images/placholder.png')}}">
                                                                        </div>
                                                                    @endforeach
                                                                </div>
                                                                <div class="w-100 mt-1 justify-content-lg-center d-inline-flex niceScroll" style="height:120px;overflow-x:auto;max-width:75vw;z-index:1000">
                                                                    @foreach($product->photo as $photo)
                                                                        <div data-target="#carousel-thumb_{{$product->id}}" id="product_{{$photo->id}}" data-slide-to="{{$loop->index}}" class="{{$loop->index == 0?'active':''}} mr-1" style="height:120px;width:90px;">
                                                                            <img class="d-flex rounded cursor-pointer" style="height:60px;width:90px;" src="{{$photo->getUrl() ?? asset('assets/images/placholder.png')}}">
                                                                            <button href="#{{$photo->id}}" id="delete_img" onclick="removeimg({{$photo->id}})" class="btn btn-danger btn-sm text-white mt-2"><i class="feather icon-trash-2 m-0"></i></button>
                                                                        </div>
                                                                    @endforeach
                                                                    <a data-toggle="modal" data-target="#productUploadimgModal" style="cursor:pointer;">
                                                                        <div style="min-height:120px;max-height:120px;min-width: 80px;max-width:80px;text-align: center;border-style:dashed;" class="rounded ml-1">
                                                                            <div class="image mt-2">
                                                                                <i class="feather icon-plus-circle font-large-1 " ></i>
                                                                            </div>
                                                                            <span style="display: block;padding: 4px;">Upload Image</span>
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                                <!-- <div class="w-100 bg-success" style="max-width:75vw;">
                                                                    <ol class=" carousel-indicators overflow-hidden position-relative m-0 bg-warning w-100" style="z-index:1;">
                                                                        @foreach($product->photo as $photo)
                                                                            <li data-target="#carousel-thumb_{{$product->id}}" id="product_{{$photo->id}}" data-slide-to="{{$loop->index}}" class="{{$loop->index == 0?'active':''}} ml-1" style="min-width:90px;min-height:120px;">
                                                                                <div style="height:90px;object-fit: contain;">
                                                                                    <img class="d-block img-fluid rounded cursor-pointer w-100" style="height:60px;" src="{{$photo->getUrl() ?? asset('assets/images/placholder.png')}}">
                                                                                </div>
                                                                                <button href="#{{$photo->id}}" id="delete_img" onclick="removeimg({{$photo->id}})" class="btn btn-danger btn-sm text-white"><i class="feather icon-trash-2 m-0"></i></button>
                                                                            </li>
                                                                        @endforeach
                                                                        <a data-toggle="modal" data-target="#productUploadimgModal" style="cursor:pointer;">
                                                                        <div style="min-height:120px;max-height:120px;min-width: 80px;max-width:80px;text-align: center;border-style:dashed;" class="rounded my-1 ml-1">
                                                                            <div class="image mt-2">
                                                                                <i class="feather icon-plus-circle font-large-1 " ></i>
                                                                            </div>
                                                                            <span style="display: block;padding: 4px;">Upload Image</span>
                                                                        </div>
                                                                        </a>
                                                                    </ol>
                                                                </div> -->

                                                            </div>
                                                        @else
                                                            <div style="height:110px;max-width: 400px;border-style:dashed;position:relative;margin-left:30%;margin-right:30%;" class="rounded mb-0 text-center">
                                                                <div class="image mt-2">
                                                                    <i data-toggle="modal" data-target="#productUploadimgModal" class="feather icon-plus-circle font-large-1 "></i>
                                                                </div>
                                                                <span style="display: block;padding: 4px;">Upload Image</span>
                                                            </div>
                                                            <h1 class="p-4 text-light">No Product Images Uploaded</h1>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card p-0" style="border: #B8C2CC;border-style: solid;">
                                        <div id="headingCollapse2" class="card-header collapse-header p-1" data-toggle="collapse" role="button" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
                                            <span class="lead collapse-title">Product Details</span>
                                        </div>
                                        <div id="collapse2" role="tabpanel" aria-labelledby="headingCollapse2" class="collapse" aria-expanded="false">
                                            <div class="card-body">
                                                <div class="col-12">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-12">
                                                            <div class="form-group">
                                                                <label for="name">{{ trans('cruds.product.fields.name') }}</label>
                                                                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $product->name) }}">
                                                                @if($errors->has('name'))
                                                                    <div class="invalid-feedback">
                                                                        {{ $errors->first('name') }}
                                                                    </div>
                                                                @endif
                                                                <span class="help-block">{{ trans('cruds.product.fields.name_helper') }}</span>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6 col-md-12">
                                                            <div class="form-group">
                                                                <label for="sku">{{ trans('cruds.product.fields.sku') }}</label>
                                                                <input class="form-control {{ $errors->has('sku') ? 'is-invalid' : '' }}" type="text" name="sku" id="sku" value="{{ old('sku', $product->sku) }}" required>
                                                                @if($errors->has('sku'))
                                                                    <div class="invalid-feedback">
                                                                        {{ $errors->first('sku') }}
                                                                    </div>
                                                                @endif
                                                                <span class="help-block">{{ trans('cruds.product.fields.sku_helper') }}</span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-12">
                                                            <div class="form-group">
                                                                <label for="price">{{ trans('cruds.product.fields.price') }}</label>
                                                                <input class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}" type="number" name="price" id="price" value="{{ old('price', $product->price) }}" step="0.01">
                                                                @if($errors->has('price'))
                                                                    <div class="invalid-feedback">
                                                                        {{ $errors->first('price') }}
                                                                    </div>
                                                                @endif
                                                                <span class="help-block">{{ trans('cruds.product.fields.price_helper') }}</span>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6 col-md-12">
                                                            <div class="form-group px-1">
                                                                <input class="form-check-input " type="checkbox" name="discount_rate" id="discount_rate"  value="1" {{ old('discount', $product->discount) ? 'checked="checked"' : '' }} />
                                                                <label class="form-check-label" id="ofr_disc">Offer Discount</label>
                                                                <label class="form-check-label" style="display: none;" id="disc_per">{{ trans('cruds.product.fields.discount') }}(%)</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                <input class="form-control {{ $errors->has('discount') ? 'is-invalid' : '' }}" style="display: none" type="number" name="discount" id="discount" value="{{ old('discount', $product->discount) }}" onkeypress="return isNumberKey(event,this)" placeholder="Enter discount percentage within 100">
                                                                <span id="errMsg"></span>
                                                                @if($errors->has('discount'))
                                                                    <div class="invalid-feedback">
                                                                        {{ $errors->first('discount') }}
                                                                    </div>
                                                                @endif
                                                                <span class="help-block">{{ trans('cruds.product.fields.discount_helper') }}</span>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-12">
                                                            <div class="form-group">
                                                                <label for="categories">{{ trans('cruds.product.fields.category') }}</label>
                                                                <select class="form-control select2 {{ $errors->has('categories') ? 'is-invalid' : '' }}" name="categories[]" id="categories">
                                                                    <option value="">Choose Category</option>
                                                                    @foreach ($categories as $category)
                                                                        <option value="{{ $category->id }}" {{ (in_array($category->id, old('categories', [])) || $product->categories->contains($category->id)) ? 'selected' : '' }}>{{ $category->name }}</option>
                                                                        @if ($category->childrenCategories)
                                                                            @foreach ($category->childrenCategories as $child_category)
                                                                                @include('admin.products.childcategory', ['product' => $product,'child_category' => $child_category,'level'=>0])
                                                                            @endforeach
                                                                        @endif
                                                                    @endforeach
                                                                </select>
                                                                @if($errors->has('categories'))
                                                                    <div class="invalid-feedback">
                                                                        {{ $errors->first('categories') }}
                                                                    </div>
                                                                @endif
                                                                <span class="help-block">{{ trans('cruds.product.fields.category_helper') }}</span>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6 col-md-12">
                                                            <div class="form-group">
                                                                <label for="categories">Add Category</label>
                                                                <a data-toggle="modal" data-target="#categoryAddModal" style="cursor:pointer;">
                                                                    <div class="image">
                                                                        <i class="feather icon-plus-circle font-large-1 " ></i>
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        </div>

                                                        <!-- <div class="col-lg-6 col-md-12">
                                                            <div class="form-group">
                                                                <label for="tags">{{ trans('cruds.product.fields.tag') }}</label>
                                                                <div style="padding-bottom: 4px">
                                                                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                                                                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                                                                </div>
                                                                <select class="form-control select2 {{ $errors->has('tags') ? 'is-invalid' : '' }}" name="tags[]" id="tags" multiple>
                                                                    @foreach($tags as $id => $tag)
                                                                        <option value="{{ $id }}" {{ (in_array($id, old('tags', [])) || $product->tags->contains($id)) ? 'selected' : '' }}>{{ $tag }}</option>
                                                                    @endforeach
                                                                </select>
                                                                @if($errors->has('tags'))
                                                                    <div class="invalid-feedback">
                                                                        {{ $errors->first('tags') }}
                                                                    </div>
                                                                @endif
                                                                <span class="help-block">{{ trans('cruds.product.fields.tag_helper') }}</span>
                                                            </div>
                                                        </div> -->
                                                    </div>

                                                    {{-- attribute div --}}
                                                    <div class="row">
                                                        @if(session('subscribed_features'))
                                                            @if(array_key_exists("inventory", session('subscribed_features')[0]))
                                                                <div class="col-lg-3 col-md-12">
                                                                    <div class="form-group pt-2 px-1">
                                                                        <input class="form-check-input" type="checkbox" id="stock_qty" name="stock_qty" value="1" {{ old('stock', $product->stock) ? 'checked="checked"' : '' }} />
                                                                        <label class="form-check-label" id="manage_inv">Manage Inventory</label>
                                                                        <label class="form-check-label" id="stock_qt" style="display: none;">Stock Quantity</label>
                                                                        <input class="form-control" style="display: none" type="number" name="stock" id="stock" value="{{ old('stock', $product->stock) }}">
                                                                        @if($errors->has('stock'))
                                                                            <div class="invalid-feedback">
                                                                                {{ $errors->first('stock') }}
                                                                            </div>
                                                                        @endif
                                                                        <span class="help-block">{{ trans('cruds.product.fields.stock_helper') }}</span>
                                                                    </div>
                                                                </div>

                                                                <div class="col-lg-3 col-md-12">
                                                                    <div class="px-1 pt-2  form-group">
                                                                        <input class="form-check-input" type="checkbox" id="out_of_stock" name="out_of_stock" value="1" {{ old('out_of_stock',$product->out_of_stock) ? 'checked="checked"' : '' }} />
                                                                        <label class="form-check-label" id="out_stock">{{ trans('cruds.product.fields.out_of_stock') }}</label>
                                                                        @if($errors->has('out_of_stock'))
                                                                            <div class="invalid-feedback">
                                                                                {{ $errors->first('out_of_stock') }}
                                                                            </div>
                                                                        @endif
                                                                        <span class="help-block">{{ trans('cruds.product.fields.out_of_stock_helper') }}</span>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @endif
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-12">
                                                            <div class="form-group">
                                                                <label for="price">{{ trans('cruds.product.fields.minimum_quantity') }}</label>
                                                                <input class="form-control {{ $errors->has('minimum_quantity') ? 'is-invalid' : '' }}" type="number" min="1" name="minimum_quantity" id="minimum_quantity" value="{{ old('minimum_quantity', $product->minimum_quantity) }}" step="1">
                                                                <span id="errMsg"></span>
                                                                @if($errors->has('minimum_quantity'))
                                                                    <div class="invalid-feedback">
                                                                        {{ $errors->first('minimum_quantity') }}
                                                                    </div>
                                                                @endif
                                                                <span class="help-block">{{ trans('cruds.product.fields.minimum_quantity_helper') }}</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-12 px-1">
                                                            <div class="form-group">
                                                                <label for="price">{{ trans('cruds.product.fields.maximum_quantity') }}</label>
                                                                <input class="form-control {{ $errors->has('maximum_quantity') ? 'is-invalid' : '' }}" type="number" min="4" name="maximum_quantity" id="maximum_quantity" value="{{ old('maximum_quantity', $product->maximum_quantity) }}" step="1">
                                                                <span id="errMsg"></span>
                                                                @if($errors->has('maximum_quantity'))
                                                                    <div class="invalid-feedback">
                                                                        {{ $errors->first('maximum_quantity') }}
                                                                    </div>
                                                                @endif
                                                                <span class="help-block">{{ trans('cruds.product.fields.maximum_quantity_helper') }}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                    <div class="col-lg-6 col-md-12">
                                                            <div class="form-group">
                                                            <label for="price">{{ trans('cruds.product.fields.unit_quantity') }}</label>
                                                            <input class="form-control " type="number" min="1" name="quantity" id="quantity" value="{{ old('quantity', $product->quantity) }}" step="1">

                                                            </div>
                                                    </div>
                                                        <div class="col-lg-6 col-md-12">
                                                            <div class="form-group">
                                                            <label for="price">{{ trans('cruds.product.fields.unit_type') }}</label>
                                                                <select class="form-control select2" id="units" name="units">
                                                                    <!-- @if($product->units != null)
                                                                        <option value="{{$product->units}}">{{$product->units}}</option>
                                                                    @else
                                                                        <option value="">Choose Unit</option>
                                                                    @endif -->
                                                                    <option value="{!!empty($product->units)?'':$product->units!!}">{!!empty($product->units)?"Choose Unit":"$product->units"!!}</option>
                                                                    @php
                                                                        $unit=array("piece","kg","gram","ml","litre","mm","ft","meter","sq.ft","sq.meter","km","set","hour","day","bunch","bundle","month","year","service","packet","work","box","pound","dozen","gunta","pair","minute","quintal","ton","capsule","tablet","plate","inch");
                                                                    @endphp
                                                                    @foreach($unit as $value)
                                                                        <option value="{{$value}}">{{$value}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        </div>
                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-12">
                                                            <div class="px-1 form-group">
                                                                <input class="form-check-input" type="checkbox" id="new" name="new" value="1" {{ old('new',$product->new) ? 'checked="checked"' : '' }}/>
                                                                <label class="form-check-label" id="new">New</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-12">
                                                            <div class="px-1 form-group">
                                                                <input class="form-check-input" type="checkbox" id="featured" name="featured" value="1" {{ old('featured',$product->featured) ? 'checked="checked"' : '' }}/>
                                                                <label class="form-check-label" id="featured">Featured</label>
                                                            </div>
                                                        </div>
                                                        @if(!empty($product->downgradeable) || $product->downgradeable == 0)
                                                        <div class="col-lg-6 col-md-12">
                                                            <div class="form-group">
                                                                <label class="form-check-label" for="downgradeable">Status</label>
                                                                <select class="custom-select form-control" name="downgradeable" id="downgradeable" required>
                                                                    <option value="0" {{ old('downgradeable',$product->downgradeable == 0) ? 'selected' : '' }}>Active</option>
                                                                    <option value="1" {{ old('downgradeable',$product->downgradeable == 1) ? 'selected' : '' }}>Inactive</option>
                                                                </select>
                                                                @if($errors->has('downgradeable'))
                                                                    <div class="invalid-feedback">
                                                                        {{ $errors->first('downgradeable') }}
                                                                    </div>
                                                                @endif
                                                                <span class="help-block">{{ trans('cruds.product.fields.downgradeable_helper') }}</span>
                                                            </div>
                                                        </div>
                                                    @else
                                                    @endif
                                                    </div>


                                                </div>
                                                <!-- <div class="row">
                                                    <div class="col-lg-6 col-md-12">
                                                        <div class="form-group">
                                                            <label for="length">Length</label>
                                                            <input class="form-control " type="number" name="length" id="length" value="{{ old('length', $product->length) }}">

                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6 col-md-12">
                                                        <div class="form-group">
                                                            <label for="width">Width</label>
                                                            <input class="form-control " type="number" name="width" id="width" value="{{ old('width', $product->width) }}" required>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-12">
                                                        <div class="form-group">
                                                            <label for="productweight">Weight</label>
                                                            <input class="form-control " type="number" name="productweight" id="productweight" value="{{ old('productweight', $product->productweight) }}">

                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6 col-md-12">
                                                        <div class="form-group">
                                                            <label for="height">Height</label>
                                                            <input class="form-control " type="number" name="height" id="height" value="{{ old('height', $product->height) }}" required>

                                                        </div>
                                                    </div>
                                                </div> -->
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="description">{{ trans('cruds.product.fields.description') }}</label>
                                                        <textarea class="form-control pb-1 {{ $errors->has('description') ? 'is-invalid' : '' }} hidden" rows="5" name="description" id="description">{{ old('description', $product->description) }}</textarea>
                                                        <div id="editor-container" style="height: 250px;"></div>
                                                        @if($errors->has('description'))
                                                            <div class="invalid-feedback">
                                                                {{ $errors->first('description') }}
                                                            </div>
                                                        @endif
                                                        <span class="help-block">{{ trans('cruds.product.fields.description_helper') }}</span>
                                                    </div>
                                                </div>


                                                @if(count($attributes)>=1)
                                                    {{-- {!!dd($attributes)!!} --}}
                                                        <div class="col-lg-6 col-md-12">
                                                            <div class="px-1 form-group">
                                                                <input class="form-check-input" type="checkbox" id="attribute" name="attribute" value="1" {{ old('attribute', count($values)>0?true:false) ? 'checked' : '' }} />
                                                                <label class="form-check-label text-capitalize" for="attribute">Add Attributes</label>
                                                            </div>
                                                            {{-- attribute-list --}}
                                                            <div id="attribute_list">
                                                                @foreach ($attributes as $groupname=>$attributes)
                                                                    <label class=" bg-light w-100 mb-1 text-capitalize font-weight-bold" style="padding:5px;">{{$groupname}}</label>
                                                                    <div>
                                                                        @foreach($attributes as $attribute)
                                                                            @if ($attribute->type=="textbox")
                                                                                <div class="form-group ml-1">
                                                                                    <label class="text-capitalize font-weight-bold" for="{{$attribute->label}}">{{ $attribute->label }}</label>
                                                                                    <input class="form-control {{ $errors->has('$attribute->label') ? 'is-invalid' : '' }}" type="text" name="{{$attribute->slug}}" id="{{$attribute->id."_".$attribute->label}}" value="{{ old('$attribute->label', count($values)>0 &&  isset($values[$attribute->attribute_id]) ? $values[$attribute->attribute_id]: '') }}">
                                                                                </div>
                                                                            @elseif($attribute->type=="checkbox")
                                                                                <div class="form-group ml-1">
                                                                                    <label class="text-capitalize font-weight-bold" for="{{$attribute->label}}">{{ $attribute->label }}</label>
                                                                                    @foreach(explode(",",$attribute->values) as $value)
                                                                                        <div class="form-check form-check-inline">
                                                                                            <input class="form-check-input {{ $errors->has('$attribute->label') ? 'is-invalid' : '' }} " type="checkbox" id="{{$attribute->id."_".$value}}" name="{{$attribute->slug}}[]" value="{{$value}}" {{count($values)>0&&isset($values[$attribute->attribute_id]) && in_array($value ,explode(",",$values[$attribute->attribute_id]))? 'checked' : ''}}>
                                                                                            <label class="form-check-label" for="{{$attribute->id."_".$value}}">{{$value}}</label>
                                                                                        </div>
                                                                                    @endforeach
                                                                                </div>
                                                                            @elseif($attribute->type=="radio_button")
                                                                                <div class="form-group ml-1">
                                                                                    <label class="text-capitalize attr_radiogroup font-weight-bold {{$attribute->slug}}" data-id={{$attribute->id}} for="{{$attribute->label}}">{{ $attribute->label }}</label>
                                                                                    @foreach(explode(",",$attribute->values) as $value)
                                                                                        <div class="form-check form-check-inline">
                                                                                            <input class="form-check-input {{ $errors->has('$attribute->label') ? 'is-invalid' : '' }} attr_radiobtn{{$attribute->id}}   " type="radio" id="{{$attribute->id."_".$value}}" name="{{$attribute->slug}}" value="{{$value}}" {{-- onclick="check(this)"   --}} {{count($values)>0&&isset($values[$attribute->attribute_id]) && $values[$attribute->attribute_id]==$value ? 'checked' : ''}}>
                                                                                            <label class="form-check-label" for="{{$attribute->id."_".$value}}">{{$value}}</label>
                                                                                        </div>
                                                                                    @endforeach
                                                                                </div>
                                                                            @elseif($attribute->type=="dropdown")
                                                                                <div class="form-group ml-1">
                                                                                    <label class="text-capitalize font-weight-bold" for="{{$attribute->label}}">{{$attribute->label}}</label>
                                                                                    <select class="form-control custom-select {{ $errors->has('$attribute->label') ? 'is-invalid' : '' }} " name="{{$attribute->slug}}" id="{{$attribute->id}}">
                                                                                        <option value="">Choose..</option>
                                                                                        @foreach (explode(",",$attribute->values) as $value)
                                                                                            <option value="{{ $value }}" {{count($values)>0&&isset($values[$attribute->attribute_id])&& $values[$attribute->attribute_id]==$value ? 'selected' : ''}}>
                                                                                                {{ $value }}
                                                                                            </option>
                                                                                        @endforeach
                                                                                    </select>
                                                                                </div>
                                                                            @endif
                                                                        @endforeach
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                @endif
                                                <div class="row">
                                            <div class="col-lg-3 col-md-12">
                                                                    <div class="form-group pt-2 px-1">
                                                                        <input class="form-check-input" type="checkbox" id="external_shop" name="external_shop" value="1" {{ old('shop', $product->shop) ? 'checked="checked"' : '' }} />
                                                                        <label class="form-check-label" id="shop_link">External Shopping Link</label><br>
                                                                        <label class="form-check-label" id="shop_name" >Name</label>
                                                                        <input class="form-control"  type="text" name="shop" id="shop" value="{{ old('shop', $product->shop) }}">
                                                                        <label class="form-check-label" id="shop_url" >Link</label>
                                                                        <input class="form-control"  type="text" name="link" id="link" value="{{ old('link', $product->link) }}">
                                                                        <!-- @if($errors->has('stock'))
                                                                            <div class="invalid-feedback">
                                                                                {{ $errors->first('stock') }}
                                                                            </div>
                                                                        @endif
                                                                        <span class="help-block">{{ trans('cruds.product.fields.stock_helper') }}</span> -->
                                                                    </div>
                                                                </div>
                                    </div>
                                            </div>



                                        </div>

                                    </div>

                                    <div class="card p-0" style="border: #B8C2CC;border-style: solid;">
                                        <div id="seo" class="card-header p-1" data-toggle="collapse" role="button" data-target="#seoCollapse" aria-expanded="false" aria-controls="seoCollapse">
                                            <span class="lead collapse-title"> SEO</span>
                                        </div>
                                        <div id="seoCollapse" role="tabpanel" aria-labelledby="seo" class="collapse">
                                            <div class="card-body">
                                                <div class="col-12">
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12">
                                                        <div class="form-group">
                                                            <label for="seo_title">SEO Title</label>
                                                            <input class="form-control " type="text" name="seo_title" id="seo_title"  maxlength="120" value="{{ old('seo_title', $product->seo_title) }}">

                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12">
                                                        <div class="form-group">
                                                            <label for="seo_description">SEO Description</label>
                                                            <textarea class="form-control" type="text" id="seo_description" name="seo_description" maxlength="160" >{{ old('seo_description', $product->seo_description) }}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12">
                                                        <div class="form-group">
                                                            <label for="seo_keyword">SEO Keyword</label>
                                                            <input class="form-control " type="text" name="seo_keyword" id="seo_keyword"  value="{{ old('seo_keyword', $product->seo_keyword) }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card p-0" style="border: #B8C2CC;border-style: solid;">
                                        <div id="shipCollapse" class="card-header p-1" data-toggle="collapse" role="button" data-target="#shippingcollapse" aria-expanded="false" aria-controls="shippingcollapse">
                                            <span class="lead collapse-title"> Shipping Parameter</span>
                                        </div>
                                        <div id="shippingcollapse" role="tabpanel" aria-labelledby="shipCollapse" class="collapse">
                                            <div class="card-body">
                                                <div class="col-12">
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-12">
                                                        <div class="form-group">
                                                            <label for="length">Length</label>
                                                            <input class="form-control " type="number" name="length" id="length" placeholder="length in meter" value="{{ old('length', $product->length) }}">

                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6 col-md-12">
                                                        <div class="form-group">
                                                            <label for="width">Width</label>
                                                            <input class="form-control " type="number" name="width" id="width" placeholder="width in meter" value="{{ old('width', $product->width) }}" required>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-12">
                                                        <div class="form-group">
                                                            <label for="productweight">Weight</label>
                                                            <input class="form-control " type="number" name="productweight" placeholder="Weight in Kg" id="productweight" value="{{ old('productweight', $product->productweight) }}">

                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6 col-md-12">
                                                        <div class="form-group">
                                                            <label for="height">Height</label>
                                                            <input class="form-control " type="number" name="height" id="height" placeholder="height in meter" value="{{ old('height', $product->height) }}" required>

                                                        </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card p-0" style="border: #B8C2CC;border-style: solid;">
                                        <div id="headingCollapse3" class="card-header collapse-header p-1" data-toggle="collapse" role="button" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                            <span class="lead collapse-title"> Try On images </span>
                                        </div>
                                        @php
                                            $ar_necklace = null;
                                            $ar_ear_left = null;
                                            $ar_ear_right = null;
                                            $ar_watch = null;
                                            $ar_ring = null;
                                            $ar_bracelet = null;
                                            $ar_apparel = null;
                                            $ar_3d_file = null;
                                            $ar_3d_ios_file = null;
                                            foreach($product->arobject as $arobject){
                                                if($arobject->ar_type == "face"){
                                                    if($arobject->position == "jwl-nck"){
                                                       $ar_necklace = $arobject;
                                                    }
                                                    else if($arobject->position == "jwl-ear-l"){
                                                        $ar_ear_left = $arobject;
                                                    }
                                                    else if($arobject->position == "jwl-ear-r"){
                                                        $ar_ear_right = $arobject;
                                                    }
                                                    else if($arobject->position == "watch"){
                                                        $ar_watch = $arobject;
                                                    }
                                                    else if($arobject->position == "ring"){
                                                        $ar_ring = $arobject;
                                                    }
                                                    else if($arobject->position == "bracelet"){
                                                        $ar_bracelet = $arobject;
                                                    }
                                                    else if($arobject->position == "apparel"){
                                                        $ar_apparel = $arobject;
                                                    }
                                                }
                                                else{
                                                    if(!empty($arobject->object_3d))
                                                    {
                                                        $ar_3d_file = $arobject;
                                                    }
                                                    if(!empty($arobject->object_3d_ios))
                                                    {
                                                        $ar_3d_ios_file = $arobject;
                                                    }
                                                }
                                            }
                                            //dd($ar_3d_file,$ar_necklace,$ar_ear_left,$ar_ear_right);
                                        @endphp
                                        <div id="collapse3" role="tabpanel" aria-labelledby="headingCollapse3" class="collapse" aria-expanded="false">
                                            <div class="card-body">
                                            @if(session('subscribed_features'))
                                                @if(array_key_exists("ar_object", session('subscribed_features')[0]))
                                                <div class="row">
                                                    <div class="col-lg-4 col-md-12 text-center mb-2">
                                                        @if(!empty($ar_necklace))
                                                            <div style="height:100px;object-fit: contain;text-align: -webkit-center;">
                                                                <h6>Necklace</h6>
                                                                <img class="d-block img-fluid" style="min-width:90px; min-height:70px;max-height:80px;" src="{{$ar_necklace->object->getUrl() ?? asset('assets/images/placholder.png')}}">
                                                            </div>
                                                            <button type="button" href="#" id="delete_img" onclick="removetryonimg({{$ar_necklace->id}})" class="btn btn-danger btn-sm text-white mt-1"><i class="feather icon-trash-2 m-0"></i></button>
                                                        @else
                                                            <div style="height:120px;max-width: 400px;position:relative;margin-left:25%;margin-right:25%;display: none;" class="rounded mb-0 text-center" id="added_img_jwl-nck">
                                                                <h5>Necklace try on image uploaded</h5>
                                                            </div>
                                                            <div style="cursor: pointer;height:120px;max-width: 400px;border-style:dashed;position:relative;margin-left:25%;margin-right:25%;" class="rounded mb-0 text-center" id="add_img_jwl-nck" data-position="jwl-nck" onclick="open_tryon_image_upload($(this))">
                                                                <div class="image mt-2">
                                                                    <i class="feather icon-plus-circle font-large-1"></i>
                                                                </div>
                                                                <span style="display: block;padding: 4px;">Upload Tryon necklace Image</span>
                                                            </div>
                                                        @endif
                                                        @if(app('impersonate')->isImpersonating())
                                                        <div class="row pl-1 pt-1">
                                                            <div class="col-lg-6 col-md-12">
                                                            <span class="float-left"><b> Necklace Offset:</b></span><br>
                                                                <div class="form-group ">
                                                                    <label for="necklace_v_position" class="float-left">V-Position</label>
                                                                    <input class="form-control" id="necklace_v_position" name="necklace_v_position" value="@if(isset($meta_data['tryon_settings']['necklace_v_position'])) {{ isset($meta_data['tryon_settings']['necklace_v_position'])?old('necklace_v_position',$meta_data['tryon_settings']['necklace_v_position']):''}} @endif">
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="necklace_h_position" class="float-left">H-Position</label>
                                                                    <input class="form-control" id="necklace_h_position" name="necklace_h_position" value="@if(isset($meta_data['tryon_settings']['necklace_h_position'])) {{ isset($meta_data['tryon_settings']['necklace_h_position'])?old('necklace_h_position',$meta_data['tryon_settings']['necklace_h_position']):''}} @endif">
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="necklace_size" class="float-left">Size</label>
                                                                    <input class="form-control" id="necklace_size" name="necklace_size" value="@if(isset($meta_data['tryon_settings']['necklace_size'])) {{ isset($meta_data['tryon_settings']['necklace_size'])?old('necklace_size',$meta_data['tryon_settings']['necklace_size']):''}} @endif">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 text-center mb-2">
                                                        @if(!empty($ar_ear_left))
                                                            <div style="height:100px;object-fit: contain;text-align: -webkit-center;">
                                                                <h6>Earring Left</h6>
                                                                <img class="d-block img-fluid" style="min-width:90px; min-height:70px;max-height:80px;" src="{{$ar_ear_left->object->getUrl() ?? asset('assets/images/placholder.png')}}">
                                                            </div>
                                                            <button type="button" href="#" id="delete_img" onclick="removetryonimg({{$ar_ear_left->id}})" class="btn btn-danger btn-sm text-white mt-1"><i class="feather icon-trash-2 m-0"></i></button>
                                                        @else
                                                            <div style="height:120px;max-width: 400px;position:relative;margin-left:25%;margin-right:25%;display: none;" class="rounded mb-0 text-center" id="added_img_jwl-ear-l">
                                                                <h5>Earring Left try on image uploaded</h5>
                                                            </div>
                                                            <div style="cursor: pointer;height:120px;max-width: 400px;border-style:dashed;position:relative;margin-left:25%;margin-right:25%;" class="rounded mb-0 text-center" id="add_img_jwl-ear-l" data-position="jwl-ear-l" onclick="open_tryon_image_upload($(this))">
                                                                <div class="image mt-2">
                                                                    <i class="feather icon-plus-circle font-large-1"></i>
                                                                </div>
                                                                <span style="display: block;padding: 4px;">Upload Tryon earring left Image</span>
                                                            </div>
                                                        @endif
                                                        @if(app('impersonate')->isImpersonating())
                                                        <div class="row pl-1 pt-1">
                                                            <div class="col-lg-6 col-md-12">
                                                            <span class="float-left"><b> Earing Left Offset:</b></span><br>
                                                                <div class="form-group ">
                                                                    <label for="ear_left_v_position" class="float-left">V-Position</label>
                                                                    <input class="form-control" id="ear_left_v_position" name="ear_left_v_position" value= "@if(isset($meta_data['tryon_settings']['ear_left_v_position'])) {{ isset($meta_data['tryon_settings']['ear_left_v_position'])?old('ear_left_v_position',$meta_data['tryon_settings']['ear_left_v_position']):''}} @endif">
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="ear_left_h_position" class="float-left">H-Position</label>
                                                                    <input class="form-control" id="ear_left_h_position" name="ear_left_h_position" value= "@if(isset($meta_data['tryon_settings']['ear_left_h_position'])) {{ isset($meta_data['tryon_settings']['ear_left_h_position'])?old('ear_left_h_position',$meta_data['tryon_settings']['ear_left_h_position']):''}} @endif">
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="ear_left_size" class="float-left">Size</label>
                                                                    <input class="form-control" id="ear_left_size" name="ear_left_size" value= "@if(isset($meta_data['tryon_settings']['ear_left_size'])) {{ isset($meta_data['tryon_settings']['ear_left_size'])?old('ear_left_size',$meta_data['tryon_settings']['ear_left_size']):''}} @endif">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 text-center mb-2">
                                                        @if(!empty($ar_ear_right))
                                                            <h6>Earring Right</h6>
                                                            <div style="height:100px;object-fit: contain;text-align: -webkit-center;">
                                                                <img class="d-block img-fluid" style="min-width:90px; min-height:70px;max-height:80px;" src="{{$ar_ear_right->object->getUrl() ?? asset('assets/images/placholder.png')}}">
                                                            </div>
                                                            <button type="button" href="#" id="delete_img" onclick="removetryonimg({{$ar_ear_right->id}})" class="btn btn-danger btn-sm text-white"><i class="feather icon-trash-2 m-0"></i></button>
                                                        @else
                                                            <div style="height:120px;max-width: 400px;position:relative;margin-left:25%;margin-right:25%;display: none;" class="rounded mb-0 text-center" id="added_img_jwl-ear-r">
                                                                <h5>Earring Right try on image uploaded</h5>
                                                            </div>
                                                            <div style="cursor: pointer;height:120px;max-width: 400px;border-style:dashed;position:relative;margin-left:25%;margin-right:25%;" class="rounded mb-0 text-center" id="add_img_jwl-ear-r" data-position="jwl-ear-r" onclick="open_tryon_image_upload($(this))">
                                                                <div class="image mt-2">
                                                                    <i class="feather icon-plus-circle font-large-1"></i>
                                                                </div>
                                                                <span style="display: block;padding: 4px;">Upload Tryon earring right Image</span>
                                                            </div>
                                                        @endif
                                                        @if(app('impersonate')->isImpersonating())
                                                        <div class="row pl-1 pt-1">
                                                            <div class="col-lg-6 col-md-12">
                                                            <span class="float-left"><b> Earing Right Offset:</b></span><br>
                                                                <div class="form-group ">
                                                                    <label for="ear_right_v_position" class="float-left">V-Position</label>
                                                                    <input class="form-control" id="ear_right_v_position" name="ear_right_v_position" value= "@if(isset($meta_data['tryon_settings']['ear_right_v_position'])) {{ isset($meta_data['tryon_settings']['ear_right_v_position'])?old('ear_right_v_position',$meta_data['tryon_settings']['ear_right_v_position']):''}} @endif">
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="ear_right_h_position" class="float-left">H-Position</label>
                                                                    <input class="form-control" id="ear_right_h_position" name="ear_right_h_position" value= "@if(isset($meta_data['tryon_settings']['ear_right_h_position'])) {{ isset($meta_data['tryon_settings']['ear_right_h_position'])?old('ear_right_h_position',$meta_data['tryon_settings']['ear_right_h_position']):''}} @endif">
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="ear_right_size" class="float-left">Size</label>
                                                                    <input class="form-control" id="ear_right_size" name="ear_right_size" value= "@if(isset($meta_data['tryon_settings']['ear_right_size'])) {{ isset($meta_data['tryon_settings']['ear_right_size'])?old('ear_right_size',$meta_data['tryon_settings']['ear_right_size']):''}} @endif">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div><br>
                                                <div class="row">
                                                    <div class="col-lg-4 col-md-12 text-center mb-2">
                                                        @if(!empty($ar_watch))
                                                            <div style="height:100px;object-fit: contain;text-align: -webkit-center;">
                                                                <h6>Watch</h6>
                                                                <img class="d-block img-fluid" style="min-width:90px; min-height:70px;max-height:80px;" src="{{$ar_watch->object->getUrl() ?? asset('assets/images/placholder.png')}}">
                                                            </div>
                                                            <button type="button" href="#" id="delete_img" onclick="removetryonimg({{$ar_watch->id}})" class="btn btn-danger btn-sm text-white mt-1"><i class="feather icon-trash-2 m-0"></i></button>
                                                        @else
                                                            <div style="height:120px;max-width: 400px;position:relative;margin-left:25%;margin-right:25%;display: none;" class="rounded mb-0 text-center " id="added_img_watch">
                                                                <h5>Watch try on image uploaded</h5>
                                                            </div>
                                                            <div style="cursor: pointer;height:120px;max-width: 400px;border-style:dashed;position:relative;margin-left:25%;margin-right:25%;" class="rounded mb-0 text-center" id="add_img_watch" data-position="watch" onclick="open_tryon_image_upload($(this))">
                                                                <div class="image mt-2">
                                                                    <i class="feather icon-plus-circle font-large-1"></i>
                                                                </div>
                                                                <span style="display: block;padding: 4px;">Upload Tryon Watch Image</span>
                                                            </div>
                                                        @endif
                                                        @if(app('impersonate')->isImpersonating())
                                                        <div class="row pl-1 pt-1">
                                                            <div class="col-lg-6 col-md-12">
                                                            <span class="float-left"><b> Watch Offset:</b></span><br>
                                                                <div class="form-group ">
                                                                    <label for="watch_v_position" class="float-left">V-Position</label>
                                                                    <input class="form-control" id="watch_v_position" name="watch_v_position" value= "@if(isset($meta_data['tryon_settings']['watch_v_position'])) {{ isset($meta_data['tryon_settings']['watch_v_position'])?old('watch_v_position',$meta_data['tryon_settings']['watch_v_position']):''}} @endif">
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="watch_h_position" class="float-left">H-Position</label>
                                                                    <input class="form-control" id="watch_h_position" name="watch_h_position" value= "@if(isset($meta_data['tryon_settings']['watch_h_position'])) {{ isset($meta_data['tryon_settings']['watch_h_position'])?old('watch_h_position',$meta_data['tryon_settings']['watch_h_position']):''}} @endif">
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="watch_size" class="float-left">Size</label>
                                                                    <input class="form-control" id="watch_size" name="watch_size" value= "@if(isset($meta_data['tryon_settings']['watch_size'])) {{ isset($meta_data['tryon_settings']['watch_size'])?old('watch_size',$meta_data['tryon_settings']['watch_size']):''}} @endif">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 text-center mb-2">
                                                        @if(!empty($ar_ring))
                                                            <div style="height:100px;object-fit: contain;text-align: -webkit-center;">
                                                                <h6>Ring</h6>
                                                                <img class="d-block img-fluid" style="min-width:90px; min-height:70px;max-height:80px;" src="{{$ar_ring->object->getUrl() ?? asset('assets/images/placholder.png')}}">
                                                            </div>
                                                            <button type="button" href="#" id="delete_img" onclick="removetryonimg({{$ar_ring->id}})" class="btn btn-danger btn-sm text-white mt-1"><i class="feather icon-trash-2 m-0"></i></button>
                                                        @else
                                                            <div style="height:120px;max-width: 400px;position:relative;margin-left:25%;margin-right:25%;display: none;" class="rounded mb-0 text-center" id="added_img_ring">
                                                                <h5>Ring try on image uploaded</h5>
                                                            </div>
                                                            <div style="cursor: pointer;height:120px;max-width: 400px;border-style:dashed;position:relative;margin-left:25%;margin-right:25%;" class="rounded mb-0 text-center" id="add_img_ring" data-position="ring" onclick="open_tryon_image_upload($(this))">
                                                                <div class="image mt-2">
                                                                    <i class="feather icon-plus-circle font-large-1"></i>
                                                                </div>
                                                                <span style="display: block;padding: 4px;">Upload Tryon Ring Image</span>
                                                            </div>
                                                        @endif
                                                        @if(app('impersonate')->isImpersonating())
                                                        <div class="row pl-1 pt-1">
                                                            <div class="col-lg-6 col-md-12">
                                                            <span class="float-left"><b> Ring Offset:</b></span><br>
                                                                <div class="form-group ">
                                                                    <label for="ring_v_position" class="float-left">V-Position</label>
                                                                    <input class="form-control" id="ring_v_position" name="ring_v_position" value= "@if(isset($meta_data['tryon_settings']['ring_v_position'])) {{ isset($meta_data['tryon_settings']['ring_v_position'])?old('ring_v_position',$meta_data['tryon_settings']['ring_v_position']):''}} @endif">
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="ring_h_position" class="float-left">H-Position</label>
                                                                    <input class="form-control" id="ring_h_position" name="ring_h_position" value= "@if(isset($meta_data['tryon_settings']['ring_h_position'])) {{ isset($meta_data['tryon_settings']['ring_h_position'])?old('ring_h_position',$meta_data['tryon_settings']['ring_h_position']):''}} @endif">
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="ring_size" class="float-left">Size</label>
                                                                    <input class="form-control" id="ring_size" name="ring_size" value= "@if(isset($meta_data['tryon_settings']['ring_size'])) {{ isset($meta_data['tryon_settings']['ring_size'])?old('ring_size',$meta_data['tryon_settings']['ring_size']):''}} @endif">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 text-center mb-2">
                                                        @if(!empty($ar_bracelet))
                                                            <h6>bracelet</h6>
                                                            <div style="height:100px;object-fit: contain;text-align: -webkit-center;">
                                                                <img class="d-block img-fluid" style="min-width:90px; min-height:70px;max-height:80px;" src="{{$ar_bracelet->object->getUrl() ?? asset('assets/images/placholder.png')}}">
                                                            </div>
                                                            <button type="button" href="#" id="delete_img" onclick="removetryonimg({{$ar_bracelet->id}})" class="btn btn-danger btn-sm mt-1 text-white"><i class="feather icon-trash-2 m-0"></i></button>
                                                        @else
                                                            <div style="height:120px;max-width: 400px;position:relative;margin-left:25%;margin-right:25%;display: none;" class="rounded mb-0 text-center" id="added_img_bracelet">
                                                                <h5>bracelet</h5>
                                                            </div>
                                                            <div style="cursor: pointer;height:120px;max-width: 400px;border-style:dashed;position:relative;margin-left:25%;margin-right:25%;" class="rounded mb-0 text-center" id="add_img_bracelet" data-position="bracelet" onclick="open_tryon_image_upload($(this))">
                                                                <div class="image mt-2">
                                                                    <i class="feather icon-plus-circle font-large-1"></i>
                                                                </div>
                                                                <span style="display: block;padding: 4px;">bracelet</span>
                                                            </div>
                                                        @endif
                                                        @if(app('impersonate')->isImpersonating())
                                                        <div class="row pl-1 pt-1">
                                                            <div class="col-lg-6 col-md-12">
                                                            <span class="float-left"><b> Bracelet Offset:</b></span><br>
                                                                <div class="form-group ">
                                                                    <label for="bracelet_v_position" class="float-left">V-Position</label>
                                                                    <input class="form-control" id="bracelet_v_position" name="bracelet_v_position" value= "@if(isset($meta_data['tryon_settings']['bracelet_v_position'])) {{ isset($meta_data['tryon_settings']['bracelet_v_position'])?old('bracelet_v_position',$meta_data['tryon_settings']['bracelet_v_position']):''}} @endif">
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="bracelet_h_position" class="float-left">H-Position</label>
                                                                    <input class="form-control" id="bracelet_h_position" name="bracelet_h_position" value= "@if(isset($meta_data['tryon_settings']['bracelet_h_position'])) {{ isset($meta_data['tryon_settings']['bracelet_h_position'])?old('bracelet_h_position',$meta_data['tryon_settings']['bracelet_h_position']):''}} @endif">
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="bracelet_size" class="float-left">Size</label>
                                                                    <input class="form-control" id="bracelet_size" name="bracelet_size" value= "@if(isset($meta_data['tryon_settings']['bracelet_size'])) {{ isset($meta_data['tryon_settings']['bracelet_size'])?old('bracelet_size',$meta_data['tryon_settings']['bracelet_size']):''}} @endif">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                    </div>
                                                    <div class="col-lg-4 col-md-12 text-center mb-2">
                                                        @if(!empty($ar_apparel))
                                                            <h6>Apparel</h6>
                                                            <div style="height:100px;object-fit: contain;text-align: -webkit-center;">
                                                                <img class="d-block img-fluid" style="min-width:90px; min-height:70px;max-height:80px;" src="{{$ar_apparel->object->getUrl() ?? asset('assets/images/placholder.png')}}">
                                                            </div>
                                                            <button type="button" href="#" id="delete_img" onclick="removetryonimg({{$ar_apparel->id}})" class="btn btn-danger btn-sm text-white mt-1"><i class="feather icon-trash-2 m-0"></i></button>
                                                        @else
                                                            <div style="height:120px;max-width: 400px;position:relative;margin-left:25%;margin-right:25%;display: none;" class="rounded mb-0 text-center" id="added_img_apparel">
                                                                <h5>Apparel</h5>
                                                            </div>
                                                            <div style="cursor: pointer;height:120px;max-width: 400px;border-style:dashed;position:relative;margin-left:25%;margin-right:25%;" class="rounded mb-0 text-center" id="add_img_apparel" data-position="apparel" onclick="open_tryon_image_upload($(this))">
                                                                <div class="image mt-2">
                                                                    <i class="feather icon-plus-circle font-large-1"></i>
                                                                </div>
                                                                <span style="display: block;padding: 4px;">Apparel</span>
                                                            </div>
                                                        @endif
                                                        @if(app('impersonate')->isImpersonating())
                                                        <div class="row pl-1 pt-1">
                                                            <div class="col-lg-6 col-md-12">
                                                            <span class="float-left"><b> Apparel Offset:</b></span><br>
                                                                <div class="form-group ">
                                                                    <label for="apparel_v_position" class="float-left">V-Position</label>
                                                                    <input class="form-control" id="apparel_v_position" name="apparel_v_position" value= "@if(isset($meta_data['tryon_settings']['apparel_v_position'])) {{ isset($meta_data['tryon_settings']['apparel_v_position'])?old('apparel_v_position',$meta_data['tryon_settings']['apparel_v_position']):''}} @endif">
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="apparel_h_position" class="float-left">H-Position</label>
                                                                    <input class="form-control" id="apparel_h_position" name="apparel_h_position" value= "@if(isset($meta_data['tryon_settings']['apparel_h_position'])) {{ isset($meta_data['tryon_settings']['apparel_h_position'])?old('apparel_h_position',$meta_data['tryon_settings']['apparel_h_position']):''}} @endif">
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="apparel_size" class="float-left">Size</label>
                                                                    <input class="form-control" id="apparel_size" name="apparel_size" value= "@if(isset($meta_data['tryon_settings']['apparel_size'])) {{ isset($meta_data['tryon_settings']['apparel_size'])?old('apparel_size',$meta_data['tryon_settings']['apparel_size']):''}} @endif">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                @else
                                                <div class="row">
                                                    <div class="col-12 text-center">
                                                        <div class="alert alert-danger" role="alert">
                                                            You do not have this feature. Please upgrade to a plan that has AR Object feature.
                                                            <a href="{{route('admin.subscriptions.index')}}" class="btn btn-info btn-xs">Upgrade plan</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                            @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="card p-0" style="border: #B8C2CC;border-style: solid;">
                                        <div id="headingCollapse4" class="card-header p-1" data-toggle="collapse" role="button" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                            <span class="lead collapse-title"> 3D File </span>
                                        </div>
                                        <div id="collapse4" role="tabpanel" aria-labelledby="headingCollapse4" class="collapse" aria-expanded="false">
                                            <div class="card-body">
                                            @if(session('subscribed_features'))
                                                @if(array_key_exists("3d_object", session('subscribed_features')[0]))
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-12 text-center mb-2">
                                                        @if(!empty($ar_3d_file))
                                                            <div style="height:90px;object-fit: contain;text-align: -webkit-center;">
                                                                <p class="text-center">3D File Uploaded</p>
                                                                <button type="button" href="#" id="delete_img" onclick="remove3DFile({{$ar_3d_file->id}})" class="btn btn-danger btn-sm text-white"><i class="feather icon-trash-2 m-0"></i></button>
                                                            </div>
                                                        @else
                                                            <div class="rounded mb-0 text-center" id="file_3d_uploaded_android" style="height:110px;max-width:400px;position:relative;margin-left:30%;margin-right:30%;display: none;">
                                                                <h5>3D file uploaded</h5>
                                                            </div>
                                                            <div class="rounded mb-0 text-center" id="file_3d_upload_android" data-3d_type="android" onclick="open_3d_file_upload($(this))" style="cursor: pointer;height:110px;max-width: 400px;border-style:dashed;position:relative;margin-left:30%;margin-right:30%;">
                                                                <div class="image mt-2">
                                                                    <i  class="feather icon-plus-circle font-large-1"></i>
                                                                </div>
                                                                <span style="display: block;padding: 4px;">Upload 3D file</span>
                                                            </div>
                                                        @endif
                                                    </div>
                                                    <div class="col-lg-6 col-md-12 text-center mb-2">
                                                        @if(!empty($ar_3d_ios_file))
                                                            <div style="height:90px;object-fit: contain;text-align: -webkit-center;">
                                                                <p class="text-center">3D iOS File Uploaded</p>
                                                                <button type="button" href="#" id="delete_img" onclick="remove3DFile({{$ar_3d_ios_file->id}})" class="btn btn-danger btn-sm text-white"><i class="feather icon-trash-2 m-0"></i></button>
                                                            </div>
                                                        @else
                                                            <div class="rounded mb-0 text-center" id="file_3d_uploaded_iOS" style="height:110px;max-width:400px;position:relative;margin-left:30%;margin-right:30%;display: none;">
                                                                <h5>iOS 3D file uploaded</h5>
                                                            </div>
                                                            <div class="rounded mb-0 text-center" id="file_3d_upload_iOS" data-3d_type="iOS" onclick="open_3d_file_upload($(this))" style="cursor: pointer;height:110px;max-width: 400px;border-style:dashed;position:relative;margin-left:30%;margin-right:30%;">
                                                                <div class="image mt-2">
                                                                    <i  class="feather icon-plus-circle font-large-1"></i>
                                                                </div>
                                                                <span style="display: block;padding: 4px;">Upload 3D iOS file</span>
                                                            </div>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="row">
                                                        <div class="col-lg-3 col-md-3">
                                                            <div class="px-1 form-group">
                                                                <input class="form-check-input" type="checkbox" id="wall" name="wall" value="1" @if(!empty($ar_3d_file->position) && $ar_3d_file->position == 'wall'){{ old('wall',$ar_3d_file->position) ? 'checked="checked"' : '' }} @endif />
                                                                <label class="form-check-label" id="wall">WALL</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3">
                                                            <div class="px-1 form-group">
                                                                <input class="form-check-input" type="checkbox" id="disable_zoom" name="disable_zoom" value="1" @if(!empty($product_meta_data)){{ old('disable_zoom',$product_meta_data['disable_zoom']) ? 'checked="checked"' : '' }} @endif />
                                                                <label class="form-check-label" id="wall">Disable Zoom in/out function</label>
                                                            </div>
                                                        </div>
                                                    @can('custom_script')
                                                        <div class="col-lg-3 col-md-3">
                                                            <div class="px-1 form-group">
                                                                <input class="form-check-input" type="checkbox" id="custom_theme" name="custom_theme" value="1" @if(!empty($product_meta_data) && !empty($product_meta_data['custom_theme']) ){{ old('custom_theme',$product_meta_data['custom_theme']) ? 'checked="checked"' : '' }} @endif />
                                                                <label class="form-check-label" id="theme">Custom Theme</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12">
                                                            <div class="form-group">
                                                                <label for="scripthtml3dmodel">Script Html 3D Model</label>
                                                                <textarea style="height: 500px;" class="form-control {{ $errors->has('scripthtml3dmodel') ? 'is-invalid' : '' }}" name="scripthtml3dmodel" id="scripthtml3dmodel">{{ old('scripthtml3dmodel', $product->scripts->script_html ?? '') }}</textarea>
                                                                @if($errors->has('scripthtml3dmodel'))
                                                                    <div class="invalid-feedback">
                                                                        {{ $errors->first('scripthtml3dmodel') }}
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12">
                                                            <div class="form-group">
                                                                <label for="scriptcss3dmodel">Script Css 3D Model</label>
                                                                <textarea style="height: 500px;" class="form-control {{ $errors->has('scriptcss3dmodel') ? 'is-invalid' : '' }}" name="scriptcss3dmodel" id="scriptcss3dmodel">{{ old('scriptcss3dmodel', $product->scripts->script_css ?? '') }}</textarea>
                                                                @if($errors->has('scriptcss3dmodel'))
                                                                    <div class="invalid-feedback">
                                                                        {{ $errors->first('scriptcss3dmodel') }}
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-md-12">
                                                            <div class="form-group">
                                                                <label for="scriptjs3dmodel">Script Js 3D Model</label>
                                                                <textarea style="height: 500px;" class="form-control {{ $errors->has('scriptjs3dmodel') ? 'is-invalid' : '' }}" name="scriptjs3dmodel" id="scriptjs3dmodel">{{ old('scriptjs3dmodel', $product->scripts->script_js ?? '') }}</textarea>
                                                                @if($errors->has('scriptjs3dmodel'))
                                                                    <div class="invalid-feedback">
                                                                        {{ $errors->first('scriptjs3dmodel') }}
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    @endcan
                                                </div>
                                                @else
                                                <div class="row">
                                                    <div class="col-12 text-center">
                                                        <div class="alert alert-danger" role="alert">
                                                            You do not have this feature. Please upgrade to a plan that has 3D Object feature.
                                                            <a href="{{route('admin.subscriptions.index')}}" class="btn btn-info btn-xs">Upgrade plan</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif

                                            @endif

                                            </div>

                                        </div>
                                    </div>
                                    @can('show_document')
                                    <div class="card p-0" style="border: #B8C2CC;border-style: solid;">
                                        <div id="headingCollapse5" class="card-header p-1" data-toggle="collapse" role="button" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                            <span class="lead collapse-title"> Documents </span>
                                        </div>
                                        <div id="collapse5" role="tabpanel" aria-labelledby="headingCollapse5" class="collapse" aria-expanded="false">
                                            <div class="card-body">
                                                {{--@if(count($product->docs) > 0)
                                                    <div style="height:90px;object-fit: contain;text-align: -webkit-center;">
                                                        <p class="text-center">{{count($product->docs)}} Documents Uploaded</p>
                                                    </div>
                                                @else--}}
                                                <div class="rounded mb-0 text-center" id="product_docs_uploaded" style="height:110px;max-width: 400px;position:relative;margin-left:30%;margin-right:30%;display: none;">
                                                    <h5>Product documents uploaded</h5>
                                                </div>
                                                    <div class="rounded mb-0 text-center" id="product_docs_upload" data-toggle="modal" data-target="#productDocumentsUploadModal" style="cursor: pointer;height:110px;max-width: 400px;border-style:dashed;position:relative;margin-left:30%;margin-right:30%;">
                                                        <div class="image mt-2">
                                                            <i  class="feather icon-plus-circle font-large-1"></i>
                                                        </div>
                                                        <span style="display: block;padding: 4px;">Upload Documents</span>
                                                    </div>
                                                {{--@endif--}}
                                                @if(count($product->docs) > 0 )
                                                    <div class="row pt-1">
                                                    @foreach($product->docs as $doc)
                                                        <div  style="height:120px;width:90px;" class="text-center col-md-4">
                                                        <a href = "{{$doc->getUrl()}}" target="_blank">
                                                            <!-- <img class="d-flex rounded cursor-pointer" style="height:60px;width:90px;" src="{{$doc->getUrl() ?? asset('assets/images/document-placeholder.png')}}"></a> -->
                                                            <div> {{$doc->name}}</div>
                                                        </a>
                                                            <button href="#{{$doc->id}}" id="delete_img" onclick="removeDoc({{$doc->id}})" class="btn btn-danger btn-sm text-white mt-2"><i class="feather icon-trash-2 m-0"></i></button>
                                                        </div>
                                                    @endforeach
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    @endcan


                                    <div class="card p-0" style="border: #B8C2CC;border-style: solid;">
                                        <div id="headingCollapse6" class="card-header collapse-header p-1" data-toggle="collapse" role="button" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                                            <span class="lead collapse-title">Product Variants</span>
                                        </div>
                                        <div id="collapse6" role="tabpanel" aria-labelledby="headingCollapse6" class="collapse" aria-expanded="false">
                                            <div class="card-body">
                                                <div class="col-12">
                                                    <div class="row">
                                                        <div class="col-12 p-0">
                                                            <div class="form-group">
                                                                <button type="button" class="btn btn-primary" onclick="location.href='{{route('admin.productVariants.index',$product->id)}}'">Add variants</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @php
                                                        $product_variants = $product->product_variants;
                                                    @endphp
                                                    @if(count($product_variants) > 0)
                                                        <div class="row mt-1">
                                                            <div class="table-responsive">
                                                                <table class="table table-bordered table-hover datatable datatable-product-variants-collection">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Product variant</th>
                                                                            <th>Product name</th>
                                                                            <th>SKU</th>
                                                                            <th>Price</th>
                                                                            @if(session('subscribed_features'))
                                                                                @if(array_key_exists("inventory", session('subscribed_features')[0]))
                                                                                    @if(!empty($product->stock))
                                                                                        <th>Stock</th>
                                                                                    @endif
                                                                                    <th>Out of stock</th>
                                                                                @endif
                                                                            @endif
                                                                            <th>Minimum order quantity</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @foreach ($product_variants as $product_variant)
                                                                            @php
                                                                                $variants_list = json_decode($product_variant->variant_data, true);
                                                                            @endphp
                                                                            <tr>
                                                                                <td>
                                                                                @if(!empty($variants_list))
                                                                                    @foreach($variants_list as $variant)
                                                                                        @php
                                                                                            $variant_details = $product->getVariantName($variant["type"]);
                                                                                        @endphp
                                                                                        {{$variant_details->name}} - {{$variant["value"]}}
                                                                                        <br>
                                                                                    @endforeach
                                                                                @endif
                                                                                </td>
                                                                                <td>{{$product_variant->name}}</td>
                                                                                <td>{{$product_variant->sku}}</td>
                                                                                <td>{{$product_variant->price}}</td>
                                                                                @if(session('subscribed_features'))
                                                                                    @if(array_key_exists("inventory", session('subscribed_features')[0]))
                                                                                        @if(!empty($product_variant->stock))
                                                                                            <td>{{$product_variant->stock}}</td>
                                                                                        @endif
                                                                                        <td>{{$product_variant->out_stock}}</td>
                                                                                    @endif
                                                                                @endif
                                                                                <td>{{$product_variant->minimum_quantity}}</td>
                                                                                <td>
                                                                                <a class="btn btn-xs btn-primary btn-icon" href="{{route('admin.productVariants.show',$product_variant->id)}}">
                                                                                    <i class="feather icon-eye"></i>
                                                                                </a>
                                                                                    <button type="button" class="btn btn-primary btn-xs btn-icon" onclick="getProductVariantDetail({{$product_variant->id}})" data-toggle="modal" data-target="#productVariantEditorModal" style="background-color: #5E50EE !important;">
                                                                                        <i class="feather icon-edit"></i>
                                                                                    </button>
                                                                                    <a class="btn btn-xs btn-danger btn-icon"  href="{{route('admin.productVariants.destroy',$product_variant->id)}}">
                                                                                        <i class="feather icon-trash"></i>
                                                                                    </a>
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    <!-- </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 mb-1">
                    <div class="form-group">
                        <button class="btn btn-danger float-right" type="button" onclick="update_product($(this))" data-modal_name="productEditorModal">
                            {{ trans('global.save') }}
                        </button>
                    </div>
                </div>

            <!-- Product upload Modal -->
            <div class="modal fade ecommerce-application" id="productUploadimgModal" tabindex="-1" role="dialog" aria-labelledby="productSelectModalTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="productSelectModalTitle">Product Upload Image</h5>
                            <button type="button" class="close" onclick="close_product_image_upload()" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="dropzone dropzone-area {{ $errors->has('photo') ? 'is-invalid' : '' }}" id="productUploadimg2">
                                <div class="dz-message">Upload Product Image</div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" onclick="close_product_image_upload()" aria-label="Close">Cancel</button>
                            <button type="button" id="productImgSubmit" class="btn btn-primary" onclick="update_product($(this))" disabled="true" data-modal_name="productUploadimgModal">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Product Upload Modal -->

            <!-- AR upload Modal -->
            <div class="modal fade ecommerce-application" id="arUploadimgModal" tabindex="-1" role="dialog" aria-labelledby="arUploadimgModalTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="arUploadimgModalTitle">Tryon Upload Images</h5>
                            <button type="button" class="close" onclick="close_tryon_image_upload()" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="required" for="arUploadimg">Tryon Image</label>
                                <div class="dropzone dropzone-area {{ $errors->has('arUploadimg') ? 'is-invalid' : '' }}" id="arUploadimg" style="min-height: 305px;">
                                    <div class="dz-message">Upload Tryon Image</div>
                                </div>
                                @if($errors->has('arUploadimg'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('arUploadimg') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" onclick="close_tryon_image_upload()" aria-label="Close">Cancel</button>
                            <button type="button" id="arImgSubmit" onclick="update_product($(this))" class="btn btn-primary" disabled="true" data-modal_name="arUploadimgModal">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End AR Upload Modal -->

            <!-- 3D Image upload Modal -->
            <div class="modal fade ecommerce-application" id="file3dUploadModal" tabindex="-1" role="dialog" aria-labelledby="file3dUploadModalTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="file3dUploadModalTitle">3D Upload File</h5>
                            <button type="button" class="close" onclick="close_3d_file_upload()" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="dropzone dropzone-area {{ $errors->has('file3dUpload') ? 'is-invalid' : '' }}" id="file3dUpload" style="min-height: 305px;display: none;">
                                    <div class="dz-message">Upload 3D file</div>
                                </div>
                                <div class="dropzone dropzone-area {{ $errors->has('file3diOSUpload') ? 'is-invalid' : '' }}" id="file3diOSUpload" style="min-height: 305px;display: none;">
                                    <div class="dz-message">Upload 3D iOS file</div>
                                </div>
                                @if($errors->has('file3diOSUpload'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('file3diOSUpload') }}
                                    </div>
                                @endif
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" onclick="close_3d_file_upload()" aria-label="Close">Cancel</button>
                            <button type="button" id="file3dSubmit" onclick="update_product($(this))" class="btn btn-primary" disabled="true" data-modal_name="file3dUploadModal">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End 3D Image Upload Modal -->

            <!-- Documents upload Modal -->
            <div class="modal fade ecommerce-application" id="productDocumentsUploadModal" tabindex="-1" role="dialog" aria-labelledby="productDocumentsUploadModalTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="productDocumentsUploadModalTitle">Upload Documents</h5>
                            <button type="button" class="close" onclick="close_product_documents_upload()" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="dropzone dropzone-area {{ $errors->has('productDocumentsUpload') ? 'is-invalid' : '' }}" id="productDocumentsUpload" style="min-height: 305px;">
                                    <div class="dz-message">Upload Documents</div>
                                </div>
                                @if($errors->has('productDocumentsUpload'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('productDocumentsUpload') }}
                                    </div>
                                @endif
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" onclick="close_product_documents_upload()" aria-label="Close">Cancel</button>
                            <button type="button" id="productDocsSubmit" onclick="update_product($(this))" class="btn btn-primary" disabled="true" data-modal_name="productDocumentsUploadModal">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Documents Upload Modal -->
        </form>
    </div>

    <!-- Category add Modal -->
    <div class="modal fade ecommerce-application" id="categoryAddModal" tabindex="-1" role="dialog" aria-labelledby="categoryAddModalTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="categoryAddModalTitle">Add Category</h5>
                            <button type="button" class="close" onclick="close_category_modal()" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="card">
                                <div class="card-header">
                                    {{ trans('global.create') }} {{ trans('cruds.productCategory.title_singular') }}
                                </div>

                                <div class="card-body">
                                    <form method="POST" action="#" name="add_category_form" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group">
                                            <label class="required" for="name">{{ trans('cruds.productCategory.fields.name') }}</label>
                                            <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                                            @if($errors->has('name'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('name') }}
                                                </div>
                                            @endif
                                            <span class="help-block">{{ trans('cruds.productCategory.fields.name_helper') }}</span>
                                        </div>

                                        <div class="form-group">
                                            <label for="description">{{ trans('cruds.productCategory.fields.description') }}</label>
                                            <textarea class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" id="description">{{ old('description') }}</textarea>
                                            @if($errors->has('description'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('description') }}
                                                </div>
                                            @endif
                                            <span class="help-block">{{ trans('cruds.productCategory.fields.description_helper') }}</span>
                                        </div>

                                        <div class="form-group">
                                            <label for="photo">{{ trans('cruds.productCategory.fields.photo') }}</label>
                                            <div class="dropzone  dropzone-area{{ $errors->has('photo') ? 'is-invalid' : '' }}" id="photo-dropzone">
                                            </div>
                                            @if($errors->has('photo'))
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('photo') }}
                                                </div>
                                            @endif
                                            <span class="help-block">{{ trans('cruds.productCategory.fields.photo_helper') }}</span>
                                        </div>

                                        <div class="form-group">
                                            <label for="product_category_id">Parent Category</label>
                                            <select class="form-control" name="product_category_id">
                                                <option value="">Select a Parent Category</option>
                                                @foreach ($categories as $category)
                                                    <option value="{{ $category->id }}" {{ $category->id === old('product_category_id') ? 'selected' : '' }}>{{ $category->name }}</option>
                                                    @if ($category->childrenCategories)
                                                        @foreach ($category->childrenCategories as $child_category)
                                                            @include('admin.productCategories.childcategory', ['child_category' => $child_category,'level'=>0])
                                                        @endforeach
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <button class="btn btn-danger float-right" type="button" onclick="create_category()">
                                                {{ trans('global.save') }}
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        </div>
                </div>
            </div>
    <!--End Category add Modal -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>
<script>
        $('.arobjectPosition').editable({
            source: [
                @foreach(App\Arobject::POSITION_SELECT as $key => $label)
                {
                    value: '{{$key}}',
                    text: '{{$label}}'
                },
                @endforeach
            ]
        });

        var ar_position = "";
        var file_3d_type = "";

        function removetryonimg(id){
            if(confirm('{{ trans('global.areYouSure') }}')) {
                var url = "{{route('admin.arobjects.destroy',':id')}}";
                url = url.replace(':id', id);
                $.ajax({
                    url: url,
                    type: "post",
                    data: {'_method': 'DELETE'},
                }).done(function (data) {
                    alert('Removed try on image');
                    $('#productEditorModal').modal('hide');
                }).fail(function (jqXHR, ajaxOptions, thrownError) {
                    // alert('No response from server');
                });
            }
        }

        function remove3DFile(id){
            if(confirm('{{ trans('global.areYouSure') }}')) {
                var url = "{{route('admin.arobjects.destroy',':id')}}";
                url = url.replace(':id', id);
                $.ajax({
                    url: url,
                    type: "post",
                    data: {'_method': 'DELETE'},
                }).done(function (data) {
                    alert('Removed the 3D File');
                    $('#productEditorModal').modal('hide');
                }).fail(function (jqXHR, ajaxOptions, thrownError) {
                    // alert('No response from server');
                });
            }
        }

        function close_product_image_upload() {
            $('#productUploadimgModal').modal('hide');
        }

        function close_tryon_image_upload() {
            ar_position = "";
            $('#arUploadimgModal').modal('hide');
        }

        function open_3d_file_upload($this){
            file_3d_type = $this.data('3d_type');
            if(file_3d_type == "android"){
                $("#file3diOSUpload").hide();
                $("#file3dUpload").show();
            }
            else{
                $("#file3dUpload").hide();
                $("#file3diOSUpload").show();
            }
            $('#file3dUploadModal').modal('show');
        }

        function close_3d_file_upload() {
            $('#file3dUploadModal').modal('hide');
        }

        function close_product_documents_upload() {
            $('#productDocumentsUploadModal').modal('hide');
        }

        $('#productUploadimgModal').on('hidden.bs.modal', function() {
            // do something…
            $('#productEditorModal').modal('show');
        })

        function open_tryon_image_upload($this) {
            ar_position = $this.data('position');
            $('#arUploadimgModal').modal('show');
        }

        function close_category_modal(){
            $('#categoryAddModal').modal('hide');
        }

        function update_product($this){
            $("#description").val(quill.root.innerHTML);
            $('#loading-bg').show();
            console.log(myProductImageDropzone.getQueuedFiles());
            if (myProductImageDropzone.getUploadingFiles().length === 0 && myProductImageDropzone.getQueuedFiles().length === 0) {} else {
                myProductImageDropzone.processQueue();
            }
            if (myARimageDropzone.getUploadingFiles().length === 0 && myARimageDropzone.getQueuedFiles().length === 0) {} else {
                myARimageDropzone.processQueue();
            }
            if (myFile3dDropzone.getUploadingFiles().length === 0 && myFile3dDropzone.getQueuedFiles().length === 0) {} else {
                myFile3dDropzone.processQueue();
            }
            if (myFile3DiOSDropzone.getUploadingFiles().length === 0 && myFile3DiOSDropzone.getQueuedFiles().length === 0) {} else {
                myFile3DiOSDropzone.processQueue();
            }
            if (myProductDocumentsDropzone.getUploadingFiles().length === 0 && myProductDocumentsDropzone.getQueuedFiles().length === 0) {} else {
                myProductDocumentsDropzone.processQueue();
            }
            var modal_name = $this.data('modal_name');
            $('#'+ modal_name).modal('hide');
            var update_url = "{{ route('admin.products.update', ':product_id') }}";
            var product_id ="{{$product->id}}";
            update_url = update_url.replace(':product_id', product_id);
            $.ajax({
                url: update_url,
                type: "PUT",
                data: $('form[name=update_product_form]').serializeArray(),
            }).done(function (data) {
                // alert(data);
                if(modal_name == "arUploadimgModal"){
                    arImageDropzone.removeAllFiles();
                    $("#add_img_" + ar_position).hide();
                    $("#added_img_" + ar_position).show();
                    $('form[name=update_product_form]').find('input[name="try_on_object"]').remove();
                    $('form[name=update_product_form]').find('input[name="position"]').remove();
                    $('form[name=update_product_form]').find('input[name="try_on_type"]').remove();
                }
                else if(modal_name == "file3dUploadModal"){
                    file3dDropzone.removeAllFiles();
                    file3DiOSDropzone.removeAllFiles();
                    $("#file_3d_upload_" + file_3d_type).hide();
                    $("#file_3d_uploaded_" + file_3d_type).show();
                    $('form[name=update_product_form]').find('input[name="object_3d_ios"]').remove();
                    $('form[name=update_product_form]').find('input[name="object_3d"]').remove();
                    $('form[name=update_product_form]').find('input[name="type_3d"]').remove();
                }
                else if(modal_name == "productDocumentsUploadModal"){
                    productDocumentsDropzone.removeAllFiles();
                    $("#product_docs_upload").hide();
                    $("#product_docs_uploaded").show();
                    $('form[name=update_product_form]').find('input[name="docs[]"]').remove();
                }
                else{
                    console.log(data);

                    // var img_html ='<div data-target="#carousel-thumb_5" id="product_74" data-slide-to="1" class=" mr-1" style="height:120px;width:90px;"><img class="d-flex rounded cursor-pointer" style="height:60px;width:90px;" src="/storage/74/60cc6492299db_cou1.jpg"><button href="#74" id="delete_img" onclick="removeimg(74)" class="btn btn-danger btn-sm text-white mt-2"><i class="feather icon-trash-2 m-0"></i></button></div>';
                    // $('#productEditorModal').modal('show');
                    // $('.niceScroll').append(img_html);
                    var producturl = "{{ route('admin.products.product_list')}}/#updat-"+ product_id;
                    window.location.href = producturl;
                    location.reload();
                }
                // console.log(data);
                $('#loading-bg').hide();
                $(".modal-backdrop").hide();
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                $('#loading-bg').hide();
                console.log('No response from server');
            });
        }

        function create_category(){
            $('#loading-bg').show();
            $('#categoryAddModal').modal('hide');
            $('#productEditorModal').modal('hide');
            $.ajax({
                url: "{{ route('admin.product-categories.store') }}",
                type: "POST",
                data: $('form[name=add_category_form]').serializeArray(),
            }).done(function (data) {
                alert(data);
                $('#loading-bg').hide();
                $(".modal-backdrop").hide();
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                $('#loading-bg').hide();
                console.log('No response from server');
            });
        }


        if ($("#attribute").is(':checked')) {
            $("#attribute_list").show();
        } else {
            $("#attribute_list").hide();
        }



        $("#attribute").on('change', function() {
            if ($("#attribute").is(':checked')) {
                $("#attribute_list").show();
            } else {
                $("#attribute_list").hide();
            }
        });

        $('.attr_radiogroup').on('click', function() {

            id = $(this).data("id")
            $('.attr_radiobtn' + id).prop('checked', false);
        });


        if ($("#discount_rate").is(':checked')) {
            $("#discount").show();
            $('#disc_per').show();
            $('#ofr_disc').hide();
        } else {
            $("#discount").hide();
            $('#ofr_disc').show();
            $('#disc_per').hide();
        }

        $("#discount_rate").on('change', function() {
            if ($("#discount_rate").is(':checked')) {
                $("#discount").show();
                $('#disc_per').show();
                $('#ofr_disc').hide();
            } else {
                $("#discount").hide();
                $('#ofr_disc').show();
                $('#disc_per').hide();
            }

        });
        if ($("#stock_qty").is(":checked")) {
            $("#stock").show();
            $("#stock_qt").show();
            $("#manage_inv").hide();
        } else {
            $("#stock").hide();
            $("#stock_qt").hide();
            $("#manage_inv").show();
        }

        $("#stock_qty").on('change', function() {
            if ($(this).is(":checked")) {
                $("#stock").show();
                $("#stock_qt").show();
                $("#manage_inv").hide();
            } else {
                $("#stock").hide();
                $("#stock_qt").hide();
                $("#manage_inv").show();
            }
        });

        $("#discount").on('input', function() {
            var num = $(this).val();
            if ($(this).val() > 100) {
                alert("No numbers above 100");
                $(this).val('100');
            }
            // var format = /^[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*$/;

        });
        var skuvalue ='';
        $('#sku').on('focusin',function(){
             skuvalue = $('#sku').val();
        });
        $('#sku').on('focusout',function(){
            var sku = $('#sku').val();
            var product_id ="{{$product->id}}";
            if(sku == ''){
                alert('sku is required field');
                $('#sku').val(skuvalue);
            }
            else{
            $.ajax({
                type: "POST",
                url: "{{route('admin.products.sku')}}",
                data: '&sku=' + sku + '&productid=' + product_id,
                success: function(data) {
                    if(data.isexist == 1){
                    alert("Sku is already used!");
                    }
                    $('#sku').val(data.productsku);
                }
            });
            }
        });

        // $("#discount").ForceNumericOnly().show();
        // $('#discount').keypress(function(event) {
        //     var num = $(this).val();
        //     var format =/^[0-9]/;
        //         if(format.test(num)){
        //         // alert("only number is allowed");
        //         // $(this).val('');
        //         }
        //         else{
        //             alert('only number can filled');
        //         }
        // });
        function isNumberKey(evt, element) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57) && !(charCode == 46 || charCode == 8))
                return false;
            else {
                var len = $(element).val().length;
                var index = $(element).val().indexOf('.');
                if (index > 0 && charCode == 46) {
                    return false;
                }
                if (index > 0) {
                    var CharAfterdot = (len + 1) - index;
                    if (CharAfterdot > 3) {
                        return false;
                    }
                }
            }
            return true;
        }
        var quill = new Quill('#editor-container', {
            modules: {
                'toolbar': [
                    [{
                        'font': []
                    }, {
                        'size': []
                    }]
                    , ['bold', 'italic', 'underline', 'strike']
                    , [{
                        'color': []
                    }, {
                        'background': []
                    }]
                    , [{
                        'script': 'super'
                    }, {
                        'script': 'sub'
                    }]
                    , [{
                        'header': '1'
                    }, {
                        'header': '2'
                    }, 'blockquote', 'code-block']
                    , [{
                        'list': 'ordered'
                    }, {
                        'list': 'bullet'
                    }, {
                        'indent': '-1'
                    }, {
                        'indent': '+1'
                    }]
                    , ['direction', {
                        'align': []
                    }]
                    , ['link', 'image', 'video', 'formula']
                    , ['clean']
                ]
            , }
            , placeholder: 'description...'
            , theme: 'snow'
        });
        quill.root.innerHTML = $("#description").val();

        var uploadedPhotoMap = {}
        var ProductImageDropzone = '';
        Dropzone.autoDiscover = false;
        productImageDropzone = new Dropzone('#productUploadimg2', {
            autoProcessQueue: false,
            parallelUploads: "{{ session('subscription_usage_remaining') ?? 50 }}",
            url: '{{ route('admin.products.storeMedia')}}',
            maxFilesize : 40,
            acceptedFiles: '.jpeg,.jpg,.png,.gif',
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            params: {
                size: 40
            },
            success: function(file, response) {
                console.log("uploaded",response)

                $('form').append('<input type="hidden" name="photo[]" value="' + response.name + '">')
            },
            removedfile: function (file) {
                file.previewElement.remove()
                var name = ''
                if (typeof file.file_name !== 'undefined') {
                    name = file.file_name
                } else {
                    name = uploadedPhotoMap[file.name]
                }
                $('form').find('input[name="photo[]"][value="' + name + '"]').remove()
            },
            init: function () {
            console.log('enter');

                // var submitButton = document.querySelector("#productImgSubmit");
                myProductImageDropzone = this; // closure
                // submitButton.addEventListener("click", function() {
                //     if (myProductImageDropzone.getUploadingFiles().length === 0 && myProductImageDropzone.getQueuedFiles().length === 0) {} else {
                //         myProductImageDropzone.processQueue();
                //     }
                // });
            this.on("addedfile", file => {
                console.log("upload");
                console.log(this.getQueuedFiles());
                if(this.getQueuedFiles().length >= 0)
                    $("#productImgSubmit").prop("disabled",false)
                else
                    $("#productImgSubmit").prop("disabled",true)
            });
            this.on("removedfile", file => {
                console.log("upload removed")
                if(this.getQueuedFiles().length >= 0)
                    $("#productImgSubmit").prop("disabled",false)
                else
                    $("#productImgSubmit").prop("disabled",true)
            });
                this.on("dragend, processingmultiple", function(file) {
                    $("#productImgSubmit").prop('disabled', true);
                });
                this.on("queuecomplete", function(file) {
            console.log('complete');
                    // if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                    //     $("#productImgSubmit").prop('disabled', false);
                    // } else {
                    //     $("#productImgSubmit").prop('disabled', true);
                    // }
                    uploadnewproduct('productUploadimgModal');

                })
            },
            error: function (file, response) {
                if ($.type(response) === 'string') {
                    var message = response //dropzone sends it's own error messages in string
                } else {
                    var message = response.errors.file
                }
                file.previewElement.classList.add('dz-error')
                _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
                _results = []
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    node = _ref[_i]
                    _results.push(node.textContent = message)
                }
                return _results
            }
        });

        arImageDropzone = new Dropzone('#arUploadimg', {

            autoProcessQueue: false,
            parallelUploads: "{{ session('subscription_usage_remaining') ?? 50 }}",
            url: '{{ route('admin.arobjects.storeMedia') }}',
            maxFilesize: 40, // MB
            maxFiles: 1,
            acceptedFiles: '.png',
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            params: {
                size: 40
            },
            success: function (file, response) {
                $('form').find('input[name="try_on_object"]').remove()
                $('form').append('<input type="hidden" name="try_on_object" value="' + response.name + '">')
                $('form').append('<input type="hidden" name="position" value="' + ar_position + '">')
                $('form').append('<input type="hidden" name="try_on_type" value="face">')
            },
            removedfile: function (file) {
                file.previewElement.remove()
                if (file.status !== 'error') {
                    $('form').find('input[name="try_on_object"]').remove()
                    this.options.maxFiles = this.options.maxFiles + 1
                }
            },
            init: function () {
                // var submitButton = document.querySelector("#arImgSubmit");
                myARimageDropzone = this; // closure
                // submitButton.addEventListener("click", function() {
                //     if (myARimageDropzone.getUploadingFiles().length === 0 && myARimageDropzone.getQueuedFiles().length === 0) {} else {
                //         myARimageDropzone.processQueue();
                //     }
                // });
                this.on("addedfile", file => {
                console.log("upload");
                console.log(this.getQueuedFiles());
                if(this.getQueuedFiles().length >= 0)
                    $("#arImgSubmit").prop("disabled",false)
                else
                    $("#arImgSubmit").prop("disabled",true)
                });
                this.on("removedfile", file => {
                    console.log("upload removed")
                    if(this.getQueuedFiles().length >= 0)
                        $("#arImgSubmit").prop("disabled",false)
                    else
                        $("#arImgSubmit").prop("disabled",true)
                });
                this.on("dragend, processingmultiple", function(file) {
                    $("#arImgSubmit").prop('disabled', true);
                });
                this.on("queuecomplete", function(file) {
                    console.log('complete')
                    // if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                    //     $("#arImgSubmit").prop('disabled', false);
                    // } else {
                    //     $("#arImgSubmit").prop('disabled', true);
                    // }
                    uploadnewproduct('arUploadimgModal');
                })
            },
            error: function (file, response) {
                if ($.type(response) === 'string') {
                    var message = response //dropzone sends it's own error messages in string
                } else {
                    var message = response.errors.file
                }
                file.previewElement.classList.add('dz-error')
                _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
                _results = []
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    node = _ref[_i]
                    _results.push(node.textContent = message)
                }

                 return _results
            }
        });

        file3dDropzone = new Dropzone('#file3dUpload', {
            autoProcessQueue: false,
            parallelUploads: "{{ session('subscription_usage_remaining') ?? 50 }}",
            url: '{{ route('admin.arobjects.storeMedia') }}',
            maxFilesize: 150, // MB
            maxFiles: 1,
            // acceptedFiles: '.glb,.fbx,.zip.vrx,.gltf',
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            success: function (file, response) {
                $('form').find('input[name="object_3d"]').remove()
                $('form').append('<input type="hidden" name="object_3d" value="' + response.name + '">')
                $('form').append('<input type="hidden" name="type_3d" value="surface">')
            },
            removedfile: function (file) {
                file.previewElement.remove()
                if (file.status !== 'error') {
                    $('form').find('input[name="object_3d"]').remove()
                    this.options.maxFiles = this.options.maxFiles + 1
                }
            },
            init: function () {
                // var submitButton = document.querySelector("#file3dSubmit");
                myFile3dDropzone = this; // closure
                // submitButton.addEventListener("click", function() {
                //     if (myFile3dDropzone.getUploadingFiles().length === 0 && myFile3dDropzone.getQueuedFiles().length === 0) {} else {
                //         myFile3dDropzone.processQueue();
                //     }
                // });
                this.on("addedfile", file => {
                console.log("upload");
                console.log(this.getQueuedFiles());
                if(this.getQueuedFiles().length >= 0)
                    $("#file3dSubmit").prop("disabled",false)
                else
                    $("#file3dSubmit").prop("disabled",true)
                });
                this.on("removedfile", file => {
                    console.log("upload removed")
                    if(this.getQueuedFiles().length >= 0)
                        $("#file3dSubmit").prop("disabled",false)
                    else
                        $("#file3dSubmit").prop("disabled",true)
                });
                this.on("dragend, processingmultiple", function(file) {
                    $("#file3dSubmit").prop('disabled', true);
                });
                this.on("queuecomplete", function(file) {
                    // if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                    //     $("#file3dSubmit").prop('disabled', false);
                    // } else {
                    //     $("#file3dSubmit").prop('disabled', true);
                    // }
                    uploadnewproduct('file3dUploadModal');

                })
            },
            error: function (file, response) {
                if ($.type(response) === 'string') {
                    var message = response //dropzone sends it's own error messages in string
                } else {
                    var message = response.errors.file
                }
                file.previewElement.classList.add('dz-error')
                _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
                _results = []
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    node = _ref[_i]
                    _results.push(node.textContent = message)
                }

                 return _results
            }
        });

        file3DiOSDropzone = new Dropzone('#file3diOSUpload', {
            autoProcessQueue: false,
            parallelUploads: "{{ session('subscription_usage_remaining') ?? 50 }}",
            url: '{{ route('admin.arobjects.storeMedia') }}',
            maxFilesize: 150, // MB
            maxFiles: 1,
            acceptedFiles: '.usdz',
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            params: {
                size: 150
            },
            success: function (file, response) {
                $('form').find('input[name="object_3d_ios"]').remove()
                $('form').append('<input type="hidden" name="object_3d_ios" value="' + response.name + '">')
                $('form').append('<input type="hidden" name="type_3d" value="surface">')
            },
            removedfile: function (file) {
                file.previewElement.remove()
                if (file.status !== 'error') {
                    $('form').find('input[name="object_3d_ios"]').remove()
                    this.options.maxFiles = this.options.maxFiles + 1
                }
            },
            init: function () {
                // var submitButton = document.querySelector("#file3dSubmit");
                myFile3DiOSDropzone = this; // closure
                // submitButton.addEventListener("click", function() {
                //     if (myFile3DiOSDropzone.getUploadingFiles().length === 0 && myFile3DiOSDropzone.getQueuedFiles().length === 0) {} else {
                //         myFile3DiOSDropzone.processQueue();
                //     }
                // });
                this.on("addedfile", file => {
                console.log("upload");
                console.log(this.getQueuedFiles());
                if(this.getQueuedFiles().length >= 0)
                    $("#file3dSubmit").prop("disabled",false)
                else
                    $("#file3dSubmit").prop("disabled",true)
                });
                this.on("removedfile", file => {
                    console.log("upload removed")
                    if(this.getQueuedFiles().length >= 0)
                        $("#file3dSubmit").prop("disabled",false)
                    else
                        $("#file3dSubmit").prop("disabled",true)
                });
                this.on("dragend, processingmultiple", function(file) {
                    $("#file3dSubmit").prop('disabled', true);
                });
                this.on("queuecomplete", function(file) {
                    // if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                    //     $("#file3dSubmit").prop('disabled', false);
                    // } else {
                    //     $("#file3dSubmit").prop('disabled', true);
                    // }
                    uploadnewproduct('file3dUploadModal');
                })
            },
            error: function (file, response) {
                if ($.type(response) === 'string') {
                    var message = response //dropzone sends it's own error messages in string
                } else {
                    var message = response.errors.file
                }
                file.previewElement.classList.add('dz-error')
                _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
                _results = []
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    node = _ref[_i]
                    _results.push(node.textContent = message)
                }

                 return _results
            }
        });

        productDocumentsDropzone = new Dropzone('#productDocumentsUpload', {
            autoProcessQueue: false,
            parallelUploads: "{{ session('subscription_usage_remaining') ?? 50 }}",
            url: '{{ route('admin.arobjects.storeMedia') }}',
            maxFilesize: 15, // MB
            maxFiles: 5,
            acceptedFiles: '',
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            params: {
                size: 40
            },
            success: function (file, response) {
                $('form').append('<input type="hidden" name="docs[]" value="' + response.name + '">')
            },
            removedfile: function (file) {
                file.previewElement.remove()
                var name = ''
                if (typeof file.file_name !== 'undefined') {
                    name = file.file_name
                } else {
                    name = uploadedPhotoMap[file.name]
                }
                $('form').find('input[name="docs[]"][value="' + name + '"]').remove()
            },
            init: function () {
                // var submitButton = document.querySelector("#productDocsSubmit");
                myProductDocumentsDropzone = this; // closure
                // submitButton.addEventListener("click", function() {
                //     if (myProductDocumentsDropzone.getUploadingFiles().length === 0 && myProductDocumentsDropzone.getQueuedFiles().length === 0) {} else {
                //         myProductDocumentsDropzone.processQueue();
                //     }
                // });
                this.on("addedfile", file => {
                console.log("upload");
                console.log(this.getQueuedFiles());
                if(this.getQueuedFiles().length >= 0)
                    $("#productDocsSubmit").prop("disabled",false)
                else
                    $("#productDocsSubmit").prop("disabled",true)
                });
                this.on("removedfile", file => {
                    console.log("upload removed")
                    if(this.getQueuedFiles().length >= 0)
                        $("#productDocsSubmit").prop("disabled",false)
                    else
                        $("#productDocsSubmit").prop("disabled",true)
                });
                this.on("dragend, processingmultiple", function(file) {
                    $("#productDocsSubmit").prop('disabled', true);
                });
                this.on("queuecomplete", function(file) {
                    // if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                    //     $("#productDocsSubmit").prop('disabled', false);
                    // } else {
                    //     $("#productDocsSubmit").prop('disabled', true);
                    // }
                    uploadnewproduct('productDocumentsUploadModal');

                })
            },
            error: function (file, response) {
                if ($.type(response) === 'string') {
                    var message = response //dropzone sends it's own error messages in string
                } else {
                    var message = response.errors.file
                }
                file.previewElement.classList.add('dz-error')
                _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
                _results = []
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    node = _ref[_i]
                    _results.push(node.textContent = message)
                }

                 return _results
            }
        });

        categoryDropzone = new Dropzone('#photo-dropzone', {
            url: '{{ route('admin.product-categories.storeMedia') }}',
            // maxFilesize: 2, // MB
            acceptedFiles: '.jpeg,.jpg,.png,.gif',
            maxFiles: 1,
            addRemoveLinks: true,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            params: {
                size: 2,
                width: 4086,
                height: 4096
            },
            success: function (file, response) {
                $('form').find('input[name="photo"]').remove()
                $('form').append('<input type="hidden" name="photo" value="' + response.name + '">')
            },
            removedfile: function (file) {
                file.previewElement.remove()
                if (file.status !== 'error') {
                    $('form').find('input[name="photo"]').remove()
                    this.options.maxFiles = this.options.maxFiles + 1
                }
            },
            init: function () {
                @if(isset($productCategory) && $productCategory->photo)
                    var file = {!! json_encode($productCategory->photo) !!}
                    this.options.addedfile.call(this, file)
                    this.options.thumbnail.call(this, file, '{{ $productCategory->photo->getUrl('thumb') }}')
                    file.previewElement.classList.add('dz-complete')
                    $('form').append('<input type="hidden" name="photo" value="' + file.file_name + '">')
                    this.options.maxFiles = this.options.maxFiles - 1
                @endif
            },
            error: function (file, response) {
                if ($.type(response) === 'string') {
                    var message = response //dropzone sends it's own error messages in string
                } else {
                    var message = response.errors.file
                }
                file.previewElement.classList.add('dz-error')
                _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
                _results = []
                for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                    node = _ref[_i]
                    _results.push(node.textContent = message)
                }

                return _results
            }
        });

        $('.carousel-thumb').carousel({
            interval: 2000
        });
        //$("button").click(() => $(".carousel").carousel("next"));

        $(function () {
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons);
            $.extend(true, $.fn.dataTable.defaults, {
                pageLength: 10,
            });

            $('.datatable-product-variants-collection:not(.ajaxTable)').DataTable({ buttons: dtButtons });
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
            });
        });
        function uploadnewproduct(modalname) {

            var modal_name = modalname;
            $('#'+ modal_name).modal('hide');
            var update_url = "{{ route('admin.products.update', ':product_id') }}";
            var product_id ="{{$product->id}}";
            update_url = update_url.replace(':product_id', product_id);
            $.ajax({
                url: update_url,
                type: "PUT",
                data: $('form[name=update_product_form]').serializeArray(),
            }).done(function (data) {
                if(modal_name == "arUploadimgModal"){
                    arImageDropzone.removeAllFiles();
                    $("#add_img_" + ar_position).hide();
                    $("#added_img_" + ar_position).show();
                    $('form[name=update_product_form]').find('input[name="try_on_object"]').remove();
                    $('form[name=update_product_form]').find('input[name="position"]').remove();
                    $('form[name=update_product_form]').find('input[name="try_on_type"]').remove();
                }
                else if(modal_name == "file3dUploadModal"){
                    file3dDropzone.removeAllFiles();
                    file3DiOSDropzone.removeAllFiles();
                    $("#file_3d_upload_" + file_3d_type).hide();
                    $("#file_3d_uploaded_" + file_3d_type).show();
                    $('form[name=update_product_form]').find('input[name="object_3d_ios"]').remove();
                    $('form[name=update_product_form]').find('input[name="object_3d"]').remove();
                    $('form[name=update_product_form]').find('input[name="type_3d"]').remove();
                }
                else if(modal_name == "productDocumentsUploadModal"){
                    productDocumentsDropzone.removeAllFiles();
                    $("#product_docs_upload").hide();
                    $("#product_docs_uploaded").show();
                    $('form[name=update_product_form]').find('input[name="docs[]"]').remove();
                    location.reload();
                }
                else{
                    console.log(data);

                    // var img_html ='<div data-target="#carousel-thumb_5" id="product_74" data-slide-to="1" class=" mr-1" style="height:120px;width:90px;"><img class="d-flex rounded cursor-pointer" style="height:60px;width:90px;" src="/storage/74/60cc6492299db_cou1.jpg"><button href="#74" id="delete_img" onclick="removeimg(74)" class="btn btn-danger btn-sm text-white mt-2"><i class="feather icon-trash-2 m-0"></i></button></div>';
                    // $('#productEditorModal').modal('show');
                    // $('.niceScroll').append(img_html);
                    var producturl = "{{ route('admin.products.product_list')}}/#updat-"+ product_id;
                    window.location.href = producturl;
                    location.reload();


                }
                $('#loading-bg').hide();
                $(".modal-backdrop").hide();
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                $('#loading-bg').hide();
                console.log('No response from server');
            });

        }
</script>
