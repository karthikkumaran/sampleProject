<html>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<body>
    <button type="button" class="btn btn-icon rounded-circle mr-1 mb-1 text-success share-icon share-button" data-share="whatsapp"><i class="fa fa-whatsapp"></i><br><br>Share</button>

</body>

</html>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>
    $(".share-button").click(function() {

        $.ajax({
            url: "{{route('admin.campaigns.shareCatalogData')}}",
            method: "get",
            data: {
                key: 'e00714aa01e842cfb50b9ae0d7bee624',
            },
            success: function(data) {
                t = data.data;
                var share_config = {
                    title: 'Simplisell',
                    text: t[0],
                    url: "www.simplisell.co"
                };

                if (/iPhone|iPad|iPod|Android/i.test(navigator.userAgent)) {
                    if (navigator.canShare) {
                        navigator.share(
                                share_config
                            ).then(() => {
                                // console.log("thanks ");
                                // window.alert("hello");
                            })
                            .catch(console.error);
                    } else {
                        window.alert("error");
                    }
                }

            }
        })
    });
</script>