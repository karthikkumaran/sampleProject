@extends('layouts.store_onboard')
@section('content')
    @php
        $annual = false;
        $subscribe = "subscribe";
        $user = auth()->user();
    @endphp
    @if ( app('impersonate')->isImpersonating())
    <div class="row mt-1 mb-1">
        <div class="col-12" style="text-align:center; vertical-align:middle;">
        <span class="text-primary">Click here for Admin Payment</span><br>
        <a class="btn btn-md btn-primary " style="color:white; " onclick="adminRenewModalShow({{$user->id}})">
                                            Admin Pay
                                        </a>
        </div>
    </div>
    @endif
    <div class="row mt-1 mb-1">
        <div class="col-12" style="text-align:center; vertical-align:middle;">
            <div class="custom-control custom-switch custom-switch-success">
                <span class="switch-label mr-1"><b>Monthly</b></span>
                <input type="checkbox" class="custom-control-input" id="subscription_interval" name="subscription_interval" {{ ($annual == true)? "checked" : ""}}>
                <label class="custom-control-label" for="subscription_interval"></label>
                <span class="switch-label"><b>Yearly</b></span>
            </div>
        </div>
    </div>
    <div class="row pricing-card">
        <div class="col-12 col-sm-offset-2 col-sm-10 col-md-12 col-lg-offset-2 col-lg-10 mx-auto" id="subscriptions_page">
            <div class="row" id="subscriptions_show">
                <!-- pricing plan cards -->
                @foreach($packages as $key => $package)
                    @php
                        $features = $package->features;
                    @endphp

                    @if($package->slug == "user-trial")
                        @continue
                    @endif
    
                    @if($package->invoice_interval == "month")
                        <div class="col-12 col-md-4 monthly_package" style="display:show">
                    @elseif($package->invoice_interval == "year")
                        <div class="col-12 col-md-4 yearly_package" style="display:none">
                    @else
                        <div class="col-12 col-md-4">
                    @endif
                            <div class="card basic-pricing text-center">
                                <div class="card-body">
                                    <h3>{!! $package->name !!}</h3>
                                    <p class="card-text">{!! $package->description !!}</p>
                                    <div class="annual-plan">
                                        <div class="plan-price mt-2">
                                            <span class="font-medium-1 font-weight-bold text-primary">{!! $package->currency !!}</span>
                                            <span class="pricing-basic-value font-weight-bolder text-primary">{!! $package->price !!}</span>
                                        </div>
                                        <small class="annual-pricing d-none text-muted"></small>
                                    </div>
                                    <ul class="list-group list-group-circle text-left">
                                        @foreach($features as $key => $feature)
                                            @php
                                                $feature_details = $feature->getFeatureDetails();
                                            @endphp
                                            <li class="list-group-item text-center">
                                                {!! $feature_details->name !!} - <b>{!! $feature->value !!}</b>
                                                <i id="feature_description_show_{{$package->id}}_{{$feature->feature_id}}" class="fa fa-angle-double-down float-right" onclick="feature_description_show({{$package->id}},{{$feature->feature_id}})" style="display:block;cursor:pointer"></i>
                                                <i id="feature_description_hide_{{$package->id}}_{{$feature->feature_id}}" class="fa fa-angle-double-up float-right" onclick="feature_description_hide({{$package->id}},{{$feature->feature_id}})" style="display:none;cursor:pointer"></i>
                                                <li class="list-group-item text-center text-dark" id="feature_description_{{$package->id}}_{{$feature->feature_id}}" style="display:none">
                                                    {!! $feature_details->description !!}
                                                </li>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <button class="btn btn-block btn-outline-success mt-2" onclick="window.location='{{ route('admin.subscription.newUserShow', $package->id) }}'">Subscribe</button>
                                </div>
                            </div>
                        </div>
                @endforeach
                <!--/ pricing plan cards -->
            </div>
        </div>
    </div>
    @include('modals.user_subscription')
    @include('modals.admin_renewal_pay')
@endsection
@section('scripts')
@parent
<script>
    function adminRenewModalShow(id){
        $("#adminRenewalPay").modal('show');
        userId(id);
        console.log(id);
    }
    if($('#subscription_interval').is(':checked') == true){
        $('.yearly_package').show();
        $('.monthly_package').hide();
    }
    else{
        $('.monthly_package').show();
        $('.yearly_package').hide();
    }

    $("#subscription_interval").change(function(){
        var x = $("#subscription_interval").is(":checked");
        if(this.checked){
            $('.yearly_package').show();
            $('.monthly_package').hide();
        }
        else{
            $('.monthly_package').show();
            $('.yearly_package').hide();
        }
    });

    function feature_description_show(package_id,feature_id){
        $('#feature_description_show_' + package_id + '_' + feature_id).hide();
        $('#feature_description_hide_' + package_id + '_' + feature_id).show();
        $('#feature_description_' + package_id + '_' + feature_id).show();
    }

    function feature_description_hide(package_id,feature_id){
        $('#feature_description_show_' + package_id + '_' + feature_id).show();
        $('#feature_description_hide_' + package_id + '_' + feature_id).hide();
        $('#feature_description_' + package_id + '_' + feature_id).hide();
    }
</script>
@endsection