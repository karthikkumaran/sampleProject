@extends('layouts.admin')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        @php
            if($plan->price > $plan->discount)
                $plan_discount = $plan->price - $plan->discount;
            else
                $plan_discount = $plan->price; 
        @endphp
                
            <div class="">
                @if($plan->discount!= 0)
                <p>You will be charged {{$plan->currency}} {{ number_format($plan_discount, 2) }} for {{ $plan->name }} Plan</p>
                @else
                <p>You will be charged {{$plan->currency}} {{ number_format($plan->price, 2) }} for {{ $plan->name }} Plan</p>
                @endif
            </div>
            <div class="card">
                <div class="card-header">
                    <div id="upgrade_alert_msg" style="display: none">
                        <label for="card-element">
                            <h5><b>Once your payment is done, your plan upgrade will take place with immediate effect.</b></h5>
                        </label>
                    </div>
                    <div id="downgrade_alert_msg" style="display: none">
                        <label for="card-element">
                            <h5><b>Your plan will be changed(downgraded) once your current plan expires.</b></h5>
                        </label>
                    </div>
                </div>
                <div class="card-body">
                    <label for="card-element">
                        Choose your payment method
                    </label>
                    <img class="card text-center cursor-pointer border p-2 payment_method" id="razorpay_payment" src="{{ asset('XR\app-assets\images\icons\razorpay_logo.svg') }}" style="width:150px;">
                    <input type="hidden" name="plan" value="{{ $plan->id }}" />
                </div>
                <div class="card-footer">
                    <button id="subscription_payment" class="btn btn-primary float-right" disabled> Pay </button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    var plan_id = "{{ $plan->id }}";
    var subscription_url = "{{route('admin.subscription.create')}}";
    var subscription = "{{$subscription}}";
    switch(subscription){
        case 'subscribe':
            subscription_url = "{{route('admin.subscription.create')}}";
            // console.log("susbcribe",subscription_url);
            break;
        case 'upgrade':
            $("#downgrade_alert_msg").hide();
            $("#upgrade_alert_msg").show();
            subscription_url = "{{route('admin.subscription.upgradePlan')}}";
            // console.log("upgrade",subscription_url);
            break;
        case 'downgrade':
            $("#upgrade_alert_msg").hide();
            $("#downgrade_alert_msg").show();
            subscription_url = "{{route('admin.subscription.downgradePlan')}}";
            // console.log("downgrade",subscription_url);
            break;
    }
    
    $("#subscription_payment").on('click', function(e) {
        subscriptionPaymentService(plan_id,subscription_url);
    });

    $("#razorpay_payment").on('click', function(e) {
        $("#razorpay_payment").addClass('border-primary');
        $('#razorpay_payment').attr('style', 'width:150px;border-width: 2px !important');
    });

    $(".payment_method").on('click', function(e) {
        $("#subscription_payment").attr('disabled',false);
    });
</script>
@endsection