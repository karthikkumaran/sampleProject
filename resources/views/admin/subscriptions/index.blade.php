@extends('layouts.admin')
@section('content')
    <style>
        .custom-switch .custom-control-label::before {
            background-color: #048DF0 !important;
        }

        .pointer {
            cursor: pointer !important;
        }
    </style>
    @php
    $upgrade = 'upgrade';
    $downgrade = 'downgrade';
    $subscribe = 'subscribe';
    $subscribed_package;
    $annual = false;
    foreach ($packages as $key => $package) {
        if ($subscription) {
            if ($package->id == $subscription->plan_id) {
                $subscribed_package = $package;
                if ($subscribed_package->invoice_interval == 'year') {
                    $annual = true;
                }
            }
        }
    }
    @endphp
    @php
    $agent = new \Jenssegers\Agent\Agent();
    @endphp
    <div class="row mt-1 mb-1">
        <div class="col-12" style="text-align:center; vertical-align:middle;">
            <h1>Find the right plan for your business</h1>
            <br><br>
            <h3>Plans & Pricing</h3>
            <div class="custom-control custom-switch custom-switch-success">
                <span class="switch-label mr-1"><b>Monthly</b></span>
                <input type="checkbox" class="custom-control-input" id="subscription_interval" name="subscription_interval"
                    {{ $annual == true ? 'checked' : '' }}>
                <label class="custom-control-label" for="subscription_interval"></label>
                <span class="switch-label"><b>Yearly</b></span>
            </div>
        </div>
    </div>
    <div class="row pricing-card">
        <div class="col-12 col-sm-offset-2 col-sm-10 col-md-12 col-lg-offset-2 col-lg-10 mx-auto" id="subscriptions_page">
            <div class="row" id="subscriptions_show">
                <!-- pricing plan cards -->
                @foreach ($packages as $key => $package)
                    @php
                        $features = $package->features;
                    @endphp
                    @if ($package->slug == 'user-trial')
                        @if ($subscription && $subscribed_package->slug == 'user-trial')
                        @else
                            @continue
                        @endif
                    @endif
                    {{-- @if ($package->invoice_interval == 'month')
                        <div class="col-12 col-md-3 monthly_package" style="display:show">
                    @elseif($package->invoice_interval == "year")
                        <div class="col-12 col-md-3 yearly_package" style="display:none">
                    @else
                        <div class="col-12 col-md-3">
                    @endif --}}
                    @if (count($packages) % 4 == 0)
                        
                        @if ($package->invoice_interval == 'month')
                            <div class="col-12 col-md-3 monthly_package" style="display:show">
                            @elseif($package->invoice_interval == 'year')
                                <div class="col-12 col-md-3 yearly_package" style="display:none">
                                @else
                                    <div class="col-12 col-md-3">
                        @endif
                        
                    @elseif(count($packages) % 3 == 0)
                        
                        @if ($package->invoice_interval == 'month')
                            <div class="col-12 col-md-4 monthly_package" style="display:show">
                            @elseif($package->invoice_interval == 'year')
                                <div class="col-12 col-md-4 yearly_package" style="display:none">
                                @else
                                    <div class="col-12 col-md-4">
                        @endif

                        
                    @else
                        
                        @if ($package->invoice_interval == 'month')
                            <div class="col-12 col-md-6 monthly_package" style="display:show">
                            @elseif($package->invoice_interval == 'year')
                                <div class="col-12 col-md-6 yearly_package" style="display:none">
                                @else
                                    <div class="col-12 col-md-6">
                        @endif
                        
                    @endif

                    <div class="card basic-pricing text-center">
                        <div class="card-body">
                            <h3>{!! $package->name !!}</h3>
                            <!-- <p class="card-text">{!! $package->description !!}</p> -->
                            <div class="annual-plan">
                                <div class="plan-price mt-2">
                                    @php
                                        if ($package->price > $package->discount) {
                                            $package_discount = $package->price - $package->discount;
                                        } else {
                                            $package_discount = $package->price;
                                        }
                                    @endphp
                                    @if ($package->discount != 0)
                                        <!-- <h2><b><div class="__price"><sup>₹</sup>{{ $package_discount }} <sub>/{{ $package->invoice_interval }}</sub></div></b></h2>
                                                <h4><b><div class="__price s__price"><s><sup>₹</sup>{{ $package->price }}<sub>/{{ $package->invoice_interval }}</s></sub></div></b></h4> -->
                                        <h2>
                                            <div class="__price">₹ {{ $package_discount }}{{-- /{{$package->invoice_interval}} --}}</div>
                                        </h2>
                                        <h4>
                                            <div class="__price">you save ₹{{ $package->discount }} </div>
                                        </h4>
                                        <!-- <h4><div class="__price s__price"><s>₹ {{ $package->price }}/{{ $package->invoice_interval }}</s></div></h4> -->
                                        <!-- <span class="font-medium-1 font-weight-bold text-primary">{!! $package->currency !!}</span> -->
                                        <!-- <span class="pricing-basic-value font-weight-bolder text-primary">{{ $package_discount }} / {{ $package->invoice_period }} {{ $package->invoice_interval }}</span><br>
                                                <s><span class="font-medium-1 font-weight-bold text-primary">{!! $package->currency !!}</span>
                                                <span class="pricing-basic-value font-weight-bolder text-primary">{!! $package->price !!} / {{ $package->invoice_period }} {{ $package->invoice_interval }}</span></s> -->
                                    @else
                                        <!-- <h2><b><div class="__price"><sup>₹</sup>{{ $package_discount }} <sub>/{{ $package->invoice_interval }}</sub></div></b></h2> -->
                                        <h2>
                                            <div class="__price">₹ {{ $package_discount }}{{-- /{{$package->invoice_interval}} --}}</div>
                                        </h2>
                                        <!-- <span class="font-medium-1 font-weight-bold text-primary">{!! $package->currency !!}</span>
                                                <span class="pricing-basic-value font-weight-bolder text-primary">{!! $package->price !!}</span> -->
                                    @endif
                                </div>
                                <small class="annual-pricing d-none text-muted"></small>
                            </div>
                            @if ($agent->isDesktop())
                                <div class="m-1">
                                    @if ($subscription && $subscribed_package->slug != 'user-trial')
                                        @if ($package->slug == $subscribed_package->slug)
                                            @if (session('subscription_expired'))
                                                <a class="btn btn-block btn-danger mt-2"
                                                    href="{{ route('admin.subscription.renewSubscription') }}">Renew</a>
                                            @else
                                                <button class="btn btn-block btn-success mt-2">Subscribed</button>
                                            @endif
                                        @else
                                            @if ($package->price > $subscribed_package->price)
                                                <button class="btn btn-block btn-primary mt-2"
                                                    onclick="userSubscription($(this))" data-subscription="upgrade"
                                                    data-plan_name="{{ $package->name }}"
                                                    data-url="{{ route('admin.subscription.show', ['subscription' => $upgrade, 'id' => $package->id]) }}">Upgrade</button>
                                            @elseif($package->price < $subscribed_package->price)
                                                <button class="btn btn-block btn-secondary mt-2"
                                                    onclick="userSubscription($(this))" data-subscription="downgrade"
                                                    data-plan_name="{{ $package->name }}"
                                                    data-url="{{ route('admin.subscription.show', ['subscription' => $downgrade, 'id' => $package->id]) }}">Downgrade</button>
                                            @endif
                                        @endif
                                    @else
                                        @if ($package->slug == 'user-trial')
                                        @else
                                            <button class="btn btn-block btn-success mt-2"
                                                onclick="userSubscription($(this))" data-subscription="subscribe"
                                                data-plan_name="{{ $package->name }}"
                                                data-url="{{ route('admin.subscription.show', ['subscription' => $subscribe, 'id' => $package->id]) }}">Subscribe</button>
                                        @endif
                                    @endif
                                </div>
                            @endif
                            @if ($agent->isMobile())
                                <button class="my-2 pointer btn btn-info" data-toggle="collapse" id="features"
                                    data-target="#demo_{{ $package->id }}">Show features &nbsp;
                                    <i class="fa fa-angle-double-down"></i>
                                    <!-- <i data-toggle="collapse" class="fa fa-angle-double-up"></i><i class="fa fa-angle-double-down"></i> -->
                                </button>
                                <div id="demo_{{ $package->id }}" class="collapse">
                            @endif
                            <ul class="list-group list-group-circle text-left ">
                                @foreach ($features as $key => $feature)
                                    @php
                                        $feature_details = $feature->getFeatureDetails();
                                    @endphp
                                    <li class="list-group-item ">
                                        @if ($feature_details->name == 'catalogs')
                                            <i class="fa fa-check-circle p-1 text-left"></i>Number of Catalogue -
                                            <b>{!! $feature->value !!}</b>
                                            <!-- Number of Catalogue - <b>{!! $feature->value !!}</b> -->
                                        @elseif($feature_details->name == 'products')
                                            <i class="fa fa-check-circle p-1 text-left"></i>Number of Products -
                                            <b>{!! $feature->value !!}</b>
                                            <!-- Number of Products- <b>{!! $feature->value !!}</b> -->
                                        @elseif($feature_details->name == 'stories')
                                            <i class="fa fa-check-circle p-1 text-left"></i>Product Stories
                                        @elseif($feature_details->name == 'pos')
                                            <i class="fa fa-check-circle p-1 text-left"></i>Point of Sale
                                        @elseif($feature_details->name == 'analytics')
                                            <i class="fa fa-check-circle p-1 text-left"></i>Store Analytics
                                        @else
                                            <i class="fa fa-check-circle p-1 text-left"></i>{!! $feature_details->name !!}
                                        @endif

                                    </li>
                                @endforeach
                                @php
                                    $catalog_count = 0;
                                    $product_count = 0;
                                    $stories_count = 0;
                                    $pos_count = 0;
                                @endphp
                                <li class="list-group-item "><i class="fa fa-check-circle p-1 text-left"></i>Free Sub Domain
                                </li>
                                <li class="list-group-item "><i class="fa fa-check-circle p-1 text-left"></i>Your Brand Logo
                                </li>
                                <li class="list-group-item "><i class="fa fa-check-circle p-1 text-left"></i>Private
                                    Catalogue</li>
                                <li class="list-group-item "><i class="fa fa-check-circle p-1 text-left"></i>QR Code for the
                                    store and Catalog</li>
                                <li class="list-group-item "><i class="fa fa-check-circle p-1 text-left"></i>Email/Text
                                    Support</li>
                                @foreach ($features as $key => $feature)
                                    @php
                                        $feature_details = $feature->getFeatureDetails();
                                        if ($feature_details->name == 'catalogs') {
                                            $catalog_count = 1;
                                        }
                                        if ($feature_details->name == 'products') {
                                            $product_count = 1;
                                        }
                                        if ($feature_details->name == 'stories') {
                                            $stories_count = 1;
                                        }
                                        if ($feature_details->name == 'pos') {
                                            $pos_count = 1;
                                        }
                                    @endphp
                                @endforeach
                                @if (empty($catalog_count))
                                    <li class="list-group-item "><i class="fa fa-minus-circle p-1 text-left"></i>Number of
                                        Catalogue</li>
                                @endif
                                @if (empty($product_count))
                                    <li class="list-group-item "><i class="fa fa-minus-circle p-1 text-left"></i>Number of
                                        Products</li>
                                @endif
                                @if (empty($stories_count))
                                    <li class="list-group-item "><i class="fa fa-minus-circle p-1 text-left"></i>Product
                                        Stories</li>
                                @endif
                                @if (empty($pos_count))
                                    <li class="list-group-item "><i class="fa fa-minus-circle p-1 text-left"></i>Point of
                                        Sale</li>
                                @endif
                            </ul>
                            @if ($agent->isMobile())
                        </div>
                @endif
                <!-- <ul class="list-group list-group-circle text-left"> -->
                {{-- @foreach ($features as $key => $feature)
                                            @php
                                                $feature_details = $feature->getFeatureDetails();
                                            @endphp --}}
                <!-- <li class="list-group-item "><i class="fa fa-check-circle p-1 text-left"></i>Number of Catalogue </li>
                                                    <li class="list-group-item "><i class="fa fa-check-circle p-1 text-left"></i>Number of Products </li>
                                                    <li class="list-group-item "><i class="fa fa-check-circle p-1 text-left"></i>Free Sub Domain</li>
                                                    <li class="list-group-item "><i class="fa fa-check-circle p-1 text-left"></i>Your Brand Logo</li>
                                                    <li class="list-group-item "><i class="fa fa-check-circle p-1 text-left"></i>Private Catalogue</li>
                                                    <li class="list-group-item "><i class="fa fa-check-circle p-1 text-left"></i>QR Code for the store and Catalog</li>
                                                    <li class="list-group-item "><i class="fa fa-check-circle p-1 text-left"></i>Store Analytics</li>
                                                    <li class="list-group-item "><i class="fa fa-check-circle p-1 text-left"></i>Email/Text Support</li>
                                                    <li class="list-group-item "><i class="fa fa-minus-circle p-1 text-left"></i>Custom Domain Mapping</li>
                                                    <li class="list-group-item "><i class="fa fa-minus-circle p-1 text-left"></i>Inventory</li>
                                                    <li class="list-group-item "><i class="fa fa-minus-circle p-1 text-left"></i>Advanced Analytics</li>
                                                    <li class="list-group-item "><i class="fa fa-minus-circle p-1 text-left"></i>Point of Sale</li>
                                                    <li class="list-group-item "><i class="fa fa-minus-circle p-1 text-left"></i>Product Stories</li> -->
                <!-- {{-- <li class="list-group-item text-center">
                                                @if ($feature_details->name == 'catalogs')
                                                    Number of Catalogue - <b>{!! $feature->value !!}</b>
                                                @elseif($feature_details->name == "products")
                                                    Number of Products- <b>{!! $feature->value !!}</b>
                                                @elseif($feature_details->name == "stories")
                                                    Product Stories- <b>{!! $feature->value !!}</b>
                                                @elseif($feature_details->name == "pos")
                                                    Point of Sale - <b>{!! $feature->value !!}</b>
                                                @elseif($feature_details->name == "analytics")
                                                    Store Analytics - <b>{!! $feature->value !!}</b>
                                                @else
                                                    {!! $feature_details->name !!} - <b>{!! $feature->value !!}</b>
                                                @endif
                                                <i id="feature_description_show_{{$package->id}}_{{$feature->feature_id}}" class="fa fa-angle-double-down float-right" onclick="feature_description_show({{$package->id}},{{$feature->feature_id}})" style="display:block;cursor:pointer"></i>
                                                <i id="feature_description_hide_{{$package->id}}_{{$feature->feature_id}}" class="fa fa-angle-double-up float-right" onclick="feature_description_hide({{$package->id}},{{$feature->feature_id}})" style="display:none;cursor:pointer"></i>
                                                <li class="list-group-item text-center text-dark" id="feature_description_{{$package->id}}_{{$feature->feature_id}}" style="display:none">
                                                    {!! $feature_details->description !!}
                                                </li>
                                            </li> --}} -->
                {{-- @endforeach --}}
                <!-- </ul> -->
                @if ($subscription && $subscribed_package->slug != 'user-trial')
                    @if ($package->slug == $subscribed_package->slug)
                        @if (session('subscription_expired'))
                            <a class="btn btn-block btn-danger mt-2"
                                href="{{ route('admin.subscription.renewSubscription') }}">Renew</a>
                        @else
                            <button class="btn btn-block btn-success mt-2">Subscribed</button>
                        @endif
                    @else
                        @if ($package->price > $subscribed_package->price)
                            <button class="btn btn-block btn-primary mt-2" onclick="userSubscription($(this))"
                                data-subscription="upgrade" data-plan_name="{{ $package->name }}"
                                data-url="{{ route('admin.subscription.show', ['subscription' => $upgrade, 'id' => $package->id]) }}">Upgrade</button>
                        @elseif($package->price < $subscribed_package->price)
                            <button class="btn btn-block btn-secondary mt-2" onclick="userSubscription($(this))"
                                data-subscription="downgrade" data-plan_name="{{ $package->name }}"
                                data-url="{{ route('admin.subscription.show', ['subscription' => $downgrade, 'id' => $package->id]) }}">Downgrade</button>
                        @endif
                    @endif
                @else
                    @if ($package->slug == 'user-trial')
                    @else
                        <button class="btn btn-block btn-success mt-2" onclick="userSubscription($(this))"
                            data-subscription="subscribe" data-plan_name="{{ $package->name }}"
                            data-url="{{ route('admin.subscription.show', ['subscription' => $subscribe, 'id' => $package->id]) }}">Subscribe</button>
                    @endif
                @endif
            </div>
        </div>
    </div>
    @endforeach



    <!--/ pricing plan cards -->
    </div>
    </div>
    </div>
    @include('modals.user_subscription')
@endsection
@section('scripts')
    @parent
    <script>
        if ($('#subscription_interval').is(':checked') == true) {
            $('.yearly_package').show();
            $('.monthly_package').hide();
        } else {
            $('.monthly_package').show();
            $('.yearly_package').hide();
        }

        $("#subscription_interval").change(function() {
            var x = $("#subscription_interval").is(":checked");
            if (this.checked) {
                $('.yearly_package').show();
                $('.monthly_package').hide();
            } else {
                $('.monthly_package').show();
                $('.yearly_package').hide();
            }
        });
        $('#features').click(function() {});

        function userSubscription($this) {
            $("#userSubscription").modal('show');
            subscribePlan($this);
        }

        function feature_description_show(package_id, feature_id) {
            $('#feature_description_show_' + package_id + '_' + feature_id).hide();
            $('#feature_description_hide_' + package_id + '_' + feature_id).show();
            $('#feature_description_' + package_id + '_' + feature_id).show();
        }

        function feature_description_hide(package_id, feature_id) {
            $('#feature_description_show_' + package_id + '_' + feature_id).show();
            $('#feature_description_hide_' + package_id + '_' + feature_id).hide();
            $('#feature_description_' + package_id + '_' + feature_id).hide();
        }
    </script>
@endsection
