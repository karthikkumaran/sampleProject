@extends('layouts.store_onboard')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @php
                if($plan->price > $plan->discount)
                    $plan_discount = $plan->price - $plan->discount;
                else
                    $plan_discount = $plan->price; 
            @endphp

            <div class=""> 
            @if($plan->discount!= 0)
                <p>You will be charged {{$plan->currency}} {{ number_format($plan_discount, 2) }} for {{ $plan->name }} Plan</p>
            @else
                <p>You will be charged {{$plan->currency}} {{ number_format($plan->price, 2) }} for {{ $plan->name }} Plan</p>
            @endif
            </div>
            <div class="card">
                <div class="card-header">
                </div>
                <div class="card-body">
                    <label for="card-element">
                        Choose your payment method
                    </label>
                    <img class="card text-center cursor-pointer border p-2 payment_method" src="{{ asset('XR\app-assets\images\icons\razorpay_logo.svg') }}" style="width:150px;">
                    <input type="hidden" name="plan" value="{{ $plan->id }}" />
                </div>
                <div class="card-footer">
                    <button id="subscription_payment" class="btn btn-primary float-right" disabled> Pay </button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    var plan_id = "{{ $plan->id }}";
    var subscription_url = "{{route('admin.subscription.create')}}";
    
    $("#subscription_payment").on('click', function(e) {
        subscriptionPaymentService(plan_id,subscription_url);
    });

    $(".payment_method").on('click', function(e) {
        $("#subscription_payment").attr('disabled',false);
    });
</script>
@endsection