@extends('layouts.admin')
@section('content')
    @section('styles')
    @parent
        <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
        <style>
            table {
                border-collapse: collapse;
                width: 100%;
            }
            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                
                padding: 8px;
            }
        </style>
    @endsection
    @php
        $features = $myplan->myFeatures();
    @endphp
    <div class="content-body">
        <section class="invoice-preview-wrapper">
            <div class="row invoice-preview">
                <!-- Invoice -->
                <div class="col-12 p-1">
                    <!-- Invoice Note starts -->
                    @if(session('payment_successful'))
                    <div class="card-body invoice-padding pt-0">
                        <div class="row">
                            <div class="col-12">
                                <i class="feather icon-info"></i>
                                <span class="font-weight-bold">Payment Success!</span>
                                <span>Your payment is successful and you have subscribed to {{$myplan->name}} plan.</span>
                            </div>
                        </div>
                    </div>
                    @endif
                    <!-- Invoice Note ends -->
                    <div class="card invoice-preview-card">
                        <div class="card-body invoice-padding pb-0">
                            <!-- Header starts -->         
                            <a class="btn btn-success pull-right" href="{{ route('admin.subscriptions.index') }}">
                                Change plan
                            </a>
                            <div class="d-flex justify-content-between flex-md-row flex-column invoice-spacing mt-0 bordered">
                                <div class="mt-md-0 mt-2">
                                    <h4 class="invoice-title">
                                        Plan Details:
                                    </h4>
                                    <div class="invoice-date-wrapper">
                                        <p class="invoice-date-title"><b>Plan name:</b> {{$myplan->name}}</p>
                                    </div>
                                    <div class="invoice-date-wrapper">
                                        <p class="invoice-date-title"><b>Plan type:</b> {{$package->invoice_interval == "year" ? 'Annual':'Monthly'}}</p>
                                    </div>
                                    <div class="invoice-date-wrapper">
                                        <p class="invoice-date-title"><b>Date Subscribed:</b> {{$myplan->created_at}}</p>
                                    </div>
                                    <div class="invoice-date-wrapper">
                                        <p class="invoice-date-title"><b>Your subscription starts on:</b> {{$myplan->starts_at}}</p>
                                    </div>
                                    <div class="invoice-date-wrapper">
                                        <p class="invoice-date-title"><b>Your subscription ends on:</b> {{$myplan->ends_at}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-between flex-md-row flex-column invoice-spacing mt-0">
                                <div class="mt-md-0 mt-2">
                                    <h4 class="invoice-title">
                                        Plan Features:
                                    </h4>
                                    <table class="mb-1">
                                        <tr>
                                            <th>Feature name</th>
                                            <th>Description</th>
                                            <th>Allowed usage</th>
                                            <th>Add on</th>
                                        </tr>
                                        @foreach($features as $key => $feature)
                                            @php
                                                $feature_details = $feature->getFeatureDetails();
                                            @endphp
                                            <tr>
                                                <td>{!! $feature_details->name !!}</td>
                                                <td>{!! $feature_details->description !!}</td>
                                                @if($feature_details->name == "grace period")
                                                    <td class="text-right">-</td>
                                                    <td class="text-right">{!! $feature->add_on_value !!} {!! $feature->value !!}</td>
                                                @else
                                                    @if($feature->is_add_on == 1)
                                                        <td class="text-right">-</td>
                                                        <td class="text-right">{!! $feature->value !!}</td>
                                                    @else
                                                        <td class="text-right">{!! $feature->value !!}</td>
                                                        <td class="text-right">{!! $feature->add_on_value ?? '-' !!}</td>
                                                    @endif
                                                    
                                                @endif
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                            <!-- Header ends -->
                        </div>
                    </div>
                </div>
                <!-- /Invoice -->
            </div>
        </section>
@endsection