@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.asset.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.assets.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="name">{{ trans('cruds.asset.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}">
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.asset.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="meta_data">{{ trans('cruds.asset.fields.meta_data') }}</label>
                <input class="form-control {{ $errors->has('meta_data') ? 'is-invalid' : '' }}" type="text" name="meta_data" id="meta_data" value="{{ old('meta_data', '') }}">
                @if($errors->has('meta_data'))
                    <div class="invalid-feedback">
                        {{ $errors->first('meta_data') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.asset.fields.meta_data_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="file_object">{{ trans('cruds.asset.fields.file_object') }}</label>
                <div class="needsclick dropzone {{ $errors->has('file_object') ? 'is-invalid' : '' }}" id="file_object-dropzone">
                </div>
                @if($errors->has('file_object'))
                    <div class="invalid-feedback">
                        {{ $errors->first('file_object') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.asset.fields.file_object_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    Dropzone.options.fileObjectDropzone = {
    url: '{{ route('admin.assets.storeMedia') }}',
    maxFilesize: 40, // MB
    maxFiles: 1,
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 40
    },
    success: function (file, response) {
      $('form').find('input[name="file_object"]').remove()
      $('form').append('<input type="hidden" name="file_object" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="file_object"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
@if(isset($asset) && $asset->file_object)
      var file = {!! json_encode($asset->file_object) !!}
          this.options.addedfile.call(this, file)
      file.previewElement.classList.add('dz-complete')
      $('form').append('<input type="hidden" name="file_object" value="' + file.file_name + '">')
      this.options.maxFiles = this.options.maxFiles - 1
@endif
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
}
</script>
@endsection