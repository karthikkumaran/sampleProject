@extends('layouts.admin')
@section('content') 
<div class="card">
    <div class="card-header">
        Edit Product Variant
    </div>
    <div class="card-body">
        <form method="POST" action="{{ route("admin.productVariants.update", [$productvariants->id]) }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                    <input type="hidden" class="form-control"  name="id" id="id" value="{{ old('id', $productvariants->id) }}" >
            </div>
            <div class="form-group">
                <label for="value"> Product Variant-Value</label>
                        @php
                            $value =json_decode($productvariants->variant_data);
                            foreach($value as $value)
                            {
                                $typevalue = $value->value;
                            }
                        @endphp
                    <input type="text" class="form-control"  name="value" id="value" value="{{ old('value', $typevalue) }}" >
            </div>
            <div class="form-group">
                <label for="product_name"> Product Name</label>
                    <input type="text" class="form-control"  name="product_name" id="product_name" value="{{ old('product_name', $productvariants->name) }}" >
            </div>
            <div class="form-group">
                <label for="sku">SKU</label>
                    <input type="text" class="form-control"  name="sku" id="sku" value="{{ old('sku', $productvariants->sku) }}" >
            </div>
            <div class="form-group">
                <label for="price"> Price</label>
                    <input type="text" class="form-control"  name="price" id="price" value="{{ old('Price', $productvariants->price) }}" >
            </div>
            <div class="form-group">
                <label for="discount"> Discount(%)</label>
                    <input type="number" class="form-control"  name="discount" id="discount" value="{{ old('Price', $productvariants->discount) }}" >
            </div>
            <!-- <div class="form-group">
                <label for="stock"> Stock</label>
                    <input type="number" class="form-control"  name="stock" id="stock" value="{{ old('Price', $productvariants->stock) }}" >
            </div> -->
            <div class="form-group">
                <input class="" type="checkbox" id="out_of_stock" name="out_of_stock" value="1" {{ old('new',$productvariants->out_of_stock) ? 'checked="checked"' : '' }}/>
                <label class="" id="out_of_stock">Out of Stock</label>
            </div>
            
            <div class="form-group">
                    <label for="units">{{ trans('cruds.product.fields.unit_type') }}</label>
                                                                <select class="form-control select2" id="units" name="units">
                                                                   
                                                                    <option value="{!!empty($productvariants->units)?'':$productvariants->units!!}">{!!empty($productvariants->units)?"Choose Unit":"$productvariants->units"!!}</option>
                                                                    @php
                                                                        $unit=array("piece","kg","gram","ml","litre","mm","ft","meter","sq.ft","sq.meter","km","set","hour","day","bunch","bundle","month","year","service","packet","work","box","pound","dozen","gunta","pair","minute","quintal","ton","capsule","tablet","plate","inch");
                                                                    @endphp
                                                                    @foreach($unit as $value)
                                                                        <option value="{{$value}}">{{$value}}</option>
                                                                    @endforeach
                                                                </select>
            </div>
            <div class="form-group">
            <label class="form-check-label" for="downgradeable">Status</label>
                <select class="custom-select form-control" name="downgradeable" id="downgradeable" required>
                    <option value="0" {{ old('downgradeable',$productvariants->downgradeable == 0) ? 'selected' : '' }}>Active</option>
                    <option value="1" {{ old('downgradeable',$productvariants->downgradeable == 1) ? 'selected' : '' }}>Inactive</option>
                </select>
            </div>
          
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-3 col-md-12">
                <input class="" type="checkbox" id="featured" name="featured" value="1" {{ old('featured',$productvariants->featured) ? 'checked="checked"' : '' }}/>
                <label class="" id="featured">Featured</label>
                        </div>
                    <div class="col-lg-3 col-md-12">
            <input class="" type="checkbox" id="new" name="new" value="1" {{ old('new',$productvariants->new) ? 'checked="checked"' : '' }}/>
                                                                <label class="" id="new">New</label>
                        </div>
                        </div>
            </div>
            <div class="form-group">
                <label for="minimum_order_quantity">Minimum Order Quantity</label>
                    <input type="text" class="form-control"  name="minimum_order_quantity" id="minimum_order_quantity" value="{{ old('minimum_order_quantity', $productvariants->minimum_quantity) }}" >
            </div>
            <div class="form-group">
                <label for="description">{{ trans('cruds.product.fields.description') }}</label>
                <textarea class="form-control pb-1 {{ $errors->has('description') ? 'is-invalid' : '' }} hidden" rows="5" name="description" id="description">{{ old('description', $productvariants->description) }}</textarea>
                <div id="editor-container" style="height: 250px;"></div>
                
            </div>
            
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
<script>
    document.addEventListener("DOMContentLoaded", function (event) {
     var quill = new Quill('#editor-container', {
            modules: {
                'toolbar': [
                    [{
                        'font': []
                    }, {
                        'size': []
                    }]
                    , ['bold', 'italic', 'underline', 'strike']
                    , [{
                        'color': []
                    }, {
                        'background': []
                    }]
                    , [{
                        'script': 'super'
                    }, {
                        'script': 'sub'
                    }]
                    , [{
                        'header': '1'
                    }, {
                        'header': '2'
                    }, 'blockquote', 'code-block']
                    , [{
                        'list': 'ordered'
                    }, {
                        'list': 'bullet'
                    }, {
                        'indent': '-1'
                    }, {
                        'indent': '+1'
                    }]
                    , ['direction', {
                        'align': []
                    }]
                    , ['link', 'image', 'video', 'formula']
                    , ['clean']
                ]
            , }
            , placeholder: 'description...'
            , theme: 'snow'
        });
        console.log(quill.root.innerHTML);
        quill.root.innerHTML = $("#description").val();
    $("form").submit(function(e) {
        // e.preventDefault();
        console.log(quill.root.innerHTML)
        $("#description").val(quill.root.innerHTML);
        $(form).serializeArray();
    });
    });
    </script>