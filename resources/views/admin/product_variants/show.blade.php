@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        Show Product Variants
    </div>
    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route("admin.products.product_list") }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            Id
                        </th>
                        <td>
                            {{ $productvariants->id }}
                        </td>
                    </tr>
                   
                    <tr>
                        <th>
                            Product Variant-Value
                        </th>
                        <td>
                            @php
                                $value =json_decode($productvariants->variant_data);
                                foreach($value as $value)
                                {
                                    $typevalue = $value->value;
                                }
                            @endphp
                                {{$typevalue}}
                        </td>
                    </tr>

                    
                    <tr>
                        <th>
                            Product Name
                        </th>
                        <td>
                        {{ $productvariants->name }}
                        </td>
                    </tr>


                    <tr>
                        <th>
                            SKU
                        </th>
                        <td>
                        {{ $productvariants->sku }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Price
                        </th>
                        <td>
                        {{ $productvariants->price }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Discount(%)
                        </th>
                        <td>
                            {{ $productvariants->discount }}
                        </td>
                    </tr>
                
                    <tr>
                        <th>
                            Description
                        </th>
                        <td>
                            {{ $productvariants->description }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Stock
                        </th>
                        <td>
                            {{ $productvariants->stock }}
                        </td>
                    </tr>
                     
                    <tr>
                        <th>
                            Out of Stock
                        </th>
                        <td>
                        {{ $productvariants->out_of_stock }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Unit
                        </th>
                        <td>
                            {{ $productvariants->units }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Status
                        </th>
                        <td>
                            {{ $productvariants->status }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Downgradeable
                        </th>
                        <td>
                            {{ $productvariants->downgradeable }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            Featured
                        </th>
                        <td>
                            {{ $productvariants->featured }}
                        </td>
                    </tr>

                    <tr>
                        <th>
                            New
                        </th>
                        <td>
                            {{ $productvariants->new }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Minimum Order Quantity
                        </th>
                        <td>
                        {{ $productvariants->minimum_quantity }}
                        </td>
                    </tr>
                   
                    
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route("admin.products.product_list") }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>
@endsection