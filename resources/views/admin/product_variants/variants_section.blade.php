<div class="row mt-1" id="variant_{{ $variant_id }}">
    <div class="form-group col-md-5">
        <select class="custom-select form-control {{ $errors->has('type') ? 'is-invalid' : '' }} type_{{ $variant_id }} variant_type" name="type[]" id="type_{{ $variant_id }}" data-id="{{ $variant_id }}" required>
            <option value="">Choose a variant</option>
            @foreach($variants as $variant)
                <option value="{{$variant->id}}">{{$variant->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group valuesdiv{{ $variant_id }} col-md-5">
        <input class="form-control {{ $errors->has('value') ? 'is-invalid' : '' }} value{{ $variant_id }}" type="text" name="value[]" id='value{{ $variant_id }}' required>
        <span class="help-block valuehelper{{ $variant_id }}">Use comma to separate the values</span>
    </div>
    <div class="col-md-2 p-1">
        <button type="button" class="btn btn-danger" onclick="deleteVariant({{ $variant_id }})"><i class="fa fa-trash"></i></button>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.js"
    integrity="sha512-wTIaZJCW/mkalkyQnuSiBodnM5SRT8tXJ3LkIUA/3vBJ01vWe5Ene7Fynicupjt4xqxZKXA97VgNBHvIf5WTvg=="
    crossorigin="anonymous"></script>
<script>
    var variants_list = [];
    function deleteVariant(variant_id) {
        $("#variant_" + variant_id).remove();
        new_variant_id--;
        if(new_variant_id == 3){
            $("#variants_alert_div").show();
        }
        else if(new_variant_id == 0){
            $("#next_btn").prop('disabled', true);
        }
        else{
            $("#variants_alert_div").hide();
        }
    }

    $('.variant_type').change(function(e) {
        var id = $(this).data('id');
        $('.valuesdiv' + id).children().show();
        $("#value" + id).hide();
        $('#value' + id).tagsInput({
            'height': '100%',
            'width': '100%',
            'defaultText': '',
        });
    });
</script>
