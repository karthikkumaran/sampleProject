@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.entitycard.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.entitycards.update", [$entitycard->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="campagin_id">{{ trans('cruds.entitycard.fields.campagin') }}</label>
                <select class="form-control select2 {{ $errors->has('campagin') ? 'is-invalid' : '' }}" name="campagin_id" id="campagin_id" required>
                    @foreach($campagins as $id => $campagin)
                        <option value="{{ $id }}" {{ ($entitycard->campagin ? $entitycard->campagin->id : old('campagin_id')) == $id ? 'selected' : '' }}>{{ $campagin }}</option>
                    @endforeach
                </select>
                @if($errors->has('campagin'))
                    <div class="invalid-feedback">
                        {{ $errors->first('campagin') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.entitycard.fields.campagin_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="product_id">{{ trans('cruds.entitycard.fields.product') }}</label>
                <select class="form-control select2 {{ $errors->has('product') ? 'is-invalid' : '' }}" name="product_id" id="product_id">
                    @foreach($products as $id => $product)
                        <option value="{{ $id }}" {{ ($entitycard->product ? $entitycard->product->id : old('product_id')) == $id ? 'selected' : '' }}>{{ $product }}</option>
                    @endforeach
                </select>
                @if($errors->has('product'))
                    <div class="invalid-feedback">
                        {{ $errors->first('product') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.entitycard.fields.product_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="video_id">{{ trans('cruds.entitycard.fields.video') }}</label>
                <select class="form-control select2 {{ $errors->has('video') ? 'is-invalid' : '' }}" name="video_id" id="video_id">
                    @foreach($videos as $id => $video)
                        <option value="{{ $id }}" {{ ($entitycard->video ? $entitycard->video->id : old('video_id')) == $id ? 'selected' : '' }}>{{ $video }}</option>
                    @endforeach
                </select>
                @if($errors->has('video'))
                    <div class="invalid-feedback">
                        {{ $errors->first('video') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.entitycard.fields.video_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="asset_id">{{ trans('cruds.entitycard.fields.asset') }}</label>
                <select class="form-control select2 {{ $errors->has('asset') ? 'is-invalid' : '' }}" name="asset_id" id="asset_id">
                    @foreach($assets as $id => $asset)
                        <option value="{{ $id }}" {{ ($entitycard->asset ? $entitycard->asset->id : old('asset_id')) == $id ? 'selected' : '' }}>{{ $asset }}</option>
                    @endforeach
                </select>
                @if($errors->has('asset'))
                    <div class="invalid-feedback">
                        {{ $errors->first('asset') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.entitycard.fields.asset_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="meta_data">{{ trans('cruds.entitycard.fields.meta_data') }}</label>
                <input class="form-control {{ $errors->has('meta_data') ? 'is-invalid' : '' }}" type="text" name="meta_data" id="meta_data" value="{{ old('meta_data', $entitycard->meta_data) }}">
                @if($errors->has('meta_data'))
                    <div class="invalid-feedback">
                        {{ $errors->first('meta_data') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.entitycard.fields.meta_data_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection