@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.companyInfo.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.company-infos.update", [$companyInfo->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.companyInfo.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $companyInfo->name) }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.companyInfo.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="email">{{ trans('cruds.companyInfo.fields.email') }}</label>
                <input class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" type="text" name="email" id="email" value="{{ old('email', $companyInfo->email) }}">
                @if($errors->has('email'))
                    <div class="invalid-feedback">
                        {{ $errors->first('email') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.companyInfo.fields.email_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="phoneno">{{ trans('cruds.companyInfo.fields.phoneno') }}</label>
                <input class="form-control {{ $errors->has('phoneno') ? 'is-invalid' : '' }}" type="text" name="phoneno" id="phoneno" value="{{ old('phoneno', $companyInfo->phoneno) }}">
                @if($errors->has('phoneno'))
                    <div class="invalid-feedback">
                        {{ $errors->first('phoneno') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.companyInfo.fields.phoneno_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="website">{{ trans('cruds.companyInfo.fields.website') }}</label>
                <input class="form-control {{ $errors->has('website') ? 'is-invalid' : '' }}" type="text" name="website" id="website" value="{{ old('website', $companyInfo->website) }}">
                @if($errors->has('website'))
                    <div class="invalid-feedback">
                        {{ $errors->first('website') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.companyInfo.fields.website_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="subdomain">{{ trans('cruds.companyInfo.fields.subdomain') }}</label>
                <input class="form-control {{ $errors->has('subdomain') ? 'is-invalid' : '' }}" type="text" name="subdomain" id="subdomain" value="{{ old('subdomain', $companyInfo->subdomain) }}" required>
                @if($errors->has('subdomain'))
                    <div class="invalid-feedback">
                        {{ $errors->first('subdomain') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.companyInfo.fields.subdomain_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="address">{{ trans('cruds.companyInfo.fields.address') }}</label>
                <textarea class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}" name="address" id="address">{{ old('address', $companyInfo->address) }}</textarea>
                @if($errors->has('address'))
                    <div class="invalid-feedback">
                        {{ $errors->first('address') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.companyInfo.fields.address_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="country_id">{{ trans('cruds.companyInfo.fields.country') }}</label>
                <select class="form-control select2 {{ $errors->has('country') ? 'is-invalid' : '' }}" name="country_id" id="country_id">
                    @foreach($countries as $id => $country)
                        <option value="{{ $id }}" {{ ($companyInfo->country ? $companyInfo->country->id : old('country_id')) == $id ? 'selected' : '' }}>{{ $country }}</option>
                    @endforeach
                </select>
                @if($errors->has('country'))
                    <div class="invalid-feedback">
                        {{ $errors->first('country') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.companyInfo.fields.country_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="city_id">{{ trans('cruds.companyInfo.fields.city') }}</label>
                <select class="form-control select2 {{ $errors->has('city') ? 'is-invalid' : '' }}" name="city_id" id="city_id">
                    @foreach($cities as $id => $city)
                        <option value="{{ $id }}" {{ ($companyInfo->city ? $companyInfo->city->id : old('city_id')) == $id ? 'selected' : '' }}>{{ $city }}</option>
                    @endforeach
                </select>
                @if($errors->has('city'))
                    <div class="invalid-feedback">
                        {{ $errors->first('city') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.companyInfo.fields.city_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="state_id">{{ trans('cruds.companyInfo.fields.state') }}</label>
                <select class="form-control select2 {{ $errors->has('state') ? 'is-invalid' : '' }}" name="state_id" id="state_id">
                    @foreach($states as $id => $state)
                        <option value="{{ $id }}" {{ ($companyInfo->state ? $companyInfo->state->id : old('state_id')) == $id ? 'selected' : '' }}>{{ $state }}</option>
                    @endforeach
                </select>
                @if($errors->has('state'))
                    <div class="invalid-feedback">
                        {{ $errors->first('state') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.companyInfo.fields.state_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="users_id">{{ trans('cruds.companyInfo.fields.users') }}</label>
                <select class="form-control select2 {{ $errors->has('users') ? 'is-invalid' : '' }}" name="users_id" id="users_id" required>
                    @foreach($users as $id => $users)
                        <option value="{{ $id }}" {{ ($companyInfo->users ? $companyInfo->users->id : old('users_id')) == $id ? 'selected' : '' }}>{{ $users }}</option>
                    @endforeach
                </select>
                @if($errors->has('users'))
                    <div class="invalid-feedback">
                        {{ $errors->first('users') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.companyInfo.fields.users_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection