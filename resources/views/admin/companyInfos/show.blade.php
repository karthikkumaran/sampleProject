@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.companyInfo.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.company-infos.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.companyInfo.fields.id') }}
                        </th>
                        <td>
                            {{ $companyInfo->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.companyInfo.fields.name') }}
                        </th>
                        <td>
                            {{ $companyInfo->name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.companyInfo.fields.email') }}
                        </th>
                        <td>
                            {{ $companyInfo->email }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.companyInfo.fields.phoneno') }}
                        </th>
                        <td>
                            {{ $companyInfo->phoneno }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.companyInfo.fields.website') }}
                        </th>
                        <td>
                            {{ $companyInfo->website }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.companyInfo.fields.subdomain') }}
                        </th>
                        <td>
                            {{ $companyInfo->subdomain }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.companyInfo.fields.address') }}
                        </th>
                        <td>
                            {{ $companyInfo->address }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.companyInfo.fields.country') }}
                        </th>
                        <td>
                            {{ $companyInfo->country->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.companyInfo.fields.city') }}
                        </th>
                        <td>
                            {{ $companyInfo->city->code ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.companyInfo.fields.state') }}
                        </th>
                        <td>
                            {{ $companyInfo->state->code ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.companyInfo.fields.users') }}
                        </th>
                        <td>
                            {{ $companyInfo->users->name ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.company-infos.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection