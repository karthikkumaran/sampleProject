@extends('layouts.admin')
@section('content')
<div style="margin-bottom: 10px;" class="row"> 
    <div class="col-lg-12">
        <a class="btn btn-success pull-right" href="{{ route('admin.plan_subscription_features.create') }}">
            Add Feature
        </a>
    </div>
</div>
<div class="card">
    <div class="card-header">
        Features
    </div>
    <div class="card-body">
        <table class="table table-bordered table-striped table-hover ajaxTable datatable datatable-PlanSubscriptionFeatures">
            <thead>
                <tr>
                    <th scope="col">
                        ID
                    </th>
                    <th scope="col">
                        Slug
                    </th>
                    <th scope="col">
                        Name
                    </th>
                    <th scope="col">
                        Description
                    </th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    // url="{{route('admin.product-attributes.destroy',':id')}}";
    // url = url.replace(':id', 83);
    // //  console.log(url);

    $(function () {
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
        let dtOverrideGlobals = {
            buttons: dtButtons,
            responsive: true,
            processing: true,
            serverSide: true,
            retrieve: true,
            aaSorting: [],
            ajax: "{{ route('admin.plan_subscription_features.index') }}",
            columns: [
                { data: 'id', name: 'id' },
                { data: 'slug', name: 'slug' },
                { data: 'name', name: 'name' },
                { data: 'description', name: 'description' },
                // { data: 'actions', name: '{{ trans('global.actions') }}' }
            ],
            order: [[ 0, 'desc' ]],
            pageLength: 100,
        };    

        $('.datatable-PlanSubscriptionFeatures').DataTable(dtOverrideGlobals);
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
        });
    });  
</script>
@endsection