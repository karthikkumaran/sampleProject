@extends('layouts.admin')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.css"
    integrity="sha512-uKwYJOyykD83YchxJbUxxbn8UcKAQBu+1hcLDRKZ9VtWfpMb1iYfJ74/UIjXQXWASwSzulZEC1SFGj+cslZh7Q==" crossorigin="anonymous" />

<style>
    div.tagsinput {
        border: 1px solid #CCC;
        background: #FFF;
        /* padding: 5px; */
        width: 300px;
        /* height: 100px; */
        overflow-y: visible;
    }

    div.tagsinput span.tag {
        border: 1px solid #1709da;
        background: #0814c2;
        color: #ffffff;
        font-size: 18px;
        font-weight: 500;
    }

    div.tagsinput span.tag a {
        font-weight: 700;
        color: white;
        text-decoration: none;
        font-size: 19px;
        margin-bottom: 0;
        padding: 0;
    }

</style>
@section('content')

    <div class="show_alert">

    </div>

    <div class="card">
        <div class="card-header">
            <div>
                <a class="btn btn-md  btn-outline-secondary " href="{{ route('admin.packages.index') }}">
                    Back to features list
                </a>
                <br>
            </div>
        </div>

        <div class="card-body">
            <form method="POST" action="{{ route('admin.plan_subscription_features.store') }}" enctype="multipart/form-data">
                @csrf
                <label class="h3">Feature</label>
                <br>
                <br>
                <div class="row">
                    <div class="col-12 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="required h4" for="name">Name</label>
                            <input class="form-control"
                                placeholder="Enter feature name" type="text" name="name" id="name"
                                value="{{ old('name', '') }}" required>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="required h4" for="description">Description</label>
                            <textarea class="form-control"
                                placeholder="Enter feature desciption" name="description" id="description"
                                value="{{ old('description', '') }}" maxlength="1000" required></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group mt-1">
                    <button class="btn btn-danger" type="submit">
                        {{ trans('global.save') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.js"
        integrity="sha512-wTIaZJCW/mkalkyQnuSiBodnM5SRT8tXJ3LkIUA/3vBJ01vWe5Ene7Fynicupjt4xqxZKXA97VgNBHvIf5WTvg=="
        crossorigin="anonymous">
    </script>
@endsection
