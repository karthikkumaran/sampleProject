@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.arobject.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.arobjects.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.arobject.fields.id') }}
                        </th>
                        <td>
                            {{ $arobject->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.arobject.fields.product') }}
                        </th>
                        <td>
                            {{ $arobject->product->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.arobject.fields.object') }}
                        </th>
                        <td>
                            @if($arobject->object)
                                <a href="{{ $arobject->object->getUrl() }}" target="_blank">
                                    {{ trans('global.view_file') }}
                                </a>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.arobject.fields.position') }}
                        </th>
                        <td>
                            {{ App\Arobject::POSITION_SELECT[$arobject->position] ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.arobjects.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection