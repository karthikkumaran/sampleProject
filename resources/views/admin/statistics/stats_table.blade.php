<h4>{{$title}} Statistics</h4>
<div class="table-responsive">
    <table class="table table-hover table-bordered bg-white" id="{{'stats_table_'.$label}}" data-label="{{$label}}">
        <thead class=" thead-light">
            {{-- <tr>
            @foreach($columns as $column)
                <th scope="col" class=" text-capitalize">{{$column}}</th>
            @endforeach
            </tr> --}}
        </thead>

    </table>
</div>
<script>
    var label = $('#stats_table_' + '{{$label}}').attr('data-label');
    var url = "{{route('admin.stats.getStatsData')}}" + "?label=" + label;
    var columns = [];
    $.ajax(url).done(function(data) {

        var keys = Object.keys(data.data[0]);
        for (var i = 0; i < keys.length; i++) {
            columns[i] = {
                'data': keys[i],
                'title': data.dataColumns[i],

            }
        }

        // console.log($('#stats_table_' + label));
        $('#stats_table_' + label).DataTable({
            processing: true,
            columns: columns,
            data: data.data,
            aaSorting: [
                [0, "desc"]
            ],
            bDestroy: true,
            fnRowCallback: function(nRow, aData, iDisplayIndex) {
                var row = $(nRow);
                row.attr("id", 'row' + aData['0']);
                $("td:first", nRow).html(iDisplayIndex + 1);
                return nRow
            }
        });
    });
</script>