@extends('layouts.admin')
@section('content')
@section ('styles')
<style>
    /* ::selection {
        /* color: white; */
    /* background: yellow; */
    /* }  */
</style>

@endsection

@php
if(empty($currency_settings['currency']))
$currency = "₹";
else
if($currency_settings['currency_selection'] == "currency_symbol")
$currency = App\User::CURRENCY_SYMBOLS[$currency_settings['currency']];
else
$currency = $currency_settings['currency'];
@endphp


<div id="store_analytics ">
    <div class="row d-flex justify-content-between">
        <div class="col-md-4 text-left">
            <p class="h3 mt-1 text-bold-700">Biz Insights (<span class="title-filter-by"></span>)</p>
        </div>
        <div class="col-md-2 col-11">
            <select class="m-1 float-md-right float-sm-right  form-control filter" id="filter">
                <option value="today">Today</option>
                <option value="yesterday">Yesterday</option>
                <option value="last_7_days" selected>Last 7 days</option>
                <option value="last_30_days">Last 30 days</option>
                <option value="thismonth">This Month</option>
                <option value="monthly">Monthly</option>
                <option value="yearly">Yearly</option>
            </select>
        </div>
    </div>
    <!-- buttons -->



    <div class="row w-100 d-flex justify-content-center metric-group ">
        <div class="col-md-4 col-4">
            <div class="card cursor-pointer  stat-card store-views" id="store-views-card" data-label="store-views">
                <div class="card-header d-flex justify-content-center pb-0">
                    <div>
                        <h3 class="text-bold-700 mb-0 store-views text-center" id="store-views-stat-value"> </h3>
                        <p>Store Views</p>
                    </div>
                    <!-- <div class="avatar bg-rgba-success p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-list text-success font-medium-5 orders-icon"></i>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>

        <div class="col-md-4 col-4">
            <div class="card cursor-pointer  stat-card dsales" id="dsales-card" data-label='dsales'>
                <div class="card-header d-flex justify-content-center pb-0">
                    <div>
                        <h3 class="text-bold-700 mb-0  dsales text-center" id="dsales-stat-value"> </h3>
                        <p>Overall Revenue</p>
                    </div>
                    <!-- <div class="avatar bg-rgba-success p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-list text-success font-medium-5 orders-icon"></i>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>


        <div class="col-md-4 col-4">
            <div class="card cursor-pointer  stat-card dorders" id="dorders-card" data-label='dorders'>
                <div class="card-header d-flex justify-content-center pb-0">
                    <div>
                        <h3 class="text-bold-700 mb-0  dorders text-center" id="dorders-stat-value"> </h3>
                        <p>Total # Orders</p>
                    </div>
                    <!-- <div class="avatar bg-rgba-success p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-list text-success font-medium-5 orders-icon"></i>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>

        <!-- <div class="col-md-3 col-3">
            <div class="card cursor-pointer  stat-card discounts" id="discounts-card" data-label='discounts'>
                <div class="card-header d-flex justify-content-center pb-0">
                    <div>
                        <h3 class="text-bold-700 mb-0 discounts text-center" id="discounts-stat-value"> </h3>
                        <p>Discounts</p>
                    </div> -->
                    <!-- <div class="avatar bg-rgba-success p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-list text-success font-medium-5 orders-icon"></i>
                        </div>
                    </div> -->
                <!-- </div>
            </div>
        </div> -->

    </div>


    <div class="row w-100 d-flex justify-content-center metric-group">

        <!-- <div class="col-md-3 col-3">
            <div class="card cursor-pointer  stat-card story-views" id="story-views-card" data-label='story-views'>
                <div class="card-header d-flex justify-content-center pb-0">
                    <div>
                        <h3 class="text-bold-700 mb-0 story-views text-center" id="story-views-stat-value"> </h3>
                        <p>Story Views</p>
                    </div> -->
                    <!-- <div class="avatar bg-rgba-success p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-list text-success font-medium-5 orders-icon"></i>
                        </div>
                    </div> -->
                <!-- </div>
            </div>
        </div> -->
        
        <!-- <div class="col-md-3 col-3">
            <div class="card cursor-pointer  stat-card story-views" id="story-views-card" data-label='story-views'>
                <div class="card-header d-flex justify-content-center pb-0">
                    <div>
                        <h3 class="text-bold-700 mb-0 story-views text-center" id="story-views-stat-value"> </h3>
                        <p>Story Views</p>
                    </div> -->
                    <!-- <div class="avatar bg-rgba-success p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-list text-success font-medium-5 orders-icon"></i>
                        </div>
                    </div> -->
                <!-- </div>
            </div>
        </div> -->

        <div class="col-md-4 col-4">
            <div class="card cursor-pointer  stat-card customers" id="customers-card" data-label='customers'>
                <div class="card-header d-flex justify-content-center pb-0">
                    <div>
                        <h3 class="text-bold-700 mb-0 customers text-center" id="customers-stat-value"> </h3>
                        <p># of Customers</p>
                    </div>
                    <!-- <div class="avatar bg-rgba-success p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-list text-success font-medium-5 orders-icon"></i>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>

        <div class="col-md-4 col-4">
            <div class="card cursor-pointer  stat-card avg-order-value" id="avg-order-value-card" data-label='avg-order-value'>
                <div class="card-header d-flex justify-content-center pb-0">
                    <div>
                        <h3 class="text-bold-700 mb-0  avg-order-value text-center" id="avg-order-value-stat-value"> </h3>
                        <p>Avg. Order Value</p>
                    </div>
                    <!-- <div class="avatar bg-rgba-success p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-list text-success font-medium-5 orders-icon"></i>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>


        <div class="col-md-4 col-4">
            <div class="card cursor-pointer  stat-card items-sold" id="items-sold-card" data-label='items-sold'>
                <div class="card-header d-flex justify-content-center pb-0">
                    <div>
                        <h3 class="text-bold-700 mb-0 items-sold text-center" id="items-sold-stat-value"> </h3>
                        <p># of Items Sold</p>
                    </div>
                    <!-- <div class="avatar bg-rgba-success p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-list text-success font-medium-5 orders-icon"></i>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>

        <!-- <div class="col-md-3 col-3">
            <div class="card cursor-pointer  stat-card conversion" id="conversion-card" data-label='conversion'>
                <div class="card-header d-flex justify-content-center pb-0">
                    <div>
                        <h3 class="text-bold-700 mb-0 conversion text-center" id="conversion-stat-value"> </h3>
                        <p>Conversion
                        </p>
                    </div>
                    <div class="avatar bg-rgba-success p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-list text-success font-medium-5 orders-icon"></i>
                        </div>
                    </div> -->
        <!-- </div> -->
        <!-- </div> -->
        <!-- </div>  -->
    </div>


</div>

<!-- chart -->
<div class="row w-100 d-flex justify-content-center m-0">

    <div class="col-md-12 col-12 d-flex justify-content-between flex-column text-right card">
        <!-- <div class="dropdown chart-dropdown filter-menu">
                <button class="btn btn-sm border-0 dropdown-toggle mt-1 filter-btn " type="button" id="dropdownItem5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Last 7 Days
                </button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownItem5">
                    <option class="dropdown-item filter-item" value="today">Today</option>
                    <option class="dropdown-item filter-item" value="yesterday">Yesterday</option>
                    <option class="dropdown-item filter-item" value="last_7_days">Last 7 days</option>
                    <option class="dropdown-item filter-item" value="last_30_days">Last 30 days</option>
                    <option class="dropdown-item filter-item" value="thismonth">This Month</option>
                    <option class="dropdown-item filter-item" value="monthly">Monthly</option>
                    <option class="dropdown-item filter-item" value="yearly">Yearly</option>
                </div>
            </div> -->
        <div class="chart-container height-300">
            <canvas id="chart"></canvas>
        </div>
    </div>
</div>

<!-- top -->
<div class="row w-100">
    <div class="col-md-6 col-12">
        <p class="h4 text-bold-700">Top Selling Products (<span class="title-filter-by"></span>)</p>
        <div class="card Tproduct-card p-1" style="max-height:80%;min-height:80%;">
            <div class="table-responsive">

                <table class="table table-striped table-hover" id="tproduct">
                    <thead class="thead-light">
                        <tr>
                            <th>Product Info</th>
                            <th>Orders</th>
                            <th>Revenue</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-12">
        <p class="h4  text-bold-700">Top Purchasing Customers (<span class="title-filter-by"></span>)</p>
        <div class="card Tcustomer-card p-1" style="max-height:80%;min-height:80%;">
            <div class="table-responsive">

                <table class="table table-striped table-hover" id="tcustomer">
                    <thead class="thead-light">
                        <tr>
                            <th>Customer Info</th>
                            <th>Orders</th>
                            <th>Revenue</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row w-100 d-flex justify-content-center m-0">

    <div class="col-md-12 col-12">
        <p class="h4"> <b>Orders by Hour</b></p>

        <div class="card">
            <!-- <div class="dropdown chart-dropdown filter-menu">
                <button class="btn btn-sm border-0 dropdown-toggle mt-1 filter-btn " type="button" id="dropdownItem5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Last 7 Days
                </button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownItem5">
                    <option class="dropdown-item filter-item" value="today">Today</option>
                    <option class="dropdown-item filter-item" value="yesterday">Yesterday</option>
                    <option class="dropdown-item filter-item" value="last_7_days">Last 7 days</option>
                    <option class="dropdown-item filter-item" value="last_30_days">Last 30 days</option>
                    <option class="dropdown-item filter-item" value="thismonth">This Month</option>
                    <option class="dropdown-item filter-item" value="monthly">Monthly</option>
                    <option class="dropdown-item filter-item" value="yearly">Yearly</option>
                </div>
            </div> -->
            <div class="chart-container height-300">
                <canvas id="hourchart"></canvas>
            </div>
        </div>
    </div>

    <!-- <div class="col-md-6 col-12">
        <p class="h4"> <b>Orders by Day</b></p>
        <div class="card"> -->
            <!-- <div class="dropdown chart-dropdown filter-menu">
                <button class="btn btn-sm border-0 dropdown-toggle mt-1 filter-btn " type="button" id="dropdownItem5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Last 7 Days
                </button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownItem5">
                    <option class="dropdown-item filter-item" value="today">Today</option>
                    <option class="dropdown-item filter-item" value="yesterday">Yesterday</option>
                    <option class="dropdown-item filter-item" value="last_7_days">Last 7 days</option>
                    <option class="dropdown-item filter-item" value="last_30_days">Last 30 days</option>
                    <option class="dropdown-item filter-item" value="thismonth">This Month</option>
                    <option class="dropdown-item filter-item" value="monthly">Monthly</option>
                    <option class="dropdown-item filter-item" value="yearly">Yearly</option>
                </div>
            </div> -->
            <!-- <div class="chart-container height-300">
                <canvas id="daychart"></canvas>
            </div>
        </div>

    </div> -->
</div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.bundle.js" integrity="sha512-zO8oeHCxetPn1Hd9PdDleg5Tw1bAaP0YmNvPY8CwcRyUk7d7/+nyElmFrB6f7vg4f7Fv4sui1mcep8RIEShczg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.bundle.min.js" integrity="sha512-SuxO9djzjML6b9w9/I07IWnLnQhgyYVSpHZx0JV97kGBfTIsUYlWflyuW4ypnvhBrslz1yJ3R+S14fdCWmSmSA==" crossorigin="anonymous"></script>
@parent

<script>
    var init_label = "";
    var config = null;
    var statChart = null;
    var flag = false;
    var filter = {
        "sdate": '',
        "edate": '',
        "month_year": '',
        'current': '',
    };


    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        // true for mobile device
        $('.metric-group').addClass('no-gutters mb-1').css('height', '100%');
        $('.stat-card').addClass('border-secondary  h-100').css('margin', '3px');
    }


    $(document).ready(function() {
        $(".stat-card").hover(
            function() {

                if (init_label != $(this).attr('data-label')) {
                    $(this).addClass('shadow-lg bg-secondary text-white');
                }
            },
            function() {
                if (init_label != $(this).attr('data-label')) {
                    $(this).removeClass('shadow-lg bg-secondary text-white');
                }
            }
        );

        // $('#filter').click();
        filter = {
            "sdate": '',
            "edate": '',
            "month_year": '',
            'current': 'last_7_days',
        };
        flag = true;
        $('.store-views').click();
        url = "{{route('admin.stats.getSalesstat')}}" + "?label=hour-order";
        $.ajax(url).done(function(data) {
            ordersChart(data);
        });
        url = "{{route('admin.stats.getSalesstat')}}" + "?label=day-order";
        $.ajax(url).done(function(data) {
            ordersChart(data);
        });
    });


    $(".stat-card").on('click', function() {

        label = $(this).attr('data-label');
        if (label && init_label == "") {
            addClass($(this));

        } else if (label != init_label) {

            removeClass($(this));
            addClass($(this));

        } else if (init_label == label) {

            return;
        }

        init_label = label;
        if (flag)
            showchart(label, filter);
        else
            showchart(label, false);

    });


    function addClass(label) {

        $(label).removeClass('bg-secondary');
        $(label.find('h3')).addClass('text-white');
        $(label).addClass('text-white');
        $(label).addClass('bg-primary');
        $(label).addClass('border-primary');

    }


    function removeClass(label) {

        $(label.find('h3')).removeClass('text-white');
        $(label).css('color', 'black');
        $("#" + init_label + "-card").removeClass('bg-primary border-primary');
        $("." + init_label).removeClass('text-white');

    }


    $('.filter').on('input', function() {
        value = $(this).val();

        filter = {
            "sdate": '',
            "edate": '',
            "month_year": '',
            'current': value
        }
        showchart(init_label, filter);


    });


    function showchart(label, filter) {

        var ctx = document.getElementById('chart').getContext("2d");
        var q = '';
        flag = false;
        if (filter) {
            flag = true;
            q = "&flag=" + flag + '&sdate=' + filter.sdate + '&edate=' + filter.edate + '&month_year=' + filter.month_year + '&current=' + filter.current;
        } else {
            flag = false;
            q = "&flag=" + flag;
        }
        var url = "{{route('admin.stats.getSalesstat')}}" + "?label=" + label + q;
        $.ajax(url).done(function(data) {
            if (data) {

                if (flag) {
                    $(".title-filter-by").html($(".filter option:selected").text());
                    // $(".top-product").html('Top Selling Products (' + $(".filter option:selected").text() + ')');
                    // $(".top-customer").html('Top Purchasing Customers (' + $(".filter option:selected").text() + ')');

                    $("#dsales-stat-value").html('{!!$currency!!}' + currencyFormatter(data.metrics.totalSales, 2));
                    $("#dorders-stat-value").html(data.metrics.ordersCount);
                    $("#store-views-stat-value").html(data.metrics.storeView);
                    $("#story-views-stat-value").html(data.metrics.story_view);
                    $("#discounts-stat-value").html('{!!$currency!!}' + currencyFormatter(data.metrics.totalDiscount, 2));
                    $("#customers-stat-value").html(data.metrics.no_of_Customers);
                    avg = parseInt(data.metrics.totalSales) / parseInt(data.metrics.ordersCount);
                    $("#avg-order-value-stat-value").html('{!!$currency!!}' + currencyFormatter(avg ? avg : 0, 2));
                    $('#items-sold-stat-value').html(data.metrics.no_of_Items);
                    datatable('tcustomer', data.metrics.top_customers);
                    datatable('tproduct', data.metrics.top_products);
                }

                total_price = data.total_price;
                data.data = Object.values(data.data);
                var dataset = [];

                temp = {};
                temp['label'] = data.label;
                temp['data'] = data.data;
                temp['backgroundColor'] = data.bgcolour[0];
                temp['borderColor'] = data.bordercolour[0];
                temp['fill'] = false;
                dataset[0] = temp;


                config = {
                    type: 'line',
                    data: {
                        labels: data.labels,
                        datasets: dataset,
                    },
                    options: {
                        tooltips: {
                            callbacks: {
                                title: function(tooltipItem, config) {
                                    // return data.axes[0] + " : " + tooltipItem[0].label;
                                },
                                afterTitle: function(tooltipItem, config) {

                                    if (!hasNull(total_price) && (init_label == 'dorders' || init_label == 'dcustomers')) {
                                        symbol = "";

                                        return ("Total Sales: " + symbol + currencyFormatter(total_price[tooltipItem[0].index], 2));
                                    }
                                },
                                label: function(tooltipItem, config) {

                                    if (init_label == "customers")
                                        return "No. of Customers : " + tooltipItem.value;
                                    else if (init_label == "dorders")
                                        return "No. of Orders : " + tooltipItem.value;
                                    else if (init_label == "store-views")
                                        return "Store Views : " + tooltipItem.value;
                                    else if (init_label == "discounts")
                                        return "Total Disicount : " + tooltipItem.value;
                                    else if (init_label == "dsales") {
                                        symbol = "";
                                        return "Total Sales : " + currencyFormatter(tooltipItem.value, 2);
                                    } else if (init_label == "customers")
                                        return "No. of Customers : " + tooltipItem.value;
                                    else if (init_label == "avg-order-value")
                                        return "Avg Order Value : " + currencyFormatter(tooltipItem.value, 2);
                                    else if (init_label == "items-sold")
                                        return "No. of Items : " + currencyFormatter(tooltipItem.value, 2);
                                    else if (init_label == "story-views")
                                        return "Story Views : " + tooltipItem.value;
                                },

                            },
                        },
                        default: {
                            ticks: {
                                min: 0
                            },
                        },

                        legend: {
                            display: false,
                            position: 'top',
                            labels: {
                                fontColor: "#000080",
                            }
                        },
                        title: {
                            text: (data.title).replace(/-/g, ' '),
                            display: true,
                            position: 'top',
                            fontSize: 16,
                        },
                        responsive: true,
                        maintainAspectRatio: false,
                        aspectRatio: 2,
                        scales: {
                            xAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                },
                                scaleLabel: {
                                    display: true,
                                    labelString: data.axes[0],
                                    fontSize: 16,
                                }
                            }],
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    precision: 0,
                                },
                                scaleLabel: {
                                    display: true,
                                    labelString: data.axes[1],
                                    fontSize: 16,
                                }
                            }]
                        }
                    }
                };


                change('line', ctx);


            }
        });

    }


    function change(newType, ctx) {

        if (statChart) {
            statChart.destroy();
        }
        // Chart.js modifies the object you pass in. Pass a copy of the object so we can use the original object later
        var temp = jQuery.extend(true, {}, config);
        temp.type = newType;
        statChart = new Chart(ctx, temp);

    }


    function ordersChart(data) {

        var chart = null;
        if (data.chart == 'hour-order')
            chart = document.getElementById('hourchart').getContext("2d");
        else
            chart = document.getElementById('daychart').getContext("2d");


        data.data = Object.values(data.data);
        var dataset = [];
        var length = data.label.length;


        for (var i = 0; i < length; i++) {
            temp = {};
            temp['yAxisID'] = data.label[i].replace(' ', '-');
            temp['label'] = data.label[i];
            temp['data'] = data.data[i];
            temp['backgroundColor'] = data.bgcolour[i];
            temp['borderColor'] = data.bordercolour[i];
            temp['fill'] = false;
            dataset[i] = temp;
        }
        var config_chart = {
            type: 'line',
            data: {
                labels: data.labels,
                datasets: dataset,
            },
            options: {
                tooltips: {
                    callbacks: {
                        title: function(tooltipItem, config) {
                            // console.log(tooltipItem[0].label);
                            // return "Tota Sales : " + currencyFormatter(tooltipItem[0].label);
                        },
                        label: function(tooltipItem, config) {
                            var label = config.datasets[tooltipItem.datasetIndex].label;
                            if (label == "Orders count")
                                return "No. of Orders : " + tooltipItem.value;
                            else if (label == "Revenue")
                                return "Revenue : " + currencyFormatter(tooltipItem.value);

                        },
                    },
                },
                // default: {
                //     ticks: {
                //         min: 0,
                //         beginAtZero: true,
                //     },
                // },

                legend: {
                    display: true,
                    position: 'top',
                    labels: {
                        fontColor: "#000080",
                    }
                },
                title: {
                    text: 'Revenue Vs Order Count',
                    display: true,
                    position: 'top',
                    fontSize: 16,
                },
                responsive: true,
                maintainAspectRatio: false,
                aspectRatio: 2,
                scales: {
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: data.axes[0],
                            fontSize: 16,
                        }
                    }],
                    yAxes: [{
                            // type: 'linear',
                            display: 'true',
                            id: data.label[1].replace(' ', '-'),
                            position: 'left',
                            ticks: {
                                callback: function(value, index, values) {

                                    return value == 0 ? 0 : currencyFormatter(value, 2);
                                },
                                precision: 0,
                                beginAtZero: true,

                            }
                        },
                        {
                            display: 'true',
                            // type: 'linear',
                            id: data.label[0].replace(' ', '-'),
                            position: 'right',
                            ticks: {
                                precision: 0,
                                beginAtZero: true,
                            }

                        }
                    ]
                }
            }
        };
        if (data.chart == 'hour-order')
            stat_hour = new Chart(chart, config_chart);
        else
            stat_day = new Chart(chart, config_chart);

    }


    function datatable(table_id, data) {

        var columns = [];
        if (data.original.data.length != 0) {
            var keys = Object.keys(data.original.data[0]);
            for (var i = 0; i < keys.length; i++) {
                columns[i] = {
                    'data': keys[i],
                    'title': keys[i],

                }
            }


            var datum = [];

            for (var i = 0; i < data.original.data.length; i++) {
                var temp = {};
                temp[keys[0]] = data.original.data[i][keys[0]].replace(/-/g, '<br/>');
                temp[keys[1]] = '{!!$currency!!}' + currencyFormatter(data.original.data[i][keys[1]], 2);
                temp[keys[2]] = data.original.data[i][keys[2]];
                datum[i] = temp;
            }

            $('#' + table_id).DataTable({
                searching: false,
                info: false,
                bPaginate: false,
                processing: true,
                columns: columns,
                data: datum,
                aaSorting: [
                    [0, "desc"]
                ],
                bDestroy: true,
            });
        } else {
            $('#' + table_id).DataTable().clear().destroy();
        }
    }
</script>

@endsection