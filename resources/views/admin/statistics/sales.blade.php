@extends('layouts.admin')
@section('content')
@section ('styles')
<style>
    /* canvas {
        /* border: 1px dotted red; */
    /* } */

    .chart-container {
        position: relative;
        margin: auto;
        height: 80vh;
        /* width: 80vw; */
    }

    .customer_search_box::after {
        display: none !important;
    }

    .ui-autocomplete {
        width: 200px;
    }

    .ui-menu-item .ui-menu-item-wrapper.ui-state-active {
        background: #D7DBDD !important;
        border-color: #D7DBDD;
    }
</style>

@endsection
<div class="card">


    <div hidden=true class="pl-1 pt-1 m-0 text-primary" id="back_btn" onclick="showchart('{{$stat}}', false);" style="cursor:pointer;"> <i class=" fa fa-arrow-left"></i> Back to All</div>
    <div class="p-1 m-0">
        <div class=" float-left float-sm-right float-md-right mb-1 mb-md-0 d-flex justify-content-end">
            <div class="p-0 m-0">
                <p class="btn fa fa-bar-chart fa-2x btn-info  p-1 m-0 chart" data-value="bar"></p>
                <p class="btn fa fa-line-chart fa-2x btn-info  p-1 m-0 chart" data-value="line"></p>
            </div>
            <div class="dropdown ">
                <button class="btn btn-secondary dropdown feather icon-filter ml-1" id="filter-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                </button>
                <div class="dropdown-menu  dropdown-menu-right scrollable-menu" role="menu" aria-labelledby="filter" style="min-width: 300px;">


                    <form class="p-1">
                        <!-- {{-- <label for="current-filter">Current</label> --}} -->
                        <select class="form-control" id="current-filter" class="current">

                            <option value=null>--select-- </option>
                            <option value="today">Today</option>
                            <option value="yesterday">Yesterday</option>
                            <option value="last_week">Last week</option>
                            <option value="last_30_days">Last 30 days</option>
                            <option value="thismonth">This Month</option>
                        </select>
                    </form>


                    <div class="row">
                        <div class="col">
                            <span class="vs-checkbox-con vs-checkbox-primary p-1">
                                <input type="radio" class="month_year" name="my_filter" value="month" id="month">
                                <span class="vs-checkbox">
                                    <span class="vs-checkbox--check">
                                        <i class="vs-icon feather icon-check m-0"></i>
                                    </span>
                                </span>
                                <span class="">Monthly</span>
                            </span>
                        </div>
                        <div class="col">
                            <span class="vs-checkbox-con vs-checkbox-primary p-1">
                                <input type="radio" class="month_year" name="my_filter" value="year" id="year">
                                <span class="vs-checkbox">
                                    <span class="vs-checkbox--check">
                                        <i class="vs-icon feather icon-check m-0"></i>
                                    </span>
                                </span>
                                <span class="">Yearly</span>
                            </span>
                        </div>
                    </div>


                    <div class="dropdown-divider"></div>


                    <span class="vs-checkbox-con vs-checkbox-primary p-1">
                        <input type="checkbox" class="cuctom" name="custom" id="custom">
                        <span class="vs-checkbox">
                            <span class="vs-checkbox--check">
                                <i class="vs-icon feather icon-check m-0"></i>
                            </span>
                        </span>
                        <span class="">Custom</span>
                    </span>




                    <div class="dropdown-divider"></div>
                    <ul class="list-unstyled custom-date">
                        <li class='px-1'>
                            <label for="start_date">Start Date</label>
                            <input class="form-control sdate_edate" type="date" name="start_date" id="start_date">
                        </li>

                        <li class="mt-1 px-1">
                            <label for="end_date">End Date</label>
                            <input class="form-control sdate_edate" type="date" name="end_date" id="end_date">
                        </li>
                        <div class="dropdown-divider"></div>
                    </ul>

                    <div class="p-1">
                        <button class="btn btn-md btn-primary " onclick="apply_filter()">Apply Filter</button>
                        <button class="btn btn-md btn-primary " onclick="clear_filter()">Clear Filter</button>
                    </div>

                </div>
            </div>
        </div>


        @if($stat=="customers")

        <div class="float-left  d-flex justify-content-left ">

            <fieldset class="form-group position-relative">
                <input type="text" class="form-control" autocomplete="off" placeholder="Search by customer" id="customer_search" oninput="dropdownSearchFunction()">
                <div class="form-control-position">
                    <i class="feather icon-search"></i>
                </div>
            </fieldset>

            <div class="col-md-5 form-group" hidden=true id="icustomer_div">
                <select class="custom-select icustomer_filter" id="icustomer_filter">
                    <!-- <option value=''>Select</option> -->
                    <option value='amount' selected>Amount</option>
                    <option value='no_of_products'>No. of products</option>
                    <option value='no_of_orders'>No. of orders</option>
                </select>
            </div>
        </div>



        @endif

    </div>

    <div class="p-0 m-0 chart-container">
        <canvas class="" id="chart"></canvas>
    </div>

</div>

@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.bundle.js" integrity="sha512-zO8oeHCxetPn1Hd9PdDleg5Tw1bAaP0YmNvPY8CwcRyUk7d7/+nyElmFrB6f7vg4f7Fv4sui1mcep8RIEShczg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.bundle.min.js" integrity="sha512-SuxO9djzjML6b9w9/I07IWnLnQhgyYVSpHZx0JV97kGBfTIsUYlWflyuW4ypnvhBrslz1yJ3R+S14fdCWmSmSA==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js" integrity="sha512-uto9mlQzrs59VwILcLiRYeLKPPbS/bT71da/OEBYEwcdNUk8jYIy+D176RYoop1Da+f9mvkYrmj5MCLZWEtQuA==" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" integrity="sha512-aOG0c6nPNzGk+5zjwyJaoRUgCdOrfSDhmMID2u4+OIslr0GjpLKo7Xm0Ao3xmpM4T8AmIouRkqwj1nrdVsLKEQ==" crossorigin="anonymous" />
<script>
    // $('#customer_reset').hide();
    var statChart = null;
    var config = null;
    var total_price = null;
    var flag = null;
    var init_label = "{{$stat}}";
    var customerData = "";

    showchart("{{$stat}}", false);



    function dropdownSearchFunction() {
        var input = document.getElementById("customer_search");


        if (input.value.length != 0) {
            // console.log(input.value.length);
            $.ajax({
                url: "{{route('admin.stats.getStatsData')}}" + '?q=' + input.value + '&label=customers',
                type: "get",
            }).done(function(data) {
                // $('#customer-menu').children().slice(1).remove();
                var datum = [];
                data.forEach((value, index) => {
                    var name = value.fname;
                    datum[index] = {
                        "label": "<b>First Name</b> : " + value.fname + "</br><b>Email </b> : " + value.email + "</br><b>Phone No</b> : " + value.mobileno,
                        "value": JSON.stringify(value),
                    };

                });

                $("#customer_search").autocomplete({
                    source: datum,
                    select: function(event, ui) {

                        $('#icustomer_div').attr('hidden', false);
                        $('#back_btn').attr('hidden', false);
                        customerData = ui.item.value;
                        getcustomerData("amount");
                        return;
                    },
                    close: function(event, ui) {
                        var customer = JSON.parse(customerData);
                        $('#customer_search').val('');
                    }
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    // console.log(ul, item, "hi");
                    return $("<li class='list-group-item rounded-0' style='padding:5px !important;'>")
                        .append($("<p class='text-dark p-0 '>").html(item.label))
                        .appendTo(ul);

                };

                // console.log($('.ui-autocomplete'));

            }).fail(function(jqXHR, ajaxOptions, thrownError) {

            });
            return;
        }
        if (input.value.length == 0) {

            showchart(init_label, false);
            $('#filter-btn').show();
            $("#customer_search").autocomplete("destroy");
            $("#customer_search").val('');


        }


    }

    function showchart(label, filter) {

        $('#icustomer_div').attr('hidden', true);
        $('#back_btn').attr('hidden', true);

        $('#filter-btn').show();

        var ctx = document.getElementById('chart').getContext("2d");
        var q = '';
        flag = false;
        if (filter) {
            flag = true;
            q = "&flag=" + flag + '&sdate=' + filter.sdate + '&edate=' + filter.edate + '&month_year=' + filter.month_year + '&current=' + filter.current;
        } else {
            flag = false;
            q = "&flag=" + flag;
        }
        var url = "{{route('admin.stats.getSalesstat')}}" + "?label=" + label + q;
        $.ajax(url).done(function(data) {
            if (data) {
                configChart(data);
                change('line');

            }
        });

    }

    function change(newType) {
        var ctx = document.getElementById('chart').getContext('2d');

        // Remove the old chart and all its event handles
        if (statChart) {
            statChart.destroy();
        }

        // Chart.js modifies the object you pass in. Pass a copy of the object so we can use the original object later
        var temp = jQuery.extend(true, {}, config);
        temp.type = newType;
        statChart = new Chart(ctx, temp);

    };


    $('#current-filter').on('input', function() {
        $('.sdate_edate').attr('disabled', 'disabled');
        $('.month_year').attr('disabled', 'disabled');
    });

    $('.month_year').on('input', function() {
        $('#current-filter').attr('disabled', 'disabled');
        $('.sdate_edate').attr('disabled', 'disabled');
        $("#custom").attr('disabled', 'disabled');

    });

    $('.sdate_edate').on('input', function() {
        $('#current-filter').attr('disabled', 'disabled');
        $('.month_year').attr('disabled', 'disabled');

    });

    if ($(".custom").prop("checked")) {
        $('.custom-date').show();
    } else {
        $('.custom-date').hide();
    }


    $("#custom").on("click", function() {
        if ($("#custom").prop("checked")) {
            $('#start_date').prop("required", true);
            $('.custom-date').show();
            $('.month_year').attr('disabled', 'disabled');
            $('#current-filter').val('');
            $('#current-filter').attr('disabled', 'disabled');
        } else {
            $('.custom-date').hide();
            $('.month_year').removeAttr('disabled');
            $('#current-filter').removeAttr('disabled');
            $('#start_date').prop("required", false);
        }
    });

    function apply_filter() {

        $('#filter-btn').addClass('btn-info');
        var current = $('#current-filter').val() == "null" ? '' : $('#current-filter').val();
        var month_year = $('input[name="my_filter"]:checked').val() == undefined ? '' : $('input[name="my_filter"]:checked').val();
        var sdate = $('#start_date').val();
        var edate = $('#end_date').val();

        showchart(init_label, {
            "sdate": sdate,
            "edate": edate,
            "month_year": month_year,
            'current': current
        });
    }

    function clear_filter() {

        $('#filter-btn').removeClass('btn-info');
        $('.month_year').removeAttr('disabled');
        $('#current-filter').removeAttr('disabled');
        $('.sdate_edate').removeAttr('disabled');
        $("#custom").removeAttr('disabled');

        $('input[name="my_filter"]').prop("checked", false);
        $('#current-filter').val('');
        $('.sdate_edate').val('');
        $("#custom").prop("checked", false);

        if (flag) {
            // console.log(input.value.length);
            $('#customer-menu').children().slice(1).remove();
            showchart(init_label, false);
            $('#filter-btn').show();
            $(".customer_search_box:first-child").text("Search here");
            $(".customer_search_box:first-child").append('<i class="fa fa-search float-right"></i>');
            $(".customer_search_box:first-child").val();

        }
    }

    $('.chart').on('click', function() {
        var type = $(this).attr('data-value');
        change(type);
    });

    function formatMoney(number, decPlaces, decSep, thouSep) 
    {
        decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
            decSep = typeof decSep === "undefined" ? "." : decSep;
        thouSep = typeof thouSep === "undefined" ? "," : thouSep;
        var sign = number < 0 ? "-" : "";
        var i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
        var j = (j = i.length) > 3 ? j % 3 : 0;

        return sign +
            (j ? i.substr(0, j) + thouSep : "") +
            i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
            (decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
    }





    function getcustomerData(filter) {
        $('.loading').show();
        q = 'q=' + customerData + '&filter=' + filter;
        $.ajax({
            url: "{{route('admin.stats.getCustomerstat')}}" + '?' + q,
            type: "get",
        }).done(function(data) {

            $('#filter-btn').hide();
            configChart(data);
            change('line');

        }).fail(function(jqXHR, ajaxOptions, thrownError) {

        });
    }

    $('#icustomer_filter').on('input', function() {
        var filter = $('#icustomer_filter').val();
        // console.log(filter);
        getcustomerData(filter);
    });



    function configChart(data) {


        total_price = data.total_price;
        data.data = Object.values(data.data);

        var dataset = [];

        temp = {};
        temp['label'] = data.label;
        temp['data'] = data.data;
        temp['backgroundColor'] = data.bgcolour[0];
        temp['borderColor'] = data.bordercolour[0];
        temp['fill'] = false;
        dataset[0] = temp;

        config = {
            type: 'line',
            data: {
                labels: data.labels,
                datasets: dataset,
            },
            options: {
                tooltips: {
                    intersect: true,
                    callbacks: {
                        title: function(tooltipItem, config) {
                            // return data.axes[0] + " : " + tooltipItem[0].label;
                        },
                        afterTitle: function(tooltipItem, config) {
                            if (total_price != null)
                                return "Total Amount: " + formatMoney(total_price[tooltipItem[0].index]);
                        },
                        label: function(tooltipItem, config) {

                            if (data.icustomer == "filter") {
                                if ($('#icustomer_filter').val() == "amount") {
                                    return "Total Amount" + " : " + formatMoney(tooltipItem.value);
                                } else if ($('#icustomer_filter').val() == "no_of_products") {
                                    return "No. of Products" + " : " + tooltipItem.value;
                                } else if ($('#icustomer_filter').val() == "no_of_orders") {
                                    return "No. of Orders" + " : " + tooltipItem.value;
                                }
                            }

                            if (init_label == "customers")
                                return "No. of Customers : " + tooltipItem.value;
                            else if (init_label == "orders")
                                return "No. of Orders : " + tooltipItem.value;

                        },

                    },
                },
                default: {
                    ticks: {
                        min: 0
                    },
                },

                legend: {
                    display: false,
                    position: 'top',
                    labels: {
                        fontColor: "#000080",
                    }
                },
                title: {
                    text: data.title,
                    display: true,
                    position: 'top',
                    fontSize: 16,
                },
                responsive: true,
                maintainAspectRatio: false,
                aspectRatio: 2,
                scales: {
                    xAxes: [{
                        ticks: {
                            // min: 0,
                            // max: 100,
                            // stepSize: 10 / 4,
                            beginAtZero: true,
                        },
                        scaleLabel: {
                            display: true,
                            labelString: data.axes[0],
                            fontSize: 16,
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            // min: 0,
                            // max: 100,
                            precision: 0,
                        },
                        scaleLabel: {
                            display: true,
                            labelString: data.axes[1],
                            fontSize: 16,
                        }
                    }]
                }
            }
        };
    }
</script>

@endsection