@extends('layouts.admin')
@section('content')
@section ('styles')
<style>
  canvas {
    /* border: 1px dotted red; */
  }

  .chart-container {
    position: relative;
    margin: auto;
    height: 80vh;
    /* width: 80vw; */
  }
</style>

@endsection
<div class="card">


  <div class="p-1 m-0">
    <div class="float-left">
      @foreach($labels as $label)
      <button class=" btn  btn-primary p-1 stat  stat_{{$label}}" type="button" data-target="#{{$label}}" aria-expanded='false' aria-controls="{{$label}}" onclick="showstats('{{$label}}')">
        {{str_replace('_', ' ', $label)}}
      </button>
      @endforeach
    </div>


    <div class="float-left float-sm-right float-md-right d-flex justify-content-right">

      <div class="p-0 m-0">
        <p class="btn fa fa-bar-chart fa-2x btn-info   m-sm-0 p-1 mt-1 chart" data-value="bar"></p>
        <p class="btn fa fa-line-chart fa-2x btn-info   m-sm-0 p-1 mt-1 chart" data-value="line"></p>
      </div>
      <div class="dropdown ">
        <button class="btn btn-secondary dropdown feather icon-filter m-sm-0 ml-sm-1 mt-1 ml-1" id="filter-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

        </button>
        <div class="dropdown-menu  dropdown-menu-right scrollable-menu" role="menu" aria-labelledby="filter" style="min-width: 300px;">


          <form class="p-1">
            {{-- <label for="current-filter">Current</label> --}}
            <select class="form-control" id="current-filter" class="current">
              <option value=null>--select-- </option>
              <option value="today">Today</option>
              <option value="yesterday">Yesterday</option>
              <option value="last_week">Last week</option>
              <option value="last_30_days">Last 30 days</option>
              <option value="thismonth">This Month</option>
            </select>
          </form>


          <div class="row">
            <div class="col">
              <span class="vs-checkbox-con vs-checkbox-primary p-1">
                <input type="radio" class="month_year" name="my_filter" value="month" id="month">
                <span class="vs-checkbox">
                  <span class="vs-checkbox--check">
                    <i class="vs-icon feather icon-check m-0"></i>
                  </span>
                </span>
                <span class="">Monthly</span>
              </span>
            </div>
            <div class="col">
              <span class="vs-checkbox-con vs-checkbox-primary p-1">
                <input type="radio" class="month_year" name="my_filter" value="year" id="year">
                <span class="vs-checkbox">
                  <span class="vs-checkbox--check">
                    <i class="vs-icon feather icon-check m-0"></i>
                  </span>
                </span>
                <span class="">Yearly</span>
              </span>
            </div>
          </div>


          <div class="dropdown-divider"></div>


          <span class="vs-checkbox-con vs-checkbox-primary p-1">
            <input type="checkbox" class="cuctom" name="custom" id="custom">
            <span class="vs-checkbox">
              <span class="vs-checkbox--check">
                <i class="vs-icon feather icon-check m-0"></i>
              </span>
            </span>
            <span class="">Custom</span>
          </span>




          <div class="dropdown-divider"></div>
          <ul class="list-unstyled custom-date">
            <li class='px-1'>
              <label for="start_date">Start Date</label>
              <input class="form-control sdate_edate" type="date" name="start_date" id="start_date">
            </li>

            <li class="mt-1 px-1">
              <label for="end_date">End Date</label>
              <input class="form-control sdate_edate" type="date" name="end_date" id="end_date">
            </li>
            <div class="dropdown-divider"></div>
          </ul>

          <div class="p-1">
            <button class="btn btn-md btn-primary " onclick="apply_filter()">Apply Filter</button>
            <button class="btn btn-md btn-primary " onclick="clear_filter()">Clear Filter</button>
          </div>

        </div>
      </div>
    </div>
  </div>
  <div class="d-none uv-count mr-1 m-0" id="uv-count">

  </div>


  <div class="p-0 m-0 chart-container">
    <canvas class="p-0 m-0 " id="chart"></canvas>
  </div>
</div>



@foreach($labels as $label)
<div class="collapse {{$label}}" id="{{$label}}">
  <div class="card card-body" id="{{$label}}">
  </div>
</div>
@endforeach

@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.bundle.js" integrity="sha512-zO8oeHCxetPn1Hd9PdDleg5Tw1bAaP0YmNvPY8CwcRyUk7d7/+nyElmFrB6f7vg4f7Fv4sui1mcep8RIEShczg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.bundle.min.js" integrity="sha512-SuxO9djzjML6b9w9/I07IWnLnQhgyYVSpHZx0JV97kGBfTIsUYlWflyuW4ypnvhBrslz1yJ3R+S14fdCWmSmSA==" crossorigin="anonymous"></script>


<script>
  var init_label = "";
  var statChart = null;
  var config = null;
  var flag = null;
  showstats("Events");
  $('.Events').collapse('show');


  function showstats(label) {

    if (label != init_label) {

      $('.' + label).collapse('toggle');
      $('.stat_' + label).addClass('btn-info');

    }
    if (init_label == label) {

      $('.' + init_label).collapse({
        toggle: false
      });

    } else if (init_label) {

      $('.stat_' + init_label).removeClass('btn-info');
      $('.stat_' + label).addClass('btn-info');

      $('.' + init_label).collapse('hide');

    }
    init_label = label;

    if (label != "Events") {

      var url = "{{route('admin.stats.getTableName')}}" + "?label=" + label;
      $('#' + label).load(url, function() {
        
      });

      if (label == "Pages") {

        document.getElementById('chart').remove(); // this is my <canvas> element
        $('.chart-container').append('<canvas id="chart"><canvas>');
        $('.chart-container').hide();

        if ($('.uv')) {
          $('.uv-count').removeClass(' d-flex justify-content-end  uv-count-show');
          $('.uv-count').addClass('d-none ');
          $('.uv-count').children().empty();

        }

      }

      if (label == "Unique_Visitors") {
        flag = false;
        clear_filter();
        showchart(label, null);
      }

    } else {
      $('#' + label).hide();
      flag = false;
      clear_filter();
      showchart(label, null);

    }
  }



  function showchart(label, filter) {


    $('.chart-container').show();
    var ctx = document.getElementById('chart').getContext("2d");
    var q = '';
    flag = false;
    if (filter) {
      flag = true;
      q = "&flag=" + flag + '&sdate=' + filter.sdate + '&edate=' + filter.edate + '&month_year=' + filter.month_year + '&current=' + filter.current;
    } else {
      flag = false;
      q = "&flag=" + flag;
    }
    var url = "{{route('admin.stats.getStatChart')}}" + "?label=" + label + q;
    $.ajax(url).done(function(data) {
      if (data) {

        data.data = Object.values(data.data);
        var dataset = [];
        var length = data.label.length == 0 ? 1 : data.label.length;
        var legend_display = data.data.length == 0 ? false : true;
        // console.log(legend_display);

        for (var i = 0; i < length; i++) {
          temp = {};
          if (data.uniquevisitor == null) {
            temp['label'] = data.label[i];
            temp['data'] = data.data[i];
            temp['backgroundColor'] = data.bgcolour[i];
            temp['borderColor'] = data.bordercolour[i];
          } else {
            temp['label'] = data.label;
            temp['data'] = data.data;
            temp['backgroundColor'] = 'rgba(54, 162, 235, 0.7)';
            temp['borderColor'] = 'rgb(54, 162, 235)';
          }

          temp['fill'] = false;
          dataset[i] = temp;
        }


        config = {
          type: 'line',
          data: {
            labels: data.labels,
            datasets: dataset,
          },
          options: {
            default: {
              ticks: {
                min: 0
              },
            },
            legend: {
              display: data.uniquevisitor == null && legend_display ? true : false,
              position: 'top',
              labels: {
                fontColor: "#000080",
              }
            },
            title: {
              text: data.title.replace('_', ' '),
              display: true,
              position: 'top',
              fontSize: 16,
            },
            responsive: true,
            maintainAspectRatio: false,
            aspectRatio: 2,
            scales: {
              xAxes: [{
                // ticks: {
                //   beginAtZero: true,
                //   display: true

                // },
                scaleLabel: {
                  display: true,
                  labelString: data.axes[0],
                  fontSize: 16,
                }
              }],
              yAxes: [{
                ticks: {
                  beginAtZero: true,
                  // min: 0,
                  // max: 100,
                  precision: 0,
                },
                scaleLabel: {
                  display: true,
                  labelString: data.axes[1],
                  fontSize: 16,
                }
              }]
            }
          }
        };



        if (data.uniquevisitor != null) {
          if (!$('#uv-count').hasClass('uv-count-show')) {
            $('.uv-count').removeClass('d-none');
            $('.uv-count').addClass('d-flex justify-content-end uv-count-show');
            $('.uv-count').append("<p class=' uv h5'><b>Total Unique Visitors :</b>" + data.uniquevisitor + "</p>");
          }
        } else {
          if ($('.uv')) {
            $('.uv-count').removeClass(' d-flex justify-content-end  uv-count-show');
            $('.uv-count').addClass('d-none ');
            $('.uv-count').children().empty();
          }
        }

        change('line');

      } else {
        document.getElementById('chart').remove(); // this is my <canvas> element
        $('.chart-container').append('<canvas id="chart"><canvas>');
        $('.chart-container').hide();

      }
    });

  }

  function change(newType) {
    var ctx = document.getElementById('chart').getContext('2d');

    // Remove the old chart and all its event handles
    if (statChart) {
      statChart.destroy();
    }

    // Chart.js modifies the object you pass in. Pass a copy of the object so we can use the original object later
    var temp = jQuery.extend(true, {}, config);
    temp.type = newType;
    statChart = new Chart(ctx, temp);

  };


  $('#current-filter').on('input', function() {
    $('.sdate_edate').attr('disabled', 'disabled');
    $('.month_year').attr('disabled', 'disabled');
  });

  $('.month_year').on('input', function() {
    $('#current-filter').attr('disabled', 'disabled');
    $('.sdate_edate').attr('disabled', 'disabled');
    $("#custom").attr('disabled', 'disabled');

  });

  $('.sdate_edate').on('input', function() {
    $('#current-filter').attr('disabled', 'disabled');
    $('.month_year').attr('disabled', 'disabled');

  });

  if ($(".custom").prop("checked")) {
    $('.custom-date').show();
  } else {
    $('.custom-date').hide();
  }


  $("#custom").on("click", function() {
    if ($("#custom").prop("checked")) {
      $('#start_date').prop("required", true);
      $('.custom-date').show();
      $('.month_year').attr('disabled', 'disabled');
      $('#current-filter').val('');
      $('#current-filter').attr('disabled', 'disabled');
    } else {
      $('.custom-date').hide();
      $('.month_year').removeAttr('disabled');
      $('#current-filter').removeAttr('disabled');
      $('#start_date').prop("required", false);
    }
  });

  function apply_filter() {

    $('#filter-btn').addClass('btn-info');
    var current = $('#current-filter').val() == "null" ? '' : $('#current-filter').val();
    var month_year = $('input[name="my_filter"]:checked').val() == undefined ? '' : $('input[name="my_filter"]:checked').val();
    var sdate = $('#start_date').val();
    var edate = $('#end_date').val();

    showchart(init_label, {
      "sdate": sdate,
      "edate": edate,
      "month_year": month_year,
      'current': current
    });
  }

  function clear_filter() {

    $('#filter-btn').removeClass('btn-info');
    $('.month_year').removeAttr('disabled');
    $('#current-filter').removeAttr('disabled');
    $('.sdate_edate').removeAttr('disabled');
    $("#custom").removeAttr('disabled');

    $('input[name="my_filter"]').prop("checked", false);
    $('#current-filter').val('');
    $('.sdate_edate').val('');
    $("#custom").prop("checked", false);

    if (flag) {
      showchart(init_label, null);
    }
  }

  $('.chart').on('click', function() {
    // console.log($(this).attr('data-value'));
    var type = $(this).attr('data-value');
    change(type);
  });
</script>

@endsection