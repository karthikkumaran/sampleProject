@extends('layouts.admin')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.css"
    integrity="sha512-uKwYJOyykD83YchxJbUxxbn8UcKAQBu+1hcLDRKZ9VtWfpMb1iYfJ74/UIjXQXWASwSzulZEC1SFGj+cslZh7Q=="
    crossorigin="anonymous" />
<style>
    div.tagsinput {
        border: 1px solid #CCC;
        background: #FFF;
        /* padding: 5px; */
        width: 300px;
        /* height: 100px; */
        overflow-y: visible;
    }

    div.tagsinput span.tag {
        border: 1px solid #1709da;
        background: #0814c2;
        color: #ffffff;
        font-size: 18px;
        font-weight: 500;
    }

    div.tagsinput span.tag a {
        font-weight: 700;
        color: white;
        text-decoration: none;
        font-size: 19px;
        margin-bottom: 0;
        padding: 0;
    }

</style>
@section('content')

    <div class="show_alert">

    </div>

    <div class="card">

        <div class="card-header">
            <div>
                <a class="btn btn-md  btn-outline-secondary " href="{{ route('admin.product-attributes.index') }}">
                    Back Attribute List
                </a>
                <br>
                <br>
                {{-- {{ trans('global.create') }}
                {{ trans('cruds.productAttribute.title_singular') }} --}}
            </div>
        </div>

        <div class="card-body">
            <form method="POST"
                action="{{ route('admin.product-attributes.update', [$attributes[0]->attribute_group_name]) }}"
                enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label class="required h4" for="group">Attribute Group Name</label>
                    <input class="form-control {{ $errors->has('group') ? 'is-invalid' : '' }}"
                        placeholder="Enter attribute's group name" type="text" name="group" id="group"
                        value="{{ old('group', $attributes[0]->attribute_group_name) }}" required>
                    @if ($errors->has('group'))
                        <div class="invalid-feedback">
                            {{ $errors->first('group') }}
                        </div>
                    @endif
                </div>

                <label class="h3"> {{ trans('cruds.productAttribute.title') }}</label>
                <div class="row mb-1 ">
                    <label class="col col-md-4 text-center h4 d-none d-sm-block" for="label">Attribute Name</label>
                    <label class="col col-md-3 text-center h4 d-none d-sm-block" for="type">Control Type</label>
                    <label class="col col-md-4 text-center h4 d-none d-sm-block" for="value">Control Value</label>
                    <div class="col-md-1 btn btn-xs col-2 ml-1 ml-sm-0  btn-success btn-icon px-md-1 add_attribute"
                        id="add_attribute">
                        <i class="fa fa-plus fa-2x"></i>
                    </div>
                </div>


                <div id="attribute_section">
                    @foreach ($attributes as $attribute)


                        <div class="row attr mb-1" id="attr_{{ $attribute->id }}">

                            <input type="hidden" value="{{ $attribute->slug }}" name="slug[]" />

                            <div class="form-group  col-md-4 col-sm-12  ">


                                <input
                                    class="form-control {{ $errors->has('label') ? 'is-invalid' : '' }} label{{ $attribute->id }}"
                                    placeholder="Enter attribute's name" type="text" name="label[ ]"
                                    id="label{{ $attribute->id }}" value="{{ old('$attribute->label', $attribute->label) }}"
                                    required>

                            </div>

                            <div class="form-group  col-md-3 col-sm-12   ">
                                <select class="custom-select form-control {{ $errors->has('type') ? 'is-invalid' : '' }} 
                                type" data-id="{{ $attribute->id }}" name="type[]" id="type{{ $attribute->id }}" required>

                                    <option value="">Select control type</option>
                                    <option value="textbox" {{ $attribute->type == 'textbox' ? 'selected' : ' ' }}>Textbox
                                    </option>
                                    <option value="checkbox" {{ $attribute->type == 'checkbox' ? 'selected' : ' ' }}>
                                        Checkbox</option>
                                    <option value="radio_button"
                                        {{ $attribute->type == 'radio_button' ? 'selected' : ' ' }}>Radio button</option>
                                    <option value="dropdown" {{ $attribute->type == 'dropdown' ? 'selected' : ' ' }}>
                                        Dropdown</option>
                                </select>

                                <span class="help-block">{{ trans('cruds.productAttribute.fields.type_helper') }}</span>
                            </div>

                            <div class="form-group values{{ $attribute->id }} col-md-4 col-sm-12  ">

                                <input class="form-control {{ $errors->has('value') ? 'is-invalid' : '' }} value"
                                    data-id={{ $attribute->id }} type="text" name="value[]" id='value{{ $attribute->id }}'
                                    value="{{ old('$attribute->values', $attribute->values) }}" required>
                                <span
                                    class="help-block valuehelper{{ $attribute->id }}">{{ trans('cruds.productAttribute.fields.value_helper') }}</span>

                            </div>

                            {{-- <div class="col-md-1 text-right px-0 form-group">
                                <button id="delete_attribute_1" class="btn btn-danger btn-sm "><i
                                        class="fa  fa-trash fa-2x"></i></button>
                            </div> --}}
                                                                      
                            <div class="col-md-1 col-2  btn btn-danger h-50 btn-sm ml-1 ml-sm-0 fixed delete_attribute_{{ $attribute->id }}"
                                id="delete_attribute_{{ $attribute->id }}" onclick="deleteAttribute($(this))" data-attributeid="{{$attribute->id }}"data-id="{{$attribute->attribute_id}}">

                                <i class="fa fa-trash fa-2x"></i>
                            </div>


                        </div>
                    @endforeach
                </div>
                <div class="form-group mt-1">
                    <button class="btn btn-danger" type="submit">
                        {{ trans('global.save') }}
                    </button>
                </div>
            </form>
        </div>
    </div>



@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.js"
        integrity="sha512-wTIaZJCW/mkalkyQnuSiBodnM5SRT8tXJ3LkIUA/3vBJ01vWe5Ene7Fynicupjt4xqxZKXA97VgNBHvIf5WTvg=="
        crossorigin="anonymous"></script>

    <script>
        function delete_attribute(id) {
            if ($('#attribute_section > .row').length == 1) {
                $('.show_alert').empty();
                $('.show_alert').append(
                    "<div class='alert alert-danger p-2 alert-validation-msg alert-dismissable' role='alert' ><i class='feather icon-info mr-1 align-middle fa-2x '></i><span class='h4 text-danger'>Cannot delete this item</span><button type='button' class='close' data-dismiss='alert' aria-hidden='true' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>"
                );
            } else {
                $('#attr_'+id).remove();
            }
        }

        function taginputs(value) {
            id = value.dataset.id;
            if (value.value != "textbox") {
                $('.values' + id).children().show();
                $('#value' + id).hide();
                // $('.valuehelper' + id).show();

                $('#value' + id).tagsInput({
                    'height': '100%',
                    'width': '100%',
                    'defaultText': '',
                });
            } else {
                $('#value' + id).removeAttr('required');
                $('.values' + id).children().hide();


            }

        }

        tag();
        var attr_id = $('#attribute_section > div').length;

        function tag() {
            $.each($('.type'), function(key, value) {
                taginputs(value);
            });
        }

        $('.type').on('input', function() {
            id = this.dataset.id;
            taginputs(document.getElementById(this.id));
        })


        $('.add_attribute').on("click", function() {

            attr_id += 1;
            url = "{{ route('admin.productAttributes.add', ':attr_id') }}";
            url = url.replace(':attr_id', attr_id);
            var attr_row = $('<div>').load(url, function(row) {
                $('#attribute_section').append(row);
            });

            // console.log($('#attribute_section > div').length ,url);

            if ($('#attribute_section > div').length == 1)
                $('#delete_attribute_1').show();

        });

@can('product_attribute_delete')
function deleteAttribute(attribute) {
    if($('#attribute_section > .row').length != 1)
    {
        // console.log(attribute.attr('id'));
        var id = attribute.attr('data-id');
        var url = "{{route('admin.productAttributes.attributedestroy',':id')}}";
        url = url.replace(':id', id);
        swal.fire({
            title: 'Are you sure to delete this Attribute?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, Delete it!',
            cancelButtonText: 'No, Cancel!',
            confirmButtonClass: 'btn btn-danger',
            cancelButtonClass: 'btn btn-light',
        }).then(function (result) {
                if (result.value) {
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                    success: function (response_sub) {
                        // console.log(response_sub.status)
                        if(response_sub.status == "warning") {
                            Swal.fire({
                            type: "warning",
                            title: 'Delete',
                            text: "This attribute is added to one of the product, Are you sure to delete this attribute?",
                            showCancelButton: true,
                            confirmButtonText: 'Yes, Delete it',
                            cancelButtonText: 'No, Cancel',
                            confirmButtonClass: 'btn btn-danger',
                            cancelButtonClass: 'btn btn-light',
                            }).then((result) => {
                               if(result.value)
                               {
                                   $.ajax({
                                      url:url+"?flag="+true,
                                      type:"POST",
                                      data:{
                                          '_method':'DELETE'
                                      },
                                      success:function(response)
                                      {
                                        if(response.status=="success"){
                                           delete_success(attribute);
                                          }
                                      },error: function (res) {
                                            Swal.fire({
                                            type: "error",
                                            title: 'Delete',
                                            text: "Can't delete this attribute.",
                                            confirmButtonClass: 'btn btn-danger',
                                            }).then((result) => {
                                            // window.location.reload();
                                            });
                                        }
                                   });
                               }
                            });
                        }
                        else if(response_sub.status == "success") 
                        {
                            delete_success(attribute);
                        }
                    }
                }); 
            }
        });

    }else
    {
        delete_attribute(attribute.attr("data-attributeid"));
    }
}
@endcan

function delete_success(attribute)
{
    Swal.fire({
        type: "success",
        title: 'Delete',
        text: 'Attribute has been deleted.',
        confirmButtonClass: 'btn btn-success',
        }).then((result) => {
                delete_attribute(attribute.attr("data-attributeid"));                                                
        });
}
    </script>
@endsection
