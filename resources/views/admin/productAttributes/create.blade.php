@extends('layouts.admin')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.css"
    integrity="sha512-uKwYJOyykD83YchxJbUxxbn8UcKAQBu+1hcLDRKZ9VtWfpMb1iYfJ74/UIjXQXWASwSzulZEC1SFGj+cslZh7Q=="
    crossorigin="anonymous" />
<style>
    div.tagsinput {
        border: 1px solid #CCC;
        background: #FFF;
        /* padding: 5px; */
        width: 300px;
        /* height: 100px; */
        overflow-y: visible;
    }

    div.tagsinput span.tag {
        border: 1px solid #1709da;
        background: #0814c2;
        color: #ffffff;
        font-size: 18px;
        font-weight: 500;
    }

    div.tagsinput span.tag a {
        font-weight: 700;
        color: white;
        text-decoration: none;
        font-size: 19px;
        margin-bottom: 0;
        padding: 0;
    }

</style>
@section('content')

    <div class="show_alert">

    </div>

    <div class="card">

        <div class="card-header">
            <div>
                <a class="btn btn-md  btn-outline-secondary " href="{{ route('admin.product-attributes.index') }}">
                    Back Attribute List
                </a>
                <br>
                <br>
                {{-- {{ trans('global.create') }}
                {{ trans('cruds.productAttribute.title_singular') }} --}}
            </div>
        </div>

        <div class="card-body">
            <form method="POST" action="{{ route('admin.product-attributes.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label class="required h4" for="group">Attribute Group Name</label>
                    <input class="form-control {{ $errors->has('group') ? 'is-invalid' : '' }}"
                        placeholder="Enter attribute's group name" type="text" name="group" id="group"
                        value="{{ old('group', '') }}" required>
                    @if ($errors->has('group'))
                        <div class="invalid-feedback">
                            {{ $errors->first('group') }}
                        </div>
                    @endif
                </div>

                <label class="h3"> {{ trans('cruds.productAttribute.title') }}</label>
                <div class="row mb-2">
                    <label class="col col-md-4 text-center h4 d-none d-sm-block" for="label">Attribute Name</label>
                    <label class="col col-md-3 text-center h4 d-none d-sm-block" for="type">Control Type</label>
                    <label class="col col-md-4 text-center h4 d-none d-sm-block" for="value">Control Value</label>
                    <div class="col-md-1 btn btn-xs col-2 ml-1 ml-sm-0  btn-success btn-icon px-md-1 add_attribute"
                        id="add_attribute">
                        <i class="fa fa-plus fa-2x"></i>
                    </div>
                </div>


                <div id="attribute_section">

                </div>
                <div class="form-group mt-1">
                    <button class="btn btn-danger" type="submit">
                        {{ trans('global.save') }}
                    </button>
                </div>
            </form>
        </div>
    </div>

@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.js"
        integrity="sha512-wTIaZJCW/mkalkyQnuSiBodnM5SRT8tXJ3LkIUA/3vBJ01vWe5Ene7Fynicupjt4xqxZKXA97VgNBHvIf5WTvg=="
        crossorigin="anonymous"></script>

    <script>
        var attr_id = 0;

        function add() {
            attr_id += 1;
            url = "{{ route('admin.productAttributes.add', ':attr_id') }}";
            url = url.replace(':attr_id', attr_id);
            var attr_row = $('<div>').load(url, function(row) {
                $('#attribute_section').append(row);

                //     if ($('#attribute_section > div').length == 1)
                //         $('#delete_attribute_1').show();

            });
        }
        add();

        $('.add_attribute').on("click", function() {
            add();
        });



    </script>
@endsection
