@extends('layouts.admin')
@section('content')
@can('product_attribute_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.product-attributes.create") }}">
            <i class="feather icon-plus"></i> {{ trans('global.add') }} {{trans('cruds.productAttribute.title_singular')}}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{trans('cruds.productAttribute.title_singular')}} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped table-hover ajaxTable datatable datatable-ProductAttribute">
            <thead>
                <tr>
                    <th scope="col">
                        Attribute Group Name
                    </th>
                    <th scope="col">
                        No of Attributes
                    </th>
                    <th scope="col">
                        &nbsp;
                    </th>
                </tr>
            </thead>
        </table>
    </div>
</div>



@endsection
@section('scripts')
@parent
 <script>
    url="{{route('admin.product-attributes.destroy',':id')}}";
    url = url.replace(':id', 83);
    //  console.log(url);
   $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)



  let dtOverrideGlobals = {
    buttons: dtButtons,
    responsive: true,
    processing: true,
    serverSide: true,
    retrieve: true,
    aaSorting: [],
    ajax: "{{ route('admin.product-attributes.index') }}",
    columns: [
{ data: 'attribute_group_name', name: 'attribute_group_name' },
{ data: 'attribute_count', name: 'attribute_count' },
{ data: 'actions', name: '{{ trans('global.actions') }}' }
    ],
    order: [[ 0, 'desc' ]],
    pageLength: 100,
  };

  $('.datatable-ProductAttribute').DataTable(dtOverrideGlobals);
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
});  

</script>
@endsection