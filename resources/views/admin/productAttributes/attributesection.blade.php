<div class="row mt-1" id="attr_{{ $id }}">
    <input type="hidden" value="1" name="slug[]" />

    <div class="form-group col-md-4 col-sm-12">

        <input class="form-control {{ $errors->has('label') ? 'is-invalid' : '' }}" placeholder="Enter attribute's name"
            type="text" name="label[]" id="label" value="{{ old('label', '') }}" required>
        @if ($errors->has('label'))
            <div class="invalid-feedback">
                {{ $errors->first('label') }}
            </div>
        @endif
        <span class="help-block">{{ trans('cruds.productAttribute.fields.label_helper') }}</span>
    </div>

    <div class="form-group col-md-3 col-sm-12 ">
        <select class="custom-select form-control {{ $errors->has('type') ? 'is-invalid' : '' }} type_{{ $id }}"
            name="type[]" id="type_{{ $id }}" oninput="tag({{ $id }})" required>
            <option value="" selected>Select control type</option>
            <option value="textbox">Textbox</option>
            <option value="checkbox">Checkbox</option>
            <option value="radio_button">Radio button</option>
            <option value="dropdown">Dropdown</option>
        </select>
        {{-- @if ($errors->has('type'))
            <div class="invalid-feedback">
                {{ $errors->first('type') }}
            </div>
        @endif --}}
        <span class="help-block">{{ trans('cruds.productAttribute.fields.type_helper') }}</span>
    </div>

    <div class="form-group valuesdiv{{ $id }} col-md-4 col-sm-12 ">
        <input class="form-control {{ $errors->has('value') ? 'is-invalid' : '' }} value{{ $id }}" type="text"
            name="value[]" id='value{{ $id }}' value="{{ old('value', '') }}" required>
        <span class="help-block valuehelper{{ $id }}">{{ trans('cruds.productAttribute.fields.value_helper') }}</span>
    </div>


    <div class="col-md-1 col-2  btn btn-danger h-50 btn-sm ml-1 ml-sm-0" id="delete_attribute_{{ $id }}"
        onclick="delete_attribute({{ $id }})">
        <i class="fa fa-trash fa-2x"></i>
    </div>


</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.js"
    integrity="sha512-wTIaZJCW/mkalkyQnuSiBodnM5SRT8tXJ3LkIUA/3vBJ01vWe5Ene7Fynicupjt4xqxZKXA97VgNBHvIf5WTvg=="
    crossorigin="anonymous"></script>
<script>
    function delete_attribute(id) {
        //console.log(id);
        if ($('#attribute_section > div').length != 1) {
            // console.log($('#attribute_section > div').length);
            $("#attr_" + id).remove();

        } else {
             $('.show_alert').empty();
            $('.show_alert').append("<div class='alert alert-danger p-2 alert-validation-msg alert-dismissable' role='alert' ><i class='feather icon-info mr-1 align-middle fa-2x '></i><span class='h4 text-danger'>Cannot delete this item</span><button type='button' class='close' data-dismiss='alert' aria-hidden='true' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
        }
    }


    function tag(id) {
        classname = '.type_' + id;
        value = $(classname).val();
        if (value != "textbox") {
            $('.valuesdiv' + id).children().show();
            $("#value" + id).hide();
            // $('.valuehelper' + id).show();
            $('#value' + id).tagsInput({
                'height': '100%',
                'width': '100%',
                'defaultText': '',
            });
        } else {
            $('#value' + id).removeAttr('required');
            $('.valuesdiv' + id).children().hide();


        }
    }

</script>
