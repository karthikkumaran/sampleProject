@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.productAttribute.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.product-attributes.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            Attribute Group Name
                        </th>
                        <td class=" w-50">
                            {{ $attribute->attribute_group_name}}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Attribute Name
                        </th>
                        <td class=" w-50">
                            {{ $attribute->label}}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Control Type
                        </th>
                        <td>
                            {{ $attribute->type}}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Control Value
                        </td>
                        <td>
                          {{$attribute->values}}
                        </td>
                    </tr>
                </tbody>
            </table>
            {{-- <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.product-attribute.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div> --}}
        </div>
    </div>
</div>



@endsection