@extends('layouts.admin')
@section('content')
    @section('styles')
    @parent
        <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
    @endsection
    @php
        $payment_data = unserialize($user_payment->meta_data);
    @endphp
    <div class="content-body">
    <section class="invoice-preview-wrapper">
            <div class="row invoice-preview">
            <div class="col-xl-9 col-md-8 col-12">
                <div class="card invoice-preview-card">
                            <div class="card-body invoice-padding pb-0">

    <table border ="1" width="100%" >
    <tr>
                <td colspan="8">
                <table class="table table-borderless">
                <tr>
                    <td>
                        <img src="{{asset('XR/app-assets/images/logo/Mirrar.png')}}" height="100" width=70%/>
                    </td>
                    <td>
                    <div class=" p-0 m-0 text-left" style="font-size: 14px;">
                        <p><b> MIRRAR INNOVATION</b><br>
                        <b>TECHNOLOGIES PRIVATE LIMITED</b><br>
                            A-6,Industrial Estate,<br>
                            West Mogappair,<br>
                            Chennai Tamilnadu 600037.<br>
                            India</p>
                        </div>
                    </td>
                    <td class="text-center">
                    <p><b>TAX INVOICE</b></p>
                    
                    </td>
        
            <!-- {{-- <td class="">
&nbsp;&nbsp;
        </td> --}} -->
                    
                </tr>
                </table>
                
            </td>
        </tr>        
        <tr class=" table-secondary p-0 m-0">
            <td colspan="4"><b>Invoice Details: </b></td>
            <td colspan="4"><b>Bill To: </b></td>
        </tr>
        <tr>
            <td>
            <table class="table table-borderless">
                    <tr>
                            <td> <h4 class="invoice-title">
                                        Invoice ID: 
                                        <span class="invoice-number">INV-SIMPLI-{{$planinvoice->inv_num}}</span>
            </h4></td>
                        </tr>
                   <tr>
                     <td>Invoice Date:<span align="right">&nbsp;{{$inv_date}}</span></td>
                     <td></td>
                     </tr>
                    <!-- <tr>
                        <td>Terms<span align="right">&nbsp;:{{$planinvoice->terms}}</span></td>
                        <td></td>                       
                    </tr>
                    <tr>
                      <td>Due Date<span align="right">&nbsp;:{{$planinvoice->due_date}}</span></td>
                       <td></td> 
                    </tr> -->
                </table>
            </td>
        
            <td colspan="4">
                <div class=" p-0 m-1" style="font-size: 14px;">
                <b> {{$user_meta_data['storefront']['storename'] ?? ''}}</b><br>
                @if(isset($user_meta_data['footer_settings']['address1']))
                    {{$user_meta_data['footer_settings']['address1'] ?? ''}},
                    {{$user_meta_data['footer_settings']['address2'] ?? ''}}<br>
                @endif
                @if(isset($user_meta_data['footer_settings']['city']))                
                    {{$user_meta_data['footer_settings']['city'] ?? ''}}-
                @endif  
                @if(isset($user_meta_data['footer_settings']['pincode']))
                 {{$user_meta_data['footer_settings']['pincode'] ?? ''}},<br>
                @endif
                @if(isset($user_meta_data['footer_settings']['state']))
                    {{$user_meta_data['footer_settings']['state'] ?? ''}},
                @endif
                @if(isset($user_meta_data['footer_settings']['country']))
                  {{$user_meta_data['footer_settings']['country'] ?? ''}}.<br>
                 @endif
                 @if(isset($user_meta_data['settings']['contactnumber_country_code'])&&isset($user_meta_data['settings']['contactnumber']))
                    {{isset($user_meta_data['settings']['contactnumber_country_code']) ? '+'.$user_meta_data['settings']['contactnumber_country_code'].'-'.$user_meta_data['settings']['contactnumber'] : '+91'.'-'.$user_meta_data['settings']['contactnumber']}}<br>
                 @elseif(isset($user_meta_data['settings']['contactnumber'])) 
                    {{'+91'.'-'.$user_meta_data['settings']['contactnumber'] }}
                 @endif                       
                    <p>@if(isset($user_meta_data['checkout_settings']['tax_setting_details']['gst_number']))GST Number<span align="right">
                                &nbsp;:
                                {{  $user_meta_data['checkout_settings']['tax_setting_details']['gst_number']  }}@endif</span></p>
                </div>
            </td>
        </tr>
        </table>

                <!-- Invoice -->
            <table width=100% border='1'>
            
                <tr class=" table-secondary p-0 m-0">
                    <th colspan="1" class=" text-center">S.No</th>
                    <th colspan="3" class=" text-center">Plan Name</th>
                    <th colspan="1" class=" text-center">Qty</th>            
                    <th colspan="1" class=" text-center">Price</th>
                    <th colspan="1" class=" text-center">Discount</th>            
                    <th colspan="1" class=" text-center">Amount Paid</th>
                </tr>
                <tr>
                    <td colspan="1" class="text-center">1</td>
                    <td colspan="3" class="font-weight-bold text-center">{{$user_payment->plan_name}}</td>
                    <td colspan="1" class="text-center">1</td>
                    <td colspan="1" class="text-center">{!! $payment_data->currency ?? $payment_data["currency"] !!} {{number_format($plan->price,2)}}</td>
                    <td colspan="1" class="text-center">{{$plan->discount}}</td>
                    <td colspan="1" class="text-center">
                                                @if(!empty($payment_data->amount))
                                                {!! $payment_data->currency ?? $payment_data["currency"] !!} {{number_format(($payment_data->amount)/100,2)}}
                                                @else
                                                {!! $payment_data->currency ?? $payment_data["currency"] !!} {{number_format($payment_data["amount"],2)}}
                                                @endif</td>
                </tr>
            </table>
            <table border='1'width=100%>
                <tr>
                    <td colspan="8" class="text-left">
                    <div class="card-body invoice-padding pt-0">
                            <div class="row invoice-spacing">
                                <div class="col-lg-6 p-1 mt-xl-0 mt-1">
                                    <h6 class="mb-2"><b>Plan Details:</b></h6>
                                    <table border='1'>
                                        <tbody>
                                            <tr>
                                                <td class="pr-1">Plan:</td>
                                                <td><span class="font-weight-bold">{{$user_payment->plan_name}}</span></td>
                                            </tr>
                                            <tr>
                                                <td class="pr-1">Plan price:</td>
                                                <td>{!! $payment_data->currency ?? $payment_data["currency"] !!}
                                                {{--@if(!empty($payment_data->amount))
                                                    {{($payment_data->amount)/100}}
                                                @else
                                                    {{$payment_data["amount"]}}
                                                @endif--}}
                                                {{number_format($plan->price,2)}}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="pr-1">Plan starts at:</td>
                                                <td>{{ $plan_start }}</td>
                                            </tr>
                                            <tr>
                                                <td class="pr-1">Plan expiry:</td>
                                                <td>{{ $plan_expiry }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-lg-6 p-1 mt-xl-0 mt-1">
                                    <h6 class="mb-2"><b>Payment Details:</b></h6>
                                    <table border='1'>
                                        <tbody>
                                            <tr>
                                                <td class="pr-1">Total Amount:</td>
                                                <td><span class="font-weight-bold">{!! $payment_data->currency ?? $payment_data["currency"] !!} {{$user_payment->plan_price}}</span></td>
                                            </tr>
                                            <tr>
                                                <td class="pr-1">Payment Id:</td>
                                                <td>{{$user_payment->payment_id}}</td>
                                            </tr>
                                            <tr>
                                                <td class="pr-1">Payment Mode:</td>
                                                <td>{{$user_payment->payment_mode}}</td>
                                            </tr>
                                            <tr>
                                                <td class="pr-1">Payment Method:</td>
                                                <td>{{$user_payment->payment_method}}</td>
                                            </tr>
                                            <tr>
                                                <td class="pr-1">Status:</td>
                                                <td>{{$user_payment->status}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                            
                    </td>
                    
                </tr>
            </table>        
                            
                        <!-- Address and Contact starts -->
                        
                        <!-- Address and Contact ends -->
                        <hr class="invoice-spacing" />
                        <!-- Invoice Note starts -->
        
        <table width=100% class="m-1">
            <tr>
            <td  class="text-center" >
            <p>Thanks for your business.</p>
                <p>This is computerized generated invoice version</p>
            </td>
            
                
            </tr>
        
        
    </table>
                    </div>
                </div>
            </div>
                <!-- /Invoice -->
                <!-- Invoice Actions -->
                <div class="col-xl-3 col-md-4 col-12 invoice-actions mt-md-0 mt-2">
                    <div class="card">
                        <div class="card-body">
                            <a class="btn btn-danger btn-block btn-download-invoice mb-75" href="{{ route('admin.export_user_payment_pdf', $user_payment->id) }}" class="mr-1 mb-1">
                                Download
                            </a>
                            <a class="btn btn-secondary btn-block mb-75" target="_blank" href="{{ route('admin.print_user_payment_pdf', $user_payment->id) }}">
                                Print
                            </a>
                        </div>
                    </div>
                </div>
                <!-- /Invoice Actions -->
            </div>
        </section>
    </div>
@endsection