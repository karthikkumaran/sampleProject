@extends('layouts.admin')
@section('content')
@php
$agent=new \Jenssegers\Agent\Agent();
@endphp
<div class="card">
    <div class="card-header">
        {{ trans('cruds.userTransaction.title_singular') }} {{ trans('global.list') }}
        <!-- <span class="float-right" onclick="show_my_transaction_filter()" style="cursor: pointer;">
        Date filter<i class="feather icon-filter" style="font-size:21px;"></i>
        </span> -->
        <!-- <table border="0" cellspacing="5" cellpadding="5">
        <tbody><tr>
            <td>Plan start date:</td>
            <td><input type="datetime-local" id="min" name="min"></td>
        </tr>
        
        <tr>
            <td>Plan end date:</td>
            <td><input type="datetime-local" id="max" name="max"></td>
        </tr>
        <tr>
            <td><span class="btn btn-primary" onclick="show_date_filter()">Apply Filter</span></td>
            <td><span class="btn btn-danger" onclick="clear_date_filter()">Clear Filter</span></td>
        </tr>
    </tbody></table> -->
    </div>
    <div class="modal modal-sm fade" id="myTranscationFilterModal" tabindex="-1" role="dialog" aria-labelledby="myProductsFilterModalTitle" aria-hidden="true" @if($agent->isMobile()) @else style="margin-left:35%" @endif>
    <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myTranscationFilterModalTitle">Choosen Date</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card" style="padding-bottom: 0;">
                    <div class="card-body" style="padding-bottom: 2vh;">
                        <div class="form-group row mt-1">
                            <div class="mb-1 form-group col-12">Plan Date</div><br>

                            <div class="form-group col-12 ">
                            <label> Plan start date</label>
                            <!-- <input type="datetime-local" value="{{$plan_start}}"> -->
                            <input class="form-control" type="datetime-local" name="transaction_start_date" id="transaction_start_date" value="{{$plan_start}}" required>

                            </div>
                            <div class="form-group col-12 ">
                            <label> Plan end date</label>
                            <!-- <input type="datetime-local" value="{{$plan_end}}"> -->
                            <input class="form-control" type="datetime-local" name="transaction_end_date" id="transaction_end_date" value="{{$plan_end}}" required>
                            </div>
                            <div class="alert alert-warning" id="error_min" style="display:none;" role="alert">plan ends should not exceeds plan starts.</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="clear_my_products_filter()">Clear filters</button>
                <button type="button" id="apply_my_products_filter" class="btn btn-primary" onclick="apply_my_products_filter()">Apply filters</button>
            </div>
        </div>
    </div>
</div>
    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-UserTransaction" id="example">
            <thead>
                    <tr>
                        <th>
                            {{ trans('cruds.userTransaction.fields.transcation_id') }}
                        </th>
                        <!-- <th>
                            {{ trans('cruds.user.fields.name') }}
                        </th> -->
                        <th>
                            {{ trans('cruds.user.fields.email') }}
                        </th>
                        <th>
                            Domain
                        </th>
                        <th>
                            Plan name
                        </th>
                        <th>
                            Amount Paid
                        </th>
                        <th>
                            Payment Method
                        </th>
                        <th>
                            Subscription start date
                        </th>
                        <th>
                            Subscription end date
                        </th>
                        <th>
                            &nbsp;Actions
                        </th>
                    </tr>
                </thead> 
                <tbody>
                    @foreach($userTransaction as $key => $transaction)
                        @php
                        if(!empty($transaction->user)){
                            $user = $transaction->user;
                            $subscriptionDetails = [];
                            $user_info = [];
                            $storeLink = false;
                        $subscriptionDetails = $user->getSubscriptionDetails();
                        $user_info = $user->getUserInfo();
                            if(!empty($user_info))
                                $storeLink = $user->getStoreLink($user_info->public_link,"false");        
                        }                
                        @endphp
                       
                        <tr data-entry-id="{{ $transaction->id }}">
                            <td>
                                {{ $transaction->payment_id ?? '' }}
                            </td>  
                            <!-- <td>
                                {{ $user->name ?? '' }}
                            </td>    -->
                            <td>
                                {{ $user->email ?? '' }}
                            </td>   
                            <td>
                                @if($user->getIsAdminAttribute())
                                @else
                                    @if(!empty($user_info))
                                        @if($storeLink != false)
                                            <a href="{{ $storeLink }}" target="__blank" class="" >{{ $storeLink }}</a>
                                        @else
                                            <span class="text-danger"><b>Domain not set</b></span>
                                        @endif
                                    @endif
                                @endif
                            </td> 
                            <td>
                                {{ $subscriptionDetails->plan->name ?? ""}}
                            </td>
                            <td>
                                {{ $transaction->plan_price ?? '' }}
                            </td>  
                            <td>
                                {{ $transaction->payment_method ?? '' }}
                            </td> 
                            <td>
                                {{ $transaction->plan_start ?? '' }}
                            </td> 
                            <td>
                                {{ $transaction->plan_expiry ?? '' }}
                            </td> 
                            <td>
                                <a class="btn btn-xs btn-primary btn-icon" href="{{ route('admin.userTranscations.show', $transaction->id) }}">
                                        <i class="feather icon-eye"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
               </tbody>     
            </table>
        </div>
    </div>
</div>                            



@endsection

@section('scripts')
@parent
<script>
    var plan_start = $('#transaction_start_date').val();
    var plan_end = $('#transaction_end_date').val();
    if(plan_start && plan_end){
        toastr.success('Date Filter is applied');
    }
    function show_my_transaction_filter(){
        $('#error_min').hide();
        $("#myTranscationFilterModal").modal('show');
    }
    function apply_my_products_filter(){
        $('#error_min').hide();
        var plan_start = $('#transaction_start_date').val();
        var plan_end = $('#transaction_end_date').val();
        if(plan_start && plan_end){
            if(plan_start < plan_end){
                var transactionUrl = "{{ route('admin.userTranscations.index')}}?plan_start="+plan_start+"&plan_end="+plan_end;
                window.location.href = transactionUrl;
            }
            else{
                $('#error_min').show();
            }
        }
        else{
            toastr.warning('Date field is required!');
        }
        
    }
    function clear_my_products_filter(){
        var transactionUrl = "{{ route('admin.userTranscations.index')}}"
        window.location.href = transactionUrl;
    }
</script>
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 0, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-UserTransaction:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

// function clear_date_filter(){
//     $('#min').val('');
//     $('#max').val('');
    
// }

// function show_date_filter(){
//     var min = $('#min').val();
//     var max = $('#max').val();
//     if(min && max){
//         $.fn.dataTable.ext.search.push(
//         function( settings, data, dataIndex ) {
//             var start_date =  data[7] ;
//             var end_date =  data[8] ;
            
//             if (
//                 ( min === null && max === null ) ||
//                 ( min === null && date <= max ) ||
//                 ( min <= start_date   && end_date === null ) ||
//                 ( min <= start_date   && end_date <= max )
//             ) {
//                 return true;
//             }
//             return false;
//         }
//         );
        
//         $('.datatable-UserTransaction').DataTable().draw();
//         // $($.fn.dataTable.tables(true)).DataTable()
//         //         .columns.adjust();
//     }
//     else{
//         toastr.error('Date field is required');
//     }

// }

</script>
@endsection

