@can('entitycard_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.entitycards.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.entitycard.title_singular') }}
            </a>
        </div>
    </div>
@endcan

<div class="card">
    <div class="card-header">
        {{ trans('cruds.entitycard.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Entitycard">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.entitycard.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.entitycard.fields.campagin') }}
                        </th>
                        <th>
                            {{ trans('cruds.entitycard.fields.product') }}
                        </th>
                        <th>
                            {{ trans('cruds.entitycard.fields.video') }}
                        </th>
                        <th>
                            {{ trans('cruds.entitycard.fields.asset') }}
                        </th>
                        <th>
                            {{ trans('cruds.entitycard.fields.meta_data') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($entitycards as $key => $entitycard)
                        <tr data-entry-id="{{ $entitycard->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $entitycard->id ?? '' }}
                            </td>
                            <td>
                                {{ $entitycard->campagin->name ?? '' }}
                            </td>
                            <td>
                                {{ $entitycard->product->name ?? '' }}
                            </td>
                            <td>
                                {{ $entitycard->video->title ?? '' }}
                            </td>
                            <td>
                                {{ $entitycard->asset->name ?? '' }}
                            </td>
                            <td>
                                {{ $entitycard->meta_data ?? '' }}
                            </td>
                            <td>
                                @can('entitycard_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.entitycards.show', $entitycard->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('entitycard_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.entitycards.edit', $entitycard->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('entitycard_delete')
                                    <form action="{{ route('admin.entitycards.destroy', $entitycard->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('entitycard_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.entitycards.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-Entitycard:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection