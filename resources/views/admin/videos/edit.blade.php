<style>
    .remove-image{
        display: inline;
        position:absolute;
        top: 350px;
        right: 370px;
        border-radius: 10em;
        padding: 2px 6px 3px;
        text-decoration: none;
        font: 15px sans-serif;
        background: red;
        border: 3px solid #fff;
        color: white;
        box-shadow: 0 2px 6px rgb(0 0 0 / 50%), inset 0 2px 4px rgb(0 0 0 / 30%);
        text-shadow: 0 1px 2px rgb(0 0 0 / 50%);
        -webkit-transition: background 0.5s;
        transition: background 0.5s;
    }
</style>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
@php
$agent=new \Jenssegers\Agent\Agent();
@endphp
    <div class="card-body container-fluid">
        <div class="row">
    @if($agent->isMobile())
        <div class="col-lg-12 col-md-12 col-sm-12">
            <form method="POST" action="{{ route("admin.videos.update", [$video->id]) }}" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="title">{{ trans('cruds.video.fields.title') }}</label>
                            <input class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" type="text" name="title" id="title" value="{{ old('title', $video->title) }}">
                            @if($errors->has('title'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('title') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.video.fields.title_helper') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="video_url">{{ trans('cruds.video.fields.video_url') }}</label>
                            <input class="form-control {{ $errors->has('video_url') ? 'is-invalid' : '' }}" type="text" name="video_url" id="video_url" value="{{ old('video_url', $video->video_url) }}" required>
                            @if($errors->has('video_url'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('video_url') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.video.fields.video_url_helper') }}</span>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        @php 
                            $agent=new \Jenssegers\Agent\Agent();
                            $attributes = [
                                'type' => null,
                                'class' => 'iframe',
                                'data-html5-parameter' => true
                            ];
                            $whitelist = []; $params = [];
                        @endphp    
                            {!! \LaravelVideoEmbed::parse( old('video_url', $video->video_url), $whitelist, $params,$attributes ) !!}
                    
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="description">{{ trans('cruds.video.fields.description') }}</label>
                            <textarea class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" id="description">{{ old('description', $video->description) }}</textarea>
                            @if($errors->has('description'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('description') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.video.fields.description_helper') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="status">{{ trans('cruds.video.fields.status') }}</label>
                            <select class="form-control {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" id="status" >
                                @if($video->status == 0)
                                    <option value="1" >Active</option>
                                    <option value="0" selected>Inactive</option>
                                @else
                                    <option value="1" selected>Active</option>
                                    <option value="0" >Inactive</option>
                            @endif
                            </select>
                            @if($errors->has('status'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('status') }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="file">{{ trans('cruds.video.fields.thumbnail') }} [Recommended size: 420 X 200]</label><br> 
                            <!-- <div class="needsclick dropzone {{ $errors->has('file') ? 'is-invalid' : '' }}" id="file-dropzone">
                            </div> -->
                            
                            @php
                            if($video->thumbnail)
                            {
                                    $img = $video->thumbnail->getUrl('thumb');
                            }
                            @endphp
                            @if($video->thumbnail)
                            <div id="thumbnailimg">
                                                                        <img class="img-responsive mw-80 mh-80 rounded-top" src="{{$img}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';"  style="height:170px;cursor:pointer;"/>
                                                                        <b><a class="remove-image " style="top:0px;right:130px;" href="javascript:void(0)" onclick="removethumbnail({{$video->thumbnail->id}},'{{$video->thumbnail->file_name}}')" style="display: inline;"><i class="fa fa-trash" aria-hidden="true"></i></a></b>
                                                                    <!-- <img class="img-responsive mw-80 mh-80 rounded-top" src="{{$img}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';"  style="height:170px;cursor:pointer;"/> -->
                            </div>
                            @endif
                            <div class="dropzone dropzone-area {{ $errors->has('thumbnail') ? 'is-invalid' : '' }} rounded mb-0 ml-1 "  id="file-dropzone">
                                <!-- <i class="feather icon-plus-circle font-large-1 "></i> -->
                                <!-- <span style="display: block;padding:2px; color:blue;"><center>Upload Image</center></span> -->
                                <div class="dz-message">Upload Thumbnail</div>
                                    </div>
                                    <!-- <div style="height:85px;width: 120px;text-align: center;border-style:dashed;padding: 7px;" id="file-dropzone" class="rounded mb-0 ml-1">
                                                                <div class="image" id="thumbnail">
                                                                    <i class="feather icon-plus-circle font-large-1 "></i>
                                                                </div>
                                                                <span style="display: block;padding:2px;">Upload Image</span>
                                    </div> -->
                            @if($errors->has('thumbnail'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('thumbnail') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.video.fields.file_helper') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    </div>
                </div>
            </form>
      
        </div>
    @else
    <div class="col-lg-6 col-md-6 col-sm-12">
            <form method="POST" action="{{ route("admin.videos.update", [$video->id]) }}" enctype="multipart/form-data">
                @method('PUT')
                @csrf
                
                        <div class="form-group">
                            <label for="title">{{ trans('cruds.video.fields.title') }}</label>
                            <input class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" type="text" name="title" id="title" value="{{ old('title', $video->title) }}">
                            @if($errors->has('title'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('title') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.video.fields.title_helper') }}</span>
                        </div>
                   
                        <div class="form-group">
                            <label for="video_url">{{ trans('cruds.video.fields.video_url') }}</label>
                            <input class="form-control {{ $errors->has('video_url') ? 'is-invalid' : '' }}" type="text" name="video_url" id="video_url" value="{{ old('video_url', $video->video_url) }}" required>
                            @if($errors->has('video_url'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('video_url') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.video.fields.video_url_helper') }}</span>
                        </div>
                    
                        <div class="form-group">
                            <label for="description">{{ trans('cruds.video.fields.description') }}</label>
                            <textarea class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" id="description">{{ old('description', $video->description) }}</textarea>
                            @if($errors->has('description'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('description') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.video.fields.description_helper') }}</span>
                        </div>
                    
                        <div class="form-group">
                            <label for="status">{{ trans('cruds.video.fields.status') }}</label>
                            <select class="form-control {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" id="status" >
                                @if($video->status == 0)
                                    <option value="1" >Active</option>
                                    <option value="0" selected>Inactive</option>
                                @else
                                    <option value="1" selected>Active</option>
                                    <option value="0" >Inactive</option>
                            @endif
                            </select>
                            @if($errors->has('status'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('status') }}
                                </div>
                            @endif
                        </div>
                    
                        <div class="form-group">
                            <label for="file">{{ trans('cruds.video.fields.thumbnail') }}</label>  [Recommended size: 420 X 200]
                            <!-- <div class="needsclick dropzone {{ $errors->has('file') ? 'is-invalid' : '' }}" id="file-dropzone">
                            </div> -->
                            
                            @php
                            if($video->thumbnail)
                            {
                                    $img = $video->thumbnail->getUrl('thumb');
                            }
                            @endphp
                            @if($video->thumbnail)
                            <div id="thumbnailimg">
                                                                        <img class="img-responsive mw-80 mh-80 rounded-top" src="{{$img}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';"  style="height:170px;cursor:pointer;"/>
                                                                        
                                                                        <b><a class="remove-image " href="javascript:void(0)" onclick="removethumbnail({{$video->thumbnail->id}},'{{$video->thumbnail->file_name}}')" style="display: inline;"><i class="fa fa-trash" aria-hidden="true"></i></a></b>
                                                                    <!-- <img class="img-responsive mw-80 mh-80 rounded-top" src="{{$img}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';"  style="height:170px;cursor:pointer;"/> -->
                            </div>
                            @endif
                            <div class="dropzone dropzone-area {{ $errors->has('thumbnail') ? 'is-invalid' : '' }} rounded mb-0 ml-1 "  id="file-dropzone">
                                <!-- <i class="feather icon-plus-circle font-large-1 "></i> -->
                                <!-- <span style="display: block;padding:2px; color:blue;"><center>Upload Image</center></span> -->
                                <div class="dz-message">Upload Thumbnail</div>
                                    </div>
                                    <!-- <div style="height:85px;width: 120px;text-align: center;border-style:dashed;padding: 7px;" id="file-dropzone" class="rounded mb-0 ml-1">
                                                                <div class="image" id="thumbnail">
                                                                    <i class="feather icon-plus-circle font-large-1 "></i>
                                                                </div>
                                                                <span style="display: block;padding:2px;">Upload Image</span>
                                    </div> -->
                            @if($errors->has('thumbnail'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('thumbnail') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.video.fields.file_helper') }}</span>
                        </div>
                    
                        <div class="form-group">
                            <button class="btn btn-danger" type="submit">
                                {{ trans('global.save') }}
                            </button>
                        </div>
                    
            </form>
      
        </div>

        <div class="col-lg-6 col-md-6 col-sm-12">
            @php 
                $agent=new \Jenssegers\Agent\Agent();
                $attributes = [
                    'type' => null,
                    'class' => 'iframe',
                    'data-html5-parameter' => true
                ];
                $whitelist = []; $params = [];
            @endphp    
                {!! \LaravelVideoEmbed::parse( old('video_url', $video->video_url), $whitelist, $params,$attributes ) !!}
                    
        </div>

    @endif
    
    
    <div class="col-12 col-sm-12 p-1">
        <input type="hidden" value="{{$video->id}}" id="editid"/>
    <button type="button" class="btn btn-primary btn-icon float-right"  id="attach_product" onclick="selectProduct({{$video->id}})">
        <i class="feather icon-plus "></i>Attach product to video</button>
    @if(!$agent->isMobile())    
    <hr class="hr-text mt-0" data-content="Products related to this video" />
    @else
    <hr>
    @endif
    <section class="grid-view d-block mb-4" id="catalog_entitycard">
                <div class="row mt-2" id="video_entitycard_gird"></div>
    </section>
    </div>
  </div>
</div>

<style>
@media (max-width: 576px) {
  .dropzone .dz-message:before {
     top : 3rem; 
  }
  .iframe{
        height:270px;
        width: 300px;
    }
}

    </style>
<script>
//     Dropzone.options.fileDropzone = {
//     url: '{{ route('admin.videos.storeMedia') }}',
//     maxFilesize: 40, // MB
//     maxFiles: 1,
//     addRemoveLinks: true,
//     headers: {
//       'X-CSRF-TOKEN': "{{ csrf_token() }}"
//     },
//     params: {
//       size: 40
//     },
//     success: function (file, response) {
//       $('form').find('input[name="file"]').remove()
//       $('form').append('<input type="hidden" name="file" value="' + response.name + '">')
//     },
//     removedfile: function (file) {
//       file.previewElement.remove()
//       if (file.status !== 'error') {
//         $('form').find('input[name="file"]').remove()
//         this.options.maxFiles = this.options.maxFiles + 1
//       }
//     },
//     init: function () {
// @if(isset($video) && $video->file)
//       var file = {!! json_encode($video->file) !!}
//           this.options.addedfile.call(this, file)
//       file.previewElement.classList.add('dz-complete')
//       $('form').append('<input type="hidden" name="file" value="' + file.file_name + '">')
//       this.options.maxFiles = this.options.maxFiles - 1
// @endif
//     },
//      error: function (file, response) {
//          if ($.type(response) === 'string') {
//              var message = response //dropzone sends it's own error messages in string
//          } else {
//              var message = response.errors.file
//          }
//          file.previewElement.classList.add('dz-error')
//          _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
//          _results = []
//          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
//              node = _ref[_i]
//              _results.push(node.textContent = message)
//          }

//          return _results
//      }
// }

@if(isset($video->thumbnail) && $video->thumbnail != null)
        $('#file-dropzone').hide();
        @endif

$("#catalog_entitycard").on('click', '.pagination a', function(event) {
        event.preventDefault();


        $('li').removeClass('active');
        $(this).parent('li').addClass('active');

        var myurl = $(this).attr('href');
        var page = $(this).attr('href').split('page=')[1];

        getEntityData(page);
    });

    // $(document).ready(function () {
    // setTimeout(function(){  }, 5000);
    getEntityData(page);
    // });
    function getEntityData(page) {
        $('#loading-bg').show();
        // if(searchData == "")
        //     q = 'page=' + page;
        // else 
        q = 'q=' + searchData + '&s=' + sort + '&page=' + page + '&filterDatas=' + filterData +
            '&campaign_id={{$campaign->id}}'+ '&video_id={{$video->id}}';
            // console.log(q);

        $.ajax({
            url: "{{route('admin.campaigns.ajaxVideoProductEntityData')}}" + '?' + q,
            type: "get",
            datatype: "html"
        }).done(function(data) {
            
            // if ($(".card")[0]){
            //     // Do something if class exists
            // } else {
            //     // Do something if class does not exist
            // }
            
            $("#video_entitycard_gird").empty().html(data);
           
            // $("#product_list").append(data);
            location.hash = page++;
            $('#loading-bg').hide();
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            // console.log("error")
            $('#loading-bg').hide();
            // alert('No response from server');
        });
    }
    $("#attach_product").on('click', function(event) {
        $("#videoEditorModal").modal('hide');  
    })
</script>
<script>
    var uploadedPhotoMap = {}
    Dropzone.autoDiscover = false;
    myProductDropzone = new Dropzone('#file-dropzone', {
        url: "{{route('admin.videos.storeMedia')}}",
        maxFilesize: 40, // MB
        maxFiles: "{{ session('subscription_usage_remaining') ?? 50 }}",
        acceptedFiles: '.jpeg,.jpg,.png,.gif',
        addRemoveLinks: true,
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        params: {
            size: 40
        },
        success: function(file, response) {
            $('#file-dropzone').css("height",'80%')
            $('#file-dropzone').css("width",'80%')
            $('form').append('<input type="hidden" name="thumbnail" value="' + response.name + '">')
        },
        removedfile: function(file) {
            file.previewElement.remove()
            $('#file-dropzone').css("height",'80px')
            $('#file-dropzone').css("width",'120px')
            var name = ''
            if (typeof file.file_name !== 'undefined') {
                name = file.file_name
            } else {
                name = uploadedPhotoMap[file.name]
            }
            $('form').find('input[name="thumbnail"][value="' + name + '"]').remove()
        },
        init: function() {
            var submitButton = document.querySelector("#uploadSubmit");
            myDropzone = this; // closure
            submitButton.addEventListener("click", function() {
                if (myDropzone.getUploadingFiles().length === 0 && myDropzone.getQueuedFiles().length === 0) {} else {
                    myDropzone.processQueue();
                }
            });
            this.on("dragend, processingmultiple", function(file) {
                $("#uploadSubmit").prop('disabled', true);
            });
            this.on("queuecomplete", function(file) {
                // if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                //     $("#uploadSubmit").prop('disabled', false);
                // } else {
                //     $("#uploadSubmit").prop('disabled', true);
                // }
            })

        },
        error: function(file, response) {
            if ($.type(response) === 'string') {
                var message = response //dropzone sends it's own error messages in string
            } else {
                var message = response.errors.file
            }
            file.previewElement.classList.add('dz-error')
            _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
            _results = []
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                node = _ref[_i]
                _results.push(node.textContent = message)
            }

            return _results
        }

    });
    function removethumbnail(id, file_name) {
        console.log('enter')
        if (confirm("{{ trans('global.areYouSure') }}")) {
            $('#loading-bg').show();
            var url = "{{route('admin.videos.removethumbnail')}}";
            $.ajax({
                url: url,
                type: "post",
                data: {
                    'id': id,
                },
            }).done(function(data) {
                //alert('Removed image');
                //location.reload();
                // $('#storelogo_' + id).remove();
                $('#loading-bg').hide();
                $('#file-dropzone').show();
                $('#thumbnailimg').hide();

                $('form').find('input[name="thumbnail"][value="' + name + '"]').remove()
                // $('#formstorefront').find("input[name='storelogo'][value='" + file_name + "']").remove()

            }).fail(function(jqXHR, ajaxOptions, thrownError) {
                // alert('No response from server');
            });
        }
    }
</script>