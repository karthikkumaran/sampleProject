@extends('layouts.admin')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.css"
    integrity="sha512-uKwYJOyykD83YchxJbUxxbn8UcKAQBu+1hcLDRKZ9VtWfpMb1iYfJ74/UIjXQXWASwSzulZEC1SFGj+cslZh7Q==" crossorigin="anonymous" />

<style>
    div.tagsinput {
        border: 1px solid #CCC;
        background: #FFF;
        /* padding: 5px; */
        width: 300px;
        /* height: 100px; */
        overflow-y: visible;
    }

    div.tagsinput span.tag {
        border: 1px solid #1709da;
        background: #0814c2;
        color: #ffffff;
        font-size: 18px;
        font-weight: 500;
    }

    div.tagsinput span.tag a {
        font-weight: 700;
        color: white;
        text-decoration: none;
        font-size: 19px;
        margin-bottom: 0;
        padding: 0;
    }

</style>
@section('content')

    <div class="show_alert">

    </div>

    <div class="card">
        <div class="card-header">
            <div>
                <a class="btn btn-md  btn-outline-secondary " href="{{ route('admin.packages.index') }}">
                    Back to packages list
                </a>
                <br>
                {{-- {{ trans('global.create') }}
                {{ trans('cruds.package.title_singular') }} --}}
            </div>
        </div>

        <div class="card-body">
            <form method="POST" action="{{ route('admin.packages.store') }}" enctype="multipart/form-data">
                @csrf
                <label class="h3"> {{ trans('cruds.package.title_singular') }}</label>
                <br>
                <br>
                <div class="row">
                    <div class="col-12 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="required h4" for="name">{{ trans('cruds.package.fields.name') }}</label>
                            <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                placeholder="Enter package name" type="text" name="name" id="name"
                                value="{{ old('name', '') }}" required>
                            @if ($errors->has('name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('name') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.package.fields.name_helper') }}</span>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="required h4" for="description">{{ trans('cruds.package.fields.description') }}</label>
                            <textarea class="form-control {{ $errors->has('package_description') ? 'is-invalid' : '' }}"
                                placeholder="Enter package desciption" name="description" id="description"
                                value="{{ old('description', '') }}" maxlength="1000" required></textarea>
                            @if ($errors->has('description'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('description') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.package.fields.description_helper') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="required h4" for="price">{{ trans('cruds.package.fields.price') }}</label>
                            <input class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}" type="number" name="price" id="price" value="{{ old('price', '') }}" step="0.01" required>
                            @if($errors->has('price'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('price') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.package.fields.price_helper') }}</span>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class=" h4" for="discount">Discount</label>
                            <input class="form-control" type="text" name="discount" id="discount">
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-12 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="required h4" for="invoice_period">{{ trans('cruds.package.fields.invoice_period') }}</label>
                            <input class="form-control {{ $errors->has('invoice_period') ? 'is-invalid' : '' }}" type="number" name="invoice_period" id="invoice_period" value="{{ old('invoice_period', '') }}" step="1" required>
                            @if($errors->has('invoice_period'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('invoice_period') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.package.fields.invoice_period_helper') }}</span>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="required h4" for="invoice_interval">{{ trans('cruds.package.fields.invoice_interval') }}</label>
                            <select class="custom-select form-control {{ $errors->has('invoice_interval') ? 'is-invalid' : '' }}" name="invoice_interval" id="invoice_interval" required>
                                <option value="day">Day</option>
                                <option value="month">Month</option>
                                <option value="year">Year</option>
                            </select>
                            @if ($errors->has('invoice_interval'))
                                <div class="invalid-feedback">
                                    $errors->first('invoice_interval')
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.package.fields.invoice_interval_helper') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="required h4" for="trial_period">{{ trans('cruds.package.fields.trial_period') }}</label>
                            <input class="form-control {{ $errors->has('trial_period') ? 'is-invalid' : '' }}" type="number" name="trial_period" id="trial_period" value="{{ old('trial_period', '') }}" step="1" required>
                            @if($errors->has('trial_period'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('trial_period') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.package.fields.trial_period_helper') }}</span>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="required h4" for="trial_interval">{{ trans('cruds.package.fields.trial_interval') }}</label>
                            <select class="custom-select form-control {{ $errors->has('trial_interval') ? 'is-invalid' : '' }}" name="trial_interval" id="trial_interval" value="{{ old('trial_interval', '') }}" required>
                                <option value="day">Day</option>
                                <option value="month">Month</option>
                            </select>
                            @if ($errors->has('trial_interval'))
                                <div class="invalid-feedback">
                                    $errors->first('trial_interval')
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.package.fields.trial_interval_helper') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="required h4" for="grace_period">{{ trans('cruds.package.fields.grace_period') }}</label>
                            <input class="form-control {{ $errors->has('grace_period') ? 'is-invalid' : '' }}" type="number" step="1" onkeypress="return !(event.charCode == 46)" name="grace_period" id="grace_period" value="{{ old('grace_period', '') }}">
                            @if($errors->has('grace_period'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('grace_period') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.package.fields.grace_period_helper') }}</span>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="required h4" for="grace_interval">{{ trans('cruds.package.fields.grace_interval') }}</label>
                            <select class="custom-select form-control {{ $errors->has('grace_interval') ? 'is-invalid' : '' }}" name="grace_interval" id="grace_interval" value="{{ old('grace_interval', '') }}">
                                <option value="" selected>Select grace interval type</option>
                                <option value="hour">Hour</option>
                                <option value="day">Day</option>
                                <option value="month">Month</option>
                            </select>
                            @if ($errors->has('grace_interval'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('grace_interval') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.package.fields.grace_interval_helper') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="required h4" for="sort_order">{{ trans('cruds.package.fields.sort_order') }}</label>
                            <input class="form-control {{ $errors->has('sort_order') ? 'is-invalid' : '' }}" type="text" name="sort_order" id="sort_order" value="{{ old('sort_order', '') }}">
                            @if ($errors->has('sort_order'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('sort_order') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.package.fields.sort_order_helper') }}</span>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="required h4" for="currency">{{ trans('cruds.package.fields.currency') }}</label>
                            <select class="custom-select form-control" {{ $errors->has('currency') ? 'is-invalid' : '' }}" name="currency" id="currency" value="{{ old('currency', '') }}" required>
                            @foreach(App\User::CURRENCY_SYMBOLS as $key => $label)
                                <option  value="{{ $key }}" {{ old('currency', 'INR') ===  $key ? 'selected' : '' }} > {{ $key }} ({{html_entity_decode($label)}})</option>
                            @endforeach        
                            </select>
                            @if ($errors->has('currency'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('currency') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.package.fields.currency_helper') }}</span>
                        </div>
                    </div>
                    
                </div>
                <div class="row">
                <div class="col-12 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="required h4" for="signup_fee">{{ trans('cruds.package.fields.signup_fee') }}</label>
                            <input class="form-control {{ $errors->has('signup_fee') ? 'is-invalid' : '' }}" type="number" name="signup_fee" id="signup_fee" value="{{ old('signup_fee', '') }}" step="0.01">
                            @if($errors->has('signup_fee'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('signup_fee') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.package.fields.signup_fee_helper') }}</span>                    
                        </div>
                    </div>  
                </div>
                
                <label class="h3">{{ trans('cruds.feature.title') }}</label>
                <div class="row mb-2">
                    <label class="col col-md-4 text-center h4 d-none d-sm-block" for="name">{{ trans('cruds.feature.fields.feature_name') }}</label>
                    <label class="col col-md-3 text-center h4 d-none d-sm-block" for="value">{{ trans('cruds.feature.fields.feature_value') }}</label>
                    <label class="col col-md-4 text-center h4 d-none d-sm-block" for="sort_order">{{ trans('cruds.feature.fields.feature_sort_order') }}</label>
                    <div class="col-md-1 btn btn-xs col-2 ml-1 ml-sm-0  btn-success btn-icon px-md-1 add_feature" id="add_feature">
                        <i class="fa fa-plus fa-2x"></i>
                    </div>
                </div>
            
                <div id="feature_section">

                </div>

                <div class="form-group mt-1">
                    <button class="btn btn-danger" type="submit">
                        {{ trans('global.save') }}
                    </button>
                </div>
            </form>
        </div>
    </div>

@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.js"
        integrity="sha512-wTIaZJCW/mkalkyQnuSiBodnM5SRT8tXJ3LkIUA/3vBJ01vWe5Ene7Fynicupjt4xqxZKXA97VgNBHvIf5WTvg=="
        crossorigin="anonymous">
    </script>

    <script>
        var feature_id = 0;

        function add_feature() {
            feature_id += 1;
            url = "{{ route('admin.packages.addPlanFeature', ':feature_id') }}";
            url = url.replace(':feature_id', feature_id);
            var feature_row = $('<div>').load(url, function(row) {
                $('#feature_section').append(row);
                //     if ($('#attribute_section > div').length == 1)
                //         $('#delete_attribute_1').show();
            });
        }
        add_feature();

        $('.add_feature').on("click", function() {
            add_feature();
        });
    </script>
@endsection
