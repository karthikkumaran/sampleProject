@extends('layouts.admin')
@section('content')
@php 
$agent=new \Jenssegers\Agent\Agent();
@endphp
    <div style="margin-bottom: 10px;" class="row"> 
        <div class="col-lg-12">
            <a class="btn btn-success pull-right" href="{{ route('admin.packages.create') }}">
                {{ trans('global.add') }} {{trans('cruds.package.title')}}
            </a>
        </div>
    </div>
    <div class="">
        <div class="row mt-1 mb-1">
            <div class="col-12" style="text-align:center; vertical-align:middle;">
                <div class="custom-control custom-switch custom-switch-success">
                    <span class="switch-label mr-1"><b>Monthly</b></span>
                    <input type="checkbox" class="custom-control-input" id="plan_interval" name="plan_interval">
                    <label class="custom-control-label" for="plan_interval"></label>
                    <span class="switch-label"><b>Yearly</b></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row pricing-card">
        <div class="col-12 col-sm-offset-2 col-sm-10 col-md-12 col-lg-offset-2 col-lg-10 mx-auto" id="packages_page">
            <div class="row" id="packages_show">
                <!-- pricing plan cards -->
                @foreach($packages as $key => $package) 
                    @php
                        $features = $package->features;
                    @endphp
                    @if($package->invoice_interval == "month")
                        <div class="col-12 col-md-3 monthly_package" style="display:show">
                    @elseif($package->invoice_interval == "year")
                        <div class="col-12 col-md-3 yearly_package" style="display:none">
                    @else
                        <div class="col-12 col-md-3">
                    @endif
                                @php
                                    if($package->price > $package->discount)
                                    $package_discount = $package->price - $package->discount;
                                       else
                                         $package_discount = $package->price; 
                                @endphp
                            <div class="card basic-pricing text-center">
                                <div class="card-body">
                                    <h3>{!! $package->name !!}</h3>
                                    <!-- <p class="card-text">{!! $package->description !!}</p> -->
                                    <div class="annual-plan">
                                        <div class="plan-price mt-2">
                                            @if($package->discount != 0)
                                            <span class="font-medium-1 font-weight-bold text-primary">{!! $package->currency !!}</span>
                                            <span class="pricing-basic-value font-weight-bolder text-primary">{{ $package_discount }} / {{$package->invoice_period}} {{$package->invoice_interval}}</span><br>
                                            <s><span class="font-medium-1 font-weight-bold text-primary">{!! $package->currency !!}</span>
                                            <span class="pricing-basic-value font-weight-bolder text-primary">{!! $package->price !!} / {{$package->invoice_period}} {{$package->invoice_interval}}</span></s>
                                            
                                            @else
                                            <span class="font-medium-1 font-weight-bold text-primary">{!! $package->currency !!}</span>
                                            <span class="pricing-basic-value font-weight-bolder text-primary">{!! $package->price !!}</span>
                                            @endif
                                        </div>
                                        <small class="annual-pricing d-none text-muted"></small>
                                    </div>
                                @if($agent->isMobile())
                                    <button class="my-2 pointer btn btn-info"  data-toggle="collapse" id="features" data-target="#demo_{{$package->id}}">Show features &nbsp;
                                        <i class="fa fa-angle-double-down"></i>
                                        <!-- <i data-toggle="collapse" class="fa fa-angle-double-up"></i><i class="fa fa-angle-double-down"></i> -->
                                    </button>
                                    <div id="demo_{{$package->id}}" class="collapse">
                                @endif
                                    <ul class="list-group list-group-circle text-left">
                                        @foreach($features as $key => $feature)
                                            @php
                                                $feature_details = $feature->getFeatureDetails();
                                            @endphp
                                            <li class="list-group-item text-center">
                                                @if($feature_details->name == "catalogs")
                                                    Number of Catalogue - <b>{!! $feature->value !!}</b>
                                                @elseif($feature_details->name == "products")
                                                    Number of Products- <b>{!! $feature->value !!}</b>
                                                @elseif($feature_details->name == "stories")
                                                    Product Stories
                                                @elseif($feature_details->name == "pos")
                                                    Point of Sale 
                                                @elseif($feature_details->name == "analytics")
                                                    Store Analytics 
                                                @else
                                                    {!! $feature_details->name !!} 
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                @if($agent->isMobile())
                                    </div>
                                @endif
                                    <button class="btn btn-block btn-outline-success mt-2" onclick="window.location='{{ route('admin.packages.edit', $package->id) }}'">Edit package</button>
                                </div>
                            </div>
                        </div>    
                @endforeach
                <!--/ pricing plan cards -->
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@parent
<script>
    if($("#plan_interval").is(":checked") == true){
        $('.yearly_package').show();
        $('.monthly_package').hide();    
    }
    else{
        $('.monthly_package').show();
        $('.yearly_package').hide();
    }

    $("#plan_interval").change(function(){
        if(this.checked){
            $('.yearly_package').show();
            $('.monthly_package').hide();
        }
        else{
            $('.monthly_package').show();
            $('.yearly_package').hide();
        }
    });
</script>
@endsection