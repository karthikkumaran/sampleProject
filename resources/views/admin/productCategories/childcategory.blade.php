@if(empty($product_category_id))
<option value="{{ $child_category->id }}">
@else
<option value="{{ $child_category->id }}" {{ ($child_category->id === $product_category_id) ? 'selected' : '' }}>
@endif
@for ($i = 0; $i <= $level; $i++)            
        &nbsp;&nbsp;
@endfor    
{{ $child_category->name }}</option>
@if ($child_category->categories)
        @foreach ($child_category->categories as $childCategory)
            @include('admin.productCategories.childcategory', ['child_category' => $childCategory,'level'=>($level + 1)])
        @endforeach
@endif