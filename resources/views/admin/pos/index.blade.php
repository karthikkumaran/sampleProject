@extends('layouts.pos')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')}}">

    <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <style>
        nav {
            width: 100%;
        }
        .scroll{
            max-height:92% !important;
            overflow-y: scroll !important;
        }

        .sidenav {
            height: 100%;
            width: 0;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background-color: #F1F1F1;
            overflow-x: hidden;
            overflow-y: hidden;
            transition: 0.5s;
            padding-top: 60px;
        }
        .sidenav a {
            padding: 5px;
            text-decoration: none;
            font-size: 25px;
            color: #818181;
            display: block;
            transition: 0.3s;
        }
        .sidenav .closebtn {
            position: absolute;
            top: 0;
            right: 25px;
            font-size: 36px;
            margin-left: 50px;
        }
        @media (max-width: 576px) {
            .discountalign{
                margin-bottom : 20px;
            }

            }
        .kbw-signature { width: 100%; height: 200px;}
        #sig canvas{
            width: 100% !important;
            height: auto;
        }

    </style>
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')}}">
@endsection
@section('header')
    <div class="d-inline-flex bg-success w-100">
        <div class="w-lg-50 w-100 d-inline-flex">
            <div class="" style="background: #006622;padding:5px">
                <a class="text-white" href="{{ route('admin.campaigns.storeIndex') }}">
                    <span class="text-center"><i class="feather icon-arrow-left"></i> Back to store</span>
                </a>
            </div>
            <div class="text-white" style="padding:5px;">
                <span class="text-truncate ml-1" id="current_time"></span>
            </div>
        </div>
        <div class="w-lg-50 d-flex">
            <a class="nav-link nav-link-expand d-none d-lg-block d-md-block text-white">
                <i class="feather icon-maximize" style="font-size:150%;"></i></a>
        </div>
    </div>
    <div class="d-lg-inline-flex d-sm-flex w-100 bg-info">
        <div class="w-lg-50 w-md-50 w-100 d-inline-flex" style="background: #ffcc99;">
            <div class="col-1 d-flex justify-content-center align-items-center">
                <i class="feather icon-user" style="font-size:180%;"></i>
            </div>
            @php
                $customer_emails = array();
                $customer_mobile = array();
            @endphp
            <div class="col-8 py-1">
                <select class="select2 w-100" id="customer_select">
                    <option value="">Guest</option>
                    @foreach($customer_address as $customer)
                        @php
                            array_push($customer_emails, $customer->email);
                            array_push($customer_mobile, $customer->mobileno);
                        @endphp
                        <option value="{{$customer->id}}" > {{ $customer->name." - ".$customer->email." - ".$customer->mobile}}</option>
                    @endforeach
                <select>
            </div>
            <div class="col-1 d-flex justify-content-center align-items-center" style="color:white;background:  #ff9900;cursor: pointer;" onclick="show_address()">
                <i class="feather icon-plus" style="font-size:180%;"></i>
            </div>
            <div class="col-2 bg-danger d-inline-flex justify-content-center align-items-center p-1 text-white" id="hold_cart" style="cursor: pointer;">
                <!-- <img src="{{ asset('XR\app-assets\images\icons\target.png') }}" style="colour:white;height: 21px;width: 21px;"> -->
                <span class="d-lg-flex ">&nbsp;HOLD</span>
            </div>
        </div>
        <div class="w-lg-50 w-md-50 w-100 d-lg-inline-flex d-md-inline-flex d-none bg-white">
            <div class="col-lg-9 col-md-8 col-12 h-100 p-1">
                <section id="ecommerce-searchbar">
                    <fieldset class="form-group position-relative mb-0">
                        <input type="text" class="form-control search-product" id="pos_product_search" placeholder="Search here">
                        <div class="form-control-position">
                            <i class="feather icon-search"></i>
                        </div>
                    </fieldset>
                </section>
            </div>
            <div class="col-lg-3 col-sm-4 col-12 text-center p-1 d-flex align-items-center">
                <div class="w-100" id="show_filter_btn" onclick="show_filter()" style="cursor:pointer;border: 2px solid gray;padding: 2px; border-radius:20px;">
                    <i class="feather icon-filter"></i> Filter
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content')
    @php
        if(empty($user_meta_data['settings']['currency'])){
            $currency = "₹";
        }
        else{
            if($user_meta_data['settings']['currency_selection'] == "currency_symbol")
                $currency = App\User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
            else
                $currency = $user_meta_data['settings']['currency'];
        }
        $agent=new \Jenssegers\Agent\Agent();

    @endphp
    <!-- Content Starts -->
    <hr class="" style="margin-top: 5px;">
    <section id="dashboard-ecommerce" style="height:93%">
        <div class="row match-height m-0" style="height:100%">
            <!-- Cart Section -->
            <div class="col-lg-6 col-md-6 col-12 p-0" style="height:100%">
                <div class="h-100">
                    <div class="header p-0 mb-1 d-lg-flex d-none justify-content-between align-items-center text-dark" style="background: #cce6ff;" >
                        <div class="col-3 text-center" style="margin: auto;padding: 0.5rem;"><strong>Product</strong></div>
                        <div class="col-3 text-center" style="margin: auto;padding: 0.5rem;"><strong>Name</strong></div>
                        <div class="col-2 pl-1" style="margin: auto;padding: 0.5rem;"><strong>Price</strong></div>
                        <div class="col-2 pl-1" style="margin: auto;padding: 0.5rem;"><strong>Quantity</strong></div>
                        <div class="col-2 pl-1" style="margin: auto;padding: 0.5rem;"><strong>Subtotal</strong></div>
                    </div>
                    <button type="button" id="productbuttonview" class="btn btn-primary rounded-circle position-absolute p-1 d-lg-none d-md-none " onclick="openNav()" style="top:0.5rem;right:0.5rem;z-index:5;" >
                        <i class="feather icon-box" style="font-size:180%;color:white;"></i>
                    </button>

                    <form action="" method="POST" name="checkout_form" class="h-100">
                        @csrf
                        <div class="checkout-items h-75 px-1 p-lg-0 p-md-0 niceScroll overflow-scroll">
                        </div>
                    </form>

                    <div class="footer" style="position: absolute;bottom:0.01rem;width: 100%;">
                        <div class="row text-center m-0" style="background: #cce6ff;font-size:1rem;padding: 0.5rem;">
                            <div class="col-6">
                                TOTAL ITEM <span class="cart-item-count mr-3 pull-right"><span>
                            </div>
                            <div class="col-6">
                                TOTAL <span class="pull-right mr-1 total-amt" ><span>
                            </div>
                        </div>
                        <div class="row text-center discountalign" style="background:  #e6f3ff;padding: 0.5rem;">
                            <!-- <div class="col-6"> -->
                            <!-- TAX AMOUNT (%) <span class="cart-product-discount pull-right">0<span> -->
                            <!-- </div> -->
                            <div class="col-6">
                                SET DISCOUNT :
                                <select id="setdiscount">
                                    <option value="percentage">PERCENTAGE</option>
                                    <option value="amount">AMOUNT</option>
                                </select>
                            </div>
                            <div class="col-6">
                                <span class="curr" style="display:none;">{!! $currency !!}</span>
                                <input type="number" class="text-right" style="width:85%" id="discount_num"/>
                                <span class="perc">%</span>
                            </div>
                        </div>
                        <div class="row text-center discountalign" style="background:  #e6f3ff;padding: 0.5rem;">
                            <div class="col-6">
                               {{-- <!-- <a href="{{ route("admin.sign.index") }}" ><button class="btn-danger ">customer sign</button></a> --> --}}

                            </div>
                            <div class="col-6 hidden">
                                TOTAL AMOUNT <span class="total-discount grand-total-amt cart-product-discount pull-right">0<span>
                                <!-- DISCOUNT <span class="total-discount cart-product-discount pull-right">0%<span> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--/ Cart Section -->


            <!-- Product Section -->
            <div class="col-lg-6 col-md-6 col-12 pt-1 niceScroll d-none d-lg-flex d-md-flex" style="max-height:92%;overflow-y: scroll;" id="pos_product_section" >
                <section id="pos_products_list" class="grid-view d-block w-100" style="max-height:100%">
                <div id="loading-bg-pos" style="display:none;">
                    <div class="loading-logo">
                        <img src="{{asset('favicon.png')}}">
                    </div>
                    <div class="loading1">
                        <div class="spinner-{{ $meta_data['splash']['loader_type'] ?? 'border' }} w-100 h-100"
                            style="color: {{ $meta_data['splash']['loader_color'] ?? '' }}" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </div>
                </div>
                <div id="pos_products" class="row mb-1 w-100" >
                </div>
                </section>
            </div>
            <!--/ Product Section -->
        </div>
    </section>
    <!-- Content ends -->

<div class="modal fade w-100 w-lg-50" id="pos_filter_modal" tabindex="-1" role="dialog" aria-labelledby="posFilterModalTitle" aria-hidden="true" style="padding-left:10%;padding-right:10%">
    <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="productNewModalTitle">Filters</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <div class="card" style="padding-bottom: 0;">
                    <div class="card-body" style="padding-bottom: 2vh;">
                        <div class="multi-range-price">
                            <div class="multi-range-title pb-75">
                                <h6 class="filter-title mb-0">Price Range</h6>
                            </div>
                            <ul class="list-unstyled price-range" id="price-range">
                                <li>
                                    <span class="vs-radio-con vs-radio-primary py-25">
                                        <input type="radio" name="price-range" checked value="all">
                                        <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                        </span>
                                        <span class="ml-50">All</span>
                                    </span>
                                </li>

                                <li>
                                    <span class="vs-radio-con vs-radio-primary py-25">
                                        <input type="radio" name="price-range" value="0,500">
                                        <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                        </span>
                                        <span class="ml-50">&lt; {!! $currency !!} 500</span>
                                    </span>
                                </li>

                                <li>
                                    <span class="vs-radio-con vs-radio-primary py-25">
                                        <input type="radio" name="price-range" value="500,1000">
                                        <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                        </span>
                                        <span class="ml-50">{!! $currency !!} 500 - {!! $currency !!} 1000</span>
                                    </span>
                                </li>

                                <li>
                                    <span class="vs-radio-con vs-radio-primary py-25">
                                        <input type="radio" name="price-range" value="1000,10000">
                                        <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                        </span>
                                        <span class="ml-50">{!! $currency !!} 1000 - {!! $currency !!} 10000</span>
                                    </span>
                                </li>

                                <li>
                                    <span class="vs-radio-con vs-radio-primary py-25">
                                        <input type="radio" name="price-range" value="10000">
                                        <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                        </span>
                                        <span class="ml-50">&gt; {!! $currency !!} 10000</span>
                                    </span>
                                </li>
                            </ul>
                        </div>
                        <!-- /Price Filter -->


                        <!-- /Price Slider -->
                        <!-- <div class="price-slider" data-currency="{{$currency}}">
                            <div class="form-group row mt-1">
                                <div class="form-group col-6 w-100">
                                    <input class="form-control" style="width: 100%" type="number" name="price_slider_min" id="price_slider_min" placeholder="Min">
                                </div>
                                <div class="form-group col-6 w-100">
                                    <input class="form-control" style="width: 100%" type="number" name="price_slider_max" id="price_slider_max" placeholder="Max">
                                </div>
                                <div class="alert alert-warning" id="error_min" style="display:none;" role="alert">Minimum value should be less than maximum value.</div>
                                <div class="alert alert-warning" id="error_max" style="display:none;" role="alert">Maximum value should be greater than minimum value.</div>
                            </div>
                            <div class="price-slider">
                                <div class="price_slider_amount mb-2">
                                </div>
                                <div class="form-group">
                                    <div class="slider-sm my-1 range-slider" id="price-slider"></div>
                                </div>
                            </div>
                        </div> -->
                        <!-- /Price Range -->
                        <!-- <hr> -->
                        @if(count($product_categories)>0)
                        <hr>
                        <!-- Categories Starts -->
                        <div id="product-categories">
                            <div class="product-category-title">
                                <h6 class="filter-title mb-1">Categories</h6>
                            </div>
                            <ul class="list-unstyled categories-list">
                                <li class="d-flex justify-content-between align-items-center py-25">
                                    <span class="vs-checkbox-con vs-checkbox-primary">
                                        <input type="checkbox" name="category-filter-all" checked value="all">
                                        <span class="vs-checkbox">
                                            <span class="vs-checkbox--check">
                                                <i class="vs-icon feather icon-check"></i>
                                            </span>
                                        </span>
                                        <span class="">All</span>
                                    </span>
                                </li>
                                @foreach($product_categories as $key => $category)
                                <li class="d-flex justify-content-between align-items-center py-25">
                                    <span class="vs-checkbox-con vs-checkbox-primary">
                                        <input type="checkbox" name="category-filter" value="{!! $category->name == " Untitled"?"&nbsp;":$category->name !!}">
                                        <span class="vs-checkbox">
                                            <span class="vs-checkbox--check">
                                                <i class="vs-icon feather icon-check"></i>
                                            </span>
                                        </span>
                                        <span class="">{!! $category->name == "Untitled"?"&nbsp;":$category->name !!}</span>
                                    </span>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- Categories Ends -->
                        @endif
                        @if(count($campaigns) > 0)
                        <hr>
                        <!-- Catalogs Starts -->
                        <div id="product-catalogs">
                            <div class="product-catalog-title">
                                <h6 class="filter-title mb-1">Catalogs</h6>
                            </div>
                            <ul class="list-unstyled categories-list">
                                <li class="d-flex justify-content-between align-items-center py-25">
                                    <span class="vs-checkbox-con vs-checkbox-primary">
                                        <input type="checkbox" name="catalog-filter-all" checked value="all">
                                        <span class="vs-checkbox">
                                            <span class="vs-checkbox--check">
                                                <i class="vs-icon feather icon-check"></i>
                                            </span>
                                        </span>
                                        <span class="">All</span>
                                    </span>
                                </li>
                                @foreach($campaigns as $campaign)
                                <li class="d-flex justify-content-between align-items-center py-25">
                                    <span class="vs-checkbox-con vs-checkbox-primary">
                                        <input type="checkbox" name="catalog-filter" value="{{$campaign->id}}">
                                        <span class="vs-checkbox">
                                            <span class="vs-checkbox--check">
                                                <i class="vs-icon feather icon-check"></i>
                                            </span>
                                        </span>
                                        <span class="">{!! $campaign->name == "Untitled"?"&nbsp;":$campaign->name !!}</span>
                                    </span>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- Catalogs Ends -->
                    </div>
                    @endif
                </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" onclick="clear_filter()">Clear filters</button>
                    <button type="button" id="uploadSubmit" class="btn btn-primary" onclick="apply_filter()">Apply filters</button>
                </div>
            </div>

        </div>
</div>
<div id="mySidenav" class="sidenav d-lg-none d-md-none bg-white">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <div class="w-100 d-lg-inline-flex d-md-inline-flex d-flex bg-white">
    <div class="col-lg-9 col-md-8 col-9 h-100 p-1">
        <section id="ecommerce-searchbar">
            <fieldset class="form-group position-relative mb-0">
                <input type="text" class="form-control search-product" id="pos_product_search1" placeholder="Search here">
                <div class="form-control-position">
                    <i class="feather icon-search"></i>
                </div>
                </fieldset>
            </section>
        </div>
        <div class="col-lg-3 col-sm-4 col-3 text-center p-1 d-flex align-items-center">
            <div class="w-100" id="show_filter_btn" onclick="show_filter()" style="cursor:pointer;border: 2px solid gray;padding: 2px; border-radius:20px;">
                <i class="feather icon-filter"></i><span class="d-none d-lg-block">Filter</span>
            </div>
        </div>
    </div>
    <div class="col-12 pt-1 niceScroll d-flex scroll" style="max-height:92%;overflow-y: scroll;" id="pos_product_section1" >
        <section id="pos_products_list1" class="grid-view d-flex" style="max-height:100%">
            <div id="loading-bg-pos" style="display:none;">
                <div class="loading-logo">
                    <img src="{{asset('favicon.png')}}">
                </div>
                <div class="loading1">
                    <div class="spinner-{{ $meta_data['splash']['loader_type'] ?? 'border' }} w-100 h-100"
                    style="color: {{ $meta_data['splash']['loader_color'] ?? '' }}" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
            </div>
            <div id="pos_products1" class="row mb-1 d-flex p-2 " >
            </div>
        </section>
    </div>
</div>
<div class="modal {{$agent->isMobile()?'modal-fullscreen p-0':''}} fade" id="pos_address_modal" tabindex="-1" role="dialog" aria-labelledby="setCustomerAddressModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="setCustomerAddressModalTitle">Add Customer</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('admin.pos.update')}}" method="POST"  enctype="multipart/form-data"  name="customer_address_form">
                @csrf
                    <div class="modal-body" style="overflow-y:scroll;">
                        <div class="row mb-2" id="customer_email_exists_alert" style="display:none;">
                            <div class="col-lg-12">
                                <div class="alert alert-success" role="alert">Customer with the entered email already exists</div>
                            </div>
                        </div>
                        <div class="row mb-2" id="customer_mobile_exists_alert" style="display:none;">
                            <div class="col-lg-12">
                                <div class="alert alert-success" role="alert">Customer with the entered mobile number already exists</div>
                            </div>
                        </div>
                        <h5 class="title">Enter Customer Details</h5>
                        <div class="row overflow-scroll">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="checkout-name">Customer Name:</label>
                                    <input type="text" id="checkout-name" class="form-control required" name="fname">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="checkout-number">Mobile Number:</label>
                                    <input type="number" id="checkout-mnumber" class="form-control required" name="mnumber">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="checkout-number">Alternative Mobile Number:</label>
                                    <input type="number" id="checkout-amnumber" class="form-control" name="amnumber">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="checkout-email">Email:</label>
                                    <input type="email" id="checkout-email" class="form-control required" name="email">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="checkout-apt_number">Flat, House No, Street:</label>
                                    <input type="text" id="checkout-apt_number" class="form-control required" name="apt_number">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="checkout-landmark">Landmark e.g. near apollo hospital:</label>
                                    <input type="text" id="checkout-landmark" class="form-control" name="landmark">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="checkout-city">Town/City:</label>
                                    <input type="text" id="checkout-city" class="form-control required" name="city">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="checkout-pincode">Pincode:</label>
                                    <input type="number" id="checkout-pincode" class="form-control required" name="pincode">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="checkout-state">State:</label>
                                    <input type="text" id="checkout-state" class="form-control required" name="state">
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="add-type">Address Type:</label>
                                    <select class="form-control" id="add-type" name="addresstype">
                                        <option>Home</option>
                                        <option>Work</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-secondary mb-1 float-left">CLEAR</button>
                        <button type="button" class="btn btn-secondary mb-1 float-left" data-dismiss="modal" aria-label="Close">Cancel</button>
                        <button type="submit" class="btn btn-primary mb-1 delivery-address float-right" id="set_address_btn" onclick="set_address()" disabled>Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<div class="modal fade modal-fullscreen fade p-0" id="customersignup" tabindex="-1" role="dialog" aria-labelledby="customersignup" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Customer signup</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
            <div class="modal-body ">
                <div id="sharelink">
                </div>
                <div class="container">
                    <div class="card" style="width:fit-content; margin: auto" >
                        <div class="card-header">
                            <h5>Customer Signature Pad</h5>
                        </div>
                        <div class="card-body">
                            <div class="sigPad">
                                <div class="row" >
                                <canvas id="pads" width="500" height="200" style="border: 1px solid #ddd;"></canvas>
                                </div>
                                <div class="row mt-1" >
                                    <input type="hidden" name="output" class="output">
                                    <button class="clearButton btn btn-danger mr-2" id="clear">Clear Signature</button>
                                    <button class="btn btn-success" id="customersign">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade w-lg-50 w-100 mx-auto" id="pos_invoice_modal_show" tabindex="-1" role="dialog" aria-labelledby="posInvoiceModalTitle" aria-hidden="true" >
    <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="setCustomerAddressModalTitle">Invoice</h5>
                <button type="button" onClick="window.location.href=window.location.href" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body overflow-scroll">
                <div id="pos_invoice_container">

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('footer')

    <div class="row text-white position-absolute w-100 d-flex" style="font-size:130%;bottom:0.1rem; " >
        <div class="col-6 text-center">
            <div class="row cursor-pointer">

                <div class="col-12 bg-success d-inline-flex justify-content-center align-items-center p-1" id="place_order" style="cursor: pointer;">
                    <i class="fa fa-money" aria-hidden="true"></i><span class="d-lg-flex ">&nbsp;BILL NOW</span>
                </div>
                <!-- <div class="col-6 bg-danger d-inline-flex justify-content-center align-items-center p-1" id="hold_cart" style="cursor: pointer;">
                    <img src="{{ asset('XR\app-assets\images\icons\target.png') }}" style="colour:white;height: 21px;width: 21px;"><span class="d-lg-flex ">&nbsp;HOLD</span>
                </div> -->
            </div>
        </div>
        <div class="col-6 bg-warning text-center d-inline-flex justify-content-center align-items-center">
            <span class="total-amt grand-total-amt" style="font-size:150%;"></span>
        </div>
    </div>
@endsection
@section('scripts')
@parent
<script src="{{asset('XR/app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')}}"></script>
<script src="{{asset('XR/app-assets/vendors/js/extensions/jquery.steps.min.js')}}"></script>
<script src="{{asset('XR/app-assets/vendors/js/forms/validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('XR/assets/js/pos_cart.js')}}"></script>
<script>
    var q = "";
    var searchData = "";
    var sort = "";
    var filter = [];
    var page = 0;
    var customer = "";
    var payment_method = "Cash on delivery";
    var customer_field =$('#customer_select');

    customer_field.change(function(){
        customer = customer_field.val();
        getPosProducts(0);
    });
    function openNav() {
        document.getElementById("mySidenav").style.width = "100%";
        document.getElementById("productbuttonview").style.display = "none";
    }
    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("productbuttonview").style.display = "flex";
    }

    function clear_filter() {
        filter = [];
        $("[name=price-range]").filter("[value=all]").prop("checked", true);
        $("[name=category-filter-all]").filter("[value=all]").prop("checked", true);
        $("[name=category-filter]").prop("checked", false);
        $("[name=catalog-filter-all]").filter("[value=all]").prop("checked", true);
        $("[name=catalog-filter]").prop("checked", false);
        filter = [];
        $("#pos_filter_modal").modal('hide');
        getPosProducts(0);
    }
    function apply_filter() {
        filter = [];
        var price_count = 0;
        var category_count = 0;
        var catalog_count = 0;
        var price_values_array = [];
        var category_values_array = [];
        var catalog_values_array = [];

        var price = document.getElementsByName('price-range');
        for (i = 0; i < price.length; i++) {
            if (price[i].checked) {
                price_count++;
                filter.push({
                    type: 'price',
                    value: price[i].value
                });
            }
        }
        if (price_count == 0) {
            if ((price_slider_min.value != "") && (price_slider_max.value == "")) {
                filter.push({
                    type: 'price',
                    value: price_slider_min.value
                });
            } else if ((price_slider_min.value == "") && (price_slider_max.value != "")) {
                price_values_array.push(0);
                price_values_array.push(price_slider_max.value);
                var price_values = price_values_array.toString();
                filter.push({
                    type: 'price',
                    value: price_values
                });
            } else {
                price_values_array.push(price_slider_min.value);
                price_values_array.push(price_slider_max.value);
                var price_values = price_values_array.toString();
                filter.push({
                    type: 'price',
                    value: price_values
                });
            }
        }

        var catg = document.getElementsByName('category-filter');
        if(catg.length == 0){
            category_count++;
            filter.push({
                type: 'category',
                value: 'all'
            });
        }
        else{
            if ($('input[name="category-filter-all"]').is(":checked") == true) {
                category_count++;
                filter.push({
                    type: 'category',
                    value: $('input[name="category-filter-all"]').val()
                });
            } else {
                for (i = 0; i < catg.length; i++) {
                    if (catg[i].checked) {
                        category_count++;
                        category_values_array.push(catg[i].value);
                        var category_values = category_values_array.toString();
                    }
                }

                if (category_count > 0) {
                    filter.push({
                        type: 'category',
                        value: category_values
                    });
                }
            }
        }

        var catalog = document.getElementsByName('catalog-filter');
        if(catalog.length == 0){
            category_count++;
            filter.push({
                type: 'catalog',
                value: 'all'
            });
        }
        else{
            if ($('input[name="catalog-filter-all"]').is(":checked") == true) {
                catalog_count++;
                filter.push({
                    type: 'catalog',
                    value: $('input[name="catalog-filter-all"]').val()
                });
            } else {
                for (i = 0; i < catalog.length; i++) {
                    if (catalog[i].checked) {
                        catalog_count++;
                        catalog_values_array.push(catalog[i].value);
                        var catalog_values = catalog_values_array.toString();
                    }
                }
                if (catalog_count > 0) {
                    filter.push({
                        type: 'catalog',
                        value: catalog_values
                    });
                }
            }
        }
        $("#pos_filter_modal").modal('hide');
        getPosProducts(0);
    }

    $('input[name="category-filter-all"]').on('change', function() {
        if ($('input[name="category-filter-all"]').is(":checked") == true) {
            $('input[name="category-filter"]').prop('checked', false);
        }
    });

    $('input[name="category-filter"]').on('change', function() {
        if ($('input[name="category-filter"]').is(":checked") == true) {
            $('input[name="category-filter-all"]').prop('checked', false);
        }
    });

    $('input[name="catalog-filter-all"]').on('change', function() {
        if ($('input[name="catalog-filter-all"]').is(":checked") == true) {
            $('input[name="catalog-filter"]').prop('checked', false);
        }
    });

    $('input[name="catalog-filter"]').on('change', function() {
        if ($('input[name="catalog-filter"]').is(":checked") == true) {
            $('input[name="catalog-filter-all"]').prop('checked', false);
        }
    });

    function show_filter(){
        $("#pos_filter_modal").modal('show');
    }

    $("#pos_product_search").on('input', function() {
        searchData = $(this).val();
        if (searchData.length >= 3 || searchData == "")
            getPosProducts(0);
    });
    $("#pos_product_search1").on('input', function() {
        searchData = $(this).val();
        if (searchData.length >= 3 || searchData == "")
            getPosProducts(0);
    });

    //Filter section script ends -->

    getPosProducts(0);

    $("#pos_products_list").on('click', '.pagination a', function(event) {
        event.preventDefault();

        $('li').removeClass('active');
        $(this).parent('li').addClass('active');

        var myurl = $(this).attr('href');
        var page = $(this).attr('href').split('page=')[1];

        getPosProducts(page);
    });
    $("#pos_products_list1").on('click', '.pagination a', function(event) {
        event.preventDefault();

        $('li').removeClass('active');
        $(this).parent('li').addClass('active');

        var myurl = $(this).attr('href');
        var page = $(this).attr('href').split('page=')[1];

        getPosProducts(page);
    });



    function getPosProducts(page) {
        $('#loading-bg-pos').show();
        q = 'q=' + searchData + '&s=' + sort + '&page=' + page + '&filter=' + JSON.stringify(filter);
        $.ajax({
            url: "{{route('admin.pos.ajaxProductsData')}}" + '?' + q,
            type: "get",
            datatype: "html"
        }).done(function(data) {
            $("{{($agent->isDesktop()||$agent->isTablet())?'#pos_products':'#pos_products1'}}").empty().html(data);
            // $("#pos_products").empty().html(data);
            // $("#pos_products1").empty().html(data);
            if (localStorage.getItem("posCart") != null) {
                const now = new Date();
                var now_time = now.getTime();
                var expire_time = localStorage.getItem("expiretime");
                if (now_time > expire_time) {
                    obj.removeItemFromCartAll();
                    obj.totalCart();
                } else {
                    loadCart();
                }
            } else {
                document.getElementById("place_order").style.pointerEvents = "none";
                document.getElementById("hold_cart").style.pointerEvents = "none";
                $(".total-discount").html("0");
                $(".total-amt").html("{!! $currency !!} 0");
                $(".cart-item-count").html("0");
                $(".checkout-items").empty();
                $(".checkout-items").html("<h2 class='text-light text-center'><i class='pt-1 ficon feather icon-shopping-cart fa-3x'></i><br>No item in cart</h2>");
            }
            $('#loading-bg-pos').hide();
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            $('#loading-bg-pos').hide();
            // alert('No response from server');
        });
    }
    setInterval(() => {
        var amt = $(".total-amt").text().replace('₹', '');
        if(amt[1] == 0){
            document.getElementById("place_order").style.pointerEvents = "none";
        }
    }, 1000);
    $(document).ready(function() {
        var currency_code = "{{html_entity_decode($currency)}}";
        var currency = String(currency_code);
        var customer_address_form_data = null;
        setInterval(PosDateTime, 1000);
    });

    function PosDateTime() {
        var dateOptions = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };
        var d = new Date();
        document.getElementById("current_time").innerHTML = d.toLocaleDateString("en-US", dateOptions) + " "+d.toLocaleTimeString();
    }

    function show_product_mobile(){
        $("#pos_products_modal").modal('show');
    }

    function show_address(){
        $("#pos_address_modal").modal('show');
    }

    function set_address(){
        if(Number($(".form-control.required").val().length) < 1){
            toastr.warning('Error', 'Please Enter Valid Details', {
                "positionClass": "toast-bottom-right"
            });
        }
        else{
            var customer_email_array = <?php echo json_encode($customer_emails); ?>;
            var customer_mobile_array = <?php echo json_encode($customer_mobile); ?>;
            if(customer_email_array.includes($("input[name=email]").val())) {
                $("#customer_mobile_exists_alert").hide();
                $("#customer_email_exists_alert").show();
            }
            else if(customer_mobile_array.includes($("input[name=mnumber]").val())) {
                $("#customer_email_exists_alert").hide();
                $("#customer_mobile_exists_alert").show();
            }
            else{
                $("#customer_email_exists_alert").hide();
                $("#customer_mobile_exists_alert").hide();
                $("form[name=customer_address_form]").append("<input type='hidden' value='true' name='customer_address' />");
                $('#pos_address_modal').modal('hide');
            }
        }
    }

    $("form[name=customer_address_form]").on('change keyup', '.required', function(e){
        let Disabled = true;
        $(".required").each(function() {
            let value = this.value
            if ((value)&&(value.trim() !='')){
                Disabled = false
            }else{
                Disabled = true
                return false
              }
        });

        if(Disabled){
            $('#set_address_btn').prop("disabled", true);
        }else{
            $('#set_address_btn').prop("disabled", false);
        }
    });
    var formdata = '';
    $("#place_order").on("click", function() {
        customer = customer_field.val();
        $("form").append("<input type='hidden' value='"+ customer +"' name='customer' />");
        load_data_Cart();
        checkout_form_data = $("form[name=checkout_form]").serialize();
        customer_address_form_data = $("form[name=customer_address_form]").serialize();
        var data = checkout_form_data.concat(customer_address_form_data);
        var orderStatusOption = "";
        var discount = $('#discount_num').val();
        var set_discount = $('#setdiscount').find('option:selected').text();
        var totaldamt = $('.total-discount').text();
        data = data + '&posdiscount=' + discount + '&posdiscountamt=' + totaldamt + '&setdiscount=' + set_discount ;
        console.log('set_discount');
            @foreach(App\orders::STATUS as $key => $label)
                    @if($key != "1" && $key < 5)
                        @if($key == "2")
                            orderStatusOption = orderStatusOption + '<option value="{{ $key }}"  selected>{{$label}}</option>';
                        @else
                            orderStatusOption = orderStatusOption + '<option value="{{ $key }}"  >{{$label}}</option>';
                        @endif
                    @endif
            @endforeach
        console.log(orderStatusOption)
        swal.fire({
        icon:'warning',
        title:'Choose your Order Status:',
        html:'<div >OrderStatus : '+'<select id="status" class="form-control">'+orderStatusOption
        // '<option value="order">orderReceived</option>'+'<option value="receive">accepted</option>'+'<option value="ship">shipping</option>'
        + '</select></div> <br>'+'<div>Write something here:</div>'+
      '<textarea row="5" cols="40" class="form-control" id="comment"></textarea></div> <br>'+
      '<input id="chk" checked value="true" type="checkbox">'+
	  '<label>Show this comment to the customer</label>',
        showCancelButton:true,
        cancelButtonText:"Cancel",
        confirmButtonText:"Ok",
        cancelButtonColor: "#DD6B55",
        preConfirm: function() {
            return new Promise((resolve, reject) => {
                resolve({
                    remarks: $('#comment').val(),
                    showremarks: $('#chk').prop('checked'),
                    status: $('#status').val()
                });
            });
        }
    }).then(function(result){
        console.log(data);
        data = data + '&status=' + result.value.status + '&remarks=' + result.value.remarks + '&showremarks=' + result.value.showremarks;
         formdata = data;
         @if(isset($user_meta_data['pos_setting']['display_customer_sign']) && $user_meta_data['pos_setting']['display_customer_sign'] == 'on')
            $('#customersignup').modal('show');
        @else
                formSubmit(formdata);
        @endif
    });
    });

    $('#customersignup').on('shown.bs.modal', function (e) {
        //var sig = $('#sig').signature({syncField: '#signature64', syncFormat: 'PNG'});
        // $('#clear').click(function(e) {
        //     e.preventDefault();
        //     sig.signature('clear');
        //     $("#signature64").val('');
        // });

        // $("#customersigndata").submit(function (e) {
        // $(document).on("#customersigndata", "submit", function(e){
        //     console.log("test submit")
        //     e.preventDefault();
        // });

        $("#customersign").click(async function (){

                console.log('executed');

                var canvas = document.getElementById('pads');
                var context = canvas.getContext('2d');
                var signed = $('.output').val();

                console.log(signed)


                console.log(canvas.toDataURL('image/png'));




                //return;
                // console.log(signed,formdata);
                $('#customersignup').toggle();

                console.log(formdata);

                var submitdata = formdata +'&customersign=' + canvas.toDataURL('image/png');
                console.log(submitdata);
                formSubmit(submitdata);

        });

    });

    $("#hold_cart").on("click", function() {
        saveCart();
        toastr.success('Cart saved successfully');
    });

    function load_data_Cart() {
        $('input[name="products"]').remove();
        $('input[name="catalogs"]').remove();
        $('input[name^=products_qty]').remove();
        if(customer == null || customer == ""){
            cart = JSON.parse(localStorage.getItem('posCart'));
        }
        else{
            customer_cart = 'posCart_' + customer;
            cart = JSON.parse(localStorage.getItem(customer_cart));
            if(cart == null){
                cart = JSON.parse(localStorage.getItem('posCart'));
            }
        }
        var cartItemCount = count = cart.length;
        if (cart.length > 0) {
            var products = [];
            var catalogs = [];
            for (var item in cart) {
                products.push(cart[item].sku);
                catalogs.push(cart[item].catalogid);
                var id = cart[item].id,
                    qty = cart[item].qty,
                    sku=cart[item].sku;
                $("form").append("<input type='hidden' id='product_qty_" + sku + "' name='products_qty[" + sku + "]' value='" + qty + "'>");
                obj.totalCart();
            }
            $("form").append("<input type='hidden' name='products' value='" + products + "'>");
            $("form").append("<input type='hidden' name='catalogs' value='" + catalogs + "'>");
            $("form").append("<input type='hidden' name='currency' value='" + currency + "'>");

        }
        // else {
        //     $(".cart-item-count").html("");
        //     $(".ecommerce-application").empty();
        //     $(".ecommerce-application").html("<h2 class='text-light text-center'>No item in cart</h2>");
        // }
    }

    obj.totalCart = function() {
        var totalCart = 0;
        for (var item in cart) {
            if (cart[item].discounted_price != 0) {
                totalCart += cart[item].discounted_price * cart[item].qty;
            } else {
                totalCart += cart[item].price * cart[item].qty;
            }
        }

        $('.total-amt').html('{!! $currency !!} ' + Number(totalCart.toFixed(2)));
        $('.grand-total-amt').html('{!! $currency !!} ' + Number(totalCart.toFixed(2)));
        $('.total-discount').html('{!! $currency !!} ' + Number(totalCart.toFixed(2)));

        if (cart.length > 0) {
            $(".cart-item-count").html(cart.length);
            $(".price-items").html("Price of " + cart.length + " items");
        } else {
            $(".cart-item-count").html("");
            $(".checkout-items").empty();
            $(".checkout-items").append().html("<h2 class='text-light text-center'><i class='pt-1 ficon feather icon-shopping-cart fa-3x'></i><br>No item in cart</h2>");
        }
        return Number(totalCart.toFixed(2));
    }

    $('#discount_num').on('input',function(){
        var discount_val = $('#discount_num').val();
        var set_discount = $('#setdiscount').find('option:selected').text();
        if(discount_val){
            if(set_discount == 'PERCENTAGE')
            {
                if(discount_val < 100){
                    total_dis_val = totalCart - ((discount_val * totalCart)/100);
                    if(total_dis_val)
                    {
                        $('total-discount').append().html('{!! $currency !!} ' + Number(total_dis_val.toFixed(2)));
                        $('.grand-total-amt').html('{!! $currency !!} ' + Number(total_dis_val.toFixed(2)));
                    }
                }
                else{
                    alert('Discount amount is not more than 100');
                }
            }
            else{
                if(discount_val < totalCart)
                {
                    total_dis_val = totalCart - discount_val;
                    if(total_dis_val)
                    {
                        $('total-discount').append().html('{!! $currency !!} ' + Number(total_dis_val.toFixed(2)));
                        $('.grand-total-amt').html('{!! $currency !!} ' + Number(total_dis_val.toFixed(2)));
                    }
                }
                else{
                        alert('Discount amount should not be more than Total amount');
                    }
            }
        }
        else{
            $('total-discount').append().html('{!! $currency !!} ' + Number(totalCart.toFixed(2)));
            $('.grand-total-amt').html('{!! $currency !!} ' + Number(totalCart.toFixed(2)));
        }
    });

    $('#setdiscount').on('change',function(){
        var discount_val = $('#discount_num').val();
        var set_discount = $('#setdiscount').find('option:selected').text();
            if(set_discount == 'PERCENTAGE')
            {
                $('.curr').css('display','none');
                $('.perc').css('display','inline-block');
            }
            else{
                $('.curr').css('display','inline-block');
                $('.perc').css('display','none');
            }
        if(discount_val){
            if(set_discount == 'PERCENTAGE')
            {

                if(discount_val < 100){
                    total_dis_val = totalCart - ((discount_val * totalCart)/100);
                    if(total_dis_val)
                    {
                        $('total-discount').append().html('{!! $currency !!} ' + Number(total_dis_val.toFixed(2)));
                        $('.grand-total-amt').html('{!! $currency !!} ' + Number(total_dis_val.toFixed(2)));
                    }
                }
                else{
                    alert('discount amount is not more than 100');
                }
            }
            else{
                if(discount_val < totalCart)
                {
                    total_dis_val = totalCart - discount_val;
                    if(total_dis_val)
                    {
                        $('total-discount').append().html('{!! $currency !!} ' + Number(total_dis_val.toFixed(2)));
                        $('.grand-total-amt').html('{!! $currency !!} ' + Number(total_dis_val.toFixed(2)));
                    }
                }
                else{
                        alert('discount amount is not more than total amount');
                    }
            }
        }
        else{
            $('total-discount').append().html('{!! $currency !!} ' + Number(totalCart.toFixed(2)));
            $('.grand-total-amt').html('{!! $currency !!} ' + Number(totalCart.toFixed(2)));
        }
    });

    function setQuantity($this,val,sku){
        var item_id = $this.data('sku');
        var item_quantity = $this.parent().find(sku);
        var cart_tag = $this.parent().parent().parent();
        var qty = Number(item_quantity.text());
        var stock = Number($this.parent().data('stock'));
        var max_qty = Number(cart_tag.data('max_qty'));
        var min_qty = Number(cart_tag.data('min_qty'));
        var qty1=val;
        if(qty1>=max_qty){
            qty1=max_qty;
        toastr.warning('Product reached Maximum quantity');
        }
        if(qty1>=stock){
            qty1=stock;
        }
        if(qty1<min_qty){
            qty1=min_qty;
        toastr.warning('Product reached Minimum quantity');
        }
        obj.setCountForItem($this.data('sku'), qty1);
        item_quantity.html(qty1);
        // document.getElementById("item_quantity_"+item_id).value=qty1;
        $('#item_quantity_'+item_id).val(qty1);

            if (Number(cart_tag.data('discounted_price')) == 0) {
                $("#subtotal_"+item_id).html(qty1 * Number(cart_tag.data('price')));
            } else {
                $("#subtotal_"+item_id).html(qty1 * Number(cart_tag.data('discounted_price')));
            }
        totalCart = obj.totalCart();
        $('.total-amt').append().html('{!! $currency !!} ' + Number(totalCart.toFixed(2)));

    }

    function increaseQuantity($this){
        var item_id = $this.data('sku');
        var item_quantity = $this.parent().find('#item_quantity_'+item_id);
        var cart_tag = $this.parent().parent().parent();
        var stock = Number($this.parent().data('stock'));
        var eleval=document.getElementById("item_quantity_"+item_id).value;
        var max_qty = Number(cart_tag.data('max_qty'));
        var qty = Number(eleval);
        $('.pos_negative_'+item_id).removeClass('disabled');
        if(qty == stock || qty >= max_qty){
        toastr.warning('Product reached Maximum quantity');
            $this.addClass('disabled');
            return false;
        }
        var qty1=qty+1;
        if(qty1 == stock){
            $this.addClass('disabled');
        }
        if(qty > 1){
            $this.siblings().prev().removeClass('disabled');
        }
        obj.setCountForItem(($this.data('sku')), qty1);
        document.getElementById("item_quantity_"+item_id).value=qty1;
        // $('#item_quantity_'+item_id).html(qty1);
        if (Number($this.parent().data('discounted_price')) == 0) {
            $("#subtotal_"+item_id).html(qty1 * Number($this.parent().data('price')));
        } else {
            $("#subtotal_"+item_id).html(qty1 * Number($this.parent().data('discounted_price')));
        }
        totalCart = obj.totalCart();
        $('.total-amt').append().html('{!! $currency !!} ' + Number(totalCart.toFixed(2)));
        $('total-discount').append().html('{!! $currency !!} ' + Number(totalCart.toFixed(2)));
        $('.grand-total-amt').html('{!! $currency !!} ' + Number(totalCart.toFixed(2)));
        $('#discount_num').val("0");
    }

    function decreaseQuantity($this){
        var item_id = $this.data('sku');
        var item_quantity = $this.parent().find('#item_quantity_'+item_id);
        var stock = Number($this.parent().data('stock'));
        var cart_tag = $this.parent().parent().parent();
        var eleval=document.getElementById("item_quantity_"+item_id).value;
        var min_qty = Number(cart_tag.data('min_qty'));
        var qty = Number(eleval);
        $('.pos_positive_'+item_id).removeClass('disabled');

        if(qty <= min_qty){
            $this.addClass('disabled');
            toastr.warning('Product reached Minimum quantity');
            return false;
        }
        var qty1=qty-1;
        if(qty < stock){
            $this.siblings().next().removeClass('disabled');
        }
        if(qty1 == 1){
            $this.addClass('disabled');
        }
        obj.setCountForItem(($this.data('sku')), qty1);
        document.getElementById("item_quantity_"+item_id).value=qty1;
        if (Number($this.parent().data('discounted_price')) == 0) {
            $("#subtotal_"+item_id).html(qty1 * Number($this.parent().data('price')));
        } else {
            $("#subtotal_"+item_id).html(qty1 * Number($this.parent().data('discounted_price')));
        }
        totalCart = obj.totalCart();
        $('.total-amt').append().html('{!! $currency !!} ' + Number(totalCart.toFixed(2)));
        $('total-discount').append().html('{!! $currency !!} ' + Number(totalCart.toFixed(2)));
        $('.grand-total-amt').html('{!! $currency !!} ' + Number(totalCart.toFixed(2)));
        $('#discount_num').val("0");
    }

    function removeItem($this)
    {
        $this.closest(".cart-item-card").remove();
        obj.removeItemFromCart(($this.data('sku')));
        getPosProducts(0);
        $('total-discount').append().html('{!! $currency !!} ' + Number(totalCart.toFixed(2)));
        $('.grand-total-amt').html('{!! $currency !!} ' + Number(totalCart.toFixed(2)));
        $('#discount_num').val("0");
    }

    var currency_code = "{{html_entity_decode($currency)}}";
    var currency = String(currency_code);

    function loadCart() {
        if(customer == null || customer == ""){
            cart = JSON.parse(localStorage.getItem('posCart'));
        }
        else{
            customer_cart = 'posCart_' + customer;
            cart = JSON.parse(localStorage.getItem(customer_cart));
            if(cart == null){
                cart = JSON.parse(localStorage.getItem('posCart'));
            }
        }
        var cartItemCount = count = cart.length;
        $(".checkout-items").empty();
        document.getElementById("place_order").style.pointerEvents = "";
        document.getElementById("hold_cart").style.pointerEvents = "";
        if (cart.length > 0) {
            var $this = cartBtn,
            addToCart = $this.find(".add-to-cart"),
            viewInCart = $this.find(".remove-from-cart");
            // alert(cartBtn.length);
            for (var i = 0; i < cartBtn.length; i++) {
                addToCart = $(cartBtn[i]).find(".add-to-cart");
                viewInCart = $(cartBtn[i]).find(".remove-from-cart");
                var pid = addToCart.data("sku");
                var cid = addToCart.data("catalogid");
                let obj = cart.find(o => (o.sku === pid && o.catalogid === cid));
                if (obj != undefined) {
                    addToCart.addClass("d-none");
                    $(cartBtn[i]).addClass("bg-danger");
                    viewInCart.addClass("d-inline-block");
                }
            }
            var subt = 0;
            $(".cart-item-count").html(count);
            totalCart = obj.totalCart();
            totalDiscount = obj.totalDiscount();
            average_discount = totalDiscount/count;
            $('.total-discount').append().html(Number(totalCart.toFixed(2)));
            // $('.total-discount').append().html(Number(average_discount.toFixed(2)) + '%');
            $('.total-amt').append().html('{!! $currency !!} ' + Number(totalCart.toFixed(2)));
            var products = [];
            var catalogs = [];
            for (var item in cart) {
                // var html_1 = '<div class="cart-item-card mb-1"><div class="rounded bg-white d-inline-flex shadow-lg w-100"  style="height:100px;"><div class="w-25 h-100"><img src="' + cart[item].img + '" class="mw-100 h-100 rounded border-0"></div><div class="h-100 justify-content-center text-center text-dark text-truncate d-flex justify-content-center align-items-center" style="width:20%;"><span class="">' + cart[item].name + '</span></div><div class="badge badge-pill badge-glow badge-primary mr-1 d-none">' + cart[item].catagory + ' </div>'
                // if (cart[item].discounted_price == 0) {
                //     subt = cart[item].price * cart[item].qty;
                //     var html_2 = '<div class="h-100 text-center d-flex align-items-center justify-content-center" style="width:20%;">{!! $currency !!} ' + cart[item].price + '</div><div class="h-100 d-flex justify-content-center text-center text-dark align-items-center" style="width:20%;" data-price="' + cart[item].price + '" data-discounted_price="'+ cart[item].discounted_price + '" data-stock="' + cart[item].stock + '"><a class="btn p-0 decq" style="cursor: pointer;" onclick="decreaseQuantity($(this))" data-id="' + cart[item].id + '"><i class="feather icon-minus-circle text-dark"></i></a><span class="ml-1 mr-1" id="item_quantity_' + cart[item].id + '">' + cart[item].qty + '</span><a class="btn p-0 incq" onclick="increaseQuantity($(this))" data-id="' + cart[item].id + '"><i class="feather icon-plus-circle text-dark"></i></a></div><div class="h-100 text-right d-flex justify-content-center align-items-center" style="width:20%;font-size:130%;">{!! $currency !!} <span id="subtotal_' + cart[item].id + '">' + subt + '</span><span class="remove-item" style="cursor: pointer;" data-id="' + cart[item].id + '" onclick="removeItem($(this))">&nbsp;<i class="feather icon-x align-right" style="color:red;"></i><span></div></div>';
                // } else {
                //     subt = cart[item].discounted_price * cart[item].qty;
                //     var html_2 = '<div class="h-100 text-center d-flex align-items-center justify-content-center" style="width:20%;">{!! $currency !!} '+ cart[item].discounted_price + ' <br><span class="text-light"><s>' + cart[item].price + '</s></span></div><div class="h-100 d-flex align-items-center justify-content-center text-center text-dark" style="width:20%;" data-price="' + cart[item].price + '" data-discounted_price="'+ cart[item].discounted_price + '" data-stock="' + cart[item].stock + '"><a class="btn p-0 decq" style="cursor: pointer;" onclick="decreaseQuantity($(this))" data-id="' + cart[item].id + '"><i class="feather icon-minus-circle text-dark"></i></a><span class="ml-1 mr-1" style="cursor: pointer;" id="item_quantity_' + cart[item].id + '">' + cart[item].qty + '</span><a class="btn p-0 incq"  onclick="increaseQuantity($(this))" data-id="' + cart[item].id + '"><i class="feather icon-plus-circle text-dark"></i></a></div><div class="h-100 text-right d-flex justify-content-center align-items-center" style="width:20%;font-size:130%;">{!! $currency !!} <span id="subtotal_' + cart[item].id + '">' + subt + '</span><span class="remove-item" style="cursor: pointer;" data-id="' + cart[item].id + '" onclick="removeItem($(this))">&nbsp;<i class="feather icon-x align-middle" style="color:red;"></i><span></div></div>';
                // }
                if (cart[item].discounted_price == 0) {
                    subt = cart[item].price * cart[item].qty;
                    // alert(cart[item].sku);
                    var html_1='<div class="w-100 mb-1 shadow-lg bg-white rounded d-inline-flex" style="height:10rem;padding:5px;"><div class="h-100 justify-content-center d-flex align-items-center border-right-secondary" style="{{($agent->isDesktop()||$agent->isTablet())?'width:25%;':'width:40%;'}} padding:5px;"><img src="' + cart[item].img + '" class="{{($agent->isDesktop()||$agent->isTablet())?'mw-100 h-100':'w-100 mh-100'}} rounded border-0"></div><div class="h-100 align-items-center d-lg-inline-flex d-md-inline-flex px-1" style="{{($agent->isDesktop()||$agent->isTablet())?'width:75%;':'width:60%;'}}"><div class="{{($agent->isDesktop()||$agent->isTablet())?'w-25 h-100 d-flex justify-content-center align-items-center':'w-100'}}" style="padding:5px;"><span class="text-truncate">' + cart[item].name + '</span><div class="badge badge-pill badge-glow badge-primary d-none">' + cart[item].catagory + ' </div></div><div class="text-truncate {{($agent->isDesktop()||$agent->isTablet())?'w-25 h-100 d-flex justify-content-center align-items-center':'w-100'}}" style="padding:5px;">{!! $currency !!} ' + cart[item].price + '</div><div class="{{($agent->isDesktop()||$agent->isTablet())?'w-25 h-100 d-flex justify-content-center align-items-center':'w-100'}}" style="padding:5px;" data-price="' + cart[item].price + '" data-discounted_price="' + cart[item].discounted_price + '" data-min_qty="' + cart[item].min_qty + '" data-max_qty="' + cart[item].max_qty +'"data-sku="'+cart[item].sku+'"><div class="w-75 d-inline-flex"><div class="w-25 d-flex justify-content-center align-items-center bg-warning rounded" data-price="' + cart[item].price + '" data-discounted_price="' + cart[item].discounted_price + '"><a class="btn p-0 decq pos_negative_'+cart[item].id+'" style="cursor: pointer;" onclick="decreaseQuantity($(this))" data-id="' + cart[item].id + '"data-sku="'+cart[item].sku+'"><i class="feather icon-minus text-white"></i></a></div><div class="w-50 d-flex justify-content-center align-items-center"><input type="number" class="w-100 mh-100 form-control text-center setQuantity" id="item_quantity_' + cart[item].sku + '"  data-id="' + cart[item].id + '"data-sku="'+cart[item].sku+'" data-value="' + cart[item].qty + '" value="' + cart[item].qty + '" onchange="setQuantity($(this),this.value,this.sku)" ></div><div class="w-25 d-flex justify-content-center align-items-center bg-warning rounded " data-price="' + cart[item].price + '" data-discounted_price="' + cart[item].discounted_price + '"><a class="btn p-0 incq pos_positive_'+cart[item].id+'" onclick="increaseQuantity($(this))" data-id="' + cart[item].id + ' "data-sku="'+cart[item].sku+'"><i class="feather icon-plus text-white"></i></a></div></div></div><div class="{{($agent->isDesktop()||$agent->isTablet())?'w-25 h-100 d-flex justify-content-center align-items-center':'w-100'}}" style="padding:5px;">{!! $currency !!}<span id="subtotal_' + cart[item].sku + '">' + subt + '</span><span class="remove-item" style="cursor: pointer;" data-id="' + cart[item].id + ' "data-sku="'+cart[item].sku+'" onclick="removeItem($(this))">&nbsp;&nbsp;&nbsp;<i class="feather icon-x align-right" style="color:red;"></i></span></div></div></div>';
                } else {
                    // alert(cart[item].sku);
                    subt = cart[item].discounted_price * cart[item].qty;
                    var html_1='<div class="w-100 mb-1 shadow-lg bg-white rounded d-inline-flex" style="height:10rem;padding:5px;"><div class="h-100 justify-content-center d-flex align-items-center border-right-secondary" style="{{($agent->isDesktop()||$agent->isTablet())?'width:25%;':'width:40%;'}} padding:5px;"><img src="' + cart[item].img + '" class="{{($agent->isDesktop()||$agent->isTablet())?'mw-100 h-100':'w-100 mh-100'}} rounded border-0"></div><div class="h-100 align-items-center d-lg-inline-flex d-md-inline-flex px-1" style="{{($agent->isDesktop()||$agent->isTablet())?'width:75%;':'width:60%;'}}"><div class="{{($agent->isDesktop()||$agent->isTablet())?'w-25 h-100 d-flex justify-content-center align-items-center':'w-100'}}" style="padding:5px;"><span class="text-truncate">' + cart[item].name + '</span><div class="badge badge-pill badge-glow badge-primary d-none">' + cart[item].catagory + ' </div></div><div class="text-truncate {{($agent->isDesktop()||$agent->isTablet())?'w-25 h-100 d-flex justify-content-center align-items-center':'w-100'}}" style="padding:5px;">{!! $currency !!}'+ cart[item].discounted_price + '&nbsp;<span class="text-light"><s>{!! $currency !!}'+ cart[item].price +'</s></span></div><div class="{{($agent->isDesktop()||$agent->isTablet())?'w-25 h-100 d-flex justify-content-center align-items-center':'w-100'}}" style="padding:5px;" data-price="' + cart[item].price + '" data-discounted_price="' + cart[item].discounted_price + '" data-min_qty="' + cart[item].min_qty + '" data-max_qty="' + cart[item].max_qty +'"data-sku="'+cart[item].sku+'"><div class="w-75 d-inline-flex"><div class="w-25 d-flex justify-content-center align-items-center bg-warning rounded" data-price="' + cart[item].price + '" data-discounted_price="' + cart[item].discounted_price + '"><a class="btn p-0 decq pos_negative_'+cart[item].id+'" style="cursor: pointer;" onclick="decreaseQuantity($(this))" data-id="' + cart[item].id + '"data-sku="'+cart[item].sku+'"><i class="feather icon-minus text-white"></i></a></div><div class="w-50 d-flex justify-content-center align-items-center"><input type="number" class="w-100 mh-100 form-control text-center setQuantity" id="item_quantity_' + cart[item].sku + '" "data-sku="'+cart[item].sku+'"  data-id="' + cart[item].id + '" data-value="' + cart[item].qty + '" value="' + cart[item].qty + '" onchange="setQuantity($(this),this.value,this.sku)" ></div><div class="w-25 d-flex justify-content-center align-items-center bg-warning rounded" data-price="' + cart[item].price + '" data-discounted_price="' + cart[item].discounted_price + '"><a class="btn p-0 incq pos_positive_'+cart[item].id+'" onclick="increaseQuantity($(this))" data-id="' + cart[item].id + '"data-sku="'+cart[item].sku+'"><i class="feather icon-plus text-white"></i></a></div></div></div><div class="{{($agent->isDesktop()||$agent->isTablet())?'w-25 h-100 d-flex justify-content-center align-items-center':'w-100'}}" style="padding:5px;">{!! $currency !!}<span id="subtotal_' + cart[item].sku + '">' + subt + '</span><span class="remove-item" style="cursor: pointer;" data-id="' + cart[item].id + '"data-sku="'+cart[item].sku+'" onclick="removeItem($(this))">&nbsp;&nbsp;&nbsp;<i class="feather icon-x align-right" style="color:red;"></i></span></div></div></div>';
                }
                var html = html_1;
                // alert(html);
                $(".checkout-items").append(html);
                products.push(cart[item].sku);
                catalogs.push(cart[item].catalogid);
                var id = cart[item].id,
                    sku=cart[item].sku,
                    qty = cart[item].qty,
                    catalogid = cart[item].catalogid;
                $("form").append("<input type='hidden' id='product_qty_" + sku + "' name='products_qty[" + sku + "]' value='" + qty + "'>");
                // for (var catalog in cart) {
                //     if (catalogs.indexOf(catalogid) == -1)
                //         catalogs.push(catalogid);
                // }
                obj.totalCart();
            }
            $("form").append("<input type='hidden' name='products' value='" + products + "'>");
            $("form").append("<input type='hidden' name='catalogs' value='" + catalogs + "'>");
            $("form").append("<input type='hidden' name='currency' value='" + currency + "'>");

        } else {
            document.getElementById("place_order").style.pointerEvents = "none";
            document.getElementById("hold_cart").style.pointerEvents = "none";
            $(".total-discount").html("0");
            $(".total-amt").html("{!! $currency !!} 0");
            $(".cart-item-count").html("0");
            $(".checkout-items").empty();
            $(".checkout-items").html("<h2 class='text-light text-center'><i class='pt-1 ficon feather icon-shopping-cart fa-3x'></i><br>No item in cart</h2>");

        }
    }

    function downloadpdf(order_id){
        url = "{{ route('customer_export_order_pdf',':order_id') }}";
        url = url.replace(':order_id',order_id);
        window.location.href = url;
    }

    function formSubmit(data) {
        $('#loading-bg').show();
        data = data + '&payment_method=' + payment_method;
        $.ajax({
            url: "{{route('admin.pos.order')}}",
            type: "POST",
            data: data,
            success: function(data) {
                obj.clearCart();
                console.log(obj);
                $('#loading-bg').hide();
                if(data){
                    $('#customersignup').modal('hide');
                    $('#pos_invoice_container').html(data.html);
                    $("#pos_invoice_modal_show").modal('show');
                }else{
                    // console.log('erro no modal');
                }
            },
            error: function(e){
                console.log(e);
                $("#pos_invoice_modal_show").modal('hide');
                $('#loading-bg').hide();
            }
        });

    }

    $(document).on("keypress", ".setQuantity", function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            $(this).trigger("change");
            // $("form").submit();
        }
    });

</script>

<script src="{{asset('/XR/assets/libs/signature_pad/jquery.signaturepad.min.js')}}"></script>

  <script>
    $(document).ready(function() {
        $('.sigPad').signaturePad({drawOnly:true, lineWidth: 0, onDraw: enableButton});
        $('#customersign').attr('disabled', true);

        console.log($(''))

        if(window.innerWidth < 480){
            $('#pads').attr('width', 250);
            $('#pads').attr('height', 200);
        }else if(window.innerWidth < 767){
            $('#pads').attr('width', 350);
            $('#pads').attr('height', 200);
        }else if(window.innerWidth < 1280){
            $('#pads').attr('width', 500);
            $('#pads').attr('height', 200);
        }else{
            $('#pads').attr('width', 600);
            $('#pads').attr('height', 300);
        }
    });

    $('#clear').on('click', ()=>{
        $('#customersign').attr('disabled', true);
    })

    function enableButton(){
        $('#customersign').removeAttr('disabled');
    }



  </script>
  <script src="{{asset('/XR/assets/libs/signature_pad/json2.min.js')}}"></script>

@endsection
