@extends('layouts.admin')
@section('content')
@can('user_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.users.create") }}">
                {{ trans('global.add') }} {{ trans('cruds.user.title_singular') }}
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.user.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.name') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.email') }}
                        </th>
                        <th>
                            Domain
                        </th>
                        <!-- <th>
                            {{ trans('cruds.user.fields.email_verified_at') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.approved') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.verified') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.roles') }}
                        </th> -->
                        <th>
                            Plan name
                        </th>
                        <th>
                            Subscription status
                        </th>
                        <th>
                            Subscription start date
                        </th>
                        <th>
                            Subscription end date
                        </th>
                        <th>
                            &nbsp;Actions
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($users as $key => $user)
                        @php
                            $subscriptionDetails = $user->getSubscriptionDetails();
                            $user_info = $user->getUserInfo();
                            if(!empty($user_info))
                                $storeLink = $user->getStoreLink($user_info->public_link,"false");
                        @endphp
                        <tr data-entry-id="{{ $user->id }}">
                            <td>
                                {{ $user->id ?? '' }}
                            </td>
                            <td>
                            @can('impersonate_user')
                                    @canBeImpersonated($user)
                                        @if($user->verified)
                                            <a  style="color:black; " href="{{ route('admin.users.impersonate',$user->id)}}">
                                            {{ $user->name ?? '' }}
                                            </a>
                                        @else
                                            <a style="color:black; " href="#">
                                            {{ $user->name ?? '' }}
                                            </a>
                                        @endif
                                    @endCanBeImpersonated
                                @endcan
                                {{--{{ $user->name ?? '' }}--}}
                            </td>
                            <td>
                                {{ $user->email ?? '' }}
                            </td>
                            <td>
                                @if($user->getIsAdminAttribute())
                                @else
                                    @if(!empty($user_info))
                                        @if($storeLink != false)
                                            <a href="{{ $storeLink }}" target="__blank" class="" >{{ $storeLink }}</a>
                                        @else
                                            <span class="text-danger"><b>Domain not set</b></span>
                                        @endif
                                    @endif
                                @endif
                            </td>
                            <td>
                                {{ $subscriptionDetails->plan->name ?? ""}}
                            </td>
                            <td>
                                @if($user->getIsAdminAttribute())
                                @else
                                    @if(!empty($user->isSubscriptionExpired()))
                                        @if($user->isSubscriptionExpired() == 1)
                                            <span class="text-danger"><b>Expired</b></span>
                                        @else
                                            <span class="text-primary"><b>{{$user->isSubscriptionExpired()}}</b></span>
                                        @endif
                                    @else
                                        <span class="text-success"><b>Active</b></span>
                                    @endif
                                    
                                @endif
                            </td>
                                
                            <td>
                                @if($user->getIsAdminAttribute())
                                @else
                                    {{ $subscriptionDetails->starts_at ?? "" }}
                                @endif
                            </td>
                            <td>
                                @if($user->getIsAdminAttribute())
                                @else
                                    {{ $subscriptionDetails->ends_at ?? "" }}
                                @endif
                            </td>
                            <!-- <td>
                                {{ $user->email_verified_at ?? '' }}
                            </td>
                            <td>
                                <span style="display:none">{{ $user->approved ?? '' }}</span>
                                <input type="checkbox" disabled="disabled" {{ $user->approved ? 'checked' : '' }}>
                            </td>
                            <td>
                                <span style="display:none">{{ $user->verified ?? '' }}</span>
                                <input type="checkbox" disabled="disabled" {{ $user->verified ? 'checked' : '' }}>
                            </td>
                            <td>
                                @foreach($user->roles as $key => $item)
                                    <span class="badge badge-info">{{ $item->title }}</span>
                                @endforeach
                            </td> -->
                            <td>
                                @can('user_show')
                                    <a class="btn btn-xs btn-primary btn-icon" href="{{ route('admin.users.show', $user->id) }}">
                                        <i class="feather icon-eye"></i>
                                    </a>
                                @endcan

                                @can('user_edit')
                                    <a class="btn btn-xs btn-info btn-icon" href="{{ route('admin.users.edit', $user->id) }}">
                                        <i class="feather icon-edit"></i>
                                    </a>
                                @endcan

                                @can('user_delete')
                                    <form action="{{ route('admin.users.destroy', $user->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger btn-icon"><i class="feather icon-trash"></i></button>
                                    </form>
                                @endcan 
                                @can('impersonate_user')
                                    @canBeImpersonated($user)
                                        @if($user->verified)
                                            <a class="btn btn-xs btn-warning btn-icon" style="color:white; " href="{{ route('admin.users.impersonate',$user->id)}}">
                                                <i class="fa fa-key"></i>
                                            </a>
                                        @else
                                            <a class="btn btn-xs btn-warning btn-icon disabled" style="color:white; " href="#">
                                                <i class="fa fa-key"></i>
                                            </a>
                                        @endif
                                    @endCanBeImpersonated
                                @endcan
                                @if($user->getIsAdminAttribute())
                                @else
                                    @if(!empty($subscriptionDetails))
                                        <!-- <a class="btn btn-xs btn-secondary btn-icon" style="color:white; " href="{{ route('admin.adminSubscription.userSubscription', $user->id) }}">
                                            <i class="fa fa-credit-card"></i>
                                        </a> -->
                                        <a class="btn btn-xs btn-secondary btn-icon" style="color:white; " onclick="adminRenewModalShow({{$user->id}})">
                                            <i class="fa fa-credit-card"></i>
                                        </a>
                                        <a class="btn btn-xs btn-primary btn-icon" style="color:white; " onclick="adminRenewEditShow({{$user->id}},{{$subscriptionDetails}})">
                                        <i class="fa fa-pencil-square-o"></i>
                                        </a>
                                        <a class="btn btn-xs btn-success btn-icon" href="{{ route('admin.package_add_ons.edit', $user->id) }}">
                                            <i class="fa fa-plus-circle"></i>
                                        </a>
                                    @endif
                                @endif

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@include('modals.admin_renewal_pay')
@include('modals.admin_renewal_pay_edit')

@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('user_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.users.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 0, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

function adminRenewModalShow(id){
    $("#adminRenewalPay").modal('show');
    userId(id);
    console.log(id);
}

function adminRenewEditShow(userid, plan){
    $("#adminRenewalEditPay").modal('show');
    userData(userid);
    planData(plan);
}
</script>
@endsection