@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        Show Devices
    </div>
    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.devices.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.user.fields.id') }}
                        </th>
                        <td>
                            {{ $device->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Key
                        </th>
                        <td>
                        {{ $device->key }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Device Name
                        </th>
                        <td>
                        {{ $device->device_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                        Device Id
                        </th>
                        <td>
                        {{ $device->device_id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                        Device start date
                        </th>
                        <td>
                        {{ $device->start_date }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                        Device end date
                        </th>
                        <td>
                        {{ $device->end_date }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                        status
                        </th>
                        <td>
                        {{ $device->status }} 
                        </td>
                    </tr>
                    <tr>
                        <th>
                        Meta Data
                        </th>
                        <td>
                        {{ $device->meta_data }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                        User Id
                        </th>
                        <td>
                        {{ $device->user_id }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.devices.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>
@endsection