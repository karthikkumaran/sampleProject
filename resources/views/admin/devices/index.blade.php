@extends('layouts.admin')
@section('content')
<div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route("admin.devices.create") }}">
                Create Device
            </a>
        </div>
    </div>
<div class="card">
    <div class="card-header">
        User List
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                <thead>
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            Key
                        </th>
                        <th>
                            Device Name
                        </th>
                        <th>
                            Device Id
                        </th>
                        <!-- <th>
                            {{ trans('cruds.user.fields.email_verified_at') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.approved') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.verified') }}
                        </th>
                        <th>
                            {{ trans('cruds.user.fields.roles') }}
                        </th> -->
                        <th>
                            Device start date
                        </th>
                        <th>
                            Device end date
                        </th>
                        <th>
                             status
                        </th>
                        <th>
                             Meta Data
                        </th>
                        <th>
                            User Id
                        </th>
                        <th>
                            &nbsp;Actions
                        </th>
                    </tr>
                </thead>
                <tbody>
                @foreach($device as $key => $device)
                        
                        <tr data-entry-id="{{ $device->id }}">
                            <td>
                                {{ $device->id ?? '' }}
                            </td>
                            <td>
                                {{ $device->key ?? '' }}
                            </td>
                            <td>
                                {{ $device->device_name ?? '' }}
                            </td>
                            <td>
                                {{ $device->device_id ?? '' }}
                            </td>
                            <td>
                                {{ $device->start_date ?? '' }}
                            </td>
                            
                            <td>
                                {{ $device->end_date ?? '' }}
                            </td>
                            
                            <td>
                                {{ $device->status ?? '' }}
                            </td>
                            <td>
                                {{ $device->meta_data}}
                            </td>    
                            <td>
                                {{ $device->user_id}}
                            </td>  
                            
                            <td>
                                    <a class="btn btn-xs btn-primary btn-icon" href="{{ route('admin.devices.show',$device->id) }}">
                                        <i class="feather icon-eye"></i>
                                    </a>

                                    <a class="btn btn-xs btn-info btn-icon" href="{{ route('admin.devices.edit', $device->id) }}">
                                        <i class="feather icon-edit"></i>
                                    </a>

                                    <form action="{{ route('admin.devices.destroy', $device->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger btn-icon"><i class="feather icon-trash"></i></button>
                                    </form>
                        
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>   

@endsection