@extends('layouts.admin')
@section('content') 
<div class="card">
    <div class="card-header">
        Edit Device
    </div>
    <div class="card-body">
        <form method="POST" action="{{ route("admin.devices.update", [$device->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="key"> Key</label>
                    <input type="text" class="form-control"  name="key" id="key" value="{{ old('key', $device->key) }}" disabled>
            </div>
            <div class="form-group">
                <label for="device_name"> Device Name</label>
                    <input type="text" class="form-control"  name="device_name" id="device_name" value="{{ old('device_name', $device->device_name) }}" disabled>
            </div>
            <div class="form-group">
                <label for="device_id"> Device Id</label>
                    <input type="text" class="form-control"  name="device_id" id="device_id" value="{{ old('device_id', $device->device_id) }}" disabled>
            </div>
            <div class="form-group">
                <label for="start_date"> Device Start Date</label>
                    <input type="datetime-local" class="form-control"  name="start_date" id="start_date" value="{{ old('start_date', $device->start_date) }}" >
            </div>
            <div class="form-group">
                <label for="end_date"> Device End Date</label>
                    <input type="datetime-local" class="form-control"  name="end_date" id="end_date" value="{{ old('end_date', $device->end_date) }}" >
            </div>
            <div class="form-group">
                <label>Status</label>
                <select class="form-control" name="status">
                    @foreach(App\Models\Device::STATUS_SELECT as $key => $label)
                    <option value="{{ $key }}" {{ old('status', $device->status) == $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="meta_data"> Meta Data</label>
                    <input type="text" class="form-control"  name="meta_data" id="meta_data" value="{{ old('status', $device->meta_data) }}" disabled>
            </div>
            <div class="form-group">
                <label for="user_id">User</label>
                <select class="form-control select2 {{ $errors->has('team') ? 'is-invalid' : '' }}" name="user_id" id="user_id">
                    @foreach($users as $id => $user)
                        <option value="{{ $id }}" {{ old('user_id', $device->user_id) == $id ? 'selected' : '' }}>{{ $user }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>
@endsection