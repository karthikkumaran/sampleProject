@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        <h3>{{ trans('cruds.sendnotification.title') }}</h3>
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.user-alerts.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="users">{{ trans('cruds.userAlert.fields.user') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('users') ? 'is-invalid' : '' }}" name="users[]" id="users" multiple>
                    @foreach($users as $id => $user)
                        <option value="{{ $id }}" {{ in_array($id, old('users', [])) ? 'selected' : '' }}>{{ $user }}</option>
                    @endforeach
                </select>
                @if($errors->has('users'))
                    <div class="invalid-feedback">
                        {{ $errors->first('users') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.userAlert.fields.user_helper') }}</span>
            </div>
            <div class="form-group">
                <p><b>{{ trans('cruds.selectnotificationchannel.title') }}</b></p>
                
                <span><input  type="checkbox" name="alert_email" id="alert_email"/>
                <label for="alert_email">{{ trans('cruds.email.title') }}</label></span>
                
                <span><input  type="checkbox" name="alert_pushnotify" id="alert_pushnotify"/>
                <label for="alert_pushnotify">{{ trans('cruds.pushnotify.title') }}</label></span>
                
                <span><input  type="checkbox" name="alert_telegram" id="alert_telegram"/>
                <label for="alert_telegram">{{ trans('cruds.telegram.title') }}</label></span>
                
            </div>
                <p><b>Push notification</b></p>
            <div class="form-group">
                <label class="required" for="alert_text">{{ trans('cruds.userAlert.fields.alert_text') }}</label>
                <input class="form-control {{ $errors->has('alert_text') ? 'is-invalid' : '' }}" type="text" name="alert_text" id="alert_text" value="{{ old('alert_text', '') }}" required>
                @if($errors->has('alert_text'))
                    <div class="invalid-feedback">
                        {{ $errors->first('alert_text') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.userAlert.fields.alert_text_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="alert_link">{{ trans('cruds.userAlert.fields.alert_link') }}</label>
                <input class="form-control {{ $errors->has('alert_link') ? 'is-invalid' : '' }}" type="text" name="alert_link" id="alert_link" value="{{ old('alert_link', '') }}">
                @if($errors->has('alert_link'))
                    <div class="invalid-feedback">
                        {{ $errors->first('alert_link') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.userAlert.fields.alert_link_helper') }}</span>
                
            </div>
            <span class="mt-1"><input type="checkbox" name="fill_to_all" id="fill_to_all"> Copy these field to all</span>
            
            <p><b>{{ trans('cruds.emailnotification.title') }}</b></p>
           
            <div class="form-group">
                <label for="email_subject">{{ trans('cruds.emailsubject.title') }}</label>
                <input class="form-control" type="input" name="email_subject" id="email_subject" value=""/>
            </div>
            <div class="form-group">
                <label for="email_message">{{ trans('cruds.emailmessage.title') }}</label>
                <textarea class="form-control hidden" rows="5" cols="50" name="email_message" id="email_message" placeholder="Enter the body of mail"></textarea>
                <div id="editor-container" style="height: 250px;"></div>
            </div>

            <p><b>{{ trans('cruds.telegramnotification.title') }}</b></p>
            <div class="form-group">
                <label for="telegram_message">{{ trans('cruds.telegrammessage.title') }}</label>
                <textarea class="form-control hidden" rows="5" cols="50" name="telegram_message" id="telegram_message" placeholder="Enter the text.."></textarea>
                <div id="editor-telegram-container" style="height: 250px;"></div>
            </div>
            <p><b>{{ trans('cruds.pushnotify.title') }}</b></p>
           
            <div class="form-group">
              <label for="title">Notification Title</label>
              <input class="form-control" type="input" name="title" id="notification_title" />
          </div>
          <div class="form-group">
              <label for="notification_message">Notification Body</label>
              <textarea class="form-control hidden" rows="5" cols="50" name="body" id="notification_message"></textarea>
              <div id="editor-notification-container" style="height: 250px;"></div>
          </div>
                
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                {{ trans('cruds.send.title') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')

<script>
    var quill = new Quill('#editor-container', {
  modules: {
    'toolbar': [
        [{
          'font': []
        }, {
          'size': []
        }],
        ['bold', 'italic', 'underline', 'strike'],
        [{
          'color': []
        }, {
          'background': []
        }],
        [{
          'script': 'super'
        }, {
          'script': 'sub'
        }],
        [{
          'header': '1'
        }, {
          'header': '2'
        }, 'blockquote', 'code-block'],
        [{
          'list': 'ordered'
        }, {
          'list': 'bullet'
        }, {
          'indent': '-1'
        }, {
          'indent': '+1'
        }],
        ['direction', {
          'align': []
        }],
        ['link', 'image', 'video', 'formula'],
        ['clean']
      ],
  },
  placeholder: 'Enter the body of mail...',
  theme: 'snow'
});

var telequill = new Quill('#editor-telegram-container', {
  modules: {
    'toolbar': [
        [{
          'font': []
        }, {
          'size': []
        }],
        ['bold', 'italic', 'underline', 'strike'],
        [{
          'color': []
        }, {
          'background': []
        }],
        [{
          'script': 'super'
        }, {
          'script': 'sub'
        }],
        [{
          'header': '1'
        }, {
          'header': '2'
        }, 'blockquote', 'code-block'],
        [{
          'list': 'ordered'
        }, {
          'list': 'bullet'
        }, {
          'indent': '-1'
        }, {
          'indent': '+1'
        }],
        ['direction', {
          'align': []
        }],
        ['link', 'image', 'video', 'formula'],
        ['clean']
      ],
  },
  placeholder: 'Enter the text...',
  theme: 'snow'
});

var notiquill = new Quill('#editor-notification-container', {
  modules: {
    'toolbar': [
        [{
          'font': []
        }, {
          'size': []
        }],
        ['bold', 'italic', 'underline', 'strike'],
        [{
          'color': []
        }, {
          'background': []
        }],
        [{
          'script': 'super'
        }, {
          'script': 'sub'
        }],
        [{
          'header': '1'
        }, {
          'header': '2'
        }, 'blockquote', 'code-block'],
        [{
          'list': 'ordered'
        }, {
          'list': 'bullet'
        }, {
          'indent': '-1'
        }, {
          'indent': '+1'
        }],
        ['direction', {
          'align': []
        }],
        ['link', 'image', 'video', 'formula'],
        ['clean']
      ],
  },
  placeholder: 'Enter the body of notification...',
  theme: 'snow'
});

quill.root.innerHTML = $("#email_message").val();
telequill.root.innerHTML = $("#telegram_message").val();
notiquill.root.innerHTML = $('#notification_message').val();
$("form").submit(function() {
        $("#email_message").val(quill.root.innerHTML);
        $("#telegram_message").val(telequill.root.innerHTML);
        $("#notification_message").val(notiquill.root.innerHTML);
        $(form).serializeArray();
    });


// telequill.root.innerHTML = $("#telegram_message").val();
// $("form").submit(function() {
//         $("#telegram_message").val(telequill.root.innerHTML);
//         $(form).serializeArray();
//     });
</script>
<script>


  $("#fill_to_all").change(function() {
    var text = $('#alert_text').val();
    var body = $('#alert_link').val();
    if(this.checked) {
      $('#email_subject').val(text);
      $('#notification_title').val(text);
      $('.ql-editor p').text(body);
    }
    else
    {
      $('#email_subject').val('');
      $('#notification_title').val('');
      $('.ql-editor p').text(''); 
    }
});
  
  
  
</script>

@endsection