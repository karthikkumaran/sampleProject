@extends('layouts.admin')
@section('content')
<div class="row mt-1">
    <div class="col-md-12 d-flex justify-content-end">
        <div class="btn-group mt-1 mb-1 mr-1">
            <a href="#" onclick="createNewCatalog()" 
                class="btn btn-primary btn-icon"><i class="feather icon-plus"></i> Add Catalog</a>
        </div>
    </div>
</div>
<h2 class="text-center text-muted"><b>Catalogs</b></h2>
<hr class="m-0">
<div class="row mt-2">
    @php
        $published_catalogs = 0;
    @endphp
    @foreach ($campaignLists as $campaign)
        @php
        $data=[];
            if(empty($userSettings->subdomain))
                $link = route('campaigns.published',$campaign->public_link);
            else
                $link = $userSettings->subdomain.".".env('SITE_URL')."/".$campaign->public_link;
             
            $entitycards_all = $campaign->campaignEntitycards;
            $entitycards = array();
            foreach($entitycards_all as $entitycard){
                if( !($entitycard->video_id != NULL && $entitycard->product_id != NULL) ){
                    array_push($entitycards, $entitycard);
                }
            }
            $n = count($entitycards);
            $img = asset('XR/assets/images/placeholder.png');
            if($campaign->is_start == 1){
                $published_catalogs++;
            }
        @endphp
    <div class="col-xl-4 col-md-6 col-sm-12 profile-card-2" id="catalog_{{$campaign->id}}">
        <div class="card h5">
            <div class="card-header p-0">
                <div class="row m-0 w-100">
                    <div class="col-sm-12" style="padding: 10px 10px 0px 10px;">
                        <div class="d-flex justify-content-between">
                            <div class="text-truncate" style="width:80%">
                                <b>{{$campaign->name}}</b>


                            </div>
                            <div class="float-right">
                                <a href="javascript:void(0);" id="catalog_settings_{{$campaign->id}}" onclick="catalog_settings($(this))" data-id="{{$campaign->id}}" data-is_start="{{$campaign->is_start}}" data-is_private="{{$campaign->is_private}}" data-private_password="{{$campaign->private_password}}" data-is_expired="{{empty($campaign->public_shareable->expires_at)?'false':'checked'}}" data-expired_date="{{ empty($campaign->public_shareable->expires_at)?'':date('Y-m-d\TH:i', strtotime($campaign->public_shareable->expires_at)) }}" data-is_added_home="{{$campaign->is_added_home}}" class="text-dark" data-toggle="modal" data-target="CatalogSettingsModal" data-catalogs_publishable="" data-catalog_downgradeable="{{$campaign->downgradeable == 1 ? 1:0 }}">
                                    <i class="feather icon-settings"></i>
                                </a>

                                <a href="javascript:void(0);" onclick="deleteCatalog({{$campaign->id}})" class="text-danger ml-1"><i class="fa fa-trash"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content" style="padding: 0px 5px 5px 5px;">
            <div class="row border-0" style=" padding:10px;cursor:pointer;height:250px;" onclick="location.href='{{route('admin.campaigns.edit',$campaign->id)}}'">
                @if(count($entitycards)==0 )
                        <div class=" col-12 rounded h-100 my-auto text-disabled">
                            <h2 class="text-center mt-3 text-muted">No Products Added Yet</h2>
                        </div>
                    @elseif(count($entitycards)==1 )
                        <div class=" col-12 rounded h-100 d-flex justify-content-center">
                            @php
                            $i = 0;
                            if(!empty($entitycards[$i]->product) && count($entitycards[$i]->product->photo) > 0){
                            $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                            }
                            elseif(!empty($entitycards[$i]->video) && !empty($entitycards[$i]->video->thumbnail)){
                            $img = $entitycards[$i]->video->thumbnail->getUrl('thumb');    
                            }
                            else{
                            $img = asset('XR/assets/images/placeholder.png');
                            }
                            @endphp
                            <img class="img-responsive mw-100 h-100 lazyload rounded" data-src="{{$img}}">
                        </div>
                    @elseif( ( count($entitycards)==2) ) 
                        <div class="col-12 h-50 bg- d-inline-flex">
                            <div class="w-50 h-100 d-flex justify-content-center" style="padding:5px;">
                                @php
                                $i = 0;
                                if(!empty($entitycards[$i]->product) && count($entitycards[$i]->product->photo) > 0){
                                $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                                }
                                elseif(!empty($entitycards[$i]->video) && !empty($entitycards[$i]->video->thumbnail)){
                                $img = $entitycards[$i]->video->thumbnail->getUrl('thumb');    
                                }
                                else{
                                $img = asset('XR/assets/images/placeholder.png');
                                }
                                @endphp
                                <img class="img-responsive w-100 lazyload rounded" data-src="{{$img}}">
                            </div>
                            <div class="w-50 h-100" style="padding:5px;"></div>
                        </div>
                        <div class="col-12 h-50 d-inline-flex">
                            <div class="w-50 h-100" style="padding:5px;"></div>
                            <div class="w-50 h-100 d-flex justify-content-center" style="padding:5px;">
                                @php
                                $i = 1;
                                if(!empty($entitycards[$i]->product) && count($entitycards[$i]->product->photo) > 0){
                                $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                                }
                                elseif(!empty($entitycards[$i]->video) && !empty($entitycards[$i]->video->thumbnail)){
                                $img = $entitycards[$i]->video->thumbnail->getUrl('thumb');    
                                }
                                else{
                                $img = asset('XR/assets/images/placeholder.png');
                                }
                                @endphp
                                <img class="img-responsive h-100 lazyload rounded" data-src="{{$img}}">
                            </div>
                        </div>
                    @elseif(count($entitycards)==3)
                        <div class="col-12 h-50 d-flex justify-content-center border-bottom" style="padding:5px;">
                            @php
                            $i = 0;
                            if(!empty($entitycards[$i]->product) && count($entitycards[$i]->product->photo) > 0){
                            $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                            }
                            elseif(!empty($entitycards[$i]->video) && !empty($entitycards[$i]->video->thumbnail)){
                            $img = $entitycards[$i]->video->thumbnail->getUrl('thumb');    
                            }
                            else{
                            $img = asset('XR/assets/images/placeholder.png');
                            }
                            @endphp
                            <img class="img-responsive h-100 lazyload rounded" data-src="{{$img}}">
                        </div>
                        <div class="col-12 h-50 d-inline-flex border-top">
                            <div class="w-50 h-100 d-flex justify-content-center border-right" style="padding:5px;">
                                @php
                                $i = 1;
                                if(!empty($entitycards[$i]->product) && count($entitycards[$i]->product->photo) > 0){
                                $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                                }
                                elseif(!empty($entitycards[$i]->video) && !empty($entitycards[$i]->video->thumbnail)){
                                $img = $entitycards[$i]->video->thumbnail->getUrl('thumb');    
                                }
                                else{
                                $img = asset('XR/assets/images/placeholder.png');
                                }
                                @endphp
                                <img class="img-responsive w-100 h-100 lazyload rounded" style="z-index:10;" data-src="{{$img}}">
                            </div>
                            <div class="w-50 h-100 d-flex justify-content-center border-left" style="padding:5px;">
                                @php
                                $i = 2;
                                if(!empty($entitycards[$i]->product) && count($entitycards[$i]->product->photo) > 0){
                                $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                                }
                                elseif(!empty($entitycards[$i]->video) && !empty($entitycards[$i]->video->thumbnail)){
                                $img = $entitycards[$i]->video->thumbnail->getUrl('thumb');    
                                }
                                else{
                                $img = asset('XR/assets/images/placeholder.png');
                                }
                                @endphp
                                <img class="img-responsive w-100 h-100 lazyload rounded" data-src="{{$img}}">
                            </div>
                        </div>
                    @elseif(count($entitycards)>3)
                        <div class="col-12 h-50 bg- d-inline-flex">
                            <div class="w-50 h-100 border-bottom border-right" style="padding:5px;">
                                @php
                                $i = 0;
                                if(!empty($entitycards[$i]->product) && count($entitycards[$i]->product->photo) > 0){
                                $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                                }
                                elseif(!empty($entitycards[$i]->video) && !empty($entitycards[$i]->video->thumbnail)){
                                $img = $entitycards[$i]->video->thumbnail->getUrl('thumb');    
                                }
                                else{
                                $img = asset('XR/assets/images/placeholder.png');
                                }
                                @endphp
                                <img class="img-responsive w-100 h-100 lazyload rounded" data-src="{{$img}}">
                            </div>
                            <div class="w-50 h-100 border-bottom border-left" style="padding:5px;">
                                @php
                                $i = 1;
                                if(!empty($entitycards[$i]->product) && count($entitycards[$i]->product->photo) > 0){
                                $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                                }
                                elseif(!empty($entitycards[$i]->video) && !empty($entitycards[$i]->video->thumbnail)){
                                $img = $entitycards[$i]->video->thumbnail->getUrl('thumb');    
                                }
                                else{
                                $img = asset('XR/assets/images/placeholder.png');
                                }
                                @endphp
                                <img class="img-responsive w-100 h-100 lazyload rounded" data-src="{{$img}}">
                            </div>
                        </div>
                        <div class="col-12 h-50 d-inline-flex">
                            <div class="w-50 h-100 border-top border-right" style="padding:5px;">
                                @php
                                $i = 2;
                                if(!empty($entitycards[$i]->product) && count($entitycards[$i]->product->photo) > 0){
                                $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                                }
                                elseif(!empty($entitycards[$i]->video) && !empty($entitycards[$i]->video->thumbnail)){
                                $img = $entitycards[$i]->video->thumbnail->getUrl('thumb');    
                                }
                                else{
                                $img = asset('XR/assets/images/placeholder.png');
                                }
                                @endphp
                                <img class="img-responsive w-100 h-100 lazyload rounded" data-src="{{$img}}">
                            </div>
                            <div class="w-50 h-100 border-top border-left" style="padding:5px;">
                                @php
                                $i = 3;
                                if(!empty($entitycards[$i]->product) && count($entitycards[$i]->product->photo) > 0){
                                $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                                }
                                elseif(!empty($entitycards[$i]->video) && !empty($entitycards[$i]->video->thumbnail)){
                                $img = $entitycards[$i]->video->thumbnail->getUrl('thumb');    
                                }
                                else{
                                $img = asset('XR/assets/images/placeholder.png');
                                }
                                @endphp
                                <img class="img-responsive w-100 h-100 lazyload rounded" data-src="{{$img}}">
                            </div>
                        </div>
                    @endif

            </div>

            <div class="row m-0 w-100">
                <div class="col-sm-12" style="padding:0px 5px 5px 5px;">
                    <div class="d-flex justify-content-between">
                        <div>
                            <span class="h2 m-0">{{count($entitycards)}} <i class="feather icon-box"></i></span>
                            <span class="h2 ml-1  m-0">{{$campaign->views}} <i class="feather icon-eye"></i></span>

                        </div>
                        <div>
                            <span class="h2 m-0 text-danger" id="lock_icon_{{$campaign->id}}" {{$campaign->is_private == 1 ? '':'hidden'}}><i class="fa fa-lock" aria-hidden="true"></i></span>
                            <span class="h2 m-0 ml-1 text-warning" id="clock_icon_{{$campaign->id}}" {{$campaign->public_shareable->expires_at ? '':'hidden'}}><i class="fa fa-clock-o" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{date('d-m-Y h:i:s A', strtotime($campaign->public_shareable->expires_at))}}"></i></span>
                        </div>
                        <div class="float-right h2 m-0">
                            <div id="catalog_share_{{$campaign->id}}" {{$campaign->is_start == 1 ? '':'hidden'}}>
                                @if(session('subscribed_features'))
                                    @if(array_key_exists("stories", session('subscribed_features')[0]))
                                        <a href="javascript:void(0);" class="text-dark sharebtn mr-1" data-link="{{$campaign->public_link}}" data-qrcode="{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'stories','',true)}}" data-title="{{$campaign->name}}  Stories " data-sharelink="{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'stories')}}" data-pdf="false" data-download="false">
                                            <img class="mt-0" src="{{asset('/XR/app-assets/images/icons/stories.svg')}}" style="height: 24px;width: 21px;">
                                        </a>
                                    @endif                                        
                                @endif
                                <a href="javascript:void(0);" class="text-dark sharebtn" data-link="{{$campaign->public_link}}" data-qrcode="{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'catalog','',true)}}" data-title="{{$campaign->name}}" data-sharelink="{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'catalog')}}" data-download="false" data-entity="catalog">
                                    <i class="fa fa-share-alt"></i>
                                </a>
                                <a href="javascript:void(0);" onclick="WhatsAppShare('{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'catalog')}}','')" class="text-success ml-1"><i class="fa fa-whatsapp"></i></a>
                            </div>
                            <div id="catalog_noshare_{{$campaign->id}}" {{$campaign->is_start == 1 ? 'hidden':''}}>
                                <a href="javascript:void(0);" class="text-light">
                                    <i class="fa fa-share-alt"></i>
                                </a>
                                <a href="javascript:void(0);" class="text-light ml-1"><i class="fa fa-whatsapp"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@endforeach

@include('modals.published1')
@include('modals.campaign_settings')

</div>
@if(count($campaignLists) == 0)
<div class="d-flex justify-content-center">
    <img src="{{asset('/XR/app-assets/images/icons/AddCatalog.png')}}" class="img-fluid" alt="">
</div>
<h3 class="text-danger text-center p-2">No Catalog added yet.</h3>
<div style="border-style:dashed;cursor:pointer;" data-toggle="modal" data-target="#createNewCatalog" class="rounded mb-0 ml-1 text-center p-2 text-dark">
    <div class="image">
        <i class="feather icon-plus-circle font-large-1"></i>
    </div>
    <span style="display: block;padding:2px;">Create Catalog</span>
</div>
@endif
{{ $campaignLists->links() }}

@endsection
@section('scripts')
@parent
<script>
    var published_catalogs = "{{$published_catalogs}}";
    var allowed_catalogs = "{{$allowed_catalogs}}";
    var catalogs_publishable = "true";
    if(published_catalogs >= allowed_catalogs){
        catalogs_publishable = "false";
    }
    var set = $('[id^="catalog_settings_"]');
    set.data('catalogs_publishable',catalogs_publishable);
    
    function WhatsAppShare(url, msg) {
        var t = "I suggest you to visit our website to view our product catalogs and place your order via this link.";
        var hashtags = "{{ isset($meta_data['storefront']['storename'])?$meta_data['storefront']['storename'] : ''}}";
        var message = encodeURIComponent(url) + " " + t + " #" + hashtags;
        // var whatsapp_url = "whatsapp://send?text=" + message;
        // window.location.href = whatsapp_url;
        var whatsapp_url = "https://wa.me/?text=" + message;
        openInNewTab(whatsapp_url);
    }

    function deleteCatalog(id) {
        var url = "{{route('admin.campaigns.destroy',':id')}}";
        url = url.replace(':id', id);
        swal.fire({
            title: 'Are you sure to remove this Catalog?',
            text: 'This is an irrevocable action and you will loose all the data regarding this catalog.',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, Remove it.',
            cancelButtonText: 'No, Cancel',
            confirmButtonClass: 'btn btn-danger',
            cancelButtonClass: 'btn btn-light',
        }).then(function(result) {
            if (result.value) {
                $('#loading-bg').show();
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                    success: function(response_sub) {
                        $('#loading-bg').show();
                        Swal.fire({
                            type: "success",
                            title: 'Deleted!',
                            text: 'Catalog has been deleted.',
                            confirmButtonClass: 'btn btn-success',
                        }).then((result) => {
                            window.location.reload();
                            $('#catalog_' + id).remove();
                            $('#loading-bg').hide();
                        });

                    }
                });


            }
        })

    }
</script>
@endsection