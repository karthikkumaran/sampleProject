@extends('layouts.admin')
@section('content')
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/plugins/forms/wizard.css')}}">
 <!-- Form wizard with step validation section start -->
 <section id="validation">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">{{ trans('global.create') }} {{ trans('cruds.campaign.title_singular') }}</h4>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <form action="#" class="steps-validation wizard-circle">
                                            <!-- Step 1 -->
                                            <h6> Step 1</h6>
                                            <fieldset>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="firstName3">
                                                            {{ trans('cruds.campaign.title_singular') }} {{ trans('cruds.campaign.fields.name') }}
                                                            </label>
                                                            <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }} required" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                                                            <input type="hidden" name="is_start" id="is_start" value="0" required>
                                                            <input type="hidden" name="is_active" id="is_active" value="1" required>
                                                            <input type="hidden" name="type" id="type" value="1" required>
                                                        </div>
                                                    </div>
                                                </div>

                                               
                                            </fieldset>

                                            <!-- Step 2 -->
                                            <h6> Step 2</h6>
                                            <fieldset>
                                            <button type="button" class="btn btn-danger mb-4" data-toggle="modal" data-target="#productSelectModal">
                                           Add Product
                                        </button>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- Form wizard with step validation section end -->

@include('modals.product')


<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.campaign.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.campaigns.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.campaign.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.campaign.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.campaign.fields.is_start') }}</label>
                <select class="form-control {{ $errors->has('is_start') ? 'is-invalid' : '' }}" name="is_start" id="is_start" required>
                    <option value disabled {{ old('is_start', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Campaign::IS_START_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('is_start', '0') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('is_start'))
                    <div class="invalid-feedback">
                        {{ $errors->first('is_start') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.campaign.fields.is_start_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.campaign.fields.type') }}</label>
                <select class="form-control {{ $errors->has('type') ? 'is-invalid' : '' }}" name="type" id="type" required>
                    <option value disabled {{ old('type', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Campaign::TYPE_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('type', '') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('type'))
                    <div class="invalid-feedback">
                        {{ $errors->first('type') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.campaign.fields.type_helper') }}</span>
            </div>
            <div class="form-group">
                <label>{{ trans('cruds.campaign.fields.is_active') }}</label>
                <select class="form-control {{ $errors->has('is_active') ? 'is-invalid' : '' }}" name="is_active" id="is_active">
                    <option value disabled {{ old('is_active', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Campaign::IS_ACTIVE_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('is_active', '1') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('is_active'))
                    <div class="invalid-feedback">
                        {{ $errors->first('is_active') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.campaign.fields.is_active_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="preview_link">{{ trans('cruds.campaign.fields.preview_link') }}</label>
                <input class="form-control {{ $errors->has('preview_link') ? 'is-invalid' : '' }}" type="text" name="preview_link" id="preview_link" value="{{ old('preview_link', '') }}">
                @if($errors->has('preview_link'))
                    <div class="invalid-feedback">
                        {{ $errors->first('preview_link') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.campaign.fields.preview_link_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="public_link">{{ trans('cruds.campaign.fields.public_link') }}</label>
                <input class="form-control {{ $errors->has('public_link') ? 'is-invalid' : '' }}" type="text" name="public_link" id="public_link" value="{{ old('public_link', '') }}">
                @if($errors->has('public_link'))
                    <div class="invalid-feedback">
                        {{ $errors->first('public_link') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.campaign.fields.public_link_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection
@section('scripts')
@parent
 <!-- BEGIN: Page Vendor JS-->
 <script src="{{asset('XR/app-assets/vendors/js/extensions/jquery.steps.min.js')}}"></script>
    <script src="{{asset('XR/app-assets/vendors/js/forms/validation/jquery.validate.min.js')}}"></script>
    <!-- END: Page Vendor JS-->
      <!-- BEGIN: Page JS-->
      <script src="{{asset('XR/app-assets/js/scripts/forms/wizard-steps.js')}}"></script>
    <!-- END: Page JS-->
    
@endsection