@extends('layouts.store_onboard')
@section('styles')
<style>
    .arrow_box {
        position: relative;
        background: #ffffff;
        border: 1px solid #0275d8;
    }

    .arrow_box:after,
    .arrow_box:before {
        top: 100%;
        left: 50%;
        border: solid transparent;
        content: "";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
    }

    .arrow_box:after {
        border-color: rgba(255, 255, 255, 0);
        border-top-color: #ffffff;
        border-width: 12px;
        margin-left: -12px;
    }

    .arrow_box:before {
        border-color: rgba(2, 117, 216, 0);
        border-top-color: #4824CF;
        border-width: 13px;
        margin-left: -13px;
    }
</style>
@endsection
@section('content')
    {{--@if($trial_plan)
        @php 
            if($trial_plan->trial_period != 0){
                $trial_period = $trial_plan->trial_period;
            }
            else{
                $trial_period = 1;
            }
            $trial_duration = $trial_plan->invoice_period + $trial_period;
        @endphp
        @if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
        <div class="content-body ecommerce-application">
            <div class="card">
                <div class="card-content">
                    <div class="card-body">
                    
                            <h4>You are currently subscribed to the Free Trial Plan</h4><br>
                            <div class="row">
                                <div class="col-lg-6">
                                    <h5><b>Plan Details</b></h5>
                                    <div class="invoice-date-wrapper">
                                        <p class="invoice-date-title"><b>Plan name:</b> {{$trial_plan->name}}</p>
                                    </div>
                                    <div class="invoice-date-wrapper">
                                        <p class="invoice-date-title"><b>Plan duration :</b> {{$trial_duration}} days</p>
                                    </div>
                                    
                                </div>
                                <div class="col-lg-6">
                                    <h5><b>Plan Features</b></h5>
                                    @php
                                        $features = $trial_plan->features;
                                    @endphp
                                    @foreach($features as $key => $feature)
                                        @php
                                            $feature_details = $feature->getFeatureDetails();
                                        @endphp
                                        <div class="invoice-date-wrapper">
                                            <p class="invoice-date-title"><b>{!! $feature_details->name !!}</b> - {!! $feature->value !!}</p>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif--}}
    @if(session('error'))
        <div class="row mb-2">
            <div class="col-lg-12">
                <div class="alert alert-danger" role="alert">{{ session('error') }}</div>
            </div>
        </div>
    @endif

    <div class="content-body ecommerce-application">
        <div class="card">
            <!-- <div class="card-content"> -->
                <div class="card-body">
                    <form method="POST" action="{{ route("admin.userSettings.set_domain") }}" enctype="multipart/form-data">
                    <div class="row">
                            <div class="col-md-12 d-flex justify-content-center">
                    <h5 class="text-center text-primary" >Your account has been successfully created. You are just one step away from creating your e-commerce store. Please fill in the details below.</h5>
                            </div>
                    </div>
                    @csrf
                        <div class="card-header p-0 card-title " style="margin:auto 5px;">
                        
                            <h3 class="w-100 text-center"><b>Store Onboarding</b></h3>
                        </div>
                        <hr class="m-0">
                        <h6 class="text-center mt-sm-1">Please set your store setings.</h6>
                        <hr class="m-0">

                        <div id="settings_store_main">
                            <div class="form-group">
                                <div class="controls">
                                    <label>Store Name</label>
                                    <input type="text" class="form-control" value="" name="storename" placeholder="Store Name here..." maxlength="35" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="controls">
                                    <label>Subdomain</label>
                                    <fieldset>
                                        <div class="input-group">
                                            <input type="text" class="form-control" style="text-transform:lowercase" placeholder="subdomain" 
                                            aria-describedby="basic-addon2" name="subdomain" class="subdomain" id="set_subdomain" onkeypress="return issuddomain(event,this)"
                                            value="" required>
                                            <div class="input-group-append">
                                                <span class="input-group-text" id="basic-addon2">.{{env('SITE_URL')}}</span>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="controls">
                                    <label>Email ID</label>
                                    <input type="email" class="form-control" value="{{ $user->email ?? '' }}" name="emailid" placeholder="Email ID here..." required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="controls">
                                    <label>Email From Name</label>
                                    <input type="text" class="form-control" value="" name="emailfromname" placeholder="Email From Name here..." required>
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-6 controls">
                                    <label for="contactnumber_country_code">Country code</label>
                                    <select id="contactnumber_country_code" class="form-control" name="contactnumber_country_code" required>
                                        @foreach(App\Customer::MOBILE_COUNTRY_CODE as $key=>$value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6 controls">
                                    <label>Contact Number</label>
                                    <input type="tel" class="form-control @error('contactnumber') is-invalid @enderror" value="{{ $user->mobile ?? '' }}" 
                                    name="contactnumber" placeholder="Contact Number here..."
                                     onkeypress="return isNumberKey(event,this)" >                                       
                                       @error('contactnumber')
                                       <div class="alert alert-danger">{{ $message }}</div>
                                   @enderror
                                   
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-6 controls">
                                    <label for="whatsapp_country_code">Country code</label>
                                    <select id="whatsapp_country_code" class="form-control" name="whatsapp_country_code" required>
                                        @foreach(App\Customer::MOBILE_COUNTRY_CODE as $key=>$value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6 controls">
                                    <label>Whatsapp Number</label>
                                    <input type="tel" class="form-control @error('whatsapp') is-invalid @enderror" value="" name="whatsapp" 
                                    placeholder="Enter your whatsapp number here with country code..." 
                                    onkeypress="return isNumberKey(event,this)" required>
                                    @error('whatsapp')
                                       <div class="alert alert-danger">{{ $message }}</div>
                                   @enderror
                                </div>
                            </div>

                            <center> <button type="submit" class="btn btn-danger">Tap to set</button></center>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
<script>
    function issuddomain(event) {
        var key = event.which;
        return ((key >= 48 && key <= 57) || (key == 45 ) ||(key >= 65 && key <= 90) || (key >= 97 && key <= 122) );
    }
    function isNumberKey(event) {
        // Allow only backspace and delete
        if (event.keyCode == 8 ) {
            // let it happen, don't do anything
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.keyCode < 48 || event.keyCode > 57 ) {
                event.preventDefault(); 
            }   
        }
    }
</script>

