@extends('layouts.admin')
@section('content') 
<div class="row mt-2">
    @foreach ($campaignLists as $campaign)
                @php
        if(empty($userSettings->subdomain))
            $link = route('campaigns.published',$campaign->public_link);
        else 
            $link = $userSettings->subdomain.".".env('SITE_URL')."/".$campaign->public_link;
        @endphp
                
    <div class="col-xl-4 col-md-6 col-sm-12 profile-card-2">
                            <div class="card">
                                <div class="card-header mx-auto pb-0">
                                    <div class="row m-0">
                                        <div class="col-sm-12 text-center">
                                            <h4>{{$campaign->name}}</h4>
                                        </div>
                                        <div class="col-sm-12 text-center">
                                            <p class="">{{App\Campaign::TYPE_SELECT[$campaign->type]}}</p>
                                        </div>
                                        <!-- <div class="heading-elements">
                                        <ul class="mb-0">
                                            <div class="dropdown">
                                            <i class="feather icon-more-vertical" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="#">Edit</a>
                                                    <a class="dropdown-item" href="#">Delete</a>
                                                </div>
                                        </div>
                                        </ul>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-body text-center mx-auto">
                                        <!-- <div class="avatar avatar-xl">
                                            <a href="{{route('admin.campaigns.preview',$campaign->preview_link)}}" target="__blank">
                                            <img class="img-fluid" src="{{route('admin.campaigns.previewQrCode',$campaign->preview_link)}}" alt="img placeholder">
                                            </a>
                                        </div>
                                        <br>view -->
                                        <div class="d-flex justify-content-between mt-2">
                                            <div class="">
                                                <p class="font-weight-bold font-medium-2 mb-0 {{ $campaign->is_active == 1?'text-success':'text-danger' }}">
                                                {{ $campaign->is_active == 1?'Active':'In-Active' }}</p>
                                                <!-- <span class="">Status</span> -->
                                            </div>
                                            <div class="">
                                                <p class="font-weight-bold font-medium-2 mb-0 {{ $campaign->is_start == '1'?'text-success':'text-danger' }}">
                                                {{ $campaign->is_start == '1'?'Published':'UnPublished' }}</p>
                                                <!-- <span class="">isPublished</span> -->
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-between mt-2">
                                            <div class="">
                                                <!-- <p class="font-weight-bold font-medium-2 mb-0">568</p>
                                                <span class="">Views</span> -->
                                                <p class="font-weight-bold font-medium-2 mb-0">{{ date('d-m-Y', strtotime($campaign->created_at)) }}</p>
                                                <span class="">Created Date</span>
                                            </div>
                                            <div class="">
                                                <!-- <p class="font-weight-bold font-medium-2 mb-0">{{ date('d-m-Y', strtotime($campaign->created_at)) }}</p>
                                                <span class="">Expried Date</span> -->
                                            </div>
                                            <div class="">
                                                @if($campaign->is_start == 1)
                                                <button type="button" class="btn btn-primary btn-icon sharebtn" data-link="{{$campaign->public_link}}" data-qrcode="{{route('admin.campaigns.previewQrCode',$campaign->public_link)}}" data-title="{{$campaign->name}}" data-sharelink="{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'catalog')}}">
                                                    <i class="feather icon-share-2"></i>
                                                </button>
                                                @endif
                                            </div>
                                        </div>
                                        
                                        <a href="{{route('admin.campaigns.edit',$campaign->id)}}" class="btn gradient-light-primary btn-block mt-2" >Open</a>
                                    </div>
                                </div>
                            </div>
                        </div>
    

    @endforeach
    @include('modals.published1')
    
</div>
@if(count($campaignLists) == 0)
    <h3 class="text-light text-center p-2">No Campaigns added yet, please add</h3>
    @endif
{{ $campaignLists->links() }}

@endsection
@section('scripts')
@parent
@endsection