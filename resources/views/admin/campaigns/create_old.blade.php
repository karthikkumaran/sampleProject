@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.campaign.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.campaigns.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="name">{{ trans('cruds.campaign.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', '') }}" required>
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.campaign.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.campaign.fields.is_start') }}</label>
                <select class="form-control {{ $errors->has('is_start') ? 'is-invalid' : '' }}" name="is_start" id="is_start" required>
                    <option value disabled {{ old('is_start', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Campaign::IS_START_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('is_start', '0') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('is_start'))
                    <div class="invalid-feedback">
                        {{ $errors->first('is_start') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.campaign.fields.is_start_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.campaign.fields.type') }}</label>
                <select class="form-control {{ $errors->has('type') ? 'is-invalid' : '' }}" name="type" id="type" required>
                    <option value disabled {{ old('type', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Campaign::TYPE_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('type', '') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('type'))
                    <div class="invalid-feedback">
                        {{ $errors->first('type') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.campaign.fields.type_helper') }}</span>
            </div>
            <div class="form-group">
                <label>{{ trans('cruds.campaign.fields.is_active') }}</label>
                <select class="form-control {{ $errors->has('is_active') ? 'is-invalid' : '' }}" name="is_active" id="is_active">
                    <option value disabled {{ old('is_active', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Campaign::IS_ACTIVE_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('is_active', '1') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('is_active'))
                    <div class="invalid-feedback">
                        {{ $errors->first('is_active') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.campaign.fields.is_active_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="preview_link">{{ trans('cruds.campaign.fields.preview_link') }}</label>
                <input class="form-control {{ $errors->has('preview_link') ? 'is-invalid' : '' }}" type="text" name="preview_link" id="preview_link" value="{{ old('preview_link', '') }}">
                @if($errors->has('preview_link'))
                    <div class="invalid-feedback">
                        {{ $errors->first('preview_link') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.campaign.fields.preview_link_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="public_link">{{ trans('cruds.campaign.fields.public_link') }}</label>
                <input class="form-control {{ $errors->has('public_link') ? 'is-invalid' : '' }}" type="text" name="public_link" id="public_link" value="{{ old('public_link', '') }}">
                @if($errors->has('public_link'))
                    <div class="invalid-feedback">
                        {{ $errors->first('public_link') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.campaign.fields.public_link_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection