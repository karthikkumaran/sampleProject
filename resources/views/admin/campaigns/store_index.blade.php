@extends('layouts.admin')
@section('content')
@section('styles')
<style>
    .arrow_box {
        position: relative;
        background: #ffffff;
        border: 1px solid #0275d8;
    }

    .arrow_box:after,
    .arrow_box:before {
        top: 100%;
        left: 50%;
        border: solid transparent;
        content: "";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
    }

    .arrow_box:after {
        border-color: rgba(255, 255, 255, 0);
        border-top-color: #ffffff;
        border-width: 12px;
        margin-left: -12px;
    }

    .arrow_box:before {
        border-color: rgba(2, 117, 216, 0);
        border-top-color: #4824CF;
        border-width: 13px;
        margin-left: -13px;
    }
</style>
@endsection

@php
$user = auth()->user();
setCookie("admin", $user->name);
if(empty($user_meta_data['settings']['currency']))
$currency = "₹";
else
if($user_meta_data['settings']['currency_selection'] == "currency_symbol")
$currency = App\User::CURRENCY_SYMBOLS[$user_meta_data['settings']['currency']];
else
$currency = $user_meta_data['settings']['currency']
@endphp

@if(count($campaignLists) == 0 && $data['catalogs_count'] == 0)
<div class="d-flex justify-content-center">
    <img src="{{asset('/XR/app-assets/images/icons/AddCatalog.png')}}" class="img-fluid" alt="">
</div>
<h3 class="text-danger text-center p-2">No Catalog Added to Store Home Screen</h3>
<div style="border-style:dashed;cursor:pointer;" data-toggle="modal" data-target="#createNewCatalog" class="rounded mb-0 ml-1 text-center p-2 text-dark">
    <div class="image">
        <i class="feather icon-plus-circle font-large-1"></i>
    </div>
    <span style="display: block;padding:2px;">Create Catalog</span>
</div>
<hr>
@elseif(count($campaignLists) == 0 && $data['catalogs_count'] > 0)
<div class="d-flex justify-content-center">
    <img src="{{asset('/XR/app-assets/images/icons/AddCatalog.png')}}" class="img-fluid" alt="">
</div>
<h3 class="text-danger text-center p-2">No Catalog Added to Store Home Screen</h3>
<div style="border-style:dashed;cursor:pointer;" onclick="location.href='{{route('admin.campaigns.index')}}'" class="rounded mb-0 ml-1 text-center p-2 text-dark">
    <div class="image">
        <i class="feather icon-plus-circle font-large-1"></i>
    </div>
    <span style="display: block;padding:2px;">Add Catalog</span>
</div>
<hr>
@endif

@if($userSettings->is_start == 1 && count($campaignLists) > 0)
<!-- analytics -->
<div id="store_analytics">
    <h2 class="text-center text-muted"><b>Store Overview</b></h2>
    <hr>
    <!-- buttons -->
    <div class="mx-3 mb-4">
    <select class="m-2 col col-md-2 float-right  form-control filter" id="filter">
        <option value="today">Today</option>
        <option value="yesterday">Yesterday</option>
        <option value="last_7_days" selected>Last 7 days</option>
        <option value="last_30_days">Last 30 days</option>
        <option value="thismonth">This Month</option>
        <option value="monthly">Monthly</option>
        <option value="yearly">Yearly</option>
    </select>
    </div>

    <div class="row w-100 d-flex justify-content-center m-0">
        <div class="col-md-3  col-sm-12">
            <div class="card cursor-pointer  stat-card orders" id="orders-card" onclick="showstats('orders')">
                <div class="card-header d-flex align-items-start pb-1">
                    <div>
                        <h3 class="text-bold-700 mb-0  orders" id="orders-stat-value"></h3>
                        <p>Orders &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                    </div>
                    <div class="avatar bg-rgba-success p-50 m-lg-0 mx-md-auto m-0">
                        <div class="avatar-content">
                            <i class="feather icon-list text-success font-medium-5 orders-icon"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3   col-sm-12">
            <div class="card cursor-pointer  stat-card sales" id="sales-card" onclick="showstats('sales')">
                <div class="card-header d-flex align-items-start pb-1">
                    <div>
                        <h3 class="text-bold-700 mb-0 sales " id="sales-stat-value"></h3>
                        <p>Sales &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
                    </div>
                    <div class="avatar bg-rgba-success p-50 mx-md-auto m-lg-0 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-shopping-cart text-success font-medium-5 sales-icon"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3  col-sm-12">
            <div class="card cursor-pointer  stat-card catalog-views" id="catalog-views-card" onclick="showstats('catalog-views')">
                <div class="card-header d-flex align-items-start pb-1">
                    <div>
                        <h3 class="text-bold-700 mb-0  catalog-views" id="catalog-views-stat-value"></h3>
                        <p>Catalog Views</p>
                    </div>
                    <div class="avatar bg-rgba-success p-50 mx-md-auto m-lg-0 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-eye text-success font-medium-5  catalog-views-icon"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-md-3 col-sm-12">
            <div class="card cursor-pointer stat-card product-views" id="product-views-card" onclick="showstats('product-views')">
                <div class="card-header d-flex align-items-start pb-1">
                    <div>
                        <h3 class="text-bold-700 mb-0  product-views" id="product-views-stat-value"></h3>
                        <p>Product Views</p>
                    </div>
                    <div class="avatar bg-rgba-success p-50 mx-md-auto m-lg-0 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-eye text-success font-medium-5  product-views-icon "></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row w-100 d-flex justify-content-center m-0">
    @if(session('subscribed_features'))
            @if(array_key_exists("Video catalog", session('subscribed_features')[0]))
            <div class="col-md-3  col-sm-12">
                <div class="card cursor-pointer  stat-card videocatalog-views" id="videocatalog-views-card" onclick="showstats('videocatalog-views')">
                    <div class="card-header d-flex align-items-start pb-1">
                        <div>
                            <h3 class="text-bold-700 mb-0  catalog-views" id="videocatalog-views-stat-value"></h3>
                            <p>Video Catalog Views</p>
                        </div>
                        <div class="avatar bg-rgba-success p-50 mx-md-auto m-lg-0 m-0">
                            <div class="avatar-content">
                                <i class="feather icon-eye text-success font-medium-5  videocatalog-views-icon"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        @endif
    </div>    
        <!-- chart -->
    <div class="row w-100 d-flex justify-content-center m-0">

        <div class="col-md-12 col-12 d-flex justify-content-between flex-column text-right card">
            <!-- <div class="dropdown chart-dropdown filter-menu">
                <button class="btn btn-sm border-0 dropdown-toggle mt-1 filter-btn " type="button" id="dropdownItem5" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Last 7 Days
                </button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownItem5">
                    <option class="dropdown-item filter-item" value="today">Today</option>
                    <option class="dropdown-item filter-item" value="yesterday">Yesterday</option>
                    <option class="dropdown-item filter-item" value="last_7_days">Last 7 days</option>
                    <option class="dropdown-item filter-item" value="last_30_days">Last 30 days</option>
                    <option class="dropdown-item filter-item" value="thismonth">This Month</option>
                    <option class="dropdown-item filter-item" value="monthly">Monthly</option>
                    <option class="dropdown-item filter-item" value="yearly">Yearly</option>
                </div>
            </div> -->
            <div class="chart-container height-300">
                <canvas id="chart"></canvas>
            </div>
        </div>
    </div>
</div>
@endif
<div id="store_details" {{(count($campaignLists) == 0 && $data['catalogs_count'] == 0) ? 'hidden':''}}>
    <h2 class="text-center text-muted"><b>Manage Store</b></h2>
    <hr>
    <div class="row w-100 d-flex justify-content-center m-0">
        <!-- <div class="col-md-4 col-sm-12">
            <div class="card cursor-pointer" onclick="location.href='{{route('admin.orders.index')}}'">
                <div class="card-header d-flex align-items-start pb-0">
                    <div>
                        <h2 class="text-bold-700 mb-0">{{$data['orders_count']}}</h2>
                        <p>Orders</p>
                    </div>
                    <div class="avatar bg-rgba-success p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-list text-success font-medium-5"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="col-md-6 col-sm-12">
            <div class="card cursor-pointer" onclick="location.href='{{route('admin.products.product_list')}}'">
                <div class="card-header d-flex align-items-start pb-0">
                    <div>
                        <h2 class="text-bold-700 mb-0">{{$data['products_count']}}</h2>
                        <p>No. Products</p>
                    </div>
                    <div class="avatar bg-rgba-primary p-50 m-0">
                        <div class="avatar-content">
                            <i class="feather icon-box text-success font-medium-5"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-12">
            <div class="card cursor-pointer" onclick="location.href='{{route('admin.campaigns.index')}}'">
                <div class="card-header d-flex align-items-start pb-0">
                    <div>
                        <h2 class="text-bold-700 mb-0">{{$data['catalogs_count']}}</h2>
                        <p>Total Catalogs</p>
                    </div>
                    <div class="avatar bg-rgba-primary p-50 m-0">
                        <div class="avatar-content">
                            <i class="fa fa-list text-success font-medium-5"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-5 col-sm-12">
            @if(empty($userSettings->subdomain) || empty($user_meta_data['storefront']['storename']) || empty($user_meta_data['settings']['emailid']) || empty($user_meta_data['settings']['contactnumber']) || empty($user_meta_data['settings']['emailfromname']))
            <div class="card" style="box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.3) !important;padding:3px;">
                <form method="POST" action="{{ route("admin.userSettings.set_domain") }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card-header pt-1 card-title " style="margin:auto 5px;">
                        <h3 class="w-100 text-center"><b>Store Settings</b></h3>
                    </div>
                    <hr class="m-0">
                    <h6 class="text-center mt-sm-1">You haven't set your store settings. Please set it.</h6>
                    <hr class="m-0">
                    <center><button type="button" class="btn btn-primary p-1 mt-sm-1 text-center" id="show_settings_store" onclick="setname()">Tap to set</button></center>
                    <div class="p-1" id="settings_store" style="display: none">
                        <div class="form-group">
                            <div class="controls">
                                <label>Store Name</label>
                                <input type="text" class="form-control" value="{{ old('name', $user_meta_data['storefront']['storename'] ?? '') }}" name="storename" placeholder="Store Name here..." maxlength="35" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="controls">
                                <label>Subdomain</label>
                                <fieldset>
                                    <div class="input-group">
                                        <input type="text" class="form-control" style="text-transform:lowercase" placeholder="subdomain" aria-describedby="basic-addon2" name="subdomain" class="subdomain" id="subdomain" value="{{ old('subdomain', $user_meta_data['settings']['subdomain'] ?? '') }}" required>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="basic-addon2">.{{env('SITE_URL')}}</span>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="controls">
                                <label>Email ID</label>
                                <input type="email" class="form-control" value="{{ old('emailid', $user_meta_data['settings']['emailid'] ?? '') }}" name="emailid" placeholder="Email ID here..." required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="controls">
                                <label>Email From Name</label>
                                <input type="text" class="form-control" value="{{ old('emailfromname', $user_meta_data['settings']['emailfromname'] ?? '') }}" name="emailfromname" placeholder="Email From Name here..." required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="controls">
                                <label>Contact Number</label>
                                <input type="number" class="form-control" value="{{ old('contactnumber', $user_meta_data['settings']['contactnumber'] ?? '') }}" name="contactnumber" placeholder="Contact Number here..." required>
                            </div>
                        </div>

                        <center><button type="submit" class="btn btn-danger" id="set_settings_store" onclick="setname()">Tap to set</button></center>

                    </div>
                </form>
            </div>
            @else
            <div class="card" style="box-shadow: 0 4px 25px 0 rgba(0, 0, 0, 0.3) !important;padding:3px;">
                <div class="card-header p-0 card-title " style="margin:auto 5px;">
                    <h3 class="w-100 text-center"><b>{{$user_meta_data['storefront']['storename'] ?? "Untitled Store"}}</b></h3>
                    <a href="{{route('admin.userSettings.index')}}#storefront" class="text-dark text-right position-absolute" style="width:95%">
                        <i class="fa fa-pencil"></i></a>
                </div>
                <hr class="m-0">
                <div class="card-content">
                    <div class="rounded justify-content-center d-flex" style="min-height:150px;max-height:150px;">
                        <img src="{{App\Http\Controllers\Admin\CampaignsController::publicLink($userSettings->public_link,'store','',true)}}" width="150" class="" style="height:140px;">
                    </div>
                    <div class="card-body p-0" style="margin:10px; auto">
                        <h6 class="text-center"><a href="{{App\Http\Controllers\Admin\CampaignsController::publicLink($userSettings->public_link,'store')}}" id="public_store_link_business_copy" target="_blank">{{App\Http\Controllers\Admin\CampaignsController::publicLink($userSettings->public_link,'store')}}
                            </a></h6>
                        <h6 class="text-center">
                            <a href="javascript:void(0);" class="text-muted" onclick="StoreLinkBusinessCopyToClipboard('#public_store_link_business_copy')"><b>Tap to copy <i class="feather icon-copy"></i></b></a>
                        </h6>
                        <h6 class="text-info"><b></b></h6>

                    </div>
                </div>
                <h6 class="text-center">Your customers can view your catalogs and place orders via this link.</h6>

                <div class="card-footer p-0 bg-white h4 m-0" style="margin-bottom:5px;padding-top:5px !important;">
                    <div class="row m-0 w-100">
                        <div class="col-sm-12 p-0" style="margin: 0px 8px 5px 5px;">
                            <div class="d-flex justify-content-between float-right">
                                <div>
                                    <!-- <a href="{{route('admin.mystore.preview',$userSettings->preview_link)}}" target="__blank" class="text-warning"><i class="feather icon-eye"></i></a> -->
                                    <!-- <a href="javascript:void(0);" onclick="linkShare($(this))" class="text-primary ml-1" style="color:#6610F2 !important;" data-link="{{$userSettings->public_link}}" data-qrcode="{{App\Http\Controllers\Admin\CampaignsController::publicLink($userSettings->public_link,'store','',true)}}" data-title='{{$user_meta_data['storefront']['storename'] ?? "Untitled Store"}}' data-sharelink="{{App\Http\Controllers\Admin\CampaignsController::publicLink($userSettings->public_link,'store')}}" data-pdf="false"> -->
                                    <a href="#" class="btn btn-primary btn-icon" data-toggle="modal" data-target="#StorePublishedModal">   
                                        <i class="fa fa-share-alt"></i>
                                    </a>
                                    <a href="javascript:void(0);" class="text-success ml-1 mr-1" style="color:#28C76F !important" onclick="WhatsAppShare('{{App\Http\Controllers\Admin\CampaignsController::publicLink($userSettings->public_link,'store')}}','')">
                                        <b><i class="fa fa-whatsapp"></i></b>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
        <div class="col-md-7 col-sm-12">
            <div class="row">
                <div class="col col-md-4 col-sm-6">
                    <div class="card text-center cursor-pointer" onclick="location.href='{{route('admin.campaigns.index')}}'">
                        <div class="card-content">
                            <div class="card-body p-1">
                                <div class="avatar bg-rgba-info p-10 m-0 mb-1">
                                    <div class="avatar-content">
                                        <i class="fa fa-list nav-icon text-info font-medium-5"></i>
                                    </div>
                                </div>
                                <p class="mb-0 line-ellipsis">Catalogs</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-md-4 col-sm-6">
                    <div class="card text-center cursor-pointer" onclick="location.href='{{route('admin.products.product_list')}}'">
                        <div class="card-content">
                            <div class="card-body p-1">
                                <div class="avatar bg-rgba-info p-10 m-0 mb-1">
                                    <div class="avatar-content">
                                        <i class="feather icon-box text-info font-medium-5"></i>
                                    </div>
                                </div>
                                <p class="mb-0 line-ellipsis">Products</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-md-4 col-sm-6">
                    <div class="card text-center cursor-pointer" onclick="location.href='{{route('admin.orders.index')}}'">
                        <div class="card-content">
                            <div class="card-body p-1">
                                <div class="avatar bg-rgba-info p-10 m-0 mb-1">
                                    <div class="avatar-content">
                                        <i class="feather icon-list text-info font-medium-5"></i>
                                    </div>
                                </div>
                                <p class="mb-0 line-ellipsis">Orders</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col col-md-4 col-sm-6">
                    <div class="card text-center cursor-pointer" onclick="location.href='{{route('admin.product-categories.index')}}'">
                        <div class="card-content">
                            <div class="card-body p-1">
                                <div class="avatar bg-rgba-info p-10 m-0 mb-1">
                                    <div class="avatar-content">
                                        <i class="feather icon-folder text-info font-medium-5"></i>
                                    </div>
                                </div>
                                <p class="mb-0 line-ellipsis">Catagories</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-md-4 col-sm-6">
                    <div class="card text-center cursor-pointer" onclick="location.href='{{route('admin.customers.index')}}'">
                        <div class="card-content">
                            <div class="card-body p-1">
                                <div class="avatar bg-rgba-info p-10 m-0 mb-1">
                                    <div class="avatar-content">
                                        <i class="feather icon-users text-info font-medium-5"></i>
                                    </div>
                                </div>
                                <p class="mb-0 line-ellipsis">Customers</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col col-md-4 col-sm-6">
                    <div class="card text-center cursor-pointer" onclick="location.href='{{route('admin.userSettings.index')}}'">
                        <div class="card-content">
                            <div class="card-body p-1">
                                <div class="avatar bg-rgba-info p-10 m-0 mb-1">
                                    <div class="avatar-content">
                                        <i class="feather icon-settings text-info font-medium-5"></i>
                                    </div>
                                </div>
                                <p class="mb-0 line-ellipsis">Settings</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(count($campaignLists) > 0)
    <h3 class="text-light text-center p-2"><b>Home Screen Catalogs ({{count($campaignLists)}})</b></h3>
    @endif
    <hr class="m-0">

    <div class="row mt-2">
        @foreach ($campaignLists as $campaign)
        @php
        if(empty($userSettings->subdomain))
        $link = route('campaigns.published',$campaign->public_link);
        else
        $link = $userSettings->subdomain.".".env('SITE_URL')."/".$campaign->public_link;
        $entitycards = $campaign->campaignEntitycards;
        $n = count($entitycards);
        $img = asset('XR/assets/images/placeholder.png');
        @endphp
        <div class="col-xl-4 col-md-6 col-sm-12 profile-card-2 shadow-lg" id="catalog_{{$campaign->id}}">
            <div class="card h5">
                <div class="card-header p-0">
                    <div class="row m-0 w-100">
                        <div class="col-sm-12" style="padding: 10px 10px 0px 10px;">
                            <div class="d-flex justify-content-between">
                                <div class="text-truncate" style="width:80%"><b>{{$campaign->name}}</b></div>
                                <div class="float-right">
                                    {{-- <a href="{{route('admin.campaigns.edit',$campaign->id)}}" class="text-dark"><i
                                        class="fa fa-pencil"></i></a> --}}
                                    <a href="javascript:void(0);" id="catalog_settings_{{$campaign->id}}" onclick="catalog_settings($(this))" data-id="{{$campaign->id}}" data-is_start="{{$campaign->is_start}}" data-is_private="{{$campaign->is_private}}" data-private_password="{{$campaign->private_password}}" data-is_expired="{{empty($campaign->public_shareable->expires_at)?'false':'checked'}}" data-expired_date="{{ empty($campaign->public_shareable->expires_at)?'':date('Y-m-d\TH:i', strtotime($campaign->public_shareable->expires_at)) }}" data-is_added_home="{{$campaign->is_added_home}}" class="text-dark" data-toggle="modal" data-target="CatalogSettingsModal">
                                        <i class="feather icon-settings"></i>
                                    </a>
                                    <a href="javascript:void(0);" onclick="deleteCatalog({{$campaign->id}})" class="text-danger ml-1"><i class="fa fa-trash"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--  <div class="card-content" style="padding: 0px 5px 5px 5px;">
                    <div class="row" style="padding: 10px 15px 10px 15px;cursor:pointer" onclick="location.href='{{route('admin.campaigns.edit',$campaign->id)}}'">
                        @if(count($entitycards)== 0 )
                        <div class="col-6 rounded-left" style="padding:5px;width:100%;top:0;bottom:0; background:#dee1e4;">
                            <div class="image-grid-container text-center" style="max-height:150px;">

                                <img class="img-fluid lazyload" loading="lazy" data-src="{{ $img }}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="width:150px;max-height:200px;">
                            </div>
                        </div>
                        <div class="col-6 pr-0">

                            <div class="image-grid-container mb-1 text-center" style="min-height:100px;max-height:100px;padding:5px;border-top-right-radius:0.5rem !important;background:#dee1e4;">

                                <img class="img-fluid lazyload" loading="lazy" data-src="{{ $img }}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="width:100px;max-height:90px;">
                            </div>
                            <div class="image-grid-container text-center" style="min-height:100px;max-height:100px;padding:5px;border-bottom-right-radius:0.5rem !important;background:#dee1e4;">

                                <img class="img-fluid lazyload" loading="lazy" data-src="{{ $img }}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="width:100px;max-height:90px;">
                            </div>
                        </div>
                        @elseif( ( count($entitycards) >=1) && (count($entitycards) <= 3) ) <div class="col-6 rounded-left" style="padding:5px;width:100%;top:0;bottom:0; background:#dee1e4;">
                            <div class="image-grid-container text-center" style="max-height:150px;">
                                @php
                                if(count($entitycards) > 2){
                                $i = 2;
                                }
                                else if (count($entitycards) > 1){
                                $i = 1;
                                }
                                else{
                                $i = 0;
                                }

                                if(count($entitycards[$i]->product->photo) > 0){
                                $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                                }
                                else{
                                $img = asset('XR/assets/images/placeholder.png');
                                }
                                @endphp
                                <img class="img-fluid lazyload" loading="lazy" data-src="{{ $img }}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="width:150px;max-height:200px;">
                            </div>
                    </div>
                    <div class="col-6 pr-0">

                        <div class="image-grid-container mb-1 text-center" style="min-height:100px;max-height:100px;padding:5px;border-top-right-radius:0.5rem !important;background:#dee1e4;">
                            @php
                            if(count($entitycards) > 2){
                            $i = 1;
                            if(count($entitycards[$i]->product->photo) > 0){
                            $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                            }
                            else{
                            $img = asset('XR/assets/images/placeholder.png');
                            }
                            }
                            else if (count($entitycards) > 1){
                            $i = 0;
                            if(count($entitycards[$i]->product->photo) > 0){
                            $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                            }
                            else{
                            $img = asset('XR/assets/images/placeholder.png');
                            }
                            }
                            else if(count($entitycards) == 1){
                            $img = asset('XR/assets/images/placeholder.png');
                            }
                            @endphp
                            <img class="img-fluid lazyload" loading="lazy" data-src="{{ $img }}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="width:100px;max-height:90px;">
                        </div>
                        <div class="image-grid-container text-center" style="min-height:100px;max-height:100px;padding:5px;border-bottom-right-radius:0.5rem !important;background:#dee1e4;">
                            @php
                            if(count($entitycards) > 2){
                            if(count($entitycards[0]->product->photo) > 0){
                            $img = $entitycards[0]->product->photo[0]->getUrl('thumb');
                            }
                            else{
                            $img = asset('XR/assets/images/placeholder.png');
                            }
                            }
                            else{
                            $img = asset('XR/assets/images/placeholder.png');
                            }
                            @endphp
                            <img class="img-fluid lazyload" loading="lazy" data-src="{{ $img }}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="width:100px;max-height:90px;">
                        </div>
                    </div>
                    @else
                    <div class="col-6 rounded-left" style="padding:5px;width:100%;top:0;bottom:0; background:#dee1e4;">
                        <div class="image-grid-container text-center" style="max-height:150px;">
                            @php
                            if(count($entitycards[$n-1]->product->photo) > 0)
                            $img = $entitycards[$n-1]->product->photo[0]->getUrl('thumb');
                            else
                            $img = asset('XR/assets/images/placeholder.png');
                            @endphp
                            <img class="img-fluid lazyload" loading="lazy" data-src="{{ $img }}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="width:150px;max-height:200px;">
                        </div>
                    </div>
                    <div class="col-6 pr-0">
                        <div class="image-grid-container mb-1 text-center" style="min-height:100px;max-height:100px;padding:5px;border-top-right-radius:0.5rem !important;background:#dee1e4;">
                            @php
                            if(count($entitycards[$n-2]->product->photo) > 0)
                            $img = $entitycards[$n-2]->product->photo[0]->getUrl('thumb');
                            else
                            $img = asset('XR/assets/images/placeholder.png');
                            @endphp
                            <img class="img-fluid lazyload" loading="lazy" data-src="{{ $img }}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="width:100px;max-height:90px;">
                        </div>
                        <div class="image-grid-container text-center" style="min-height:100px;max-height:100px;padding:5px;border-bottom-right-radius:0.5rem !important;background:#dee1e4;">
                            @php
                            if(count($entitycards[$n-3]->product->photo) > 0)
                            $img = $entitycards[$n-3]->product->photo[0]->getUrl('thumb');
                            else
                            $img = asset('XR/assets/images/placeholder.png');
                            @endphp
                            <img class="img-fluid lazyload" loading="lazy" data-src="{{ $img }}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="width:100px;max-height:90px;">
                        </div>
                    </div>
                    @endif

                </div> --}}
                <div class="card-content" style="padding: 0px 5px 5px 5px;">
                <div class="row border-0" style=" padding:10px;cursor:pointer;height:250px;" onclick="location.href='{{route('admin.campaigns.edit',$campaign->id)}}'">
                @if(count($entitycards)==0 )
                        <div class=" col-12 rounded h-100 my-auto text-disabled">
                            <h2 class="text-center mt-3 text-muted">No Products Added Yet</h2>
                        </div>
                    @elseif(count($entitycards)==1 )
                        <div class=" col-12 rounded h-100 d-flex justify-content-center">
                            @php
                            $i = 0;
                            if(!empty($entitycards[$i]->product) && count($entitycards[$i]->product->photo) > 0){
                            $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                            }
                            else{
                            $img = asset('XR/assets/images/placeholder.png');
                            }
                            @endphp
                            <img class="img-responsive mw-100 h-100 lazyload rounded" data-src="{{$img}}">
                        </div>
                    @elseif( ( count($entitycards)==2) ) 
                        <div class="col-12 h-50 bg- d-inline-flex">
                            <div class="w-50 h-100 d-flex justify-content-center" style="padding:5px;">
                                @php
                                $i = 0;
                                if(!empty($entitycards[$i]->product) && count($entitycards[$i]->product->photo) > 0){
                                $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                                }
                                else{
                                $img = asset('XR/assets/images/placeholder.png');
                                }
                                @endphp
                                <img class="img-responsive mw-100 h-100 lazyload rounded" data-src="{{$img}}">
                            </div>
                            <div class="w-50 h-100" style="padding:5px;"></div>
                        </div>
                        <div class="col-12 h-50 d-inline-flex">
                            <div class="w-50 h-100" style="padding:5px;"></div>
                            <div class="w-50 h-100 d-flex justify-content-center" style="padding:5px;">
                                @php
                                $i = 1;
                                if(!empty($entitycards[$i]->product) && count($entitycards[$i]->product->photo) > 0){
                                $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                                }
                                else{
                                $img = asset('XR/assets/images/placeholder.png');
                                }
                                @endphp
                                <img class="img-responsive mw-100 h-100 lazyload rounded" data-src="{{$img}}">
                            </div>
                        </div>
                    @elseif(count($entitycards)==3)
                        <div class="col-12 h-50 d-flex justify-content-center border-bottom" style="padding:5px;">
                            @php
                            $i = 0;
                            if(!empty($entitycards[$i]->product) && count($entitycards[$i]->product->photo) > 0){
                            $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                            }
                            else{
                            $img = asset('XR/assets/images/placeholder.png');
                            }
                            @endphp
                            <img class="img-responsive mw-100 h-100 lazyload rounded" data-src="{{$img}}">
                        </div>
                        <div class="col-12 h-50 d-inline-flex border-top">
                            <div class="w-50 h-100 d-flex justify-content-center border-right" style="padding:5px;">
                                @php
                                $i = 1;
                                if(!empty($entitycards[$i]->product) && count($entitycards[$i]->product->photo) > 0){
                                $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                                }
                                else{
                                $img = asset('XR/assets/images/placeholder.png');
                                }
                                @endphp
                                <img class="img-responsive mw-100 h-100 lazyload rounded" style="z-index:10;" data-src="{{$img}}">
                            </div>
                            <div class="w-50 h-100 d-flex justify-content-center border-left" style="padding:5px;">
                                @php
                                $i = 2;
                                if(!empty($entitycards[$i]->product) && count($entitycards[$i]->product->photo) > 0){
                                $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                                }
                                else{
                                $img = asset('XR/assets/images/placeholder.png');
                                }
                                @endphp
                                <img class="img-responsive mw-100 h-100 lazyload rounded" data-src="{{$img}}">
                            </div>
                        </div>
                    @elseif(count($entitycards)>3)
                        <div class="col-12 h-50 bg- d-inline-flex">
                            <div class="w-50 h-100 border-bottom border-right d-flex justify-content-center" style="padding:5px;">
                                @php
                                $i = 0;
                                if(!empty($entitycards[$i]->product) && count($entitycards[$i]->product->photo) > 0){
                                $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                                }
                                else{
                                $img = asset('XR/assets/images/placeholder.png');
                                }
                                @endphp
                                <img class="img-responsive mw-100 h-100 lazyload rounded" data-src="{{$img}}">
                            </div>
                            <div class="w-50 h-100 border-bottom border-left d-flex justify-content-center" style="padding:5px;">
                                @php
                                $i = 1;
                                if(!empty($entitycards[$i]->product) && count($entitycards[$i]->product->photo) > 0){
                                $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                                }
                                else{
                                $img = asset('XR/assets/images/placeholder.png');
                                }
                                @endphp
                                <img class="img-responsive mw-100 h-100 lazyload rounded" data-src="{{$img}}">
                            </div>
                        </div>
                        <div class="col-12 h-50 d-inline-flex">
                            <div class="w-50 h-100 border-top border-right d-flex justify-content-center" style="padding:5px;">
                                @php
                                $i = 2;
                                if(!empty($entitycards[$i]->product) && count($entitycards[$i]->product->photo) > 0){
                                $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                                }
                                else{
                                $img = asset('XR/assets/images/placeholder.png');
                                }
                                @endphp
                                <img class="img-responsive mw-100 h-100 lazyload rounded" data-src="{{$img}}">
                            </div>
                            <div class="w-50 h-100 border-top border-left d-flex justify-content-center" style="padding:5px;">
                                @php
                                $i = 3;
                                if(!empty($entitycards[$i]->product) && count($entitycards[$i]->product->photo) > 0){
                                $img = $entitycards[$i]->product->photo[0]->getUrl('thumb');
                                }
                                else{
                                $img = asset('XR/assets/images/placeholder.png');
                                }
                                @endphp
                                <img class="img-responsive mw-100 h-100 lazyload rounded" data-src="{{$img}}">
                            </div>
                        </div>
                    @endif
                </div>

                <div class="row m-0 w-100">
                    <div class="col-sm-12" style="padding:0px 5px 5px 5px;">
                        <div class="d-flex justify-content-between">
                            <div>
                                <span class="h2 m-0">{{count($entitycards)}} <i class="feather icon-box"></i></span>
                                <span class="h2 ml-1  m-0">{{$campaign->views}} <i class="feather icon-eye"></i></span>
                            </div>
                            <div>
                                <span class="h2 m-0 text-danger" id="lock_icon_{{$campaign->id}}" {{$campaign->is_private == 1 ? '':'hidden'}}><i class="fa fa-lock" aria-hidden="true"></i></span>
                                <span class="h2 m-0 ml-1 text-warning" id="clock_icon_{{$campaign->id}}" {{$campaign->public_shareable->expires_at ? '':'hidden'}}><i class="fa fa-clock-o" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="" data-original-title="{{date('d-m-Y h:i:s A', strtotime($campaign->public_shareable->expires_at))}}"></i></span>
                            </div>
                            <div class="float-right h2 m-0">
                                <div id="catalog_share_{{$campaign->id}}" {{$campaign->is_start == 1 ? '':'hidden'}} {{auth()->user()->userInfo->is_start == 1 ? '':'hidden'}}>
                                    @if(session('subscribed_features'))
                                        @if(array_key_exists("stories", session('subscribed_features')[0]))
                                            <a href="javascript:void(0);" class="text-dark sharebtn mr-1" data-link="{{$campaign->public_link}}" data-qrcode="{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'stories','',true)}}" data-title="{{$campaign->name}}  Stories " data-sharelink="{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'stories')}}" data-pdf="false" data-download="false">
                                                <img class="mt-0" src="{{asset('/XR/app-assets/images/icons/stories.svg')}}" style="height: 24px;width: 21px;">
                                            </a>
                                        @endif
                                    @endif
                                    <a href="javascript:void(0);" class="text-dark sharebtn" data-link="{{$campaign->public_link}}" data-qrcode="{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'catalog','',true)}}" data-title="{{$campaign->name}}" data-sharelink="{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'catalog')}}" data-download="false">
                                        <i class="fa fa-share-alt"></i> 
                                    </a>
                                    <a href="javascript:void(0);" onclick="WhatsAppShare('{{App\Http\Controllers\Admin\CampaignsController::publicLink($campaign->public_link,'catalog')}}','')" class="text-success ml-1"><i class="fa fa-whatsapp"></i></a>
                                </div>
                                <div id="catalog_noshare_{{$campaign->id}}" {{$campaign->is_start == 1 ? 'hidden':''}}>
                                    <a href="javascript:void(0);" class="text-light">
                                        <i class="fa fa-share-alt"></i>
                                    </a>
                                    <a href="javascript:void(0);" class="text-light ml-1"><i class="fa fa-whatsapp"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @endforeach
    @include('modals.published1')
    @include('modals.campaign_settings')

</div>
</div>
{{ $campaignLists->links() }}

@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.bundle.js" integrity="sha512-zO8oeHCxetPn1Hd9PdDleg5Tw1bAaP0YmNvPY8CwcRyUk7d7/+nyElmFrB6f7vg4f7Fv4sui1mcep8RIEShczg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.bundle.min.js" integrity="sha512-SuxO9djzjML6b9w9/I07IWnLnQhgyYVSpHZx0JV97kGBfTIsUYlWflyuW4ypnvhBrslz1yJ3R+S14fdCWmSmSA==" crossorigin="anonymous"></script>
@parent
<script>
    var init_label = "";
    var statChart = null;
    var config = null;
    var flag = false;
    var filter = {
        "sdate": '',
        "edate": '',
        "month_year": '',
        'current': '',
    }


    $(document).ready(function() {

        $(".stat-card").hover(
            function() {
                $(this).addClass('shadow-lg bg-secondary text-white');
            },
            function() {
                $(this).removeClass('shadow-lg bg-secondary text-white');
            }
        );
        showstats("orders");
    });

    function showstats(label) {


        if (label && init_label == "") {
            addClass(label);
        } else if (label != init_label) {

            removeClass();
            addClass(label);
        } else if (init_label == label) {

            return;
        }

        init_label = label;
        if (flag)
            showchart(label, filter);
        else
            showchart(label, false);
    }

    function showchart(label, filter) {


        var ctx = document.getElementById('chart').getContext("2d");
        var q = '';
        flag = false;
        if (filter) {
            flag = true;
            q = "&flag=" + flag + '&sdate=' + filter.sdate + '&edate=' + filter.edate + '&month_year=' + filter.month_year + '&current=' + filter.current;
        } else {
            flag = false;
            q = "&flag=" + flag;
        }
        var url = "{{route('admin.stats.getSalesstat')}}" + "?label=" + label + q;
        $.ajax(url).done(function(data) {
            if (data) {

                // console.log(data);

                $("#sales-stat-value").html("{!! $currency !!}" + currencyFormatter(data.metrics.totalSales, 2));
                $("#orders-stat-value").html(data.metrics.ordersCount);
                $("#catalog-views-stat-value").html(data.metrics.catalogView);
                $("#videocatalog-views-stat-value").html(data.metrics.videocatalogview);
                $("#product-views-stat-value").html(data.metrics.prodView);

                total_price = data.total_price;
                // console.log(data);
                data.data = Object.values(data.data);
                var dataset = [];
                var length = data.label.length == 0 ? 1 : data.label.length;

                temp = {};
                temp['label'] = data.label;
                temp['data'] = data.data;
                temp['backgroundColor'] = data.bgcolour[0];
                temp['borderColor'] = data.bordercolour[0];
                temp['fill'] = false;
                dataset[0] = temp;


                config = {
                    type: 'line',
                    data: {
                        labels: data.labels,
                        datasets: dataset,
                    },
                    options: {
                        // layout: {
                        //     padding: {
                        //         left: 0,
                        //         right: 0,
                        //         top: 0,
                        //         bottom: 0
                        //     }
                        // },
                        tooltips: {
                            callbacks: {
                                title: function(tooltipItem, config) {
                                    // return data.axes[0] + " : " + tooltipItem[0].label;
                                },
                                afterTitle: function(tooltipItem, config) {
                                    // console.log(total_price);
                                    if (!hasNull(total_price) && (init_label == 'orders')) {
                                        symbol = "{{ $currency }}";
                                        return ("Total Sales : " + currencyFormatter(total_price[tooltipItem[0].index], 2));
                                    }
                                },
                                label: function(tooltipItem, config) {

                                    if (init_label == "customers")
                                        return "No. of Customers : " + tooltipItem.value;
                                    else if (init_label == "orders")
                                        return "No. of Orders : " + tooltipItem.value;
                                    else if (init_label == "catalog-views")
                                        return "Catalog Views : " + tooltipItem.value;
                                    else if (init_label == "product-views")
                                        return "Product Views : " + tooltipItem.value;
                                    else if (init_label == "sales") {
                                        symbol = "{!! $currency !!}";
                                        return "Total Sales : " + currencyFormatter(tooltipItem.value, 2);
                                    }
                                },

                            },
                        },
                        default: {
                            ticks: {
                                min: 0
                            },
                        },

                        legend: {
                            display: false,
                            position: 'top',
                            labels: {
                                fontColor: "#000080",
                            }
                        },
                        title: {
                            text: data.title.replace('-', " "),
                            display: true,
                            position: 'top',
                            fontSize: 16,
                        },
                        responsive: true,
                        maintainAspectRatio: false,
                        aspectRatio: 2,
                        scales: {
                            xAxes: [{
                                ticks: {
                                    // min: 0,
                                    // max: 100,
                                    // stepSize: 10 / 4,
                                    beginAtZero: true,
                                },
                                scaleLabel: {
                                    display: true,
                                    labelString: data.axes[0],
                                    fontSize: 16,
                                }
                            }],
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true,
                                    // min: 0,
                                    // max: 100,
                                    precision: 0,
                                },
                                scaleLabel: {
                                    display: true,
                                    labelString: data.axes[1],
                                    fontSize: 16,
                                }
                            }]
                        }
                    }
                };


                change('line');

            }
        });

    }

    function change(newType) {
        var ctx = document.getElementById('chart').getContext('2d');

        // Remove the old chart and all its event handles
        if (statChart) {
            statChart.destroy();
        }

        // Chart.js modifies the object you pass in. Pass a copy of the object so we can use the original object later
        var temp = jQuery.extend(true, {}, config);
        temp.type = newType;
        statChart = new Chart(ctx, temp);

    };

    //applyfilter
    // $('.filter-item').on('click', function() {
    //     value = $(this).val();
    //     $('.filter-btn').text($(this).html());
    //     showchart(init_label, {
    //         "sdate": '',
    //         "edate": '',
    //         "month_year": '',
    //         'current': value
    //     });

    // });

    $('.filter').on('input', function() {
        value = $(this).val();

        filter = {
            "sdate": '',
            "edate": '',
            "month_year": '',
            'current': value
        }
        showchart(init_label, filter);


    });


    function addClass(label) {
        $("#" + label + "-card").addClass('arrow_box');
        $("#" + label + "-card").addClass('border-primary');
        $("." + label + "-icon").removeClass('text-success');
        $("." + label + "-icon").addClass('text-primary');
        $("." + label).addClass('text-primary');
    }

    function removeClass() {

        $("#" + init_label + "-card").removeClass('arrow_box');
        $("#" + init_label + "-card").removeClass('border-primary');
        $("." + init_label + "-icon").removeClass('text-primary');
        $("." + init_label + "-icon").addClass('text-success');
        $("." + init_label).removeClass('text-primary');
    }




    function WhatsAppShare(url, msg) {
        var t = "I suggest you to visit our website to view our product catalogs and place your order via this link.";
        var hashtags = "{{ isset($meta_data['storefront']['storename'])?$meta_data['storefront']['storename'] : ''}}";
        var message = encodeURIComponent(url) + " " + t + " #" + hashtags;
        
        var whatsapp_url = "https://wa.me/?text=" + message;
        openInNewTab(whatsapp_url);
    }

    function StoreLinkBusinessCopyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text());
        $temp.select();
        document.execCommand("copy");
        $temp.remove();
        toastr.success('Share Link', 'Copied!');
    }


    function deleteCatalog(id) {
        var url = "{{route('admin.campaigns.destroy',':id')}}";
        url = url.replace(':id', id);
        swal.fire({
            title: 'Are you sure to remove this Catalog?',
            text: 'This is an irrevocable action and you will loose all the data regarding this catalog.',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, Remove it.',
            cancelButtonText: 'No, Cancel',
            confirmButtonClass: 'btn btn-danger',
            cancelButtonClass: 'btn btn-light',
        }).then(function(result) {
            if (result.value) {
                $('#loading-bg').show();
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                    success: function(response_sub) {
                        $('#loading-bg').show();
                        Swal.fire({
                            type: "success",
                            title: 'Deleted!',
                            text: 'Catalog has been deleted.',
                            confirmButtonClass: 'btn btn-success',
                        }).then((result) => {
                            $('#catalog_' + id).remove();
                            $('#loading-bg').hide();
                        });

                    }
                });


            }
        })

    }

    function setname() {
        $("#show_settings_store").hide();
        $("#settings_store").show();
    }

    $('input[name="subdomain"]').keypress(function(e) {
        var key = e.which;
        return ((key >= 48 && key <= 57) || (key >= 65 && key <= 90) || (key >= 97 && key <= 122));
    });
</script>
@endsection