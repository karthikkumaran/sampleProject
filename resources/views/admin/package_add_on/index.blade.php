@extends('layouts.admin')
@section('content')
<div class="card">
    <div class="card-header">
        Subscribed users
    </div>
    <div class="card-body">
        <table class="table table-bordered table-striped table-hover ajaxTable datatable datatable-SubscribedUsers">
            <thead>
                <tr>
                    <th scope="col">
                        User Name
                    </th>
                    <th scope="col">
                        Plan 
                    </th>
                    <th scope="col">
                        Add ons
                    </th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    // url="{{route('admin.product-attributes.destroy',':id')}}";
    // url = url.replace(':id', 83);
    // //  console.log(url);

    $(function () {
        let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
        let dtOverrideGlobals = {
            buttons: dtButtons,
            responsive: true,
            processing: true,
            serverSide: true,
            retrieve: true,
            aaSorting: [],
            ajax: "{{ route('admin.package_add_ons.index') }}",
            columns: [
                { data: 'user_name', name: 'user_name' },
                { data: 'plan', name: 'plan' },
                { data: 'actions', name: '{{ trans('global.actions') }}' }
            ],
            order: [[ 0, 'desc' ]],
            pageLength: 100,
        };    

        $('.datatable-SubscribedUsers').DataTable(dtOverrideGlobals);
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
            $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
        });
    });  
</script>
@endsection