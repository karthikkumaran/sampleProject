@extends('layouts.admin')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.css"
    integrity="sha512-uKwYJOyykD83YchxJbUxxbn8UcKAQBu+1hcLDRKZ9VtWfpMb1iYfJ74/UIjXQXWASwSzulZEC1SFGj+cslZh7Q==" crossorigin="anonymous" />

<style>
    div.tagsinput {
        border: 1px solid #CCC;
        background: #FFF;
        /* padding: 5px; */
        width: 300px;
        /* height: 100px; */
        overflow-y: visible;
    }

    div.tagsinput span.tag {
        border: 1px solid #1709da;
        background: #0814c2;
        color: #ffffff;
        font-size: 18px;
        font-weight: 500;
    }

    div.tagsinput span.tag a {
        font-weight: 700;
        color: white;
        text-decoration: none;
        font-size: 19px;
        margin-bottom: 0;
        padding: 0;
    }

</style>
@section('content')

    <div class="show_alert">

    </div>

    <div class="card">
        <div class="card-header">
            <div>
                <a class="btn btn-md  btn-outline-secondary " href="{{ route('admin.users.index') }}">
                    Back to users list
                </a>
                <br>
            </div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-12 col-md-6 col-sm-12">
                    <h4>{!! $user->name !!} </h4>
                    <h6>{!! $user->email !!} </h6>
                </div>
                <div class="col-12 col-md-6 col-sm-12">
                    <div class="row">
                        <h4><b>Plan - {!! $subscription->name !!}</b></h4>
                    </div>
                    <div class="row">
                        <h4>Starts at: {!! $subscription->starts_at !!}</h4>
                    </div>
                    <div class="row">
                        <h4>Ends at: {!! $subscription->ends_at !!}</h4>
                    </div>
                </div>
            </div>
            <br>
            <form method="POST" action="{{ route('admin.package_add_ons.update', $user->id) }}" enctype="multipart/form-data">
            @method('PUT')    
            @csrf
                
                <label class="h3">{{ trans('cruds.feature.title') }}</label>
                <div class="col-md-1 btn btn-xs col-2 ml-1 ml-sm-0  btn-success btn-icon px-md-1 add_feature_add_on float-right" id="add_feature_add_on">
                    <i class="fa fa-plus fa-2x"></i>
                </div>
                <div class="row mb-2 mr-0">
                    <label class="form-group col col-md-4 text-center h4 d-none d-sm-block" for="name">{{ trans('cruds.feature.fields.feature_name') }}</label>
                    <label class="form-group col col-md-4 text-center h4 d-none d-sm-block" for="value">{{ trans('cruds.feature.fields.feature_value') }}</label>
                    <label class="form-group col col-md-4 text-center h4 d-none d-sm-block" for="add_on_value">{{ trans('cruds.feature.fields.feature_add_on') }}</label>
                </div>
                
                <div id="feature_add_on_section">
                    @foreach ($subscribed_features as $key => $subscribed_feature)
                        @php
                            $feature_details = $subscribed_feature->getFeatureDetails();
                            $grace_period_interval = "";
                            $grace_period_duration = "";
                        @endphp
                        @if($feature_details->name == "grace period")
                            @php
                                $grace_period_interval = $subscribed_feature->value;
                                $grace_period_duration = $subscribed_feature->add_on_value;
                            @endphp
                        @else
                        <div class="row" id="feature_{{$subscribed_feature->feature_id}}">
                            <input class="form-control {{ $errors->has('feature_name') ? 'is-invalid' : '' }}" 
                                type="hidden" name="feature_id[]" id="feature_id" value="{{ $subscribed_feature->feature_id }}">
                        <div class="form-group text-center col-md-4 col-sm-12 ">
                            <input class="form-control {{ $errors->has('feature_name') ? 'is-invalid' : '' }}" 
                                type="text" value="{{ $feature_details->name }}" disabled>
                        </div>
                        <div class="form-group text-center col-md-4 col-sm-12">
                            <input class="form-control {{ $errors->has('feature_value') ? 'is-invalid' : '' }}" 
                                type="text" name="feature_value[]" id="feature_value" value="{{ old('feature_value', $subscribed_feature->value) ?? null}}" disabled>
                        </div>
                        <div class="form-group col-md-3 col-sm-12">
                            <input class="form-control {{ $errors->has('feature_add_on') ? 'is-invalid' : '' }}" 
                                type="number" step="1" onkeypress="return !(event.charCode == 46)" name="feature_add_on[]" id="feature_add_on" value="{{ old('feature_add_on', $subscribed_feature->add_on_value) ?? null}}">
                            @if ($errors->has('feature_add_on'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('feature_add_on') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.feature.fields.feature_add_on_helper') }}</span>
                        </div>

                        @if($subscribed_feature->is_add_on == 1)
                            <div class="col-md-1 col-2  btn btn-danger h-50 btn-sm ml-1 ml-sm-0 fixed" id="delete_feature_{{ $subscribed_feature->feature_id }}" onclick="deleteUserFeature($(this))" data-feature_id="{{$subscribed_feature->feature_id }}">
                                <i class="fa fa-trash fa-2x"></i>
                            </div>
                        @endif
                        </div>
                        @endif
                    @endforeach
                    <label class="h3">Grace period</label>
                    <div class="row mb-2 mr-0">
                        <div class="col-lg-6">
                            <label class="h5">Grace period interval</label>
                            <select class="custom-select form-control" name="grace_period_interval" id="grace_period_interval">
                                <option value="">Select grace period interval type</option>
                                <option value="day" {{$grace_period_interval == 'day' ? 'selected' : ''}}>Day</option>
                                <option value="month" {{$grace_period_interval == 'month' ? 'selected' : ''}}>Month</option>
                                <option value="year" {{$grace_period_interval == 'year' ? 'selected' : ''}}>Year</option>
                            </select>
                        </div>
                        <div class="col-lg-6">
                            <label class="h5">Grace period duration</label>
                            <input class="form-control" type="number" step="1" onkeypress="return !(event.charCode == 46)" name="grace_period_duration" id="grace_period_duration" value="{{ $grace_period_duration ?? 0}}">
                        </div>
                    </div>
                </div>
                <div class="form-group mt-1">
                    <button class="btn btn-danger" type="submit">
                        {{ trans('global.save') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.js"
        integrity="sha512-wTIaZJCW/mkalkyQnuSiBodnM5SRT8tXJ3LkIUA/3vBJ01vWe5Ene7Fynicupjt4xqxZKXA97VgNBHvIf5WTvg=="
        crossorigin="anonymous">
    </script>

    <script>
        var feature_id = 0;
        var user_feature_id;
        var user_id = "{{$user->id}}"
        function add_feature_add_on() {
            feature_id += 1;
            url = "{{ route('admin.package_add_ons.addfeatureaddon', [':feature_id', ':user_id']) }}";
            url = url.replace(':feature_id', feature_id);
            url = url.replace(':user_id', user_id);
            var feature_row = $('<div>').load(url, function(row) {
                $('#feature_add_on_section').append(row);
                //     if ($('#attribute_section > div').length == 1)
                //         $('#delete_attribute_1').show();
            });
        }
        
        $('.add_feature_add_on').on("click", function() {
            add_feature_add_on();
        });

        function deleteUserFeature($this) {
            
                user_feature_id = $this.data('feature_id');
                var plan_id = "{{$subscription->plan_id}}"
                var url = "{{route('admin.package_add_ons.removeAddOnFeature',':id')}}";
                url = url.replace(':id', user_feature_id);
                swal.fire({
                    title: 'Are you sure to remove this feature?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, Remove it!',
                    cancelButtonText: 'No, Cancel!',
                    confirmButtonClass: 'btn btn-danger',
                    cancelButtonClass: 'btn btn-light',
                }).then(function (result) {
                    if (result.value) {
                        $.ajax({
                            url: url,
                            type: "POST",
                            data: {
                                '_method': 'DELETE',
                                'user_id': user_id,
                                'plan_id': plan_id
                            },
                            success: function (response_sub) {
                                // console.log(response_sub.status)
                                if(response_sub.status == "warning") {
                                    Swal.fire({
                                        type: "warning",
                                        title: 'Delete',
                                        text: "This feature is being used by the user, Are you sure to remove this feature?",
                                        showCancelButton: true,
                                        confirmButtonText: 'Yes, Remove it',
                                        cancelButtonText: 'No, Cancel',
                                        confirmButtonClass: 'btn btn-danger',
                                        cancelButtonClass: 'btn btn-light',
                                    }).then((result) => {
                                        if(result.value){
                                            $.ajax({
                                            url:url+"?flag="+true,
                                            type:"POST",
                                            data:{
                                                '_method':'DELETE',
                                                'user_id': user_id,
                                                'plan_id': plan_id
                                            },
                                            success:function(response){
                                                if(response.status=="success"){
                                                    delete_success(user_feature_id);
                                                }
                                            },error: function (res) {
                                                Swal.fire({
                                                    type: "error",
                                                    title: 'Delete',
                                                    text: "Can't remove this feature.",
                                                    confirmButtonClass: 'btn btn-danger',
                                                }).then((result) => {
                                                    // window.location.reload();
                                                });
                                            }
                                            });
                                        }
                                    });
                                }
                                else if(response_sub.status == "success") {
                                    delete_success(user_feature_id);
                                }
                            }
                        }); 
                    }
                });
            
        }
        
        function delete_success(user_feature_id)
        {
            Swal.fire({
                type: "success",
                title: 'Removed',
                text: 'Feature has been removed.',
                confirmButtonClass: 'btn btn-success',
            }).then((result) => {
                $('#feature_'+user_feature_id).remove();                                            
            });
        }
    </script>
@endsection