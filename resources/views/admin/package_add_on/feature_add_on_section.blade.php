<div class="row mt-1" id="add_on_feature_{{ $id }}">
    <!-- <input type="hidden" value="1" name="slug[]" /> -->

    <div class="form-group col-md-4 col-sm-12">
        <select class="custom-select form-control {{ $errors->has('feature_name') ? 'is-invalid' : '' }}"
            name="feature_id[]" id="feature_id" required>
            <option value="" selected>Select feature</option>
            @foreach($plan_subscription_features as $plan_subscription_feature)
                @if(in_array($plan_subscription_feature->id,$subscribed_features))
                @else
                <option value="{{ $plan_subscription_feature->id }}" data-feature_name="{{$plan_subscription_feature->name}}"> {{ $plan_subscription_feature->name }}</option>
                @endif
            @endforeach
        </select>
        {{-- @if ($errors->has('feature_name'))
            <div class="invalid-feedback">
                {{ $errors->first('feature_name') }}
            </div>
        @endif --}}
        <span class="help-block">{{ trans('cruds.feature.fields.feature_name_helper') }}</span>
    </div>
    
    <div class="form-group col-md-4 col-sm-12">
        <input class="form-control {{ $errors->has('feature_value') ? 'is-invalid' : '' }}" placeholder="Enter value for the feature"
            type="number" step="1" onkeypress="return !(event.charCode == 46)" name="feature_value[]" id="feature_value" value="{{ old('feature_value', '') }}" required>
        @if ($errors->has('feature_value'))
            <div class="invalid-feedback">
                {{ $errors->first('feature_value') }}
            </div>
        @endif
        <span class="help-block">{{ trans('cruds.feature.fields.feature_value_helper') }}</span>
    </div>

    <div class="form-group col-md-3 col-sm-12" id="feature_add_on_value">
        <input class="form-control {{ $errors->has('feature_add_on') ? 'is-invalid' : '' }}" 
            type="text" name="feature_add_on[]" id="feature_add_on" value="{{ old('feature_add_on', '') }}" disabled>
        @if ($errors->has('feature_add_on'))
            <div class="invalid-feedback">
                {{ $errors->first('feature_add_on') }}
            </div>
        @endif
        <span class="help-block">{{ trans('cruds.feature.fields.feature_add_on_helper') }}</span>
    </div>

    <div class="col-md-1 col-2  btn btn-danger h-50 btn-sm ml-1 ml-sm-0" id="delete_feature_{{ $id }}"
        onclick="delete_feature({{ $id }})">
        <i class="fa fa-trash fa-2x"></i>
    </div>
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.js"
    integrity="sha512-wTIaZJCW/mkalkyQnuSiBodnM5SRT8tXJ3LkIUA/3vBJ01vWe5Ene7Fynicupjt4xqxZKXA97VgNBHvIf5WTvg=="
    crossorigin="anonymous">
</script>
<script>
    function delete_feature(id) {
        if ($('#feature_add_on_section > div').length != 1) {
            $("#add_on_feature_" + id).remove();
        }
        else {
            $('.show_alert').empty();
            $('.show_alert').append("<div class='alert alert-danger p-2 alert-validation-msg alert-dismissable' role='alert' ><i class='feather icon-info mr-1 align-middle fa-2x '></i><span class='h4 text-danger'>Cannot delete this item</span><button type='button' class='close' data-dismiss='alert' aria-hidden='true' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
        }
    }
</script>
