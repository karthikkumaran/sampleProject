@extends('layouts.admin')
@section('content')
    <div class="show_alert">
    </div>
    <div class="card">
        <div class="card-header">
            <div>
                <a class="btn btn-md  btn-outline-secondary " href="{{ route("admin.users.index") }}">
                    Back to users list
                </a>
                <br>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-12 col-md-6 col-sm-12">
                    <h4>{!! $user->name !!} </h4>
                    <h6>{!! $user->email !!} </h6>
                </div>
                <div class="col-12 col-md-6 col-sm-12">
                    <div class="row">
                        <h4><b>Plan - {!! $subscription->name !!}</b></h4>
                    </div>
                    <div class="row">
                        <h4>Starts at: {!! $subscription->starts_at !!}</h4>
                    </div>
                    <div class="row">
                        <h4>Ends at: {!! $subscription->ends_at !!}</h4>
                    </div>
                </div>
            </div>
            <br>
            @php
                $upgrade = "upgrade";
                $downgrade = "downgrade";
                $subscribe = "subscribe";
                $subscribed_package;
                $annual = false;
                foreach($packages as $key => $package){
                    if($subscription){
                        if($package->id == $subscription->plan_id){
                            $subscribed_package = $package;
                            if($subscribed_package->invoice_interval == "year"){
                                $annual = true;
                            }
                        }
                    }
                }
            @endphp
            <div class="row m-0">
                <h4>Select plan</h4>
                <hr>
            </div>
            <div class="row mt-1 mb-1">
                <div class="col-12" style="text-align:center; vertical-align:middle;">
                    <div class="custom-control custom-switch custom-switch-success">
                        <span class="switch-label mr-1"><b>Monthly</b></span>
                        <input type="checkbox" class="custom-control-input" id="subscription_interval" name="subscription_interval" {{ ($annual == true)? "checked" : ""}}>
                        <label class="custom-control-label" for="subscription_interval"></label>
                        <span class="switch-label"><b>Yearly</b></span>
                    </div>
                </div>
            </div>
            <div class="row pricing-card">
                <div class="col-12 col-sm-offset-2 col-sm-10 col-md-12 col-lg-offset-2 col-lg-10 mx-auto" id="subscriptions_page">
                    <div class="row" id="subscriptions_show">
                        <!-- pricing plan cards -->
                        @foreach($packages as $key => $package)
                            @php
                                $features = $package->features;
                            @endphp
                            @if($package->slug == "user-trial")
                                @if($subscription && $subscribed_package->slug == "user-trial")
                                @else
                                    @continue
                                @endif
                            @endif
                            @if($package->invoice_interval == "month")
                                <div class="col-12 col-md-4 monthly_package" style="display:show;border: 2px solid black;">
                            @elseif($package->invoice_interval == "year")
                                <div class="col-12 col-md-4 yearly_package" style="display:none;border: 2px solid black;">
                            @else
                                <div class="col-12 col-md-4" style="border: 2px solid black;">
                            @endif
                            @php
                                if($package->price > $package->discount)
                                    $package_discount = $package->price - $package->discount;
                                else
                                         $package_discount = $package->price; 
                            @endphp
                                    <div class="card basic-pricing text-center">
                                        <div class="card-body">
                                            <h3>{!! $package->name !!}</h3>
                                            <p class="card-text">{!! $package->description !!}</p>
                                            <div class="annual-plan">
                                                <div class="plan-price mt-2">
                                            @if($package->discount != 0)
                                            <span class="font-medium-1 font-weight-bold text-primary">{!! $package->currency !!}</span>
                                            <span class="pricing-basic-value font-weight-bolder text-primary">{{ $package_discount }} / {{$package->invoice_period}} {{$package->invoice_interval}}</span><br>
                                            <s><span class="font-medium-1 font-weight-bold text-primary">{!! $package->currency !!}</span>
                                            <span class="pricing-basic-value font-weight-bolder text-primary">{!! $package->price !!} / {{$package->invoice_period}} {{$package->invoice_interval}}</span></s>
                                            
                                            @else
                                            <span class="font-medium-1 font-weight-bold text-primary">{!! $package->currency !!}</span>
                                            <span class="pricing-basic-value font-weight-bolder text-primary">{!! $package->price !!}</span>
                                            @endif
                                                </div>
                                                <small class="annual-pricing d-none text-muted"></small>
                                            </div>
                                            <ul class="list-group list-group-circle text-left">
                                                @foreach($features as $key => $feature)
                                                    @php
                                                        $feature_details = $feature->getFeatureDetails();
                                                    @endphp
                                                    <li class="list-group-item text-center">
                                                        @if($feature_details->name == 'catalogs' || $feature_details->name == 'products')
                                                        {!! $feature_details->name !!} - <b>{!! $feature->value !!}</b>
                                                        @else
                                                        {!! $feature_details->name !!} 
                                                        @endif

                                                        <i id="feature_description_show_{{$package->id}}_{{$feature->feature_id}}" class="fa fa-angle-double-down float-right" onclick="feature_description_show({{$package->id}},{{$feature->feature_id}})" style="display:block;cursor:pointer"></i>
                                                        <i id="feature_description_hide_{{$package->id}}_{{$feature->feature_id}}" class="fa fa-angle-double-up float-right" onclick="feature_description_hide({{$package->id}},{{$feature->feature_id}})" style="display:none;cursor:pointer"></i>
                                                        <li class="list-group-item text-center text-dark" id="feature_description_{{$package->id}}_{{$feature->feature_id}}" style="display:none">
                                                            {!! $feature_details->description !!}
                                                        </li>
                                                    </li>
                                                @endforeach
                                            </ul>
                                            @if($subscription && $subscribed_package->slug != "user-trial")
                                                @if($package->slug == $subscribed_package->slug)
                                                    @if($user->isSubscriptionExpired())
                                                        <button class="btn btn-block btn-outline-danger mt-2" onclick="adminSubscriptionModalShow($(this))" data-subscription="renew" data-plan_name="{{$package->name}}" data-plan_id="{{$package->id}}" data-url="{{ route('admin.adminSubscription.renewSubscription') }}">Renew</button>
                                                    @else
                                                        <button class="btn btn-block btn-outline-success mt-2">Current plan</button>
                                                    @endif
                                                @else
                                                    @if($package->price > $subscribed_package->price)
                                                        <button class="btn btn-block btn-outline-primary mt-2" onclick="adminSubscriptionModalShow($(this))" data-subscription="upgrade" data-plan_name="{{$package->name}}" data-plan_id="{{$package->id}}" data-url="{{ route('admin.adminSubscription.upgradeSubscription')}}">Upgrade</button>
                                                    @elseif($package->price < $subscribed_package->price)
                                                        <button class="btn btn-block btn-outline-secondary mt-2" onclick="adminSubscriptionModalShow($(this))" data-subscription="downgrade" data-plan_name="{{$package->name}}" data-plan_id="{{$package->id}}" data-url="{{ route('admin.adminSubscription.downgradeSubscription') }}">Downgrade</button>
                                                    @endif
                                                @endif
                                            @else
                                                @if($package->slug == "user-trial")
                                                    <button class="btn btn-block btn-outline-success mt-2">Current plan</button>
                                                @else
                                                    <button class="btn btn-block btn-outline-warning mt-2" onclick="adminSubscriptionModalShow($(this))" data-subscription="subscribe" data-plan_name="{{$package->name}}" data-plan_id="{{$package->id}}" data-url="{{ route('admin.adminSubscription.createSubscription')}}">Subscribe</button>
                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>
                        @endforeach
                        <!--/ pricing plan cards -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('modals.admin_subscription')
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-tagsinput/1.3.6/jquery.tagsinput.min.js"
        integrity="sha512-wTIaZJCW/mkalkyQnuSiBodnM5SRT8tXJ3LkIUA/3vBJ01vWe5Ene7Fynicupjt4xqxZKXA97VgNBHvIf5WTvg=="
        crossorigin="anonymous">
    </script>

    <script>
     if($('#subscription_interval').is(':checked') == true){
        $('.yearly_package').show();
        $('.monthly_package').hide();
    }
    else{
        $('.monthly_package').show();
        $('.yearly_package').hide();
    }

    $("#subscription_interval").change(function(){
        var x = $("#subscription_interval").is(":checked");
        if(this.checked){
            $('.yearly_package').show();
            $('.monthly_package').hide();
        }
        else{
            $('.monthly_package').show();
            $('.yearly_package').hide();
        }
    });

    function adminSubscriptionModalShow($this) {
        $("#adminSubscription").modal('show');
        subscribePlan($this);
    }

    function adminRenewalPayModalShow(){
        $("#adminRenewalPay").modal('show');
    }

    function feature_description_show(package_id,feature_id){
        $('#feature_description_show_' + package_id + '_' + feature_id).hide();
        $('#feature_description_hide_' + package_id + '_' + feature_id).show();
        $('#feature_description_' + package_id + '_' + feature_id).show();
    }

    function feature_description_hide(package_id,feature_id){
        $('#feature_description_show_' + package_id + '_' + feature_id).show();
        $('#feature_description_hide_' + package_id + '_' + feature_id).hide();
        $('#feature_description_' + package_id + '_' + feature_id).hide();
    }
    </script>
@endsection