@extends('layouts.admin')
@section('styles')
@parent
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/plugins/file-uploaders/dropzone.css')}}">
<style>
.select2 {
    width: 100% !important;
}
.dropzone .dz-message {
height: auto !important;
}
</style>
@endsection
@section('content')
<h4><a href="{{ route("admin.products.product_list") }}">
                        <i class="feather icon-arrow-left"></i> Back to Product List
                    </a></h4>
<div class="card">
    <div class="card-header h1">
        {{ trans('global.edit') }} {{ trans('cruds.product.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.products.update", [$product->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="row">
                <div class="col-md-12">
                    <h3> Product Images
                    <!-- <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#productUploadimgModal">Upload Images</button> -->
                    </h3>
                   <div class="form-group">
                            <div class="item-img text-center d-inline p-1" style="min-height: 12.85rem;">
                            @if(count($product->photo) > 0)
                                 <!--Carousel Wrapper-->
                                 <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails"  data-interval="false" data-ride="carousel">
                                    <!--Slides-->
                                    <div class="carousel-inner" role="listbox">
                                        @foreach($product->photo as $photo)
                                            <div class="carousel-item  {{$loop->index == 0?'active':''}}" style="max-height:250px;">
                                                <img class="img-fluid" style="max-width: 50%; max-height:250px;"src="{{$photo->getUrl() ?? asset('assets/images/placholder.png')}}">
                                            </div>
                                        @endforeach
                                    </div>
                                    <!--/.Slides-->
                                    <ol class="carousel-indicators position-relative m-0 mt-1 niceScroll" style="overflow:hidden;overflow-x:scroll;z-index:1;">
                                        @foreach($product->photo as $photo)
                                        <li data-target="#carousel-thumb" id="product_{{$photo->id}}" data-slide-to="{{$loop->index}}" class="{{$loop->index == 0?'active':''}}" style="min-width:90px;min-height:120px;">
                                            <div style="height:90px;object-fit: contain;">
                                            <img class="d-block img-fluid" style="min-width:90px; min-height:70px;max-height:80px;"
                                                src="{{$photo->getUrl() ?? asset('assets/images/placholder.png')}}">
                                            </div>
                                                <button href="#{{$photo->id}}" onclick="removeimg({{$photo->id}})" class="btn btn-danger btn-sm text-white"><i
                                                class="feather icon-trash-2 m-0"></i></button>
                                        </li>
                                        @endforeach
                                        <div class="ml-1" style="min-height:70px;max-height:120px;min-width: 70px;max-width:75px;text-align: center;border-style:dashed;" class="rounded mb-0" >
                                                <div class="image mt-2" >
                                                    <i data-toggle="modal" data-target="#productUploadimgModal" class="feather icon-plus-circle font-large-1 "></i> 
                                                </div>
                                                <span style="display: block;padding: 4px;">Upload Image</span>
                                            </div>
                                    </ol>
                                </div>
                                <!--/.Carousel Wrapper-->
                                @else 
                                    <div style="height:110px;max-width: 400px;border-style:dashed;position:relative;margin-left:30%;margin-right:30%;" class="rounded mb-0 d-block text-center" >
                                    <div class="image mt-2" >
                                                    <i data-toggle="modal" data-target="#productUploadimgModal" class="feather icon-plus-circle font-large-1 "></i> 
                                                </div>
                                                <span style="display: block;padding: 4px;">Upload Image</span>

                                    </div>
                                    <h1 class="p-4 text-light">No Product Images Uploaded</h1>
                            @endif
                        </div>
                    </div>
                </div>
            
            <div class="row">
                <div class="col-md-6">
                <div class="form-group">
        <label for="categories">{{ trans('cruds.product.fields.category') }}</label>
        
        <select class="form-control select2 {{ $errors->has('categories') ? 'is-invalid' : '' }}" name="categories[]" id="categories">
        <option value="">Choose Category</option>
            @foreach ($categories as $category)
                <option value="{{ $category->id }}" {{ (in_array($category->id, old('categories', [])) || $product->categories->contains($category->id)) ? 'selected' : '' }}>{{ $category->name }}</option>
                @if ($category->childrenCategories)
                    @foreach ($category->childrenCategories as $child_category)
                        @include('admin.products.childcategory', ['product' => $product,'child_category' => $child_category,'level'=>0])
                    @endforeach
                @endif
            @endforeach
        </select>
        @if($errors->has('categories'))
            <div class="invalid-feedback">
                {{ $errors->first('categories') }}
            </div>
        @endif
        <span class="help-block">{{ trans('cruds.product.fields.category_helper') }}</span>
    </div>
    <div class="row">
    <div class="col-md-6">
    <div class="form-group">
        <label for="product_category_id">Choose Parent Category</label>
        <select class="form-control select2 {{ $errors->has('product_category_id') ? 'is-invalid' : '' }}" name="product_category_id" id="product_category_id">
        <option value="">Choose Parent Category</option>
        @foreach ($categories as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
                @if ($category->childrenCategories)
                    @foreach ($category->childrenCategories as $child_category)
                        @include('admin.productCategories.childcategory', ['product' => $product,'child_category' => $child_category,'level'=>0])
                    @endforeach
                @endif
            @endforeach
        </select>
    </div>
</div> 
    <div class="col-md-6">
    <div class="form-group">
        <label for="product_category_name">Category Name</label>
        <input class="form-control" type="text" name="product_category_name" id="product_category_name">
    </div> 
    <div class="form-group">
                <button class="btn btn-info float-right btn-sm" type="button" onclick="createCategory()">
                    Create category
                </button>
    </div> 
</div>
</div>

<div class="form-group">
                <label for="tags">{{ trans('cruds.product.fields.tag') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('tags') ? 'is-invalid' : '' }}" name="tags[]" id="tags" multiple>
                    @foreach($tags as $id => $tag)
                        <option value="{{ $id }}" {{ (in_array($id, old('tags', [])) || $product->tags->contains($id)) ? 'selected' : '' }}>{{ $tag }}</option>
                    @endforeach
                </select>
                @if($errors->has('tags'))
                    <div class="invalid-feedback">
                        {{ $errors->first('tags') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.tag_helper') }}</span>
            </div>
            <!-- <div class="form-group">
                <label for="url">{{ trans('cruds.product.fields.url') }} (Buy / Order)</label>
                <input class="form-control {{ $errors->has('url') ? 'is-invalid' : '' }}" type="text" name="url" id="url" value="{{ old('url', $product->url) }}">
                @if($errors->has('url'))
                    <div class="invalid-feedback">
                        {{ $errors->first('url') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.url_helper') }}</span>
            </div> -->
            
           

                </div>
                <div class="col-md-6">
                <div class="form-group">
                <label for="sku">{{ trans('cruds.product.fields.sku') }}</label>
                <input class="form-control {{ $errors->has('sku') ? 'is-invalid' : '' }}" type="text" name="sku" id="sku" value="{{ old('sku', $product->sku) }}">
                @if($errors->has('sku'))
                    <div class="invalid-feedback">
                        {{ $errors->first('sku') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.sku_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="name">{{ trans('cruds.product.fields.name') }}</label>
                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="name" id="name" value="{{ old('name', $product->name) }}">
                @if($errors->has('name'))
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="price">{{ trans('cruds.product.fields.price') }}</label>
                <input class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}" type="number" name="price" id="price" value="{{ old('price', $product->price) }}" step="0.01">
                @if($errors->has('price'))
                    <div class="invalid-feedback">
                        {{ $errors->first('price') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.price_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="discount">{{ trans('cruds.product.fields.discount') }}</label>
                <input class="form-control {{ $errors->has('discount') ? 'is-invalid' : '' }}" type="text" name="discount" id="discount" value="{{ old('discount', $product->discount) }}">
                @if($errors->has('discount'))
                    <div class="invalid-feedback">
                        {{ $errors->first('discount') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.discount_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="stock">Stock Quantity</label>
                <input class="form-control" type="text" name="stock" id="stock" value="{{ old('stock', $product->stock) }}">
            </div>
            

                </div>
            <div class="col-md-12">    
<div class="form-group">
                <label for="description">{{ trans('cruds.product.fields.description') }}</label>
                <textarea class="form-control pb-1 {{ $errors->has('description') ? 'is-invalid' : '' }} hidden" rows="5" name="description" id="description">{{ old('description', $product->description) }}</textarea>
                <div id="editor-container" style="height: 250px;"></div>
                @if($errors->has('description'))
                    <div class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.product.fields.description_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger float-right" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
</div>

            </div>

            <!-- Product upload Modal -->
            <div class="modal fade ecommerce-application" id="productUploadimgModal" tabindex="-1" role="dialog"
    aria-labelledby="productSelectModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="productSelectModalTitle">Product Upload Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="dropzone dropzone-area {{ $errors->has('photo') ? 'is-invalid' : '' }}"
                        id="productUploadimg">
                        <div class="dz-message">Upload Product Image</div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"
                        aria-label="Close">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>

        </div>
</div>

            <!-- End Product Upload Modal -->
            
            <!-- AR upload Modal -->
            <div class="modal fade ecommerce-application" id="arUploadimgModal" tabindex="-1" role="dialog"
    aria-labelledby="productSelectModalTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="productSelectModalTitle">Tryon Upload Images</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <div class="form-group">
                        <label class="required" for="arUploadimg">Tryon Image</label>
                        <div class="dropzone dropzone-area {{ $errors->has('arUploadimg') ? 'is-invalid' : '' }}" id="arUploadimg" style="min-height: 305px;">
                            <div class="dz-message">Upload Tryon Image</div>    
                        </div>
                        @if($errors->has('arUploadimg'))
                            <div class="invalid-feedback">
                                {{ $errors->first('arUploadimg') }}
                            </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="position">Position</label>
                        <select class="form-control" name="position" id="position">
                        @foreach(App\Arobject::POSITION_SELECT as $key => $label)
                            <option value="{{ $key }}">{{ $label }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"
                        aria-label="Close">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </div>

        </div>
</div>

            <!-- End Product Upload Modal -->
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    $('.arobjectPosition').editable({
        source: [
            @foreach(App\Arobject::POSITION_SELECT as $key => $label)
            {
            value: '{{$key}}',
            text: '{{$label}}'
            },
            @endforeach
        ]
    });
    function removeimg(id){
        if(confirm('{{ trans('global.areYouSure') }}')) {
            var url = "{{route('admin.products.removeimg')}}";
            // url = url.replace(':id', id);
            $.ajax({
                url: url,
                type: "post",
                data: {'id': id},
            }).done(function (data) {
                alert('Removed image');
                location.reload();
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                
                // alert('No response from server');
            });
        }
    }

    function removetryonimg(id){
        if(confirm('{{ trans('global.areYouSure') }}')) {
            var url = "{{route('admin.arobjects.destroy',':id')}}";
            url = url.replace(':id', id);
            $.ajax({
                url: url,
                type: "post",
                data: {'_method': 'DELETE'},
            }).done(function (data) {
                alert('Removed image');
                location.reload();
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                
                // alert('No response from server');
            });
        }
    }

    function createCategory(){
        var url = "{{route('admin.product-categories.ajaxAddCategory')}}";
        // url = url.replace(':id', id);
        var name = $('#product_category_name').val();
        var id = $('#product_category_id').val();
        // console.log("add category",id);
        if(name != undefined && name != "") {
            $.ajax({
                url: url,
                type: "post",
                data: {'name': name,'product_category_id': id},
            }).done(function (data) {
                alert('added category');
                $('#categories').append("<option value='"+data['data']['id']+"' selected>"+data['data']['name']+"</option>")
                $('#product_category_id').append("<option value='"+data['data']['id']+"'>"+data['data']['name']+"</option>")
            });
        } else {
            alert('Fill category name');
        }
        
    }

    var quill = new Quill('#editor-container', {
  modules: {
    'toolbar': [
        [{
          'font': []
        }, {
          'size': []
        }],
        ['bold', 'italic', 'underline', 'strike'],
        [{
          'color': []
        }, {
          'background': []
        }],
        [{
          'script': 'super'
        }, {
          'script': 'sub'
        }],
        [{
          'header': '1'
        }, {
          'header': '2'
        }, 'blockquote', 'code-block'],
        [{
          'list': 'ordered'
        }, {
          'list': 'bullet'
        }, {
          'indent': '-1'
        }, {
          'indent': '+1'
        }],
        ['direction', {
          'align': []
        }],
        ['link', 'image', 'video', 'formula'],
        ['clean']
      ],
  },
  placeholder: 'description...',
  theme: 'snow'
});
quill.root.innerHTML = $("#description").val();
$("form").submit(function() {
        $("#description").val(quill.root.innerHTML);
        $(form).serializeArray();
    });
        
</script>
<script>
    var uploadedPhotoMap = {}
      Dropzone.autoDiscover = false;
    myProductDropzone = new Dropzone('#productUploadimg', {
        url: '{{ route('admin.products.storeMedia') }}',
    maxFilesize: 40, // MB
    // maxFiles: 1,
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 40
    },
    success: function (file, response) {
      $('form').append('<input type="hidden" name="photo[]" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
            var name = ''
            if (typeof file.file_name !== 'undefined') {
                name = file.file_name
            } else {
                name = uploadedPhotoMap[file.name]
            }
            $('form').find('input[name="photo[]"][value="' + name + '"]').remove()
    },
    init: function () {
      
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
    
});

myDropzone = new Dropzone('#arUploadimg', {
        url: '{{ route('admin.arobjects.storeMedia') }}',
    maxFilesize: 40, // MB
    maxFiles: 1,
    acceptedFiles: '.jpeg,.jpg,.png,.gif',
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 40
    },
    success: function (file, response) {
      $('form').find('input[name="object"]').remove()
      $('form').append('<input type="hidden" name="object" value="' + response.name + '">')
    },
    removedfile: function (file) {
      file.previewElement.remove()
      if (file.status !== 'error') {
        $('form').find('input[name="object"]').remove()
        this.options.maxFiles = this.options.maxFiles + 1
      }
    },
    init: function () {
      
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
    
});


</script>
@endsection