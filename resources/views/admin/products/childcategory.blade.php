<option value="{{ $child_category->id }}" {{  (in_array($child_category->id, old('categories', [])) || $product->categories->contains($child_category->id)) ? 'selected' : '' }}>
@for ($i = 0; $i <= $level; $i++)            
        &nbsp;&nbsp;
@endfor    
{{ $child_category->name }}</option>
@if ($child_category->categories)
        @foreach ($child_category->categories as $childCategory)
            @include('admin.products.childcategory', ['child_category' => $childCategory,'level'=>($level + 1)])
        @endforeach
@endif