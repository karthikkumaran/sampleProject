@extends('layouts.admin')
@section('styles')
<style>
    .form-toggle input[type="checkbox"]:checked+.form-toggle__item,
    .form-toggle input[type="checkbox"]:checked+* .form-toggle__item,
    .form-toggle input[type="radio"]:checked+.form-toggle__item,
    .form-toggle input[type="radio"]:checked+* .form-toggle__item {
        background-color: #0dd157;
        border-color: #0dd157;
    }

    svg {
        width: 80px;
        height: 80px;
        fill: red;
    }

    .form-toggle__item {
        width: 53px;
        height: 24px;
    }

    /* svg:hover {
        fill: red;
        } */

</style>
@endsection
@section('content')
@livewire('admin.maintenance-mode')
@endsection
<script>
function copyToClipboard(element) {
  var $temp = $("<input>");
  $(".card-body").append($temp);
  $temp.val($(element).text());
  $temp.select();
  document.execCommand("copy");
  $temp.remove();
  toastr.success('Link', 'Copied!');
}
</script>