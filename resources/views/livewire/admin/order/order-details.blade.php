<div>
    <div class="content-header row">
    <div class="content-header-left col-md-9 col-12 mb-2">
        <div class="row breadcrumbs-top">
            <div class="col-12">
                <a href="{{url()->previous()}}">
                    <h4 class="text-primary"><i class="feather icon-arrow-left"></i> Back</h4>
                </a>
            </div>
        </div>
    </div>
    </div>
    <div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4 text-left">
                        <h3>Order Details</h3>
                        <h4><b>Order #{{$customer->order->order_ref_id}}</b></h4>
                        <h4>No. Products : <b>{{ count($customer->orderProducts) }}</b></h4>
                        @php
                        $cmeta_data = json_decode($customer->meta_data, true);
                        @endphp
                        @if(isset($customer->order->transactions->amount))
                        <h4> Total Price : <b>{!! $cmeta_data['currency'] !!}{{ $customer->order->transactions->amount }}</b></h4>
                        @endif
                        <!-- <h4> Total Price : <b>{!! $cmeta_data['currency'] !!}{{ $cmeta_data['product_total_price'] }}</b></h4> -->
                    </div>
                    <div class="col-md-4 text-md-center p-1">
                        <a href="https://wa.me/{{$customer->country_code ? $customer->country_code.$customer->mobileno : '91'.$customer->mobileno}}" class="mr-1 mb-1" target="_blank">
                            <i style="color:#25D366" class="fa fa-whatsapp fa-2x"></i></a>
                        <a href="tel:{{$customer->mobileno}}" class="mr-1 mb-1">
                            <i style="color:#2F91F3" class="fa fa-phone fa-2x"></i></a>
                        <a href="{{ route('admin.export_order_pdf', $customer->order_id) }}" class="mr-1 mb-1">
                            <i style="color:red" class="fa fa-file-pdf-o fa-2x"></i></a>
                    </div>
                    <div class="col-md-4 text-md-right ">
                        @if($customer->order->order_mode)
                            <h4>Order mode: <b>{{$customer->order->order_mode=="Online-Storefront" ? "Online" : $customer->order->order_mode}}</b></h4>
                        @endif
                        <span style="font-size:1.5rem;">Order date: </span><b>{{date('d-m-Y h:i:s A', strtotime($customer->created_at))}}</b>

                        @if($customer->order->notes)
                        <div style="border: 1px solid black;border-radius:5px;padding: 5px;background: #e7e9ec;">
                            <h4>Notes : </h4>
                            <p>{!! $customer->order->notes !!}</p>
                        </div>
                        @endif
                        @if(isset($user_meta_data['shipping_settings']['shipping_method']) && !empty($user_meta_data['shipping_settings']['shipping']) && count($trackingdata) == 0 && !empty($cmeta_data['Shyplite_price']))
                        <button class="btn btn-primary" data-toggle="modal" data-target="#shyplite_modal" >order shyplite shipping</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div id="shyplite_modal" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Shyplite Order Booking</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form name="ordersyplite" method="POST" id="ordersyplite">
                <div class="modal-body">
                    <div class="card-body">
                        <div class="form-group">
                            <div class="row">
                                    <div class="col-4 align-center text-right">
                                        <label for="order_id">Order ID:</label>
                                    </div>
                                    <div class="col-6 text-left">
                                        <input type="text" class="form-control" id="order_id"  value="{{$customer->order->order_ref_id}}" required/><br>
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-4 align-center text-right">
                                    <label for="order_type">Type:</label>
                                </div>
                                <div class="col-6 text-left">
                                <select id="order_type"><option>Prepaid</option></select><br><br>
                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-4 align-center text-right">
                                        <label for="mode_type">Mode Type:</label>
                                    </div>
                                    <div class="col-6 text-left">
                                        <select id="mode_type"><option>Air</option></select><br><br>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-4 align-center text-right">
                                        <label for="package_length">Package Length:</label>
                                    </div>
                                    <div class="col-6 text-left">
                                        <input type="number" id="package_length" required><br><br>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-4 align-center text-right">
                                        <label for="package_width">Package Width:</label>
                                    </div>
                                    <div class="col-6 text-left">
                                        <input type="number" id="package_width" required><br><br>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-4 align-center text-right">
                                        <label for="package_height">Package Height:</label>
                                    </div>
                                    <div class="col-6 text-left">
                                        <input type="number" id="package_height" required><br><br>
                                    </div>
                            </div>
                            <div class="row">
                                    <div class="col-4 align-center text-right">
                                        <label for="package_weight">Package weight:</label>
                                    </div>
                                    <div class="col-6 text-left">
                                        <input type="number" id="package_weight" required><br><br>
                                    </div>
                            </div>
                            <h5> Seller address information :</h5>
                                <div class="form-group">
                                    <center>
                                        <div class="row">
                                            <div class="col-4 align-center text-right">
                                                <label for="seller_address">Address name:</label>
                                            </div>
                                            <div class="col-8 text-left">
                                                <select id="seller_address"><option>AS01</option></select><br>
                                            </div>
                                        </div>
                                    </center>
                                </div>
                                <h5>Customer information :</h5>
                                <div class="form-group">
                                    <div class="row">
                                            <div class="col-4 align-center text-right">
                                                <label for="customer_name">Name</label>
                                            </div>
                                            <div class="col-6 text-left">
                                                <input type="text" class="form-control" id="customer_name"  value="{{$customer->fname ?? '' }}"><br>
                                            </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-4 align-center text-right">
                                                <label for="customer_address">Full address</label>
                                            </div>
                                            <div class="col-6 text-left">
                                                <textarea id="customer_address" rows="8" cols="30" >
                                                    {{$customer->address1 ?? "" }}
                                                    {{$customer->address2 ?? "" }}
                                                    {{$customer->city ?? "" }}
                                                    {{$customer->pincode ?? "" }}
                                                    {{$customer->state ?? "" }}
                                                    {{$customer->country ?? "" }}
                                                </textarea><br>
                                            </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-4 align-center text-right">
                                                <label for="contact_number">Contact NO:</label>
                                            </div>
                                            <div class="col-6 text-left">
                                                <input type="number" class="form-control" id="contact_number"  value="{{ $customer->mobileno ?? '' }}"><br>
                                            </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-4 align-center text-right">
                                                <label for="city">City</label>
                                            </div>
                                            <div class="col-6 text-left">
                                                <input type="text" class="form-control" id="city"  value="{{$customer->city ?? '' }}"><br>
                                            </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-4 align-center text-right">
                                                <label for="pin">pincode</label>
                                            </div>
                                            <div class="col-6 text-left">
                                                <input type="text" class="form-control" id="pin"  value="{{$customer->pincode ?? '' }}"><br>
                                            </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit"  data-modal_name="shyplite_modal" id="ordersubmit" class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>

     @php
         $statusvalue =  App\orders::STATUS[$customer->order->status] ;
     @endphp

    <div class="card ">
        <div class="card-body">
            <div class=" mt-2 mb-1">
                <div class="row">
                    <div class="col-md-6">
                        <!-- <div class="bg-white p-2 border rounded px-3"> -->
                            <div class="d-flex flex-row justify-content-between align-items-center order">
                                <div class="d-flex flex-column order-details"><h3>Order Status</h3></div>

                            </div>
                    </div>

                    <div class="col-md-6 pr-3 text-right">
                    <h4>Change order status:</h4>
                        <div class="row justify-content-end pb-1">
                            <select class="form-control" style="width:200px;" name="order_status" id='order_status' >
                                @foreach(App\orders::STATUS as $key => $label)
                                @if($key >= $customer->order->status)
                                <option value="{{ $key }}" {{ $customer->order->status ==  $key ? 'selected' : '' }} >{{$label}}</option>
                                @endif
                                @endforeach
                            </select>
                            <input type="hidden" id="order_status_previous" value= {{ $customer->order->status }} />
                        </div>
                    </div>
                </div>
            </div>
            <div class=" mt-1 mb-1">
                <div class="row">
                    <div class="col-md-12">
                            <!-- <hr class="divider mb-4"> -->
                            <div class="d-flex flex-row justify-content-between align-items-center">

                                @foreach($orderstatus as $key => $value)
                                    <div class="d-flex flex-column align-items-start">{{App\orders::STATUS[$value->status]}}</div>
                                @endforeach

                                @if($customer->order->status == 5 && empty(App\orders::STATUS[$value->status]))
                                <div class="d-flex flex-column align-items-start">{{App\orders::STATUS[5]}}</div>
                                @elseif($customer->order->status == 6 && empty(App\orders::STATUS[$value->status]))
                                <div class="d-flex flex-column align-items-start">{{App\orders::STATUS[6]}}</div>
                                @elseif($customer->order->status == 7 && empty(App\orders::STATUS[$value->status]))
                                <div class="d-flex flex-column align-items-start">{{App\orders::STATUS[7]}}</div>
                                @else
                                    @foreach(App\orders::STATUS as $key => $label)
                                        @if($key > $customer->order->status && $key <= 4)
                                        <div class="d-flex flex-column align-items-start">{{ $label }}</div>
                                        @endif
                                    @endforeach
                                @endif
                                <!-- <div class="d-flex flex-column justify-content-center"><span>Pending</span></div>
                                <div class="d-flex flex-column justify-content-center align-items-center"><span>Accepted</span></div>
                                <div class="d-flex flex-column align-items-center"><span>Shipping</span></div>
                                <div class="d-flex flex-column align-items-end"><span>Delivered</span></div> -->
                            </div>
                            <div class="d-flex flex-row justify-content-between align-items-center">

                                @foreach($orderstatus as $key => $value)
                                    @if($key == 0)
                                        @if($key != count($orderstatus)-1)
                                            <span class="dot cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')"></span>
                                        @else
                                            <span class="d-flex justify-content-center align-items-center big-dot dot cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')"><i class="fa fa-check text-white"></i></span>
                                        @endif
                                    @elseif($key != count($orderstatus)-1)
                                        @if($value->status < 5)
                                        <hr class="flex-fill track-line align-items-start"><span class="dot cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')"></span>
                                        @else
                                        <hr class="flex-fill failtrack-line align-items-start"><span class="d-flex justify-content-center align-items-center red-dot cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')"><i class="fa fa-times text-white"></i></span>
                                        @endif

                                    @else
                                        @if($customer->order->status < 5)
                                            <hr class="flex-fill track-line"><span class="d-flex justify-content-center align-items-center big-dot dot cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')"><i class="fa fa-check text-white"></i></span>
                                        @else
                                            <hr class="flex-fill failtrack-line align-items-start"><span class="d-flex justify-content-center align-items-center red-dot cursor-pointer" onmouseover="show('{{ $value }}','{{App\orders::STATUS[$value->status]}}')"><i class="fa fa-times text-white"></i></span>
                                        @endif
                                    @endif
                                @endforeach
                                @foreach(App\orders::STATUS as $key => $label)
                                    @if($key > $customer->order->status && $key <= 4)
                                    <hr class="flex-fill untrack-line align-items-start"><span class="white-dot"></span></span>
                                    @endif
                                @endforeach

                                @php
                                     $statusvalue =  App\orders::STATUS[$customer->order->status] ;
                                 @endphp

                                <!-- <hr class="flex-fill track-line"><span class="dot"></span>
                                <hr class="flex-fill track-line"><span class="dot"></span>
                                <hr class="flex-fill track-line"><span class="d-flex justify-content-center align-items-center big-dot dot"><i class="fa fa-check text-white"></i></span> -->
                                </div>
                            <div class="d-flex flex-row justify-content-between align-items-center">
                                @foreach($orderstatus as $key => $value)
                                <div class="d-flex flex-column align-items-start">
                                    @php
                                        $s= $value->created_at ;
                                        $date = $s->format('m/d/Y');
                                        $time = date("h:i:s A",strtotime($s));
                                    @endphp
                                    {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                    <br>
                                    {{ $time }}
                                </div>
                                @endforeach
                                @if($customer->order->status == 5 && empty(App\orders::STATUS[$value->status]))
                                        @php
                                             $s= $value->created_at ;
                                             $date = $s->format('m/d/Y');
                                             $time = date("h:i:s A",strtotime($s));
                                         @endphp
                                    {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                    <br>
                                    {{ $time }}
                                @elseif($customer->order->status == 6 && empty(App\orders::STATUS[$value->status]))
                                        @php
                                             $s= $value->created_at ;
                                             $date = $s->format('m/d/Y');
                                             $time = date("h:i:s A",strtotime($s));
                                         @endphp
                                    {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                    <br>
                                    {{ $time }}
                                    @elseif($customer->order->status == 7 && empty(App\orders::STATUS[$value->status]))
                                        @php
                                             $s= $value->created_at ;
                                             $date = $s->format('m/d/Y');
                                             $time = date("h:i:s A",strtotime($s));
                                         @endphp
                                    {{ \Carbon\Carbon::parse($date)->isoFormat('ddd,Do MMM  YYYY')}}
                                    <br>
                                    {{ $time }}
                                    @else
                                        @foreach(App\orders::STATUS as $key => $label)
                                        @if($key > $customer->order->status && $key <= 4)
                                        <span>-</span>
                                        @endif
                                        @endforeach
                                @endif
                                </div>

                                <!-- <div class="d-flex flex-column justify-content-center"><span>15 Mar</span><span>Order placed</span></div>
                                <div class="d-flex flex-column justify-content-center align-items-center"><span>15 Mar</span><span>Order Dispatched</span></div>
                                <div class="d-flex flex-column align-items-center"><span>15 Mar</span><span>Out for delivery</span></div>
                                <div class="d-flex flex-column align-items-end"><span>15 Mar</span><span>Delivered</span></div> -->
                            </div>
                        <!-- </div> -->
                    </div>
                </div>

                <div class=" mt-1 mb-2">
                    <div class="row">
                        <div class="col-md-12">
                            <hr class="divider mb-2" style="color:black;">

                            <div class="text-left">

                            @foreach($orderstatus as $key => $value)
                                <div>
                                @if($value->status == $customer->order->status)
                                        <div id='showremarks'><b>{{ App\orders::STATUS[$value->status] }}: </b>
                                        </div>
                                @endif
                                </div>
                            @endforeach
                             <div>

                             @foreach($orderstatusremark as $key => $value)
                                @if($value->status == $customer->order->status)
                                    <div class="remark remarks-{{$value->status}}">
                                        <p> {{$value->remarks}} on {{$value->created_at}}</p>
                                    </div>
                                @else
                                    <div class="remark remarks-{{$value->status}}" style="display: none">
                                        <p> {{$value->remarks}} on {{$value->created_at}}</p>
                                    </div>
                                @endif
                             @endforeach
                             <p><button class="btn btn-info addComment" data-status={{$customer->order->status}}>Add Comments</button></p>
                            </div>
                            </div>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @if(!empty($transaction))
    <div class="col-md-12 p-0">
        <section id="collapsible">
            <div class="collapse-default collapse-icon">
                <div class="card ">
                    <div id="headingCollapse4" class="card-header pb-1 cursor-pointer" data-toggle="collapse" role="button" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">

                        <label class="h3" >Transaction Details<span class="lead collapse-title text-dark h3"> - #{{$transaction->transaction_id}}</span></label>
                    </div>
                    <div id="collapse4" role="tabpanel" aria-labelledby="headingCollapse4" class="collapse" aria-expanded="false">
                        <div class="card-body">
                            <div class="row">
                                <div class="col col-md-4 order-1 cursor-pointer">
                                    <h5>Mode: <b>{{$transaction->payment_mode}}</b></h5>
                                    <h5>{{$transaction->payment_method}}</h5>

                                    @if($transaction->payment_mode == "UPI" && !empty($transaction->photo->getUrl('thumb')))
                                    <img id="upi-ss" class="img-fluid p-1" style="max-width: 150px; max-height:150px;" src="{{$transaction->photo->getUrl('thumb')}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                    @endif
                                </div>
                                <div class="col col-md-4 order-2 d-flex flex-md-row flex-column justify-content-md-center">

                                    <h6 href="#" class="mr-1 mb-1">Order #{{$transaction->order_id}}</h6>

                                </div>
                                <div class="col col-md-4 order-3 text-md-right cursor-pointer">

                                    <h5>{{ date('d-m-Y', strtotime($transaction->created_at)) }}</h5>
                                    <h5>{{ date('h:i:s A', strtotime($transaction->created_at)) }}</h5>

                                    <h5><b>{!! $transaction['amount']==0?" ":$cmeta_data['currency'].''.$transaction['amount'] !!}</b></h5>

                                    <h4>Status: <b>{{$transaction->status}}</b></h4>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @endif
    <!-- && count($trackingdata) > 0 -->
@if(!empty($cmeta_data['Shyplite_price']) && count($trackingdata) > 0)
    <div class="col-md-12 p-0">
        <section id="collapsible">
            <div class="collapse-default collapse-icon">
                <div class="card ">
                    <div id="headingCollapse5" class="card-header pb-1 cursor-pointer" data-toggle="collapse" role="button" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">

                        <label class="h3" >Shipping Details<span class="lead collapse-title text-dark h3"> - #</span></label>
                    </div>
                    <div id="collapse5" role="tabpanel" aria-labelledby="headingCollapse5" class="collapse" aria-expanded="false">
                        <div class="card-body">
                                <!-- <form method="POST" id="formsplashscreen" action="" enctype="multipart/form-data">

                                <h5> Order information :</h5>
                                <div class="form-group">
                                    <div class="row">
                                            <div class="col-4 align-center text-right">
                                                <label for="order_id">Order ID:</label>
                                            </div>
                                            <div class="col-6 text-left">
                                                <input type="text" class="form-control" id="order_id"  value="{{$customer->order->id}}"/><br>
                                            </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-4 align-center text-right">
                                                <label for="order_date">Order Date:</label>
                                            </div>
                                            <div class="col-6 text-left">
                                                <input type="text" class="form-control" id="order_date"  value="{{date('d-m-Y', strtotime($customer->created_at))}}"><br>
                                            </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-4 align-center text-right">
                                                <label for="order_type">Type:</label>
                                            </div>
                                            <div class="col-6 text-left">
                                                <input type="text" class="form-control" id="order_type"  value=""><br>
                                            </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-4 align-center text-right">
                                                <label for="mode_type">Mode Type:</label>
                                            </div>
                                            <div class="col-6 text-left">
                                                <select><option>Air</option></select><br><br>
                                            </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-4 align-center text-right">
                                                <label for="total_value">Total value</label>
                                            </div>
                                            <div class="col-6 text-left">
                                                <input type="text" class="form-control" id="total_value"  value=""><br>
                                            </div>
                                    </div>

                                </div>

                                <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                                <hr> -->
                                <div class="float-right"><button class="btn btn-primary btn-sm" id="track">Again Tracking</button></div>
                                <h4>Shipment Tracking :</h4>

                                <div class="d-flex flex-row justify-content-between align-items-center" >
                                @foreach($trackingdata as $key => $value)
                                    @if(!empty($value->eventstatus))
                                    <div class="d-flex flex-column align-items-start" >
                                            @if($value->eventstatus == 'SB')
                                            <b>Shipment Booked</b><br>
                                            @elseif($value->eventstatus == 'PU')
                                            <b>Picked Up</b><br>
                                            @elseif($value->eventstatus == 'IT')
                                            <b>In Transit</b><br>
                                            @elseif($value->eventstatus == 'DY')
                                            <b>Delay</b><br>
                                            @elseif($value->eventstatus == 'EX')
                                            <b>Exception</b><br>
                                            @elseif($value->eventstatus == 'OD')
                                            <b>Out for Delivery</b><br>
                                            @elseif($value->eventstatus == 'RT')
                                            <b>Return</b><br>
                                            @elseif($value->eventstatus == 'DL')
                                            <b>Delivered</b><br>
                                            @endif
                                    </div>
                                    @endif
                                @endforeach
                                </div>
                                @php
                                    $event = 0;
                                    foreach($trackingdata as $key => $value){
                                        if(!empty($value->eventstatus)){
                                            $event =  1;
                                        }
                                    }
                                @endphp
                                    @if($event == 0)
                                        <center><h3> No Tracking Event for this order </h3></center>
                                    @endif
                                <div class="d-flex flex-row justify-content-between align-items-center">

                                @foreach($trackingdata as $key => $value)
                                    @if(!empty($value->eventstatus))
                                        @if($key == 0)
                                            @if($key != count($trackingdata)-1)
                                                <span class="dot cursor-pointer" onmouseover="showtrack('{{ $key }}')"></span>
                                            @else
                                                <span class="d-flex justify-content-center align-items-center big-dot dot cursor-pointer" onmouseover="showtrack('{{ $key }}')"><i class="fa fa-check text-white"></i></span>
                                            @endif
                                        @elseif($key != count($trackingdata)-1)
                                            @if(!empty($trackingdata[0]->eventstatus))
                                                <hr class="flex-fill track-line align-items-start"><span class="dot cursor-pointer" onmouseover="showtrack('{{ $key }}')"></span>
                                            @else
                                                <span class="dot cursor-pointer" onmouseover="showtrack('{{ $key }}')"></span>
                                            @endif
                                        @elseif($key == count($trackingdata)-1)
                                            @if(!empty($trackingdata[0]->eventstatus))
                                                <hr class="flex-fill track-line align-items-start"><span class="d-flex justify-content-center align-items-center big-dot dot cursor-pointer" onmouseover="showtrack('{{ $key }}')"><i class="fa fa-check text-white"></i></span>
                                            @else
                                            <span class="d-flex justify-content-center align-items-center big-dot dot cursor-pointer" onmouseover="showtrack('{{ $key }}')"><i class="fa fa-check text-white"></i></span>
                                            @endif
                                        @endif
                                    @endif
                                @endforeach
                                </div>
                                <div class="d-flex flex-row justify-content-between align-items-center">
                                @foreach($trackingdata as $key => $value)
                                    @if(!empty($value->eventstatus))
                                        <b> {{ \Carbon\Carbon::parse($value->eventtime)}}</b>
                                    @endif
                                @endforeach
                                </div>
                                <hr class="divider mb-2" style="color:black;">

                            <div class="text-left">
                            @foreach($trackingdata as $key => $value)
                                    @if($key == count($trackingdata)-1)
                                        <div class='lastkey'><h4>{{$value->eventstatus}}: </h4>
                                        @if($value->eventstatus == 'SB')
                                        <b>Status:</b>Shipment Booked<br>
                                        @elseif($value->eventstatus == 'PU')
                                        <b>Status:</b>Picked Up<br>
                                        @elseif($value->eventstatus == 'IT')
                                        <b>Status:</b>In Transit<br>
                                        @elseif($value->eventstatus == 'DY')
                                        <b>Status:</b>Delay<br>
                                        @elseif($value->eventstatus == 'EX')
                                        <b>Status:</b>Exception<br>
                                        @elseif($value->eventstatus == 'OD')
                                        <b>Status:</b>Out for Delivery<br>
                                        @elseif($value->eventstatus == 'RT')
                                        <b>Status:</b>Return<br>
                                        @elseif($value->eventstatus == 'DL')
                                        <b>Status:</b>Delivered<br>
                                        @endif
                                        <b>AWB ID:</b>{{$value->awb_id}}<br>
                                        <b>AWB NO:</b>{{$value->awb_no}}<br>
                                        <b>Carrier Name:</b>{{$value->carrier_name}}<br>
                                        <b>Remarks:</b>{{$value->remark}}<br>
                                        <b>Location:</b>{{$value->location}}<br>
                                        <b>Time:</b>{{ \Carbon\Carbon::parse($value->eventtime)}}
                                        </div>
                                    @endif
                            @endforeach
                            @foreach($trackingdata as $key => $value)
                                        <div class='trackvalue_{{$key}}' style="display:none"><h4>{{$value->eventstatus}}: </h4>
                                        @if($value->eventstatus == 'SB')
                                        <b>Status:</b>Shipment Booked<br>
                                        @elseif($value->eventstatus == 'PU')
                                        <b>Status:</b>Picked Up<br>
                                        @elseif($value->eventstatus == 'IT')
                                        <b>Status:</b>In Transit<br>
                                        @elseif($value->eventstatus == 'DY')
                                        <b>Status:</b>Delay<br>
                                        @elseif($value->eventstatus == 'EX')
                                        <b>Status:</b>Exception<br>
                                        @elseif($value->eventstatus == 'OD')
                                        <b>Status:</b>Out for Delivery<br>
                                        @elseif($value->eventstatus == 'RT')
                                        <b>Status:</b>Return<br>
                                        @elseif($value->eventstatus == 'DL')
                                        <b>Status:</b>Delivered<br>
                                        @endif
                                        <b>AWB ID:</b>{{$value->awb_id}}<br>
                                        <b>AWB NO:</b>{{$value->awb_no}}<br>
                                        <b>Carrier Name:</b>{{$value->carrier_name}}<br>
                                        <b>Remarks:</b>{{$value->awb_id}}<br>
                                        <b>Location:</b>{{$value->location}}<br>
                                        <b>Time:</b>{{ \Carbon\Carbon::parse($value->eventtime)}}
                                        </div>
                            @endforeach
                             <div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
@endif

    <div class="col-md-12 p-0">
        <div class="card ">
            <div class="card-header">
                <h3>Customer Details</h3>
            </div>
            <div class="card-body">
                <div class="row no-gutters">
                    <div class="col-md-6  order-1 no-gutters">
                        <div class="row no-gutters">
                            <div class="col col-md-2 no-gutters">
                                Name
                            </div>
                            <div class="col col-md-5 no-gutters">
                                <b> {{$customer->fname ?? "" }}</b>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col col-md-2  no-gutters">
                                Address
                            </div>
                            <div class="col col-md-5 no-gutters">
                                <b>
                                    {{$customer->address1 ?? "" }}<br>
                                    {{$customer->address2 ?? "" }}<br>
                                    {{$customer->city?? "" }}<br>
                                    {{$customer->pincode?? "" }}<br>
                                    {{$customer->state ?? "" }}<br>
                                    {{$customer->country ?? "" }}<br>
                                </b>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 order-2 no-gutters">
                        <div class="row no-gutters">
                            <div class="col col-md-5 no-gutters">
                                Email
                            </div>
                            <div class="col-6 col-md-6 no-gutters">
                                <b>{{ $customer->email ?? "" }}</b>
                            </div>
                        </div>
                        <div class="row no-gutters">
                            <div class="col col-md-5 no-gutters">
                                Mobile No
                            </div>
                            <div class="col col-md-5 no-gutters">
                                <b>{{ $customer->mobileno ?? "" }}</b>
                            </div>
                        </div>
                        @if(!empty($customer->amobileno))
                        <div class="row no-gutters">
                            <div class="col col-md-5 no-gutters">
                                Alternative Mobile No
                            </div>
                            <div class="col-6 col-md-5 no-gutters">
                                <b>{{ $customer->amobileno ?? "" }}</b>
                            </div>
                        </div>
                        @endif
                        <div class="row no-gutters">
                            <div class="col col-md-5 no-gutters">
                                Address Type
                            </div>
                            <div class="col col-md-5 no-gutters">
                                <b>{{ $customer->addresstype?? "" }}</b>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>
    </div>

    <div class="ecommerce-application">
    <h3>Ordered Product List</h3>

    <section class="list-view d-block mb-4">
        <div class="" id="product_list">

            @foreach ($products as $oproduct)
            @php
            $pmeta_data = json_decode($oproduct['meta_data'], true);
            $pmeta_data['campaign_name'];
            if(isset($pmeta_data['category']))
            {
                $product_categories = $pmeta_data['category'];
            }
            else{
                $product_categories = [];
            }
            if(empty($oproduct->product_variant_id))
            {
                $product = $oproduct->product;
            }
            else {
                $product = $oproduct->productVariant;
            }
            if($oproduct['discount'] != null)
            {
            $discount_price = $oproduct['price'] * ($oproduct['discount']/100);
            $discounted_price = $oproduct['price'] - $discount_price;
            }
            $img = $objimg = asset('XR/assets/images/placeholder.png');
            $obj_id = "";
            if(count($product->photo) > 0) {
            $img = $product->photo[0]->getUrl('thumb');
            $objimg = asset('XR/assets/images/placeholder.png');
            $obj_id = "";
            } else if(count($product->arobject) > 0) {
            if(!empty($product->arobject[0]->object))
            if(!empty($product->arobject[0]->object->getUrl()))
            $img = $product->arobject[0]->object->getUrl();
            }
            if(count($product->arobject) > 0){
            if(!empty($product->arobject[0]->object))
            if(!empty($product->arobject[0]->object->getUrl())) {
            $objimg = $product->arobject[0]->object->getUrl();
            $obj_id = $product->arobject[0]->id;
            }
            }
            @endphp
            <div class="card ecommerce-card" id="product_list_{{$product->id}}">
                <div class="card-content">
                    <div class="item-img text-center d-inline p-1" style="min-height: 12.85rem;">

                        @if(count($product->photo) > 1)
                        <div id="carousel-thumb" class="carousel slide carousel-fade carousel-thumbnails" data-interval="false" data-ride="carousel">
                            <div class="carousel-inner" role="listbox">
                                @foreach($product->photo as $photo)
                                <div class="carousel-item {{$loop->index == 0?'active':''}}">
                                    <img class="img-fluid" style="max-width: 150px; max-height:150px;" src="{{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl('thumb'):$objimg ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                </div>
                                @endforeach
                            </div>
                            <div class="text-center niceScroll" style="width:200px;overflow-x: scroll; display: inline-block; margin: 0 auto; padding: 3px;">
                                <ol class="carousel-indicators position-relative ">
                                    @foreach($product->photo as $photo)
                                    <li data-target="#carousel-thumb" data-slide-to="{{$loop->index}}" class=" {{$loop->index == 0?'active':''}}" style="width:50px; height:50px;">
                                        <img class="d-block img-fluid" style="max-width:50px;max-height: 50px;" src="{{(count($product->photo) > 0)?$product->photo[$loop->index]->getUrl('thumb'):$objimg ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';">
                                    </li>
                                    @endforeach
                                </ol>
                            </div>
                        </div>
                        @else
                        <img class="img-fluid" src="{{$img ?? ''}}" onerror="this.onerror=null;this.src='{{asset('XR/assets/images/placeholder.png')}}';" style="width: 150px; max-height:150px;">
                        @endif
                        @if(!empty($obj_id))
                        <div class="position-absolute" style="top:0;left:0;">
                            <div class="badge badge-glow badge-info">
                                <img class="img-fluid" src="{{asset('XR/assets/images/ar.png')}}" style="width: 32px; max-height:32px;">
                                <h6 class="text-white m-0">Tryon</h6>
                            </div>
                        </div>
                        @endif
                    </div>

                    <div class="card-body">
                        <div class="item-name">
                            <input type="hidden" class="tryonobj" value="{{$objimg ?? ''}}" />
                            <input type="hidden" class="obj_id" value="{{$obj_id ?? ''}}" />
                            <span>{{$oproduct['name'] ?? 'Untitled'}}</span>
                            <p class="item-company">
                                @if(count($product_categories) > 0)
                                <div class="badge badge-pill badge-glow badge-secondary text-dark mr-1">
                                    {{$product_categories[0]['name']}}
                                </div>
                                @endif
                            </p>
                        </div>
                        <div>
                            <span>{{$oproduct['sku'] ?? ''}}</span>
                        </div>

                        <div>
                            <h4 class="text-dark">{!! $pmeta_data['campaign_name'] !!}</h4>
                        </div>
                        @if($oproduct->notes)
                        <div style="border: 1px solid black;background: #D5CCCB;border-radius:5px;padding: 5px;">
                            <h4 class="text-dark">Note:{{" ".$oproduct->notes}}</h4>
                        </div>

                        @endif
                        <div>
                            <span>{{$product->url ?? ''}}</span>
                        </div>

                    </div>
                    <div class="item-options text-center">
                        <div class="item-wrapper">
                            <div class="item-rating">
                            </div>
                            <div class="item-cost text-left">
                                <h6 class="item-price">
                                    Qty: <span>{{$oproduct['qty'] ?? ''}}</span>
                                </h6>

                            </div>
                            <div class="item-cost text-left">
                                <h6 class="item-price">
                                    {!! $cmeta_data['currency'] !!}
                                    @if($oproduct['discount'] != null)
                                    {{$discounted_price}}
                                    <span class="text-primary"><s>{{$oproduct['price']}}</s></span>
                                    <br><span class="text-warning" style="font-size: 80%;">{{$oproduct['discount'] ?? ''}}%</span>
                                    @else
                                    <span>{{$oproduct['price'] ?? '00.00'}}</span>
                                    @endif
                                </h6>
                            </div>

                        </div>
                        <div class="wishlist bg-white">
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @if(count($customer->orderProducts) == 0)
            <h3 class="text-light text-center p-2">No Products added yet, please add</h3>
            @endif
        </div>
        @php
        $trackvalue  = 1;
        @endphp
    </section>

    </div>
</div>

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/viewerjs/1.6.2/viewer.min.js" integrity="sha512-VzJLwaOOYyQemqxRypvwosaCDSQzOGqmBFRrKuoOv7rF2DZPlTaamK1zadh7i2FRmmpdUPAE/VBkCwq2HKPSEQ==" crossorigin="anonymous"></script>
@parent
<script>
     window.addEventListener('alert', event => {
             toastr[event.detail.type](event.detail.message,
             event.detail.title ?? ''), toastr.options = {
                    "closeButton": true,
                    "progressBar": true,
                }
            });
$('#ordersubmit').click(function(e){
               e.preventDefault();
               $('#loading-bg').show();
               var orderid = $('#order_id').val();
               var ordertype = $('#order_type').val();
               var modetype = $('#mode_type').val();
               var packagelength = $('#package_length').val();
               var packagewidth = $('#package_width').val();
               var packageheight = $('#package_height').val();
               var packageweight = $('#package_weight').val();
                if(packagelength !='' && packagewidth !='' && packageheight !='' && packageweight != '' && orderid !='' && ordertype !='' && modetype !='')
                {
                $.ajax({
                  url: "{{route('admin.orders.shyplite')}}",
                  method: 'post',
                  data: {
                    orderid: orderid,
                    ordertype: ordertype,
                    modetype: modetype,
                    packagelength: packagelength,
                    packagewidth: packagewidth,
                    packageheight: packageheight,
                    packageweight: packageweight
                  },
                  success: function(result){
                    $('#shyplite_modal').modal('hide');
                    Swal.fire('order booked in shyplite');
                    $('#loading-bg').hide();
                    location.reload();
                  }
                });
                }
                else{
                    alert('Fill all fields in the form and then save the form');
                }

});
$('#track').click(function(e){
               e.preventDefault();
        var awbno ='';
        var orderid = $('#order_id').val();
               @if(!empty($trackingdata))
               @foreach($trackingdata as $key => $value)
                 awbno = "{{$value->awb_no}}";
                @endforeach
                @endif
        if(awbno){
            $.ajax({
                  url: "{{route('admin.track.shyplite')}}",
                  method: 'post',
                  data: {
                    awbno: awbno,
                    orderid: orderid
                  },
                  success: function(result){
                    Swal.fire('order Tracked in shyplite Successfully!');
                    $('#loading-bg').hide();
                    location.reload();
                  }
                });
        }

});
// function order_shyplite($this){
//     $('#loading-bg').show();
//             var modal_name = $this.data('modal_name');
//             $('#'+ modal_name).modal('hide');
//             alert($('#orderid').val());
//     $('#loading-bg').hide();

// }
var val;
var status_id;
 function show(x,statusContent){
    var order_data = JSON.parse(x)
    $('.remark').css("display", "none");
    $('.remarks-'+order_data.status).css("display", "block");
    $(".addComment").data('status',order_data.status)
    var remarkhtml = "<b>"+statusContent+":</b>";
    $("#showremarks").html(remarkhtml)
 }
 var y='';

 function showtrack(x){
    $('.trackvalue_'+y).css("display", "none");
    y=x;
    $('.lastkey').css("display", "none");
    $('.trackvalue_'+x).css("display", "block");
 }

$('#order_status').on('change', function() {
    // var val=$(this).children("option:selected").val();
     val = $(' #order_status option:selected').text();

    swal.fire({
        icon:'warning',
        title:'Are you sure to change the status?',
        html:'<div>You are changing Order status from {{$statusvalue}}  to  </div>'+ val +
      '<textarea row="5" cols="40" id="comment" required ></textarea></div> <br>'+
      '<input id="chk" checked value="true" type="checkbox">'+
	  '<label>Show this comment to the customer</label>',
        showCancelButton:true,
        cancelButtonText:"Cancel",
        confirmButtonText:"Ok",
        cancelButtonColor: "#DD6B55",
        preConfirm: function() {
            return new Promise((resolve, reject) => {
                resolve({
                    remarks: $('#comment').val(),
                    showremarks: $('#chk').prop('checked')
                });
            });
        }
    }).then(function(result){
        if(result.value)
        {
         console.log(result);
            Livewire.emit('updateOrderStatus',$('#order_status').val(),{{$customer->order->id}},result.value.remarks,result.value.showremarks);

        }
        else{

            // let prev_status = $("#order_previous_status").val()
            // $('#order_status').val(prev_status).change()

        }
    })
});

$(document).on('click',".addComment",function(){
    var status = $(this).data("status")
    console.log(status);
    swal.fire({
        icon:'warning',
        title:'Are you add the comments:',
        html:'<div>Write something here : </div>'+
      '<textarea row="5" cols="40" id="comment"></textarea></div> <br>'+
      '<input id="chk" checked value="true" type="checkbox">'+
	  '<label>Show this comment to the customer</label>',
        showCancelButton:true,
        cancelButtonText:"Cancel",
        confirmButtonText:"Ok",
        cancelButtonColor: "#DD6B55",
        preConfirm: function() {
            return new Promise((resolve, reject) => {
                resolve({
                    remarks: $('#comment').val(),
                    showremarks: $('#chk').prop('checked')
                });
            });
        }
    }).then(function(result){
        console.log(result.value)

        if(result.value)
        {
           if(result.value.remarks != '') {
            Livewire.emit('updateStatusRemarks',status,{{$customer->order->id}},result.value.remarks,result.value.showremarks)
           }
           else{
            Swal.fire({
                icon: 'error',
                title: 'write something in comment'
                });
           }
        }
    })
});


document.getElementById("upi-ss").onmousemove = function() {

let toolbar = {
    zoomOut: 3,
    zoomIn: 3,
    prev: 3,
    play: 3,
    next: 3,
    reset: 3,
}
const viewer = new Viewer(this, {
    inline: false,
    toolbar: toolbar,
    title: false,
    zoomOnTouch: true,
    slideOnTouch: true,
    movable: false,
    className: "bg-dark",
});
}

$(".niceScroll").niceScroll({
        cursorcolor: "grey", // change cursor color in hex
        cursoropacitymin: 0, // change opacity when cursor is inactive (scrollabar "hidden" state), range from 1 to 0
        cursoropacitymax: 1, // change opacity when cursor is active (scrollabar "visible" state), range from 1 to 0
        cursorwidth: "5px", // cursor width in pixel (you can also write "5px")
        cursorborder: "1px solid #fff", // css definition for cursor border
        cursorborderradius: "5px", // border radius in pixel for cursor
        zindex: "auto", // change z-index for scrollbar div
        scrollspeed: 60, // scrolling speed
        mousescrollstep: 40, // scrolling speed with mouse wheel (pixel)
        touchbehavior: false, // enable cursor-drag scrolling like touch devices in desktop computer
        hwacceleration: true, // use hardware accelerated scroll when supported
        boxzoom: false, // enable zoom for box content
        dblclickzoom: true, // (only when boxzoom=true) zoom activated when double click on box
        gesturezoom: true, // (only when boxzoom=true and with touch devices) zoom activated when pinch out/in on box
        grabcursorenabled: true, // (only when touchbehavior=true) display "grab" icon
        autohidemode: true, // how hide the scrollbar works, possible values:
        background: "", // change css for rail background
        iframeautoresize: true, // autoresize iframe on load event
        cursorminheight: 32, // set the minimum cursor height (pixel)
        preservenativescrolling: true, // you can scroll native scrollable areas with mouse, bubbling mouse wheel event
        railoffset: false, // you can add offset top/left for rail position
        bouncescroll: false, // (only hw accell) enable scroll bouncing at the end of content as mobile-like
        spacebarenabled: true, // enable page down scrolling when space bar has pressed
        disableoutline: true, // for chrome browser, disable outline (orange highlight) when selecting a div with nicescroll
        horizrailenabled: true, // nicescroll can manage horizontal scroll
        railalign: "right", // alignment of vertical rail
        railvalign: "bottom", // alignment of horizontal rail
        enabletranslate3d: true, // nicescroll can use css translate to scroll content
        enablemousewheel: true, // nicescroll can manage mouse wheel events
        enablekeyboard: true, // nicescroll can manage keyboard events
        smoothscroll: true, // scroll with ease movement
        sensitiverail: true, // click on rail make a scroll
        enablemouselockapi: true, // can use mouse caption lock API (same issue on object dragging)
        cursorfixedheight: false, // set fixed height for cursor in pixel
        hidecursordelay: 400, // set the delay in microseconds to fading out scrollbars
        irectionlockdeadzone: 6, // dead zone in pixels for direction lock activation
        nativeparentscrolling: true, // detect bottom of content and let parent to scroll, as native scroll does
        enablescrollonselection: true, // enable auto-scrolling of content when selection text
        cursordragspeed: 0.3, // speed of selection when dragged with cursor
        rtlmode: "auto", // horizontal div scrolling starts at left side
        cursordragontouch: false, // drag cursor in touch / touchbehavior mode also
        oneaxismousemode: "auto",
        scriptpath: "", // define custom path for boxmode icons ("" => same script path)
        preventmultitouchscrolling: true, // prevent scrolling on multitouch events
        disablemutationobserver: false,
    });


</script>

@endsection


