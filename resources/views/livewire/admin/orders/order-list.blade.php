@php
    $agent=new \Jenssegers\Agent\Agent();
@endphp

<div>

    {{-- <div>
        @if (session()->has('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
    </div> --}}

    <div class="ecommerce-application">
        <section id="ecommerce-searchbar">
            <div class="row justify-content-end">
                <div class="col">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="product-all-tab" data-toggle="tab" aria-selected="true">Orders ({{empty($orders)?'0':$orders->total()}})</a>
                        </li>
                    </ul>
                </div>
                @if($agent->isDesktop() || $agent->isTablet())
                <div class="col-md-3 col-sm-10">
                <select name="" class="form-control search-product mb-1" id="day_select">
                    <option value="today">Today</option>
                    <option value="yesterday">Yesterday</option>
                    <option value="last_7_days">Last 7 days</option>
                    <option value="last_30_days">Last 30 days</option>
                    <option value="thismonth">This Month</option>
                    <option value="yearly" selected>Yearly</option>
                </select>
                </div>
               <div class="col-md-2 col-sm-10">
                    <select name="" class="form-control search-product mb-1" id="order_mode">
                        <option value="*" class="form-control" selected>All</option>
                        <option value="POS" class="form-control"> POS </option>
                        <option value="Online-Storefront" class="form-control"> Online Storefront </option>
                    </select>
                </div>
                <div class="col-md-2 col-sm-10">
                    <select name="" class="form-control search-product mb-1" id="order_status">
                        <option value="*" class="form-control" selected>All</option>
                        @foreach(App\orders::STATUS as $key => $label)
                        <option value="{{ $key }}" class="form-control"> {{$label}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-3 col-sm-12">
                    <fieldset class="form-group position-relative">
                        <input type="text" class="form-control search-product" id="order_search" placeholder="Search here">
                        <div class="form-control-position">
                            <i class="feather icon-search"></i>
                        </div>
                    </fieldset>
                </div>
                <div class="col-rt-10 mb-2 mr-4">
                    <button class="form-control btn btn-success waves-effect waves-light" data-bs-toggle="modal" data-bs-target="#exampleModal">Export</button>
                </div>
                <!-- Button trigger modal -->


  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Export Orders</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
          {{-- <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">X</span>
          </button> --}}
        </div>
        <div class="modal-body">
            {{-- <div class="col-sm-10 mb-3">
                <input id="party" type="datetime-local" name="partydate" class="form-control">
              </div> --}}
              <form method="POST" action='orders/export'>
                  @csrf
                  <div class="form-group " id="dateSection">
                    <div class="form-row">
                        <div class="col-12 mt-2">
                            <label class="export">Filters</label>
                            <select name="range_type" class="form-control mb-1"  onchange="toggleDate(event)" id="statementDuration">
                            <option value="1"   id="today">Today</option>
                            <option value="2"   id="yesterday">Yesterday</option>
                            <option value="3"   id="last7Day">Last 7 days</option>
                            <option value="4"   id="last30Days">Last 30 days</option>
                            <option value="5"   id="thisMonth">This Month</option>
                            <option value="6"   id="yearly">Yearly</option>
                            <option value="7"   id="custom">Custom Date</option>
                            </select>
                            </div>
                            <div class="col">
                                <label class="export">Choose format</label>
                                <select name="file_format" class="form-control mb-1"  id="fileFormat">
                                <option value="1">XLSX</option>
                                <option value="2">XLS</option>
                                <option value="3">CSV</option>
                                </select>
                                </div>

                        <div class="col custom-section d-none  ">
                            <span for="startdate" class="h5">Start Date</span>
                            <input type="date" id="startdate" class="form-control"   name="startdate" aria-describedby="start_dateHelp"  >
                            <small id="start_dateHelp" class="form-text text-muted"> </small>

                        </div>
                        <div class="col custom-section d-none">
                            <span for="enddate" class="h5">End Date</span>
                            <input type="date" id="enddate" class="form-control" name="enddate" aria-describedby="expires_inHelp" >
                        </div>
                    </div>
                </div>

            {{-- <div class="form-check ">
                <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault1">
                <label class="form-check-label" for="flexRadioDefault1">
                  CSV
                </label>
              </div> --}}


        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-primary">Export</button>
        </div>
    </form>
      </div>
    </div>
  </div>

            @endif
            @if($agent->isMobile())


                <div class=" col-sm-6">
                    <fieldset class="form-group position-relative">
                        <input type="text" class="form-control search-product" id="order_search" placeholder="Search here">
                        <div class="form-control-position">
                            <i class="feather icon-search"></i>
                        </div>
                    </fieldset>
                </div>
                <div class="col-sm-6 text-right" id="order_filter">
                <i class="fa fa-filter fa-2x m-1"></i>
                </div>

                <div class="col-md-3 col-sm-10 day_select" style="display:none;">
                <select name="" class="form-control search-product mb-1 " id="day_select">
                    <option value="today">Today</option>
                    <option value="yesterday">Yesterday</option>
                    <option value="last_7_days">Last 7 days</option>
                    <option value="last_30_days">Last 30 days</option>
                    <option value="thismonth">This Month</option>
                    <option value="yearly" selected>Yearly</option>
                </select>
                </div>
               <div class="col-md-2 col-sm-10 order_mode" style="display:none;">
                    <select name="" class="form-control search-product mb-1" id="order_mode">
                        <option value="*" class="form-control" selected>All</option>
                        <option value="POS" class="form-control"> POS </option>
                        <option value="Online-Storefront" class="form-control"> Online Storefront </option>
                    </select>
                </div>
                <div class="col-md-2 col-sm-10 order_status" style="display:none;">
                    <select name="" class="form-control search-product mb-1" id="order_status">
                        <option value="*" class="form-control" selected>All</option>
                        @foreach(App\orders::STATUS as $key => $label)
                        <option value="{{ $key }}" class="form-control"> {{$label}}</option>
                        @endforeach
                    </select>
                </div>
            @endif

            </div>
        </section>
    </div>




    <div class="justify-content-center m-5" wire:loading.flex>
        <div class="spinner-border" role="status">
        </div>
    </div>
    @if($orders==null || $orders->total()==0)
    <h3 class="text-light text-center p-2 w-100">{{$order_info_text}}</h3>
    @else

    @foreach ($orders as $customer)
    @php
    $order_meta_data= json_decode($customer->meta_data, true);
    @endphp
    @if(isset($customer->order->transactions->amount))

    <div class="card ecommerce-card" id="order_list_{{$customer->order_id}}" wire:loading.remove>
        <div class="card-body">
            <div class="row">
                <div class="col col-md-4 order-1 cursor-pointer" onclick="location.href='{{route('admin.customers.orderDetails',$customer->id)}}';">

                    <h4><b>Order #{{$customer->store_order_id}}</b></h4>


                    <h5 class="mb-1 mt-1"><b>{{$customer->fname}} {{$customer->lname ?? ""}}</b></h5>


                    <h5>{{$customer->mobileno ?? ""}}</h5>


                    @php
                    if(App\orders::STATUS[$customer->status] == 'Order Recieved')
                    $class="text-primary";
                    elseif (App\orders::STATUS[$customer->status] == 'Pending')
                    $class="text-warning";
                    elseif ( in_array(App\orders::STATUS[$customer->status] , ['Accepted','Shipped', 'Delivered']))
                    $class="text-success";
                    elseif ( in_array(App\orders::STATUS[$customer->status] ,['Rejected','Cancelled','Failed']))
                    $class="text-danger";
                    @endphp

                    <h5>Status: <span class='{{$class}}'>{{App\orders::STATUS[$customer->status]}}</span></h5>

                </div>
                <div class="col col-md-4 order-2 d-flex flex-md-row flex-column justify-content-md-center">

                    <a href="https://wa.me/{{$customer->country_code ? $customer->country_code.$customer->mobileno : '91'.$customer->mobileno}}" class="mr-1 mb-1" target="_blank">
                        <p class="text-center"><i style="color:#25D366" class="fa fa-whatsapp fa-2x"></i></p></a>
                    <a href="tel:{{$customer->mobileno}}" class="mr-1 mb-1">
                        <p class="text-center"><i style="color:#2F91F3" class="fa fa-phone fa-2x "></i></p></a>
                    <a href="{{ route('admin.export_order_pdf', $customer->order_id) }}" class="mr-1 mb-1">
                        <p class="text-center"><i style="color:red" class="fa fa-file-pdf-o fa-2x"></i></p><p class="text-center">order Summary</p></a>
                    <a href="{{ route('admin.export_label_pdf', $customer->order_id) }}" class="mr-1 mb-1 ">
                    @if($agent->isMobile())
                        <p class="btn  btn-success btn-sm " style="margin-left:20px;">Print label</p></a>
                    @else
                        <p class="btn  btn-success btn-sm ">Print label</p></a>
                    @endif
                    @if($customer->is_generate_invoice == 0)

                    <a><span class="btn btn-danger btn-sm " wire:click="updateGenerateInvoice({{ $customer->order_id }})">

                    Generate Invoice</span></a>
                    @else
                        <a href="{{ route('admin.export_invoice_pdf', $customer->order_id) }}" class="mr-1 mb-1">
                        <p class="text-center"><i style="color:green" class="fa fa-file-pdf-o fa-2x"></i></p><p class="text-info text-center">invoice</p></a>
                    @endif
                </div>
                <div class="col col-md-4 order-3 text-md-right cursor-pointer" onclick="location.href='{{route('admin.customers.orderDetails',$customer->id)}}';">

                    <h5>{{ date('d-m-Y', strtotime($customer->created_at)) }}</h5>
                    <h5>{{ date('h:i:s A', strtotime($customer->created_at)) }}</h5>

                    <h5><b>No. of Products: {{count($customer->orderProducts)}}</b></h5>
                    <!-- <h4><b>{!! $order_meta_data['product_total_price']==0?" ":$currency.''.$order_meta_data['product_total_price'] !!}</b></h4> -->
                    <h4><b>{{ $customer->order->transactions->amount }}</b></h4>

                </div>
            </div>
        </div>

    </div>
    @endif
    @endforeach
    {{$orders->links()}}

    @endif
</div>
@section('scripts')
@parent
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script>
window.addEventListener('alert', event => {
             toastr[event.detail.type](event.detail.message,
             event.detail.title ?? ''), toastr.options = {
                    "closeButton": true,
                    "progressBar": true,
                }
            });
    //emiting the event on order search
    $('#order_search').on('input', function() {
        Livewire.emit('SearchOrder', $('#order_search').val())
    });

    $('#order_status').on('change', function() {
        Livewire.emit('SearchOrderStatus', $('#order_status').val())
    });

    $('#order_mode').on('change', function() {
        Livewire.emit('SearchOrderMode', $('#order_mode').val())
    });

    $('#day_select').on('change', function() {
        Livewire.emit('SearchDay', $('#day_select').val())
        // alert($('#day_select').val())
    });
    $(document).on('click', '#order_filter', function(){
        $(".day_select").toggle();
        $(".order_status").toggle();
        $('.order_mode').toggle();

    });
    // $('#generateinvoice').on('click',function(){
    //     Livewire.emit('updateGenerateInvoice',$customer->order_id)
    // });


</script>
<script type="text/javascript">
    $('.input-daterange input').each(function() {
    $(this).datepicker('clearDates');
});
$('#statementDuration').click(function(){
    $('dateSection').addClass('d-none');
});

function toggleDate(event){
    ;
    if(event.target.value == 7){

        $('.custom-section').removeClass('d-none');
    }else{
        $('.custom-section').addClass('d-none');
    }
}

    </script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<style>
    .export
        {
    color: #464646;
    font-size: 1.2rem;
    margin-bottom: 0;
    padding-left: 0.2rem;
      }

</style>
    @endsection
