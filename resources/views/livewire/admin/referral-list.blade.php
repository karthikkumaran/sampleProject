<div>
    {{-- To attain knowledge, add things every day; To attain wisdom, subtract things every day. --}} 
    <div class="row mb-1">
        <div class="col-sm-1 p-0">
        </div>
        <div class="col-sm-4 p-0">
        <div class="item-img text-left">
        <h4>Referral Program</h4>
        <h1><b>Refer More! Earn More!</b></h1>
        <p >Refer SimpleSell to your friends.Earn amazing rewards upon referal Signup.More Signups Big Reward</p><br><br>
        <p>Your invite link</p>        
               <div> <label style="background:white;"><span id="refer_link">{{ $link }}</span>               
               <!-- <div> <label style="background:white;"><span id="refer_link"></span>                -->
                    <!-- <button class="btn btn-flat btn-icon" style="border:5pxl;" onclick="catalogCopyToClipboard('#refer_link')">
                        <span id="public_catalog_link_copy"></span><br><br><i class="feather icon-copy"></i><span class="text-success">copy</span>
                    </button>                     -->
               <button class="btn btn-flat btn-icon"  onclick="copyToClipboard('#refer_link')">
                <i class="feather icon-copy"></i><span class="text-success">copy</span></button>               
            </div><br>
              
        <p>share in</p>       
        <!-- <button type="button" class="btn btn-icon rounded-circle btn-outline-danger mr-1 mb-1 sharebtn" style="background:red;"><a href="https://gmail.com" target="_blank"><i class="feather icon-mail "style="color:white;"></i></a></button> -->
        <button type="button" class="btn btn-icon rounded-circle btn-outline-primary mr-1 mb-1 sharebtn" onclick='share("fb")' style="background:darkblue;"><i class="fa fa-facebook fa-fw"style="color:white;"></i></button>   
        <button type="button" class="btn btn-icon rounded-circle btn-outline-success mr-1 mb-1 sharebtn bg bg-success" onclick="share('whatsapp')" ><i class="fa fa-whatsapp fa-fw"style="color:white;"></i></button>   
        <!-- <button type="button" class="btn btn-icon rounded-circle btn-outline-primary mr-1 mb-1 sharebtn "  style="background:blue;"><a href="https://in.linkedin.com" target="_blank"><i class="feather icon-linkedin "style="color:white;"></i></a></button>  -->
        <button type="button" class="btn btn-icon rounded-circle btn-outline-info mr-1 mb-1 sharebtn bg bg-info"onclick="share('twitter')" ><i class="fa fa-twitter fa-fw"style="color:white;"></i></button>   

        </div>
        </div>
        <div class="col-sm-1 p-0">
        </div>
        <div class="col-sm-6 p-0">
        <div class="item-img text-center">
        <img src="{{asset('XR/app-assets/images/pages/graphic-5.png')}}" id="referral_logo" style="margin-center: -1rem;width: 360px;"/>
        </div>
        </div>
</div>
</div>
<div class="row mb-1">
    <div class="col-sm-1 p-0">
        </div>
        <div class="col-sm-11 p-0">
        <h2><b>How does Refer and Earn Reward work?</b></h2><br>
        </div>
</div>
<div class="row mb-1">
<div class="col-lg-4 col-md-6 col-12">
                            <div class="card">
                                <div class="card-header d-flex flex-column align-items-start pb-0">
                                    <div class="avatar bg-rgba-success p-50 m-0">
                                        <div class="avatar-content">
                                            <i class="feather icon-share-2 text-success font-medium-3"></i>
                                        </div>
                                    </div>
                                    <h4 class="text-bold-700 mt-1 mb-25">Share Referral Link</h4>
                                </div>
                                <div class="card-content p-1">
                                <p>Login to SimpliSell website and get your referral link to share.Share your referral link to someone you know or on social media site</p>

                                </div>
                            </div>
                        </div>
<div class="col-lg-4 col-md-6 col-12">
                            <div class="card">
                                <div class="card-header d-flex flex-column align-items-start pb-0">
                                    <div class="avatar bg-rgba-success p-50 m-0">
                                        <div class="avatar-content">
                                            <i class="feather icon-target text-success font-medium-3"></i>
                                        </div>
                                    </div>
                                    <h4 class="text-bold-700 mt-1 mb-25">Make Referral Count</h4>
                                </div>
                                <div class="card-content p-1">
                                <p>Each Signup through your referral link Counts</p><p>Note: Only users who have validated their Email and Contact number are considered as valid Signups</p>

                                </div>
                            </div>
                        </div>
 <div class="col-lg-4 col-md-6 col-12">
                            <div class="card">
                                <div class="card-header d-flex flex-column align-items-start pb-0">
                                    <div class="avatar bg-rgba-success p-50 m-0">
                                        <div class="avatar-content">
                                            <i class="feather icon-gift text-success font-medium-3"></i>
                                        </div>
                                    </div>
                                    <h4 class="text-bold-700 mt-1 mb-25">Earn Rewards and Referral Signup</h4>
                                </div>
                                <div class="card-content p-1">
                                <p>Refer more friends.Earn greater Rewards.Check below for the Referral Score Board</p>

                                </div>
                            </div>
                        </div>
</div>
<br>

<div class="row mb-1">
        
        <div class="col-sm-12 p-1"style="background:white;">            
            <br>
            <div class="text-center"> 
            <h3><b>Referral Score Board</b></h3><br>
            </div>
            <div class="row justify-content-center">                                
            <div class="col-lg-4 col-md-6 col-12">
                            <div class="card text-center">
                                <div class="card-header d-flex flex-column align-items-center pb-0">
                                    <div class="avatar bg-rgba-success p-50 m-0">
                                        <div class="avatar-content">
                                            <i class="feather icon-log-in text-success font-medium-3 "></i>
                                        </div>
                                    </div>
                                    <h4 class="text-bold-700 mt-1 mb-25">Friends who Signed Up</h4>
                                </div>
                                <div class="card-content p-1">
                                <h2>{{ count($referred) }}</h2>
                                </div>
                            </div>
                        </div>  
         <div class="col-lg-4 col-md-6 col-12">
                            <div class="card text-center">
                                <div class="card-header d-flex flex-column align-items-center pb-0">
                                    <div class="avatar bg-rgba-success p-50 m-0">
                                        <div class="avatar-content">
                                            <i class="feather icon-user-check  text-success font-medium-3"></i>
                                        </div>
                                    </div>
                                    <h4 class="text-bold-700 mt-1 mb-25">Free Trail</h4>
                                </div>
                                <div class="card-content p-1">  
                                @php
                                $trailSubscription = 0;
                                $paidSubscription = 0;
                                
                                foreach($plan_id as $key => $value){
                                if(!empty($value)){  
                                if($value->plan_id == 1){
                                  $trailSubscription = $trailSubscription + 1;
                                }
                                else{
                                  $paidSubscription = $paidSubscription + 1;
                                }
                                } 
                                }
                                @endphp                              
                                <h2>{{ $trailSubscription }}</h2>
                                </div>
                            </div>
                        </div> 
          <div class="col-lg-4 col-md-6 col-12">
                            <div class="card text-center">
                                <div class="card-header d-flex flex-column align-items-center pb-0">
                                    <div class="avatar bg-rgba-success p-50 m-0">
                                        <div class="avatar-content">
                                            <i class="feather icon-user-plus  text-success font-medium-3"></i>
                                        </div>
                                    </div>
                                    <h4 class="text-bold-700 mt-1 mb-25">Paid Subscription</h4>
                                </div>
                                <div class="card-content p-1">
                                <h2>{{ $paidSubscription }}</h2>
                                </div>
                            </div>
                        </div>   
             <!-- <div class="col-sm-3 p-0">
             </div>    
             <div class="col-sm-3 p-0">    
                <div class="card"style="background:white;width: 12rem; height:10rem;">
                <br>
                <span class="text-center"><i class="fa fa-envelope-open-o" style="color:green;"></i></span><br>
                <span class="text-center"><span style="font-size:0.8rem;"><b>Freinds who Signed Up<br>6</b></span></span><br>                
                </div>
              </div> 
              <div class="col-sm-3 p-0">    
                <div class="card"style="background:white;width: 12rem; height:10rem;">
                <br>
                <span class="text-center"><i class="fa fa-envelope-open-o" style="color:green;"></i></span><br>
                <span class="text-center"><span style="font-size:0.8rem;"><b>Account verified<br>1</b></span></span><br>                
                </div>
              </div> 
              <div class="col-sm-2 p-0">
             </div>                    
            </div> -->
            <!-- </div> -->
        </div>

        
</div>
</div>
<br>
<div class="row mb-1">
        
        <div class="col-sm-12 p-2"style="background:white;">        
        <h1> Terms and Conditions</h1>
        <p> For every bunch of successfull account registration you will get rewards.Just share to get rewards.play cool!<p>
        <p>Users involving in malicious activities to manipulate the referral counts is strictly prohibited any such found incident is subjected to immediate suspension of the account and might lead to legal consequences.</p>
        <p>Note: Only verified Email users are Considered as valid Signups.</p><br>
        <!-- <h1> Frequently Asked Questions</h1>
        <div class="collapse-margin collapse-icon mt-2" id="faq-payment-qna">
        <div class="card">
              <div
                class="card-header"
               
                data-toggle="collapse"
                role="button"
                data-target="#faq-payment-one"
                aria-expanded="false"
                aria-controls="faq-payment-one"
              >
                <span class="lead collapse-title">How I will receive the discount code?</span>
              </div>
              <div
                id="faq-payment-one"
                class="collapse"
                aria-labelledby="paymentone"
                data-parent="#faq-payment-qna"
              >
                <div class="card-body">
                  Tart gummies dragée lollipop fruitcake pastry oat cake. Cookie jelly jelly macaroon icing jelly beans
                  soufflé cake sweet. Macaroon sesame snaps cheesecake tart cake sugar plum. Dessert jelly-o sweet
                  muffin chocolate candy pie tootsie roll marzipan.
                </div>
              </div>
            </div>

            <div class="card">
              <div
                class="card-header"
                id="paymenttwo"
                data-toggle="collapse"
                role="button"
                data-target="#faq-payment-two"
                aria-expanded="false"
                aria-controls="faq-payment-two"
              >
                <span class="lead collapse-title">How I will receive the earned rewards?</span>
              </div>
              <div
                id="faq-payment-two"
                class="collapse"
                aria-labelledby="paymenttwo"
                data-parent="#faq-payment-qna"
              >
                <div class="card-body">
                  Tart gummies dragée lollipop fruitcake pastry oat cake. Cookie jelly jelly macaroon icing jelly beans
                  soufflé cake sweet. Macaroon sesame snaps cheesecake tart cake sugar plum. Dessert jelly-o sweet
                  muffin chocolate candy pie tootsie roll marzipan.
                </div>
              </div>
</div>
              <div class="card">
              <div
                class="card-header"
                id="paymentthree"
                data-toggle="collapse"
                role="button"
                data-target="#faq-payment-three"
                aria-expanded="false"
                aria-controls="faq-payment-three"
              >
                <span class="lead collapse-title">why my referral signups are not updated?</span>
              </div>
              <div
                id="faq-payment-three"
                class="collapse"
                aria-labelledby="paymentthree"
                data-parent="#faq-payment-qna"
              >
                <div class="card-body">
                  Tart gummies dragée lollipop fruitcake pastry oat cake. Cookie jelly jelly macaroon icing jelly beans
                  soufflé cake sweet. Macaroon sesame snaps cheesecake tart cake sugar plum. Dessert jelly-o sweet
                  muffin chocolate candy pie tootsie roll marzipan.
                </div>
              </div>
            </div>
            </div>
        </div>
        </div> -->
 </div>       
    
 <script>
function copyToClipboard(element) {
  var $temp = $("<input>");
  $("#refer_link").append($temp);
  $temp.val($(element).text());
  $temp.select();
  document.execCommand("copy");
  $temp.remove();
  toastr.success('Referral Link', 'Copied!');
}

function share(share_name) {
        // console.log(share_name);
       var msg = "your reference link {{$link}} to share your friends"
        // console.log(t[0], "hi", t[1]);
        switch (share_name) {
            case 'fb':
                // window.open('http://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t[0] + " #" + hashtags), 'sharer', 'toolbar=0,status=0,width=626,height=436');
                openInNewTab('http://www.facebook.com/sharer.php?u={{$link}}');

                break;
            case 'whatsapp':
                // var message = encodeURIComponent(t[1]) + " " + encodeURIComponent(u) + " #" + hashtags;
                // var whatsapp_url = "whatsapp://send?text=" + message;
                var whatsapp_url = "https://wa.me/?text=" + msg;
                openInNewTab(whatsapp_url);
                break;
            case 'twitter':
                // window.open("https://twitter.com/intent/tweet?text=" + encodeURIComponent(t[0]) + "&url=" + u + "&hashtags=" + hashtags, 'sharer', 'toolbar=0,status=0,width=626,height=436')
                openInNewTab("https://twitter.com/intent/tweet?text=" + msg );

                break;
            
        }

    }
</script>