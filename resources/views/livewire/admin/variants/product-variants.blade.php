<div>
    <div class="content-header row ">
        <div class="content-header-left col-md-9 col-12 mb-1">
            <div class="row breadcrumbs-top ">
                <div class="col-12 pb-0">
                    <a href="{{ route("admin.products.product_list") }}" >
                        <h4 class="text-primary mb-0 pb-0"><i class="feather icon-arrow-left"></i>Back to Product</h4>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-content">
            <div class="card-body">
                <div id="set_variants" style="display:{{$setVariants}}">
                    <div class="row mb-2">
                        <div class="col-12">
                            <strong><h4>Product Name: {{$product["name"] ?? "Untitled product"}}</h4></strong>
                            <br>
                        </div>
                        <div class="col-12">
                            <button class="btn btn-success" id="add_variant" onclick="addVariant()"> Add variant </button>
                        </div>
                    </div>
                    <div class="row mt-2 mb-2" style="display: none;" id="variants_alert_div">
                        <div class="col-lg-12">
                            <div class="alert alert-success" role="alert">You can add a maximum of 4 variants.</div>
                        </div>
                    </div>
                    <div class="row mt-2 mb-2" style="display: none;" id="duplicate_variant_alert_div">
                        <div class="col-lg-12">
                            <div class="alert alert-success" role="alert">You have selected the same variant more than once.</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <form name="addVariantsForm">
                                @csrf
                                <fieldset>
                                    <div id="variants_section">

                                    </div>
                                    <div>
                                        <button type="button" class="btn btn-danger float-right" id="next_btn" onclick="submitVariantsForm()" disabled>Next</button>
                                    </div>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="p-1" id="select_product_variants" style="display:{{$selectProducts}}">
                    @if(count($product_variants) > 0)
                    <div class="row">
                        <div class="col-6">Total product variants available: <strong><span id="product_variants_count">{{$product_variants_count}}</span></strong></div>
                        <div class="col-6">Product variants selected: <strong><span id="selected_product_variants_count">0</span></strong></div>
                        <div class="col-12">
                            <button type="button" class="btn btn-success" id="select_all_product_variants" onclick="selectAllProductVariants()">Select All</button>
                            <button type="button" class="btn btn-danger" id="unselect_all_product_variants" onclick="unselectAllProductVariants()" style="display: none;">Unselect All</button>
                        </div>
                    </div>
                    <div class="row mt-2 mb-2" style="display: none;" id="selected_products_alert_div">
                        <div class="col-lg-12">
                            <div class="alert alert-success" role="alert">You can select a maximum of 50 product variants.</div>
                        </div>
                    </div>
                    <div class="row mt-2 mb-2" style="display: none;" id="select_products_alert_div">
                        <div class="col-lg-12">
                            <div class="alert alert-success" role="alert">Select atleast one product variant to add.</div>
                        </div>
                    </div>

                    <div class="row mt-1">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover datatable datatable-product-variants">
                            <thead>
                                <tr>
                                    <th>Add variant</th>
                                    <th>Product name</th>
                                    <th>SKU</th>
                                    <th>Price</th>
                                    @if(session('subscribed_features'))
                                        @if(array_key_exists("inventory", session('subscribed_features')[0]))
                                            @if(!empty($product['stock']))
                                                <th>Stock</th>
                                            @endif
                                            <th>Out of stock</th>
                                        @endif
                                    @endif
                                    <th>Minimum order quantity</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                if(isset($product_variants_last['product_id']) && count((array)$product_variants_last['product_id'])>0)
                                {
                                    $i=  explode("-","{$product_variants_last['sku']}");
                                    $d=$i[array_key_last($i)];
                                }
                                @endphp
                                @foreach ($product_variants as $key=> $product)
                                    <tr>
                                        <td>
                                            @foreach($product["variants"] as $variant)
                                                <br>
                                                @php
                                                    $variant_details = $original_product->getVariantName($variant["type"]);
                                                @endphp
                                                {!! $variant_details->name !!} - {!! $variant["value"] !!}
                                            @endforeach
                                            @php
                                              if($key==0)
                                              {
                                                $n=0;
                                              }
                                              else {
                                                  $n=$n+1;
                                              }

                                              if(isset($d) && $key==0)
                                              {
                                                $n=$d+1;
                                              }
                                            @endphp
                                             {{-- {{ dd($n) }} --}}
                                            <div class="form-group m-0 p-1">
                                                <input class="form-check add_to_store_checkbox" type="checkbox" id="add_to_store_{{$loop->index}}" name="add_to_store[]" value="1" onclick='checkProductsCount(this)' />
                                                @if($errors->has('out_of_stock'))
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('out_of_stock') }}
                                                    </div>
                                                @endif
                                                <span class="help-block">{{ trans('cruds.product.fields.out_of_stock_helper') }}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group m-0">
                                                <input class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" type="text" name="product_name[]" id="name_{{$loop->index}}" value="{{ old('name', $product['name']) }}">
                                                @if($errors->has('name'))
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('name') }}
                                                    </div>
                                                @endif
                                                <span class="help-block">{{ trans('cruds.product.fields.name_helper') }}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group m-0">
                                                <input class="form-control {{ $errors->has('sku') ? 'is-invalid' : '' }}" type="text" name="sku[]" id="sku_{{$loop->index}}" value="{{ old('sku', $product['sku'].'SKUID-'.($n))}}">
                                                @if($errors->has('sku'))
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('sku') }}
                                                    </div>
                                                @endif
                                                <span class="help-block">{{ trans('cruds.product.fields.sku_helper') }}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="form-group m-0">
                                                <input class="form-control {{ $errors->has('price') ? 'is-invalid' : '' }}" type="number" name="price[]" id="price_{{$loop->index}}" value="{{ old('price', $product['price']) }}" step="0.01">
                                                @if($errors->has('price'))
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('price') }}
                                                    </div>
                                                @endif
                                                <span class="help-block">{{ trans('cruds.product.fields.price_helper') }}</span>
                                            </div>
                                        </td>
                                        @if(session('subscribed_features'))
                                            @if(array_key_exists("inventory", session('subscribed_features')[0]))
                                                @if(!empty($product['stock']))
                                                    <td>
                                                        <div class="form-group m-0">
                                                            <input class="form-control" type="text" name="stock[]" id="stock_{{$loop->index}}" value="{{ old('stock', $product['stock']) }}">
                                                            @if($errors->has('stock'))
                                                                <div class="invalid-feedback">
                                                                    {{ $errors->first('stock') }}
                                                                </div>
                                                            @endif
                                                            <span class="help-block">{{ trans('cruds.product.fields.stock_helper') }}</span>
                                                        </div>
                                                    </td>
                                                @endif
                                                <td>
                                                    <div class="form-group m-0 p-1">
                                                        <input class="form-check" type="checkbox" id="out_of_stock_{{$loop->index}}" name="out_of_stock[]" value="1" />
                                                        @if($errors->has('out_of_stock'))
                                                            <div class="invalid-feedback">
                                                                {{ $errors->first('out_of_stock') }}
                                                            </div>
                                                        @endif
                                                        <span class="help-block">{{ trans('cruds.product.fields.out_of_stock_helper') }}</span>
                                                    </div>
                                                </td>
                                            @endif
                                        @endif
                                        <td>
                                            <div class="form-group m-0">
                                                <input class="form-control {{ $errors->has('minimum_quantity') ? 'is-invalid' : '' }}" type="number" min="1" name="minimum_quantity[]" id="minimum_quantity_{{$loop->index}}" value="{{ old('minimum_quantity', $product['minimum_quantity'] ?? null )  }}" step="1">
                                                <span id="errMsg"></span>
                                                @if($errors->has('minimum_quantity'))
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('minimum_quantity') }}
                                                    </div>
                                                @endif
                                                <span class="help-block">{{ trans('cruds.product.fields.minimum_quantity_helper') }}</span>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <button type="button" class="btn btn-primary" id="add_selected_prdocuts_btn" onclick="addSelectedProductVariants()" disabled>Add selected products</button>
                    </div>
                    <form name="addSelectedProductsForm">
                        @csrf
                    </form>
                    @else
                        <div class="row">
                            <div class="col-12 text-center">
                                <h4>All combinations of the selected variants have already been added.</h4>
                            </div>
                        </div>
                    @endif
                </div>
                <div style="display:{{$products_uploaded}}">
                    Variants uploaded
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script>
        var new_variant_id = 0;
        var variant_types = [];
        var variant_values = [];
        var product = {!! json_encode($product) !!};
        var product_variants_array = new Array();
        var product_variants_count = 0;
        var selected_product_variants_count = 0;
        var variant_values_array = new Array();

        function selectAllProductVariants(){
            $("#select_all_product_variants").hide();
            $("#unselect_all_product_variants").show();
            selected_product_variants_count = 0;
            $('.add_to_store_checkbox').each(function(){
                selected_product_variants_count++;
                $(".add_to_store_checkbox").prop('checked', true);
            });
            $("#selected_product_variants_count").html(selected_product_variants_count);
            if(selected_product_variants_count >= 50) {
                $("#selected_products_alert_div").show();
                $("#add_selected_prdocuts_btn").prop('disabled', true);
            }
            else{
                $("#add_selected_prdocuts_btn").prop('disabled', false);
            }
        }

        function unselectAllProductVariants(){
            $("#unselect_all_product_variants").hide();
            $("#select_all_product_variants").show();
            $('.add_to_store_checkbox').each(function(){
                $(".add_to_store_checkbox").prop('checked', false);
            });
            selected_product_variants_count = 0;
            $("#selected_products_alert_div").hide();
            $("#select_products_alert_div").show();
        }

        function addVariant() {
            new_variant_id += 1;
            url = "{{ route('admin.productVariants.addVariant', ':variant_id') }}";
            url = url.replace(':variant_id', new_variant_id);
            var attr_row = $('<div>').load(url, function(row) {
                $('#variants_section').append(row);
            });
            $("#next_btn").prop('disabled', false);
            if(new_variant_id == 4){
                $("#variants_alert_div").show();
            }
            else{
                $("#variants_alert_div").hide();
            }
        }

        function submitVariantsForm() {
            variant_types = [];
            variant_values = [];
            formData = $("form[name=addVariantsForm]").serializeArray();
            for( var i=0; i<formData.length; i++){
                if(formData[i].name == "type[]") {
                    variant_types.push(formData[i].value);
                }
                else if(formData[i].name == "value[]") {
                    var values_array = [];
                    values_array = formData[i].value.split(",");
                    variant_values.push(values_array);
                }
            }
            let hasDuplicate = variant_types.some((val, i) => variant_types.indexOf(val) !== i);
            if(hasDuplicate == true) {
                $("#duplicate_variant_alert_div").show();
            }
            else{
                $("#duplicate_variant_alert_div").hide();
                var variant_count = variant_types.length;
                if(variant_count == 1) {
                    for(var i = 0; i < variant_values[0].length; i++) {
                        variant_values_array = new Array();
                        variant_values_array.push({
                            type: variant_types[0],
                            value:  variant_values[0][i]
                        });
                        product_variants_array.push({
                            add_to_store: 0,
                            name: product["name"],
                            price: product["price"],
                            sku: product["sku"],
                            out_of_stock: product["out_of_stock"],
                            stock: product["stock"],
                            minimum_quantity: product["minimum_quantity"],
                            variants: variant_values_array,
                        });
                    }
                }
                else if(variant_count == 2) {
                    for(var i = 0; i < variant_values[0].length; i++) {
                        for(var j = 0; j < variant_values[1].length; j++) {
                            variant_values_array = new Array();
                            variant_values_array.push({
                                type: variant_types[0],
                                value:  variant_values[0][i]
                            });
                            variant_values_array.push({
                                type: variant_types[1],
                                value:  variant_values[1][j]
                            });
                            product_variants_array.push({
                                add_to_store: 0,
                                name: product["name"],
                                price: product["price"],
                                sku: product["sku"],
                                out_of_stock: product["out_of_stock"],
                                stock: product["stock"],
                                minimum_quantity: product["minimum_quantity"],
                                variants: variant_values_array,
                            });
                        }
                    }
                }
                else if(variant_count == 3) {
                    for(var i = 0; i < variant_values[0].length; i++) {
                        for(var j = 0; j < variant_values[1].length; j++) {
                            for(var k = 0; k < variant_values[2].length; k++) {
                                variant_values_array = new Array();
                                variant_values_array.push({
                                    type: variant_types[0],
                                    value:  variant_values[0][i]
                                });
                                variant_values_array.push({
                                    type: variant_types[1],
                                    value:  variant_values[1][j]
                                });
                                variant_values_array.push({
                                    type: variant_types[2],
                                    value:  variant_values[2][k]
                                });
                                product_variants_array.push({
                                    add_to_store: 0,
                                    name: product["name"],
                                    price: product["price"],
                                    sku: product["sku"],
                                    out_of_stock: product["out_of_stock"],
                                    stock: product["stock"],
                                    minimum_quantity: product["minimum_quantity"],
                                    variants: variant_values_array,
                                });
                            }
                        }
                    }
                }
                else if(variant_count == 4) {
                    for(var i = 0; i < variant_values[0].length; i++) {
                        for(var j = 0; j < variant_values[1].length; j++) {
                            for(var k = 0; k < variant_values[2].length; k++) {
                                for(var l=0; l<variant_values[3].length;l++){
                                variant_values_array = new Array();
                                variant_values_array.push({
                                    type: variant_types[0],
                                    value:  variant_values[0][i]
                                });
                                variant_values_array.push({
                                    type: variant_types[1],
                                    value:  variant_values[1][j]
                                });
                                variant_values_array.push({
                                    type: variant_types[2],
                                    value:  variant_values[2][k]
                                });
                                variant_values_array.push({
                                    type: variant_types[3],
                                    value:  variant_values[3][l]
                                });
                                product_variants_array.push({
                                    add_to_store: 0,
                                    name: product["name"],
                                    price: product["price"],
                                    sku: product["sku"],
                                    out_of_stock: product["out_of_stock"],
                                    stock: product["stock"],
                                    minimum_quantity: product["minimum_quantity"],
                                    variants: variant_values_array,
                                });
                                }
                            }
                        }
                    }
                }
                @this.getProductVariants(product_variants_array);
            }
        }

        function checkProductsCount($this) {
            if($this.checked) {
                selected_product_variants_count++;
                $("#select_products_alert_div").hide();
                $("#add_selected_prdocuts_btn").prop('disabled', false);
                if(selected_product_variants_count >= 50) {
                    $("#selected_products_alert_div").show();
                    $("#add_selected_prdocuts_btn").prop('disabled', true);
                }
            }
            else{
                selected_product_variants_count--;
                if(selected_product_variants_count == 0) {
                    $("#select_products_alert_div").show();
                    $("#add_selected_prdocuts_btn").prop('disabled', true);
                }
                if(selected_product_variants_count < 50) {
                    $("#selected_products_alert_div").hide();
                    $("#add_selected_prdocuts_btn").prop('disabled', false);
                }
            }
            $("#selected_product_variants_count").html(selected_product_variants_count);
        }

        function addSelectedProductVariants() {
            var product_name = $("input[name='product_name[]']").map(function(){return $(this).val();}).get();
            var sku = $("input[name='sku[]']").map(function(){return $(this).val();}).get();
            var price = $("input[name='price[]']").map(function(){return $(this).val();}).get();
            var stock = $("input[name='stock[]']").map(function(){return $(this).val();}).get();
            var minimum_quantity = $("input[name='minimum_quantity[]']").map(function(){return $(this).val();}).get();
            // if(minimum_quantity == '')
            // {
            //     minimum_quantity=null;
            // }
            // else{
            //     alert("byee");
            // }
            var add_to_store = $("input[name='add_to_store[]']").map(function() {
                                    if($(this).prop("checked"))
                                        return $(this).val();
                                    else
                                        return "0";
                                }).get();
            var out_of_stock = $("input[name='out_of_stock[]']").map(function(){
                                    if($(this).prop("checked"))
                                        return $(this).val();
                                    else
                                        return "0";
                                }).get();
            $('form[name=addSelectedProductsForm]').append("<input type='hidden' name='add_to_store' value='" + add_to_store + "'>");
            $('form[name=addSelectedProductsForm]').append("<input type='hidden' name='product_name' value='" + product_name + "'>");
            $('form[name=addSelectedProductsForm]').append("<input type='hidden' name='sku' value='" + sku + "'>");
            $('form[name=addSelectedProductsForm]').append("<input type='hidden' name='price' value='" + price + "'>");
            $('form[name=addSelectedProductsForm]').append("<input type='hidden' name='stock' value='" + stock + "'>");
            $('form[name=addSelectedProductsForm]').append("<input type='hidden' name='out_of_stock' value='" + out_of_stock + "'>");
            $('form[name=addSelectedProductsForm]').append("<input type='hidden' name='minimum_quantity' value='" + minimum_quantity + "'>");
            var selectedProductVariantsFormData = $('form[name=addSelectedProductsForm]').serializeArray();
            @this.addSelectedProductVariants(selectedProductVariantsFormData);
        }

        $(function () {
            let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons);
            $.extend(true, $.fn.dataTable.defaults, {
                pageLength: 10,
            });

            $('.datatable-product-variants:not(.ajaxTable)').DataTable({ buttons: dtButtons });
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
            });
        });
    </script>
@endpush
