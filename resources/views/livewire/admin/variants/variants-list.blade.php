<div>
    <span class="h1">Variants</span>
    <button class="btn btn-primary float-right" data-toggle="modal" data-target="#variant_modal" data-keyboard="false" data-backdrop="static">Create</button>

    <!-- variants list view starts -->
    @if(count($variants) == 0)
    <!-- {{ print_r($variants)}} -->
        <h3 class=" text-center mt-2">You have not yet created any variants.</h3>
    @else
        <div class="card table-responsive mt-1" style="height: 100%;">
            <div class="m-1">
                <table class="table table-bordered">
                    <thead class="thead-light">
                        <tr>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>

                    @foreach($variants as $variant)
                        <tr>
                            <td>{{$variant->name}}</td>
                            <td>
                                <button wire:click="editVariant({{$variant->id}})" class="btn btn-sm btn-info" style="padding:8px;"><i class="feather icon-edit fa-2x"></i> </button>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    @endif
    <!-- variants list view ends -->

    <!-- create variant modal starts -->
    <div wire:ignore.self class="modal fade" id="variant_modal" tabindex="-1" role="dialog" aria-labelledby="variant_modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form id="variant_form" wire:submit.prevent="{{$edit?'updateVariant':'createVariant'}}">
                    @csrf
                    <div class="modal-header bg-white">
                        <h4 class="modal-title text-dark text-center" id="variant_modal">{{$edit?'Update Variant':'Create Variant'}}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="controls">
                                <span for="variant_name" class="h6">Variant Name</span>
                                <input type='text' class="form-control" wire:model.lazy="variant_name"  aria-describedby="variant_nameHelp" maxlength="50" required></textarea>
                                @error('variant_name')
                                    <span class="text-danger">{{ $message }}</span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">{{$edit?'Save Changes':'Create'}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- create variant modal ends -->
</div>

@section('scripts')
    <script>
 window.addEventListener('alert', event => {
             toastr[event.detail.type](event.detail.message,
             event.detail.title ?? ''), toastr.options = {
                    "closeButton": true,
                    "progressBar": true,
                }
            });
        Livewire.on('open_variant_edit_modal', id=>{
            $('#variant_modal').modal('show');
        })

        Livewire.on('close_variant_modal',id=>{
            $('#variant_modal').modal('hide');
        })

        $('#variant_modal').on('hide.bs.modal',function(){
            // @this.closeVariantEditModal();
        })
    </script>
@endsection
