<div class="">
@if($alert)
<div class="alert alert-danger" role="alert">
    <span>Enable display coupons in checkout Settings for the coupons to be effective.</span>                                                       
    <a href="/admin/userSettings#checkout-settings" class="btn btn-md btn-info">Checkout Settings</a>
</div>
@endif

@php
if(empty($meta_data['settings']['currency']))
    {
        $currency = "₹";
    }

else {
        if($meta_data['settings']['currency_selection'] == "currency_symbol")
            $currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
        else
            $currency = $meta_data['settings']['currency'];
    }

@endphp
    <span class="h1">Coupons</span>
    <button class="btn btn-primary float-right" data-toggle="modal" data-target="#coupon_modal" data-keyboard="false" data-backdrop="static"><i class="feather icon-plus"></i> Create Coupons</button>
    
    <!-- coupon list view starts -->
    @if(count($coupons)==0)
    <h3 class=" text-center mt-2">You have not yet created any coupons.</h3>
    @else
    <div class="card table-responsive mt-1" style="height: 100%;">
    <div class="m-1">
    <table class="table table-bordered">
        <thead class="thead-light">
            <tr>
                <th>Code</th>
                <th>Discount</th>
                <th>Description</th>
                <th>Start At</th>
                <th>Expires At</th>
                <th>Min. Cart Value</th>
                {{--<th>Logged in Users</th>--}}
                <th>Usage Limit</th>
                <th>Total Usage</th> 
                <th>Action</th>

            </tr>
        </thead>
    @foreach($coupons as $coupon)
      <tr>
        <td>{{$coupon->code}}</td>
        <td>{!! $coupon->data['unit']=='amount'?$currency:''!!}{{$coupon->reward}}{{$coupon->data['unit']=='percentage'?'%':'.00'}}</td>
        <td>{{$coupon->data['description']}}</td>
        <td>{{$coupon->data['start_date']}}</td>
        <td>{{$coupon->data['end_date']}}</td>
        <td>{{$coupon->data['min_cart_value']}}</td>

        {{--<td>{{$coupon->data['allow_only_logged_in_user']==TRUE?'True':'False'}}</td> --}}
        <td>{{  $coupon->data['usage_limit'] }}</td>
        <td>{{ isset($coupon_count[$coupon->id]) ? $coupon_count[$coupon->id][0]['total'] : 0}}</td>
        <td>
        <button wire:click="editCoupon({{$coupon->id}})" class="btn btn-sm btn-info" style="padding:8px;"><i class="feather icon-edit fa-2x"></i> </button>
        @if($coupon->disable==0)
        <button wire:click="disableCoupon({{$coupon->id}})" class="btn btn-sm btn-danger" style="padding:8px;"><i class="fa fa-ban fa-2x"></i></button>
        @endif
        @if($coupon->disable==1)
        <button wire:click="enableCoupon({{$coupon->id}})" class="btn btn-sm btn-success" style="padding:8px;"><i class="fa fa-check fa-2x"></i></button>
        @endif
        </td>
      </tr>
    @endforeach
    </table> 
    </div>
    </div>
    @endif

    <!-- coupon list view ends -->



    <!-- create coupon modal starts -->
    <div wire:ignore.self class="modal fade" id="coupon_modal" tabindex="-1" role="dialog" aria-labelledby="coupon_modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form id="coupon_form" wire:submit.prevent="{{$edit?'updateCoupon':'createCoupon'}}">
                    @csrf
                    <div class="modal-header bg-white">
                        <h4 class="modal-title text-dark text-center" id="coupon_modal">{{$edit?'Update Coupon':'Create Coupon'}}</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-row align-items-center">
                            <div class="col col-md-6">
                                <span for="discount" class="h6">Discount</span>
                                <input type="number" id="discount" class="form-control" wire:model.lazy="discount" aria-describedby="discountHelp" placeholder="30" required>
                                <small id="discountHelp" class="form-text text-muted">Discount that user gets (ex: 30 - can be used as 30% sale on something)</small>
                                @error('discount') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                            <div class="col-md-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" id="percentage" name="unit" value="percentage" wire:model="unit" required>
                                    <label class="form-check-label" for="percentage">
                                        Percentage
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" id="amount" name="unit" value="amount"wire:model="unit" required>
                                    <label class="form-check-label" for="amount">
                                        Amount
                                    </label>
                                </div>
                            </div>                            
                        </div>

                        <div class="form-group">
                            <div class="controls">
                                <span for="discription" class="h6">Description</span>
                                <textarea class="form-control" wire:model.lazy="description"  aria-describedby="discriptionHelp" maxlength="1000" required></textarea>
                                <small id="discriptionHelp" class="form-text text-muted">Description about coupon</small>
                                @error('description') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>

                       
                       

                        <div class="form-group">
                            <div class="form-row">
                                <div class="col">
                                    <span for="start_date" class="h6">Start Date</span>
                                    <input type="date" id="start_date" class="form-control"   wire:model.lazy="start_date" aria-describedby="start_dateHelp" placeholder="30" max="9999-12-31" required >
                                    <small id="start_dateHelp" class="form-text text-muted"> </small>
                                    @error('start_date') <span class="text-danger">{{ $message }}</span> @enderror
                                </div>
                                <div class="col">
                                    <span for="expires_in" class="h6">Expire Date</span>
                                    <input type="date" id="expires_in" class="form-control" wire:model.lazy="expires_in" aria-describedby="expires_inHelp" placeholder="30"  max="9999-12-31" required>
                                    @error('expires_in') <span class="text-danger">{{ $message }}</span> @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="controls">
                                <span for="discription" class="h6">Minimum Cart Amount</span>
                                <input type='number' class="form-control" wire:model.lazy="min_cart_value"  aria-describedby="min_cart_valueHelp" maxlength="1000" required></textarea>
                                @error('min_cart_value') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>

                        <div class="form-group" id="usage_limit_div">
                            <div class="controls">
                                <span for="usage_limit" class="h6">Number of times</span>
                                <input type='number' class="form-control" wire:model="usage_limit" placeholder="Number of times this coupon can be used"  aria-describedby="usage_limitHelp" maxlength="1000" required>
                                @error('usage_limit') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">{{$edit?'Save Changes':'Create'}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<!-- create coupon modal ends -->
</div>

@section('scripts')
<script>

Livewire.on('open_coupon_edit_modal', id=>{
    $('#coupon_modal').modal('show');
})

Livewire.on('close_coupon_modal',id=>{
    $('#coupon_modal').modal('hide');
})

$('#coupon_modal').on('hide.bs.modal',function(){
    @this.closeCouponEditModal();
})


</script>
@endsection


