<div>
    <div class="content-header row ">
        <div class="content-header-left col-md-9 col-12 mb-2 ">
            <div class="row breadcrumbs-top ">
                <div class="col-12 pb-0">
                    <a href="{{ route('admin.customers.index')}}" >
                        <h4 class="text-primary mb-0 pb-0"><i class="feather icon-arrow-left"></i>Back to Customers</h4>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="ecommerce-application">
        <section id="ecommerce-searchbar">
            <div class="row align-items-end pt-0">
                <div class="col order-md-0 order-1 ">
                    <h2 class="pb-0 ">Customer Details</h2>
                </div>
                <div class="col-md-4 col-sm-12  order-md-1  order-0 ">
                    <fieldset class="form-group position-relative">
                        <!-- <input type="text" class="form-control search-product" id="order_search" placeholder="Search order id here">
                        <div class="form-control-position">
                            <i class="feather icon-search"></i>
                        </div> -->
                    </fieldset>
                </div>
            </div>
        </section>
    </div>
    <div class="card ecommerce-card">
        <div class="card-body">
            <div class="row">
                <div class="col col-md-4 order-1">

                    <h4><b>{{$customer->fname}} {{$customer->lname ?? ""}}</b></h4>

                    <h4>{{$customer->mobileno ?? ""}}</h4>

                    <h4>{{$customer->email ?? ""}}</h4>

                </div>
                <div class="col col-md-4 order-2 d-flex flex-md-row flex-column justify-content-md-center justify-content-end">

                    <p><a href="https://wa.me/{{$customer->country_code ? $customer->country_code.$customer->mobileno : '91'.$customer->mobileno}}" target="_blank"><i style="color:#25D366" class="fa fa-whatsapp fa-2x"></i></a></p>&nbsp;&nbsp;&nbsp;&nbsp;
                    <p><a href="tel:{{$customer->mobileno}}" style="padding-left:5px"><i style="color:#2F91F3" class="fa fa-phone fa-2x"></i></a></p>&nbsp;&nbsp;&nbsp;&nbsp;
                    <!-- <p><a href="#" onclick='' style="padding-left:5px"><i style="color:red" class="fa fa-file-pdf-o fa-2x"></i></a></p> -->

                </div>
                <div class="col col-md-4 order-3 text-md-right">

                    <h4><b>No of Orders : {{$orders_count }} </b></h4>
                    @php
                    $customer_meta_data= json_decode( $customer->meta_data, true);
                    @endphp

                    {{--<h4><b>Total Amount: {!! $customer_meta_data["currency"] .$total_amount !!} </b></h4> --}}
                </div>

            </div>
        </div>

    </div>


    <section id="ecommerce-products">
        <div class="justify-content-center m-5" wire:loading.flex>
            <div class="spinner-border" role="status">
            </div>
        </div>

        @if($orders->total()==0)
        <h3 class="text-light text-center p-2 w-100">{{$customer_info_text}}</h3>
        @else
        <div wire:loading.remove>
            <h2>Order Details</h2>
            <div class="row" id="order_list">
                @foreach ($orders as $customer_order)
                @php
                $customer_order_meta_data= json_decode( $customer_order->meta_data, true);
                @endphp
                @if(isset($customer_order->order->transactions->amount))

                <div class="col-md-4 col-sm-12">

                    <div class="card ecommerce-card">
                        <div class="card-body d-flex">
                    
                            <div class="float-left">

                                <h5 class="card-title">Order<b>#{{ $customer_order->order->order_ref_id}}</b></h5>
                                <br>

                                <p><a href="{{ route('admin.export_order_pdf',  $customer_order->order_id) }}" style="padding-left:5px"><i style="color:red" class="fa fa-file-pdf-o fa-2x"></i></a></p>

                            </div>

                            <div class="float-right text-right cursor-pointer" style="width:75%; height:100%;" onclick="location.href='{{route('admin.customers.orderDetails', $customer_order->id)}}';">

                                <h6>{{ date('d-m-Y', strtotime( $customer_order->created_at)) }}</h6>
                                <h6>{{ date('h:i:s A', strtotime( $customer_order->created_at)) }}</h6>

                                <h4><b>No. of Products: {{count( $customer_order->orderProducts)}}</b></h4>
                                 <h4><b>{!! $customer_order_meta_data["currency"] !!} {{ $customer_order->order->transactions->amount }} </h4></b>
                                <!-- <h4><b>{!! $customer_order_meta_data["currency"].''. ($customer_order_meta_data['product_total_price']==0 ? '00.00' :  $customer_order_meta_data['product_total_price']) !!}</b></h4> -->

                                
                                @if(!empty($customer_order->order->status))
                                @php
                                if(App\orders::STATUS[$customer_order->order->status] == 'Order Recieved')
                                $class="text-primary";
                                elseif (App\orders::STATUS[$customer_order->order->status] == 'Pending')
                                $class="text-warning";
                                elseif ( in_array(App\orders::STATUS[$customer_order->order->status] , ['Accepted','Shipped', 'Delivered']))
                                $class="text-success";
                                elseif ( in_array(App\orders::STATUS[$customer_order->order->status] ,['Rejected','Cancelled','Failed']))
                                $class="text-danger";
                                @endphp
                                <h4>Status: <span class='{{$class}}'>{{App\orders::STATUS[ $customer_order->order->status]}}</span></h4>
                                @endif
                            </div>


                        </div>
                    </div>
                </div>
                @endif   

                @endforeach
            </div>
            {{ $orders->links() }}
        </div>

        @endif
    </section>

</div>
@section('scripts')
@parent

<script>
    //emiting the event on order search
    $('#order_search').on('input', function() {
        Livewire.emit('SearchOrder', $('#order_search').val())
    });
</script>
@endsection