@section('styles')
@parent
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
@endsection

<div>

    <!-- condition to check whether the div should be shown or not - customer list-->
    <div class="ecommerce-application">
        <section id="ecommerce-searchbar">
            <div class="row justify-content-end">
                <div class="col">
                    <ul class="nav nav-tabs" role="tablist">
                        <!-- tabs -->
                        <li class="nav-item">
                            <a class="nav-link {{$register_tab?'active':'' }}" wire:click="RegisteredCustomersList" id="registered-customers-tab" aria-selected="{{$register_tab}}">Registered Customers (<span>{{$registered_customers_count}}</span>)</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{$guest_tab?'active':'' }}" wire:click="GuestCustomersList" id="guest-customers-tab" aria-selected="{{$guest_tab}}">Guest Customers (<span>{{$guest_customers_count}}</span>)</a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 col-sm-12">
                    <fieldset class="form-group position-relative">
                        <!-- search box -->
                        <!-- <input type="text" id="customer_search" class="form-control search-product" placeholder="search here">
                        <div class="form-control-position">
                            <i class="feather icon-search"></i>
                        </div> -->
                    </fieldset>
                </div>
        </section>
    </div>

    <div class="customers">
        <!-- show if customers exist -->
        <div class="justify-content-center m-5" wire:loading.flex>
            <div class="spinner-border" role="status">
            </div>
        </div>

        @if($customers->total()==0)
            <h3 class="text-light text-center p-2 w-100">{{$customer_info_text}}</h3>
        @else
        @foreach ($customers as $customer)
        <!-- customers card -->
        <div class="card ecommerce-card"  wire:loading.remove>

            <div class="card-body">
                <div class="row ">
                    <div class="col-10 col-md-4 cursor-pointer"  onclick="location.href='{{route('admin.customers.details',$customer->id)}}'">

                        <h4><b>{{$customer->fname}} {{$customer->lname ?? ""}}</b></h4>

                        <h4>{{$customer->mobileno ?? ""}}</h4>

                        <h4>{{$customer->email ?? ""}}</h4>

                    </div>
                    <div class="col col-md-4  d-flex flex-md-row flex-column justify-content-md-center justify-content-end">

                        <p><a href="https://wa.me/{{ isset($customer->country_code) ? $customer->country_code.$customer->mobileno : '91'.$customer->mobileno}}" target="_blank"><i style="color:#25D366" class="fa fa-whatsapp fa-2x"></i></a></p>&nbsp;&nbsp;&nbsp;&nbsp;
                        <p><a href="tel:{{$customer->mobileno}}" style="padding-left:5px"><i style="color:#2F91F3" class="fa fa-phone fa-2x"></i></a></p>&nbsp;&nbsp;&nbsp;&nbsp;

                    </div>
                    <div class="col-sm-12 col-md-4 text-md-right cursor-pointer"  onclick="location.href='{{route('admin.customers.details',$customer->id)}}'">

                        <h4><b>No. of Orders: {{$customer->total_orders}}</b></h4>
                        <h4><b>Total Amount: {!! $currency.''.$customer->total_amount !!}</b></h4>

                    </div>

                </div>
            </div>

        </div>

        @endforeach
        <!-- pagination -->
        {{ $customers->links() }}
        @endif
    </div>
</div>
@section('scripts')
@parent

<script>
    //emiting the event on customer search
    $('#customer_search').on('input', function() {
        Livewire.emit('SearchCustomer', $('#customer_search').val())
    });
    
</script>
@endsection