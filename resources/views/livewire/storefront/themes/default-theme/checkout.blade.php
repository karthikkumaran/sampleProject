@section ('styles')
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/plugins/forms/wizard.css')}}">
@endsection
<div class="mx-1">
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <div class="breadcrumb-wrapper col-12" style="margin-left: -44px;">
                        <ol class="breadcrumb border-0 ">
                            <li class="breadcrumb-item ml-2" onclick="track_events('breadcrumb-home-button-clicked',null)" style="font-size:14px;color: #0B1A89 !important;"><a class="text-green" href="{{$homeLink}}"><i class="fa fa-home mr-1"></i>Home</a></li>
                            <!-- <li class="breadcrumb-item"><a class="text-green" href="{{$catalogLink}}">{!! $campaign->name == "Untitled"?"&nbsp;":$campaign->name !!}</a></li> -->
                            <li class="breadcrumb-item active" style="font-size:14px">Checkout</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(isset($meta_data['checkout_settings']['minimum_checkout_value']))
    <div class="content-header row" id="minimum_checkout_alert" style="display: none;">
        <div class="col-lg-12">
            <div class="alert alert-danger" role="alert">
            Your cart should contain items worth more than {!! $currency !!}{{$meta_data['checkout_settings']['minimum_checkout_value']}} to place an order.
            </div>
        </div>
    </div>
    @endif
    <div class="content-body ecommerce-application mx-1">
        <form action="#" name="checkout_form" class="icons-tab-steps checkout-tab-steps wizard-circle">
            @csrf
            <input type="hidden" value="{{$campaign->public_link}}" name="key" />
            <!-- coupon fields -->
            <input type="hidden" name='coupon_code_applied' id="coupon_code_applied">
            <input type="hidden" name='coupon_discount_amount' id="coupon_discount_amount">
            <input type="hidden" name='coupon_id' id="coupon_id">
            <input type="hidden" name='tax_percentage' id="tax_percentage">
            <input type="hidden" name='tax_amount' id="tax_amount">
            <input type="hidden" name='shipping_charges' id="shipping_charges">
            <input type="hidden" name='totalamount' id="totalamount">
            <input type="hidden" name="sphylite_charge" id="sphylite_charge">
            <!-- Checkout Place order starts -->
            <h6><i class="step-icon step feather icon-shopping-cart"></i>Cart</h6>
            <fieldset class="checkout-step-1 px-0">
                <section id="place-order" class="list-view product-checkout ">
                    <div class="row">

                    <div class="checkout-items p-1 w-100">

                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title text-right">Enter Your Details</h4>
                            @if(!Auth::guard('customer')->check())
                                <h5 class="text-left">Already a customer? <button type="button" class="btn btn-link p-0 text-green" id='checkout_login_link'>Login</button> / <button type="button" class="btn btn-link p-0 text-green" id='checkout_signup_link'>Sign up</button></h5>
                            @endif
                        </div>
                        <div class="card-content">
                            <div class="card-body">
                                <h6><span>*</span> Required fields</h6>
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="checkout-name">Full Name<span class="text-danger">*</span>:</label>
                                            <input type="text" id="checkout-name" class="form-control required" name="fname" value="{{ isset($customer)? $customer['name']:''}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group row">
                                            <div class="col-6">
                                                <label for="country_code">Country code</label>
                                                <select id="checkout-country_code" class="form-control required" name="country_code">
                                                    @foreach(App\Customer::MOBILE_COUNTRY_CODE as $key=>$value)
                                                        <option value="{{$key}}" {{ isset($customer["country_code"]) && $customer["country_code"] == $key ? 'selected' : '' }}>{{$value}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-6">
                                                <label for="checkout-number">Mobile Number<span class="text-danger">*</span>:</label>
                                                <input type="tel" id="checkout-mnumber" class="form-control required" name="mnumber"  value="{{ isset($customer)? $customer['mobile'] :''}}" placeholder="Contact Number here..." onkeypress="return isNumberKey(event,this)" pattern="[0-9]{10}" maxlength="10">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="checkout-number">Alternative Mobile Number:</label>
                                            <input type="number" id="checkout-amnumber" class="form-control" name="amnumber" value="{{ isset($customer['amobileno'])? $customer['amobileno'] :''}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="checkout-email">Email<span class="text-danger">*</span>:</label>
                                            <input type="email" id="checkout-email" class="form-control required" name="email" value="{{  isset($customer)? $customer['email'] :''}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="checkout-apt_number">Flat, House No, Street<span class="text-danger">*</span>:</label>
                                            <input type="text" id="checkout-apt_number" class="form-control required" name="apt_number" value="{{ isset($customer['address1'])? $customer['address1'] :''}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="checkout-landmark">Landmark e.g. near apollo hospital:</label>
                                            <input type="text" id="checkout-landmark" class="form-control" name="landmark" value="{{ isset($customer['address2'])? $customer['address2'] :''}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="checkout-city">Town/City<span class="text-danger">*</span>:</label>
                                            <input type="text" id="checkout-city" class="form-control required" name="city" value="{{ isset($customer['city'])? $customer['city'] :''}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="checkout-pincode">Pincode<span class="text-danger">*</span>:</label>
                                            <input type="number" id="checkout-pincode" class="form-control required" name="pincode" value="{{ isset($customer['pincode'])? $customer['pincode'] :''}}">
                                            <span id="pinservice" class="text-danger text-sm"></span>
                                            <!-- <button type="button" class="btn btn-primary mb-1 delivery-address float-right" id="check_pin">CHECK</button> -->
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="checkout-state">State<span class="text-danger">*</span>:</label>
                                            <input type="text" id="checkout-state" class="form-control required" name="state" value="{{ isset($customer['state'])? $customer['state'] :''}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="add-type">Address Type:</label>
                                            <select class="form-control" id="add-type" name="addresstype">
                                                <option "{{ isset($customer['addresstype'])&& $customer['addresstype']=='Home' ? 'selected':''}}" >Home</option>
                                                <option "{{ isset($customer['addresstype'])&& $customer['addresstype']=='work' ? 'selected':''}}" >Work</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <!-- <button type="reset" class="btn btn-secondary mb-1 float-left">CLEAR ADDRESS</button>
                                <button type="button" class="btn btn-primary mb-1 delivery-address float-right" id="delivery_address">NEXT</button> -->
                            </div>
                        </div>
                    </div>


                    </div>
                    <div class="checkout-options">
                        <div class="card">
                            <div class="card-content">
                                <div class="card-body">
                                    <div class="price-details">
                                        <p>Order Summary</p>
                                    </div>
                                    <div class="detail" style="display:none;">
                                        <div class="detail-title">Total MRP</div>
                                        <div class="detail-amt mrp-amt">

                                        </div>
                                    </div>
                                    <hr>
                                    <div class="detail">
                                        <div class="detail-title detail-total">Items</div>
                                        <div class="detail-amt total-amt text-dark"></div>
                                    </div>
                                    {{-- <div class="detail">
                                        <div class="detail-title detail-total">Discount</div>
                                        <div class="detail-amt discount-amt text-dark text-bold-700">- {!! $currency !!}0</div>
                                    </div> --}}
                                    @if(isset($meta_data['checkout_settings']['display_coupons']) && $meta_data['checkout_settings']['display_coupons'] == 'on')
                                        <div class="detail">
                                            <div class="detail-total">Coupon Discount</div>
                                            <div class="detail-amt coupon-discount-amt text-dark">- {!! $currency !!}0</div>
                                        </div>
                                    @endif
                                    @if(empty($meta_data['shipping_settings']['shipping_method']))
                                        <div class="detail">
                                            <div class="detail-total">Shipping</div>
                                            <div class="detail-amt delivery-charge-amt text-dark">{!! $currency !!}0</div>
                                        </div>
                                    @endif
                                    @if(isset($meta_data['checkout_settings']['extra_charges']) && count($meta_data['checkout_settings']['extra_charges']) > 0)
                                        @foreach($meta_data['checkout_settings']['extra_charges'] as $key => $value)
                                            <div class="detail">
                                                <div class="detail-total">{!! $value['extra_charge_name'] !!}</div>
                                                <div class="detail-amt extra-charge-amt text-dark" id="extra_charge_amt_{{$value['extra_charge_id']}}"></div>
                                            </div>
                                        @endforeach
                                    @endif
                                    @if(isset($meta_data['checkout_settings']['tax_settings']) && $meta_data['checkout_settings']['tax_settings'] == '1')
                                        <div class="detail">
                                            <div class="detail-title detail-total">Total before tax</div>
                                            <div class="detail-amt before-tax-amt text-dark text-bold-700"></div>
                                        </div>
                                    @endif

                                    @if(isset($meta_data['checkout_settings']['tax_settings']) && $meta_data['checkout_settings']['tax_settings'] == '1')
                                        <div class="detail">
                                            <div class="detail-total">Tax ({{$meta_data['checkout_settings']['tax_setting_details']['gst_percentage']}}% GST)</div>
                                            <div class="detail-amt tax-amt text-dark"></div>
                                        </div>
                                    @endif
                                    @if(!empty($meta_data['shipping_settings']['shipping_method']))
                                        <div class="detail">
                                            <div class="detail-total">Shipping</div>
                                            <div class="detail-amt delivery-charge-amt text-dark">{!! $currency !!}0</div>
                                        </div>
                                    @endif
                                    <div class="detail">
                                        <div class="detail-title detail-total">Total to pay</div>
                                        <div class="detail-amt grand-amt text-dark text-bold-700" id="totalpay"></div>
                                    </div>
                                    <hr>
                                    @if(isset($meta_data['checkout_settings']['display_coupons']) && $meta_data['checkout_settings']['display_coupons'] == 'on')
                                        <div class="detail-title detail-total"><b>Coupon Code</b></div><br>
                                        <div class="input-group">
                                            <input class="form-control" name="couponCode" id="coupon_code">
                                            <div class="alert alert-success alert-dismissible"  id="coupon_alert" style="width:10rem;" hidden>
                                                <a href="javascript:void(0);" class="close" id="coupon_alert_close"  aria-label="close">&times;</a>
                                                <strong><span id="coupon_badge" ><span></strong>
                                            </div>
                                        </div>
                                        <small class="text-danger text-sm" id="coupon_error_message"></small>
                                        <small class="text-success text-sm" id="coupon_success_message"></small>
                                        <button type="button" class="mt-1 btn btn-primary btn-block apply_coupon cursor-pointer" id="apply_coupon">Apply Coupon</button>
                                        <hr>
                                    @endif
                                    <div class="detail-title detail-total"><b>Special Notes</b></div><br>
                                    <div class="input-group">
                                        <textarea class="form-control" aria-label="With textarea" name="order_notes" maxlength="1000"></textarea>
                                    </div>
                                    @if(in_array( 'Enquiry' , $payment_settings['payment_method']))
                                        <button type="button" class=" mt-1 btn btn-primary btn-block place-order cursor-pointer" onclick="track_events('order-enquire-button-clicked',null)" id="enquiry_btn">Enquire</button>
                                    @endif
                                    @if(in_array( 'Enquiry' , $payment_settings['payment_method']) && count( $payment_settings['payment_method'])>1)
                                        <h4 class="text-center mt-1">-OR-</h4>
                                    @endif
                                    @if( !in_array( 'Enquiry' , $payment_settings['payment_method']) || count($payment_settings['payment_method'])>1)
                                        <button type="button" class=" mt-1 btn btn-primary btn-block place-order cursor-pointer" onclick="track_events('place-order-button-clicked',null)" id="place_order_btn">Place Order</button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </fieldset>
            <!-- Checkout Place order Ends -->

            <!-- Checkout Customer Address Starts -->
            <!-- <h6><i class="step-icon step feather icon-home"></i>Address</h6>
            <fieldset class="checkout-step-2 px-0">
                <section id="checkout-address" class="product-checkout">

                </section>
            </fieldset> -->
            <!-- Checkout Customer Address Ends -->

            <!-- checkout payment starts -->
            <h6><i class="step-icon step fa fa-money"></i>Payment</h6>
            <fieldset class="checkout-step-2 px-0">
                <section id="checkout-payment" class="product-checkout">
                <p><b>The total amount to pay : </b><span class="detail-amt grand-amt text-dark text-bold-700"></span></p><br>
                    <h4 class="mb-1"><b>Select a payment method</b></h4>
                    <div class="row">
                        @foreach($payment_settings['payment_method'] as $method)
                            @if($method == "Enquiry")
                                @continue
                            @endif
                            @if($method=="Razorpay")
                                <div class="col-12 col-md-3 col-sm-6">
                                    <div class="card text-center cursor-pointer paymentMethod" data-method="{{$method}}" id="{{str_replace(' ','-',$method)}}" style="max-height:101px;">
                                        <div class="card-body">
                                            <div class="avatar-content">
                                                <img src="{{ asset('XR\app-assets\images\icons\razorpay_logo.svg') }}" style="width:150px;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if($method=="Cash on delivery")
                                <div class="col-12 col-md-3 col-sm-6">
                                    <div class="card text-center cursor-pointer paymentMethod" data-method="{{$method}}" id="{{str_replace(' ','-',$method)}}" style="max-height:101px;">
                                        <div class="card-body">
                                            <div class="avatar-content">
                                                <img src="{{ asset('XR\app-assets\images\icons\cash_on_delivery.png') }}" style="width:31px;margin-left:-11px"> <span style='font-size:18px' class="text-dark" onclick="track_events('cash-on-delivery-button clicked',null)"><b>Cash on Delivery</b></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if($method=="Paypal")
                                <div class="col-12 col-md-3 col-sm-6">
                                    <div class="card text-center cursor-pointer paymentMethod" data-method="{{$method}}" id="{{str_replace(' ','-',$method)}}" style="max-height:101px;">
                                        <div class="card-body">
                                            <div class="avatar-content">
                                                <img src="{{ asset('XR\app-assets\images\icons\paypal_logo.svg') }}" style="width:129px;margin:-50px">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            @if($method=="UPI")
                                <div class="col-12 col-md-3 col-sm-6">
                                    <div class="card text-center cursor-pointer paymentMethod" data-method="{{$method}}" id="{{str_replace(' ','-',$method)}}" style="max-height:101px;">
                                        <div class="card-body">
                                            <div class="avatar-content">
                                                <img src="{{ asset('XR\app-assets\images\icons\upi.svg') }}" style="width:129px;margin:-16px">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                        @endforeach
                    </div>
                    <hr>
                    @foreach($payment_settings['payment_method'] as $method)
                        <div class="pay_description" id="desc_{{str_replace(' ','-',$method)}}" hidden=true>
                            {{$payment_settings[$method]['desc-text'] ?? ""}}
                        </div>
                    @endforeach
                    <button type="button" class="btn btn-primary make-payment float-right " id="make_payment" disabled>Make Payment</button><br><br>
                    <div class="float-right" id="paypal-button-container" style="margin-top:-40px;"></div>
                </section>
            </fieldset>
            <!-- checkout payment ends -->
        </form>
    </div>
</div>
@section('scripts')
@parent
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="{{asset('XR/app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js')}}"></script>
<script src="{{asset('XR/app-assets/vendors/js/extensions/jquery.steps.min.js')}}"></script>
<script src="{{asset('XR/app-assets/vendors/js/forms/validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('js/customer.js')}}"></script>
<script>
   
    var cart = [];
    var obj = {};
    var slink = "";
    var payment_method = '';
    var init_id = "";
    var place_order_btn_disabled = 0;
    var coupon_applied=false;
    var min_cart_value='';
    var coupon_amount = '';
    var coupon_code='';
    var coupon_unit='';
    var coupon_id='';
    var coupon_desc='';
    var discount=0;
    var shippingcharge=0;
    var shyplitecharge=0;
    var coupon_discount=0;
    var currency_code = "{{html_entity_decode($currency)}}";
    var currency = String(currency_code);
    var url;
    var order_id;
    var today = new Date();
    var expiry = new Date(today.getTime() + 30 * 24 * 3600 * 1000); // plus 30 days
    var items_amount = 0;
    var minimum_checkout_value = {{$meta_data['checkout_settings']['minimum_checkout_value'] ?? 0}};

    function payment_selection(id) {
        if (id != init_id) {
            $('#' + id).addClass('border-primary border-3 p-0');
            payment_method = $('#' + id).attr('data-method');
            if (init_id != "")
                $('#' + init_id).removeClass('border-primary border-3');
            init_id = id;
        } else if (id == init_id) {
            return;
        }

        $('.pay_description').prop('hidden', true);
        $('#desc_' + id).prop('hidden', false);

        if (payment_method == "Cash on delivery") {
            if ($('.make-payment').html != 'Place Order')
                payment_button('Place Order');
        } else if (payment_method == "Paypal") {
            $('.make-payment').attr('hidden', 'hidden');
            $('#paypal-button-container').removeAttr('hidden');
            $('.make-payment').click();
        } else {
            if ($('.make-payment').html != 'Make Payment')
                payment_button('Make Payment');
        }
    }

    function payment_button(method_name) {
        if(document.getElementById("make_payment").innerHTML.trim() != "SUBMIT"){
        $('.make-payment').attr('disabled', false);
        $('.make-payment').removeAttr('hidden');
        $('.make-payment').html(method_name);
        $('#paypal-button-container').attr('hidden', 'hidden');
        }
        else{
            $('.make-payment').attr('disabled', false);
        }
    }

    $(document).ready(function() {
        "use strict";
        // Checkout Wizard
        var checkoutWizard = $(".checkout-tab-steps"),
            checkoutValidation = checkoutWizard.show();
        if (checkoutWizard.length > 0) {
            $(checkoutWizard).steps({
                headerTag: "h6",
                bodyTag: "fieldset",
                transitionEffect: "fade",
                titleTemplate: '<span class="step">#index#</span> #title#',
                enablePagination: false,
                onStepChanging: function(event, currentIndex, newIndex) {
                    // allows to go back to previous step if form is
                    if (currentIndex > newIndex) {
                        return true;
                    }

                    // console.log("test step"+ currentIndex)
                    // console.log("test step new"+ newIndex)

                    // Needed in some cases if the user went back (clean up)
                    if (currentIndex < newIndex) {
                        // To remove error styles
                        checkoutValidation.find(".body:eq(" + newIndex + ") label.error").remove();
                        checkoutValidation.find(".body:eq(" + newIndex + ") .error").removeClass("error");
                    }
                    // check for valid details and show notification accordingly
                    // if(getCookie("fname")){
                    // if (newIndex == 1) {
                    //     @if(!auth()->guard('customer')->check() || $customer['address']==FALSE)
                    //         document.checkout_form.fname.value = getCookie("fname");
                    //         document.checkout_form.mnumber.value = getCookie("mnumber");
                    //         document.checkout_form.amnumber.value = getCookie("amnumber");
                    //         document.checkout_form.email.value = getCookie("email");
                    //         document.checkout_form.apt_number.value = getCookie("apt_number");
                    //         document.checkout_form.landmark.value = getCookie("landmark");
                    //         document.checkout_form.city.value = getCookie("city");
                    //         document.checkout_form.pincode.value = getCookie("pincode");
                    //         document.checkout_form.state.value = getCookie("state");
                    //         document.checkout_form.addresstype.value = getCookie("addresstype");
                    //     @endif
                    // }
                    // }

                    // console.log("test steps prior")

                    checkoutValidation.validate().settings.ignore = ":disabled,:hidden";
                    // console.log(checkoutValidation.valid())

                    // if (checkoutValidation.valid()) {

                    // console.log('FORM VALID');

                    // } else {

                    // console.log('FORM INVALID');

                    // var validator = checkoutValidation.validate();

                    // $.each(validator.errorMap, function (index, value) {

                    //     console.log('Id: ' + index + ' Message: ' + value);

                    // });

                    // }

                    return checkoutValidation.valid();
                },
                onStepChanged: function(event, currentIndex, priorIndex) {
                    event.preventDefault();
                    // console.log("test step change new"+ currentIndex)
                    // console.log("test step change new"+ priorIndex)
                    $('.paymentMethod').click(function() {
                        payment_selection($(this).attr('id'));
                    });
                },
                onFinished: function(event, currentIndex) {
                    checkoutValidation.validate().settings.ignore = ":disabled,:hidden";
                    if (checkoutValidation.valid()) {
                        track_events('confirm_order', null, slink);

                        var $this = $('.make-payment');
                        var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> loading...';

                        @if(!auth()->guard('customer')->check() || $customer['address']==FALSE)
                            setCookie("fname", document.checkout_form.fname.value);
                            setCookie("mnumber", document.checkout_form.mnumber.value);
                            setCookie("amnumber", document.checkout_form.amnumber.value);
                            setCookie("email", document.checkout_form.email.value);
                            setCookie("apt_number", document.checkout_form.apt_number.value);
                            setCookie("landmark", document.checkout_form.landmark.value);
                            setCookie("city", document.checkout_form.city.value);
                            setCookie("pincode", document.checkout_form.pincode.value);
                            setCookie("state", document.checkout_form.state.value);
                            setCookie("addresstype", document.checkout_form.addresstype.value);
                        @endif

                        if ($this.html() !== loadingText) {
                            $this.data('original-text', $this.html());
                            // $this.html(loadingText);
                            cart = JSON.parse(localStorage.getItem('shoppingCart'));
                            var products = [];
                            var catalogs = [];
                            var products_qty = [];
                            var products_notes = [];
                            var sphylite_amount = 0;

                            sphylite_amount = $('#sphylite_charge').val();

                            if (cart.length > 0) {
                                for (var item in cart) {
                                    products.push(cart[item].sku);
                                    var id = cart[item].id,
                                        qty = cart[item].qty,
                                        sku=cart[item].sku,
                                        catalogid = cart[item].catalogid;

                                    for (var catalog in cart) {
                                        if (catalogs.indexOf(catalogid) == -1)
                                            catalogs.push(catalogid);
                                    }
                                    if ($("#product_notes_" + cart[item].id).val() != "") {
                                        var product_note = $("#product_notes_" + cart[item].id).val();
                                    } else {
                                        var product_note = "";
                                    }
                                    $('form[name=checkout_form]').append("<input type='hidden' id='product_qty_" + sku + "' name='products_qty[" + sku + "]' value='" + qty + "'>");
                                    $("form[name=checkout_form]").append("<input type='hidden' id='product_notes_" + cart[item].sku + "' name='product_notes[" + cart[item].sku + "]' value='" + product_note + "'>");
                                }
                                $('form[name=checkout_form]').append("<input type='hidden' name='products' value='" + products + "'>");
                                $('form[name=checkout_form]').append("<input type='hidden' name='catalogs' value='" + catalogs + "'>");
                                $('form[name=checkout_form]').append("<input type='hidden' name='currency' value='" + currency + "'>");
                                $('form[name=checkout_form]').append("<input type='hidden' name='sphylite_amount' value='" + sphylite_amount + "'>");
                            }
                            formSubmit($('form[name=checkout_form]').serialize(), $this);

                        }
                    }
                },
            });

            @if(!auth()->guard('customer')->check() || $customer['address']==FALSE)
                            document.checkout_form.fname.value = getCookie("fname");
                            document.checkout_form.mnumber.value = getCookie("mnumber");
                            document.checkout_form.amnumber.value = getCookie("amnumber");
                            document.checkout_form.email.value = getCookie("email");
                            document.checkout_form.apt_number.value = getCookie("apt_number");
                            document.checkout_form.landmark.value = getCookie("landmark");
                            document.checkout_form.city.value = getCookie("city");
                            document.checkout_form.pincode.value = getCookie("pincode");
                            document.checkout_form.state.value = getCookie("state");
                            document.checkout_form.addresstype.value = getCookie("addresstype");
            @endif
            //apply coupon code
            $('#apply_coupon').click(function(){applyCouponCode()});
            $('#coupon_alert_close').click(function(){CouponCodeInput()});
    $('#checkout-pincode').keyup(function(e){
                var pin = $(this).val();
                // console.log(pin);
        var source_code = "";
        var destination_code = "";
        var public_key_id = "";
        var app_id = "";
        var sell_id = "";
        var private_key = "";
        var data="";
        var pricing="";
        var service="";
        var weight=0;
        var length=0;
        var width =0;
        var height=0;
        var shareableLink = "{{ $this->shareableLink }}";

        cart = JSON.parse(localStorage.getItem('shoppingCart'));
        if(cart.length > 0){
            for (var item in cart) {
                weight = weight + cart[item].weight;
                length = length + cart[item].length;
                width = width + cart[item].width;
                height = height + cart[item].height;
            }
        }
        else{
            weight = cart[item].weight;
            length = cart[item].length;
            width = cart[item].width;
            height = cart[item].height;
        }
        if(pin.length == 6 && weight > 0.5){
            @php
            $shipdata = '';
            if(isset($meta_data['shipping_settings'])){
                foreach($meta_data['shipping_settings']['shipping_method'] as $val){
                    $shipdata = $val;
                }
            }
            @endphp

            @if(isset($meta_data['shipping_settings']['shipping_method']) && !empty($shipdata))
                @if(isset($meta_data['footer_settings']['pincode']))
                source_code = {{$meta_data['footer_settings']['pincode']}};
                @endif
                destination_code = pin;
                // public_key_id = "{{$meta_data['shipping_settings']['shipping']['public-key-id']}}";
                // app_id = {{$meta_data['shipping_settings']['shipping']['sp-app-id']}};
                // sell_id = {{$meta_data['shipping_settings']['shipping']['sp-sell-id']}};
                // private_key = "{{$meta_data['shipping_settings']['shipping']['sp-private-key']}}";
                data = '&source_code='+ source_code +'&destination_code=' + destination_code + '&weight=' + weight + '&height=' + height +
                '&width=' + width + '&length=' + length + '&shareable_link='+ shareableLink;
                // alert(data);
                // + '&public_key_id=' + public_key_id + '&app_id=' + app_id + '&sell_id=' + sell_id + '&private_key=' + 'private_key';
                $.ajax({
                url: "{{route('shipping.price')}}",
                type: "POST",
                data: data,
                datatype:"json",
                success: function(data) {
                    $('#loading-bg').hide();
                    if(data){
                        data =  JSON.parse(data);
                        let displayShip = [];
                        for(var service in data["service"]){

                            if(data["service"][service]){
                                let name = service.replace(/[^0-9.]/g, "") //remove logic
                                displayShip.push({
                                    name: name,
                                    price: data["pricing"][service]
                                })
                            }
                        }
                            for(var weigh in displayShip){
                                if(weight < displayShip[weigh]["name"] ){
                                    var shippingcharge = displayShip[weigh]["price"];
                                        shyplitecharge = displayShip[weigh]["price"];
                                    var breakout = true;
                                }
                            }
                            if(shippingcharge && displayShip.length > 0)
                            {
                            $('#sphylite_charge').val(shippingcharge);
                            $('.delivery-charge-amt').html('{!! $currency !!}' + Number(shippingcharge.toFixed(2)));
                            var totalamount = $('#totalamount').val();
                            $('#pinservice').text('');
                            $('.place-order').attr("disabled", false);
                            // var totalamount = $('#totalpay').text();
                            // totalamount =totalamount.replace(/[^0-9.]/g, "");
                            totalamount = parseFloat(totalamount) + parseFloat(shippingcharge);
                            $('.grand-amt').html('{!! $currency !!}' + Number(totalamount.toFixed(2)));
                            }
                            else{
                                // alert('Sorry we are unable to deliver to your pincode:'+pin);
                                $('#pinservice').text('Sorry we are unable to deliver to your pincode:'+pin);
                                $('.place-order').attr("disabled", true);
                                var shippingcharge = 0;
                                $('#sphylite_charge').val(shippingcharge);
                                $('.delivery-charge-amt').html('{!! $currency !!}' + Number(shippingcharge.toFixed(2)));
                                var totalamount = $('#totalamount').val();
                                // var totalamount = $('#totalpay').text();
                                // totalamount =totalamount.replace(/[^0-9.]/g, "");
                                totalamount = parseFloat(totalamount) + parseFloat(shippingcharge);
                                $('.grand-amt').html('{!! $currency !!}' + Number(totalamount.toFixed(2)));
                            }

                    }else{
                    }
                },
                error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert("Error");
                    }
            });
            @endif
        }
        else
        {
            @php
            $shipdata = '';
            if(isset($meta_data['shipping_settings'])){
                foreach($meta_data['shipping_settings']['shipping_method'] as $val){
                    $shipdata = $val;
                }
            }
            @endphp
            @if(isset($meta_data['shipping_settings']['shipping_method']) && !empty($shipdata) )
                // if(weight == 0 && pin.length == 6){
                if((weight == 0 && pin.length == 6) || pin.length == 0){
                    if(pin.length != 0){
                        alert('Shipping Service is not available for this product');
                    }
                $('.place-order').attr("disabled", true);
                }
            @endif
        }

            });
            var customer_pin = $('#checkout-pincode').val();
            if(customer_pin){
                var pin = customer_pin;
                // console.log(pin);
        var source_code = "";
        var destination_code = "";
        var public_key_id = "";
        var app_id = "";
        var sell_id = "";
        var private_key = "";
        var data="";
        var pricing="";
        var service="";
        var weight=0;
        var length=0;
        var width =0;
        var height=0;
        var shareableLink = "{{ $this->shareableLink }}";

        cart = JSON.parse(localStorage.getItem('shoppingCart'));
        if(cart.length > 0){
            for (var item in cart) {
                weight = weight + cart[item].weight;
                length = length + cart[item].length;
                width = width + cart[item].width;
                height = height + cart[item].height;
            }
        }
        else{
            weight = cart[item].weight;
            length = cart[item].length;
            width = cart[item].width;
            height = cart[item].height;
        }
        if(pin.length == 6 && weight > 0.5){
            @php
            $shipdata = '';
            if(isset($meta_data['shipping_settings'])){
                foreach($meta_data['shipping_settings']['shipping_method'] as $val){
                    $shipdata = $val;
                }
            }
            @endphp
            @if(isset($meta_data['shipping_settings']['shipping_method']) && !empty($shipdata) )
                @if(isset($meta_data['footer_settings']['pincode']))
                source_code = {{$meta_data['footer_settings']['pincode']}};
                @endif
                destination_code = pin;
                // public_key_id = "{{$meta_data['shipping_settings']['shipping']['public-key-id']}}";
                // app_id = {{$meta_data['shipping_settings']['shipping']['sp-app-id']}};
                // sell_id = {{$meta_data['shipping_settings']['shipping']['sp-sell-id']}};
                // private_key = "{{$meta_data['shipping_settings']['shipping']['sp-private-key']}}";
                data = '&source_code='+ source_code +'&destination_code=' + destination_code + '&weight=' + weight +
                '&height=' + height + '&width=' + width + '&length=' + length + '&shareable_link='+ shareableLink;
                // alert(data);
                // + '&public_key_id=' + public_key_id + '&app_id=' + app_id + '&sell_id=' + sell_id + '&private_key=' + 'private_key';
                $.ajax({
                url: "{{route('shipping.price')}}",
                type: "POST",
                data: data,
                datatype:"json",
                success: function(data) {
                    $('#loading-bg').hide();
                    if(data){
                        data =  JSON.parse(data);

                        let displayShip = [];
                        for(var service in data["service"]){

                            if(data["service"][service]){
                                let name = service.replace(/[^0-9.]/g, "") //remove logic
                                displayShip.push({
                                    name: name,
                                    price: data["pricing"][service]
                                })
                            }
                        }
                            for(var weigh in displayShip){
                                if(weight < displayShip[weigh]["name"] ){
                                    var shippingcharge = displayShip[weigh]["price"];
                                        shyplitecharge = displayShip[weigh]["price"];
                                    var breakout = true;
                                }
                            }
                            if(shippingcharge && displayShip.length > 0)
                            {
                            $('#sphylite_charge').val(shippingcharge);
                            $('#pinservice').text('');
                            $('.place-order').attr("disabled", false);
                            $('.delivery-charge-amt').html('{!! $currency !!}' + Number(shippingcharge.toFixed(2)));
                            var totalamount = $('#totalamount').val();
                            totalamount = parseFloat(totalamount) + parseFloat(shippingcharge);
                            $('.grand-amt').html('{!! $currency !!}' + Number(totalamount.toFixed(2)));
                            }
                            else{
                                // alert('Sorry we are unable to deliver to your pincode:'+pin);
                                $('#pinservice').text('Sorry we are unable to deliver to your pincode:'+pin);
                                $('.place-order').attr("disabled", true);
                                var shippingcharge = 0;
                                $('#sphylite_charge').val(shippingcharge);
                                $('.delivery-charge-amt').html('{!! $currency !!}' + Number(shippingcharge.toFixed(2)));
                                var totalamount = $('#totalamount').val();
                                totalamount = parseFloat(totalamount) + parseFloat(shippingcharge);
                                $('.grand-amt').html('{!! $currency !!}' + Number(totalamount.toFixed(2)));
                            }

                    }else{
                    }
                },
                error: function(jqXHR, textStatus, errorThrown)
                    {
                        alert("Error");
                    }
            });
            @endif
        }
        else
            {
                @php
                $shipdata = '';
                if(isset($meta_data['shipping_settings'])){
                    foreach($meta_data['shipping_settings']['shipping_method'] as $val){
                        $shipdata = $val;
                    }
                }
                @endphp
                @if(isset($meta_data['shipping_settings']['shipping_method']) && !empty($shipdata) )
                    // if(weight == 0 && pin.length == 6){
                    if((weight == 0 && pin.length == 6) || pin.length == 0){
                        if(pin.length != 0){
                        alert('Shipping Service is not available for this product');
                        }
                    $('.place-order').attr("disabled", true);
                    }
                @endif
            }

            }
            // to move to next step on place order and save address click
            $(".place-order").on("click", function() {

                @if(!auth()->guard('customer')->check() && !$guest_user)
                    console.log("if test steps")
                    $('.loginButton').click();
                @else
                    if($(this).attr('id') == "enquiry_btn"){
                        $(".make-payment").empty().html("SUBMIT");
                        $(".checkout-tab-steps").steps("finish", {});
                    }
                    // console.log("test steps")
                    else{
                    $(".checkout-tab-steps").steps("next", {});
                    }
                @endif
            });

            $("#enquiry_btn").on("click", function() {

                $(".checkout-tab-steps").steps("finish", {});
            });
            // check if user has entered valid cvv
            // $(".delivery-address").on("click", function() {
            //     if(document.getElementById("delivery_address").innerHTML.trim() == "NEXT")
            //         $(".checkout-tab-steps").steps("next", {});
            //     else
            //         $(".checkout-tab-steps").steps("finish", {});
            // });

            $(".make-payment").on("click", function() {
                $(".checkout-tab-steps").steps("finish", {});
            });

            $('#checkout_login_link').click(function(){
                $('.loginButton').click();
            });

            $('#checkout_signup_link').click(function(){
                $('.registerButton').click();
            });
        }

        // checkout quantity counter
        var quantityCounter = $(".quantity-counter"),
            CounterMin = 1,
            CounterMax = 10000;
            // alert(quantityCounter.length);
        if (quantityCounter.length > 0) {
            quantityCounter.TouchSpin({
                min: CounterMin,
                max: CounterMax
            }).on('touchspin.on.startdownspin', function() {
                var $this = $(this);
                track_events('product-quantity-decrement-checkout-sku-', $(this).data("id"));
                $('.bootstrap-touchspin-up').removeClass("disabled-max-min");
                var product_min_qty = Number($this.data('min_qty'));
                if ($this.val() < product_min_qty) {
                    if($(this).siblings().find('.bootstrap-touchspin-down').hasClass('disabled-max-min') == false) {
                        place_order_btn_disabled++;
                        disable_place_order_enquiry_buttons();
                        var product_id = Number($this.data('id'));
                        $("#product_quantity_alert_"+ product_id).show();
                        $(this).parent().parent().siblings().find('.maximum_product_quantity').text(product_max_qty);
                    }
                }
                else{
                    obj.setCountForItem(($this.data('sku')), $this.val());
                    $("#product_qty_" + $this.data('sku')).val($this.val());
                    if (Number($this.data('discounted_price')) == 0) {
                        $('.item-price-' + $this.data('sku')).html('{!! $currency !!}' + $this.val() * Number($this.data('price')));
                    } else {
                        $('.item-price-' + $this.data('sku')).html('{!! $currency !!}' + $this.val() * Number($this.data('discounted_price')) + '&nbsp;<span class="text-light"><s>{!! $currency !!}' + $this.val() * Number($this.data('price')) + '</span></s>');
                    }
                }
                if ($this.val() == 1) {
                    $(this).siblings().find('.bootstrap-touchspin-down').addClass("disabled-max-min");
                }
                checkCoupon();
            }).on('touchspin.on.startupspin', function() {
                var $this = $(this);
                track_events('product-quantity-increment-checkout-sku-', $(this).data("id"));
                var product_max_qty = Number($this.data('max_qty'));
                $('.bootstrap-touchspin-down').removeClass("disabled-max-min");
                if ($this.val() >= product_max_qty) {
                    $(this).siblings().find('.bootstrap-touchspin-up').addClass("disabled-max-min");
                }
                if ($this.val() >= product_max_qty && place_order_btn_disabled > 0) {
                    place_order_btn_disabled--;
                }
                if ($this.val() >= product_max_qty) {
                    var product_id = ($this.data('sku'));
                    $("#product_quantity_alert_"+ product_id).hide();
                    if(place_order_btn_disabled == 0) {
                        if(items_amount >= minimum_checkout_value) {
                            $("#minimum_checkout_alert").hide();
                            enable_place_order_enquiry_buttons();
                        }
                    }
                    obj.setCountForItem(($this.data('sku')), $this.val());
                    $("#product_qty_" + $this.data('sku')).val($this.val());
                    update_product_total($this);
                }
                checkCoupon();
            }).on('change', function() {
                var $this = $(this);
                track_events('product-quantity-count-chekout-sku-', $(this).data("id"));
                var product_min_qty = Number($this.data('min_qty'));
                var product_max_qty = Number($this.data('max_qty'));
                if ($this.val() < product_min_qty) {
                    disable_place_order_enquiry_buttons();
                    var product_id = ($this.data('sku'));
                    $("#product_quantity_alert_"+ product_id).show();
                    $(this).parent().parent().siblings().find('.minimum_product_quantity').text(product_min_qty);
                    $("#product_qty_" + $this.data('sku')).val(product_min_qty);
                    $("#quantity_counter_" + $this.data('sku')).val(product_min_qty);
                    obj.setCountForItem(($this.data('sku')), product_min_qty);
                    update_product_total($this);
                } else if ($this.val() > product_max_qty) {
                    disable_place_order_enquiry_buttons();
                    var product_id = $this.data('sku');
                    $("#product_quantity_alerts_"+ product_id).show();
                    $(this).parent().parent().siblings().find('.maximum_product_quantity').text(product_max_qty);
                    $("#product_qty_" + $this.data('sku')).val(product_max_qty);
                    $("#quantity_counter_" + $this.data('sku')).val(product_max_qty);
                    obj.setCountForItem(($this.data('sku')), product_max_qty);
                    obj.setCountForItem(($this.data('sku')), product_min_qty);
                    update_product_total($this);
                } else if ($this.val() > CounterMax) {
                    obj.setCountForItem(($this.data('sku')), CounterMax);
                    update_product_total($this);
                } else {
                    obj.setCountForItem(($this.data('sku')), $this.val());
                    update_product_total($this);
                }
            });
        }

        // remove items from wishlist page
        $(".remove-wishlist , .move-cart").on("click", function() {
            var remove_product_id = $(this).data('sku');
            var remove_product_qty = $("#quantity_counter_"+remove_product_id).val();
            var remove_product_min_qty = $("#quantity_counter_"+remove_product_id).data('min_qty');
            var remaining_quantity = remove_product_min_qty - remove_product_qty;
            place_order_btn_disabled-=remaining_quantity;
            if(place_order_btn_disabled == 0) {
                if(items_amount >= minimum_checkout_value) {
                    $("#minimum_checkout_alert").hide();
                    enable_place_order_enquiry_buttons();
                }
            }
            $(this).closest(".checkout_item").remove();
            obj.removeItemFromCart(remove_product_id);
            obj.totalCart();
            checkCoupon();
            track_events('remove-product-button-clicked-sku-'+ $(this).data("id"));
            track_events("remove_product", $(this).data('product_id'), slink);
        });
    });

    // Set count from item
    obj.setCountForItem = function(sku, qty) {
        for (var i in cart) {
            // alert(sku);
            if (cart[i].sku === sku) {
                cart[i].qty = qty;
                break;
            }
        }
        saveCart();
        obj.totalCart();
    };

    // Remove item from cart
    obj.removeItemFromCart = function(sku) {
        for (var item in cart) {
            if (cart[item].sku === sku) {
                cart.splice(item, 1);
            }
        }
        saveCart();
    }

    // Remove all items from cart
    obj.removeItemFromCartAll = function(name) {
        for (var item in cart) {
            if (cart[item].name === name) {
                cart.splice(item, 1);
                break;
            }
        }
        saveCart();
    }

    // Count cart
    obj.totalCount = function() {
        var totalCount = 0;
        for (var item in cart) {
            totalCount += cart[item].qty;
        }
        return totalCount;
    }

    // Total cart
    obj.totalCart = function() {
        var totalCart = 0;
        var discountCart = 0;

        for (var item in cart) {
            if (cart[item].discounted_price != 0) {
                discountCart += cart[item].price * cart[item].qty - cart[item].discounted_price * cart[item].qty;
            }
            totalCart += cart[item].price * cart[item].qty;

        }
        items_amount = totalCart - discountCart;
        if(items_amount < minimum_checkout_value) {
            disable_place_order_enquiry_buttons();
            $("#minimum_checkout_alert").show();
        }
        else{
            if(place_order_btn_disabled == 0){
                $("#minimum_checkout_alert").hide();
                enable_place_order_enquiry_buttons();
            }
        }
        $('.total-amt, .mrp-amt, .items-amt').html('{!! $currency !!}' + Number(items_amount.toFixed(2)));
        // $('.discount-amt').html('- {!! $currency !!}' + Number(discountCart.toFixed(2)));
        discount = discountCart;

        if (cart.length > 0) {
            $(".cart-item-count").html(cart.length);
            $(".price-items").html("Price of " + cart.length + " items");
        } else {
            $(".cart-item-count").html("");
            $(".ecommerce-application").empty();
            $(".ecommerce-application").html("<h2 class='text-light text-center'><i class='pt-1 ficon feather icon-shopping-cart fa-3x'></i><br>No item in cart<br><a class='btn btn-success m-2' onclick='track_events(`go-to-store-button-clicked`,null)' href='{{$homeLink}}'>Go to store</a></h2>");
        }
        return Number(totalCart.toFixed(2));
    }

    // Clear cart
    obj.clearCart = function() {
        cart = [];
        saveCart();
    }

    // Save cart
    function saveCart() {
        const now = new Date();
        var time = now.getTime() + 1000 * 60 * 60 * 2;
        localStorage.setItem('shoppingCart', JSON.stringify(cart));
        localStorage.setItem('expiretime', JSON.stringify(time));
    }

    //loads the cart from local storage when the page is loaded
    function loadCart() {
        cart = JSON.parse(localStorage.getItem('shoppingCart'));
        var cartItemCount = count = cart.length;
        $(".checkout-items").empty();
        if (cart.length > 0) {
            $(".cart-item-count").html(count);
            for (var item in cart) {
                if(cart[item].max_qty == 0){
                    cart[item].max_qty = 10000;
                }
                if (cart[item].discounted_price == 0) {
                    // alert("hi");
                    console.log(cart[item].max_qty);
                    var html_1='<div class="row bg-white shadow-lg rounded mb-1 checkout_item" style="min-height:200px"><div class="col-lg-3 col-md-3 col-sm-12 h-100 d-flex justify-content-center rounded p-1"><div class="item-img text-center"><a href="#"><img src="' + cart[item].img + '" style="min-height:180px;" class="mw-100 "></a></div></div><div class="col-lg-9 col-md-9 col-sm-12 h-100"><div class="row h-50 w-100 m-0"><div class="col-lg-6 col-md-6 col-sm-6 h-100 p-1"><div class="item-name"><a href="javascript:void(0);" data-id="' + cart[item].id + '" data-catalog_id="' + cart[item].catalogid + '" onclick="catalogDetailsPage(this)" >' + cart[item].name + '</a><span></span><div class="item-company"><div class="badge badge-pill badge-glow badge-primary mr-1 d-none">' + cart[item].catagory + ' </div></div></div><div class="item-quantity"><div><p class="quantity-title">Quantity</p></div><div class="input-group quantity-counter-wrapper"><input type="text" class="quantity-counter" id="quantity_counter_' + cart[item].sku + '" data-id="' + cart[item].id + '" data-min_qty="' + cart[item].min_qty + '" data-max_qty="' + cart[item].max_qty +  '" data-price="' + cart[item].price + '" data-discounted_price="' + cart[item].discounted_price + '"   data-sku="'+cart[item].sku+'"  value="' + cart[item].qty + '"></div></div><div id="product_quantity_alert_' + cart[item].sku + '" class="alert alert-danger mt-1" role="alert" style="display:none;"><i class="feather icon-info mr-1 align-middle"></i><span>Please select a minimum of <span class="minimum_product_quantity"></span> to proceed with the order.</span></div><div id="product_quantity_alerts_' + cart[item].sku + '" class="alert alert-danger mt-1" role="alert" style="display:none;"><i class="feather icon-info mr-1 align-middle"></i><span>Please select a maximum of <span class="maximum_product_quantity"></span> to proceed with the order.</span></div></div><div class="col-lg-6 col-md-6 col-sm-6 h-100 p-1"><div class="item-wrapper text-right"><div class="item-rating"><div class="badge badge-primary badge-md hidden">4<i class="feather icon-star ml-25"></i></div></div><div class="item-cost"><h6 class="item-price item-price-' + cart[item].sku + '">{!! $currency !!}' + cart[item].qty * cart[item].price + ' </h6><p class="shipping hidden"><i class="feather icon-shopping-cart"></i>Free Shipping </p></div></div></div></div><div class="row h-50 w-100 m-0"><div class="col-lg-9 col-md-9 col-sm-12 h-100 p-1"><div class="input-group"><textarea placeholder="Enter your comments / questions here" id="product_notes_' + cart[item].id + '" class="form-control" aria-label="With textarea product" maxlength="1000"></textarea></div></div><div class="col-lg-3 col-md-3 col-sm-12 h-100"><button type="button" class="btn btn-danger wishlist remove-wishlist w-100" data-id="' + cart[item].id + '" data-min_qty="' + cart[item].min_qty + '" data-max_qty="' + cart[item].max_qty + '" data-sku="'+cart[item].sku+'"><i class="feather icon-trash"></i> Remove</button><div class="cart remove-wishlist hidden"><i class="fa fa-heart-o mr-25"></i>Wishlist </div></div></div></div></div>';
                    // var html_1='<div class="row bg-white shadow-lg rounded mb-1 checkout_item" style="min-height:200px"><div class="col-lg-3 col-md-3 col-sm-12 h-100 d-flex justify-content-center rounded p-1"><div class="item-img text-center"><a href="#"><img src="' + cart[item].img + '" style="min-height:180px;" class="mw-100 "></a></div></div><div class="col-lg-9 col-md-9 col-sm-12 h-100"><div class="row h-50 w-100 m-0"><div class="col-lg-6 col-md-6 col-sm-6 h-100 p-1"><div class="item-name"><a href="javascript:void(0);" data-id="' + cart[item].id + '" data-catalog_id="' + cart[item].catalogid + '" onclick="catalogDetailsPage(this)" >' + cart[item].name + '</a><span></span><div class="item-company"><div class="badge badge-pill badge-glow badge-primary mr-1 d-none">' + cart[item].catagory + ' </div></div></div><div class="item-quantity"><div><p class="quantity-title">Quantity</p></div><div class="input-group quantity-counter-wrapper"><input type="text" class="quantity-counter" id="quantity_counter_' + cart[item].id + '" data-id="' + cart[item].id + '" data-min_qty="' + cart[item].min_qty + '"  data-max_qty="' + cart[item].max_qty +  '" data-price="' + cart[item].price + '" data-discounted_price="' + cart[item].discounted_price + '"   data-sku="'+cart[item].sku+'" value="' + cart[item].qty +'"></div></div><div id="product_quantity_alert_' + cart[item].id + '" class="alert alert-danger mt-1" role="alert" style="display:none;"><i class="feather icon-info mr-1 align-middle"></i><span>Please select a minimum of <span class="minimum_product_quantity"></span> to proceed with the order.</span></div><div id="product_quantity_alerts_' + cart[item].id + '" class="alert alert-danger mt-1" role="alert" style="display:none;"><i class="feather icon-info mr-1 align-middle"></i><span>Please select a maximum of <span class="maximum_product_quantity"></span> to proceed with the order.</span></div></div><div class="col-lg-6 col-md-6 col-sm-6 h-100 p-1"><div class="item-wrapper text-right"><div class="item-rating"><div class="badge badge-primary badge-md hidden">4<i class="feather icon-star ml-25"></i></div></div><div class="item-cost"><h6 class="item-price item-price-' + cart[item].sku + '">{!! $currency !!}' + cart[item].qty * cart[item].price + ' </h6><p class="shipping hidden"><i class="feather icon-shopping-cart"></i>Free Shipping </p></div></div></div></div><div class="row h-50 w-100 m-0"><div class="col-lg-9 col-md-9 col-sm-12 h-100 p-1"><div class="input-group"><textarea placeholder="Enter your comments / questions here" id="product_notes_' + cart[item].id + '" class="form-control" aria-label="With textarea product" maxlength="1000"></textarea></div></div><div class="col-lg-3 col-md-3 col-sm-12 h-100"><button type="button" class="btn btn-danger wishlist remove-wishlist w-100" data-id="' + cart[item].sku + '" data-min_qty="' + cart[item].min_qty + '" data-max_qty="' + cart[item].max_qty + '"><i class="feather icon-trash"></i> Remove</button><div class="cart remove-wishlist hidden"><i class="fa fa-heart-o mr-25"></i>Wishlist </div></div></div></div></div>';
                } else {
                    // alert("byee");
                  var html_1='<div class="row bg-white shadow-lg rounded mb-1 checkout_item" style="min-height:200px"><div class="col-lg-3 col-md-3 col-sm-12 h-100 d-flex justify-content-center rounded p-1"><div class="item-img text-center"><a href="#"><img src="' + cart[item].img + '" style="min-height:180px;" class="mw-100 "></a></div></div><div class="col-lg-9 col-md-9 col-sm-12 h-100"><div class="row h-50 w-100 m-0"><div class="col-lg-6 col-md-6 col-sm-6 h-100 p-1"><div class="item-name"><a href="javascript:void(0);" data-id="' + cart[item].id + '" data-catalog_id="' + cart[item].catalogid + '" onclick="catalogDetailsPage(this)" >' + cart[item].name + '</a><span></span><div class="item-company"><div class="badge badge-pill badge-glow badge-primary mr-1 d-none">' + cart[item].catagory + ' </div></div></div><div class="item-quantity"><div><p class="quantity-title">Quantity</p></div><div class="input-group quantity-counter-wrapper"><input type="text" class="quantity-counter"id="quantity_counter_' + cart[item].sku + '" data-id="' + cart[item].id + '" data-min_qty="' + cart[item].min_qty + '" data-max_qty="' + cart[item].max_qty +  '"  data-price="' + cart[item].price + '" data-sku="'+cart[item].sku+'" data-discounted_price="' + cart[item].discounted_price + '" data-sku="'+cart[item].sku+'"  value="' + cart[item].qty + '"></div></div><div id="product_quantity_alert_' + cart[item].sku + '" class="alert alert-danger mt-1" role="alert" style="display:none;"><i class="feather icon-info mr-1 align-middle"></i><span>Please select a minimum of <span class="minimum_product_quantity"></span> to proceed with the order.</span></div><div id="product_quantity_alerts_' + cart[item].sku + '" class="alert alert-danger mt-1" role="alert" style="display:none;"><i class="feather icon-info mr-1 align-middle"></i><span>Please select a maximum of <span class="maximum_product_quantity"></span> to proceed with the order.</span></div></div><div class="col-lg-6 col-md-6 col-sm-6 h-100 p-1"><div class="item-wrapper text-right"><div class="item-rating"><div class="badge badge-primary badge-md hidden">4<i class="feather icon-star ml-25"></i></div></div><div class="item-cost"><h6 class="item-price item-price-' + cart[item].sku + '">{!! $currency !!}' + cart[item].qty * cart[item].discounted_price + ' &nbsp;<span class="text-light"><s>{!! $currency !!}' + cart[item].qty * cart[item].price + '</s></span></h6><p class="shipping hidden"><i class="feather icon-shopping-cart"></i>Free Shipping </p></div></div></div></div><div class="row h-50 w-100 m-0"><div class="col-lg-9 col-md-9 col-sm-12 h-100 p-1"><div class="input-group"><textarea placeholder="Enter your comments / questions here" id="product_notes_' + cart[item].id + '" class="form-control" aria-label="With textarea product" maxlength="1000"></textarea></div></div><div class="col-lg-3 col-md-3 col-sm-12 h-100 "><button type="button" class="btn btn-danger wishlist remove-wishlist w-100" data-id="' + cart[item].id + '" data-min_qty="' + cart[item].min_qty + '" data-sku="'+cart[item].sku+'"><i class="feather icon-trash"></i> Remove</button><div class="cart remove-wishlist hidden"><i class="fa fa-heart-o mr-25"></i>Wishlist </div></div></div></div></div>';
                    // var html_1='<div class="row bg-white shadow-lg rounded mb-1 checkout_item" style="min-height:200px"><div class="col-lg-3 col-md-3 col-sm-12 h-100 d-flex justify-content-center rounded p-1"><div class="item-img text-center"><a href="#"><img src="' + cart[item].img + '" style="min-height:180px;" class="mw-100 "></a></div></div><div class="col-lg-9 col-md-9 col-sm-12 h-100"><div class="row h-50 w-100 m-0"><div class="col-lg-6 col-md-6 col-sm-6 h-100 p-1"><div class="item-name"><a href="javascript:void(0);" data-id="' + cart[item].id + '" data-catalog_id="' + cart[item].catalogid + '" onclick="catalogDetailsPage(this)" >' + cart[item].name + '</a><span></span><div class="item-company"><div class="badge badge-pill badge-glow badge-primary mr-1 d-none">' + cart[item].catagory + ' </div></div></div><div class="item-quantity"><div><p class="quantity-title">Quantity</p></div><div class="input-group quantity-counter-wrapper"><input type="text" class="quantity-counter" id="quantity_counter_' + cart[item].id + '" data-id="' + cart[item].id + '" data-min_qty="' + cart[item].min_qty + '"  data-price="' + cart[item].price + '" data-discounted_price="' + cart[item].discounted_price + '" data-sku="'+cart[item].sku+'"  value="' + cart[item].qty + '"></div></div><div id="product_quantity_alert_' + cart[item].id + '" class="alert alert-danger mt-1" role="alert" style="display:none;"><i class="feather icon-info mr-1 align-middle"></i><span>Please select a minimum of <span class="minimum_product_quantity"></span> to proceed with the order.</span></div></div><div class="col-lg-6 col-md-6 col-sm-6 h-100 p-1"><div class="item-wrapper text-right"><div class="item-rating"><div class="badge badge-primary badge-md hidden">4<i class="feather icon-star ml-25"></i></div></div><div class="item-cost"><h6 class="item-price item-price-' + cart[item].sku+ '">{!! $currency !!}' + cart[item].qty * cart[item].discounted_price + ' &nbsp;<span class="text-light"><s>{!! $currency !!}' + cart[item].qty * cart[item].price + '</s></span></h6><p class="shipping hidden"><i class="feather icon-shopping-cart"></i>Free Shipping </p></div></div></div></div><div class="row h-50 w-100 m-0"><div class="col-lg-9 col-md-9 col-sm-12 h-100 p-1"><div class="input-group"><textarea placeholder="Enter your comments / questions here" id="product_notes_' + cart[item].id + '" class="form-control" aria-label="With textarea product" maxlength="1000"></textarea></div></div><div class="col-lg-3 col-md-3 col-sm-12 h-100 "><button type="button" class="btn btn-danger wishlist remove-wishlist w-100" data-id="' + cart[item].sku + '" data-min_qty="' + cart[item].min_qty + '"><i class="feather icon-trash"></i> Remove</button><div class="cart remove-wishlist hidden"><i class="fa fa-heart-o mr-25"></i>Wishlist </div></div></div></div></div>';
                }
                var html = html_1;
                $(".checkout-items").append(html);
                obj.totalCart();
            }

            grandTotal();
        } else {
            $(".cart-item-count").html("");
            $(".ecommerce-application").empty();
            $(".ecommerce-application").html("<h2 class='text-light text-center'><i class='pt-1 ficon feather icon-shopping-cart fa-3x'></i><br>No item in cart<br><a class='btn btn-success m-2' onclick='track_events(`go-to-store-button-clicked`,null)' href='{{$homeLink}}'>Go to store</a></h2>");
        }
    }

    if (localStorage.getItem("shoppingCart") != null) {
        const now = new Date();
        var now_time = now.getTime();
        var expire_time = localStorage.getItem("expiretime");
        if (now_time > expire_time) {
            obj.removeItemFromCartAll();
            obj.totalCart();
        } else {
            loadCart();
        }
    } else {
        obj.totalCart();
    }

    function disable_place_order_enquiry_buttons() {
        $("#delivery_address").attr("disabled", true);
        $("#enquiry_btn").attr("disabled", true);
    }

    function enable_place_order_enquiry_buttons() {
        $("#delivery_address").attr("disabled", false);
        $("#enquiry_btn").attr("disabled", false);
    }

    function update_product_total($this){
        if (Number($this.data('discounted_price')) == 0) {
            $('.item-price-' + $this.data('sku')).html('{!! $currency !!}' + $this.val() * Number($this.data('price')));
        } else {
            $('.item-price-' + $this.data('sku')).html('{!! $currency !!}' + $this.val() * Number($this.data('discounted_price')) + '&nbsp;<span class="text-light"><s>{!! $currency !!}' + $this.val() * Number($this.data('price')) + '</span></s>');
        }
        grandTotal();
    }

    function formSubmit(data, $this) {
        data = data + '&payment_method=' + payment_method;
        if(document.getElementById("make_payment").innerHTML.trim() == "Make Payment" || document.getElementById("make_payment").innerHTML.trim() == "Place Order"){
            if (payment_method != 'Paypal') {
                $('.make-payment').attr('disabled', 'disabled');
                var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> loading...';
                $('.make-payment').html(loadingText);
            }

            if (payment_method == 'UPI') {
                data = data + '&q=store';
            }

            // if (payment_method != 'Cash on delivery') {
            $.ajax({
                url: "{{route('orders.payment')}}",
                type: "POST",
                data: data,
                success:function(response) {
                    // alert(data);
                    obj.clearCart();
                    payment(response, data);
                }
            });
        }
        else{
            $('.make-payment').attr('disabled', 'disabled');
            var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> loading...';
            $('.make-payment').html(loadingText);
            $('#enquiry_btn').html(loadingText);
            $('.paymentMethod').css("pointer-events", "none");
            $('.paymentMethod').css("opacity", 0.5);
            $.ajax({
                url: "{{route('enquiry.store')}}",
                type: "POST",
                data: data,
                success: function(response) {
                    obj.clearCart();
                    url = window.location.origin + '/ec/' + response.key + '/' + response.enquiry_id;
                    window.location = url;
                }
            });
        }
    }

    function setCookie(name, value) {
        document.cookie = name + "=" + escape(value) + "; path=/; expires=" + expiry.toGMTString();
    }

    function getCookie(name) {
        var re = new RegExp(name + "=([^;]+)");
        var value = re.exec(document.cookie);
        return (value != null) ? unescape(value[1]) : null;
    }

    function catalogDetailsPage(that) {
        // alert("hi");
        var product_id = that.getAttribute("data-id");
        var catalog_id = that.getAttribute("data-catalog_id");
        var url = '{{ route('campaigns.publishedCatalogDetails',[":product_id",":catalog_id"]) }}';
        url = url.replace(':product_id', product_id);
        url = url.replace(':catalog_id', catalog_id);
        window.location.href = url;
    }

    window.onload = function() {
        url = window.location.pathname;
        slink = url.slice(url.lastIndexOf('/') + 1);
        track_events('checkout_page', null, slink);
    };
    
    function payment(response, data) {
        data = data + "&store_order_id=" + response.store_order_id;
        if (payment_method == "Razorpay") {
            var order_id = response['order_id'];
            var options = {
                "key": response['razorpayId'], // Enter the Key ID generated from the Dashboard
                "amount": response['amount'], // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
                "currency": response['currency'],
                "name": response['name'],
                "description": response['description'],
                "order_id": response['orderId'], //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
                "handler": function(response) {
                    data = data + "&rzp_paymentid=" + response.razorpay_payment_id + "&rzp_orderid=" + response.razorpay_order_id + "&rzp_signature=" + response.razorpay_signature + "&status=success";
                    page_redirection(data);
                },
                "prefill": {
                    "name": response['name'],
                    "email": response['email'],
                    "contact": response['contactNumber']
                },
                "notes": {
                    "address": response['address']
                },
                "theme": {
                    "color": "#528FF0"
                }
            };

            var rzp1 = new Razorpay(options);
            rzp1.open();

            rzp1.on('payment.failed', function(response) {
                data = data + "&status=failure" + "&rzp_paymentid=" + response.metadata.payment_id + "&rzp_orderid=" + response.metadata.order_id + "&response=" + response;
                page_redirection(data);
            });
        }
        else if (payment_method == "Paypal") {
            loaded = false;
            var s = document.createElement('script');
            s.type = "text/javascript";
            s.setAttribute('src', "https://www.paypal.com/sdk/js?client-id=" + response.client_id + "&components=buttons&currency=" + response.currency);
            s.addEventListener("load", function() {
                paypal.Buttons({
                    enableStandardCardFields: true,
                    style: {
                        layout: 'horizontal',
                        color: 'blue',
                        shape: 'rect',
                        label: 'paypal',
                        size: 'responsive',
                    },
                    createOrder: function(data, actions) {
                        return actions.order.create({
                            payer: {
                                name: {
                                    given_name: response.name,
                                },
                                email_address: response.email,
                                phone: {
                                    phone_type: "MOBILE",
                                    phone_number: {
                                        national_number: response.mobileno
                                    }
                                }
                            },
                            purchase_units: [{
                                amount: {
                                    value: response.amount,
                                },
                            }]
                        });
                    },
                    onApprove: function(data, actions) {
                        // This function captures the funds from the transaction.
                        return actions.order.capture().then(function(details) {
                            window.alert('approved');
                            // This function shows a transaction success message to your buyer.
                            // alert('Transaction completed by ' + details.payer.name.given_name);
                        });
                    },
                    onCancel: function(data) {
                        window.alert('cancel');
                        // Show a cancel page, or return to cart
                    },
                    onError: function(err) {
                        window.alert('error');
                        // For example, redirect to a specific error page
                        // window.location.href = "/your-error-page-here";
                    },
                }).render('#paypal-button-container');
            });
            document.body.appendChild(s);
        }
        else if (payment_method == "UPI") {
            url = window.location.origin + '/pay/' + response.key;
            window.location = url;
        }
        else if (payment_method == "Cash on delivery") {
            page_redirection(data);
        }
    }

    function page_redirection(data) {
        $.ajax({
            url: "{{route('transaction.store')}}",
            type: "POST",
            data: data,
            success: function(response) {
                order_id = response.order_id;
                url = window.location.origin + '/pc/' + response.key + '/' + response.order_id;
                window.location = url;
            }
        });
    }

    function applyCouponCode (){
        var loadingText = '<i class="fa fa-circle-o-notch fa-spin"></i> loading...';

        if($('#coupon_code').val()=='')
            return $('#coupon_error_message').html('Please Enter the coupon code');

        $('#apply_coupon').html(loadingText);

        if(coupon_code == $('#coupon_code').val() && !coupon_applied )
        {
            coupon_applied=true;
            coupon_code=$('#coupon_code').val();
            checkCoupon();
            $('#apply_coupon').html('Apply Coupon');
            CouponCodeBadge(coupon_code)
            $('#coupon_error_message').html('');
            $('#coupon_success_message').html('Coupon code applied - '+coupon_desc);
            return;
        }

        $.ajax({
            url:"{{ route('coupon.apply') }}",
            type:"POST",
            data:{
                'X-CSRF-TOKEN': "{{ csrf_token() }}",
                code:$('#coupon_code').val(),
                amount : obj.totalCart() - discount,
                shareable_link:"{{$campaign->public_link}}" },
            success:function(res){
                if(res.status=='error'){
                    $('#apply_coupon').html('Apply Coupon');
                    $('#coupon_success_message').html('');
                    $('#coupon_error_message').html(res.msg);
                    if(res.login==true)
                        $('.loginButton').click();
                }

                if(res.status=='min_cart_value'){
                    $('#apply_coupon').html('Apply Coupon');
                    $('#coupon_success_message').html('');
                    $('#coupon_error_message').html("You have to purchase more than {!! $currency !!}"+res.min_cart_value+ " to use this coupon" );
                }

                if(res.status=='success'){
                    coupon_applied=true;
                    min_cart_value= res.min_cart_value;
                    coupon_amount=res.discount_amount;
                    coupon_unit=res.unit;
                    coupon_id=res.id;
                    coupon_desc=res.desc;
                    coupon_code=$('#coupon_code').val();
                    checkCoupon();
                    $('#apply_coupon').html('Apply Coupon');
                    CouponCodeBadge(coupon_code);
                    $('#coupon_error_message').html('');
                    $('#coupon_success_message').html(res.msg+coupon_desc);
                }
            }
        })
    }

    function checkCoupon(){
        var amount = obj.totalCart() - discount;
        var discount_amount = 0;

        if(coupon_applied)
        {
            if(min_cart_value > (amount))
            {
                CouponCodeInput();
                coupon_applied=false;
                discount_amount=0;
                discount=discount+discount_amount;
                $('.coupon-discount-amt').html('- {!! $currency !!}' + Number(discount.toFixed(2)));
                $('#coupon_code').val('');
                $('#coupon_success_message').html('');
                $('#coupon_error_message').html('');
                $('#coupon_error_message').html("You have to purchase more than {!! $currency !!}"+min_cart_value+ " to use this coupon");
            }else
            {
                if(coupon_unit=='percentage'){
                    discount_amount= (amount/100) * coupon_amount
                    $('.coupon-discount-amt').html('- ('+coupon_amount+'%) {!! $currency !!}' + Number(discount_amount.toFixed(2)));
                }

                if(coupon_unit=='amount'){
                    discount_amount=coupon_amount;
                    $('.coupon-discount-amt').html('- {!! $currency !!}' + Number(coupon_amount.toFixed(2)));
                }

                $('#coupon_id').val(coupon_id);
                $('#coupon_code').val(coupon_code);
                $("#coupon_discount_amount").val(discount_amount);
                $("#coupon_code_applied").val('true');
            }
        }else
        {
            $('#coupon_code').val('');
            $("#coupon_code_applied").val('false');
            $("#coupon_discount_amount").val('');
            $('#coupon_id').val('');
        }

        coupon_discount = discount_amount;
        grandTotal();
    }

    function CouponCodeBadge(coupon_code){
        $('#coupon_code').hide();
        $('#apply_coupon').hide();
        $('#coupon_badge').html(coupon_code);
        $('#coupon_alert').prop('hidden',false);
    }

    function CouponCodeInput(){
        coupon_applied = false;
        coupon_discount = 0;
        $('.coupon-discount-amt').html('- {!! $currency !!}' + Number(coupon_discount.toFixed(2)));
        grandTotal();
        $('#coupon_success_message').html('');
        $('#coupon_badge').html('');
        $('#coupon_alert').prop('hidden',true);
        $('#coupon_code').show();
        $('#apply_coupon').show();
    }



    // $('#checkout-pincode').on('change',function() {
    //         var discount_val = $('#checkout-pincode').val();
    //         console.log(discount_val);
    // });

    // $('#checkout-pincode').on('input', function() {
    // });
        // $('#checkout-pincode').on('input', function() {
        // alert();
        // });
    function grandTotal()
    {
        var cart_total = obj.totalCart() - discount;
        var cart = JSON.parse(localStorage.getItem('shoppingCart'));
        var product_count = cart.length;
        var delivery_charges_total = 0;
        var tax_percentage = 0;
        var tax_total = 0;
        var extra_charges_total = 0;
        var total_before_tax = 0;
        var total_after_tax = 0;
        var extra_charge_names = [];
        var extra_charges_values = [];
        // console.log(shippingcharge);
        @php
            $shipdata = '';
            if(isset($meta_data['shipping_settings'])){
                foreach($meta_data['shipping_settings']['shipping_method'] as $val){
                    $shipdata = $val;
                }
            }
        @endphp
        @if(isset($meta_data['checkout_settings']['delivery_charge']) && $meta_data['checkout_settings']['delivery_charge'] != "no_charge" && empty($shipdata))
            var delivery_charge = 0;
            var free_delivery_above = {{$meta_data['checkout_settings']['delivery_charge_settings']['free_delivery_above']}};
            if(cart_total < free_delivery_above){
                @if($meta_data['checkout_settings']['delivery_charge'] == "charge_per_order")
                    delivery_charge = {{ $meta_data['checkout_settings']['delivery_charge_settings']['delivery_charge_per_order'] }};
                    delivery_charges_total = delivery_charge;
                @elseif($meta_data['checkout_settings']['delivery_charge'] == "charge_per_product")
                    delivery_charge = {{ $meta_data['checkout_settings']['delivery_charge_settings']['delivery_charge_per_product'] }};
                    delivery_charges_total = delivery_charge * product_count;
                @endif
            }
            $('.delivery-charge-amt').html('{!! $currency !!}' + Number(delivery_charges_total.toFixed(2)));
        @endif

        $("#shipping_charges").val(delivery_charges_total);

        @if(isset($meta_data['checkout_settings']['extra_charges']) && count($meta_data['checkout_settings']['extra_charges']) > 0)
            var extra_charge_amount = 0;
            @foreach($meta_data['checkout_settings']['extra_charges'] as $key => $value)
                var extra_charge_id = {{ $value['extra_charge_id'] }};
                var extra_charge_name = "{{ $value['extra_charge_name']}}";
                extra_charge_names.push(extra_charge_name);
                @if($value['extra_charge_type']=='flat_charge')
                    extra_charge_amount = {{ $value['extra_charge_amount'] }};
                    $('#extra_charge_amt_' + extra_charge_id).html('{!! $currency !!}' + Number(extra_charge_amount.toFixed(2)));
                    extra_charges_total += extra_charge_amount;
                @elseif($value['extra_charge_type']=='percentage')
                    extra_charge_amount = ({{ $value['extra_charge_percentage'] }}/100) * cart_total;
                    $('#extra_charge_amt_' + extra_charge_id).html('{!! $currency !!}' + Number(extra_charge_amount.toFixed(2)));
                    extra_charges_total += extra_charge_amount;
                @endif
                extra_charges_values.push(extra_charge_amount);
            @endforeach
        @endif

        $('form[name=checkout_form]').append("<input type='hidden' name='extra_charges_names' value='" + extra_charge_names + "'>");
        $('form[name=checkout_form]').append("<input type='hidden' name='extra_charges_values' value='" + extra_charges_values + "'>");

        total_before_tax = cart_total - coupon_discount + delivery_charges_total + extra_charges_total;

        @if(isset($meta_data['checkout_settings']['tax_settings']) && $meta_data['checkout_settings']['tax_settings'] == '1')
            tax_percentage = {{$meta_data['checkout_settings']['tax_setting_details']['gst_percentage']}};
            tax_total = (tax_percentage/100) * total_before_tax;
            $('.tax-amt').html('{!! $currency !!}' + Number(tax_total.toFixed(2)));
            $('.before-tax-amt').html('{!! $currency !!}' + Number(total_before_tax.toFixed(2)));
        @endif

        $("#tax_percentage").val(tax_percentage);
        $("#tax_amount").val(tax_total);

        total_after_tax = total_before_tax + tax_total;

            @if( isset($meta_data['shipping_settings']))
                total_after_tax = total_after_tax + shyplitecharge;
            @endif

        $('#totalamount').val(total_after_tax);
        $('.grand-amt').html('{!! $currency !!}' + Number(total_after_tax.toFixed(2)));
    }

    function isNumberKey(event) {
        // Allow only backspace and delete
        if (event.keyCode == 8 ) {
            // let it happen, don't do anything
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.keyCode < 48 || event.keyCode > 57 ) {
                event.preventDefault();
            }
        }
    }

</script>
@endsection
