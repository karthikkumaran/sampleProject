<!-- <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#exampleModalScrollable">
                                            Launch demo modal
                                        </button> -->
@section('styles')
@parent
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/plugins/file-uploaders/dropzone.css')}}">
<style>
    .dropzone .dz-message {
        height: auto !important;
    }
</style>
@endsection
@include('modals.edit_product')
@include('modals.edit_productvariant')
<!-- Modal -->
<div class="modal fade ecommerce-application" id="productSelectModal" tabindex="-1" role="dialog" aria-labelledby="productSelectModalTitle" aria-hidden="true">
    <form method="POST" action="#" id="productAddForm" enctype="multipart/form-data" style="height: 90%;">
        @csrf
        <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
            <input type="hidden" name="campaign_id" value="{{$campaign->id}}" />
            <!-- <input type="hidden" name="videoid" value="{{$campaign->id}}" /> -->
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="productSelectModalTitle">Select Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <!-- Ecommerce Content Section Starts -->
                    <section id="ecommerce-header">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="ecommerce-header-items">
                                    <div class="result-toggler">
                                        <button class="navbar-toggler shop-sidebar-toggler" type="button" data-toggle="collapse">
                                            <span class="navbar-toggler-icon d-block d-lg-none"><i class="feather icon-menu"></i></span>
                                        </button>
                                        <div class="search-results">
                                            <span id="productSelectModalTotal"></span> results found
                                        </div>
                                    </div>
                                    <div class="view-options">
                                        <!-- <select class="price-options form-control" id="productSelectModalCatagory">
                                        <option value="">Catagory</option>
                                        @foreach($categories as $id => $category)
                                            <option value="{{ $id }}">{{ $category }}</option>
                                        @endforeach
                                        </select> -->
                                        <div class="view-btn-option">
                                            <!-- <button type="button" class="btn btn-white view-btn grid-view-btn active">
                                                <i class="feather icon-grid"></i>
                                            </button> -->
                                            <!-- <button type="button" class="btn btn-white list-view-btn view-btn">
                                                <i class="feather icon-list"></i>
                                            </button> -->
                                            <button type="button" class="btn btn-primary pull-right" onclick="addnewproduct()">
                                                <i class="feather icon-cube"></i> Add New Product</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- Ecommerce Content Section Starts -->
                    <!-- background Overlay when sidebar is shown  starts-->
                    <!-- <div class="shop-content-overlay"></div> -->
                    <!-- background Overlay when sidebar is shown  ends-->

                    <!-- Ecommerce Search Bar Starts -->
                    <section id="ecommerce-searchbar">
                        <div class="row mt-1">
                            <div class="col-sm-11">
                                <fieldset class="form-group position-relative">
                                    <input type="text" class="form-control search-product" id="ModalProductSearch" placeholder="Search here">
                                    <div class="form-control-position">
                                        <i class="feather icon-search"></i>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-sm-1" id="show_filter_btn" onclick="show_filter()" style="cursor:pointer;">
                                <button class="navbar-toggler shop-sidebar-toggler w-100 h-75 d-flex justify-content-center" type="button" data-toggle="collapse" onclick="show_filter()">
                                    <span class="navbar-toggler-icon d-block d-lg-block d-md-block d-sm-block"><i class="feather icon-filter" style="font-size:21px;"></i></span>
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <button class="btn btn-warning ml-1 mr-1" type="button" id="select_all_my_products" onclick="selectAllMyProductsToCatalog()">
                                <span>Select all</span>
                            </button>
                            <button class="btn btn-danger ml-1 mr-1" type="button" id="deselect_all_my_products" onclick="deselectAllMyProductsToCatalog()" style="display: none;">
                                <span>Deselect all</span>
                            </button>
                        </div>
                    </section>
                    <!-- Ecommerce Search Bar Ends -->
                    <section id="ecommerce-products" class="grid-view d-block mb-4">
                        <div class="row" id="product_gird">

                        </div>
                        <!-- <a href="#" onclick="getData(page++)">more</a> -->
                    </section>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancel</button>
                    <button type="submit" class="btn btn-primary"id="add_btn" disabled="true">Add</button>
                </div>
            </div>

        </div>
    </form>
</div>

<!-- Add New Product Modal -->
@php 
$agent=new \Jenssegers\Agent\Agent();
@endphp
@if($agent->isMobile())
<div class="modal modal-fullscreen p-0 fade ecommerce-application" id="productAddNewModal" tabindex="-1" role="dialog" aria-labelledby="productNewModalTitle" aria-hidden="true">
@else
<div class="modal fade ecommerce-application" id="productAddNewModal" tabindex="-1" role="dialog" aria-labelledby="productNewModalTitle" aria-hidden="true">
@endif
    <form method="POST" action="#" id="productAddNewForm" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="is_product" value="" id="is_product" />
        <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
            <input type="hidden" name="campaign_id" value="{{$campaign->id}}" />
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="productNewModalTitle">Add Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    
                    @if(count(App\Product::all()) > 0)
                    <div class="row mb-2">
                        <div class="col-12 text-center">
                            <!-- <h4 class="position-absolute float-right">  -->
                            Select Existing product from My Product <i class="feather icon-arrow-right"></i>
                            <!-- </h4> -->
                            <button type="button" class="btn btn-primary" onclick="selectProduct()">
                                <i class="feather icon-cube"></i> My Product</button>
                        </div>
                    </div>
                        @if(session('subscription_usage_remaining') != 0)
                            <hr class="hr-text" data-content="OR" />
                        @else
                            <div class="row mb-2">
                                <div class="col-lg-12">
                                    <div class="alert alert-danger" role="alert">You cannot add new products. You can select from the existing products.</div>
                                </div>
                            </div>
                        @endif
                    @endif
                    @if(session('subscription_usage_remaining'))
                        <div class="row mb-2">
                            <div class="col-lg-12">
                                <div class="alert alert-danger" role="alert">
                                    You can upload only {{ session('subscription_usage_remaining') }} more products.
                                </div>
                            </div>
                        </div>
                    @endif
                    @if(session('subscription_usage_remaining') != 0)
                        <div class="dropzone dropzone-area {{ $errors->has('photo') ? 'is-invalid' : '' }}" id="photo-dropzone">
                            <div class="dz-message">Upload Product Images</div>
                        </div>
                    @else
                        
                    @endif
                    
                    <div class="form-group ar-position">
                        <label for="position">Position</label>
                        <select class="form-control" name="position" id="position">
                            @foreach(App\Arobject::POSITION_SELECT as $key => $label)
                            <option value="{{ $key }}">{{ $label }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancel</button>
                    <button type="submit" id="uploadSubmit" disabled class="btn btn-primary uploadSubmit">Add</button>
                </div>
            </div>

        </div>
    </form>
</div>
<!-- end Add New Product Modal -->

<!-- Added New Product Modal -->
<div class="modal fade ecommerce-application" id="productAddedModal" tabindex="1" role="dialog" aria-labelledby="productAddedModalTitle" aria-hidden="true">
    <form method="POST" action="#" id="productAddForm" enctype="multipart/form-data">
        @csrf
        <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
            <input type="hidden" name="campaign_id" value="{{$campaign->id}}" />
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="productAddedModalTitle">Added Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <section id="ecommerce-products" class="list-view d-block mb-4">
                        <div class="" id="product_list">

                        </div>
                    </section>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancel</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </div>

        </div>
    </form>
</div>
<!-- end Added New Product Modal -->

<!-- Add New Product Filter Modal -->
<div class="modal modal-sm fade" id="productAddNewFilterModal" tabindex="-1" role="dialog" aria-labelledby="productAddNewFilterModalTitle" aria-hidden="true" style="margin-left:25%">
    <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="productAddNewFilterModalTitle">Filters</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card" style="padding-bottom: 0;">
                    <div class="card-body" style="padding-bottom: 2vh;">
                        <!-- Price Filter -->
                        <div class="multi-range-price">
                            <div class="multi-range-title pb-75">
                                <h6 class="filter-title mb-0">Price Range</h6>
                            </div>
                            <ul class="list-unstyled price-range" id="price-range">
                                <li>
                                    <span class="vs-radio-con vs-radio-primary py-25">
                                        <input type="radio" name="price-range" checked value="all">
                                        <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                        </span>
                                        <span class="ml-50">All</span>
                                    </span>
                                </li>

                                <li>
                                    <span class="vs-radio-con vs-radio-primary py-25">
                                        <input type="radio" name="price-range" value="0,500">
                                        <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                        </span>
                                        <span class="ml-50">&lt; {!! $currency !!} 500</span>
                                    </span>
                                </li>

                                <li>
                                    <span class="vs-radio-con vs-radio-primary py-25">
                                        <input type="radio" name="price-range" value="500,1000">
                                        <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                        </span>
                                        <span class="ml-50">{!! $currency !!} 500 - {!! $currency !!} 1000</span>
                                    </span>
                                </li>

                                <li>
                                    <span class="vs-radio-con vs-radio-primary py-25">
                                        <input type="radio" name="price-range" value="1000,10000">
                                        <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                        </span>
                                        <span class="ml-50">{!! $currency !!} 1000 - {!! $currency !!} 10000</span>
                                    </span>
                                </li>

                                <li>
                                    <span class="vs-radio-con vs-radio-primary py-25">
                                        <input type="radio" name="price-range" value="10000">
                                        <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                        </span>
                                        <span class="ml-50">&gt; {!! $currency !!} 10000</span>
                                    </span>
                                </li>
                            </ul>
                        </div>
                        <!-- /Price Filter -->

                        <!-- /Price Slider -->
                        <!-- <div class="price-slider" data-currency="{{$currency}}">
                            <div class="form-group row mt-1">
                                <div class="form-group col-6 w-100">
                                    <input class="form-control" style="width: 100%" type="number" name="price_slider_min" id="price_slider_min" placeholder="Min">
                                </div>
                                <div class="form-group col-6 w-100">
                                    <input class="form-control" style="width: 100%" type="number" name="price_slider_max" id="price_slider_max" placeholder="Max">
                                </div>
                                <div class="alert alert-warning" id="error_min" style="display:none;" role="alert">Minimum value should be less than maximum value.</div>
                                <div class="alert alert-warning" id="error_max" style="display:none;" role="alert">Maximum value should be greater than minimum value.</div>
                            </div>
                            <div class="price-slider">
                                <div class="price_slider_amount mb-2">
                                </div>
                                <div class="form-group">
                                    <div class="slider-sm my-1 range-slider" id="price-slider"></div>
                                </div>
                            </div>
                        </div> -->
                        <!-- /Price Range -->
                        <!-- <hr> -->

                        @if(count($categories)>0)
                        <hr>
                        <!-- Categories Starts -->
                        <div id="product-categories">
                            <div class="product-category-title">
                                <h6 class="filter-title mb-1">Categories</h6>
                            </div>
                            <ul class="list-unstyled categories-list">
                                <li class="d-flex justify-content-between align-items-center py-25">
                                    <span class="vs-checkbox-con vs-checkbox-primary">
                                        <input type="checkbox" name="category-filter-all" checked value="all">
                                        <span class="vs-checkbox">
                                            <span class="vs-checkbox--check">
                                                <i class="vs-icon feather icon-check"></i>
                                            </span>
                                        </span>
                                        <span class="">All</span>
                                    </span>
                                </li>
                                @foreach($categories as $key => $category)
                                <li class="d-flex justify-content-between align-items-center py-25">
                                    <span class="vs-checkbox-con vs-checkbox-primary">
                                        <input type="checkbox" name="category-filter" value="{!! $category->name == " Untitled"?"&nbsp;":$category->name !!}">
                                        <span class="vs-checkbox">
                                            <span class="vs-checkbox--check">
                                                <i class="vs-icon feather icon-check"></i>
                                            </span>
                                        </span>
                                        <span class="">{!! $category->name == "Untitled"?"&nbsp;":$category->name !!}</span>
                                    </span>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- Categories Ends -->
                        <hr>
                        @endif
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="clear_filter()">Clear filters</button>
                <button type="button" id="uploadSubmit" class="btn btn-primary" onclick="apply_filter()">Apply filters</button>
            </div>
        </div>
    </div>
</div>
<!-- End Add New Product Filter Modal -->
@section('scripts')
@parent

<script src="{{asset('XR/app-assets/js/scripts/pages/app-ecommerce-shop.js')}}"></script>

<script>
// console.log("imported");
var clicked_video_id = '';
var uploadnewproduct_type = "";
    var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    if(isMobile){
        select_all_my_products
        $("#select_all_my_products").addClass("col-sm-12");
        $("#deselect_all_my_products").addClass("col-sm-12");
        document.getElementById("productAddNewFilterModal").style.marginLeft = "0px";
    }
    $('#productSelectModal').on('show.bs.modal', function() {
        // do something…
        console.log('select')
        // getData(0);
    })

    $('#productSelectModal').on('hidden.bs.modal', function() {
        filter = [];
        $("[name=price-range]").filter("[value=all]").prop("checked", true);
        $("[name=category-filter-all]").filter("[value=all]").prop("checked", true);
        $("[name=category-filter]").prop("checked", false);
        $("#deselect_all_my_products").hide();
        $("#select_all_my_products").show();
    })

    $('#productAddNewModal').on('hidden.bs.modal', function() {
        // do something…
        // $('#productSelectModal').modal('show');
        Dropzone.forElement('#photo-dropzone').removeAllFiles(true)
    })

    $('#productAddedModal').on('show.bs.modal', function() {
        // do something…
        getAddedData(0);
    })

    function show_filter(){
        $("#productAddNewFilterModal").modal('show');
    }

    function clear_filter() {
        filter = [];
        clicked_video_id = $("#editid").val();
        $("[name=price-range]").filter("[value=all]").prop("checked", true);
        $("[name=category-filter-all]").filter("[value=all]").prop("checked", true);
        $("[name=category-filter]").prop("checked", false);
        $("#productAddNewFilterModal").modal('hide');
        getData(0,clicked_video_id);
    }
    function apply_filter() {
        filter = [];
        var price_count = 0;
        var category_count = 0;
        var price_values_array = [];
        var category_values_array = [];
        clicked_video_id = $("#editid").val();
        var price = document.getElementsByName('price-range');
        for (i = 0; i < price.length; i++) {
            if (price[i].checked) {
                price_count++;
                filter.push({
                    type: 'price',
                    value: price[i].value
                });
            }
        }
        if (price_count == 0) {
            if ((price_slider_min.value != "") && (price_slider_max.value == "")) {
                filter.push({
                    type: 'price',
                    value: price_slider_min.value
                });
            } else if ((price_slider_min.value == "") && (price_slider_max.value != "")) {
                price_values_array.push(0);
                price_values_array.push(price_slider_max.value);
                var price_values = price_values_array.toString();
                filter.push({
                    type: 'price',
                    value: price_values
                });
            } else {
                price_values_array.push(price_slider_min.value);
                price_values_array.push(price_slider_max.value);
                var price_values = price_values_array.toString();
                filter.push({
                    type: 'price',
                    value: price_values
                });
            }
        }

        var catg = document.getElementsByName('category-filter');
        if(catg.length == 0){
            category_count++;
            filter.push({
                type: 'category',
                value: 'all'
            });
        }
        else{
            if ($('input[name="category-filter-all"]').is(":checked") == true) {
                category_count++;
                filter.push({
                    type: 'category',
                    value: $('input[name="category-filter-all"]').val()
                });
            } else {
                for (i = 0; i < catg.length; i++) {
                    if (catg[i].checked) {
                        category_count++;
                        category_values_array.push(catg[i].value);
                        var category_values = category_values_array.toString();
                    }
                }
                if (category_count > 0) {
                    filter.push({
                        type: 'category',
                        value: category_values
                    });
                }
            }
        }
        
        // console.log(filter);
        $("#productAddNewFilterModal").modal('hide');
        getData(0,clicked_video_id);
    }

    $('input[name="category-filter-all"]').on('change', function() {
        if ($('input[name="category-filter-all"]').is(":checked") == true) {
            $('input[name="category-filter"]').prop('checked', false);
        }
    });

    $('input[name="category-filter"]').on('change', function() {
        if ($('input[name="category-filter"]').is(":checked") == true) {
            $('input[name="category-filter-all"]').prop('checked', false);
        }
    });

    // $(window).on('hashchange', function () {
    //     if (window.location.hash) {
    //         page = window.location.hash.replace('#', '');
    //         if (page == Number.NaN || page <= 0) {
    //             return false;
    //         } else {
    //             getData(page);
    //         }
    //     }
    // });

    $("#ecommerce-products").on('click', '.pagination a', function(event) {
        event.preventDefault();

        $('li').removeClass('active');
        $(this).parent('li').addClass('active');

        var myurl = $(this).attr('href');
        console.log(myurl);
        var page = $(this).attr('href').split('page=')[1];
        clicked_video_id = $("#editid").val();
        console.log(clicked_video_id);

        getData(page,clicked_video_id);
    });
    var q = "";
    var searchData = "";
    var sort = "";
    var page = 0;
    var filterData = "";
    var filter = [];

    $("#ModalProductSearch").on('input', function() {
        searchData = $(this).val();
        clicked_video_id = $("#editid").val();
        if (searchData.length >= 3 || searchData == "")
            getData(0,clicked_video_id);
    });

    $("#productSelectModalCatagory").change(function() {
        var selectedCountry = $(this).children("option:selected").val();
        alert("You have selected - " + selectedCountry);
    });

    function getData(page, videoid) {
        q = 'q=' + searchData + '&s=' + sort + '&page=' + page + '&filterData=' + filterData + '&filter=' + JSON.stringify(filter)+ '&videoid='+ videoid;
        console.log(q);
        $.ajax({
            url: "{{route('admin.products.ajaxData')}}" + '?' + q,
            type: "get",
            datatype: "html",
            data:{campaign_id: "{{$campaign->id}}", videoid: videoid}
        }).done(function(data) {
            // console.log("product");
            // console.log(data)
            $("#product_gird").empty().html(data);
            // $("#product_list").append(data);
            location.hash = page++;
            // $('#loading-bg').hide();
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            // $('#loading-bg').hide();
            // alert('No response from server');
        });
    }

    function getAddedData(page) {
        $('#loading-bg').show();
        // if(searchData == "")
        //     q = 'page=' + page;
        // else
        q = 'q=' + searchData + '&s=' + sort + '&page=' + page + '&filterData=' + filterData;

        $.ajax({
            url: "{{route('admin.products.AddedajaxData')}}" + '?' + q,
            type: "get",
            datatype: "html"
        }).done(function(data) {
            $("#product_list").empty().html(data);
            // $("#product_list").append(data);
            location.hash = page++;
            $('#loading-bg').hide();
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            $('#loading-bg').hide();
            // alert('No response from server');
        });
    }

    function addnewproduct() {
        $('#productSelectModal').modal('hide');
        $('#productAddNewModal').modal('show');
        $('#is_product').val("NONAR");

        $("#productNewModalTitle").html("Add Product Image");
        $(".dz-message").html("Upload Product Images");
        $(".ar-position").remove();

    }
    var video_id = "";
    function selectProduct(videoid = "") {
        console.log(videoid );
        video_id = videoid;
        if(videoid){
            $('#videoEditorModal').modal('hide');
        }
        $('#productAddNewModal').modal('hide');
        $('#productSelectModal').modal('show');
        getData(0, videoid)
    }

    // $("#productAddForm").submit(function(e) {
    $(document).on('submit',"#productAddForm", function(e) {
        e.preventDefault();
        $('#productAddNewModal').modal('hide');
        $('#productSelectModal').modal('hide');
        $('#loading-bg').show();
        // console.log($('#productAddForm').serialize());
        var selectedProducts = $('#productAddForm').serializeArray();
        // var mydata = new Array();
        if (localStorage.getItem('selectedProducts') !== null && localStorage.getItem('selectedProducts') !== "") {
            $.each(JSON.parse(localStorage.getItem('selectedProducts')), function(i, field) {
                if (selectedProducts.findIndex(x => x.value[0] == field[0]) == -1) {
                    // console.log("submit field",field[0]);
                    selectedProducts.push({
                        'name': 'product_id[]',
                        'value': field[0]
                    });
                    selectedProducts.push({
                        'name': 'catagory_id[]',
                        'value': field[1]
                    });
                }
            });

            console.log("selectedProducts",selectedProducts);

        }

        if (selectedProducts.findIndex(x => x.name == 'product_id[]') > 0) {
            console.log("product add")
            $.ajax({
                type: "POST",
                url: "{{ route('admin.campaigns.createEntityCard') }}",
                data: selectedProducts,
                success: function(data) {
                    $('#loading-bg').hide();
                    // console.log(data);
                    if (data.status) {
                        localStorage.setItem("selectedProducts", "");
                        Swal.fire({
                            type: "success",
                            title: 'Added!',
                            text: 'Product has been added to the catalog.',
                            confirmButtonClass: 'btn btn-success',
                        }).then((result) => {
                            window.location.reload();
                        });
                    }
                }
            });
        } else {
            Swal.fire('Error!', "Please select one or more product", 'warning');
        }
    });

    $("#productAddNewForm").submit(function(e) {
        e.preventDefault();
        $('#loading-bg').show();
        $(".uploadSubmit").prop('disabled', true);
        var inputOptions = new Promise(function(resolve) {
                resolve({
                    'single': '<div><div class="text-center"><img src="{{asset('XR/app-assets/images/icons/single_product.png')}}" alt="users avatar" class="users-avatar-shadow rounded" height="90" width="90"></div><br><div class="text-center" style="width: 140px;">Single Product</div></div>',
                    'multiple': '<div><div class="text-center"><img src="{{asset('XR/app-assets/images/icons/multiple_products.png ')}}" alt="users avatar" class="users-avatar-shadow rounded" height="90" width="90"></div><br><div class="text-center" style="width: 140px;">Multiple Products</div></div>'
                });
            });
        if(myProductDropzone_1.getQueuedFiles().length  >= 2 ) {
            swal.fire({
                    title: 'Upload image as',
                    type: 'warning',
                    input: 'radio',
                    inputOptions: inputOptions,
                    inputValidator: (value) => {
                        return !value && 'You need to select something!'
                    },
                    reverseButtons: true,
                    showCancelButton: true,
                    confirmButtonText: "Submit",
                    cancelButtonText: "Cancel",
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-light',
                }).then(function(result) {
                    console.log(result);
                    if (result.value) {
                        myProductDropzone_1.processQueue();
                        uploadnewproduct_type = result.value
                    }
                })
        } else if(myProductDropzone_1.getQueuedFiles().length  == 1) {
            myProductDropzone_1.processQueue();
            uploadnewproduct_type = "single";
        }

    });

    function uploadnewproduct(type) {
        var photos = $("#productAddNewForm").find('input[name="photo[]"]');
        $("#uploadSubmit").prop('disabled', true);
        if(photos == undefined)
            {
                $.ajax({
                type: "POST",
                url: "{{route('admin.products.logshow')}}",
                data: '',
                success: function(data) {
                    $('#loading-bg').show();
                        location.reload();
                }
                });
            }
        if ($('#is_product').val() == "NONAR") {
                $('#loading-bg').show();
                $.ajax({
                    type: "POST",
                    url: "{{route('admin.products.store')}}",
                    data: $('#productAddNewForm').serialize() + '&producttype=' + type + '&videoid=' + video_id, // serializes the form's elements.
                    success: function(data) {
                        afteraddedProduct();
                    },
                    error: function (jqXHR, exception) {
                    var msg = '';
                    var photoerror = '';
                    if (jqXHR.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                        photoerror = jqXHR.responseText;
                    }
                    if(photoerror){
                     photoerror = JSON.parse(photoerror);
                     console.log(photoerror.message);
                     Swal.fire({
                            type: "error",
                            title: 'file upload error!',
                            text: photoerror.message,
                            confirmButtonClass: 'btn btn-success',
                        }).then((result) => {
                            window.location.reload();
                        });
                    }
                    
                },
                });
                // location.reload();
            
        } else if ($('#is_product').val() == "AR") {
            $('#loading-bg').show();
            $.ajax({
                type: "POST",
                url: "{{route('admin.products.store')}}",
                data: $('#productAddNewForm').serialize() + '&producttype=multiple', // serializes the form's elements.
                success: function(data) {
                    // window.location.href = "{{route('admin.products.product_list')}}"
                    $('#loading-bg').hide();
                    afteraddedProduct();
                }
            });
            $('#loading-bg').hide();
        }
    }

    function afteraddedProduct() {
        Dropzone.forElement('#photo-dropzone').removeAllFiles(true)
        // Swal.fire({
        //     type: "warning",
        //     title: 'Product Details',
        //     text: 'Do you want to add additional product details?',
        //     showCancelButton: true,
        //     confirmButtonText: "Yes",
        //     cancelButtonText: "No",
        //     confirmButtonClass: 'btn btn-primary',
        //     cancelButtonClass: 'btn btn-light',
        // }).then((result) => {
        //     console.log("added product", result)
        //     if (result.isConfirmed) {
        //         window.location.href = "{{route('admin.products.product_list')}}";
        //     } else {
                $('#productAddNewModal').modal('hide');
                $('#loading-bg').hide();
                location.reload();
                // $('#productSelectModal').modal('show');
        //     }
        // });
    }
    
    function imagelog(msg){
        $.ajax({
                type: "POST",
                url: "{{route('admin.products.log')}}",
                data: '&msg=' + msg,
                success: function(data) {

                }

    });
    }
</script>
<script>
    console.log("Product dropzone");
    var uploadedPhotoMap = {}
    // Dropzone.options.photoDropzone = {
        // Dropzone.autoDiscover = false;
    myProductDropzone_1 = new Dropzone('#photo-dropzone', {
        autoProcessQueue: false,
        parallelUploads: "{{ session('subscription_usage_remaining') ?? 50 }}",
        url: "{{ route('admin.products.storeMedia')}}",
        maxFilesize: 40, // MB
        maxFiles: "{{ session('subscription_usage_remaining') ?? 50 }}",
        acceptedFiles: '.jpeg,.jpg,.png,.gif',
        addRemoveLinks: true,
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        params: {
            size: 40,
        },
        // maxFiles: 1 ,
        success: function(file, response) {
            $('form').append('<input type="hidden" name="photo[]" value="' + response.name + '">')
            uploadedPhotoMap[file.name] = response.name
        },
        removedfile: function(file) {
            file.previewElement.remove()
            var name = ''
            if (typeof file.file_name !== 'undefined') {
                name = file.file_name
            } else {
                name = uploadedPhotoMap[file.name]
            }
            $('form').find('input[name="photo[]"][value="' + name + '"]').remove()
        },
        init: function() {
            console.log('product image dropzone init',this.getQueuedFiles().length)
            // var submitButton = document.querySelector("#uploadSubmit");
            // myDropzone = this; // closure
            // submitButton.addEventListener("click", function() {
            //     if (myDropzone.getUploadingFiles().length === 0 && myDropzone.getQueuedFiles().length === 0) {} else {
            //         myDropzone.processQueue();
            //     }
            // });
            this.on("addedfile", file => {
                console.log("upload",this.getQueuedFiles().length)
                if(this.getQueuedFiles().length >= 0)
                    $(".uploadSubmit").prop("disabled",false)
                else
                    $(".uploadSubmit").prop("disabled",true)
            });
            this.on("removedfile", file => {
                console.log("upload removed")
                if(this.getQueuedFiles().length >= 0)
                    $(".uploadSubmit").prop("disabled",false)
                else
                    $(".uploadSubmit").prop("disabled",true)
            });
            this.on("dragend, processingmultiple", function(file) {
                // $("#uploadSubmit").prop('disabled', true);
            });
            this.on("queuecomplete", function(file) {
                // if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                //     $("#uploadSubmit").prop('disabled', false);
                // } else {
                //     $("#uploadSubmit").prop('disabled', true);
                // }
                $('#loading-bg').show();
                $('#productAddNewModal').modal('hide');
                uploadnewproduct(uploadnewproduct_type);
            })
            // this.on("addedfile", function (file) {
            // if (this.files.length === 0) {
            //     $("#uploadSubmit").prop('disabled', true);
            // } else {
            //     $("#uploadSubmit").prop('disabled', false);
            // }
            // });

        },
        error: function(file, response) {
            if ($.type(response) === 'string') {
                var message = response //dropzone sends it's own error messages in string
                imagelog(message);

            } else {
                var message = response.errors.file
                imagelog(message);
            }
            file.previewElement.classList.add('dz-error')
            _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
            _results = []
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                node = _ref[_i]
                _results.push(node.textContent = message)
            }

            return _results
        }
    })
</script>
@endsection