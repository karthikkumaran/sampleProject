<div class="modal fade " id="StorePublishedModal" tabindex="-1" role="dialog"
    aria-labelledby="StorePublishedModalTitle" aria-hidden="true">
       <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Share Store - {{$meta_data['storefront']['storename'] ?? "Store "}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                <h5><b>Share your store to social media</b></h5>
                <b>
                <button type="button" class="btn btn-icon rounded-circle mr-1 mb-1 text-success share-icon " onclick='sharestore("whatsapp","")' data-share="whatsapp"><i class="fa fa-whatsapp"></i><br><br>WhatsApp</button>
                <button type="button" class="btn btn-icon rounded-circle mr-1 mb-1 text-primary share-icon " onclick='sharestore("fb","")' data-share="fb"><i class="feather icon-facebook"></i><br><br>Facebook</button>
                <button type="button" class="btn btn-icon rounded-circle mr-1 mb-1 text-info share-icon " onclick='sharestore("twitter","")'data-share="twitter"><i class="feather icon-twitter"></i><br><br>Twitter</button>        
                </b>
                <br>
                <br>
                <div style="border:dotted;border-radius: 10px;width: 100%;word-break: break-all;">
                    <button class="btn btn-flat btn-icon" onclick="StoreLinkCopyToClipboard('#public_store_link_copy')">
                        <span id="public_store_link_copy"> {{App\Http\Controllers\Admin\CampaignsController::publicLink($userSettings->public_link,'store')}} </span><br><br><i class="feather icon-copy"></i> Tap to copy
                    </button>
                    <a href="{{App\Http\Controllers\Admin\CampaignsController::publicLink($userSettings->public_link,'store')}}" target="__blank" class="btn btn-sm gradient-light-primary mb-1" >Goto Store</a>
                </div>  
                <hr>
             
                <!-- <h5>Your store QR code</h5> -->
                <a href="{{App\Http\Controllers\Admin\CampaignsController::publicLink($userSettings->public_link,'store')}}" target="__blank">
                    <img class="card-img-top img-fluid" src="{{App\Http\Controllers\Admin\CampaignsController::publicLink($userSettings->public_link,'store','',true)}}" style="width:150px;">
                </a> <br>
                <!-- <h5>Download your store QR code</h5> -->
                <a href="{{App\Http\Controllers\Admin\CampaignsController::publicLink($userSettings->public_link,'store','',true)}}" target="__blank" class="btn btn-md border border-primary mb-1" id="QRdownload" download="{{$meta_data['storefront']['storename'] ?? 'Store'}}"><span><i class="feather icon-download"></i> Download Store QR code</span></a>            

                </div>
                <!-- <div class="modal-footer">
                
                </div> -->
            </div>

        </div>
</div>

<script>
function StoreLinkCopyToClipboard(element) {
  var $temp = $("<input>");
  $("#StorePublishedModal").append($temp);
  $temp.val($(element).text());
  $temp.select();
  document.execCommand("copy");
  $temp.remove();
  toastr.success('Share Link', 'Copied!');
}
function sharestore(share_name, msg) {
        // console.log(share_name);
        var u = $("#public_store_link_copy").html();
        var t =  "Please checkout our Store";
        var hashtags = "{{ isset($meta_data['storefront']['storename'])?$meta_data['storefront']['storename'] : ''}}";
        // console.log(t[0], "hi", t[1]);
        switch (share_name) {
            case 'fb':
                // window.open('http://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t[0] + " #" + hashtags), 'sharer', 'toolbar=0,status=0,width=626,height=436');
                openInNewTab('http://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t[0] + " #" + hashtags));

                break;
            case 'whatsapp':
                var message = t + " " + encodeURIComponent(u) + " #" + hashtags;
                // var whatsapp_url = "whatsapp://send?text=" + message;
                var whatsapp_url = "https://wa.me/?text=" + message;
                openInNewTab(whatsapp_url);
                break;
            case 'twitter':
                // window.open("https://twitter.com/intent/tweet?text=" + encodeURIComponent(t[0]) + "&url=" + u + "&hashtags=" + hashtags, 'sharer', 'toolbar=0,status=0,width=626,height=436')
                openInNewTab("https://twitter.com/intent/tweet?text=" + t + "&url=" + u + "&hashtags=" + hashtags);

                break;
        }
}
</script>