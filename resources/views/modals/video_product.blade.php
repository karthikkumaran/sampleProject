
<div class="modal fade ecommerce-application" id="videoProductModal" tabindex="-1" role="dialog"  aria-hidden="true">
        @csrf
        <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
            <input type="hidden" name="campaign_id" value="{{$campaign->id}}" />
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="productSelectModalTitle">Select Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer"></div>
<script>
    function getVideoProduct(id){
        $('#loading-bg').show();
        $("#videoProductModal").modal('show')
        var url = "{{route('admin.videos.video_product',':id')}}";
        url = url.replace(':id',id);
        $.ajax({
            url: url,
            type: "get",
            datatype: "html"
        }).done(function (data) {
            
            $("#videoProductModal .modal-body").empty().html(data);
            // $("#product_list").append(data);
            $('#loading-bg').hide();
        }).fail(function (jqXHR, ajaxOptions, thrownError) {
            $('#loading-bg').hide();
            // alert('No response from server');
        });
    }
</script>
