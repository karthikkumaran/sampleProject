@php
$userSettings = auth()->user()->userInfo;
$meta_data = json_decode($userSettings->meta_data, true);
@endphp
<style>
    .share-icon i {
        font-size: 2rem !important;
    }
</style>
<div class="modal fade " id="CatalogPublishedModal" tabindex="-1" role="dialog" aria-labelledby="PublishedModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Share Catalog - <span id="title_name"></span></h5>
                <input type="hidden" id="link_share_id" value="" />
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
      
                

               <h5><b>Share your catalog to social media</b></h5>
                <button type="button" class="btn btn-icon rounded-circle mr-1 mb-1 text-success share-icon share-button" data-share="whatsapp"><i class="fa fa-whatsapp"></i><br><br>WhatsApp</button>
                <button type="button" class="btn btn-icon rounded-circle mr-1 mb-1 text-primary share-icon share-button" data-share="fb"><i class="feather icon-facebook"></i><br><br>Facebook</button>
                <button type="button" class="btn btn-icon rounded-circle mr-1 mb-1 text-info share-icon share-button" data-share="twitter"><i class="feather icon-twitter"></i><br><br>Twitter</button>
                <button type="button" class="btn btn-icon rounded-circle mr-1 mb-1 text-danger share-icon" id="pdfbtn" onclick='share("pdf","")'><i class="fa fa-file-pdf-o"></i><br><br>PDF</button>
                <button type="button" class="btn btn-icon rounded-circle mr-1 mb-1 text-black share-icon" id="downloadbtn" onclick='share("download","")'><i class="feather icon-download"></i><br><br>Download</button>
        
                <hr>
                <div style="border:dotted;border-radius: 10px;width: 100%;word-break: break-all;">
                    <button class="btn btn-flat btn-icon" style="" onclick="catalogCopyToClipboard('#public_catalog_link_copy')">
                        <span id="public_catalog_link_copy"></span><br><br><i class="feather icon-copy"></i> Tap to copy
                    </button>
                    <a href="" target="__blank" id="public_catalog_link_open" class="btn btn-sm gradient-light-primary mb-1">Goto Store</a>
                </div>
                <hr>
               <div id="embbed" style="display:none;">
                    <h5>Embbed Script</h5>
                    <button class="btn btn-flat btn-icon public_embbed_tryon_link w-100" style="" onclick="catalogCopyToClipboard('#public_embbed_tryon_link_copy')">
                        <span id="public_embbed_tryon_link_copy" style="text-align:left;"></span><br><br><i class="feather icon-copy"></i> Virtual Tryon Tap to copy 
                    </button>
                    <button class="btn btn-flat btn-icon public_embbed_viewar_link w-100" style="" onclick="catalogCopyToClipboard('#public_embbed_viewar_link_copy')">
                        <span id="public_embbed_viewar_link_copy" style="text-align:left;"></span><br><br><i class="feather icon-copy"></i> AR in Space Tap to copy
                    </button>
                </div>
               
                <hr>

                <!-- <h5>Your Catalog QR code</h5> -->
                <a href="#" target="__blank">
                    <img class="card-img-top img-fluid" id="QRcode" src="#" style="width:150px !important;">
                </a> <br>
                <!-- <h5>Download your catalog QR code</h5> -->
                <a href="" class="btn btn-md border border-primary mb-1" target="__blank" id="QRdownload" download=""><span><i class="feather icon-download"></i> Download Catalog QR code</spanp></a>
            </div>
            <!-- <div class="modal-footer">
            </div> -->
        </div>

    </div>
</div>

<div class="modal fade" id="share_options_modal" tabindex="-1" data-bs-backdrop="static" data-bs-keyboard="false" aria-labelledby="share_options_modal" aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <a href="#" class=" btn btn-sm border-2 border-primary mb-1 share-option" data-value="txt">Share as Text</a></br>
                    <a href="#" class=" btn btn-sm border-2 border-primary share-option" data-value="img">Share as Image</a>
                </div>
            </div>
        </div>
    </div>
</div>

@section('scripts')
@parent
<script>
    var catalog_data = null;
    var link = '';
    var entity = '';
    var share_Link = '';
    var tempsharelink = '123';
    // var share_config = {};

    var shareButton = document.querySelector('.share-button');
    var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    var product_name;
    var product_price;
    var product_currency;
    var product_sku;
    var product_discount;
    var product_discount_price;
    var product_image;
    var txt = "";
    var t = txt + "\n";
    var u = "";
    var file_name;
    var file, i;
    var filesArray = [];


    function linkShare($this) {

        //product
        entity = 'product';
        $("#title_name").html($this.data('title'))
        $("#link_share_id").val($this.data('link'))
        $("#public_catalog_link_copy").html($this.data('sharelink'))
        $("#public_catalog_link_open").attr("href", $this.data('sharelink'));
        $("#QRcode").attr("src", $this.data('qrcode'))
        $("#QRdownload").attr("href", $this.data('qrcode'))
        $("#QRdownload").attr("download", $this.data('title'))
        $("#CatalogPublishedModal").modal('show');
        $('#embbed').show();
        var tryon_link='<iframe src="'+$this.data('tryonlink')+'&embbed=true" width="100%" height="100%" frameborder="0" allowfullscreen></iframe>';
        var viewar_link='<iframe src="'+$this.data('viewarlink')+'&embbed=true" width="100%" height="100%" frameborder="0" allowfullscreen></iframe>';
        var isEmbbed = false;
        if($this.data('tryonlink') != ''){
            isEmbbed = true;
            $('.public_embbed_tryon_link').show();
            $('#public_embbed_tryon_link_copy').text(tryon_link);
        } else {
            $('.public_embbed_tryon_link').hide();
        }
        
        if($this.data('viewarlink') != ''){
            isEmbbed = true;
            $('.public_embbed_viewar_link').show();
            $('#public_embbed_viewar_link_copy').text(viewar_link);
        } else {
            $('.public_embbed_viewar_link').hide();
        }

        if(!isEmbbed)
            $('#embbed').hide();            
        
        if ($this.data("pdf") == false) {
            $("#pdfbtn").hide();
        } else {
            $("#pdfbtn").show();
        }
        if ($this.data("download") == false) {
            $("#downloadbtn").hide();
        } else {
            $("#downloadbtn").show();
        }
        file_name = product_name = $this.data('name');
        if (product_name) {
            file_name = product_name
        } else {
            file_name = "untitled";
        }
        file_name += ".png";
        filesArray = [];
        product_price = $this.data('price').trim();
        product_currency = $this.data('currency');
        product_sku = $this.data('sku');
        product_discount = $this.data('discount');
        product_discount_price = $this.data('discounted_price');
        product_image = $this.data('img');
        toDataURL(product_image, file_name);
        t = txt + "\n";
        if (product_name) {
            t += "*" + product_name + "*";
        }
        if (product_price) {
            if (product_discount) {
                t += "\n" + product_currency + product_discount_price + " ~" + product_price + "~";
            } else {
                t += "\n" + product_currency + product_price;
            }
        }
        if (product_discount) {
            t += "\n" + "*Discount:* " + product_discount + "% OFF";
        }
        if (product_sku) {
            t += "\n" + "SKU: " + product_sku;
        }
        t += "\n" + "*Order Now:* ";
    }

    function dataURLtoFile(dataurl, file_name) {
        var arr = dataurl.split(','),
            mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]),
            n = bstr.length,
            u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], file_name, {
            type: mime
        });
    }

    function toDataURL(src, callback, outputFormat) {
        var img = new Image();
        img.crossOrigin = 'Anonymous';
        img.onload = function() {
            var canvas = document.createElement('CANVAS');
            var ctx = canvas.getContext('2d');
            var dataURL;
            canvas.height = this.naturalHeight;
            canvas.width = this.naturalWidth;
            ctx.drawImage(this, 0, 0);
            dataURL = canvas.toDataURL(outputFormat);
            file = dataURLtoFile(dataURL, file_name);
            filesArray.push(file);
            // callback(dataURL);

        };

        img.src = src;
        if (img.complete || img.complete === undefined) {
            img.src = src;
        }
    }

    $(".sharebtn").click(function() {
        // console.log('catalog');
        //catalog.
        entity = 'catalog';
        $("#title_name").html($(this).data('title'))
        share_Link = $(this).data('link');
        $("#link_share_id").val($(this).data('link'))
        $("#public_catalog_link_copy").html($(this).data('sharelink'))

        $("#public_catalog_link_open").attr("href", $(this).data('sharelink'));
        $("#QRcode").attr("src", $(this).data('qrcode'))
        $("#QRdownload").attr("href", $(this).data('qrcode'))
        $("#QRdownload").attr("download", $(this).data('title'))
        $("#CatalogPublishedModal").modal('show');
        if ($(this).data("pdf") == false) {
            $("#pdfbtn").hide();
        } else {
            $("#pdfbtn").show();
        }
        if ($(this).data("download") == false) {
            $("#downloadbtn").hide();
        } else {
            $("#downloadbtn").show();
        }
    });




    //modal share button
    $(".share-button").click(function() {

        u = $("#public_catalog_link_copy").html();

        // share_config = {
        //     title: 'Simplisell',
        //     text: t,
        //     files: filesArray,
        //     url: u,
        // };

        if (isMobile) {
            if (entity == "product") {

                var ShareOptionModal = new bootstrap.Modal(document.getElementById('share_options_modal'));
                ShareOptionModal.toggle();

            } else {
                if (share_Link != tempsharelink)
                    get_catalog_products(share_Link, true, '');
                else
                    Catalogshare(catalog_data, true, '');
            }

        } else {
            if (entity != "product") {
                if (share_Link != tempsharelink)
                    get_catalog_products(share_Link, false, $(this).attr("data-share"));
                else {
                    Catalogshare(catalog_data, false, $(this).attr("data-share"));
                }
            } else {
                share($(this).attr("data-share"), "", t);
            }
        }
    });

    $('.share-option').click(function() {
        var share_config = {};
        if ($(this).attr('data-value') == 'img') {
            share_config = {
                title: 'Simplisell',
                text: t,
                files: filesArray,
                url: u,
            };
        } else {
            share_config = {
                title: 'Simplisell',
                text: t,
                url: u,
            };
        }
        mobile_share(share_config);

    });

    function get_catalog_products(link, ismobile, share_option) {

        tempsharelink = share_Link;
        var text = '';
        $.ajax({
            url: "{{route('admin.campaigns.shareCatalogData')}}",
            method: 'GET',
            data: {
                key: link,
            },
            type: 'json',
            success: function(data) {
                // console.log(data);
                text = data.data;
                catalog_data = text;

                // catalog_data=$('<div>').html(text);
                // console.log(catalog_data);
                // text.forEach(function(item, index) {

                // catalog_data

                //     if (ismobile) {
                //         catalog_data += "\n";
                //         catalog_data += "*" + item.name + "*";
                //         catalog_data += "\n" + "~" + item.price + "~";
                //         catalog_data += "\n" + item.discount;
                //         catalog_data += "--------------------";
                //     } else {
                //         catalog_data += "<br>";
                //         catalog_data += "<b>" + item.name + "</b><br>";
                //         catalog_data += "<strike>" + item.price + "</strike><br>";
                //         catalog_data += "<span>" + item.discount + "</span><br>";
                //         catalog_data += "<span>--------------------</span>" + "<br>";
                //     }
                // });

                Catalogshare(catalog_data, ismobile, share_option);
            }
        });
    }

    function Catalogshare(text, ismobile, share_option) {

        t = text;
        if (ismobile) {
            var share_config = {
                title: 'Simplisell',
                text: t[0],
                url: u,
            };
            mobile_share(share_config);
        } else {
            // console.log(t);

            share(share_option, "", t);
            // if (share_option == "whatsapp") {
            //     share("whatsapp", "", t);
            // } else if (share_option == "facebook") {
            //     share("fb", "", t);
            // } else if (share_option == "twitter") {
            //     share("twitter", "", t);
            // }
        }

    }

    function mobile_share(share_config) {
        // console.log(share_config);
        if (entity == "product") {

            // if (navigator.canShare && (navigator.canShare({files: filesArray}))) {
            if (navigator.canShare) {

                navigator.share(
                        share_config
                        // {
                        // title: 'Simplisell',
                        // text: t,
                        // files: filesArray,
                        // url: u
                        // }
                    ).then(() => {

                        // console.log("thanks ");
                    })
                    .catch((error) => {
                        // console.log(error);
                    });
                // alert(navigator.share);
            } else {
                // window.alert("error");

            }
        } else {
            if (navigator.canShare) {
                navigator.share(
                        share_config
                        // {
                        // title: 'Simplisell',
                        // text: t,
                        // files: filesArray,
                        // url: u
                        // }
                    ).then(() => {
                        // console.log("thanks ");
                        // window.alert("hello");
                    })
                    .catch(console.error);
                // alert(navigator.share);
            } else {
                // window.alert("error");
            }
        }
    }
</script>
@endsection
<script>
    function catalogCopyToClipboard(element) {
        var $temp = $("<input>");
        $("#CatalogPublishedModal").append($temp);
        $temp.val($(element).text());
        $temp.select();
        document.execCommand("copy");
        $temp.remove();
        toastr.success('Share Link', 'Copied!');
    }

    function share(share_name, msg, t) {
        // console.log(share_name);
        var u = $("#public_catalog_link_copy").html();
        var t = entity == "catalog" ? t : "I found this product interesting, open the link and check";
        var hashtags = "{{ isset($meta_data['storefront']['storename'])?$meta_data['storefront']['storename'] : ''}}";
        // console.log(t[0], "hi", t[1]);
        switch (share_name) {
            case 'fb':
                // window.open('http://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t[0] + " #" + hashtags), 'sharer', 'toolbar=0,status=0,width=626,height=436');
                openInNewTab('http://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t[0] + " #" + hashtags));

                break;
            case 'whatsapp':
                var message = encodeURIComponent(t[1]) + " " + encodeURIComponent(u) + " #" + hashtags;
                // var whatsapp_url = "whatsapp://send?text=" + message;
                var whatsapp_url = "https://wa.me/?text=" + message;
                openInNewTab(whatsapp_url);
                break;
            case 'twitter':
                // window.open("https://twitter.com/intent/tweet?text=" + encodeURIComponent(t[0]) + "&url=" + u + "&hashtags=" + hashtags, 'sharer', 'toolbar=0,status=0,width=626,height=436')
                openInNewTab("https://twitter.com/intent/tweet?text=" + encodeURIComponent(t[0]) + "&url=" + u + "&hashtags=" + hashtags);

                break;
            case 'pdf':
                let link = "{{route('admin.export_catalog_pdf','id')}}";
                link = link.replace('id', $("#link_share_id").val());
                window.location = link;

                break;
        }

    }
</script>