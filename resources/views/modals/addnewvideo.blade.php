<!-- Add New Video Modal -->
<style>
@media (max-width: 576px) {
    .dropzone {
    min-height: 250px !important;
    }
}
</style>
<div class="modal fade ecommerce-application" id="videoAddNewModal" tabindex="-1" role="dialog" aria-labelledby="productNewModalTitle" aria-hidden="true">
    <form method="POST" action="#" id="videoAddNewForm" enctype="multipart/form-data">
        @csrf
        <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
            <input type="hidden" name="campaign_id" value="{{(!empty($campaign))?$campaign->id:'' }}" />
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="videoNewModalTitle">Add New Video</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                @if(session('subscription_usage_remaining'))
                    <div class="row mb-2">
                        <div class="col-lg-12">
                            <div class="alert alert-danger" role="alert">
                               You can upload only {{ session('subscription_usage_remaining') }} more videos.
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="title">{{ trans('cruds.video.fields.title') }}</label>
                            <input class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}" type="text" name="title" id="title" value="{{ old('title', '') }}" required>
                            @if($errors->has('title'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('title') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.video.fields.title_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="video_url">{{ trans('cruds.video.fields.video_url') }}</label>
                            <input class="form-control {{ $errors->has('video_url') ? 'is-invalid' : '' }}" type="text" name="video_url" id="video_url" value="{{ old('video_url', '') }}" required>
                            @if($errors->has('video_url'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('video_url') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.video.fields.video_url_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="description">{{ trans('cruds.video.fields.description') }}</label>
                            <textarea class="form-control {{ $errors->has('description') ? 'is-invalid' : '' }}" name="description" id="description">{{ old('description') }}</textarea>
                            @if($errors->has('description'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('description') }}
                                </div>
                            @endif
                            <span class="help-block">{{ trans('cruds.video.fields.description_helper') }}</span>
                        </div>
                        <div class="form-group">
                            <label for="status">{{ trans('cruds.video.fields.status') }}</label>
                            <select class="form-control {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" id="status" value="{{ old('status') }}">
                            <option value="1" >Active</option>
                            <option value="0">Inactive</option></select>

                            @if($errors->has('status'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('status') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="file">{{ trans('cruds.video.fields.thumbnail') }}</label>  [Recommended size: 420 X 200]
                        <div class="dropzone dropzone-area {{ $errors->has('thumbnail') ? 'is-invalid' : '' }}" id="uploadVideoThumbnail">
                            <div class="dz-message">Upload Video Thumbnail</div>
                        </div>
                        </div>  
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                    <iframe src="" id="videoIframe" type="text/html" width="480" height="295" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancel</button>
                    <button type="submit" id="uploadSubmit" class="btn btn-primary">Submit</button>
                </div>
            </div>

        </div>
    </form>
</div>
<!-- end Add New Product Modal -->
@section('scripts')
@parent
<script>
    // $("#video_url").change(function(){
    //    $("#videoIframe").src("")
    // });

    // $("#video_url").on('input', function() {
    //     $("#videoIframe").src("https://www.youtube.com/embed/"+$(this).val()+"?wmode=transparent")
    // });

    function addnewvideo() {
        $('#videoAddNewModal').modal('show');
    }

    $("#videoAddNewForm").submit(function(e) {
        e.preventDefault();
        var form = this;
        var el = form.elements;
        var photos = el["photo[]"];
        $("#uploadSubmit").prop('disabled', true);
        console.log($('#videoAddNewForm').serialize());

        $('#loading-bg').show();
        $.ajax({
            type: "POST",
            url: "{{route('admin.videos.store')}}",
            data: $('#videoAddNewForm').serialize(), // serializes the form's elements.
            success: function(data) {
                $('#loading-bg').hide();
                if (data.status) {
                    Swal.fire({
                        type: "success",
                        title: 'Added!',
                        text: 'Video have been added to the catalogs.',
                        confirmButtonClass: 'btn btn-success',
                    }).then((result) => {
                        var url = "{{route('admin.campaigns.edit',':campaign_id')}}"
                        url = url.replace(':campaign_id', data.campaign_id);
                        url = url+"#open-"+data.video_id;
                        document.location.href = url;
                        location.reload();
                        // window.open(url);
                    });
                }
            }
        });
    });
</script>
<script>
    var uploadedPhotoMap = {}
    Dropzone.autoDiscover = false;
    myProductDropzone = new Dropzone('#uploadVideoThumbnail', {
        url: "{{route('admin.videos.storeMedia')}}",
        maxFilesize: 40, // MB
        maxFiles: "{{ session('subscription_usage_remaining') ?? 50 }}",
        acceptedFiles: '.jpeg,.jpg,.png,.gif',
        addRemoveLinks: true,
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        params: {
            size: 40
        },
        success: function(file, response) {
            $('form').append('<input type="hidden" name="thumbnail" value="' + response.name + '">')
        },
        removedfile: function(file) {
            file.previewElement.remove()
            var name = ''
            if (typeof file.file_name !== 'undefined') {
                name = file.file_name
            } else {
                name = uploadedPhotoMap[file.name]
            }
            $('form').find('input[name="thumbnail"][value="' + name + '"]').remove()
        },
        init: function() {
            var submitButton = document.querySelector("#uploadSubmit");
            myDropzone = this; // closure
            submitButton.addEventListener("click", function() {
                if (myDropzone.getUploadingFiles().length === 0 && myDropzone.getQueuedFiles().length === 0) {} else {
                    myDropzone.processQueue();
                }
            });
            this.on("dragend, processingmultiple", function(file) {
                $("#uploadSubmit").prop('disabled', true);
            });
            this.on("queuecomplete", function(file) {
                // if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                //     $("#uploadSubmit").prop('disabled', false);
                // } else {
                //     $("#uploadSubmit").prop('disabled', true);
                // }
            })

        },
        error: function(file, response) {
            if ($.type(response) === 'string') {
                var message = response //dropzone sends it's own error messages in string
            } else {
                var message = response.errors.file
            }
            file.previewElement.classList.add('dz-error')
            _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
            _results = []
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                node = _ref[_i]
                _results.push(node.textContent = message)
            }

            return _results
        }

    });
</script>

@endsection