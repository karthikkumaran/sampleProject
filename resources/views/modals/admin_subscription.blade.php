<div class="modal fade" id="adminSubscription" tabindex="-1" role="dialog"
    aria-labelledby="adminSubscriptionTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Subscribe to plan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <form method="POST" action="{{ route('admin.adminSubscription.createSubscription') }}" name="adminSubscriptionForm" id="adminSubscriptionForm" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="user_id" value="{{$user->id}}">
                    <input type="hidden" name="plan_id" id="plan_id" value="">
                    <div class="row m-0">
                    <div id="subscription_msg_subscribe" style="display: none">
                        <span>Subscribe to</span> <b><span id="plan_name"></span> <span>plan.</span></b>
                    </div>
                    <div id="subscription_msg_renew" style="display: none">
                        <span>Renew your current subscription:</span> <b><span id="renew_plan_name"></span> <span>plan.</span></b>
                    </div>
                    <div id="subscription_msg_upgrade" style="display: none">
                        Upgrade to <b><span id="current_plan_upgrade"></span> <span>plan.</span></b>
                    </div>
                    <div id="subscription_msg_downgrade" style="display: none">
                    <p>Are you sure you want to downgrade?</p>
                        <p><span>Important!</span></p><p><span> You are currently downgrading from</span> <b><span id="current_plan_downgrade"></span> Plan.</b> Please note that all data and configuration associated with the features available in your current Plan will not be accessable.</p>
                        <p>To see the full list of plans and features, please visit Plans and Pricing page. Still not sure? or Let us help you decide, contact our support for further help.</p>
                        <div class="col-12">
                            <div class="form-group mb-0 ml-2">
                                <input class="form-check-input" type="checkbox" id="immediate_downgrade" name="immediate_downgrade" value="1">
                                <label class="form-check-label" id="immediate_downgrade">Check to downgrade immediately</label>
                            </div>
                        </div>
                    </div>
                    </div>
                    <br>
                    <div class="row m-0">
                        <div class="col-12">
                            <h5>Payment Details</h5>
                            <hr>
                            <h6><span class="text-danger">*</span> Required fields</h6>
                        </div>
                        <!-- <div class="col-6">
                            <div class="form-group">
                                <label for="package_type">Package Type<span class="text-danger"></span></label><br>
                                <select class="form-control select2" name="package_type" id="package_type" >
                                    <option value="">Choose package type</option>
                                    <option value="day">Day</option>
                                    <option value="month">Month</option>
                                    <option value="year">Year</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="package">Package <span class="text-danger"></span></label><br>
                                <select class="form-control plan_hide" name="package" id="package" >
                                    <option value="">Choose package </option>
                                    
                                </select>
                                <span id="plan_option"></span>
                            </div>
                        </div> -->
                        <div class="col-12">
                            <div class="form-group">
                                <label for="payment_method">Payment method<span class="text-danger">*</span></label><br>
                                <select class="form-control select2" name="payment_method" id="payment_method" required>
                                    <option value="">Choose payment method</option>
                                    <option value="account_transfer">Account transfer</option>
                                    <option value="cheque">Cheque</option>
                                    <option value="admin_renewal">Admin renewal</option>
                                    <option value="cash">cash</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12" id="plan_amount" style="display:none">
                            <div class="form-group" >
                                <label for="transaction_amount">Transaction Amount<span class="text-danger">*</span></label>
                                <input class="form-control " type="text" name="transaction_amount" id="transaction_id" value="0" >
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="form-group">
                                <label for="transaction_id">Transaction ID<span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="transaction_id" id="transaction_id" value="" required>
                            </div>
                        </div>
                        @php
                        $current_time =Carbon\Carbon::now()->format('Y-m-d\TH:i');
                        @endphp
                        <div class="col-12">
                            <div class="form-group">
                                <label for="transaction_date">Transaction date<span class="text-danger">*</span></label>
                                <input class="form-control" type="datetime-local" name="transaction_date" id="transaction_date" value="{{$current_time}}" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="plan_start_date">Plan start date<span class="text-danger"></span></label>
                                <input class="form-control" type="datetime-local" name="plan_start_date" id="plan_start_date" value="" disabled >
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="plan_end_date">Plan expires on<span class="text-danger"></span></label>
                                <input class="form-control" type="datetime-local" name="plan_end_date" id="plan_end_date" value="" disabled>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="next_pay_date">Next pay date<span class="text-danger"></span></label>
                                <input class="form-control" type="datetime-local" name="next_pay_date" id="next_pay_date" value="" disabled>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="modal_close" data-dismiss="modal"
                        aria-label="Close">Cancel</button>
                    <button type="button" class="btn btn-primary" id="adminSubscribeSubmit">Subscribe</button>
                </div>
            </div>
        </div>
    
</div>
@section('scripts')
@parent
<script>
    var url;
    var subscription;
    var plan_id;
    var start_date = '';
    var end_date = '';
    function dateChanges(id){
        var planUrl = "{{route('admin.plan.data')}}";
        $.ajax({
                    url: planUrl,
                    type: "POST",
                    data: {
                        'plan_id': id
                    },
                    success: function(data) {
                        start_date = data.start_date.substr(0, 16); 
                        end_date = data.end_date.substr(0, 16);
                        next_pay_date = data.next_pay_date.substr(0, 16);
                        $('#plan_start_date').val(start_date);
                        $('#plan_end_date').val(end_date);
                        $('#next_pay_date').val(next_pay_date);
                    }
                    });
    }
    function subscribePlan($this){
        subscription = $this.data('subscription');
        plan_id = $this.data('plan_id');
        this.dateChanges(plan_id);
        document.getElementById("plan_id").value = $this.data('plan_id');
        switch(subscription){
            case 'subscribe':
                $("#subscription_msg_subscribe").show();
                document.getElementById("plan_name").textContent = $this.data('plan_name');
                break;
            case 'upgrade':
                $("#subscription_msg_downgrade").hide();
                $("#subscription_msg_upgrade").show();
                document.getElementById("current_plan_upgrade").textContent = $this.data('plan_name');
                break;
            case 'downgrade':
                $("#subscription_msg_upgrade").hide();
                $("#subscription_msg_downgrade").show();
                document.getElementById("current_plan_downgrade").textContent = $this.data('plan_name');
                break;
            case 'renew':
                $("#subscription_msg_renew").show();
                document.getElementById("renew_plan_name").textContent = $this.data('plan_name');
                break;
        }
        url = $this.data('url');
    }

    $(document).ready(function(){
        $('#adminSubscribeSubmit').click(function(e){
            var form = document.getElementById('adminSubscriptionForm');
            for(var i=0; i < form.elements.length; i++){
                if(form.elements[i].value === '' && form.elements[i].hasAttribute('required')){
                    alert('Please fill all required fields!');
                    return false;
                }
            }
            e.preventDefault();
            $('#loading-bg').show();
            $.ajax({
                url: url,
                method: 'post',
                data: $('form[name=adminSubscriptionForm]').serializeArray(),
            }).done(function (data) {
                $('#loading-bg').hide();
                window.location.reload();
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                $('#loading-bg').hide();
                console.log('No response from server');
            });
        });
    });
    $('#payment_method').change(function(){
        var value = $('#payment_method').val();
        if(value == 'admin_renewal'){
        $('#plan_amount').show(); 
        }
        else{
        $('#plan_amount').hide(); 
        }
    });
    $('#transaction_date').change(function(){
        var pay_date = $('#transaction_date').val();
        if(pay_date){
        // this.dateChanges(plan_id);
        var planUrl = "{{route('admin.plan.data')}}";
        $('#loading-bg').show();
        $.ajax({
                    url: planUrl,
                    type: "POST",
                    data: {
                        'plan_id': plan_id,
                        'pay_date' : pay_date
                    },
                    success: function(data) {
                        start_date = data.start_date.substr(0, 16); 
                        end_date = data.end_date.substr(0, 16);
                        next_pay_date = data.next_pay_date.substr(0, 16);
                        $('#plan_start_date').val(start_date);
                        $('#plan_end_date').val(end_date);
                        $('#next_pay_date').val(next_pay_date);
                        $('#loading-bg').hide();
                    }
                    });
                }
    });
    
    $('#package_type').change(function(){
        alert('hai');
        var planDetailUrl = "{{route('admin.plan.details')}}";
        var packageType = $('#package_type').val();
        $('#loading-bg').show();
        $.ajax({
                    url: planDetailUrl,
                    type: "POST",
                    data: {
                        'package_type': packageType
                    },
                    success: function(data) {
                        var planOption = '';
                        for(i=0; i< data.length; i++){
                            var optiondata = '<option value="'+data[i].id+'">'+data[i].name+'</option>';
                            planOption = planOption+optiondata;
                        }
                        console.log(planOption);
                        $('.plan_hide').hide();
                        $('#plan_option').html('<select class="form-control " name="package" id="package" ><option value="">Choose package </option>'+planOption+'</select>');
                        $('#loading-bg').hide();
                    }
                    });
    });
</script>
@endsection