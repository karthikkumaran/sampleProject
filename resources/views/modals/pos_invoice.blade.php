<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous"> -->
        <style>
            /* *{ font-family:Arial, Helvetica, sans-serif !important;} */
            /* * {
                font-family: DejaVu Sans !important;
            } */

            .table-borderless>tbody>tr>td,
            .table-borderless>tbody>tr>th,
            .table-borderless>tfoot>tr>td,
            .table-borderless>tfoot>tr>th,
            .table-borderless>thead>tr>td,
            .table-borderless>thead>tr>th {
                border: 1px;
                border-color: white;
            }
        </style>
    </head>
    <body>
        @php
            $meta_data = $data['meta_data'];
            $customer = $data['customerData'];
            $products = $customer->orderProducts;
            $cmeta_data = json_decode($customer->meta_data, true);
            if(empty($meta_data['settings']['currency']))
                $currency = "₹";
            else
                $currency = App\User::CURRENCY_SYMBOLS[$meta_data['settings']['currency']];
            $subtotal=0;
            $total_discounted_price = 0;
        @endphp
        <table class="table table-borderless">
            <!-- <tr>
                <td>
                    <p class=" font-weight-bold h5 text-capitalize text-dark p-0 m-0">{{$meta_data['storefront']['storename'] ?? ''}}</p>
                </td>
                {{-- <td class="">
                    &nbsp;&nbsp;
                </td> --}}
                <td>
                    <p class=" font-weight-bold h5 text-dark p-0 m-0 float-right"><b> Date:</b>&nbsp;{{date('d-m-Y h:i:s A', strtotime($customer->created_at))}}</p>
                </td>
            </tr> -->
            <tr>
            <td>
                <p class=" font-weight-bold h5 text-capitalize text-secondary p-0 m-0">{{$meta_data['storefront']['storename'] ?? ''}}</p>
                <div class=" p-0 m-0" style="font-size: 14px;">
                    {{$meta_data['settings']['emailid'] ?? ''}}<br>
                    {{isset($meta_data['settings']['contactnumber_country_code']) ? '+'.$meta_data['settings']['contactnumber_country_code'].'-'.$meta_data['settings']['contactnumber'] : '+91'.'-'.$meta_data['settings']['contactnumber']}}<br>
                    @if(isset($meta_data['checkout_settings']['tax_settings']) && $meta_data['checkout_settings']['tax_settings'] == '1')
                    <b> GST Number :</b>&nbsp;{{$meta_data['checkout_settings']['tax_setting_details']['gst_number']}}<br>
                    @endif
                </div>
            </td>

            <td >
                <p class=" font-weight-bold h5 text-secondary p-0 m-0">Order Details</p>
                <div class=" p-0 m-0" style="font-size: 14px;">
                    <b> Date:</b>&nbsp;{{date('d-m-Y h:i:s A', strtotime($customer->created_at))}}<br>
                    <b> Order #</b>{{$customer->order->order_ref_id}}<br>
                    @if(isset($customer->order->transactions->transaction_id))
                    <b> Transaction Id:</b>&nbsp;{{$customer->order->transactions->transaction_id}}
                    <br><b> Payment Method:</b>&nbsp;{{$customer->order->transactions->payment_mode}}
                    @endif
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <p class="font-weight-bold bg-secondary text-white p-1 m-0 w-100">Billed to</p>
                <div class=" p-0 m-0" style="font-size: 14px;">
                    <b> {{ $customer->fname ?? "" }}</b><br>
                    {{-- {{ $customer->address1 ?? "" }}, --}}
                    @if ($customer->address2)
                    {{ $customer->address2}}<br>
                    @else
                    <br>
                    @endif
                    {{-- {{ $customer->city ?? "" }} - {{ $customer->pincode ?? "" }},<br>
                    {{ $customer->state ?? "" }}, {{ $customer->country ?? "" }}.<br> --}}
                    {{ $customer->mobileno ?? "" }}<br>
                    {{ $customer->amobileno ?? "" }}
                </div>
            </td>
            {{-- <td>
            </td> --}}
            <td class="text-left text-bold-300 ">
                <p class="font-weight-bold bg-secondary text-white p-1 m-0 w-100">Ship to</p>
                <div class=" p-0 m-0" style="font-size: 14px;">
                     <b> {{ $customer->fname ?? "" }}</b><br>
                    {{-- {{ $customer->address1 ?? "" }},   --}}
                    @if ($customer->address2)
                    {{ $customer->address2}}<br>
                    @else
                    <br>
                    @endif
                    {{-- {{ $customer->city ?? "" }} - {{ $customer->pincode ?? "" }},<br>
                    {{ $customer->state ?? "" }}, {{ $customer->country ?? "" }}.<br> --}}
                    {{ $customer->mobileno ?? "" }}<br>
                    {{ $customer->amobileno ?? "" }}
                </div>
            </td>
        </tr>
        </table>
        <table class="table">
            <tr class="table bg-dark text-white">
                <th scope="col" class=" text-center">S.No</th>
                <th scope="col" class=" text-center">Description</th>
                <th scope="col" class=" text-center">Item Price</th>
                <th scope="col" class=" text-center">Discount</th>
                <th scope="col" class=" text-center">Qty</th>
                <th scope="col" class=" text-center">Amount</th>
            </tr>
            @foreach ($products as $oproduct)
                @php
                    // $img = $objimg = asset('XR/assets/images/placeholder.png');
                    // if(count($aproduct->photo) > 0){
                    // //$img = $aproduct->photo[0]->getFullUrl();
                    // //$img = $aproduct->photo[0]->getUrl();
                    // $img = $aproduct->photo[0]->getUrl('thumb');
                    // //$img = public_path($img); // only for local images
                    // }

                    if($oproduct['discount'] != null) {
                        $discount_price = $oproduct['price'] * ($oproduct['discount']/100);
                        $total_discounted_price += $discount_price * $oproduct->qty;
                        $discounted_price = $oproduct['price'] - $discount_price;
                        $product_price = $discounted_price * $oproduct->qty;
                    }
                    $actual_price=number_format($oproduct->price * $oproduct->qty,2);
                    $subtotal += $oproduct->price*$oproduct->qty;
                @endphp
                <tr class=" table-secondary p-0 m-0">
                    <td>{{ $loop->index + 1 }}</td>
                    <td scope="row"><b class=" text-capitalize">{{$oproduct->name}}</b><br>
                        <p class=" text-capitalize" style="font-size: 10px">
                            @if($oproduct->sku)
                                <b class=" text-uppercase">Sku:</b>{{" ".$oproduct->sku}}<br>
                            @endif
                            @if($oproduct->notes)
                                <b>Note:</b>{{" ".$oproduct->notes}}
                            @endif
                        </p>
                    </td>
                    <td class="text-right">{!!$oproduct->price ? $cmeta_data['currency'] ."".$oproduct->price :$currency.'00.00' !!}</td>
                    <td class="text-right">{{$oproduct->discount ? $oproduct->discount.'%' : '00.00%'}}</td>
                    <td class="text-right">{{$oproduct->qty}}</td>
                    <td class="text-right">
                        @if($oproduct['discount'] != null)
                            <p class=" m-0 p-0"> {!! $cmeta_data['currency']."".number_format($product_price,2) !!}</p>
                            <p class=" m-0 p-0"><s>{!! $cmeta_data['currency']."".$actual_price !!}</s></p>
                            @php
                                $productvalue = $product_price;
                            @endphp
                       @else
                        {!! $cmeta_data['currency']."".$actual_price !!}
                        @php
                            $productvalue = $actual_price;
                        @endphp
                    @endif
                    </td>
                </tr>
            @endforeach
            <tr class="">
                <td colspan="4" class="font-weight-bold bg-dark text-white mt-1">
                    Special notes and instructions
                </td>
                <td>Subtotal</td>
                <td class=" text-right">
                    {!! $cmeta_data['currency']."".number_format($subtotal,2) !!}
                </td>
            </tr>
            <tr>
                <td class="table-secondary" colspan="4">
                    {{$customer->order->notes ? $customer->order->notes :  "" }}
                </td>
                <td>Discount</td>
                <td class=" text-right">
                    -{!! $cmeta_data['currency']."".number_format( $total_discounted_price,2) !!}</s>
                </td>
            </tr>

        @if($cmeta_data['pos_discount'] != null)
            <tr>
                <td colspan="4">&nbsp;</td>
                <td>POS Discount</td>
                @php
                    if($cmeta_data['set_discount'] == 'PERCENTAGE')
                    {
                    // $posamount =str_replace(',', '', $productvalue);
                    $posamount=$subtotal-$total_discounted_price;
                    $posdiscount = ($cmeta_data['pos_discount'] * ((float) $posamount))/100;
                    }
                    else{
                    $posdiscount = $cmeta_data['pos_discount'] ;
                    }
                @endphp
                <td class=" text-right">
                    -{!! $cmeta_data['currency']."".number_format((float)$posdiscount,2)!!}</s>
                </td>
            </tr>
            @endif
            @php
                $img= '';
                if(isset($customer->customer_sign))
                {
                    $img = $customer->customer_sign;
                }
                @endphp
            <tr>
                @if(!empty($customer->getFirstMediaUrl('signature')))
                <td colspan="4" class=" text-left"><b>CustomerSign:</b><img src="{{$customer->getFirstMediaUrl('signature')}}" width="100"height="100"/></td>
                @else
                <td colspan="4">&nbsp;</td>
                @endif
                <td><b>Total</b></td>
                <td class=" text-right"><b>{!! $cmeta_data['currency']."".number_format($cmeta_data['product_total_price'],2)!!}</b></td>
            </tr>
        </table>
        <div class="justify-content-center text-center">
            <a class="btn btn-secondary btn-lg " target="_blank" href="{{ route('admin.print_pos_invoice_pdf',$order->id) }}">
                Print
            </a>
        </div>
    </body>
</html>
