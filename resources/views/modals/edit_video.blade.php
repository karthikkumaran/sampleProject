@section('styles')
@parent
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/plugins/file-uploaders/dropzone.css')}}">
<style>
.select2 {
    width: 100% !important;
}
.dropzone {
min-height: 150px !important;  
}
.dropzone .dz-message {
height: 150px !important;  
margin-top: -60px !important;
}
</style>
@endsection
<div class="modal fade p-0 ecommerce-application" id="videoEditorModal" tabindex="-1" role="dialog" aria-labelledby="videoEditorModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="videoEditorModalTitle">Edit Video</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                
            <div class="modal-body">
                <div class="row" id="videoEditor">
                
                </div>
                

            </div>  
        </div>
    </div>
</div>

@section('scripts')
@parent

<script src="{{asset('XR/app-assets/js/scripts/pages/app-ecommerce-shop.js')}}"></script>
<script>
    
    var previous_video_id = "";
    function getVideoDetail(id){
        if(id != previous_video_id) {
            $("#videoEditorModal").modal('show');
            $('#loading-bg').show();
            $("#videoEditor").empty()
            previous_video_id = id;            
            var url = "{{route('admin.videos.edit',':id')}}";
            url = url.replace(':id',id);
            $.ajax({
                url: url,
                type: "get", 
                datatype: "html"
            }).done(function (data) {
                $("#videoEditor").empty().html(data);
                $('#loading-bg').hide();
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                console.log("failed")
                $('#loading-bg').hide();
            });
        }
    }
    
    function removetryonimg(id){
        $('#loading-bg').hide();
        if(confirm('{{ trans('global.areYouSure') }}')) {
            var url = "{{route('admin.arobjects.destroy',':id')}}";
            url = url.replace(':id', id);
            $.ajax({
                url: url,
                type: "post",
                data: {'_method': 'DELETE'},
            }).done(function (data) {
                alert('Removed image');
                $('#loading-bg').hide();
                location.reload();
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                
                // alert('No response from server');
            });
        }
    }
    
    function removeimg(id){
        $('#loading-bg').show();
        if(confirm('{{ trans('global.areYouSure') }}')) {
            $('#loading-bg').show();
            var url = "{{route('admin.products.removeimg')}}";
            // url = url.replace(':id', id);
            $.ajax({
                url: url,
                type: "post",
                data: {'id': id},
            }).done(function (data) {
                $('#carousel-thumb').carousel("next");
                $('.carousel-thumb').carousel({
                    interval: 2000
                })
                $('#image_'+ id).remove();
                $('#product_'+ id).remove();
                $('#loading-bg').hide();
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                
                // alert('No response from server');
            });
        }
    }
    
    if(window.location.hash)
    {
        var hash_product_edit = window.location.hash.replace (/[^a-z\s]/gi, '');
        if(hash_product_edit == "updat")
        {
        var hash_product_id = window.location.hash.replace ( /[^\d.]/g, '' ); 
        $("#videoEditorModal").modal('show');   
        getVideoDetail(hash_product_id);
        var loc = window.location;
        history.pushState("", document.title, loc.pathname + loc.search);
        }
    }
    
</script>

@endsection