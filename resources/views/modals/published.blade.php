<style>
.share-icon i {
    font-size: 2rem !important;
}
</style>

<div class="modal fade " id="PublishedModal" tabindex="-1" role="dialog"
    aria-labelledby="PublishedModalTitle" aria-hidden="true">
       <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{$campaign->name}} - Published</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                @if(empty($userSettings->subdomain))
                <a href="{{route('campaigns.published',$campaign->public_link)}}" target="__blank">
                                    <img class="card-img-top img-fluid" src="{{route('admin.campaigns.previewQrCode',$campaign->public_link)}}" style="width:250px;">
                                </a> <br>
                                <!-- <span id="public_link_copy">{{route('campaigns.published',$campaign->public_link)}}</span> -->
                @php
                    $copyLink = route('mystore.published',$userSettings->public_link)
                @endphp
                @else
                @php
                    $link = $userSettings->subdomain.".".env('SITE_URL')."/".$campaign->public_link;
                    $copyLink = "https://".$link;
                @endphp
                <a href="https://{{$link}}" target="__blank">
                                    <img class="card-img-top img-fluid" src="{{route('admin.campaigns.previewQrCode',$campaign->public_link)}}" style="width:250px;">
                                </a> <br>
                                <!-- <span id="public_link_copy">https://{{$link}}</span> -->
                @endif
                <button class="btn btn-flat btn-icon" style="border:dotted;width: 100%;word-break: break-all;" onclick="copyToClipboard('#public_link_copy')">
                <span id="public_link_copy"> {{ $copyLink }} </span><br><br><i class="feather icon-copy"></i> Tap to copy</button>
                <hr>
                <button type="button" class="btn btn-icon rounded-circle mr-1 mb-1 text-success share-icon" onclick='share("whatsapp","")'><i class="fa fa-whatsapp"></i><br><br>WhatsApp</button>
                <button type="button" class="btn btn-icon rounded-circle mr-1 mb-1 text-primary share-icon" onclick='share("fb","")'><i class="feather icon-facebook"></i><br><br>Facebook</button>
                <button type="button" class="btn btn-icon rounded-circle mr-1 mb-1 text-info share-icon" onclick='share("twitter","")'><i class="feather icon-twitter"></i><br><br>Twitter</button>
                <button type="button" class="btn btn-icon rounded-circle mr-1 mb-1 text-danger share-icon" onclick='share("pdf","")'><i class="fa fa-file-pdf-o"></i><br><br>PDF</button>
                <button type="button" class="btn btn-icon rounded-circle mr-1 mb-1 text-black share-icon" onclick='share("download","")'><i class="feather icon-download"></i><br><br>Download</button>
               
                </div>
                <div class="modal-footer">
                @if(empty($userSettings->subdomain))
                <a href="{{route('campaigns.published',$campaign->public_link)}}" target="__blank" class="btn gradient-light-primary mt-2" >Open Link</a>
                @else
                <a href="https://{{$link}}" target="__blank" class="btn gradient-light-primary mt-2" >Open Link</a>
                @endif
                </div>
            </div>

        </div>
</div>

<script>
function copyToClipboard(element) {
  var $temp = $("<input>");
  $("#PublishedModal").append($temp);
  $temp.val($(element).text());
  $temp.select();
  document.execCommand("copy");
  $temp.remove();
  toastr.success('Share Link', 'Copied!');
}
</script>