@php 
$agent=new \Jenssegers\Agent\Agent();
@endphp
@if($agent->isMobile())
<div class="modal modal-fullscreen p-0 fade ecommerce-application" id="addProductToCatalogsModal" tabindex="-1" role="dialog" aria-labelledby="addProductToCatalogsModalTitle" aria-hidden="true">
@else
<div class="modal fade ecommerce-application" id="addProductToCatalogsModal" tabindex="-1" role="dialog" aria-labelledby="addProductToCatalogsModalTitle" aria-hidden="true">
@endif
    <form method="POST" action="#" id="addProductsToCatalogsForm" enctype="multipart/form-data" style="height: 90%;">
        @csrf
        <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addProductToCatalogsModalTitle">Select Products</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="loading-bg-modal" style="display:none;">
                        <div class="loading-logo">
                            <img src="{{asset('favicon.png')}}">
                        </div>
                        <div class="loading1">
                            <div class="spinner-{{ $meta_data['splash']['loader_type'] ?? 'border' }} w-100 h-100"
                                style="color: {{ $meta_data['splash']['loader_color'] ?? '' }}" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                    </div>
                    <section id="products_selection">
                        <!-- Ecommerce Content Section Starts -->
                        <section id="ecommerce-header">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="ecommerce-header-items">
                                        <div class="result-toggler">
                                            <button class="navbar-toggler shop-sidebar-toggler" type="button" data-toggle="collapse">
                                                <span class="navbar-toggler-icon d-block d-lg-none"><i class="feather icon-menu"></i></span>
                                            </button>
                                            <div class="search-results">
                                                <span id="productSelectModalTotal"></span> results found
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </section>
                        <!-- Ecommerce Content Section Starts -->
                    
                        <!-- Ecommerce Search Bar Starts -->
                        <section id="ecommerce-searchbar">
                            <div class="row mt-1">
                                <div class="col-sm-11">
                                        <fieldset class="form-group position-relative">
                                        <input type="text" class="form-control search-product" id="addProductsSearch" placeholder="Search here">
                                        <div class="form-control-position">
                                            <i class="feather icon-search"></i>
                                            </div>
                                    </fieldset>
                                </div>
                                <div class="col-sm-1" id="show_filter_btn" onclick="show_filter()" style="cursor:pointer;">
                                    <button class="navbar-toggler shop-sidebar-toggler w-100 h-75 d-flex justify-content-center" type="button" data-toggle="collapse" onclick="show_filter()">
                                        <span class="navbar-toggler-icon d-block d-lg-block d-md-block d-sm-block"><i class="feather icon-filter" style="font-size:21px;"></i></span>
                                    </button>
                                </div>  
                            </div>
                            <div class="row">
                                <button class="btn btn-warning ml-1 mr-1" type="button" id="select_all_products" onclick="selectAllProductsToCatalog()">
                                    <span>Select all</span>
                                </button>
                                <button class="btn btn-danger ml-1 mr-1" type="button" id="deselect_all_products" onclick="deselectAllProductsToCatalog()" style="display: none;">
                                    <span>Deselect all</span>
                                </button>
                            </div>
                        </section>
                        <!-- Ecommerce Search Bar Ends -->
                    
                        <section id="ecommerce-products-select" class="grid-view d-block mb-4">
                            <div class="row" id="select_products">
                            </div>
                        </section>
                    </section>
                    <section class="p-1" id="campaigns_selection" style="display:none">
                        <!-- Ecommerce Content Section Starts -->
                        <section id="ecommerce-header">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="ecommerce-header-items">
                                        <div class="search-results">
                                            <span onclick="show_products()" style="cursor: pointer;"><i class="feather icon-arrow-left"></i> Back</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section id="ecommerce-catalogs-select">
                            <div class="p-1 m-1" id="select_catalogs">
                                <div class="catalog-title">
                                    <h6 class="filter-title mb-1">Select the Catalogs</h6>
                                </div>
                                <div>
                                <ul class="list-unstyled categories-list">
                                @foreach($campaigns as $campaign)
                                    @if($campaign->type == 3)
                                        <li class="d-flex justify-content-between align-items-center py-25">
                                            <span class="vs-checkbox-con vs-checkbox-primary">
                                                <input type="checkbox" name="catalogs_selected[]" value="{{$campaign->id}}">
                                                <span class="vs-checkbox">
                                                    <span class="vs-checkbox--check">
                                                        <i class="vs-icon feather icon-check"></i>
                                                    </span>
                                                </span>
                                                <span class="">{!! $campaign->name == "Untitled"?"&nbsp;":$campaign->name !!}</span>
                                            </span>
                                        </li>
                                    @endif
                                @endforeach
                                </ul>
                            </div>
                        </section>
                    </section>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancel</button>
                    <button type="button" class="btn btn-success" id="next_btn" onclick="show_catalogs()" disabled="true">Next</button>
                    <button type="submit" class="btn btn-primary" id="add_to_catalogs_btn" style="display: none;" disabled="true">Add to Catalogs</button>
                </div>
            </div>
        </div>
    </form>
</div>

<!-- Select Products Filter Modal -->
<div class="modal modal-sm fade" id="productSelectFilterModal" tabindex="-1" role="dialog" aria-labelledby="productSelectFilterModalTitle" aria-hidden="true" style="margin-left:25%">
    <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="productSelectFilterModalTitle">Filters</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card" style="padding-bottom: 0;">
                    <div class="card-body" style="padding-bottom: 2vh;">
                        <!-- Price Filter -->
                        <div class="multi-range-price">
                            <div class="multi-range-title pb-75">
                                <h6 class="filter-title mb-0">Price Range</h6>
                            </div>
                            <ul class="list-unstyled price-range" id="price-range">
                                <li>
                                    <span class="vs-radio-con vs-radio-primary py-25">
                                        <input type="radio" name="price-range" checked value="all">
                                        <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                        </span>
                                        <span class="ml-50">All</span>
                                    </span>
                                </li>
                                <li>
                                    <span class="vs-radio-con vs-radio-primary py-25">
                                        <input type="radio" name="price-range" value="0,500">
                                        <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                        </span>
                                        <span class="ml-50">&lt; {!! $currency !!}500</span>
                                    </span>
                                </li>
                                <li>
                                    <span class="vs-radio-con vs-radio-primary py-25">
                                        <input type="radio" name="price-range" value="500,1000">
                                        <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                        </span>
                                        <span class="ml-50">{!! $currency !!}500 - {!! $currency !!}1000</span>
                                    </span>
                                </li>
                                <li>
                                    <span class="vs-radio-con vs-radio-primary py-25">
                                        <input type="radio" name="price-range" value="1000,10000">
                                        <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                        </span>
                                        <span class="ml-50">{!! $currency !!}1000 - {!! $currency !!}10000</span>
                                    </span>
                                </li>
                                <li>
                                    <span class="vs-radio-con vs-radio-primary py-25">
                                        <input type="radio" name="price-range" value="10000">
                                        <span class="vs-radio">
                                            <span class="vs-radio--border"></span>
                                            <span class="vs-radio--circle"></span>
                                        </span>
                                        <span class="ml-50">&gt; {!! $currency !!}10000</span>
                                    </span>
                                </li>
                            </ul>
                        </div>
                        <!-- /Price Filter -->
                        
                        @if(count($categories)>0)
                        <hr>
                        <!-- Categories Starts -->
                        <div id="product-categories">
                            <div class="product-category-title">
                                <h6 class="filter-title mb-1">Categories</h6>
                            </div>
                            <ul class="list-unstyled categories-list">
                                <li class="d-flex justify-content-between align-items-center py-25">
                                    <span class="vs-checkbox-con vs-checkbox-primary">
                                        <input type="checkbox" name="category-filter-all" checked value="all">
                                        <span class="vs-checkbox">
                                            <span class="vs-checkbox--check">
                                                <i class="vs-icon feather icon-check"></i>
                                            </span>
                                        </span>
                                        <span class="">All</span>
                                    </span>
                                </li>
                                @foreach($categories as $key => $category)
                                <li class="d-flex justify-content-between align-items-center py-25">
                                    <span class="vs-checkbox-con vs-checkbox-primary">
                                        <input type="checkbox" name="category-filter" value="{!! $category->name == " Untitled"?"&nbsp;":$category->name !!}">
                                        <span class="vs-checkbox">
                                            <span class="vs-checkbox--check">
                                                <i class="vs-icon feather icon-check"></i>
                                            </span>
                                        </span>
                                        <span class="">{!! $category->name == "Untitled"?"&nbsp;":$category->name !!}</span>
                                    </span>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- Categories Ends -->
                        <hr>
                        @endif
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="clear_filter()">Clear filters</button>
                <button type="button" id="uploadSubmit" class="btn btn-primary" onclick="apply_filter()">Apply filters</button>
            </div>
        </div>
    </div>
    </form>
</div>
<!-- End Select Products Filter Modal -->
@section('scripts')
@parent
<script src="{{asset('XR/app-assets/js/scripts/pages/app-ecommerce-shop.js')}}"></script>
<script>
    var q = "";
    var searchData = "";
    var sort = "";
    var page = 0;
    var filterData = "";
    var filter = [];
    var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    if(isMobile){
        $("#select_all_products").addClass("col-sm-12");
        $("#deselect_all_products").addClass("col-sm-12");
        document.getElementById("productSelectFilterModal").style.marginLeft = "0px";
    }
    function addProductsToCatalog(){
        $("#addProductToCatalogsModal").modal('show');
    }

    function show_catalogs(){
        $("#campaigns_selection").show();
        $("#products_selection").hide();
        $("#next_btn").hide();
        $("#add_to_catalogs_btn").show();
    }

    function show_products(){
        $("#products_selection").show();
        $("#campaigns_selection").hide();
        $("#next_btn").show();
        $("#add_to_catalogs_btn").hide();
    }

    $('#addProductToCatalogsModal').on('show.bs.modal', function() {
        show_products();
        getProductsData(0, false);
    })

    $('#addProductToCatalogsModal').on('hidden.bs.modal', function() {
        filter = [];
        $("[name=price-range]").filter("[value=all]").prop("checked", true);
        $("[name=category-filter-all]").filter("[value=all]").prop("checked", true);
        $("[name=category-filter]").prop("checked", false);
        $('input[name="catalogs_selected[]"]').prop("checked", false);
        $("#deselect_all_products").hide();
        $("#select_all_products").show();
    })

    $("#addProductsSearch").on('input', function() {
        searchData = $(this).val();
        if (searchData.length >= 3 || searchData == "")
            getProductsData(0);
    });

    $("#ecommerce-products-select").on('click', '.pagination a', function(event) {
        event.preventDefault();

        $('li').removeClass('active');
        $(this).parent('li').addClass('active');

        var myurl = $(this).attr('href');
        var page = $(this).attr('href').split('page=')[1];

        getProductsData(page);
    });

    function getProductsData(page, isReload = true) {
        $('#loading-bg-modal').show();
        q = 'q=' + searchData + '&s=' + sort + '&page=' + page + '&filterData=' + filterData + '&filter=' + JSON.stringify(filter);
        $.ajax({
            url: "{{route('admin.products.ajaxSelectProducts')}}" + '?' + q,
            type: "get",
            datatype: "html",
        }).done(function(data) {
            $("#select_products").empty().html(data);
            location.hash = page++;
            $('#loading-bg-modal').hide();
            if(isReload)
                window.location.reload();
        }).fail(function(jqXHR, ajaxOptions, thrownError) {
            $('#loading-bg-modal').hide();
            // alert('No response from server');
        });
    }

    function show_filter(){
        $("#productSelectFilterModal").modal('show');
    }

    function clear_filter() {
        filter = [];
        $("[name=price-range]").filter("[value=all]").prop("checked", true);
        $("[name=category-filter-all]").filter("[value=all]").prop("checked", true);
        $("[name=category-filter]").prop("checked", false);
        $("#productSelectFilterModal").modal('hide');
        getProductsData(0);
    }
    function apply_filter() {
        filter = [];
        var price_count = 0;
        var category_count = 0;
        var price_values_array = [];
        var category_values_array = [];
        
        var price = document.getElementsByName('price-range');
        for (i = 0; i < price.length; i++) {
            if (price[i].checked) {
                price_count++;
                filter.push({
                    type: 'price',
                    value: price[i].value
                });
            }
        }
        if (price_count == 0) {
            if ((price_slider_min.value != "") && (price_slider_max.value == "")) {
                filter.push({
                    type: 'price',
                    value: price_slider_min.value
                });
            } else if ((price_slider_min.value == "") && (price_slider_max.value != "")) {
                price_values_array.push(0);
                price_values_array.push(price_slider_max.value);
                var price_values = price_values_array.toString();
                filter.push({
                    type: 'price',
                    value: price_values
                });
            } else {
                price_values_array.push(price_slider_min.value);
                price_values_array.push(price_slider_max.value);
                var price_values = price_values_array.toString();
                filter.push({
                    type: 'price',
                    value: price_values
                });
            }
        }

        var catg = document.getElementsByName('category-filter');
        if(catg.length == 0){
            category_count++;
            filter.push({
                type: 'category',
                value: 'all'
            });
        }
        else{
            if ($('input[name="category-filter-all"]').is(":checked") == true) {
                category_count++;
                filter.push({
                    type: 'category',
                    value: $('input[name="category-filter-all"]').val()
                });
            } else {
                for (i = 0; i < catg.length; i++) {
                    if (catg[i].checked) {
                        category_count++;
                        category_values_array.push(catg[i].value);
                        var category_values = category_values_array.toString();
                    }
                }
                if (category_count > 0) {
                    filter.push({
                        type: 'category',
                        value: category_values
                    });
                }
            }
        }
        $("#productSelectFilterModal").modal('hide');
        getProductsData(0);
    }

    $('input[name="category-filter-all"]').on('change', function() {
        if ($('input[name="category-filter-all"]').is(":checked") == true) {
            $('input[name="category-filter"]').prop('checked', false);
        }
    });

    $('input[name="category-filter"]').on('change', function() {
        if ($('input[name="category-filter"]').is(":checked") == true) {
            $('input[name="category-filter-all"]').prop('checked', false);
        }
    });

    $('input[name="catalogs_selected[]"]').on('change', function() {
        if($('input[name="catalogs_selected[]"]').filter(':checked').length > 0){
            $("#add_to_catalogs_btn").attr('disabled', false);
        }
        else{
            $("#add_to_catalogs_btn").attr('disabled', true);
        }
    });

    $("#addProductsToCatalogsForm").submit(function(e) {
        e.preventDefault();
        $('#loading-bg-modal').show();
        var productsToBeAdded = $('#addProductsToCatalogsForm').serializeArray();
        if (localStorage.getItem('productsToBeAdded') !== null && localStorage.getItem('productsToBeAdded') !== "") {
            $.each(JSON.parse(localStorage.getItem('productsToBeAdded')), function(i, field) {
                if (productsToBeAdded.findIndex(x => x.value[0] == field[0]) == -1) {
                    productsToBeAdded.push({
                        'name': 'product_id[]',
                        'value': field[0]
                    });
                    productsToBeAdded.push({
                        'name': 'catagory_id[]',
                        'value': field[1]
                    });
                }
            });
        }

        if (productsToBeAdded.findIndex(x => x.name == 'product_id[]') > 0) {
            $.ajax({
                type: "POST",
                url: "{{ route('admin.campaigns.createEntityCardFromProducts') }}",
                data: productsToBeAdded,
                success: function(data) {
                    $('#loading-bg-modal').hide();
                    if (data.status) {
                        localStorage.setItem("productsToBeAdded", "");
                        Swal.fire({
                            type: "success",
                            title: 'Added!',
                            text: 'Products have been added to the catalogs.',
                            confirmButtonClass: 'btn btn-success',
                        }).then((result) => {
                            window.location.reload();
                        });
                    }
                }
            });
        } else {
            Swal.fire('Error!', "Please select one or more product", 'warning');
        }
    });
</script>
@endsection