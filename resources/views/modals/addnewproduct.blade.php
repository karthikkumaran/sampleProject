<!-- Add New Product Modal -->
@php 
$agent=new \Jenssegers\Agent\Agent();
@endphp
@if($agent->isMobile())
<div class="modal modal-fullscreen p-0 fade ecommerce-application" id="productAddNewModal" tabindex="-1" role="dialog" aria-labelledby="productNewModalTitle" aria-hidden="true">
@else
<div class="modal  fade ecommerce-application" id="productAddNewModal" tabindex="-1" role="dialog" aria-labelledby="productNewModalTitle" aria-hidden="true">
@endif
   <form method="POST" action="#" id="productAddNewForm" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="is_product" value="" id="is_product" />
        <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
            <input type="hidden" name="campaign_id" value="{{(!empty($campaign))?$campaign->id:'' }}" />
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="productNewModalTitle">Add New Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                @if(session('subscription_usage_remaining'))
                    <div class="row mb-2">
                        <div class="col-lg-12">
                            <div class="alert alert-danger" role="alert">
                               You can upload only {{ session('subscription_usage_remaining') }} more products.
                            </div>
                        </div>
                    </div>
                @endif
                    <div class="row mb-2">
                        <div class="col-lg-12 text-right">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancel</button>
                            <button type="submit" class="btn btn-primary uploadSubmit" disabled="true">Submit</button>
                        </div>
                    </div>
              
                <div class="dropbody">
                <div class="inner-scroll-height-500">
                    <div class="dropzone dropzone-area {{ $errors->has('photo') ? 'is-invalid' : '' }}" id="productUploadimg">
                        <div class="dz-message">Upload Product Image</div>
                    </div>
                </div>
                </div>
                    <div class="form-group ar-position">
                        <label for="position">Position</label>
                        <select class="form-control" name="position" id="position">
                            @foreach(App\Arobject::POSITION_SELECT as $key => $label)
                            <option value="{{ $key }}">{{ $label }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Cancel</button>
                    <button type="submit" class="btn btn-primary uploadSubmit" disabled="true">Submit</button>
                </div>
            </div>

        </div>
    </form>
</div>
<!-- end Add New Product Modal -->
@section('scripts')
@parent
<script>
    
    function addnewproduct() {
        $('#productSelectModal').modal('hide');
        $('#productAddNewModal').modal('show');
        $('#is_product').val("NONAR");

        $("#productNewModalTitle").html("Add New Product Image");
        $(".dz-message").html("Upload Product Image");
        $(".ar-position").remove();

    }
    var uploadnewproduct_type = "";
     var uploadedPhotoMap = {};
    Dropzone.autoDiscover = false;
    myProductDropzone = new Dropzone('#productUploadimg', {
        autoProcessQueue: false,
        parallelUploads: "{{ session('subscription_usage_remaining') ?? 50 }}",
        url: "{{route('admin.products.storeMedia')}}",
        maxFilesize: 40, // MB
        maxFiles: "{{ session('subscription_usage_remaining') ?? 50 }}",
        acceptedFiles: '.jpeg,.jpg,.png,.gif',
        addRemoveLinks: true,
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        params: {
            size: 40
        },
        success: function(file, response) {
            console.log("uploaded",response)
            $('form').append('<input type="hidden" name="photo[]" value="' + response.name + '">')
        },
        removedfile: function(file) {
            file.previewElement.remove()
            var name = ''
            if (typeof file.file_name !== 'undefined') {
                name = file.file_name
            } else {
                name = uploadedPhotoMap[file.name]
            }
            $('form').find('input[name="photo[]"][value="' + name + '"]').remove()
        },
        init: function() {
            console.log("product queue");
            // var submitButton = document.querySelector(".uploadSubmit");
            // myDropzone = this; // closure
            // submitButton.addEventListener("click", function() {
            //     if (myDropzone.getUploadingFiles().length === 0 && myDropzone.getQueuedFiles().length === 0) {} else {
            //         myDropzone.processQueue();
            //     }
            // });
            this.on("addedfile", file => {
                console.log("upload")
                if(this.getQueuedFiles().length >= 0)
                    $(".uploadSubmit").prop("disabled",false)
                else
                    $(".uploadSubmit").prop("disabled",true)
            });
            this.on("removedfile", file => {
                console.log("upload removed")
                if(this.getQueuedFiles().length >= 0)
                    $(".uploadSubmit").prop("disabled",false)
                else
                    $(".uploadSubmit").prop("disabled",true)
            });
            this.on("dragend, processingmultiple", function(file) {
                // $("#uploadSubmit").prop('disabled', true);
            });
            this.on("queuecomplete", function(file) {
                $('#loading-bg').show();
                $('#productAddNewModal').modal('hide');
                uploadnewproduct(uploadnewproduct_type);
            })

        },
        error: function(file, response) {
            $('#loading-bg').hide();
            if ($.type(response) === 'string') {
                var message = response //dropzone sends it's own error messages in string
                imagelog(message);
            } else {
                var message = response.errors.file
                imagelog(message);
            }
            file.previewElement.classList.add('dz-error')
            _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
            _results = []
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
                node = _ref[_i]
                _results.push(node.textContent = message)
            }

            return _results
        }

    });

    $("#productAddNewForm").submit(function(e) {
        e.preventDefault();
        $('#loading-bg').show();
        $(".uploadSubmit").prop('disabled', true);
        var inputOptions = new Promise(function(resolve) {
                resolve({
                    'single': '<div><div class="text-center"><img src="{{asset('XR/app-assets/images/icons/single_product.png')}}" alt="users avatar" class="users-avatar-shadow rounded" height="90" width="90"></div><br><div class="text-center" style="width: 140px;">Single Product</div></div>',
                    'multiple': '<div><div class="text-center"><img src="{{asset('XR/app-assets/images/icons/multiple_products.png ')}}" alt="users avatar" class="users-avatar-shadow rounded" height="90" width="90"></div><br><div class="text-center" style="width: 140px;">Multiple Products</div></div>'
                });
            });
        if(myProductDropzone.getQueuedFiles().length  >= 2 ) {
            swal.fire({
                    title: 'Upload image as',
                    type: 'warning',
                    input: 'radio',
                    inputOptions: inputOptions,
                    inputValidator: (value) => {
                        return !value && 'You need to select something!'
                    },
                    reverseButtons: true,
                    showCancelButton: true,
                    confirmButtonText: "Submit",
                    cancelButtonText: "Cancel",
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn btn-light',
                }).then(function(result) {
                    console.log(result);
                    if (result.value) {
                        myProductDropzone.processQueue();
                        uploadnewproduct_type = result.value
                    }
                })
        } else if(myProductDropzone.getQueuedFiles().length  == 1) {
            myProductDropzone.processQueue();
            uploadnewproduct_type = "single";
        }
        
    });

    function uploadnewproduct(type) {
        var photos = $("#productAddNewForm").find('input[name="photo[]"]');
        // $("#uploadSubmit").prop('disabled', true);
        if ($('#is_product').val() == "NONAR") {
            $.ajax({
                type: "POST",
                url: "{{route('admin.products.store')}}",
                data: $('#productAddNewForm').serialize() + '&producttype=' + type, // serializes the form's elements.
                success: function(data) {
                    // $('#productAddNewModal').modal('hide');
                    //     $('#productSelectModal').modal('show');
                    $('#loading-bg').hide();
                    window.location.href = "{{route('admin.products.product_list')}}"
                    //    alert(data); // show response from the php script.
                },error: function (jqXHR, exception) {
                    var msg = '';
                    if (jqXHR.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }
                }
            });
        } else if ($('#is_product').val() == "AR") {
            $.ajax({
                type: "POST",
                url: "{{route('admin.products.store')}}",
                data: $('#productAddNewForm').serialize() + '&producttype=multiple', // serializes the form's elements.
                success: function(data) {
                    window.location.href = "{{route('admin.products.product_list')}}"
                }
            });
        }
    }

    function imagelog(msg){
        $.ajax({
                type: "POST",
                url: "{{route('admin.products.log')}}",
                data: '&msg=' + msg,
                success: function(data) {

                }

    });
    }
   

</script>
<style>
  

</style>

@endsection