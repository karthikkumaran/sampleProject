<div class="modal fade " id="CatalogSettingsModal" tabindex="-1" role="dialog"
    aria-labelledby="CatalogSettingsModalTitle" aria-hidden="true">
    <form method="POST" action="#" id="CatalogSettingForm" enctype="multipart/form-data">
        @method('POST')
        @csrf
       <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Catalog Setting</h5>
                    <input type="hidden" id="catalog_setting_campaign_id" name="pk"/>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="subscription_exhausted_msg_catalogs" style="display:none">
                    <h6 class="alert alert-danger alert-validation-msg" role="alert">
                        <p><i class="feather icon-info mr-1 align-middle"></i>You have exhausted your plan. Please update your plan to proceed.</p>
                        <p class="text-center"><a href="{{route('admin.subscriptions.index')}}" class="btn btn-info btn-xs">Upgrade plan</a></p></h6>
                    </div>
                    <div class="row mt-1">
                        <div class="col-12">
                            <div class="custom-control custom-switch custom-switch-success">
                                <span class="switch-label"><i class="fa fa-rocket mr-sm-1" aria-hidden="true" style="font-size:17px;"></i><b>Catalog Published</b></span>
                                <input type="checkbox" class="custom-control-input" id="is_start" name="is_start">
                                <label class="custom-control-label float-right" for="is_start">
                                    <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                    <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                </label>
                                <h5 class="alert alert-danger mt-1 alert-validation-msg w-100" style="display:none;" id="publish_alert" role="alert">
                                    <i class="feather icon-info mr-1 align-middle"></i>
                                    <span>Your catalog will be removed from the store if you unpublish it.</span>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="col-12">
                            <div class="custom-control custom-switch custom-switch-success">
                                <span class="switch-label"><i class="fa fa-lock mr-sm-1" aria-hidden="true" style="font-size:17px;"></i>  <b>Make Private Catalog</b></span>
                                <input type="checkbox" class="custom-control-input" id="is_private" name="is_private">
                                <label class="custom-control-label float-right" for="is_private">
                                    <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                    <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                </label>
                                    <h5 class="alert alert-danger mt-1 alert-validation-msg w-100" style="display:none;" id="private_alert" role="alert">
                                        <i class="feather icon-info mr-1 align-middle"></i>
                                        <span>Cannot make a catalog private when it is added to the store.Your catalog will be removed from the store if you make it private.</span>
                                    </h5>
                            </div>
                            <div class="form-group m-1" id="private_password_txt">
                                <label for="private_password">Password</label>
                                <input class="form-control" type="password" name="private_password" id="private_password" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="col-12">
                            <div class="custom-control custom-switch custom-switch-success">
                                <span class="switch-label"><i class="fa fa-clock-o mr-sm-1" aria-hidden="true" style="font-size:17px;"></i><b>Set Expired Date</b></span>
                                <input type="checkbox" class="custom-control-input" id="is_expired" name="is_expired" >
                                <label class="custom-control-label float-right" for="is_expired">
                                    <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                    <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                </label>
                            </div>
                            <div class="form-group m-1" id="expired_date_txt">
                                <label for="expired_date">Date</label>
                                <input class="form-control" type="datetime-local" name="expired_date" id="expired_date" >
                            </div>
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="col-12">
                            <div class="custom-control custom-switch custom-switch-success">
                                <span class="switch-label"><img  class="mr-sm-1" src="{{asset('XR/app-assets/images/icons/store.png')}}" style="height:16px;width:16px;"><b>Add to Store</b></span>
                                <input type="checkbox" class="custom-control-input" id="is_added_home" name="is_added_home">
                                <label class="custom-control-label float-right" for="is_added_home">
                                    <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                    <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                </label>
                                <h5 class="alert alert-danger mt-1 alert-validation-msg w-100" style="display:none;" id="added_alert" role="alert">
                                    <i class="feather icon-info mr-1 align-middle"></i>
                                    <span>You can add the catalog to the store only if you publish it.</span>
                                </h5>
                            </div>
                    </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"
                        aria-label="Close">Cancel</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>

        </div>
        </form>
</div>
@section('scripts')
@parent
<script>
    var catalog_settings_id,catalogs_publishable,catalog_downgradeable;
    function catalog_settings($this){
        //  var catalogSettingAction = "{{ route("admin.campaigns.update", ['id']) }}";
        //  catalogSettingAction = catalogSettingAction.replace("id",$(this).data('campaign_id'));
        catalog_settings_id = $this.data('id');
        catalogs_publishable = $this.data('catalogs_publishable');
        catalog_downgradeable = $this.data('catalog_downgradeable');
        $("#catalog_setting_campaign_id").val($this.data('id'));
        $("#is_start").prop( "checked", $this.data('is_start'));
        $("#is_private").prop( "checked", $this.data('is_private'));
        $('#is_expired').prop( "checked", $this.data('is_expired'));
        $('#expired_date').val($this.data('expired_date'));
        $("#is_added_home").prop( "checked", $this.data('is_added_home'));
        // $("#CatalogSettingForm").attr("action",catalogSettingAction);
        $("#CatalogSettingsModal").modal('show');

        if($('#is_start').is(':checked') == false){
            $('#is_added_home').prop('disabled',true);
            $("#added_alert").show();
        }else{
            $('#is_added_home').prop('disabled',false);
        }

        if($('#is_private').is(':checked') == false){
            $("#private_password_txt").prop("hidden",true);
        }
        else{
            $("#private_alert").hide();
            $('#is_added_home').prop('checked',false);
            $('#is_added_home').prop('disabled',true);
            $("#private_password_txt").prop("hidden",false);
        }

        if($('#is_expired').is(':checked') == false){
            $("#expired_date_txt").prop("hidden",true);
        }
    
        if($('#is_expired').is(':checked') == true){
            $("#expired_date_txt").prop("hidden",false);
        }

        if((catalogs_publishable == "false") && (catalog_downgradeable == 1)){
            $("#subscription_exhausted_msg_catalogs").show();
            $('#is_start').prop('disabled',true);
            $('#is_private').prop('disabled',true);
            $('#is_expired').prop('disabled',true);
            $('#is_added_home').prop('disabled',true);
            $("#added_alert").hide();
        }
        else{
            $("#subscription_exhausted_msg_catalogs").hide();
            $('#is_start').prop('disabled',false);
            $('#is_private').prop('disabled',false);
            $('#is_expired').prop('disabled',false);
            $('#is_added_home').prop('disabled',false);
        }
    }

    $("#modal_close").on('click',function(){
        $(".alert-validation-msg").hide();
    });

    $(".close").on('click',function(){
        $(".alert-validation-msg").hide();
    });

    $("#is_start").change(function(){
        var x = $("#is_added_home").is(":checked");
        if(this.checked){
            $('#catalog_share_' + catalog_settings_id).prop("disabled",false);
            $('#catalog_settings_' + catalog_settings_id ).data('is_start',1);
            $('#catalogSettingsBtn_' + catalog_settings_id ).data('is_start',1);
            if($("#is_private").is(":checked") == false)
            {
                $('#is_added_home').prop('disabled',false);
            }
            $("#added_alert").hide();
        }
        else{
            if(x==true){
                $('#is_added_home').prop('checked',false);
                $('#catalog_settings_' + catalog_settings_id ).data('is_added_home',0);
                $('#catalogSettingsBtn_' + catalog_settings_id ).data('is_added_home',0);
            }
            $('#catalog_share_' + catalog_settings_id).prop("disabled",true);
            $('#catalog_settings_' + catalog_settings_id ).data('is_start',0);
            $('#catalogSettingsBtn_' + catalog_settings_id ).data('is_start',0);
            $('#is_added_home').prop('disabled',true);
            $("#added_alert").show();
        }
    });

    $('#is_private').change(function() {
        var x = $("#is_added_home").is(":checked");
        if(this.checked){
            $('#catalog_settings_' + catalog_settings_id ).data('is_private',1);
            $('#catalogSettingsBtn_' + catalog_settings_id ).data('is_private',1);
            if(x==true)
            {
                $("#private_alert").show();
                $('#is_added_home').prop('checked',false);
            }
            $('#is_added_home').prop('disabled',true);
            $("#private_password_txt").attr("hidden",false);
            // $("#private_password").attr("required",true);
        } 
        else{ 
            $('#catalog_settings_' + catalog_settings_id ).data('is_private',0);
            $('#catalogSettingsBtn_' + catalog_settings_id ).data('is_private',0);
            $("#private_alert").hide();
            if($("#is_start").is(":checked") == true)
            {
                $('#is_added_home').prop('disabled',false);
            }
            $("#private_password_txt").prop("hidden",true);
            // $("#private_password").attr("required",false);
        }
    });

    $('#is_expired').change(function() {
        if(this.checked) {
            $("#expired_date_txt").prop("hidden",false);
            $('#catalog_settings_' + catalog_settings_id ).data('is_expired',1);
            $('#catalogSettingsBtn_' + catalog_settings_id ).data('is_expired',1);
            
        } else {         
            $("#expired_date_txt").prop("hidden",true);
            $('#catalog_settings_' + catalog_settings_id ).data('is_expired',0);
            $('#catalogSettingsBtn_' + catalog_settings_id ).data('is_expired',0);
        }
    });

    $('#is_added_home').change(function(){
        if(this.checked) {
            $('#catalog_settings_' + catalog_settings_id ).data('is_added_home',1);
            $('#catalogSettingsBtn_' + catalog_settings_id ).data('is_added_home',1);
        } 
        else{            
            $('#catalog_settings_' + catalog_settings_id ).data('is_added_home',0);
            $('#catalogSettingsBtn_' + catalog_settings_id ).data('is_added_home',0);
        }
    });

    $("#CatalogSettingForm").submit(function (e) {
        e.preventDefault();
                
        var catalogSettingAction = "{{ route("admin.campaigns.updateCatalogSettings", ['id']) }}";
        catalogSettingAction = catalogSettingAction.replace("id",$("#catalog_setting_campaign_id").val());
        var is_private = $('#is_private').is(":checked")==true?1:0;
        var is_start = $('#is_start').is(":checked")==true?1:0;
        var is_added_home = $("#is_added_home").is(":checked")==true?1:0;
        var downgradeable = 0;
        if(is_start == 1)
        {
            $('#catalog_share_' + catalog_settings_id).prop("hidden",false);
            $('#catalog_noshare_' + catalog_settings_id).prop("hidden",true);
            $('.catalog_share_card').prop("hidden",false);
            $('.catalog_noshare_card').prop("hidden",true);
            $('#share_public_icon_' + catalog_settings_id).prop("hidden",false);
            $('#publish_catalog_icon_' + catalog_settings_id).prop("hidden",true);
            downgradeable = 0;
        }
        else
        {
            $('#catalog_share_' + catalog_settings_id).prop("hidden",true);
            $('#catalog_noshare_' + catalog_settings_id).prop("hidden",false);
            $('.catalog_share_card').prop("hidden",true);
            $('.catalog_noshare_card').prop("hidden",false);
            $('#share_public_icon_' + catalog_settings_id).prop("hidden",true);
            $('#publish_catalog_icon_' + catalog_settings_id).prop("hidden",false);
            downgradeable = 1;
        }
        var is_expired = $('#is_expired').is(":checked")==true?1:0;
        $('#lock_icon_' + catalog_settings_id).prop("hidden",!is_private);
        $('#clock_icon_' + catalog_settings_id).prop("hidden",!is_expired);
        var data = new Array();
        data.push({'name': 'id','value': $("#catalog_setting_campaign_id").val()});
        data.push({'name': 'is_start','value': is_start});
        data.push({'name': 'is_private','value': is_private});
        data.push({'name': 'downgradeable','value': downgradeable});
        if(is_private == 1){
            data.push({'name': 'private_password','value': $("#private_password").val()});
        }
        if(is_expired == 1){
            data.push({'name': 'expired_date','value': $("#expired_date").val()});
        }
        data.push({'name': 'is_added_home','value': is_added_home});
        $('#loading-bg').show();
        $.ajax({
                    type: "POST",
                    url: catalogSettingAction,
                    data: data,
                    success: function (data) {
                        if (typeof published_catalogs === 'undefined' || typeof allowed_catalogs === 'undefined' || typeof catalogs_publishable === 'undefined') {
                            // variable is undefined
                        }
                        else if(allowed_catalogs == true){

                        }
                        else{
                            if(is_start == 1){
                                published_catalogs++;
                            }
                            else{
                                published_catalogs--;
                            }
                        
                            if(published_catalogs >= allowed_catalogs){
                                window.location.reload();
                            }
                            else{
                                catalogs_publishable = "true";
                            }
                            set.data('catalogs_publishable',catalogs_publishable);
                        }
                        
                        $("#CatalogSettingsModal").modal('hide');
                        $('#loading-bg').hide();
                    }
                });
    });
</script>
@endsection