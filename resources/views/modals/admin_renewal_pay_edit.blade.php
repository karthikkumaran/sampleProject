<div class="modal fade" id="adminRenewalEditPay" tabindex="-1" role="dialog"
    aria-labelledby="adminSubscriptionTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit plan payment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <form method="POST" action="{{ route('admin.adminSubscription.updateSubscription') }}" name="adminSubscriptionEditForm" id="adminSubscriptionEditForm" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="edit_user_id" id="edit_user_id" value="">
                    <input type="hidden" name="edit_plan_id" id="edit_plan_id" value="">
                    <input type="hidden" name="edit_plan_start" id="edit_plan_start" value="">
                    <input type="hidden" name="edit_plan_end" id="edit_plan_end" value="">
                    <!-- <div class="row m-0">
                    <div id="subscription_msg_subscribe" style="display: none">
                        <span>Subscribe to</span> <b><span id="plan_name"></span> <span>plan.</span></b>
                    </div>
                    <div id="subscription_msg_renew" style="display: none">
                        <span>Renew your current subscription:</span> <b><span id="renew_plan_name"></span> <span>plan.</span></b>
                    </div>
                    <div id="subscription_msg_upgrade" style="display: none">
                        Upgrade to <b><span id="current_plan_upgrade"></span> <span>plan.</span></b>
                    </div>
                    <div id="subscription_msg_downgrade" style="display: none">
                    <p>Are you sure you want to downgrade?</p>
                        <p><span>Important!</span></p><p><span> You are currently downgrading from</span> <b><span id="current_plan_downgrade"></span> Plan.</b> Please note that all data and configuration associated with the features available in your current Plan will not be accessable.</p>
                        <p>To see the full list of plans and features, please visit Plans and Pricing page. Still not sure? or Let us help you decide, contact our support for further help.</p>
                        <div class="col-12">
                            <div class="form-group mb-0 ml-2">
                                <input class="form-check-input" type="checkbox" id="immediate_downgrade" name="immediate_downgrade" value="1">
                                <label class="form-check-label" id="immediate_downgrade">Check to downgrade immediately</label>
                            </div>
                        </div>
                    </div>
                    </div>
                    <br> -->
                    <div class="row m-0">
                        <div class="col-12">
                            <h5>Payment Details</h5>
                            <hr>
                            <h6><span class="text-danger">*</span> Required fields</h6>
                        </div>
                        <!-- <div class="col-6">
                            <div class="form-group">
                                <label for="package_type">Package Type<span class="text-danger">*</span></label><br>
                                <select class="form-control select2" name="package_type" id="package_type" required>
                                    <option value="">Choose package type</option>
                                    <option value="day">Day</option>
                                    <option value="month">Month</option>
                                    <option value="year">Year</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="package">Package <span class="text-danger">*</span></label><br>
                                <select class="form-control plan_hide" name="package_select" id="package_select" >
                                    <option value="">Choose package </option>
                                    
                                </select>
                                <span id="plan_option"></span>
                            </div>
                        </div>
                        <div class="col-6">
                            Cash Mode
                            <div class="form-group">
                                <div class="row" >
                                    <div class="col-6">
                                        <input type="radio" id="cash_mode" name="cash_mode" onclick="cashMode('Free')" value="Free" required>
                                        <label for="Free">Free</label><br>
                                    </div>
                                    <div class="col-6">
                                        <input type="radio" id="cash_mode" name="cash_mode" onclick="cashMode('Pay')" value="Pay">
                                        <label for="pay">Pay</label><br>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        <div class="col-12" id="payment_method_display" >
                            <div class="form-group">
                                <label for="payment_edit_method">Payment method<span class="text-danger">*</span></label><br>
                                <select class="form-control select2" name="payment_edit_method" id="payment_edit_method" placeholder="Choose payment method"  required>
                                    <option value="">Choose payment method</option>
                                    <option value="account_transfer">Account transfer</option>
                                    <option value="cheque">Cheque</option>
                                    <!-- <option value="admin_renewal">Admin renewal</option> -->
                                    <option value="cash">cash</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12" id="plan_amount" >
                            <div class="form-group" >
                                <label for="transaction_edit_amount">Transaction Amount<span class="text-danger">*</span></label>
                                <input class="form-control " type="number" name="transaction_edit_amount" id="transaction_edit_amount" value="0" required>
                            </div>
                        </div>
                        
                        <div class="col-12" id="transaction_id_display" >
                            <div class="form-group">
                                <label for="transaction_edit_id">Transaction ID<span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="transaction_edit_id" id="transaction_edit_id" value="" required>
                            </div>
                        </div>
                        @php
                        $current_time =Carbon\Carbon::now()->format('Y-m-d\TH:i');
                        @endphp
                        <!-- <div class="col-12">
                            <div class="form-group">
                                <label for="transaction_date">Transaction date<span class="text-danger">*</span></label>
                                <input class="form-control" type="datetime-local" name="transaction_date" id="transaction_date" value="{{$current_time}}" required>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="plan_start_date">Plan start date<span class="text-danger"></span></label>
                                <input class="form-control" type="datetime-local" name="plan_start_date" id="plan_start_date" value=""  >
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="plan_end_date">Plan expires on<span class="text-danger"></span></label>
                                <input class="form-control" type="datetime-local" name="plan_end_date" id="plan_end_date" value="" >
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="next_pay_date">Next pay date<span class="text-danger"></span></label>
                                <input class="form-control" type="datetime-local" name="next_pay_date" id="next_pay_date" value="" disabled>
                            </div>
                        </div> -->
                    </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="modal_close" data-dismiss="modal"
                        aria-label="Close">Cancel</button>
                    <button type="button" class="btn btn-primary" id="adminEditSubscribeSubmit">Update Subscribe</button>
                </div>
            </div>
        </div>
    
</div>
@section('scripts')
@parent
<script>
    var edit_url;
    var subscription;
    var user_id;
    var plan_id;
    var start_date = '';
    var end_date = '';
    var paymentData = '';

    function userData(id){
        user_id = id;
        $('#edit_user_id').val(user_id);
    }

    function planData(plan)
    {
       console.log(plan);
       $('#edit_plan_id').val(plan['plan_id']);
       $('#loading-bg').show();
        $.ajax({
                    url: "{{route('admin.subscriptionPayment.data')}}",
                    type: "POST",
                    data: {
                        'plan': plan,
                        'user_id' : user_id
                    },
                    success: function(data) {
                     paymentData  = data;
                     console.log(paymentData);      
                     console.log(paymentData['payment_method']);   
                     if(paymentData['subscribed_by'] == 1){
                     $('#payment_edit_method').val(paymentData['payment_method']).change();            
                     $('#transaction_edit_amount').val(paymentData['plan_price']);            
                     $('#transaction_edit_id').val(paymentData['payment_id']);  
                     $('#edit_plan_start').val(paymentData['plan_start']);  
                     $('#edit_plan_end').val(paymentData['plan_expiry']);  
                     $('#loading-bg').hide();                                  
                     } 
                     else{
                     $("#adminRenewalEditPay").modal('hide');
                     $('#loading-bg').hide();                                  
                    Swal.fire({
                    type: "warning",
                    title: 'User subscribed!',
                    text: 'Cannot Renewed by Admin',
                    confirmButtonClass: 'btn btn-warning',
                    }).then((result) => {
                        window.location.reload();
                    });              
                     }  
                    
                    }
      });             
        
    }

//     function getval(sel)
//     {
//         plan_id = sel.value;
//         $('#plan_id').val(plan_id);
//         this.dateChanges(plan_id);
//     }

//     function dateChanges(id){
//         var planUrl = "{{route('admin.plan.data')}}";
//         $.ajax({
//                     url: planUrl,
//                     type: "POST",
//                     data: {
//                         'plan_id': id
//                     },
//                     success: function(data) {
//                         start_date = data.start_date.substr(0, 16); 
//                         end_date = data.end_date.substr(0, 16);
//                         next_pay_date = data.next_pay_date.substr(0, 16);
//                         $('#plan_start_date').val(start_date);
//                         $('#plan_end_date').val(end_date);
//                         $('#next_pay_date').val(next_pay_date);
//                     }
//                     });
//     }
    

    $(document).ready(function(){
        edit_url="{{route('admin.adminSubscription.updateSubscription')}}";
        $('#adminEditSubscribeSubmit').click(function(e){
         console.log(edit_url);
            console.log($('form[name=adminSubscriptionEditForm]').serializeArray());
            var form = document.getElementById('adminSubscriptionEditForm');
            for(var i=0; i < form.elements.length; i++){
                if(form.elements[i].value === '' && form.elements[i].hasAttribute('required')){
                    alert('Please fill all required fields!');
                    return false;
                }
            }
            e.preventDefault();
            $('#loading-bg').show();
            $.ajax({
                url: edit_url,
                method: 'post',
                data: $('form[name=adminSubscriptionEditForm]').serializeArray(),
            }).done(function (data) {
                $('#loading-bg').hide();
                // console.log(data);
                if(data == 'current'){
                    // Swal.fire('Your current plan is not expired', 'you are already in subscription', 'error');
                    Swal.fire({
                    type: "warning",
                    title: 'Subscribed!',
                    text: 'Your are already subscribed this plan',
                    confirmButtonClass: 'btn btn-success',
                }).then((result) => {
                    window.location.reload();
                });
                }
                else{
                    Swal.fire({
                    type: "success",
                    title: data,
                    text: 'Payment updated successfully',
                    confirmButtonClass: 'btn btn-success',
                    }).then((result) => {
                        window.location.reload();
                    });
                    toastr.success('Payment updated successfully ');
                }
                // window.location.reload();
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                $('#loading-bg').hide();
                console.log('No response from server');
            });
        });
    });
    // $('#payment_method').change(function(){
    //     var value = $('#payment_method').val();
    //     if(value == 'admin_renewal'){
    //     $('#plan_amount').show(); 
    //     }
    //     else{
    //     $('#plan_amount').hide(); 
    //     }
    // });
//     function cashMode(cash_pay){
//         if(cash_pay == 'Pay'){
//             $('#payment_method_display').show();
//             $('#payment_method').prop('required',true);
//             $('#plan_amount').show();
//             $('#transaction_amount').prop('required',true);
//             $('#transaction_id_display').show();
//             $('#transaction_id').prop('required',true);
//         }
//         else{
//             $('#payment_method_display').hide();
//             $('#payment_method').prop('required',false);
//             $('#plan_amount').hide();
//             $('#transaction_amount').prop('required',false);
//             $('#transaction_id_display').hide(); 
//             $('#transaction_id').prop('required',false);
//         }
//     }
//     $('#transaction_date').change(function(){
//         var pay_date = $('#transaction_date').val();
//         if(plan_id){
//         var planUrl = "{{route('admin.plan.data')}}";
//         $('#loading-bg').show();
//         $.ajax({
//                     url: planUrl,
//                     type: "POST",
//                     data: {
//                         'plan_id': plan_id,
//                         'pay_date' : pay_date
//                     },
//                     success: function(data) {
//                         start_date = data.start_date.substr(0, 16); 
//                         end_date = data.end_date.substr(0, 16);
//                         next_pay_date = data.next_pay_date.substr(0, 16);
//                         $('#plan_start_date').val(start_date);
//                         $('#plan_end_date').val(end_date);
//                         $('#next_pay_date').val(next_pay_date);
//                         $('#loading-bg').hide();
//                     }
//                     });
//                 }
//     });
    
//     $('#package_type').change(function(){
//         var planDetailUrl = "{{route('admin.plan.details')}}";
//         var packageType = $('#package_type').val();
//         $('#loading-bg').show();
//         $.ajax({
//                     url: planDetailUrl,
//                     type: "POST",
//                     data: {
//                         'package_type': packageType
//                     },
//                     success: function(data) {
//                         var planOption = '';
//                         for(i=0; i< data.length; i++){
//                             var optiondata = '<option value="'+data[i].id+'">'+data[i].name+'</option>';
//                             planOption = planOption+optiondata;
//                         }
//                         console.log(planOption);
//                         $('.plan_hide').hide();
//                         $('#plan_option').html('<select class="form-control " name="package" id="package" onchange="getval(this);" required><option value="">Choose package </option>'+planOption+'</select>');
//                         $('#loading-bg').hide();
//                     }
//                     });
//     });

//         $('#plan_end_date').change(function(){
//             var plan_end_date = $('#plan_end_date').val();
//             $('#next_pay_date').val(plan_end_date);
//         });
    
</script>
@endsection