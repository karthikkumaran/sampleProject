<div class="modal fade" id="userSubscription" tabindex="-1" role="dialog"
    aria-labelledby="userSubscriptionTitle" aria-hidden="true">
    <form method="POST" action="#" id="userSubscriptionForm" enctype="multipart/form-data">
        @csrf
        {{ csrf_field() }}
        <!-- <input type="hidden" name="_method" value="PUT"> -->
       <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Subscribe to plan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="subscription_msg_subscribe" style="display: none">
                        <span>Subscribe to</span> <b><span id="plan_name"></span> <span>plan.</span></b>
                    </div>
                    <div id="subscription_msg_upgrade" style="display: none">
                        Upgrade to <b><span id="current_plan_upgrade"></span> <span>plan.</span></b>
                    </div>
                    <div id="subscription_msg_downgrade" style="display: none">
                        <p>Are you sure you want to downgrade?</p>
                        <p><span>Important!</span></p><p><span> You are currently downgrading from</span> <b><span id="current_plan_downgrade"></span> Plan.</b> Please note that all data and configuration associated with the features available in your current Plan will not be accessable.</p>
                        <p>To see the full list of plans and features, please visit Plans and Pricing page. Still not sure? or Let us help you decide, contact our support for further help.</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="modal_close" data-dismiss="modal"
                        aria-label="Close">Cancel</button>
                    <button type="button" class="btn btn-primary" onclick="showSubscription()">Subscribe</button>
                </div>
            </div>
        </div>
    </form>
</div>

<script>
    var url;
    var subscription;
    function subscribePlan($this){
        subscription = $this.data('subscription');
        switch(subscription){
            case 'subscribe':
                $("#subscription_msg_subscribe").show();
                document.getElementById("plan_name").textContent = $this.data('plan_name');
                break;
            case 'upgrade':
                $("#subscription_msg_downgrade").hide();
                $("#subscription_msg_upgrade").show();
                document.getElementById("current_plan_upgrade").textContent = $this.data('plan_name');
                break;
            case 'downgrade':
                $("#subscription_msg_upgrade").hide();
                $("#subscription_msg_downgrade").show();
                document.getElementById("current_plan_downgrade").textContent = $this.data('plan_name');
                break;
        }
        url = $this.data('url');
    }
    function showSubscription(){
        window.location.href = url;
    }
</script>
