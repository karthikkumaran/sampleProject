@section('styles')
@parent
<link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/plugins/file-uploaders/dropzone.css')}}">
<style>
.select2 {
    width: 100% !important;
}
.dropzone .dz-message {
height: auto !important;  
}
</style>
@endsection
<div class="modal modal-fullscreen fade p-0 ecommerce-application" id="productEditorModal" tabindex="-1" role="dialog" aria-labelledby="productEditorModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="productEditorModalTitle">Edit Product</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
                
            <div class="modal-body">
                <div class="row" id="productEditor">
                
                </div>
            </div>  
        </div>
    </div>
</div>

@section('scripts')
@parent

<script src="{{asset('XR/app-assets/js/scripts/pages/app-ecommerce-shop.js')}}"></script>
<script>
    function getProductDetail(id){
        $('#loading-bg').show();
        $("#videoEditorModal").modal('hide');
        var url = "{{route('admin.products.edit',':id')}}";
        url = url.replace(':id',id);
        $.ajax({
            url: url,
            type: "get",
            datatype: "html"
        }).done(function (data) {
            $("#productEditor").empty().html(data);
            // $("#product_list").append(data);
            $('#loading-bg').hide();
        }).fail(function (jqXHR, ajaxOptions, thrownError) {
            $('#loading-bg').hide();
            // alert('No response from server');
        });
    }

    $('.arobjectPosition').editable({
        source: [
            @foreach(App\Arobject::POSITION_SELECT as $key => $label)
            {
            value: '{{$key}}',
            text: '{{$label}}'
            },
            @endforeach
        ]
    });
    
    function removetryonimg(id){
        $('#loading-bg').hide();
        if(confirm('{{ trans('global.areYouSure') }}')) {
            var url = "{{route('admin.arobjects.destroy',':id')}}";
            url = url.replace(':id', id);
            $.ajax({
                url: url,
                type: "post",
                data: {'_method': 'DELETE'},
            }).done(function (data) {
                alert('Removed image');
                $('#loading-bg').hide();
                location.reload();
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                
                // alert('No response from server');
            });
        }
    }
    
    function removeimg(id){
        $('#loading-bg').show();
        if(confirm('{{ trans('global.areYouSure') }}')) {
            $('#loading-bg').show();
            var url = "{{route('admin.products.removeimg')}}";
            // url = url.replace(':id', id);
            $.ajax({
                url: url,
                type: "post",
                data: {'id': id},
            }).done(function (data) {
                $('#carousel-thumb').carousel("next");
                $('.carousel-thumb').carousel({
                    interval: 2000
                })
                $('#image_'+ id).remove();
                $('#product_'+ id).remove();
                $('#loading-bg').hide();
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                
                // alert('No response from server');
            });
        }
    }

    function removeDoc(id){
        $('#loading-bg').show();
        if(confirm('{{ trans('global.areYouSure') }}')) {
            $('#loading-bg').show();
            var url = "{{route('admin.products.removeimg')}}";
            // url = url.replace(':id', id);
            $.ajax({
                url: url,
                type: "post",
                data: {'id': id},
            }).done(function (data) {
                location.reload();
            }).fail(function (jqXHR, ajaxOptions, thrownError) {
                
                // alert('No response from server');
            });
        }
    }
    
    if(window.location.hash)
    {
        var hash_product_edit = window.location.hash.replace (/[^a-z\s]/gi, '');
        if(hash_product_edit == "updat")
        {
        var hash_product_id = window.location.hash.replace ( /[^\d.]/g, '' ); 
        $("#productEditorModal").modal('show');   
        getProductDetail(hash_product_id);
        var loc = window.location;
        history.pushState("", document.title, loc.pathname + loc.search);
        }
    }
</script>

@endsection