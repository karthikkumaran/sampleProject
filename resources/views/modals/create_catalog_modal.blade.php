@section('styles')
@parent
    
@endsection
<div class="modal fade" id="createNewCatalog" tabindex="-1" role="dialog"
    aria-labelledby="createNewCatalogTitle" aria-hidden="true">
    <form method="POST" action="#" onsubmit="return false" id="createNewCatalogForm" enctype="multipart/form-data">
        @csrf
        {{ csrf_field() }}
        <!-- <input type="hidden" name="_method" value="PUT"> -->
       <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create Catalog</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="subscription_alert_msg_catalogs">
                    </div>
                    <div class="subscription_content_catalogs">
                    <div class="row mt-1">
                        <div class="col-lg-12 col-sm-12">
                            <label>Catalog Name:</label>
                            <input class="form-control" type="text" name="catalog_name" id="catalog_name" required>
                        </div>
                    </div>
                    <hr class="hr-text" data-content="Catalog Settings" />
                    <div class="row mt-1">
                        <div class="col-12">
                            <div class="custom-control custom-switch custom-switch-success">
                                <span class="switch-label"><i class="fa fa-rocket mr-sm-1" aria-hidden="true" style="font-size:17px;"></i><b>Catalog Published</b></span>
                                <input type="checkbox" class="custom-control-input" id="is_start_2" name="is_start_2" checked>
                                <label class="custom-control-label float-right" for="is_start_2">
                                    <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                    <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                </label>
                                <h5 class="alert alert-danger mt-1 alert-validation-msg w-100" style="display:none;" id="publish_alert_2" role="alert">
                                    <i class="feather icon-info mr-1 align-middle"></i>
                                    <span>Your catalog will be removed from the store if you unpublish it.</span>
                                </h5>
                            </div>
                        </div> 
                    </div>
                    <div class="row mt-1">
                        <div class="col-12">
                            <div class="custom-control custom-switch custom-switch-success">
                                <span class="switch-label"><i class="fa fa-lock mr-sm-1" aria-hidden="true" style="font-size:17px;"></i>   <b>Make Private Catalog</b></span>
                                <input type="checkbox" class="custom-control-input" id="is_private_2" name="is_private_2">
                                <label class="custom-control-label float-right" for="is_private_2">
                                    <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                    <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                </label>
                                    <h5 class="alert alert-danger mt-1 alert-validation-msg w-100" style="display:none;" id="private_alert_2" role="alert">
                                        <i class="feather icon-info mr-1 align-middle"></i>
                                        <span>Cannot make a catalog private when it is added to the store.Your catalog will be removed from the store if you make it private.</span>
                                    </h5>
                            </div>
                            <div class="form-group m-1" id="private_password_txt_2">
                                <label for="private_password_2">Password</label>
                                <input class="form-control" type="password" name="private_password_2" id="private_password_2" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="col-12">
                            <div class="custom-control custom-switch custom-switch-success">
                                <span class="switch-label"><i class="fa fa-clock-o mr-sm-1" aria-hidden="true" style="font-size:17px;"></i><b>Set Expired Date</b></span>
                                <input type="checkbox" class="custom-control-input" id="is_expired_2" name="is_expired_2">
                                <label class="custom-control-label float-right" for="is_expired_2">
                                    <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                    <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                </label>
                            </div>
                            <div class="form-group m-1" id="expired_date_txt_2">
                                <label for="expired_date_2">Date</label>
                                <input class="form-control" type="datetime-local" name="expired_date_2" id="expired_date_2">
                            </div>
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="col-12">
                            <div class="custom-control custom-switch custom-switch-success">
                                <span class="switch-label"><img  class="mr-sm-1" src="{{asset('XR/app-assets/images/icons/store.png')}}" style="height:16px;width:16px;"><b>Add to Store</b></span>
                                <input type="checkbox" class="custom-control-input" id="is_added_home_2" name="is_added_home_2" checked>
                                <label class="custom-control-label float-right" for="is_added_home_2">
                                    <span class="switch-text-left"><i class="feather icon-check"></i></span>
                                    <span class="switch-text-right"><i class="feather icon-x"></i></span>
                                </label>
                                <h5 class="alert alert-danger mt-1 alert-validation-msg w-100" style="display:none;" id="added_alert_2" role="alert">
                                    <i class="feather icon-info mr-1 align-middle"></i>
                                    <span >You can add the catalog to the store only if you publish it.</span>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <hr class="hr-text mt-0" data-content="Choose Catalog Type" />
                    <div class="grid-wrapper grid-col-2 row">
                        <div class="selection-wrapper col">
                            <label for="selected-item-1" class="selected-label">
                                <input type="radio" checked name="catalog_type" value="3" id="selected-item-1">
                                <span class="icon"></span>
                                <div class="selected-content">
                                    <img src="https://image.freepik.com/free-vector/group-friends-giving-high-five_23-2148363170.jpg" alt="">
                                    <h4>Product Catalog</h4>
                                </div>
                            </label>
                        </div>
                        @if(session('subscribed_features'))
                        @if(array_key_exists("Video catalog", session('subscribed_features')[0]))
                        <div class="selection-wrapper col">
                            <label for="selected-item-2" class="selected-label">
                                <input type="radio" name="catalog_type" value="2" id="selected-item-2">
                                <span class="icon"></span>
                                <div class="selected-content">
                                    <img src="https://image.freepik.com/free-vector/people-putting-puzzle-pieces-together_52683-28610.jpg" alt="">
                                    <h4>Video Catalog</h4>
                                </div>
                            </label>
                        </div>
                        @endif
                        @endif
                    </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="modal_close" data-dismiss="modal"
                        aria-label="Close">Cancel</button>
                    <button type="submit" class="btn btn-primary ">Save</button>
                </div>
            </div>
        </div>
    </form>
</div>


@section('scripts')
@parent
<script>

    $('#is_start_2').change(function() {
        if(this.checked) {
            $('#is_added_home_2').prop('disabled',false);
            $("#added_alert_2").hide();
        }
        else{
            if($("#is_added_home_2").is(":checked")==true){
                $('#is_added_home_2').prop('checked',false);
                $('#is_added_home_2').prop('disabled',true);
                $("#publish_alert_2").show();
            }  
            $('#is_added_home_2').prop('disabled',true);
            $("#added_alert_2").show();
        }    
    });

    $('#is_private_2').change(function() {
        if(this.checked) {
            if($("#is_added_home_2").is(":checked")==true)
            {
                $('#is_added_home_2').prop('checked',false);
                $("#private_alert_2").show();
            }
            $('#is_added_home_2').prop('disabled',true);
            $("#private_password_txt_2").attr("hidden",false);
            $("#private_password_2").prop("required",true);
        } else {         
            $("#private_alert_2").hide();
            if($("#is_start_2").is(":checked")==true)
            {
                $('#is_added_home_2').prop('disabled',false);
            }
            $("#private_password_txt_2").prop("hidden",true);
            $("#private_password_2").prop("required",false);
        }
    });
    
    $("#private_password_txt_2").attr("hidden",true);
    $("#expired_date_txt_2").attr("hidden",true);

    $('#is_expired_2').change(function() {
        if(this.checked) {
            $("#expired_date_txt_2").attr("hidden",false);
            $("#expired_date_2").prop("required",true);
        } else {            
            $("#expired_date_txt_2").attr("hidden",true);
            $("#expired_date_2").prop("required",false);
        }
    });

</script>
@endsection
