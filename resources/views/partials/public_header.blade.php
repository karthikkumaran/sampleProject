<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="navbar-collapse" id="navbar-mobile">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                        <ul class="nav navbar-nav">
                            <!-- <li class="nav-item mobile-menu d-xl-none mr-auto">
                        <a class="nav-link nav-menu-main menu-toggle hidden-xs" href="#"><i class="ficon feather icon-menu"></i></a> 
                    </li> -->
                    <li class="nav-item mr-auto">
                    @if(!empty($meta_data['splash']))
                    @if($meta_data['splash']['brand_type'] == 2 && $meta_data['splash']['brand_name'] != "")
                        <a class="nav-link" href="{{$homeLink}}"><i class="ficon"></i><h1 style="color: {{ $meta_data['splash']['brand_color'] ?? '' }}">{{$meta_data['splash']['brand_name']}}</h1></a>
                    @else
                        <a class="nav-link" href="{{$homeLink}}"><i class="ficon"></i><img class=" lazyload" loading="lazy" data-src="{{ $userSettings->getFirstMediaUrl('splashlogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                            onerror="this.onerror=null;this.src='{{asset('XR/app-assets/images/logo/logo.png')}}';" alt="Logo" style="height: 25px;"></a>
                    @endif
                    @else
                        <a class="nav-link" href="{{$homeLink}}"><i class="ficon"></i><img class=" lazyload" loading="lazy" data-src="{{ $userSettings->getFirstMediaUrl('splashlogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                            onerror="this.onerror=null;this.src='{{asset('XR/app-assets/images/logo/logo.png')}}';" alt="Logo" style="height: 25px;"></a>
                    @endif
                    </li> 
                        </ul>
                    </div>
                    <ul class="nav navbar-nav float-right">
                        <li class="nav-item d-none d-lg-block"><a class="nav-link nav-link-expand"><i class="ficon feather icon-maximize"></i></a></li>
                        <!-- <li class="nav-item nav-search"><a class="nav-link nav-link-search"><i
                                    class="ficon feather icon-search"></i></a>
                            <div class="search-input">
                                <div class="search-input-icon"><i class="feather icon-search primary"></i></div>
                                <input class="input" type="text" placeholder="Search Product" tabindex="-1"
                                    data-search="template-list">
                                <div class="search-input-close"><i class="feather icon-x"></i></div>
                                <ul class="search-list search-list-main"></ul>
                            </div>
                        </li> -->
                        @if(!empty($checkoutLink))
                        <li class="dropdown dropdown-notification nav-item pr-2">
                            <a class="nav-link nav-link-label" href="{{$checkoutLink}}">
                                <i class="ficon feather icon-shopping-cart"></i>
                                <span class="badge badge-pill badge-primary badge-up cart-item-count"></span></a>
                        </li>
                        @endif
                        @if(!empty($meta_data['settings']['whatsapp']))
                        <li class="nav-item"><a class="nav-link btn btn-white list-view-btn view-btn p-1" href="https://api.whatsapp.com/send?phone={{isset($meta_data['settings']['whatsapp_country_code']) ? $meta_data['settings']['whatsapp_country_code'].$meta_data['settings']['whatsapp'] : '91'.$meta_data['settings']['whatsapp']}}"><i
                                    class="ficon fa fa-whatsapp bg-success text-white" style="padding: 8px !important;border-radius: 8px;"> Chat</i></a>
                        @endif
                           </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>