<script>
        function track_events(event_name, product_id, slink) {
            $.ajax({
                url: "{{route('admin.stats.ajaxEvents')}}",
                type: "post",
                data: {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    "event": event_name,
                    "slink": slink,
                    "product_id": product_id,
                },
            });     
            if (product_id == null){
                    product_id="";
                }
            if (typeof(umami) != 'undefined')
            {
                umami(event_name+product_id); 
            }
                   
        }
</script>