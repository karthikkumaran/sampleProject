@can($viewGate??'')
    <!-- <a class="btn btn-xs btn-primary" href="{{ route('admin.' . $crudRoutePart . '.show', $row->id) }}">
        {{ trans('global.view') }}
    </a> -->
    <a class="btn btn-xs btn-primary btn-icon" href="{{ route('admin.' . $crudRoutePart . '.show', $row->id) }}">
    <i class="feather icon-eye"></i>
    </a>
@endcan
@can($editGate)
    <!-- <a class="btn btn-xs btn-info" href="{{ route('admin.' . $crudRoutePart . '.edit', $row->id) }}">
        {{ trans('global.edit') }}
    </a> -->
    <a class="btn btn-xs btn-info btn-icon" href="{{ route('admin.' . $crudRoutePart . '.edit', $row->id) }}">
    <i class="feather icon-edit"></i>
    </a>
@endcan
@can($deleteGate)
    <form action="{{ route('admin.' . $crudRoutePart . '.destroy', $row->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <!-- <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}"> -->
        <button type="submit" class="btn btn-xs btn-danger btn-icon"><i class="feather icon-trash"></i></button>
    </form>
@endcan