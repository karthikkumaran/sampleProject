@section ('styles')
@parent
<style>
        .text-blue{
            color: #3AADD8;
        }
        .text-red {
            color: #DB1F26;
        }
    </style>
@endsection
    <footer class="footer footer-static footer-light">
        <p class="clearfix blue-grey lighten-2 mb-0">
            <span class="float-md-left d-block d-md-inline-block mt-25">COPYRIGHT &copy; {{ date('Y') }}
            <a class="text-bold-800 grey darken-2" href="{{$homeLink}}">
                {{ $meta_data['storefront']['storename'] ?? "Store" }},</a>
                All rights Reserved. 
                Powered by <a class="text-bold-800 grey darken-2" href="{{env('APP_URL')}}" target="_blank"><span class="text-blue">Simpli</span><span class="text-red">Sell</span></a>
        </span>
        <span class="float-md-right d-md-block">
            <a href="#" onclick='share("fb","")'><i class="fa fa-facebook fa-fw"></i></a>
            <a href="#" onclick='share("whatsapp","")'><i class="fa fa-whatsapp fa-fw"></i></a>
            <a href="#" onclick='share("twitter","")'><i class="fa fa-twitter fa-fw"></i></a>
        </span>
            <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="feather icon-arrow-up"></i></button>
        </p>    
    </footer>