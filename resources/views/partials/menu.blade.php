<style>
    .main-menu .main-menu-content {
    height: calc(88% - 6rem) !important;
    position: relative;
}

/* .navigation-main{
    font-size:1rem;
} */



</style>
<div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
        @if(!(auth()->user()->getIsAdminAttribute()))
        <li class="{{ request()->is('admin') ? 'active' : '' }}"><a href="{{ route("admin.home") }}"><i class="feather icon-home"></i>
                <span class="menu-item">{{ trans('global.dashboard') }}</span></a>
        </li>
        @endif
        
        @can('store_user_management')
        
        
    <li class=" nav-item"><a href="#"><i class="feather icon-users"></i><span class="menu-title">{{ trans('cruds.storeUserManagement.title') }}</span></a>
        <ul class="menu-content">
            @can('permission_access')
            <li class="{{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                <a href="{{ route("admin.permissions.index") }}"><i class="feather icon-lock"></i><span class="menu-item"> {{ trans('cruds.permission.title') }}</span></a>
            </li>
            @endcan
            @can('role_access')
            <li class="{{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                <a href="{{ route("admin.roles.index") }}">
                    <i class="feather icon-briefcase"></i>
                    <span class="menu-item"> {{ trans('cruds.storeRole.title') }}</span>
                </a>
            </li>
            @endcan
            @can('user_access')
            <li class="{{ request()->is('admin/storeUser') || request()->is('admin/storeUser/*') ? 'active' : '' }}">
                <a href="{{ route("admin.storeUser.index") }}">
                    <i class="feather icon-user"></i>
                    <span class="menu-item">{{ trans('cruds.storeUser.title') }}</span>
                </a>
            </li>
            @endcan
            
        </ul>
    </li>
    
 @endcan
        @can('user_management_access')
        
        @if(!(auth()->user()->getIsAdminAttribute()))
        <li class="{{ request()->is('admin') ? 'active' : '' }}"><a href="{{ route("admin.home") }}"><i class="feather icon-home"></i>
                <span class="menu-item">{{ trans('global.dashboard') }}</span></a>
        </li>
        @endif
        <li class=" nav-item"><a href="#"><i class="feather icon-users"></i><span class="menu-title">{{ trans('cruds.userManagement.title') }}</span></a>
            <ul class="menu-content">
                @can('permission_access')
                <li class="{{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.permissions.index") }}"><i class="feather icon-lock"></i><span class="menu-item"> {{ trans('cruds.permission.title') }}</span></a>
                </li>
                @endcan
                @can('role_access')
                <li class="{{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.roles.index") }}">
                        <i class="feather icon-briefcase"></i>
                        <span class="menu-item"> {{ trans('cruds.role.title') }}</span>
                    </a>
                </li>
                @endcan
                @can('user_access')
                <li class="{{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.users.index") }}">
                        <i class="feather icon-user"></i>
                        <span class="menu-item">{{ trans('cruds.user.title') }}</span>
                    </a>
                </li>
                @endcan
                @can('audit_log_access')
                <li class="{{ request()->is('admin/audit-logs') || request()->is('admin/audit-logs/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.audit-logs.index") }}">
                        <i class="feather icon-file-text"></i>
                        <span class="menu-item">{{ trans('cruds.auditLog.title') }}</span>
                    </a>
                </li>
                @endcan
                @can('team_access')
                <li class="{{ request()->is('admin/teams') || request()->is('admin/teams/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.teams.index") }}">
                        <i class="feather icon-users"></i>
                        <span class="menu-item">{{ trans('cruds.team.title') }}</span>
                    </a>
                </li>
                @endcan
            </ul>
        </li>
        
        @can('store_user_management')
        @if(!(auth()->user()->getIsAdminAttribute()))
        <li class="{{ request()->is('admin') ? 'active' : '' }}"><a href="{{ route("admin.home") }}"><i class="feather icon-home"></i>
                <span class="menu-item">{{ trans('global.dashboard') }}</span></a>
        </li>
        @endif
        <li class=" nav-item"><a href="#"><i class="feather icon-users"></i><span class="menu-title">{{ trans('cruds.userManagement.title') }}</span></a>
            <ul class="menu-content">
                @can('permission_access')
                <li class="{{ request()->is('admin/permissions') || request()->is('admin/permissions/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.permissions.index") }}"><i class="feather icon-lock"></i><span class="menu-item"> {{ trans('cruds.permission.title') }}</span></a>
                </li>
                @endcan
                @can('role_access')
                <li class="{{ request()->is('admin/roles') || request()->is('admin/roles/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.roles.index") }}">
                        <i class="feather icon-briefcase"></i>
                        <span class="menu-item"> {{ trans('cruds.role.title') }}</span>
                    </a>
                </li>
                @endcan
                @can('user_access')
                <li class="{{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.users.index") }}">
                        <i class="feather icon-user"></i>
                        <span class="menu-item">{{ trans('cruds.user.title') }}</span>
                    </a>
                </li>
                @endcan
                
            </ul>
        </li>
        
     @endcan

        
        
        @if(!(auth()->user()->getIsAdminAttribute()))
        @can('product_management_access')
        <li class=" nav-item"><a href="#"><i class="feather icon-shopping-cart"></i><span class="menu-title">{{ trans('cruds.productManagement.title') }}</span></a>
            <ul class="menu-content">
                @can('product_category_access')
                <li class="{{ request()->is('admin/product-categories') || request()->is('admin/product-categories/*') ? 'active' : '' }}">
                    <a href="{{ route("admin.product-categories.index") }}">
                        <i class="feather icon-folder"></i>
                        <span class="menu-item">{{ trans('cruds.productCategory.title') }}</span>
                    </a>
                </li>
                @endcan
                @can('product_tag_access')
                <!-- <li class="{{ request()->is('admin/product-tags') || request()->is('admin/product-tags/*') ? 'active' : '' }}">
                                <a href="{{ route("admin.product-tags.index") }}" >
                                    <i class="feather icon-tag"></i>
                                    <span class="menu-item">{{ trans('cruds.productTag.title') }}</span>
                                </a>
                            </li> -->
                @endcan
                {{-- @can('product_attribute_access')
                <li class="{{ request()->is('admin/product-attribute') || request()->is('admin/product-attribute/*') ? 'active' : '' }}">
                <a href="{{ route("admin.product-attributes.index") }}">
                    <i class="feather icon-folder"></i>
                    <span class="menu-item">Attributes</span>
                </a>
        </li>
        @endcan --}}
        @can('product_access')
        <li class="nav-item">
            <a href="{{ route("admin.products.index") }}" class="nav-link {{ request()->is('admin/products') || request()->is('admin/products/*') ? 'active' : '' }}">
                <i class="feather icon-shopping-cart">

                </i>
                <span class="menu-item">{{ trans('cruds.product.title') }}</span>
            </a>
        </li>
        @endcan
        @can('arobject_access')
        <li class="{{ request()->is('admin/arobjects') || request()->is('admin/arobjects/*') ? 'active' : '' }}">
            <a href="{{ route("admin.arobjects.index") }}">
                <i class="feather icon-box">

                </i>
                <span class="menu-item"> {{ trans('cruds.arobject.title') }}</span>
            </a>
        </li>
        @endcan

    </ul>
    </li>
    @endif
    @endcan
    @endcan
    @if(!(auth()->user()->getIsAdminAttribute()))
    @can('store_access')
    <li class="{{ request()->is('admin/mystore') || request()->is('admin/mystore/*') ? 'active' : '' }}">
        <a href="{{ route("admin.campaigns.storeIndex") }}">

            @if(request()->is('admin/mystore') )
            <img src="{{ asset('XR\app-assets\images\icons\store_white.png') }}" style="height: 21px;width: 21px;">
            @else
            <img src="{{ asset('XR\app-assets\images\icons\store.png') }}" style="height: 21px;width: 21px;">
            @endif

            <span class="menu-item">&nbsp;&nbsp;Store</span>
        </a>
    </li>
    @endcan
    <li class=" nav-item has-sub open"><a href="#"><i class="feather icon-shopping-cart"></i><span class="menu-title">{{ trans('cruds.productManagement.title') }}</span></a>
    <ul class="menu-content">
    
    @can('campaign_access')
    
    @php
    $isVideo_menu = false;
    if(!empty($isvideo) && $isvideo == true){
        $isVideo_menu = true;
    }    
    
    @endphp
    
    <li class="{{ ( (request()->is('admin/campaigns') || request()->is('admin/campaigns/*')) && $isVideo_menu == false) ? 'active' : '' }}">
        <a href="{{ route("admin.campaigns.index") }}">
            <i class="fa fa-list nav-icon"></i>
            <span class="menu-item">Product Catalogs</span>
        </a>
    </li>
    @if(session('subscribed_features'))
        @if(array_key_exists("Video catalog", session('subscribed_features')[0]))
    <li class="{{ (request()->is('admin/videocampaigns') || request()->is('admin/videocampaigns/*') || $isVideo_menu == true) ? 'active' : '' }}">
        <a href="{{ route("admin.campaigns.videoindex") }}">
            @if((request()->is('admin/videocampaigns') || request()->is('admin/videocampaigns/*') || $isVideo_menu == true))
                <i><img src="{{asset('XR\app-assets\images\icons\video-shopping-white.png')}}" class="nav-icon" style="width:21px;"/></i>
            @else
            <i><img src="{{asset('XR\app-assets\images\icons\video-shopping.png')}}" class="nav-icon" style="width:21px;"/></i>
            @endif
            <span class="menu-item">Video Catalogs</span>
        </a>
    </li>
        @endif
    @endif
    @endcan

    @can('product_management_access')
    @can('product_access')
    <li class="{{ request()->is('admin/myproducts') || request()->is('admin/myproducts/*') ? 'active' : '' }}">
        <a href="{{ route("admin.products.product_list") }}">
            <i class="feather icon-box">

            </i>
            <span class="menu-item">My Products</span>
        </a>
    </li>
    @endcan
    @can('product_category_access')
    <li class="{{ request()->is('admin/product-categories') || request()->is('admin/product-categories/*') ? 'active' : '' }}">
        <a href="{{ route("admin.product-categories.index") }}">
            <i class="feather icon-folder"></i>
            <span class="menu-item">{{ trans('cruds.productCategory.title') }}</span>
        </a>
    </li>
    @endcan

    @can('product_attribute_access')
    <li class="{{ request()->is('admin/product-attributes') || request()->is('admin/product-attribute/*') ? 'active' : '' }}">
        <a href="{{ route("admin.product-attributes.index") }}">
            @if(request()->is('admin/product-attributes') )
            <img src="{{ asset('XR\app-assets\images\icons\attributeWhite.png') }}" style="width: 21px;">
            @else
            <img src="{{ asset('XR\app-assets\images\icons\attribute.png') }}" style="width: 21px;">
            @endif
            <span class="menu-item p-1">Attributes</span>
        </a>
    </li>
    @endcan
    <li class="{{ request()->is('admin/variants') || request()->is('admin/variants/*') ? 'active' : '' }} ">
        <a href="{{route("admin.variants.index")}}">
            @if(request()->is('admin/variants') )
            <img src="{{ asset('XR\app-assets\images\icons\Variants__Active.png') }}" style="width: 21px;">
            @else
            <img src="{{ asset('XR\app-assets\images\icons\Variants_2.png') }}" style="width: 21px;">
            @endif
            {{-- <i class="feather icon-pocket"></i> --}}
            <span class="menu-item p-1">Variants</span> 
        </a>
    </li>
    @can('product_tag_access')
    <!-- <li class="{{ request()->is('admin/product-tags') || request()->is('admin/product-tags/*') ? 'active' : '' }}">
                        <a href="{{ route("admin.product-tags.index") }}" >
                            <i class="feather icon-tag"></i>
                            <span class="menu-item">{{ trans('cruds.productTag.title') }}</span>
                        </a>
                    </li> -->
    @endcan
    @endcan
    </ul>
</li>
    @if(session('subscribed_features'))
        @if(array_key_exists("pos", session('subscribed_features')[0]))
            <li>
                <a href="{{ route('admin.pos.index') }}">
                @if(request()->is('admin/pos') )
                <img src="{{asset('/XR/app-assets/images/icons/pos-terminal-white.png')}}" style="width: 21px;">
                @else
                <img src="{{asset('/XR/app-assets/images/icons/pos-terminal.png')}}" style="width: 21px;">
                @endif
                    <span class="menu-item">&nbsp;&nbsp;Point of sales</span>
                </a>
            </li>
        @endif
    @endif
    @can('order_access')
    <li class="{{ request()->is('admin/orders') || request()->is('admin/orders/*') ? 'active' : '' }}">
        <a href="{{ route("admin.orders.index") }}">
                @if(request()->is('admin/orders') )
                <img src="{{ asset('XR\app-assets\images\icons\order-white.png') }}" style="width: 21px;">
                @else
                <img src="{{ asset('XR\app-assets\images\icons\order.png') }}" style="width: 21px;">
                @endif
        
            <span class="menu-item p-1">Orders</span>
        </a>
    </li>
    <li class="{{ request()->is('admin/customers') || request()->is('admin/customers/*') ? 'active' : '' }}">
        <a href="{{route("admin.customers.index")}}">
            @if(request()->is('admin/customers') )
            <img src="{{ asset('XR\app-assets\images\icons\customer-white.png') }}" style="width: 21px;">
            @else
            <img src="{{ asset('XR\app-assets\images\icons\customer.png') }}" style="width: 21px;">
            @endif
            <span class="menu-item p-1">Customers</span>
        </a>
    </li>
    <li class="{{ request()->is('admin/enquiry') || request()->is('admin/enquiry/*') ? 'active' : '' }}">
        <a href="{{route("admin.enquiry.index")}}">
            <i class="feather icon-help-circle"></i>
            <span class="menu-item">Enquiries</span>
        </a>
    </li>
    
    <li class="{{ request()->is('admin/coupons') || request()->is('admin/coupons/*') ? 'active' : '' }}">
        <a href="{{route("admin.coupons.index")}}">
            <i class="fa fa-ticket"></i>
            <span class="menu-item">Coupons</span>
        </a>
    </li>

    

    @can('show_transactions')
    <li class="{{ request()->is('admin/transactions') || request()->is('admin/transactions/*') ? 'active' : '' }}">
        <a href="{{route("admin.transactions.index")}}">
            @if(request()->is('admin/transactions') || request()->is('admin/transactions/*') )
            <img src="{{ asset('XR\app-assets\images\icons\transaction_white.png') }}" style="width: 28px;margin-left:-7px;padding:2px;">
            @else
            <img src="{{ asset('XR\app-assets\images\icons\transaction.svg') }}" style="width: 28px;margin-left:-7px;padding:2px;">
            @endif
            <span class="menu-item">Transactions</span>
        </a>
    </li>
    @endcan

    @can('show_analytics')
    <li>
        <a href="#">
            @if(request()->is('admin/stats/*'))
            <img src="{{ asset('XR\app-assets\images\icons\intelligence_white.png') }}" style="width: 32px;margin-left:-7px;">
            @else
            <img src="{{ asset('XR\app-assets\images\icons\intelligence.png') }}" style="width: 32px;margin-left:-7px;">
            @endif
            <span class="menu-item">Insights IQ</span>
        </a>

        <ul>
            <li class="{{ request()->is('admin/stats/dashboard') ? 'active' : '' }}">
                <a href="{{route('admin.stats.getSalesstat',['sales'=>'dashboard'])}}">

                    @if( request()->is('admin/stats/dashboard'))
                    <img src="{{ asset('XR\app-assets\images\icons\stats_white.png') }}" style="width: 25px;margin-left: -8px;margin-right: 2px;">
                    @else
                    <img src="{{ asset('XR\app-assets\images\icons\stats.png') }}" style="width: 25px;margin-left: -8px;margin-right: 2px;">
                    @endif
                    <span class="menu-item">Biz Insights</span>
                </a>
            </li>

            <!-- <li class="{{ request()->is('admin/stats/customers') ? 'active' : '' }}">
                <a href="{{route('admin.stats.getSalesstat','customers')}}">
                    <i class="feather icon-users"></i>
                    <span class="menu-item">Customers</span>
                </a>
            </li> -->
            <!-- <li class="{{ request()->is('admin/stats/orders')  ? 'active' : '' }}">
                <a href="{{route('admin.stats.getSalesstat','orders')}}">
                    <i class="feather icon-list"></i>
                    <span class="menu-item">Orders</span>
                </a>
            </li> -->
            <!-- 
            <li class="{{ request()->is('admin/stats_index') ? 'active' : '' }}">
                <a href="{{route("admin.stats.index")}}">
                    <i class="fa fa-bar-chart"></i>
                    <span class="menu-item">All</span>
                </a>
            </li> -->

        </ul>
    </li>
    @endcan

    

    <li class="{{ request()->is('admin/userSettings') || request()->is('admin/userSettings/*') ? 'active' : '' }}">
        <a href="{{ route("admin.userSettings.index") }}">
            <i class="feather icon-settings"></i>
            <span class="menu-item">Settings</span>
        </a>
    </li>
    @endif
    @endcan

    @if((auth()->user()->getIsAdminAttribute())== true)
        <li>
            <a href="{{ route('admin.plan_subscription_features.index') }}" >
                <img src="{{asset('/XR/app-assets/images/icons/subscription.svg')}}" style="width: 21px;">
                <span class="menu-item">&nbsp;&nbsp;Subscription features</span>
            </a>
        </li>
        <li>
            <a href="{{ route('admin.packages.index') }}" >
                <i class="feather icon-package"></i>
                <span class="menu-item">&nbsp;&nbsp;Packages</span>
            </a>
        </li>
        <li>
            <a href="log-viewer" >
            <!-- <a href="{{-- {{ route('admin.log-viewer') }}--}}" > -->
                <i class="feather icon-list"></i>
                <span class="menu-item">&nbsp;&nbsp;LogViewer</span>
            </a>
        </li>
        <li class=" nav-item"><a href="#"><i class="feather icon-bell"></i><span class="menu-title">{{ trans('cruds.notification.title') }}</span></a>
            <ul class="menu-content">
                <li class="{{ request()->is('admin/user-alerts') || request()->is('admin/user-alerts') ? 'active' : '' }}">
                        <a href="{{ route("admin.user-alerts.index") }}"><i class="feather icon-eye"></i><span class="menu-item">{{ trans('cruds.viewnotification.title') }}</span></a>
                </li>
                <li class="{{ request()->is('admin/user-alerts/create') || request()->is('admin/user-alerts/create*') ? 'active' : '' }}"">
                        <a href="{{ route("admin.user-alerts.create") }}"><i class="feather icon-send"></i><span class="menu-item">{{ trans('cruds.sendnotification.title') }}</span></a>
                </li>
            </ul>
        </li>
    @endif
    <!-- <li class="{{ request()->is('admin/accountSettings') || request()->is('admin/accountSettings/*') ? 'active' : '' }}">
        <a href="{{ route("admin.users.accountSettings") }}">
            <i class="feather icon-user"></i>
            <span class="menu-item">My Profile</span>
        </a>
    </li> -->
    @if((auth()->user()->getIsAdminAttribute()) != true)
    <li class=" nav-item"><a href="#"><i class="feather icon-users"></i><span class="menu-title">My Subscription</span></a>
    <ul class="menu-content">
    <!-- <li class="{{ request()->is('admin/accountSettings') || request()->is('admin/accountSettings/*') ? 'active' : '' }}">
        <a href="{{ route("admin.users.accountSettings") }}">
            <i class="feather icon-user"></i>
            <span class="menu-item">User Profile</span>
        </a>
    </li> -->
    @if((auth()->user()->getIsAdminAttribute()) != true)
        
        <li class="{{ request()->is('admin/subscriptions') || request()->is('admin/subscriptions/*') ? 'active' : '' }}">
            <a href="{{ route('admin.subscriptions.index') }}">
                <i class="fa fa-usd" aria-hidden="true"></i>
                <span class="menu-item">&nbsp;&nbsp;Plans & Pricing</span>
            </a>
        </li>
        <li class="{{ request()->is('admin/subscription/myPlan') || request()->is('admin/subscription/*') ? 'active' : '' }}">
            <a href="{{ route('admin.subscription.myPlan') }}">
                <i class="feather icon-package"></i>
                <span class="menu-item">My plan</span>
            </a>
        </li>
        <li class="{{ request()->is('admin/user_payments') || request()->is('admin/user_payments/*') ? 'active' : '' }}">
            <a href="{{ route('admin.user_payments.index') }}">
                <i class="fa fa-credit-card"></i>
                <span class="menu-item">My Payments</span>
            </a>
        </li>
        <li class="{{ request()->is('admin/account/mobileLogin') || request()->is('admin/account/*') ? 'active' : '' }}">
            <a href="{{ route('admin.mobileLogin') }}">
                <i class="feather icon-smartphone"></i>
                <span class="menu-item">Mobile App Login</span>
            </a>
        </li>
    @endif
    </li>
    </ul>
    @endif

    @if((auth()->user()->getIsAdminAttribute())== true)
    <li class="">
        <a class=" {{ request()->is('admin/userTranscations') || request()->is('admin/userTranscations/*') ? 'active' : '' }}" href="{{ route('admin.userTranscations.index') }}">
            <i class="fa fa-credit-card"></i>
            <span class="menu-item">User Transaction</span>
        </a>
    </li>
    @endif 
    
    @if((auth()->user()->getIsAdminAttribute())== true)
    <li class="">
        <a class=" {{ request()->is('admin/devices') || request()->is('admin/devices/*') ? 'active' : '' }}" href="{{ route('admin.devices.index') }}">
            <i class="fa fa-desktop"></i>
            <span class="menu-item">Device</span>
        </a>
    </li>
    @endif
    @if(auth()->user()->getIsAdminAttribute()==true)
    <li class="">
        <a class=" {{ request()->is('admin/maintenance') || request()->is('admin/maintenance/*') ? 'active' : '' }}" href="{{ route('admin.maintenance') }}">
            <i class="fa fa-check-circle"></i>
            <span class="menu-item">{{ __('Maintenance mode') }}</span>
        </a>
    </li>
    @endif

    <li class="{{ request()->is('admin/referrals') || request()->is('admin/referrals/*') ? 'active' : '' }}">
        <a href="{{route("admin.referrals.index")}}">
        @if(request()->is('admin/referrals') )
            <img src="{{ asset('XR\app-assets\images\icons\referral-white.png') }}" style="height: 21px;width: 21px;">
        @else
            <img src="{{ asset('XR\app-assets\images\icons\referral.png') }}" style="height: 21px;width: 21px;">
        @endif
            
            <span class="menu-item p-1">Referral</span>
        </a>
    </li>
    
    @if((auth()->user()->getIsAdminAttribute()) != true)
    <li>
        <a target="_blank" href="https://api.whatsapp.com/send?phone=918925685969&text=I need your support for our portal {{App\Http\Controllers\Admin\CampaignsController::publicLink($userSettings->public_link,'store')}}, can we chat now?">
            <i class="fa fa-whatsapp"></i>
            <span class="menu-item"> Chat with us</span>
        </a>
    </li>
    @endif


    @if(app('impersonate')->isImpersonating())

    @else

    <li>
        <a href="#" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
            <i class="feather icon-log-out">

            </i>
            <span class="menu-item">{{ trans('global.logout') }}</span>
        </a>
    </li>

    @endif
    @impersonating
    <li>
        <a href="{{ route('admin.users.stopimpersonate') }}">
            <i class="fa fa-stop-circle"></i>
            <span class="menu-item">Stop Impersonating</span>
        </a>
    </li>
    @endImpersonating

    @can('super_admin')
    @can('user_management_access')
    @can('faq_management_access')
    <li class=" nav-item"><a href="#"><i class="feather icon-help-circle"></i><span class="menu-title">{{ trans('cruds.faqManagement.title') }}</span></a>
        <ul class="menu-content">
            @can('faq_category_access')
            <li class="{{ request()->is('admin/faq-categories') || request()->is('admin/faq-categories/*') ? 'active' : '' }}">
                <a href="{{ route("admin.faq-categories.index") }}">
                    <i class="feather icon-folder">

                    </i>
                    <span class="menu-item"> {{ trans('cruds.faqCategory.title') }}</span>
                </a>
            </li>
            @endcan
            @can('faq_question_access')
            <li class="{{ request()->is('admin/faq-questions') || request()->is('admin/faq-questions/*') ? 'active' : '' }}">
                <a href="{{ route("admin.faq-questions.index") }}">
                    <i class="feather icon-help-circle">

                    </i>
                    <span class="menu-item"> {{ trans('cruds.faqQuestion.title') }}</span>
                </a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan
    @can('content_management_access')
    <li class=" nav-item"><a href="#"><i class="feather icon-book"></i><span class="menu-title">{{ trans('cruds.contentManagement.title') }}</span></a>
        <ul class="menu-content">
            @can('content_category_access')
            <li class="{{ request()->is('admin/content-categories') || request()->is('admin/content-categories/*') ? 'active' : '' }}">
                <a href="{{ route("admin.content-categories.index") }}">
                    <i class="feather icon-folder">

                    </i>
                    <span class="menu-item"> {{ trans('cruds.contentCategory.title') }}</span>
                </a>
            </li>
            @endcan
            @can('content_tag_access')
            <li class="{{ request()->is('admin/content-tags') || request()->is('admin/content-tags/*') ? 'active' : '' }}">
                <a href="{{ route("admin.content-tags.index") }}">
                    <i class="feather icon-tag">

                    </i>
                    <span class="menu-item"> {{ trans('cruds.contentTag.title') }}</span>
                </a>
            </li>
            @endcan
            @can('content_page_access')
            <li class="{{ request()->is('admin/content-pages') || request()->is('admin/content-pages/*') ? 'active' : '' }}">
                <a href="{{ route("admin.content-pages.index") }}">
                    <i class="feather icon-file">

                    </i>
                    <span class="menu-item"> {{ trans('cruds.contentPage.title') }}</span>
                </a>
            </li>
            @endcan
        </ul>
    </li>
    @endcan

    @can('user_alert_access')
    <li class="{{ request()->is('admin/user-alerts') || request()->is('admin/user-alerts/*') ? 'active' : '' }}">
        <a href="{{ route("admin.user-alerts.index") }}">
            <i class="feather icon-bell">

            </i>
            <span class="menu-item"> {{ trans('cruds.userAlert.title') }}</span>
        </a>
    </li>
    @endcan
    @can('video_access')
    <li class="{{ request()->is('admin/videos') || request()->is('admin/videos/*') ? 'active' : '' }}">
        <a href="{{ route("admin.videos.index") }}">
            <i class="feather icon-video">

            </i>
            <span class="menu-item"> {{ trans('cruds.video.title') }}</span>
        </a>
    </li>
    @endcan
    @can('entitycard_access')
    <li class="{{ request()->is('admin/entitycards') || request()->is('admin/entitycards/*') ? 'active' : '' }}">
        <a href="{{ route("admin.entitycards.index") }}">
            <i class="fa fa-cube nav-icon">

            </i>
            <span class="menu-item"> {{ trans('cruds.entitycard.title') }}</span>
        </a>
    </li>
    @endcan
    @can('asset_access')
    <li class="{{ request()->is('admin/assets') || request()->is('admin/assets/*') ? 'active' : '' }}">
        <a href="{{ route("admin.assets.index") }}">
            <i class="feather icon-package">

            </i>
            <span class="menu-item"> {{ trans('cruds.asset.title') }}</span>
        </a>
    </li>
    @endcan


    <li class=" nav-item"><a href="#"><i class="feather icon-book"></i><span class="menu-title">{{ trans('cruds.userInfo.title') }}</span></a>
        <ul class="menu-content">
            @can('user_info_access')
            <li class="{{ request()->is('admin/user-infos') || request()->is('admin/user-infos/*') ? 'active' : '' }}">
                <a href="{{ route("admin.user-infos.index") }}">
                    <i class="feather icon-user">

                    </i>
                    <span class="menu-item"> {{ trans('cruds.userInfo.title') }}</span>
                </a>
            </li>
            @endcan
            @can('company_info_access')
            <li class="{{ request()->is('admin/company-infos') || request()->is('admin/company-infos/*') ? 'active' : '' }}">
                <a href="{{ route("admin.company-infos.index") }}">
                    <i class="fa fa-cogs nav-icon">

                    </i>
                    <span class="menu-item"> {{ trans('cruds.companyInfo.title') }}</span>
                </a>
            </li>
            @endcan
            @can('currency_access')
            <li class="{{ request()->is('admin/currencies') || request()->is('admin/currencies/*') ? 'active' : '' }}">
                <a href="{{ route("admin.currencies.index") }}">
                    <i class="fa fa-inr nav-icon">

                    </i>
                    <span class="menu-item"> {{ trans('cruds.currency.title') }}</span>
                </a>
            </li>
            @endcan
            @can('country_access')
            <li class="{{ request()->is('admin/countries') || request()->is('admin/countries/*') ? 'active' : '' }}">
                <a href="{{ route("admin.countries.index") }}">
                    <i class="fa fa-flag nav-icon">

                    </i>
                    <span class="menu-item"> {{ trans('cruds.country.title') }}</span>
                </a>
            </li>
            @endcan
            @can('state_access')
            <li class="{{ request()->is('admin/states') || request()->is('admin/states/*') ? 'active' : '' }}">
                <a href="{{ route("admin.states.index") }}">
                    <i class="fa fa-cogs nav-icon">

                    </i>
                    <span class="menu-item"> {{ trans('cruds.state.title') }}</span>
                </a>
            </li>
            @endcan
            @can('city_access')
            <li class="{{ request()->is('admin/cities') || request()->is('admin/cities/*') ? 'active' : '' }}">
                <a href="{{ route("admin.cities.index") }}">
                    <i class="fa fa-question-circle nav-icon">

                    </i>
                    <span class="menu-item"> {{ trans('cruds.city.title') }}</span>
                </a>
            </li>
            @endcan
        </ul>
    </li>


    @php($unread = \App\QaTopic::unreadCount())
    <li class="{{ request()->is('admin/messenger') || request()->is('admin/messenger/*') ? 'active' : '' }} nav-link">
        <a href="{{ route("admin.messenger.index") }}">
            <i class="feather icon-message-square">

            </i>
            <span class="menu-item">{{ trans('global.messages') }}</span>
            @if($unread > 0)
            <strong>( {{ $unread }} )</strong>
            @endif
        </a>
    </li>


    @endcan
    @endcan

    </ul>
</div>