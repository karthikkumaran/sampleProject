    @if(count($userSettings->bannerimg) > 0)
    <div class="col-md-12 col-sm-12 p-0">
        <div class="card mb-0">
            <!-- <div class="card-header">
                <h4 class="card-title">Optional captions</h4>
            </div> -->
            <div class="card-content">
                <div class="card-body" style="padding:5px;">
                    <div id="banner" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                        @foreach($userSettings->bannerimg as $bannerimg)
                            @if($loop->index == (count($userSettings->bannerimg) - 1))
                            <li data-target="#banner" data-slide-to="{{$loop->index}}" class="{{$loop->index == 0?'active':'active'}}"></li>
                            @endif
                        @endforeach
                        </ol>
                        <div class="carousel-inner" role="listbox">
                        @foreach($userSettings->bannerimg as $bannerimg)
                            @if($loop->index == (count($userSettings->bannerimg) - 1))
                            <div class="carousel-item {{$loop->index == 0?'active':'active'}}">
                                <img class="img-fluid w-100" style="height:280px;border-radius: 5px;" src="{{$bannerimg->getUrl()}}">
                                <div class="carousel-caption">
                                    <!-- <h3>First Slide {{ $loop->index }}</h3> -->
                                    <!-- <p>Donut jujubes I love topping I love sweet. Jujubes I love brownie gummi bears I
                                        love donut sweet
                                        chocolate. Tart chocolate marshmallow.Tart carrot cake muffin.</p> -->
                                </div>
                            </div>
                            @endif
                        @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#banner" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#banner" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif