<style>
    .font12{
        font-size: 11px !important;
    }
   
</style>

        @if(!(request()->is('admin/subscriptions')))
        <li class="nav-item">
            <div class="btn-group mt-1 mb-1 mr-1">
                <a href="/admin/subscriptions" class="btn btn-info btn-icon" >
                <img src="{{ asset('XR\app-assets\images\icons\planCrown-white.png') }}"  style="height: 21px;width: 21px;  margin-top:-5px; margin-right:4px;">
                <!-- <i class="fa fa-usd fa-2x " aria-hidden="true"></i> -->
                <span class="font12"> Upgrade Plan</span>
                </a>
                            
            </div>
        </li>
        @endif
    @if($userSettings->is_start == 0)
        <li class="nav-item">
            <div class="btn-group m-1 ">
                <a href="#" onclick="publishStore({{ auth()->user()->id }},{{$userSettings->is_start == 1?0:1}})"
                    class="btn {{$userSettings->is_start == 1?'btn-danger':'btn-info'}} btn-icon"><img class="mb-xs-1" src="{{ asset('XR\app-assets\images\icons\store_white.png') }}" style="height: 18px;width: 18px;"> {{$userSettings->is_start == 1?"Store Unpublish":"Publish Store"}}</a>
            </div>
        </li>
    @endif

    
    @if($userSettings->is_start == 1)
        <li class="nav-item">
            <div class="btn-group mt-1 mb-1 mr-1">
                <a href="#" class="btn btn-primary btn-icon" data-toggle="modal" data-target="#StorePublishedModal">
                <img src="{{ asset('XR\app-assets\images\icons\share.png') }}" style="height: 21px;width: 21px;  margin-top:-10px; margin-right:5px;"><span class="font12"> Share Store</span>
                </a>
                            
            </div>
        </li>
    @endif

    {{-- <li class="nav-item {{request()->is('admin/mystore') ? '' : 'd-none'}}">
        <div class="btn-group mt-1 mb-1 mr-1">
            <a href="{{route('admin.mystore.preview',$userSettings->preview_link)}}" target="__blank"
                class="btn btn-warning btn-icon"><i class="feather icon-eye"></i></a>
        </div>
    </li> --}}

    

    <!-- <li class="nav-item"><a href="{{route('admin.campaigns.create')}}" class="btn btn-primary mt-1 mr-1 mb-1 waves-effect waves-light"><i class="feather icon-plus-circle"></i> Create Campaign</a></li> -->
    <li class="nav-item d-none d-lg-block mr-1"><a class="nav-link nav-link-expand"><i class="ficon feather icon-maximize"></i></a></li>
    <!-- <li class="nav-item dropdown notifications-menu">
                    
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        @if(count($alerts = \Auth::user()->userUserAlerts()->withPivot('read')->limit(10)->orderBy('created_at', 'ASC')->get()->reverse()) > 0)
                            @foreach($alerts as $alert)
                                <div class="dropdown-item">
                                    <a href="{{ $alert->alert_link ? $alert->alert_link : "#" }}" target="_blank" rel="noopener noreferrer">
                                        @if($alert->pivot->read === 0) <strong> @endif
                                            {{ $alert->alert_text }}
                                            @if($alert->pivot->read === 0) </strong> @endif
                                    </a>
                                </div>
                            @endforeach
                        @else
                            <div class="text-center">
                                {{ trans('global.no_alerts') }}
                            </div>
                        @endif
                    </div>
                </li> -->
                    
                        <!-- <li class="dropdown dropdown-user nav-item"><a class="dropdown-toggle nav-link dropdown-user-link" href="#" data-toggle="dropdown">
                                <div class="user-nav d-sm-flex d-none"><span class="user-name text-bold-600">{{Illuminate\Support\Facades\Auth::user()->name}}</span><span class="user-status">Available</span></div><span><img class="round" src="{{ asset('XR/assets/images/profile.png') }}" alt="avatar" height="40" width="40"></span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" href="{{ route("admin.users.accountSettings") }}"><i class="feather icon-user"></i> Edit Profile</a>
                            <div class="dropdown-divider"></div><a class="dropdown-item" href="#" onclick="event.preventDefault(); document.getElementById('logoutform').submit();"><i class="feather icon-power"></i> Logout</a>
                            </div>
                        </li>  -->
                        