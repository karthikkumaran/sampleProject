<footer class="footer footer-static footer-light mb-0">
    <p class="clearfix blue-grey lighten-2 mb-0 "><span class="float-md-left d-block d-md-inline-block mt-25">COPYRIGHT &copy; 2020 - 2022<a class="text-bold-800 grey darken-2" href="https://simplisell.co" target="_blank"><b><span class="text-blue">Simpli</span><span class="text-red">Sell</span></b>,</a>a product of <a class="text-bold-800 grey darken-2" href="https://mirrar.co" target="_blank">MirrAR Innovation Technologies Pvt Ltd</a>,All rights Reserved</span>
        <span class="float-md-right ">Version {{env('BUILD_VERSION')}}</span>
        <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="feather icon-arrow-up"></i></button>
    </p>
    
</footer>
<div class="w-100 d-inline-flex d-lg-none d-md-inline-flex bg-white position-sticky border-top-light" style="bottom:0px;z-index:5;height:55px;">
    <div class="h-100 d-flex justify-content-center align-items-center" style="width:20%;">
        <a href="/admin/mystore" id="store" class="text-dark">
            <span class="d-block text-center"><i class="feather icon-archive" style="font-size:160%;"></i></span>
            <span class="text-center d-block" style="font-size:80%">My Store</span>
        </a>
    </div>
    <div class="h-100 d-flex justify-content-center align-items-center" style="width:20%">
        <a href="/admin/campaigns" id="cat" class="text-dark">
            <span class="d-block text-center"><i class="feather icon-folder" style="font-size:160%;"></i></span>
            <span class="text-center d-block" style="font-size:80%">Catalogs</span>
        </a>
        
    </div>
    <div class="h-100 d-flex justify-content-center align-items-center" style="width:20%">
        <a href="/admin/myproducts" id="pro" class="text-dark">
            <span class="d-block text-center"><i class="feather icon-box" style="font-size:160%;"></i></span>
            <span class="text-center d-block" style="font-size:80%">Products</span>
        </a>
        
    </div>
    <div class="h-100 d-flex justify-content-center align-items-center" style="width:20%">
        <a href="/admin/orders" id="ord" class="text-dark">
            <span class="d-block text-center"><i class="feather icon-list" style="font-size:160%;"></i></span>
            <span class="text-center d-block" style="font-size:80%">Orders</span>
        </a>
        
    </div>
    <div class="h-100 d-flex justify-content-center align-items-center" style="width:20%">
        <a href="/admin/enquiry" id="enq" class="text-dark">
            <span class="d-block text-center"><i class="feather icon-help-circle" style="font-size:160%;"></i></span>
            <span class="text-center d-block" style="font-size:80%">Enquiry</span>
        </a>
    </div>
</div>
<script>
    function geturl() {
        var url=window.location.href;
        var urs=window.location.origin + '/admin/mystore';
        var urp=window.location.origin + '/admin/myproducts';
        var uro=window.location.origin + '/admin/orders';
        var ure=window.location.origin + '/admin/enquiry';
        var urc=window.location.origin + '/admin/campaigns';
        if(url==urs){
            $("#store").removeClass('text-dark');
        }
        if(url==urp){
            $("#pro").removeClass('text-dark');
        }
        if(url==uro){
            $("#ord").removeClass('text-dark');
        }
        if(url==ure){
            $("#enq").removeClass('text-dark');
        }
        if(url==urc){
            $("#cat").removeClass('text-dark');
        }
    }
    window.onload = geturl;
</script>