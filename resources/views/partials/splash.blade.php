<style>
        #loading-bg {
            width: 100%;
            height: 100%;

            background: {{ $meta_data['splash']['bg_color'] ?? '#FFF' }};
            display: block;
            position: absolute;
            z-index: 19;
        }

        .loading-logo {
            position: absolute;
            left: calc(50% - 73px);
            top: 40%;
        }

        .loading-logo img {
            width: 150px;
            max-width: 150px;
        }

        .loading1 {
            position: absolute;
            left: calc(50% - 35px);
            top: 50%;
            width: 55px;
            height: 55px;
            /* border-radius: 50%;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            border: 3px solid transparent; */
        }

        .loading1 .effect-1,
        .loading1 .effect-2 {
            position: absolute;
            width: 100%;
            height: 100%;
            border: 3px solid transparent;
            border-left: 3px solid rgba(121, 97, 249, 1);
            border-radius: 50%;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }

        .loading1 .effect-1 {
            animation: rotate 1s ease infinite;
        }

        .loading1 .effect-2 {
            animation: rotateOpacity 1s ease infinite .1s;
        }

        .loading1 .effect-3 {
            position: absolute;
            width: 100%;
            height: 100%;
            border: 3px solid transparent;
            border-left: 3px solid rgba(121, 97, 249, 1);
            -webkit-animation: rotateOpacity 1s ease infinite .2s;
            animation: rotateOpacity 1s ease infinite .2s;
            border-radius: 50%;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
        }

        .loading1 .effects {
            transition: all .3s ease;
        }

        @keyframes rotate {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(1turn);
                transform: rotate(1turn);
            }
        }

        @keyframes rotateOpacity {
            0% {
                -webkit-transform: rotate(0deg);
                transform: rotate(0deg);
                opacity: .1;
            }

            100% {
                -webkit-transform: rotate(1turn);
                transform: rotate(1turn);
                opacity: 1;
            }
        }

    </style>

<div id="loading-bg">
        <div class="loading-logo">
        @if(!empty($meta_data['splash']))    
        @if($meta_data['splash']['brand_type'] == 2 && $meta_data['splash']['brand_name'] != "")
            <h1 style="color: {{ $meta_data['splash']['brand_color'] ?? '' }}">{{$meta_data['splash']['brand_name']}}</h1>
        @else
            <img src="{{ $userSettings->getFirstMediaUrl('splashlogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                onerror="this.onerror=null;this.src='{{asset('XR/app-assets/images/logo/logo.png')}}';" alt="Logo">
        @endif
        @else
            <img src="{{ $userSettings->getFirstMediaUrl('splashlogo') ?? asset('XR/app-assets/images/logo/logo.png') }}"
                onerror="this.onerror=null;this.src='{{asset('XR/app-assets/images/logo/logo.png')}}';" alt="Logo">
        @endif
        </div>
        <!-- <div class="loading1">
            <div class="effect-1 effects"></div>
            <div class="effect-2 effects"></div>
            <div class="effect-3 effects"></div>
        </div> -->
        <div class="loading1">
            <!-- <div class="spinner-grow text-danger w-100 h-100" role="status">
                <span class="sr-only">Loading...</span>
            </div> -->
            <div class="spinner-{{ $meta_data['splash']['loader_type'] ?? 'border' }} w-100 h-100"
                style="color: {{ $meta_data['splash']['loader_color'] ?? '' }}" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>
   
