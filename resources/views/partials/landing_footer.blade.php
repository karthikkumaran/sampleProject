<!-- start footer -->
<footer class="footer footer--s1 footer--color-light">
				<div class="footer__line footer__line--first">
					<div class="container">
						<div class="row">
							<div class="col-12 col-md-12 col-lg-12 col-xl-12 section-heading--center">
								<div class="footer__item">
									<a class="footer__logo site-logo" href="index.html">
										<img class="img-fluid" src="{{asset('XR/app-assets/images/logo/logo.png')}}" width="159" height="45" alt="demo" />
									</a>
								</div>

								<div class="footer__item">
									<!-- start social buttons -->
									<div class="s-btns s-btns--md s-btns--colored s-btns--rounded" style="display:inline-block;">
										<ul class="d-flex flex-row flex-wrap align-items-center">
											<li><a class="f" href="https://www.facebook.com/simplisell.co/" target="_blank"><i class="fontello-facebook"></i></a></li>
											<li><a class="t" href="#"><i class="fontello-twitter"></i></a></li>
											<li><a class="y" href="#"><i class="fontello-youtube-play"></i></a></li>
											<li><a class="i" href="#"><i class="fontello-instagram"></i></a></li>
										</ul>
									</div>
									<!-- end social buttons -->
								</div>

								<div class="footer__item">
									<span class="__copy"><a class="__dev" href="{{route('landing.termsAndConditions')}}" target="_blank">Terms and Conditions</a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="__dev" href="{{route('landing.privacyPolicy')}}" target="_blank">Privacy Policy</a>&nbsp;&nbsp;&nbsp;&nbsp;<a class="__dev" href="{{route('landing.bannedItems')}}" target="_blank">Banned Items</a></span></br>
									<span class="__copy">COPYRIGHT © 2020 - 2022 a Product By <a class="__dev" href="https://mirrar.co" target="_blank">MirrAR Innovation Technology Pvt. Ltd.</a></span>
								</div>
							</div>

							
						</div>
					</div>
				</div>

				<div class="footer__waves-container">
					<svg class="footer__wave js-wave" data-wave='{"height": 40, "bones": 6, "amplitude": 70, "color": "rgba(78, 111, 136, 0.14)", "speed": 0.3}' width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg"><defs></defs><path d=""/></svg>

					<svg class="footer__wave js-wave" data-wave='{"height": 60, "bones": 5, "amplitude": 90, "color": "rgba(243, 248, 249, 0.02)", "speed": 0.35}' width="100%" height="100%" version="1.1" xmlns="http://www.w3.org/2000/svg"><defs></defs><path d=""/></svg>
				</div>
			</footer>
			<!-- end footer -->