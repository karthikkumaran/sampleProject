@can($viewGate??'')
    <!-- <a class="btn btn-xs btn-primary" href="{{ route('admin.' . $crudRoutePart . '.show', $row->id) }}">
        {{ trans('global.view') }}
    </a> -->
    <a class="btn btn-xs btn-primary btn-icon" href="{{ route('admin.' . $crudRoutePart . '.show', $row->id) }}">
    <i class="feather icon-eye"></i>
    </a>
@endcan
@can($editGate)
    <!-- <a class="btn btn-xs btn-info" href="{{ route('admin.' . $crudRoutePart . '.edit', $row->id) }}">
        {{ trans('global.edit') }}
    </a> -->
    <a class="btn btn-xs btn-info btn-icon" href="{{ route('admin.' . $crudRoutePart . '.edit', $row->attribute_group_name) }}">
    <i class="feather icon-edit"></i>
    </a>
@endcan
@can($deleteGate)
    {{-- <form action="{{ route('admin.' . $crudRoutePart . '.destroy', $row->id) }}"  method="POST"  style="display: inline-block>
        <input type="hidden" name="_method" value="DELETE">
        <input type="hidden" name="_token" value="{{ csrf_token() }}"> --}}
        <!-- <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}"> -->
        <button type="submit" class="btn btn-xs btn-danger btn-icon" 
        onclick="delete_attr_group($(this))" 
        data-crudroutepart={{$crudRoutePart}}
        data-id="{{$row->id}}">
            <i class="feather icon-trash"></i></button>
    </form>
@endcan




<script>

@can('product_attribute_delete')

function delete_attr_group(attribute) {

        var id = attribute.attr('data-id');
        var crudroutepart=attribute.attr('data-crudroutepart');
        var url = "{{route('admin.product-attributes.destroy',':id')}}";
        url = url.replace(':id', id);
        swal.fire({
            title: 'Are you sure to delete this attribute group?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, Delete it',
            cancelButtonText: 'No, Cancel',
            confirmButtonClass: 'btn btn-danger',
            cancelButtonClass: 'btn btn-light',
        }).then(function (result) {
            if (result.value) {
                // console.log(result.value);
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        '_method': 'DELETE'
                    },
                    success: function (response_sub) {
                        // console.log(response_sub.status)

                        
                        if(response_sub.status == "warning") {
                            Swal.fire({
                            type: "warning",
                            title: 'Delete',
                            text: "This attribute group has an attribute added to one of the product, Are you sure to delete this attribute group?",
                            showCancelButton: true,
                            confirmButtonText: 'Yes, Delete it',
                            cancelButtonText: 'No, Cancel',
                            confirmButtonClass: 'btn btn-danger',
                            cancelButtonClass: 'btn btn-light',
                            }).then((result) => {
                               if(result.value)
                               {
                                   $.ajax({
                                      url:url+"?flag="+true,
                                      type:"POST",
                                      data:{
                                          '_method':'DELETE'
                                      },
                                      success:function(response)
                                      {
                                        if(response.status=="success"){
                                            delete_success();    
                                          }
                                      },error: function (res) {
                                            Swal.fire({
                                            type: "error",
                                            title: 'Delete',
                                            text: "Can't delete this attribute group.",
                                            confirmButtonClass: 'btn btn-danger',
                                            }).then((result) => {
                                            // window.location.reload();
                                            });
                                        }
                                   });
                               }
                            });
                        }
                        else if(response_sub.status == "success")
                        {
                           delete_success();
                        }
                    }
                }); 
            }
        });
}
@endcan

function delete_success(){
 Swal.fire({
    type: "success",                   
    title: 'Delete',
    text: 'Attribute group has been deleted.',
    confirmButtonClass: 'btn btn-success',
    }).then((result) => {
    window.location.reload();
                                               
});
}
</script>
