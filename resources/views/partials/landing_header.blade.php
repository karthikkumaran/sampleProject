<!-- start header -->
<header id="top-bar" class="top-bar top-bar--dark" data-nav-anchor="true">
				<div class="top-bar__inner">
					<div class="container-fluid">
						<div class="row align-items-center no-gutters">

							<a class="top-bar__logo site-logo" href="index_5.html">
								<img class="img-fluid" src="{{asset('XR/app-assets/images/logo/logo.png')}}" width="159" height="45" alt="demo" />
							</a>

							<a id="top-bar__navigation-toggler" class="top-bar__navigation-toggler" href="javascript:void(0);">
								<span></span>
							</a>

							<div class="top-bar__collapse">
								<nav id="top-bar__navigation" class="top-bar__navigation" role="navigation">
									<ul>
										<li>
											<a class="nav-link active" href="/">Home</a>
										</li>

										<li>
											<a class="nav-link" href="#features">Features</a>
										</li>

										<li>
											<a class="nav-link" href="#pricing">Pricing</a>
										</li>
										
										<li>
											<a class="nav-link" href="#faq">FAQ</a>
										</li>

										<li>
											<a class="nav-link" href="#contact">Contact</a>
										</li>

									
									</ul>
								</nav>

								<div id="top-bar__action" class="top-bar__action">
									<div class="d-xl-flex flex-xl-row flex-xl-wrap align-items-xl-center">
										<div class="top-bar__auth-btns">
											<a href="{{ route('login') }}">Login</a>

											<a class="custom-btn custom-btn--medium btn-danger custom-btn--style-3" href="{{ route('register') }}">Signup</a>
										</div>

									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</header>
			<!-- end header -->

		