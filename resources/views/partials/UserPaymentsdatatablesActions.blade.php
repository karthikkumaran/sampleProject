<a class="btn btn-xs btn-primary btn-icon" href="{{ route('admin.' . $crudRoutePart . '.show', $row->id) }}">
    <i class="feather icon-eye"></i>
</a>