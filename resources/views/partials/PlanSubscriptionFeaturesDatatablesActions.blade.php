@can($editGate)
    <a class="btn btn-xs btn-info btn-icon" href="{{ route('admin.' . $crudRoutePart . '.edit', $row->id) }}">
        <i class="feather icon-edit"></i>
    </a>
@endcan
