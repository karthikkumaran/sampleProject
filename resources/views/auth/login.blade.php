@extends('layouts.app')
@section('styles')
<link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/pages/authentication.css') }}">
@endsection
@section('content')
@php 
$agent=new \Jenssegers\Agent\Agent();
@endphp
<style>
    .customBtn {
      display: inline-block;
      background: white;
      color: #444;
      width: 190px;
      border-radius: 5px;
      border: thin solid #888;
      box-shadow: 1px 1px 1px grey;
      white-space: nowrap;
    }
    .btn-googl:hover {
    color: #FFF;
    background-color: #4285F4;
    border-color: #4285F4;
}
.login-footer{
background-color: #FFF;
}
</style>
<div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="row flexbox-container">
                    <div class="col-xl-8 col-11 d-flex justify-content-center">
                        <div class="card bg-authentication rounded-0 mb-0">
                            <div class="row m-0">
                                <div class="col-lg-6 d-lg-block d-none text-center align-self-center px-1 py-0">
                                    <img src="{{asset('XR/app-assets/images/pages/login.png')}}">
                                </div>
                                <div class="col-lg-6 col-12 p-0">
                                    <div class="card rounded-0 mb-0 p-2">
                                    <div class="divider mb-0">
                                    <img src="{{asset('XR/app-assets/images/logo/logo.png')}}" style="height: 40px;"/>
                                            </div>
                                        <div class="card-header ">

                                            <div class="card-title">
                                                <h4 class="mb-0">{{ trans('global.login') }} </h4>
                                            </div>
                                        </div>
                                        <!-- <p class="px-2">Welcome back, please login to your account.</p> -->
                                        @if(session('message'))
                                            <div class="alert alert-danger" role="alert">
                                                {{ session('message') }}
                                            </div>
                                        @endif
                                        <div class="card-content">
                                            <div class="card-body pt-1">
                                            
                                            <!-- <div>Login or Signup using</div><br> -->
                                            <div class="footer-btn d-inline">
                                                                             
                    <div> <a href="{{ route('social.login','google') }}" class="btn btn-googl customBtn w-100 ">
                    @if($agent->isMobile())
                    <div class="row">
                            <div class="col-2">
                        <img src="{{ asset('XR\app-assets\images\icons\G-logo.png') }}" class="float-left mr-2" style="width: 24px; height:24px;">
                        </div>
                            <div class="col-10 m-auto ">
                        <span class=" float-left m-auto " >Login with Google</span>
                        </div>
                        </div>

                    @else
                        <div class="row">
                        <div class="col-md-2">
                        <img src="{{ asset('XR\app-assets\images\icons\G-logo.png') }}" class="float-left" style="width: 24px; height:24px;">
                        </div>
                        <div class="col-md-10 m-auto" id="google">
                        <span class=" float-left " >Login with Google</span>
                        </div>
                        </div>
                    @endif  

                    </a>
                    </div><br>
                        <!-- <span class="fa fa-google">oogle</span> -->
                                                <div><a href="{{ route('social.login','facebook') }}" class="btn btn-facebook w-100">
                    @if($agent->isMobile())
                    <div class="row">
                            <div class="col-2">
                    <i class="fa fa-facebook fa-2x mr-2" aria-hidden="true" style="color:'white;background-color:'blue';"></i>
                            </div>
                            <div class="col-10 m-auto ">
                        <span class="float-left " style=" text-align:Center;">Login with Facebook</span>
                        </div>
                            </div>
                    @else                       
                        <div class="row">
                            <div class="col-md-2">
                        <i class="fa fa-facebook fa-2x" aria-hidden="true" style="color:'white;background-color:'blue';"></i>
                            </div>
                            <div class="col-md-10 m-auto">

                        <span class=" float-left " >Login with Facebook</span>
                        </div>
                        </div>
                    @endif
                                                    <!-- <span class="fa fa-facebook">acebook</span> -->
                                                </a></div> 
</div>
                                                <!-- <a href="{{ route('social.login','facebook') }}" class="btn btn-facebook"><span class="fa fa-facebook">acebook</span></a> -->
                                                <!-- <a href="#" class="btn btn-twitter white"><span class="fa fa-twitter"></span></a> -->
                                                <!-- <a href="{{ route('social.login','google') }}" class="btn btn-google"><span class="fa fa-google">oogle</span></a> -->
                                                <!-- <a href="#" class="btn btn-github"><span class="fa fa-github-alt"></span></a> -->
                                            </div>
                                            <div class="divider">
                                                <div class="divider-text">OR</div>
                                            </div>
                                            <form method="POST" action="{{ route('login') }}">
                                            @csrf

                                                    <fieldset class="form-label-group form-group position-relative has-icon-left">
                                                    <input id="email" name="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required autocomplete="email" autofocus placeholder="{{ trans('global.login_email') }}" value="{{ old('email', null) }}">
                                                        <div class="form-control-position">
                                                            <i class="feather icon-user"></i>
                                                        </div>
                                                        <label for="user-name" class="text-bold">Your Email Address</label>
                                                    </fieldset>

                                                    <fieldset class="form-label-group position-relative has-icon-left">
                                                    <input id="password" name="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required placeholder="{{ trans('global.login_password') }}">
                                                        <div class="form-control-position">
                                                            <i class="feather icon-lock"></i>
                                                        </div>
                                                        <label for="user-password" class="text-bold">Your Password</label>
                                                    </fieldset>
                                                    
                                                    <div class="form-group ">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                    <button type="submit" class="btn btn-primary d-flex justify-content-center w-100 btn-inline">{{ trans('global.login') }}</button>
                                                    </div>
                                                    </div>
                                                    </div>
                                                    <div class="form-group d-flex justify-content-center align-items-center">

                                                        <!-- <div class="text-left">
                                                            <fieldset class="checkbox">
                                                                <div class="vs-checkbox-con vs-checkbox-primary">
                                                                <input name="remember" type="checkbox" id="remember"/>
                                                                    <span class="vs-checkbox">
                                                                        <span class="vs-checkbox--check">
                                                                            <i class="vs-icon feather icon-check"></i>
                                                                        </span>
                                                                    </span>
                                                                    <span class=""> {{ trans('global.remember_me') }}</span>
                                                                </div>
                                                            </fieldset>
                                                        </div> -->
                                                        @if(Route::has('password.request'))
                                                            <div class="text-center"><u><a href="{{ route('password.request') }}" class="card-link">{{ trans('global.forgot_password') }}</a></u></div>
                                                        @endif              
                                                    </div>
                                                    <div class="text-center">
                                                    <span>Don't have an account yet?</span>
                                                    <a href="{{ route('register') }}" ><b><u>{{ trans('global.signup') }}</u></b></a>
                                                    </div>
                                                    <!-- <button type="submit" class="btn btn-primary float-right btn-inline">{{ trans('global.login') }}</button> -->
                                        <br>

                                                </form>
                                                <div class="text-center">
                                                By continuing, you agree to the <a href="{{route('landing.privacyPolicy')}}" target="_blank">&nbsp;privacy policy</a> and 
                                                <a href="{{route('landing.termsAndConditions')}}" target="_blank">terms & conditions</a>, 
                                                also you assure that you will not list any of the banned products mentioned here <a href="{{route('landing.bannedItems')}}" target="_blank">Banned Items.</a><br><br>
                                                <h5>Go to Website <a href="/"><b>Home</b></a></h5>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="login-footer text-center">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
@endsection