@component('mail::message')
{{-- Greeting --}}
# @lang('Hello!')


{{-- Intro Lines --}}
@foreach ($introLines as $line)
{{ $line }}
@endforeach 

{{-- Action Button --}}
@isset($actionText)

@component('mail::button', ['url' => $actionUrl, 'color' => 'primary'])
{{ $actionText }}
@endcomponent
@endisset

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}
@endforeach

{{-- Salutation --}}
@lang('Regards'),<br>
{{ $regards_name }}

{{-- Subcopy --}}
@isset($actionText)
@slot('subcopy')
@lang(
    "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n".
    'into your web browser:',
    [
        'actionText' => $actionText,
    ]
) <span class="break-all"><a href="{{$actionUrl}}">({{ $actionUrl }})</a></span>
@endslot
@endisset
@endcomponent
