@extends('layouts.register')
@section('content')
<style>
    .customBtn {
      display: inline-block;
      background: white;
      color: #444;
      width: 190px;
      border-radius: 5px;
      border: thin solid #888;
      box-shadow: 1px 1px 1px grey;
      white-space: nowrap;
    }
    .btn-googl:hover {
    color: #FFF;
    background-color: #4285F4;
    border-color: #4285F4;
}
#signup_btn{
    /* background-color: #3B5998 !important; */
}
.w-100{
    width: 105% !important;
}
/* .btn-facebook {
    color: #3B5998;
    background-color: #FFFFFF;
    border-color: #000;
} */
 /* #google:hover {
    color: #fff;
    background-color: #4285F4;
    border-color: #4285F4;
} */
</style>
@php 
$agent=new \Jenssegers\Agent\Agent();
@endphp
    <div class="auth-wrapper auth-v2 h-100">
        <div class="auth-inner row m-0 h-100">
            <!-- Left Text-->
            <div class="d-none d-lg-flex col-lg-8 align-items-center p-1">
                <div class="row">
                    <div class="w-100 text-center px-3">
                        <h1>Everything you need to get your business online</h1>
                        <h1>Try for free - no credit card required</h1>
                    </div>
                    <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
                        <img class="img-fluid" src="{{ asset('XR/app-assets/images/pages/register_page_bg.png') }}" alt="Register V2" style="opacity: 0.75;"/>
                    </div>
                </div>
            </div>
            <!-- /Left Text-->
            
            <!-- Register-->
            <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5 bg-white">
                <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto p-3">
                    <div class="loading-logo text-center ">
                        <img src="{{asset('XR/app-assets/images/logo/logo.png')}}" id="expand_logo" style="margin-top: -1rem;width: 180px;"/>
                    </div>
                    <br>
                    <div class="text-center">Start your  21 days free trail.</div><br>
                    <h6 class="card-text mb-2 text-center">Create your online store account</h6>
                    
                                            <div> Signup using</div><br>
                    <div class="footer-btn d-inline">
                                                
                    <div> <a href="{{ route('social.login','google') }}" class="btn btn-googl customBtn w-100 ">
                    @if($agent->isMobile())
                    <div class="row">
                            <div class="col-2">
                        <img src="{{ asset('XR\app-assets\images\icons\G-logo.png') }}" class="float-left mr-2" style="width: 24px; height:24px;">
                        </div>
                            <div class="col-10 m-auto ">
                        <span class=" float-left m-auto " >Signup with Google</span>
                        </div>
                        </div>

                    @else
                        <div class="row">
                        <div class="col-md-2">
                        <img src="{{ asset('XR\app-assets\images\icons\G-logo.png') }}" class="float-left" style="width: 24px; height:24px;">
                        </div>
                        <div class="col-md-10 m-auto" id="google">
                        <span class=" float-left " >Signup with Google</span>
                        </div>
                        </div>
                    @endif  

                    </a>
                    </div><br>
                        <!-- <span class="fa fa-google">oogle</span> -->
                                                <div><a href="{{ route('social.login','facebook') }}" class="btn btn-facebook w-100">
                    @if($agent->isMobile())
                    <div class="row">
                            <div class="col-2">
                    <i class="fa fa-facebook fa-2x mr-2" aria-hidden="true" style="color:'white;background-color:'blue';"></i>
                            </div>
                            <div class="col-10 m-auto ">
                        <span class="float-left " style=" text-align:Center;">Signup with Facebook</span>
                        </div>
                            </div>
                    @else                       
                        <div class="row">
                            <div class="col-md-2">
                        <i class="fa fa-facebook fa-2x" aria-hidden="true" style="color:'white;background-color:'blue';"></i>
                            </div>
                            <div class="col-md-10 m-auto">

                        <span class=" float-left " >Signup with Facebook</span>
                        </div>
                        </div>
                    @endif
                                                    <!-- <span class="fa fa-facebook">acebook</span> -->
                                                </a></div>  
                                                <!-- <a href="#" class="btn btn-twitter white"><span class="fa fa-twitter"></span></a> -->
                                                <!-- <a href="#" class="btn btn-github"><span class="fa fa-github-alt"></span></a> -->
                                            </div>
                                            <div class="divider">
                                                <div class="divider-text">OR</div>
                                            </div>
                    <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label class="form-label" for="register-username">Your name</label>
                            <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" required autofocus placeholder="Your name" value="{{ old('name', null) }}">
                            <!-- <input class="form-control" id="register-username" type="text" name="register-username" placeholder="johndoe" aria-describedby="register-username" autofocus="" tabindex="1" /> -->
                        </div>
                        <div class="form-group">
                            <label class="form-label " for="register-email">Mobile</label>
                            <input class="form-control {{ $errors->has('mobile') ? ' is-invalid' : '' }}" type="number" name="mobile" placeholder="Your mobile number" pattern="[0-9]{10}" maxlength="10" required/>
                            @if($errors->has('mobile'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('mobile') }}
                                </div>
                            @endif
                            <!-- <input type="tel" name="mobile" class="form-control {{ $errors->has('mobile') ? ' is-invalid' : '' }}" required placeholder="Mobile Number" value="{{ old('mobile', null) }}"> -->
                            <!-- <input class="form-control" id="register-email" type="text" name="register-email" placeholder="john@example.com" aria-describedby="register-email" tabindex="2" /> -->
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="register-email">Email</label>
                            <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" required placeholder="Your Email" value="{{ old('email', null) }}">
                            @if($errors->has('email'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('email') }}
                                </div>
                            @endif
                            <!-- <input class="form-control" id="register-email" type="text" name="register-email" placeholder="john@example.com" aria-describedby="register-email" tabindex="2" /> -->
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="register-password">Password</label>
                            <div class="input-group input-group-merge form-password-toggle">
                                <input type="password" name="password" id="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required placeholder="Enter password">    
                                <!-- <input class="form-control form-control-merge" id="register-password" type="password" name="register-password"  aria-describedby="register-password" tabindex="3" /> -->
                                <div class="input-group-append">
                                    <span class="input-group-text cursor-pointer" ><i class="fa fa-eye" id="togglePassword" aria-hidden="true"></i></span>
                                </div>
                            </div>
                            @if($errors->has('password'))
                                <div class="invalid-feedback">
                                    Please enter a valid password
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="register-password">Confirm Password</label>
                            <div class="input-group input-group-merge form-password-toggle">
                                <input type="password" name="confirm_password" id="confirm_password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required placeholder="Re-enter password">    
                                <!-- <input class="form-control form-control-merge" id="register-password" type="password" name="register-password"  aria-describedby="register-password" tabindex="3" /> -->
                                <div class="input-group-append">
                                    <span class="input-group-text cursor-pointer" ><i class="fa fa-eye" id="toggleConfirmPassword" aria-hidden="true"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="custom-control custom-checkbox">
                                <input class="custom-control-input" id="register-privacy-policy" type="checkbox" tabindex="4" />
                                <label class="custom-control-label" for="register-privacy-policy">
                                    I agree to the <a href="{{route('landing.privacyPolicy')}}" target="_blank">&nbsp;privacy policy</a> and 
                                    <a href="{{route('landing.termsAndConditions')}}" target="_blank">terms & conditions</a>, 
                                    also I assure that I will not list any of the banned products mentioned here <a href="{{route('landing.bannedItems')}}" target="_blank">Banned Items.</a>
                                </label>
                            </div>
                        </div>
                        <button class="btn btn-primary btn-block" tabindex="5" id="signup_btn" disabled="true">Sign up</button>
                    </form>
                    <p class="text-center mt-2"><span>Already have an account?</span><a href="{{ route('login')}}"><b><span>&nbsp;Login</span></b></a></p>
                    <div class="text-center">
                    <h5>Go to Website <a href="/"><b>Home</b></a></h5>
                    
                </div>
                </div>
            </div><br>
            <!-- /Register-->
        </div>
    </div>
@endsection
@section('scripts')
@parent
<script src="{{ asset('XR/app-assets/vendors/js/vendors.min.js')}}"></script>
<script>
const togglePassword = document.querySelector('#togglePassword');
const toggleConfirmPassword = document.querySelector('#toggleConfirmPassword');
const password = document.querySelector('#password');
const confirmPassword = document.querySelector('#confirm_password');
togglePassword.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);
    // toggle the eye slash icon
    this.classList.toggle('fa-eye-slash');
});

toggleConfirmPassword.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = confirmPassword.getAttribute('type') === 'password' ? 'text' : 'password';
    confirmPassword.setAttribute('type', type);
    // toggle the eye slash icon
    this.classList.toggle('fa-eye-slash');
});

function validatePassword(){
  if(password.value != confirm_password.value) {
    confirm_password.setCustomValidity("Passwords Don't Match");
  } else {
    confirm_password.setCustomValidity('');
  }
}

password.onchange = validatePassword;
confirm_password.onkeyup = validatePassword;

$(document).ready(function() {
    $('#register-privacy-policy').change(function() {
        if(this.checked) {
            $("#signup_btn").prop('disabled',false);
        }
        else{
            $("#signup_btn").prop('disabled',true);
        }
    });
    // $('#google').mouseover(function(){
    //     $("#google").css("background-color", "blue");
    // });
    // $('#google').mouseout(function(){
    //     $("#google").css("background-color", "white");
    // });
});
</script>
@endsection