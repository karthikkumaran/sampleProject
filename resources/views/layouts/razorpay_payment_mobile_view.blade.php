<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="keywords" content="Simplisell, simple, simpli, simply, simplysell, sell, ecommerce, store, shop, shopping, product, products, catalogue, catalogues">
	<meta name="description" content="{{ trans('panel.site_desc') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon.png') }}">

</head>
<body>
</body>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>

    $( document ).ready(function() {
        let response = {
            "key": "{{$response['razorpayId']}}", // Enter the Key ID generated from the Dashboard
            "amount": "{{$response['amount']}}", // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
            "currency": "{{$response['currency']}}",
            "name": "{{$response['name']}}",
            "description": "{{$response['description']}}",
            "order_id": "{{$response['orderId']}}", //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
            "name": "{{$response['name']}}",
            "email": "{{$response['email']}}",
            "contact": "{{$response['contactNumber']}}",
            "address": "{{$response['address']}}"
        }
        // console.log(options);
        payment(response);
        
    });


  

  function payment(response){
    var options = {
        "key": response['key'], // Enter the Key ID generated from the Dashboard
        "amount": response['amount'], // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
        "currency": response['currency'],
        "name": response['name'],
        "description": response['description'],
        "order_id": response['order_id'], //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
        "handler": function(response) {
            //data = data + "&rzp_paymentid=" + response.razorpay_payment_id + "&rzp_orderid=" + response.razorpay_order_id + "&rzp_signature=" + response.razorpay_signature + "&status=success";
            window.razorpay_webview.postMessage("rzp_paymentid=" + response.razorpay_payment_id + "&rzp_orderid=" + response.razorpay_order_id + "&rzp_signature=" + response.razorpay_signature + "&status=success" );
            //console.log(response)
            return response;
        },
        "prefill": {
            "name": response['name'],
            "email": response['email'],
            "contact": response['contact']
        },
        "notes": {
            "address": response['address']
        },
        "theme": {
            "color": "#528FF0"
        }
    };
    //console.log(options);

    var rzp1 = new Razorpay(options);
    rzp1.open();

    rzp1.on('payment.failed', function(response) {
        window.razorpay_webview.postMessage(response);
        //console.log(response)
        return response;
    });
  }
</script>

</html>