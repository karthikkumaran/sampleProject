<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
@php
$userSettings = auth()->user()->userInfo;
$meta_data = json_decode($userSettings->meta_data, true);
@endphp
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ trans('panel.site_title') }} - {{ trans('panel.site_tagline') }}</title>
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon.png') }}">

     <!-- BEGIN: Vendor CSS-->
     <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/vendors.min.css') }}">
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/charts/apexcharts.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/extensions/tether-theme-arrows.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/extensions/tether.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/extensions/shepherd-theme-default.css') }}"> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/animate/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/extensions/sweetalert2.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/forms/select/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/ui/prism.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/file-uploaders/dropzone.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/editors/quill/quill.snow.css')}}">
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" rel="stylesheet" /> -->
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/components.css') }}">
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/themes/dark-layout.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/themes/semi-dark-layout.css') }}"> -->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/core/colors/palette-gradient.css') }}">
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/pages/dashboard-analytics.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/pages/card-analytics.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/plugins/tour/tour.css') }}"> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/extensions/toastr.css') }}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/assets/css/style.css') }}">
    <link href="{{ asset('XR/bootstrap4-editable/css/bootstrap-editable.css') }}" rel="stylesheet"/>
    <!-- END: Custom CSS-->
    <style>
        .editable-buttons {
            display: block !important;
            margin-top: 5px;
        }
        nav .pagination {
            float: right;
        }
        #loading-bg {
            background: #e9e9e9;
            position: absolute;
            top: 0;
            left: 0;
            width: 100vw;
            height: 100vh;
            opacity: 0.5;
            z-index:1040;
            overflow: hidden;
        }
        .loading-logo img {
            position: absolute;
            height:30px;
            width: 30px;
            left: calc(50% - 25px);
            top: 52%;
        }
        .loading1 {
            position: absolute;
            left: calc(50% - 35px);
            top: 50%;
            width: 55px;
            height: 55px;
            /* border-radius: 50%;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            border: 3px solid transparent; */
        }
        
        .text-blue{
            color: #3AADD8;
        }
        .text-red {
            color: #DB1F26;
        }
    </style>

    @yield('styles')
    
    @PWA
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-65PERJ2GMK"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-65PERJ2GMK');
    </script>
</head>

<body class="vertical-layout vertical-menu-modern 2-columns  navbar-floating footer-static" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
    <div id="loading-bg" style="display:none;">
        <div class="loading-logo">
            <img src="{{asset('favicon.png')}}">
        </div>

        <div class="loading1">
            <!-- <div class="spinner-grow text-danger w-100 h-100" role="status">
                <span class="sr-only">Loading...</span>
            </div> -->
            <div class="spinner-{{ $meta_data['splash']['loader_type'] ?? 'border' }} w-100 h-100"
                style="color: {{ $meta_data['splash']['loader_color'] ?? '' }}" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>
    <nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="navbar-collapse" id="navbar-mobile">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                        @include('partials.header_left_menu')
                    </div>
                    @if((auth()->user()->getIsAdminAttribute())== true)
                    @else
                        <ul class="nav navbar-nav float-right"> 
                            
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </nav>
    
     <!-- BEGIN: Main Menu-->
     <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="#">
                        <!-- <div class="brand-logo"></div> -->
                        <img src="{{asset('XR/app-assets/images/logo/logo.png')}}" id="expand_logo" style="margin-top: -1rem;width: 180px;"/>
                        <img src="{{asset('XR/app-assets/images/logo/logo-mini.png')}}" id="collapse_logo" style="margin-left: -18px;width:60px;display:none"/>
                        <!-- <h2 class="brand-text mb-0">{{ trans('panel.site_title') }}</h2> -->
                    </a></li>
                <li class="nav-item nav-toggle">
                    <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
                        <i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon"></i>
                        <i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="icon-disc"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        <div class="main-menu-content">
            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                @if(app('impersonate')->isImpersonating())
                @else
                <li>
                    <a href="#" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                        <i class="feather icon-log-out"></i>
                        <span class="menu-item">{{ trans('global.logout') }}</span>
                    </a>
                </li>
                @endif

                @impersonating
                <li>
                    <a href="{{ route('admin.users.stopimpersonate') }}">
                        <i class="fa fa-stop-circle"></i>
                        <span class="menu-item">Stop Impersonating</span>
                    </a>
                </li>
                @endImpersonating
            </ul>
        </div>
    </div>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
        <div class="content-overlay"></div>
            <div class="header-navbar-shadow"></div>
            <div class="content-wrapper">
                <div class="content-header row">
                </div>
                <div class="content-body">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>
    <!-- END: Content-->
   
    <form id="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    @include('partials.adminfooter')
    <!-- END: Footer-->

    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('XR/app-assets/vendors/js/vendors.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <script src="{{ asset('XR/bootstrap4-editable/js/bootstrap-editable.js') }}"></script>

    <!-- BEGIN: Page Vendor JS-->
    <!-- <script src="{{ asset('XR/app-assets/vendors/js/charts/apexcharts.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/extensions/tether.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/extensions/shepherd.min.js') }}"></script> -->

    <script src="{{asset('XR/app-assets/vendors/js/extensions/sweetalert2.all.min.js')}}"></script>
    
    <script src="{{asset('XR/app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
   
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>

    <script src="{{ asset('XR/app-assets/vendors/js/editors/quill/quill.min.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>

    <script src="{{ asset('XR/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('XR/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('XR/app-assets/js/core/app.js') }}"></script>
    <script src="{{ asset('XR/app-assets/js/scripts/components.js') }}"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <!-- END: Theme JS-->
    <script>
        $(function() {
            let copyButtonTrans = '{{ trans('global.datatables.copy') }}'
            let csvButtonTrans = '{{ trans('global.datatables.csv') }}'
            let excelButtonTrans = '{{ trans('global.datatables.excel') }}'
            let pdfButtonTrans = '{{ trans('global.datatables.pdf') }}'
            let printButtonTrans = '{{ trans('global.datatables.print') }}'
            let colvisButtonTrans = '{{ trans('global.datatables.colvis') }}'
            let selectAllButtonTrans = '{{ trans('global.select_all') }}'
            let selectNoneButtonTrans = '{{ trans('global.deselect_all') }}'
            let languages = {
                // 'en': 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/English.json'
            };

            $.extend(true, $.fn.dataTable.Button.defaults.dom.button, { className: 'btn' })
            $.extend(true, $.fn.dataTable.defaults, {
            language: {
                url: languages['{{ app()->getLocale() }}']
            },
            columnDefs: [{
                orderable: false,
                className: 'select-checkbox',
                targets: 0
            }, {
                orderable: false,
                searchable: false,
                targets: -1
            }],
            select: {
                style:    'multi+shift',
                selector: 'td:first-child'
            },
            order: [],
            scrollX: true,
            pageLength: 100,
            dom: 'lBfrtip<"actions">',
            buttons: [
                {
                    extend: 'selectAll',
                    className: 'btn-primary',
                    text: selectAllButtonTrans,
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'selectNone',
                    className: 'btn-primary',
                    text: selectNoneButtonTrans,
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'copy',
                    className: 'btn-default',
                    text: copyButtonTrans,
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'csv',
                    className: 'btn-default',
                    text: csvButtonTrans,
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excel',
                    className: 'btn-default',
                    text: excelButtonTrans,
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdf',
                    className: 'btn-default',
                    text: pdfButtonTrans,
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'print',
                    className: 'btn-default',
                    text: printButtonTrans,
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'colvis',
                    className: 'btn-default',
                    text: colvisButtonTrans,
                    exportOptions: {
                        columns: ':visible'
                    }
                }
            ]
            });

            $.fn.dataTable.ext.classes.sPageButton = '';
        });
    </script>
    <script>
        $(document).ready(function () {
            $(".notifications-menu").on('click', function () {
                if (!$(this).hasClass('open')) {
                    $('.notifications-menu .label-warning').hide();
                    $.get('/admin/user-alerts/read');
                }
            });
        });
    
        var csrf_token = "{{csrf_token()}}";
    
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            error: function (jqXHR, exception) {
                if (jqXHR.status === 0) {
                    Swal.fire('You are offline', 'Verify your internet connectivity.', 'error');
                } else if (jqXHR.status == 401) {
                    window.location = "/login";
                } else if (jqXHR.status == 404) {
                    Swal.fire('Requested page not found', '404', 'error');
                } else if (jqXHR.status == 500) {
                    Swal.fire('Internal Server Error', '500', 'error');
                } else if (exception === 'parsererror') {
                    Swal.fire('Requested JSON parse failed', 'JSON Error', 'error');
                } else if (exception === 'timeout') {
                    Swal.fire('Time out error', '', 'error');
                } else if (exception === 'abort') {
                    Swal.fire('Ajax request aborted', '', 'error');
                } else {
                    var err = eval("(" + jqXHR.responseText + ")");
                    if(err.errors.code != undefined)
                        Swal.fire('Error!', err.errors.code, 'error');
                    else if(err.errors.name != undefined)
                        Swal.fire('Error!', err.errors.name, 'error');
                    else if(err.message != undefined)
                        Swal.fire('Error!', err.message, 'error');
                    else
                        Swal.fire('Error!', err, 'error');
                }
            }
        });

        //lazy loading
        if ('loading' in HTMLImageElement.prototype) {
            const images = document.querySelectorAll("img.lazyload");
            images.forEach(img => {
                img.src = img.dataset.src;
            });
        }
        else {
            // Dynamically import the LazySizes library
            let script = document.createElement("script");
            script.async = true;
            script.src =
            "https://cdnjs.cloudflare.com/ajax/libs/lazysizes/4.1.8/lazysizes.min.js";
            document.body.appendChild(script);
        }
    </script>

    @yield('scripts')

</body>
</html>