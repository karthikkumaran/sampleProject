<!DOCTYPE html>
<html class="loaded" lang="en" data-textdirection="ltr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $meta_data['splash']['brand_name'] ?? trans('panel.site_title') }}</title>

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/vendors.min.css') }}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/components.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/extensions/toastr.css') }}">
    <!-- BEGIN: Page CSS-->

    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/assets/css/style.css') }}">
    <!-- END: Custom CSS-->

    @yield('styles')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-65PERJ2GMK"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-65PERJ2GMK');
    </script>
    @if(!empty($meta_data['analytic_settings']['google_analytics_id']))
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-{{$meta_data['analytic_settings']['google_analytics_id']}}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '{{$meta_data['analytic_settings']['google_analytics_id']}}');
        </script>
      @endif
    @if(!empty($meta_data['custom_script_settings']['custom_script_header']))
    {!! $meta_data['custom_script_settings']['custom_script_header'] !!}
    @endif
</head>

<!-- BEGIN: Body-->

<body oncontextmenu="return false" class="vertical-layout vertical-menu-modern 1-column  navbar-floating footer-static" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N67BLVX" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @if(!empty($meta_data['analytic_settings']['google_analytics_gtm_id']))
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{$meta_data['analytic_settings']['google_analytics_gtm_id']}}" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    @endif
    <!-- BEGIN: Content-->
    @if(!empty($userSettings))
        @include('partials.splash')
    @endif
    <div class="app-content content">
        <div class="content-overlay"></div>
        <!-- <div class="header-navbar-shadow"></div> -->
        <div class="mt-0 pt-0">
            @yield("content")
        </div>
    </div>
    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('XR/app-assets/vendors/js/vendors.min.js') }}"></script>
    <script src="{{asset('XR/app-assets/vendors/js/extensions/sweetalert2.all.min.js')}}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js"></script>
    <!-- <script src="{{ asset('XR/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('XR/app-assets/js/core/app.js') }}"></script> -->
    <!-- BEGIN Vendor JS-->
    <script>
        $(window).on('load', function() {
            // setTimeout(removeLoader, 3000); //wait for page load PLUS two seconds.
        });

        function removeLoader() {
            $("#loading-bg").fadeOut(500, function() {
                // fadeOut complete. Remove the loading div
                $("#loading-bg").remove(); //makes page more lightweight 
            });
        }

        var csrf_token = "{{csrf_token()}}";

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            error: function(jqXHR, exception) {
                if (jqXHR.status === 0) {
                    Swal.fire('You lost connectivity', 'Verify your internet connection.', 'error');
                } else if (jqXHR.status == 404) {
                    Swal.fire('Requested page not found', '404', 'error');
                } else if (jqXHR.status == 500) {
                    Swal.fire('Internal Server Error', '500', 'error');
                } else if (exception === 'parsererror') {
                    Swal.fire('Requested JSON parse failed', 'JSON Error', 'error');
                } else if (exception === 'timeout') {
                    Swal.fire('Time out error', '', 'error');
                } else if (exception === 'abort') {
                    Swal.fire('Ajax request aborted', '', 'error');
                } else {
                    var err = eval("(" + jqXHR.responseText + ")");
                    if (err.errors.code != undefined)
                        Swal.fire('Error!', err.errors.code, 'error');
                    else if (err.errors.name != undefined)
                        Swal.fire('Error!', err.errors.name, 'error');
                    else if (err.message != undefined)
                        Swal.fire('Error!', err.message, 'error');
                    else
                        Swal.fire('Error!', err, 'error');
                }
            }
        });
        $(".niceScroll").niceScroll({
            cursorcolor: "grey", // change cursor color in hex
            cursoropacitymin: 0, // change opacity when cursor is inactive (scrollabar "hidden" state), range from 1 to 0
            cursoropacitymax: 1, // change opacity when cursor is active (scrollabar "visible" state), range from 1 to 0
            cursorwidth: "5px", // cursor width in pixel (you can also write "5px")
            cursorborder: "1px solid #fff", // css definition for cursor border
            cursorborderradius: "5px", // border radius in pixel for cursor
            zindex: "auto", // change z-index for scrollbar div
            scrollspeed: 60, // scrolling speed
            mousescrollstep: 40, // scrolling speed with mouse wheel (pixel)
            touchbehavior: false, // enable cursor-drag scrolling like touch devices in desktop computer
            hwacceleration: true, // use hardware accelerated scroll when supported
            boxzoom: false, // enable zoom for box content
            dblclickzoom: true, // (only when boxzoom=true) zoom activated when double click on box
            gesturezoom: true, // (only when boxzoom=true and with touch devices) zoom activated when pinch out/in on box
            grabcursorenabled: true, // (only when touchbehavior=true) display "grab" icon
            autohidemode: true, // how hide the scrollbar works, possible values: 
            background: "", // change css for rail background
            iframeautoresize: true, // autoresize iframe on load event
            cursorminheight: 32, // set the minimum cursor height (pixel)
            preservenativescrolling: true, // you can scroll native scrollable areas with mouse, bubbling mouse wheel event
            railoffset: false, // you can add offset top/left for rail position
            bouncescroll: false, // (only hw accell) enable scroll bouncing at the end of content as mobile-like 
            spacebarenabled: true, // enable page down scrolling when space bar has pressed
            disableoutline: true, // for chrome browser, disable outline (orange highlight) when selecting a div with nicescroll
            horizrailenabled: true, // nicescroll can manage horizontal scroll
            railalign: "right", // alignment of vertical rail
            railvalign: "bottom", // alignment of horizontal rail
            enabletranslate3d: true, // nicescroll can use css translate to scroll content
            enablemousewheel: true, // nicescroll can manage mouse wheel events
            enablekeyboard: true, // nicescroll can manage keyboard events
            smoothscroll: true, // scroll with ease movement
            sensitiverail: true, // click on rail make a scroll
            enablemouselockapi: true, // can use mouse caption lock API (same issue on object dragging)
            cursorfixedheight: false, // set fixed height for cursor in pixel
            hidecursordelay: 400, // set the delay in microseconds to fading out scrollbars
            irectionlockdeadzone: 6, // dead zone in pixels for direction lock activation
            nativeparentscrolling: true, // detect bottom of content and let parent to scroll, as native scroll does
            enablescrollonselection: true, // enable auto-scrolling of content when selection text
            cursordragspeed: 0.3, // speed of selection when dragged with cursor
            rtlmode: "auto", // horizontal div scrolling starts at left side
            cursordragontouch: false, // drag cursor in touch / touchbehavior mode also
            oneaxismousemode: "auto",
            scriptpath: "", // define custom path for boxmode icons ("" => same script path)
            preventmultitouchscrolling: true, // prevent scrolling on multitouch events
            disablemutationobserver: false,
        });
        //lazy loading
        if ('loading' in HTMLImageElement.prototype) {
            const images = document.querySelectorAll("img.lazyload");
            images.forEach(img => {
                img.src = img.dataset.src;
            });
        } else {
            // Dynamically import the LazySizes library
            let script = document.createElement("script");
            script.async = true;
            script.src =
                "https://cdnjs.cloudflare.com/ajax/libs/lazysizes/4.1.8/lazysizes.min.js";
            document.body.appendChild(script);
        }

        function track_events(event_name, product_id, slink) {
            $.ajax({
                url: "{{route('admin.stats.ajaxEvents')}}",
                type: "post",
                data: {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    "event": event_name,
                    "slink": slink,
                    "product_id": product_id,
                },
            });
        }
       
      window.onload = function () {
          document.addEventListener("contextmenu", function (e) {
              e.preventDefault();
          }, false);
          document.addEventListener("keydown", function (e) {
              //document.onkeydown = function(e) {
              // "I" key
              if (e.ctrlKey && e.shiftKey && e.keyCode == 73) {
                  disabledEvent(e);
              }
              // "J" key
              if (e.ctrlKey && e.shiftKey && e.keyCode == 74) {
                  disabledEvent(e);
              }
              // "S" key + macOS
              if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
                  disabledEvent(e);
              }
              // "U" key
              if (e.ctrlKey && e.keyCode == 85) {
                  disabledEvent(e);
              }
              // "F12" key
              if (event.keyCode == 123) {
                  disabledEvent(e);
              }
          }, false);
          function disabledEvent(e) {
              if (e.stopPropagation) {
                  e.stopPropagation();
              } else if (window.event) {
                  window.event.cancelBubble = true;
              }
              e.preventDefault();
              return false;
          }
      }
</script>
    @yield('scripts')
    @if(!empty($meta_data['custom_script_settings']['custom_script_footer']))
    {!! $meta_data['custom_script_settings']['custom_script_footer'] !!}
    @endif
</body>

</html>