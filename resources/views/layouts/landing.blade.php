<!DOCTYPE html>
<html class="no-js" lang="en">
@php
\Debugbar::disable();
if ( app('impersonate')->isImpersonating() ||(auth()->user() && auth()->user()->getIsAdminAttribute())) {
\Debugbar::enable();
}
@endphp
	<head>
		<title>{{ trans('panel.site_title') }} - {{ trans('panel.site_tagline') }}</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="user-scalable=no, width=device-width, height=device-height, initial-scale=1, maximum-scale=1, minimum-scale=1, minimal-ui" />

		<meta name="theme-color" content="#341880" />
		<meta name="msapplication-navbutton-color" content="#341880" />
		<meta name="apple-mobile-web-app-status-bar-style" content="#341880" />

		<meta name="keywords" content="Simplisell, simple, simpli, simply, simplysell, sell, ecommerce, store, shop, shopping, product, products, catalogue, catalogues">
		<meta name="description" content="{{ trans('panel.site_desc') }}">

		<meta name="twitter:title"content="{{ trans('panel.site_title') }}" />
		<meta name="twitter:card" content="{{ trans('panel.site_desc') }}" />
		<meta name="twitter:creator" content="@simplisell" />

		<meta property="og:description"content="{{ trans('panel.site_desc') }}" />
		<meta property="og:title"content="{{ trans('panel.site_title') }}" />
		<meta property="og:url"content="https://simplisell.co/" />
		<meta property="og:site_name"content="{{ trans('panel.site_title') }}" />
		<meta name="og:site_name" content="{{ trans('panel.site_title') }}" />
		<meta name="og:locale" content="en-us" />

		@if(env('SITE_URL') == "simplisell.co")
			<link rel="canonical" href="{{url()->current()}}"/>
		@else
			<link rel="canonical" href="https://simplisell.co/"/>
		@endif
		<!-- Favicons
		================================================== -->
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon.png') }}">
		<link rel="shortcut icon" href="{{ asset('favicon.png') }}">
		<link rel="apple-touch-icon" href="{{ asset('favicon.png') }}">
		<link rel="apple-touch-icon" sizes="72x72" href="{{ asset('favicon.png') }}">
		<link rel="apple-touch-icon" sizes="114x114" href="{{ asset('favicon.png') }}">

		<!-- Critical styles
		================================================== -->
		<link rel="stylesheet" href="{{ asset('landing/css/critical.min.css') }}" type="text/css">
		<style>
			.s__price {
				font-size: 3rem !important;
			}
			.s__price sup {
				font-size: 2.5rem !important;
			}
		</style>
		<!-- Load google font
		================================================== -->
		<script type="text/javascript">
			WebFontConfig = {
				google: { families: [ 'Nunito+Sans:400,400i,700,700i,800,800i,900,900i', 'Quicksand:300,400,700'] }
			};
			(function() {
				var wf = document.createElement('script');
				wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
				wf.type = 'text/javascript';
				wf.async = 'true';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(wf, s);
			})();
		</script>

		<!-- Load other scripts
		================================================== -->
		<script type="text/javascript">
			var _html = document.documentElement,
				isTouch = (('ontouchstart' in _html) || (navigator.msMaxTouchPoints > 0) || (navigator.maxTouchPoints));

			_html.className = _html.className.replace("no-js","js");
			_html.classList.add( isTouch ? "touch" : "no-touch");
		</script>
		<script type="text/javascript" src="{{ asset('landing/js/device.min.js') }}"></script>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-65PERJ2GMK"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'G-65PERJ2GMK');
		</script>
	</head>

	<body>
		<div id="app">
            @include('partials.landing_header')
			
			@yield('content')

            @include('partials.landing_footer')
			
		</div>

		<div id="btn-to-top-wrap">
			<a id="btn-to-top" class="circled" href="javascript:void(0);" data-visible-offset="800"></a>
		</div>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="js/jquery-2.2.4.min.js"><\/script>')</script>

		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

		<script type="text/javascript" src="{{ asset('landing/js/main.min.js') }}"></script>
		
	</body>
</html>