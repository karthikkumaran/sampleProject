<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
@php
$user = auth()->user()->getIsAdminAttribute();
$device_token=Auth()->user()->device_token;
$userSettings = auth()->user()->userInfo;
$meta_data = json_decode($userSettings->meta_data, true);

//debuggar bar 

\Debugbar::disable();
if ( app('impersonate')->isImpersonating() || auth()->user()->getIsAdminAttribute()) {
\Debugbar::enable();
}

@endphp
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ trans('panel.site_title') }} - {{ trans('panel.site_tagline') }}</title>

    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon.png') }}">

    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <style>
        *,html,body{
            font-family :  'Poppins', sans-serif;
        }
        </style>
    @livewireStyles
     <!-- BEGIN: Vendor CSS-->
     <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/vendors.min.css') }}">
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/charts/apexcharts.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/extensions/tether-theme-arrows.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/extensions/tether.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/extensions/shepherd-theme-default.css') }}"> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/animate/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/extensions/sweetalert2.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/plugins/forms/wizard.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/forms/select/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/ui/prism.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/file-uploaders/dropzone.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/editors/quill/quill.snow.css')}}">
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" rel="stylesheet" /> -->
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/components.css') }}">
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/themes/dark-layout.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/themes/semi-dark-layout.css') }}"> -->

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/core/colors/palette-gradient.css') }}">
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/pages/dashboard-analytics.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/pages/card-analytics.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/plugins/tour/tour.css') }}"> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/extensions/toastr.css') }}">
    <!-- END: Page CSS-->

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/assets/css/style.css') }}">
    <link href="{{ asset('XR/bootstrap4-editable/css/bootstrap-editable.css') }}" rel="stylesheet"/>
    <!-- END: Custom CSS-->
    <style>
        .editable-buttons {
            display: block !important;
            margin-top: 5px;
        }
        nav .pagination {
            float: right;
        }
        #loading-bg {
            background: #e9e9e9;
            position: absolute;
            top: 0;
            left: 0;
            width: 100vw;
            height: 100vh;
            opacity: 0.5;
            z-index:1040;
            overflow: hidden;
        }
        #loading-bg-modal {
            background: #e9e9e9;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            opacity: 0.75;
            z-index:1040;
            overflow: hidden;
        }
        .loading-logo img {
            position: absolute;
            height:30px;
            width: 30px;
            left: calc(50% - 25px);
            top: 52%;
        }
        .pointer{
            cursor: pointer;
        }
        .marginNotify{
            margin-bottom : 0.5em !important;
        }
        .marginCollapse{
            margin-top : 0.5em !important;
        }
        .loading1 {
            position: absolute;
            left: calc(50% - 35px);
            top: 50%;
            width: 55px;
            height: 55px;
            /* border-radius: 50%;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            border: 3px solid transparent; */
        }
        .text-blue{
            color: #3AADD8;
        }
        .text-red {
            color: #DB1F26;
        }
        .menuTextColor{
            color: #565656;
        }
        /* .fa-2x{
            font-size: 1.8em;
        } */
        .bg-alice{
            background-color: aliceblue;
        }
        .dz-filename {
            display: none !important;
        }
        .dz-details {
            width: auto !important;
            height: auto !important;
            background: none !important;
            padding: 0 !important;
            margin-bottom: auto !important;
        }
    </style>
    <style>
        /* @import url('https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap'); */
        /* ******************* Selection Radio Item */
        .selected-content{
            text-align: center;
            border-radius: 3px;
            box-shadow: 0 2px 4px 0 rgba(219, 215, 215, 0);
            border: solid 2px transparent;
            background: #fff;
            max-width: 180px;
            height: 130px;
            padding: 15px;
            display: grid;
            grid-gap: 15px;
            place-content: center;
            transition: .3s ease-in-out all;
            box-shadow: 0 2px 4px 0 rgb(219 215 215 / 50%);
            border: solid 2px #e6e6e6;
        }

        .selected-content img {
            width: 70px;
                margin: 0 auto;
        }
        .selected-content h4 {
            font-size: 16px;
        letter-spacing: -0.24px;
        text-align: center;
        color: #1f2949;
        }
        .selected-content h5 {
            font-size: 14px;
        line-height: 1.4;
        text-align: center;
        color: #686d73;
        }

        .selected-label{
            position: relative;
        }
        .selected-label input{
            display: none;
        }
        .selected-label .icon{
            width: 20px;
            height: 20px;
            border: solid 2px #e3e3e3;
            border-radius: 50%;
            position: absolute;
            top: 15px;
            left: 15px;
            transition: .3s ease-in-out all;
            transform: scale(1);
            z-index: 1;
        }
        .selected-label .icon:before{
            content: "\f00c";
            position: absolute;
            width: 100%;
            height: 100%;
            font-family: "FontAwesome";
            font-weight: 900;
            font-size: 12px;
            color: #000;
            text-align: center;
            opacity: 0;
            transition: .2s ease-in-out all;
            transform: scale(2);
        }
        .selected-label input:checked + .icon{
            background: #3057d5;
            border-color: #3057d5;
            transform: scale(1.2);
        }
        .selected-label input:checked + .icon:before{
            color: #fff;
            opacity: 1;
            transform: scale(.8);
        }
        .selected-label input:checked ~ .selected-content{
            box-shadow: 0 2px 4px 0 rgba(219, 215, 215, 0.5);
            border: solid 2px #3057d5;
        }
    </style>
    @yield('styles')
    
    @PWA
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-65PERJ2GMK"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-65PERJ2GMK');
    </script>

</head>

<body class="vertical-layout vertical-menu-modern 2-columns  navbar-floating footer-static" data-open="click" data-menu="vertical-menu-modern" data-col="2-columns">
<div id="loading-bg" style="display:none;">
    <div class="loading-logo">
        <img src="{{asset('favicon.png')}}">
    </div>

    <div class="loading1">
        <!-- <div class="spinner-grow text-danger w-100 h-100" role="status">
            <span class="sr-only">Loading...</span>
        </div> -->
        <div class="spinner-{{ $meta_data['splash']['loader_type'] ?? 'border' }} w-100 h-100"
            style="color: {{ $meta_data['splash']['loader_color'] ?? '' }}" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
</div>
<nav class="header-navbar navbar-expand-lg navbar navbar-with-menu floating-nav navbar-light navbar-shadow">
        <div class="navbar-wrapper">
            <div class="navbar-container content">
                <div class="navbar-collapse" id="navbar-mobile">
                    <div class="mr-auto float-left bookmark-wrapper d-flex align-items-center">
                        @include('partials.header_left_menu')
                    </div>
                    @if((auth()->user()->getIsAdminAttribute())== true)
                    @else
                        <ul class="nav navbar-nav float-right"> 
                            @include('partials.header_right_menu')
                        </ul>
                    @endif
                </div>
            </div>
        </div>
    </nav>
    <ul class="main-search-list-defaultlist d-none">
        <li class="d-flex align-items-center"><a class="pb-25" href="#">
                <h6 class="text-primary mb-0">Files</h6>
            </a></li>
        <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a class="d-flex align-items-center justify-content-between w-100" href="#">
                <div class="d-flex">
                    <div class="mr-50"><img src="{{ asset('XR/app-assets/images/icons/xls.png') }}" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Two new item submitted</p><small class="text-muted">Marketing Manager</small>
                    </div>
                </div><small class="search-data-size mr-50 text-muted">&apos;17kb</small>
            </a></li>
        <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a class="d-flex align-items-center justify-content-between w-100" href="#">
                <div class="d-flex">
                    <div class="mr-50"><img src="{{ asset('XR/app-assets/images/icons/jpg.png') }}" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">52 JPG file Generated</p><small class="text-muted">FontEnd Developer</small>
                    </div>
                </div><small class="search-data-size mr-50 text-muted">&apos;11kb</small>
            </a></li>
        <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a class="d-flex align-items-center justify-content-between w-100" href="#">
                <div class="d-flex">
                    <div class="mr-50"><img src="{{ asset('XR/app-assets/images/icons/pdf.png') }}" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">25 PDF File Uploaded</p><small class="text-muted">Digital Marketing Manager</small>
                    </div>
                </div><small class="search-data-size mr-50 text-muted">&apos;150kb</small>
            </a></li>
        <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a class="d-flex align-items-center justify-content-between w-100" href="#">
                <div class="d-flex">
                    <div class="mr-50"><img src="{{ asset('XR/app-assets/images/icons/doc.png') }}" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Anna_Strong.doc</p><small class="text-muted">Web Designer</small>
                    </div>
                </div><small class="search-data-size mr-50 text-muted">&apos;256kb</small>
            </a></li>
        <li class="d-flex align-items-center"><a class="pb-25" href="#">
                <h6 class="text-primary mb-0">Members</h6>
            </a></li>
        <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="#">
                <div class="d-flex align-items-center">
                    <div class="avatar mr-50"><img src="{{ asset('XR/app-assets/images/portrait/small/avatar-s-8.jpg') }}" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">John Doe</p><small class="text-muted">UI designer</small>
                    </div>
                </div>
            </a></li>
        <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="#">
                <div class="d-flex align-items-center">
                    <div class="avatar mr-50"><img src="{{ asset('XR/app-assets/images/portrait/small/avatar-s-1.jpg') }}" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Michal Clark</p><small class="text-muted">FontEnd Developer</small>
                    </div>
                </div>
            </a></li>
        <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="#">
                <div class="d-flex align-items-center">
                    <div class="avatar mr-50"><img src="{{ asset('XR/app-assets/images/portrait/small/avatar-s-14.jpg') }}" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Milena Gibson</p><small class="text-muted">Digital Marketing Manager</small>
                    </div>
                </div>
            </a></li>
        <li class="auto-suggestion d-flex align-items-center cursor-pointer"><a class="d-flex align-items-center justify-content-between py-50 w-100" href="#">
                <div class="d-flex align-items-center">
                    <div class="avatar mr-50"><img src="{{ asset('XR/app-assets/images/portrait/small/avatar-s-6.jpg') }}" alt="png" height="32"></div>
                    <div class="search-data">
                        <p class="search-data-title mb-0">Anna Strong</p><small class="text-muted">Web Designer</small>
                    </div>
                </div>
            </a></li>
    </ul>
    <ul class="main-search-list-defaultlist-other-list d-none">
        <li class="auto-suggestion d-flex align-items-center justify-content-between cursor-pointer"><a class="d-flex align-items-center justify-content-between w-100 py-50">
                <div class="d-flex justify-content-start"><span class="mr-75 feather icon-alert-circle"></span><span>No results found.</span></div>
            </a></li>
    </ul>
    <!-- END: Header-->
     <!-- BEGIN: Main Menu-->
     <div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
        <div class="navbar-header ">
            <ul class="nav navbar-nav flex-row">
                <li class="nav-item mr-auto"><a class="navbar-brand" href="#">
                        <!-- <div class="brand-logo"></div> -->
                        <img src="{{asset('XR/app-assets/images/logo/logo.png')}}" id="expand_logo" style="margin-top: -1rem;width: 180px;"/>
                        <img src="{{asset('XR/app-assets/images/logo/logo-mini.png')}}" id="collapse_logo" style="margin-left: -18px;width:60px;display:none"/>
                        <!-- <h2 class="brand-text mb-0">{{ trans('panel.site_title') }}</h2> -->
                    </a></li>
                <li class="nav-item nav-toggle">
                    <a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse">
                    <i class="feather icon-x d-block d-xl-none font-medium-4 primary toggle-icon" style="display:none;"></i>
                    <i class="toggle-icon feather icon-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" id="icondisc" data-ticon="icon-disc" style="display:none !important;"></i>
                </a></li>
            </ul>
        </div>
        <div class="shadow-bottom"></div>
        @include('partials.menu')
        <div class="navbar-header bg-alice">
            <div class=" pr-1 pb-1">
                <div class="row ">
                    <div class="col-12">
                        <ul class="nav navbar-nav flex-row marginNotify">
                            <li class="nav-item mr-auto ">
                                <a href="{{ route("admin.users.accountSettings") }}">
                                <!-- <i class="fa fa-user-circle fa-2x" style="color:black;"></i> -->
                                <img src="{{asset('XR/app-assets/images/icons/userprofile.png')}}"  alt="My Profile" style="height: 26px;width:26px;"/>
                                <!-- <b style="color:black;">Profile</b> -->
                                </a>
                            </li>
                            <li class="nav-item ml-auto ">
                                {{--@if((auth()->user()->getIsAdminAttribute())== true)--}}
                                    <!-- <a href="{{ route("admin.user-alerts.index") }}"> -->
                                        <!-- <i class="fa fa-bell fa-2x" style="color:black;"></i> -->
                                <img src="{{asset('XR/app-assets/images/icons/notification.png')}}"  style="height: 22px;width:22px;"/>

                                    <!-- </a> -->
                                {{--@endif--}}
                            </li>
                        </ul>
                    </div>
                
                </div>
            
                        <div class="text-info pointer" id="sidecollapse">
                            <div class="row">
                                <div class="col-12">
                                    <ul class="nav navbar-nav flex-row">
                                    <li class="nav-item ml-1 mr-2 ">
                                        <span id="arrowicon" style="color:black;display:none;"><i class="fa fa-angle-double-right fa-2x" ></i> </span>
                                    </li>
                                    </ul> 
            <hr class="p-0 m-0"/>
                            <div id="arrowtexticon "  style="color:black;">
                                <ul class="nav navbar-nav flex-row">
                                    <li class="nav-item ml-1 mr-2 marginCollapse">
                                    <i class="fa fa-angle-double-left fa-2x "></i>
                                    </li>
                                    <li class="nav-item mr-auto mt-auto">
                                    <h5 class="pt-1">Collapse Sidebar </h5>
                                    </li>
                                </ul>
                            </div>
                                </div>
                            </div>
                        </div>
                
            </div>
        </div>
        <div class="navbar-header bg-alice">
        </div>
        
    </div>
    <!-- END: Main Menu-->

    <!-- BEGIN: Content-->
    <div class="app-content content">
    <div id="aboutModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title custom-modal-title float-left">SimpliSell-PWA</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body modal-container">
          <p>An offline-capable SimpliSell app which is a Progressive Web App.</p>

          <p>The app serves the following features:</p>

          <ul>
            <li>Write notes which then saved to the browser's <a href="https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage" target="_blank">localStorage</a>.</li>
            <li><a href="https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Developer_guide/Installing" target="_blank">Installable</a> on supported browsers for offline usage.</li>
            <li>"Add To Home Screen" feature on Android supported devices to launch the app from the home screen.</li>
            <li>Dark mode.</li>
            <li>Privacy-focused - We'll never collect your precious data.</li>
            <li>Light-weight - Loads almost instantly.</li>
          </ul>
          <link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet">
          
        </div>
        <div class="modal-footer">
        <span class="install-app-btn-container">
            <a id="installApp" class="install-app-btn btn btn-success" href="javascript:void(0);">Install</a>
          </span>
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
        @if((auth()->user()->getIsAdminAttribute())== true)
        @else
           @if(empty($userSettings->subdomain) && empty($meta_data))
    
            @else
                @if(auth()->user()->userInfo->is_start == 1)

                @else
                    <h5 class="alert alert-danger  alert-validation-msg " role="alert">
                        <i class="feather icon-info mr-1 align-middle"></i>
                        <span>Your store is not visible to the world, hit the Publish Store button and start getting customers.</span>
                    </h5>
                @endif
            @endif
        @endif
            <div class="content-header row">
            </div>
            <div class="content-body">
                @if(session('subscription_alert_message'))
                    <div class="row mb-2">
                        <div class="col-lg-12">
                            <div class="alert alert-danger" role="alert">
                                {{ session('subscription_alert_message') }} 
                                <a href="{{route('admin.subscriptions.index')}}" class="btn btn-info btn-xs">Upgrade plan</a>
                            </div>
                        </div>
                    </div>
                @endif

                @if(session('subscription_start_alert'))
                    <div class="row mb-2">
                        <div class="col-lg-12">
                            <div class="alert alert-danger" role="alert">
                                {{ session('subscription_start_alert') }} 
                                <!-- <a href="{{route('admin.subscriptions.index')}}" class="btn btn-info btn-xs">Upgrade plan</a> -->
                            </div>
                        </div>
                    </div>
                @endif

                @if(session('no_subscription'))
                    <div class="row mb-2">
                        <div class="col-lg-12">
                            <div class="alert alert-danger" role="alert">
                                {{session('no_subscription')}}
                            </div>
                        </div>
                    </div>
                @endif

                @if(session('subscription_usage_exceeded'))
                    <div class="row mb-2">
                        <div class="col-lg-12">
                            <div class="alert alert-danger" role="alert">
                                {{ session('subscription_usage_exceeded') }} 
                                <a href="{{route('admin.subscriptions.index')}}" class="btn btn-info btn-xs">Upgrade plan</a>
                            </div>
                        </div>
                    </div>
                @endif

                @if(session('subscription_expiry_alert'))
                    <div class="row mb-2">
                        <div class="col-lg-12">
                            <div class="alert alert-danger" role="alert">
                                @if(session('subscription_expiry_alert') > 0)
                                    <span>Your subscription will end in {{session('subscription_expiry_alert')}} days. Please renew your plan at the earliest to continue using our product without interruptions.</span>
                                @else
                                    <span>Your subscription will end today. Please renew your plan at the earliest to continue using our product without interruptions.</span>
                                @endif
                                <a href="{{route('admin.subscriptions.index')}}" class="btn btn-md btn-info">Renew</a>
                            </div>
                        </div>
                    </div>
                @endif

                @if(session('subscription_grace_period_alert'))
                    <div class="row mb-2">
                        <div class="col-lg-12">
                            <div class="alert alert-danger" role="alert">
                                @if(session('subscription_grace_period_alert') > 1)
                                    <p>Your subscription has expired and you are on grace period.</p> <span>Your grace period will end in {{session('subscription_grace_period_alert')}} days. Please renew your plan at the earliest to continue using our product without interruptions.</span>
                                @else
                                    <p>Your subscription has expired and you are on grace period.</p><span> Your grace period will end today. Please renew your plan at the earliest to continue using our product without interruptions.</span>
                                @endif
                                <a href="{{route('admin.subscriptions.index')}}" class="btn btn-md btn-info">Renew</a>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="subscription_alert_msg">
                </div>

                @if(session('user_trial_subscribed'))
                    @if(session('user_trial_subscribed')["trial_subscription"] == true && session('user_trial_subscribed')["trial_duration"] >= 0)
                        <div class="row mb-2">
                            <div class="col-lg-12">
                                <div class="alert alert-danger" role="alert">
                                    @if(session('user_trial_subscribed')["trial_duration"] > 0)
                                        <span>Your free trial will end in {{session('user_trial_subscribed')["trial_duration"]}} days. Please subscribe to a plan to use our product without interruptions.</span>
                                    @else
                                        <span>Your free trial will end today. Please subscribe to a plan to use our product without interruptions.</span>
                                    @endif
                                    <a href="{{route('admin.subscriptions.index')}}" class="btn btn-md btn-info mx-2">Subscribe</a>
                                </div>
                            </div>
                        </div>
                    @endif
                @endif

                @if(session('subscription_expired'))
                    <div class="row mb-2">
                        <div class="col-lg-12">
                            <div class="alert alert-danger" role="alert">{{ session('subscription_expired') }} 
                                <a href="{{route('admin.subscriptions.index')}}"  class="btn btn-md btn-info  mx-2">Subscribe</a> </div>
                            
                        </div>
                    </div>
                @endif

                @if(session('message'))
                        <div class="row mb-2">
                            <div class="col-lg-12">
                                <div class="alert alert-success" role="alert">{{ session('message') }}</div>
                            </div>
                        </div>
                    @endif
                    @if($errors->count() > 0)
                        <div class="alert alert-danger">
                            <ul class="list-unstyled">
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @yield('content')

            </div>
        </div>
    </div>
    @if(auth()->user()->userInfo->is_start == 1)
        @include('modals.storepublished')
    @endif
    @include('modals.create_catalog_modal')
    <!-- END: Content-->

   
    <form id="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>

    <!-- BEGIN: Footer-->
    @include('partials.adminfooter')
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('XR/app-assets/vendors/js/vendors.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <script src="{{ asset('XR/bootstrap4-editable/js/bootstrap-editable.js') }}"></script>

    <!-- BEGIN: Page Vendor JS-->
    <!-- <script src="{{ asset('XR/app-assets/vendors/js/charts/apexcharts.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/extensions/tether.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/extensions/shepherd.min.js') }}"></script> -->

    <script src="{{asset('XR/app-assets/vendors/js/extensions/sweetalert2.all.min.js')}}"></script>
    
    <script src="{{asset('XR/app-assets/vendors/js/extensions/jquery.steps.min.js')}}"></script>
    <script src="{{asset('XR/app-assets/vendors/js/forms/validation/jquery.validate.min.js')}}"></script>
    <script src="{{asset('XR/app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
   
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>

    <script src="{{ asset('XR/app-assets/vendors/js/editors/quill/quill.min.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>

    <script src="{{ asset('XR/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js"></script>
    <!-- END: Page Vendor JS-->

    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('XR/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('XR/app-assets/js/core/app.js') }}"></script>
    <script src="{{ asset('XR/app-assets/js/scripts/components.js') }}"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/common.js') }}"></script>
    <script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-messaging.js"></script>
    <script src="https://www.gstatic.com/firebasejs/8.3.1/firebase.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/easyqrcodejs@4.1.0/dist/easy.qrcode.min.js"></script>

    @livewireScripts

    <!-- END: Theme JS-->

    
    <!-- BEGIN: Page JS-->
    <!-- <script src="{{ asset('XR/app-assets/js/scripts/pages/dashboard-analytics.js') }}"></script> -->
    <!-- END: Page JS -->
     <!-- <script type="text/javascript">

                var pwaInstallBtn = document.getElementById('pwa-install');
                let isInitiatePwa = false;
                pwaInstallBtn.style.display = 'none';

                window.addEventListener('beforeinstallprompt', (e) => {
                  // Prevent the mini-infobar from appearing on mobile
                  e.preventDefault();
                  window.deferredPrompt = e;
                  pwaInstallBtn.style.display = 'block';
                });

                pwaInstallBtn.addEventListener('click', (e) => {
                  // Show the install prompt
                  const deferredPrompt = window.deferredPrompt;
                  deferredPrompt.prompt();
                  // Wait for the user to respond to the prompt
                  deferredPrompt.userChoice.then((choiceResult) => {
                    if (choiceResult.outcome === 'accepted') {
                      console.log('User accepted the install prompt');
                    } else {
                      console.log('User dismissed the install prompt');
                    }
                  });
                });

                window.addEventListener('appinstalled', (evt) => {
                    pwaInstallBtn.style.display = 'none'
                    window.deferredPrompt = null
                  // Log install to analytics
                  // console.log('INSTALL: Success');
                });

            </script> -->
    <script>

function campCreate() {
    $('#createNewCatalog').modal('hide');
    $('#loading-bg').show();
    var name = $('#catalog_name').val();
    var is_start = $('#is_start_2').is(":checked") == true ? 1 : 0;
    var is_private = $('#is_private_2').is(":checked") == true ? 1 : 0;
    var private_password;
    if (is_private == 1) {
        var private_password = $("#private_password_2").val();
    }
    var is_expired = $('#is_expired_2').is(":checked") == true ? 1 : 0;
    var expired_date;
    if (is_expired == 1) {
        expired_date = $("#expired_date_2").val();
    }
    var is_added_home = $("#is_added_home_2").is(":checked") == true ? 1 : 0;
    var type = $('input[name="catalog_type"]:checked').val();
    // alert(name + "\n" + is_start + "\n" + is_private + "\n" + private_password + "\n" + is_expired + "\n" + expired_date + "\n" + is_added_home + '\n' + type);
    $.ajax({
        url: "{{route('admin.campaigns.catalogCreate')}}",
        method: "POST",
        data: { 'type': type, 'name': name, 'is_start': is_start, 'is_private': is_private, 'private_password': private_password, 'is_expired': is_expired, 'expired_date': expired_date, 'is_added_home': is_added_home }
    }).done(function(data) {
        // console.log(data);
        if (isNaN(data)) {
            alertUsageLimitReached(data);
        } else {
            window.location.href = "{{route('admin.campaigns.edit',':id')}}".replace(':id', data);
        }
        $('#loading-bg').hide();
    }).fail(function(jqXHR, ajaxOptions, thrownError) {
        $('#loading-bg').hide();
        Swal.fire('No response from server', '', 'warning');
    });
}

        $(function() {
  let copyButtonTrans = '{{ trans('global.datatables.copy') }}'
  let csvButtonTrans = '{{ trans('global.datatables.csv') }}'
  let excelButtonTrans = '{{ trans('global.datatables.excel') }}'
  let pdfButtonTrans = '{{ trans('global.datatables.pdf') }}'
  let printButtonTrans = '{{ trans('global.datatables.print') }}'
  let colvisButtonTrans = '{{ trans('global.datatables.colvis') }}'
  let selectAllButtonTrans = '{{ trans('global.select_all') }}'
  let selectNoneButtonTrans = '{{ trans('global.deselect_all') }}'

  let languages = {
    // 'en': 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/English.json'
  };

  $.extend(true, $.fn.dataTable.Button.defaults.dom.button, { className: 'btn' })
  $.extend(true, $.fn.dataTable.defaults, {
    language: {
      url: languages['{{ app()->getLocale() }}']
    },
    columnDefs: [{
        orderable: false,
        className: 'select-checkbox',
        targets: 0
    }, {
        orderable: false,
        searchable: false,
        targets: -1
    }],
    select: {
      style:    'multi+shift',
      selector: 'td:first-child'
    },
    order: [],
    scrollX: true,
    pageLength: 100,
    dom: 'lBfrtip<"actions">',
    buttons: [
      {
        extend: 'selectAll',
        className: 'btn-primary',
        text: selectAllButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'selectNone',
        className: 'btn-primary',
        text: selectNoneButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'copy',
        className: 'btn-default',
        text: copyButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'csv',
        className: 'btn-default',
        text: csvButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'excel',
        className: 'btn-default',
        text: excelButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'pdf',
        className: 'btn-default',
        text: pdfButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'print',
        className: 'btn-default',
        text: printButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'colvis',
        className: 'btn-default',
        text: colvisButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      }
    ]
  });

  $.fn.dataTable.ext.classes.sPageButton = '';
});

</script>

<script>
        $(document).ready(function () {
            $(".notifications-menu").on('click', function () {
                if (!$(this).hasClass('open')) {
                    $('.notifications-menu .label-warning').hide();
                    $.get('/admin/user-alerts/read');
                }
            });
            
            if({{auth()->user()->id}} != 1)
            {
                if ('Notification' in window) 
                {
                
                     if( Notification.permission=='default' || (Notification.permission =='granted' ) )
                    {
                        navigator.serviceWorker.register('../firebase-messaging-sw.js');
                        initFirebaseMessagingRegistration()
                    }
                }
            }
        //          console.log(Notification.permission,'Notification' in window);
        //  if (Notification.permission === "granted")
        //  {
        //       @if(empty(Auth()->user()->device_token))
        //           initFirebaseMessagingRegistration();
        //         @endif
        //  }      
    });


var csrf_token = "{{csrf_token()}}";
    
$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
},
error: function (jqXHR, exception) {
    if (jqXHR.status === 0) {
        Swal.fire('You are offline', 'Verify your internet connectivity.', 'error');
    } else if (jqXHR.status == 401) {
        window.location = "/login";
    } else if (jqXHR.status == 404) {
        Swal.fire('Requested page not found', '404', 'error');
    } else if (jqXHR.status == 500) {
        Swal.fire('Internal Server Error', '500', 'error');
    } else if (exception === 'parsererror') {
        Swal.fire('Requested JSON parse failed', 'JSON Error', 'error');
    } else if (exception === 'timeout') {
        Swal.fire('Time out error', '', 'error');
    } else if (exception === 'abort') {
        Swal.fire('Ajax request aborted', '', 'error');
    } else {
        var err = eval("(" + jqXHR.responseText + ")");
        if(err.errors.code != undefined)
            Swal.fire('Error!', err.errors.code, 'error');
        else if(err.errors.name != undefined)
            Swal.fire('Error!', err.errors.name, 'error');
        else if(err.message != undefined)
            Swal.fire('Error!', err.message, 'error');
        else
            Swal.fire('Error!', err, 'error');
    }
}
});

    $('#createNewCatalogForm').submit(function(event) {

        event.preventDefault(); //this will prevent the default submit
        var type = $('input[name="catalog_type"]:checked').val();
        console.log(type);
        if(type == 2){
        // getSubscriptionFeatureUsage('Video catalog');
        }
        else{
        getSubscriptionFeatureUsage('catalogs');
        }
        campCreate()
        // your code here (But not asynchronous code such as Ajax because it does not wait for response and move to next line.)

        // $(this).unbind('submit').submit(); // continue the submit unbind preventDefault
    })
    $(".niceScroll").niceScroll({
    cursorcolor: "grey", // change cursor color in hex
    cursoropacitymin: 0, // change opacity when cursor is inactive (scrollabar "hidden" state), range from 1 to 0
    cursoropacitymax: 1, // change opacity when cursor is active (scrollabar "visible" state), range from 1 to 0
    cursorwidth: "5px", // cursor width in pixel (you can also write "5px")
    cursorborder: "1px solid #fff", // css definition for cursor border
    cursorborderradius: "5px", // border radius in pixel for cursor
    zindex: "auto", // change z-index for scrollbar div
    scrollspeed: 60, // scrolling speed
    mousescrollstep: 40, // scrolling speed with mouse wheel (pixel)
    touchbehavior: false, // enable cursor-drag scrolling like touch devices in desktop computer
    hwacceleration: true, // use hardware accelerated scroll when supported
    boxzoom: false, // enable zoom for box content
    dblclickzoom: true, // (only when boxzoom=true) zoom activated when double click on box
    gesturezoom: true, // (only when boxzoom=true and with touch devices) zoom activated when pinch out/in on box
    grabcursorenabled: true, // (only when touchbehavior=true) display "grab" icon
    autohidemode: true, // how hide the scrollbar works, possible values: 
    background: "", // change css for rail background
    iframeautoresize: true, // autoresize iframe on load event
    cursorminheight: 32, // set the minimum cursor height (pixel)
    preservenativescrolling: true, // you can scroll native scrollable areas with mouse, bubbling mouse wheel event
    railoffset: false, // you can add offset top/left for rail position
    bouncescroll: false, // (only hw accell) enable scroll bouncing at the end of content as mobile-like 
    spacebarenabled: true, // enable page down scrolling when space bar has pressed
    disableoutline: true, // for chrome browser, disable outline (orange highlight) when selecting a div with nicescroll
    horizrailenabled: true, // nicescroll can manage horizontal scroll
    railalign: "right", // alignment of vertical rail
    railvalign: "bottom", // alignment of horizontal rail
    enabletranslate3d: true, // nicescroll can use css translate to scroll content
    enablemousewheel: true, // nicescroll can manage mouse wheel events
    enablekeyboard: true, // nicescroll can manage keyboard events
    smoothscroll: true, // scroll with ease movement
    sensitiverail: true, // click on rail make a scroll
    enablemouselockapi: true, // can use mouse caption lock API (same issue on object dragging)
    cursorfixedheight: false, // set fixed height for cursor in pixel
    hidecursordelay: 400, // set the delay in microseconds to fading out scrollbars
    irectionlockdeadzone: 6, // dead zone in pixels for direction lock activation
    nativeparentscrolling: true, // detect bottom of content and let parent to scroll, as native scroll does
    enablescrollonselection: true, // enable auto-scrolling of content when selection text
    cursordragspeed: 0.3, // speed of selection when dragged with cursor
    rtlmode: "auto", // horizontal div scrolling starts at left side
    cursordragontouch: false, // drag cursor in touch / touchbehavior mode also
    oneaxismousemode: "auto", 
    scriptpath: "", // define custom path for boxmode icons ("" => same script path)
    preventmultitouchscrolling: true,// prevent scrolling on multitouch events
    disablemutationobserver: false,
  });

  function publishStore(id, status) {
    $('#loading-bg').show();
  var url = "{{route('admin.mystore.publish')}}";
  url = url.replace(':id', id);
  var txt = (status == 1) ? "Published" : "Unpublished";
  $.ajax({
      url: url,
      type: "POST",
      data: {
          '_method': 'POST',
          'id': id,
          'status': status
      },
      success: function (response_sub) {
        $('#loading-bg').show();
          Swal.fire({
              type: "success",
              title: txt + '!',
              text: 'Your store has been ' + txt + '.',
              confirmButtonClass: 'btn btn-success',
          }).then((result) => {
              window.location.reload();
              $('#loading-bg').hide();
          });
      }
  });

}

        //lazy loading
        if ('loading' in HTMLImageElement.prototype) {
            const images = document.querySelectorAll("img.lazyload");
            images.forEach(img => {
                img.src = img.dataset.src;
            });
        } else {
            // Dynamically import the LazySizes library
            let script = document.createElement("script");
            script.async = true;
            script.src =
            "https://cdnjs.cloudflare.com/ajax/libs/lazysizes/4.1.8/lazysizes.min.js";
            document.body.appendChild(script);
        }
   
   //notification
   
   var firebaseConfig = {
     apiKey: "AIzaSyCuIegfFzl7QxmU5jNaBKFZHV-Pdv6TPIs",
   authDomain: "simplisell.firebaseapp.com",
   projectId: "simplisell",
   storageBucket: "simplisell.appspot.com",
   messagingSenderId: "198993584705",
   appId: "1:198993584705:web:fcc57013d86757ec465cc0"

/* Test Firebase */
// apiKey: "AIzaSyDlJMBb8cPCOOSZ2KGMRu32g7zgQynAUAU",
//   authDomain: "simplisell-firebase.firebaseapp.com",
//   projectId: "simplisell-firebase",
//   storageBucket: "simplisell-firebase.appspot.com",
//   messagingSenderId: "231552305286",
//   appId: "1:231552305286:web:8705f49f76db096d1ce907",
//   measurementId: "G-E2VPRNK2N5"
  };
    
  firebase.initializeApp(firebaseConfig);
  const messaging = firebase.messaging();

  function initFirebaseMessagingRegistration() {
    var data = [];
    data = {{auth()->user()->id}}
          messaging
          .requestPermission()
           messaging.getToken()
          .then((currentToken)=>{
              if(currentToken)
              {
                $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });

                $.ajax({
                    url: '{{ route("admin.notification.save") }}',
                    type: 'POST',
                    data: {
                        token: currentToken,
                        data:data,
                    },
                    dataType: 'JSON',
                    success: function (response) {
                        console.log('Token saved successfully.');
                    },
                    error: function (err) {
                        console.log('User Chat Token Error'+ err);
                    },
                });
              }
              
            }).catch(function (err) {
              console.log('User Chat Token Error'+ err);
          });
          
   }  
    
  messaging.onMessage(function(payload) {
      const noteTitle = payload.notification.title;
      const noteOptions = {
          body: payload.notification.body,
          icon: payload.notification.icon,
      };
      
      new Notification(noteTitle, noteOptions);
  });

    $('#sidecollapse').click(function(){
        $('#icondisc').click();
        var x = document.getElementById("arrowicon");
        if (x.style.display === "none") {
            x.style.display = "block";
            $('#arrowtexticon').css({'display':'none'});
        } else {
            x.style.display = "none";
            $('#arrowtexticon').css({'display':'block'});
        }
    });
   
</script>
@yield('scripts')
@stack('scripts')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10">
    </script>
   <x-livewire-alert::scripts />
   
</body>

</html>


<style>
    /* .btn1{

        margin-left: 160px;
    } */

</style>