<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
@php
$userSettings = auth()->user()->userInfo;
$meta_data = json_decode($userSettings->meta_data, true);
@endphp
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <!-- BEGIN: Page CSS-->
    
    <!-- END: Page CSS-->

    <title>{{ trans('panel.site_title') }} - {{ trans('panel.site_tagline') }}</title>
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon.png') }}">

     <!-- BEGIN: Vendor CSS-->
     <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/vendors.min.css') }}">
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/charts/apexcharts.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/extensions/tether-theme-arrows.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/extensions/tether.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/extensions/shepherd-theme-default.css') }}"> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/tables/datatable/datatables.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/animate/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/extensions/sweetalert2.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/forms/select/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/ui/prism.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/file-uploaders/dropzone.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/vendors/css/editors/quill/quill.snow.css')}}">
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.css" rel="stylesheet" /> -->
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/components.css') }}">
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/themes/dark-layout.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/themes/semi-dark-layout.css') }}"> -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="{{asset('XR/app-assets/css/pages/app-ecommerce-shop.css')}}">
    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/core/colors/palette-gradient.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/core/menu/menu-types/horizontal-menu.css') }}">
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/pages/dashboard-analytics.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/pages/card-analytics.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/plugins/tour/tour.css') }}"> -->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/extensions/toastr.css') }}">
    <!-- END: Page CSS-->

    

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/assets/css/style.css') }}">
    <link href="{{ asset('XR/bootstrap4-editable/css/bootstrap-editable.css') }}" rel="stylesheet"/>
    <!-- END: Custom CSS-->
    <style>
        .cart-product-discount {
            color: white;
            background-color: #333300; /* Changing background color */
            font-weight: bold; /* Making font bold */
            border-radius: 15px; /* Making border radius */
            width: auto; /* Making auto-sizable width */
            height: auto; /* Making auto-sizable height */
            padding: 2px 30px 2px 30px; /* Making space around letters */
            /* font-size: 18px; Changing font size */
        }
        .cart-product-name {
            color: white;
            background-color: #336600; /* Changing background color */
            font-weight: bold; /* Making font bold */
            border-color: white;
            border-radius: 15px; /* Making border radius */
            width: auto; /* Making auto-sizable width */
            height: auto; /* Making auto-sizable height */
            padding: 2px 10px 2px 10px; /* Making space around letters */
            /* font-size: 18px; Changing font size */
        }
        .editable-buttons {
            display: block !important;
            margin-top: 5px;
        }
        nav .pagination {
            float: right;
        }
        #loading-bg {
            background: #e9e9e9;
            position: absolute;
            top: 0;
            left: 0;
            width: 100vw;
            height: 100vh;
            opacity: 0.5;
            z-index:1040;
            overflow: hidden;
        }
        #loading-bg-pos {
            background: #e9e9e9;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            opacity: 0.75;
            z-index:1040;
            overflow: hidden;
        }
        .loading-logo img {
            position: absolute;
            height:30px;
            width: 30px;
            left: calc(50% - 25px);
            top: 52%;
        }
        .loading1 {
            position: absolute;
            left: calc(50% - 35px);
            top: 50%;
            width: 55px;
            height: 55px;
            /* border-radius: 50%;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            border: 3px solid transparent; */
        }
        </style>

    @yield('styles')
    
    @PWA
    <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-65PERJ2GMK"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-65PERJ2GMK');
  </script>

</head>

<body class="horizontal-layout horizontal-menu 1-column navbar-floating footer-static  " data-open="hover" data-menu="horizontal-menu" data-col="1-column">
    <div id="loading-bg" style="display:none;">
        <div class="loading-logo">
            <img src="{{asset('favicon.png')}}">
        </div>

        <div class="loading1">
            <!-- <div class="spinner-grow text-danger w-100 h-100" role="status">
                <span class="sr-only">Loading...</span>
            </div> -->
            <div class="spinner-{{ $meta_data['splash']['loader_type'] ?? 'border' }} w-100 h-100"
                style="color: {{ $meta_data['splash']['loader_color'] ?? '' }}" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>
    <!-- BEGIN: Content-->
    <div class="app-content content pt-0">
        <div class="content-overlay"></div>
        <div class="content-wrapper p-0">
            <div class="content-header row m-0">
                @yield('header')
            </div>
            <div class="content-body" style="height:calc(100vh - 132px)">
                @yield('content')
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN: Footer-->
    <footer class="footer footer-static footer-light" style="padding: 0rem 1rem;">
        @yield('footer')    
    </footer>
    <!-- END: Footer-->


    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('XR/app-assets/vendors/js/vendors.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->

    <script src="{{ asset('XR/bootstrap4-editable/js/bootstrap-editable.js') }}"></script>

    <!-- BEGIN: Page Vendor JS-->
    <!-- <script src="{{ asset('XR/app-assets/vendors/js/charts/apexcharts.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/extensions/tether.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/extensions/shepherd.min.js') }}"></script> -->

    <script src="{{asset('XR/app-assets/vendors/js/extensions/sweetalert2.all.min.js')}}"></script>
    
    <script src="{{asset('XR/app-assets/vendors/js/forms/select/select2.full.min.js')}}"></script>
   
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/vfs_fonts.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/datatables.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>

    <script src="{{ asset('XR/app-assets/vendors/js/editors/quill/quill.min.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.5.1/min/dropzone.min.js"></script>

    <script src="{{ asset('XR/app-assets/vendors/js/extensions/toastr.min.js') }}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js"></script>
    <!-- END: Page Vendor JS-->

    <script src="{{ asset('XR/app-assets/vendors/js/ui/prism.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/extensions/wNumb.js') }}"></script>
    <script src="{{ asset('XR/app-assets/vendors/js/extensions/nouislider.min.js') }}"></script>
    <script src="{{ asset('XR/app-assets/js/scripts/pages/app-ecommerce-shop.js') }}"></script>
    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('XR/app-assets/js/core/app-menu.js') }}"></script>
    <script src="{{ asset('XR/app-assets/js/core/app.js') }}"></script>
    <script src="{{ asset('XR/app-assets/js/scripts/components.js') }}"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/common.js') }}"></script>


    <!-- END: Theme JS-->

    
    <!-- BEGIN: Page JS-->
    <!-- <script src="{{ asset('XR/app-assets/js/scripts/pages/dashboard-analytics.js') }}"></script> -->
    <!-- END: Page JS -->
     <!-- <script type="text/javascript">

                var pwaInstallBtn = document.getElementById('pwa-install');
                let isInitiatePwa = false;
                pwaInstallBtn.style.display = 'none';

                window.addEventListener('beforeinstallprompt', (e) => {
                  // Prevent the mini-infobar from appearing on mobile
                  e.preventDefault();
                  window.deferredPrompt = e;
                  pwaInstallBtn.style.display = 'block';
                });

                pwaInstallBtn.addEventListener('click', (e) => {
                  // Show the install prompt
                  const deferredPrompt = window.deferredPrompt;
                  deferredPrompt.prompt();
                  // Wait for the user to respond to the prompt
                  deferredPrompt.userChoice.then((choiceResult) => {
                    if (choiceResult.outcome === 'accepted') {
                      console.log('User accepted the install prompt');
                    } else {
                      console.log('User dismissed the install prompt');
                    }
                  });
                });

                window.addEventListener('appinstalled', (evt) => {
                    pwaInstallBtn.style.display = 'none'
                    window.deferredPrompt = null
                  // Log install to analytics
                  // console.log('INSTALL: Success');
                });

            </script> -->
    <script>

    $(function() {
  let copyButtonTrans = '{{ trans('global.datatables.copy') }}'
  let csvButtonTrans = '{{ trans('global.datatables.csv') }}'
  let excelButtonTrans = '{{ trans('global.datatables.excel') }}'
  let pdfButtonTrans = '{{ trans('global.datatables.pdf') }}'
  let printButtonTrans = '{{ trans('global.datatables.print') }}'
  let colvisButtonTrans = '{{ trans('global.datatables.colvis') }}'
  let selectAllButtonTrans = '{{ trans('global.select_all') }}'
  let selectNoneButtonTrans = '{{ trans('global.deselect_all') }}'

  let languages = {
    // 'en': 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/English.json'
  };

  $.extend(true, $.fn.dataTable.Button.defaults.dom.button, { className: 'btn' })
  $.extend(true, $.fn.dataTable.defaults, {
    language: {
      url: languages['{{ app()->getLocale() }}']
    },
    columnDefs: [{
        orderable: false,
        className: 'select-checkbox',
        targets: 0
    }, {
        orderable: false,
        searchable: false,
        targets: -1
    }],
    select: {
      style:    'multi+shift',
      selector: 'td:first-child'
    },
    order: [],
    scrollX: true,
    pageLength: 100,
    dom: 'lBfrtip<"actions">',
    buttons: [
      {
        extend: 'selectAll',
        className: 'btn-primary',
        text: selectAllButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'selectNone',
        className: 'btn-primary',
        text: selectNoneButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'copy',
        className: 'btn-default',
        text: copyButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'csv',
        className: 'btn-default',
        text: csvButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'excel',
        className: 'btn-default',
        text: excelButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'pdf',
        className: 'btn-default',
        text: pdfButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'print',
        className: 'btn-default',
        text: printButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'colvis',
        className: 'btn-default',
        text: colvisButtonTrans,
        exportOptions: {
          columns: ':visible'
        }
      }
    ]
  });

  $.fn.dataTable.ext.classes.sPageButton = '';
});

    </script>
    <script>
        $(document).ready(function () {
            $(".notifications-menu").on('click', function () {
                if (!$(this).hasClass('open')) {
                    $('.notifications-menu .label-warning').hide();
                    $.get('/admin/user-alerts/read');
                }
            });
        });


var csrf_token = "{{csrf_token()}}";
    
$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
},
error: function (jqXHR, exception) {
    if (jqXHR.status === 0) {
        Swal.fire('You are offline', 'Verify your internet connectivity.', 'error');
    } else if (jqXHR.status == 401) {
        window.location = "/login";
    } else if (jqXHR.status == 404) {
        Swal.fire('Requested page not found', '404', 'error');
    } else if (jqXHR.status == 500) {
        Swal.fire('Internal Server Error', '500', 'error');
    } else if (exception === 'parsererror') {
        Swal.fire('Requested JSON parse failed', 'JSON Error', 'error');
    } else if (exception === 'timeout') {
        Swal.fire('Time out error', '', 'error');
    } else if (exception === 'abort') {
        Swal.fire('Ajax request aborted', '', 'error');
    } else {
        var err = eval("(" + jqXHR.responseText + ")");
        if(err.errors.code != undefined)
            Swal.fire('Error!', err.errors.code, 'error');
        else if(err.errors.name != undefined)
            Swal.fire('Error!', err.errors.name, 'error');
        else if(err.message != undefined)
            Swal.fire('Error!', err.message, 'error');
        else
            Swal.fire('Error!', err, 'error');
    }
}
});

    $('#createNewCatalogForm').submit(function(event) {

        event.preventDefault(); //this will prevent the default submit
        campCreate()
        // your code here (But not asynchronous code such as Ajax because it does not wait for response and move to next line.)

        // $(this).unbind('submit').submit(); // continue the submit unbind preventDefault
    })
    $(".niceScroll").niceScroll({
    cursorcolor: "grey", // change cursor color in hex
    cursoropacitymin: 0, // change opacity when cursor is inactive (scrollabar "hidden" state), range from 1 to 0
    cursoropacitymax: 1, // change opacity when cursor is active (scrollabar "visible" state), range from 1 to 0
    cursorwidth: "5px", // cursor width in pixel (you can also write "5px")
    cursorborder: "1px solid #fff", // css definition for cursor border
    cursorborderradius: "5px", // border radius in pixel for cursor
    zindex: "auto", // change z-index for scrollbar div
    scrollspeed: 60, // scrolling speed
    mousescrollstep: 40, // scrolling speed with mouse wheel (pixel)
    touchbehavior: false, // enable cursor-drag scrolling like touch devices in desktop computer
    hwacceleration: true, // use hardware accelerated scroll when supported
    boxzoom: false, // enable zoom for box content
    dblclickzoom: true, // (only when boxzoom=true) zoom activated when double click on box
    gesturezoom: true, // (only when boxzoom=true and with touch devices) zoom activated when pinch out/in on box
    grabcursorenabled: true, // (only when touchbehavior=true) display "grab" icon
    autohidemode: true, // how hide the scrollbar works, possible values: 
    background: "", // change css for rail background
    iframeautoresize: true, // autoresize iframe on load event
    cursorminheight: 32, // set the minimum cursor height (pixel)
    preservenativescrolling: true, // you can scroll native scrollable areas with mouse, bubbling mouse wheel event
    railoffset: false, // you can add offset top/left for rail position
    bouncescroll: false, // (only hw accell) enable scroll bouncing at the end of content as mobile-like 
    spacebarenabled: true, // enable page down scrolling when space bar has pressed
    disableoutline: true, // for chrome browser, disable outline (orange highlight) when selecting a div with nicescroll
    horizrailenabled: true, // nicescroll can manage horizontal scroll
    railalign: "right", // alignment of vertical rail
    railvalign: "bottom", // alignment of horizontal rail
    enabletranslate3d: true, // nicescroll can use css translate to scroll content
    enablemousewheel: true, // nicescroll can manage mouse wheel events
    enablekeyboard: true, // nicescroll can manage keyboard events
    smoothscroll: true, // scroll with ease movement
    sensitiverail: true, // click on rail make a scroll
    enablemouselockapi: true, // can use mouse caption lock API (same issue on object dragging)
    cursorfixedheight: false, // set fixed height for cursor in pixel
    hidecursordelay: 400, // set the delay in microseconds to fading out scrollbars
    irectionlockdeadzone: 6, // dead zone in pixels for direction lock activation
    nativeparentscrolling: true, // detect bottom of content and let parent to scroll, as native scroll does
    enablescrollonselection: true, // enable auto-scrolling of content when selection text
    cursordragspeed: 0.3, // speed of selection when dragged with cursor
    rtlmode: "auto", // horizontal div scrolling starts at left side
    cursordragontouch: false, // drag cursor in touch / touchbehavior mode also
    oneaxismousemode: "auto", 
    scriptpath: "", // define custom path for boxmode icons ("" => same script path)
    preventmultitouchscrolling: true,// prevent scrolling on multitouch events
    disablemutationobserver: false,
  });

  function publishStore(id, status) {
    $('#loading-bg').show();
  var url = "{{route('admin.mystore.publish')}}";
  url = url.replace(':id', id);
  var txt = (status == 1) ? "Published" : "Unpublished";
  $.ajax({
      url: url,
      type: "POST",
      data: {
          '_method': 'POST',
          'id': id,
          'status': status
      },
      success: function (response_sub) {
        $('#loading-bg').show();
          Swal.fire({
              type: "success",
              title: txt + '!',
              text: 'Your store has been ' + txt + '.',
              confirmButtonClass: 'btn btn-success',
          }).then((result) => {
              window.location.reload();
              $('#loading-bg').hide();
          });
      }
  });

}


        //lazy loading
        if ('loading' in HTMLImageElement.prototype) {
            const images = document.querySelectorAll("img.lazyload");
            images.forEach(img => {
                img.src = img.dataset.src;
            });
        } else {
            // Dynamically import the LazySizes library
            let script = document.createElement("script");
            script.async = true;
            script.src =
            "https://cdnjs.cloudflare.com/ajax/libs/lazysizes/4.1.8/lazysizes.min.js";
            document.body.appendChild(script);
        }


    </script>
 
    @yield('scripts')
  
</body>

</html>