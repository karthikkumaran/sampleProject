<!DOCTYPE html>
<html lang="en" data-textdirection="ltr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="HandheldFriendly" content="True">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $meta_data['splash']['brand_name'] ?? trans('panel.site_title') }}</title>

    <!-- BEGIN: Custom CSS-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Raleway&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <link href="{{ asset('mirrar-viewar/css/styles.min.css') }}" rel="stylesheet">
    <!-- END: Custom CSS-->

    @yield('styles')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-65PERJ2GMK"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-65PERJ2GMK');
    </script>
    @if(!empty($meta_data['analytic_settings']['google_analytics_id']))
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-{{$meta_data['analytic_settings']['google_analytics_id']}}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '{{$meta_data['analytic_settings']['google_analytics_id']}}');
        </script>
      @endif
    @if(!empty($meta_data['custom_script_settings']['custom_script_header']))
    {!! $meta_data['custom_script_settings']['custom_script_header'] !!}
    @endif
</head>

<!-- BEGIN: Body-->

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N67BLVX" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    @if(!empty($meta_data['analytic_settings']['google_analytics_gtm_id']))
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{$meta_data['analytic_settings']['google_analytics_gtm_id']}}" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    @endif
    <!-- BEGIN: Content-->
    @if(!empty($userSettings))
        @include('partials.splash')
    @endif
   
    @yield("content")
  
    <!-- BEGIN: Vendor JS-->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
  integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  <script type="module" src="{{ asset('mirrar-viewar/js/mirrar-viewar.min.js') }}"></script>
<script src="{{ asset('mirrar-viewar/js/app.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->
    <script>
        $(window).on('load', function() {
            // setTimeout(removeLoader, 3000); //wait for page load PLUS two seconds.
        });

        function removeLoader() {
            $("#loading-bg").fadeOut(500, function() {
                // fadeOut complete. Remove the loading div
                $("#loading-bg").remove(); //makes page more lightweight 
            });
        }

        var csrf_token = "{{csrf_token()}}";

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            error: function(jqXHR, exception) {
                if (jqXHR.status === 0) {
                    Swal.fire('You lost connectivity', 'Verify your internet connection.', 'error');
                } else if (jqXHR.status == 404) {
                    Swal.fire('Requested page not found', '404', 'error');
                } else if (jqXHR.status == 500) {
                    Swal.fire('Internal Server Error', '500', 'error');
                } else if (exception === 'parsererror') {
                    Swal.fire('Requested JSON parse failed', 'JSON Error', 'error');
                } else if (exception === 'timeout') {
                    Swal.fire('Time out error', '', 'error');
                } else if (exception === 'abort') {
                    Swal.fire('Ajax request aborted', '', 'error');
                } else {
                    var err = eval("(" + jqXHR.responseText + ")");
                    if (err.errors.code != undefined)
                        Swal.fire('Error!', err.errors.code, 'error');
                    else if (err.errors.name != undefined)
                        Swal.fire('Error!', err.errors.name, 'error');
                    else if (err.message != undefined)
                        Swal.fire('Error!', err.message, 'error');
                    else
                        Swal.fire('Error!', err, 'error');
                }
            }
        });
        //lazy loading
        if ('loading' in HTMLImageElement.prototype) {
            const images = document.querySelectorAll("img.lazyload");
            images.forEach(img => {
                img.src = img.dataset.src;
            });
        } else {
            // Dynamically import the LazySizes library
            let script = document.createElement("script");
            script.async = true;
            script.src =
                "https://cdnjs.cloudflare.com/ajax/libs/lazysizes/4.1.8/lazysizes.min.js";
            document.body.appendChild(script);
        }

        function track_events(event_name, product_id, slink) {
            $.ajax({
                url: "{{route('admin.stats.ajaxEvents')}}",
                type: "post",
                data: {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    "event": event_name,
                    "slink": slink,
                    "product_id": product_id,
                },
            });
        }
       
      window.onload = function () {
          document.addEventListener("contextmenu", function (e) {
              e.preventDefault();
          }, false);
          document.addEventListener("keydown", function (e) {
              //document.onkeydown = function(e) {
              // "I" key
              if (e.ctrlKey && e.shiftKey && e.keyCode == 73) {
                  disabledEvent(e);
              }
              // "J" key
              if (e.ctrlKey && e.shiftKey && e.keyCode == 74) {
                  disabledEvent(e);
              }
              // "S" key + macOS
              if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
                  disabledEvent(e);
              }
              // "U" key
              if (e.ctrlKey && e.keyCode == 85) {
                  disabledEvent(e);
              }
              // "F12" key
              if (event.keyCode == 123) {
                  disabledEvent(e);
              }
          }, false);
          function disabledEvent(e) {
              if (e.stopPropagation) {
                  e.stopPropagation();
              } else if (window.event) {
                  window.event.cancelBubble = true;
              }
              e.preventDefault();
              return false;
          }
      }
</script>
    @yield('scripts')
    @if(!empty($meta_data['custom_script_settings']['custom_script_footer']))
    {!! $meta_data['custom_script_settings']['custom_script_footer'] !!}
    @endif
</body>

</html>