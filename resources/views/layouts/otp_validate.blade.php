<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Vuexy admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Vuexy admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>{{ trans('panel.site_title') }}</title>
    <link rel="apple-touch-icon" href="../../../app-assets/images/ico/apple-icon-120.png">
    <link rel="shortcut icon" type="image/x-icon" href="../../../app-assets/images/ico/favicon.ico">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,300;0,400;0,500;0,600;1,400;1,500;1,600" rel="stylesheet">

    <!-- BEGIN: Theme CSS-->
    

    <!-- BEGIN: Page CSS-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ trans('panel.site_title') }}</title>
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon.png') }}">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/vendors/css/vendors.min.css') }}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/bootstrap-extended.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/colors.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/app-assets/css/components.css') }}">
    
    <!-- BEGIN: Page CSS-->
    
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('XR/assets/css/style.css') }}">
    <!-- END: Custom CSS-->
    <style>
        .loading1 {
            position: absolute;
            left: calc(50% - 35px);
            top: 50%;
            width: 55px;
            height: 55px;
        }
        #wrapper {
        text-align: center;
        box-sizing: border-box;
        color: #333;
      }
      h3 {
        margin: 0 0 10px;
        padding: 0;
        line-height: 1.25;
      }
      span {
        font-size: 90%;
      }
      input {
        margin: 0 5px;
        text-align: center;
        font-size: 25px;
        border: solid 1px #ccc;
        box-shadow: 0 0 5px #ccc inset;
        outline: none;
        width: 95%;
        transition: all .2s ease-in-out;
        border-radius: 3px;
      }  
      &:focus {
        border-color: purple;
        box-shadow: 0 0 5px purple inset;
      }
      &::selection {
        background: transparent;
      }
      button {
        margin:  30px 0 50px;
        width: 100%;
        padding: 6px;
        background-color: #B85FC6;
        border: none;
        text-transform: uppercase;
      }
      button {
        &.close {
          border: solid 2px;
          border-radius: 30px;
          line-height: 19px;
          font-size: 120%;
          width: 22px;
          position: absolute;
          right: 5px;
          top: 5px;
        }           
      }
      div {
        position: relative;
        z-index: 1;
      }
    </style>
    @yield('styles')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-65PERJ2GMK"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-65PERJ2GMK');
    </script>

</head>

<!-- BEGIN: Body-->

<body class="vertical-layout vertical-menu-modern 1-column  navbar-floating footer-static bg-full-screen-image  blank-page blank-page" data-open="click" data-menu="vertical-menu-modern" data-col="1-column">
    <div class="loading1" style="display:none">
        <!-- <div class="spinner-grow text-danger w-100 h-100" role="status">
                <span class="sr-only">Loading...</span>
            </div> -->
        <div class="spinner-{{ $meta_data['splash']['loader_type'] ?? 'border' }} w-100 h-100" style="color: {{ $meta_data['splash']['loader_color'] ?? '' }}" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
    <!-- BEGIN: Content-->
    <div class="app-content content h-100">
        @yield("content")
    </div>
    @yield('scripts')
    <script src="{{ asset('XR/app-assets/vendors/js/vendors.min.js')}}"></script>
    <!-- BEGIN Vendor JS-->

    <!-- BEGIN: Page Vendor JS-->
    <script src="{{ asset('XR/app-assets/vendors/js/forms/validation/jquery.validate.min.js')}}"></script>
    <!-- END: Page Vendor JS-->
</body>

</html>