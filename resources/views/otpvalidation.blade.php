@extends('layouts.otp_validate')
@section('content')
  <div class="auth-wrapper auth-v2 h-100">
    <div class="auth-inner row m-0 h-100">
      <!-- Left Text-->
      <div class="d-none d-lg-flex col-lg-8 align-items-center p-1">
        <div class="w-100 d-lg-flex align-items-center justify-content-center px-5">
          <img class="img-fluid" src="{{ asset('XR/app-assets/images/pages/register-v2.svg') }}" alt="Register V2" />
        </div>
      </div>
      <!-- /Left Text-->            
      <!-- OTP-->
      <div class="d-flex col-lg-4 align-items-center auth-bg px-2 p-lg-5 bg-white">
        <div class="col-12 col-sm-8 col-md-6 col-lg-12 px-xl-2 mx-auto">
          <div id="wrapper">
            <div id="dialog">
              <img src="{{asset('XR/app-assets/images/logo/logo.png')}}" id="expand_logo" style="margin-top: -1rem;width: 180px;"/>
              <br><br>
              <h3>Please enter the 6-digit verification code we sent via E-mail:</h3>
              <span>(We want to make sure it's you before we allow you inside.)</span>
              <br>
              @if(session('message'))
                <div class="row mt-2">
                  <div class="col-lg-12">
                    <div class="alert alert-success" role="alert">{{ session('message') }}</div>
                  </div>
                </div>
              @endif
              @php
                if(session('user'))
                  $user_id = session('user');
                else
                  $user_id = null;
              @endphp
              @if(session('expired'))
              @else
              <div class="text-center" id="form">
                <div class="alert alert-success" role="alert" id="error" style="display: none;"></div><br>
                <input class="otp_string" type="text" maxLength="6" pattern="[0-9]{6}" required />
                <button type="submit" class="btn btn-primary btn-embossed" onclick="verifyOTP()">Verify</button>
              </div>
              @endif
              <div class="text-center" id="">
                Didn't receive the code?<br/>
                <form action="{{route('otp.resend')}}" method="post">
                @csrf
                  <input type="hidden" value="{{$user_id}}" name="user_id" />
                  <button class="btn btn-link text-capitalize m-0" type="submit">Send code again</button><br/>
                </form>
              </div>
            </div>  
          </div>
        </div>
      </div>
      <!-- /OTP-->
    </div>
  </div>
@endsection
@section('scripts')
@parent
  <script>
    function verifyOTP(){
      var finalString = "";
      var error = document.getElementById("error");
      finalString = $(".otp_string").val();
      if(finalString.length == 6){
        error.style.display = 'none';
        var url = '{{ route("userVerification_otp", ":token") }}';
        url = url.replace(':token', finalString);
        window.location.href = url;
      }
      else{
        error.style.display = 'block';
        error.textContent = "Please enter the OTP" 
      }
    }
</script>
@endsection
