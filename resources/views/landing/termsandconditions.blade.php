@extends('layouts.landing')
@section('content')
<!-- start main -->
<main role="main">
    <!-- Common styles
    ================================================== -->
    <link rel="stylesheet" href="{{ asset('landing/css/style.min.css') }}" type="text/css">

    <!-- Theme styles
    ================================================== -->
    <link rel="stylesheet" href="{{ asset('landing/css/color-7.min.css') }}" type="text/css">
<!-- start section -->
<section class="section">

					<div class="container">
						<div class="row">
							<div class="col-12">

								<div class="content-container">
								<div class="section-heading">
                                    <h2 class="__title">Terms and Conditions</h2>
                                   <p>EFFECTIVE DATE: MARCH 27, 2021</p>
                                </div>	
									<p>
                                    These terms and conditions (“Terms”) and the privacy policy (as available at <a href="{{route('landing.privacyPolicy')}}">simplisell.co/privacy</a>) (“Privacy Policy”) (collectively “User Agreement”) is an electronic record in terms of Information Technology Act, 2000 (“IT Act”) and rules there under, as applicable, and the amended provisions pertaining to electronic records in various statutes as amended from time to time.
                                    </p>
                                    <p>This User Agreement is published in accordance with the provisions of Rule 3 (1) of the Information Technology (Intermediaries Guidelines) Rules, 2011 that require publishing the rules and regulations, privacy policy and terms and conditions for access or usage of the website <b>Simplisell.co</b> (“Website”) and its mobile application (hereinafter referred to as the “Platform”), run by MirrAR Innovation Technology Private Limited, a company incorporated under the laws of India and its registered office at A-6, Industrial Estate, Mogappair West, Chennai - 37</p>
                                    <div class="spacer py-4"></div>

                                    <div class="section-heading">
                                        <h4 class="__title">General</h4>
                                    </div>	
                                <p>For the purposes of these Terms, ‘User’ and wherever the context may require, ‘You’ (or any variation thereof), shall mean any natural or legal person (including any legal heirs, administrators or successors) who has agreed to become a User of the Platform by accessing or using the Platform. If You are accepting these Terms and using the Services (as defined below) or the Platform on behalf of a company, organization, government, or other legal entity, You represent and warrant that You are authorized to do so.</p>

<p>                                Unless the context otherwise requires, “MirrAR Innovation Technology Private Limited’’, “We”, “us”, “our” or “Company” shall mean MirrAR Innovation Technology Private Limited.</p>

<p>The Company enables transactions on its Platform between participating restaurants/stores/merchants/sellers and buyers, dealing in (a) food and beverages, (b) provisions, consumer goods, consummables etc., and (c) any other products or services ("Platform Services"). The buyers ("Buyers") can choose and place orders ("Orders") from a variety of products and services listed and offered for sale by various merchants including but not limited to the restaurants, eateries, grocery stores, other service providers ("Merchants"), on the Platform.</p>

<p>These Terms contain rules, regulations, policies, terms and conditions applicable to any Person who may access the Platform, as modified and updated from time to time. You hereby agree and acknowledge to be bound by the User Agreement</p>

<p>Use of the Platform and the Services provided are subject to the rules and regulations, policies, notices, terms and conditions set forth or included by reference in these Terms.</p>

<p>As a User, the User Agreement shall be effective and binding upon Your ‘acceptance’. ‘Acceptance’ shall mean Your affirmative action in clicking on ‘check box’ and/or the ‘Accept’ button and on entering information as requested on the sign up page [or simply by accessing or visiting the Platform]. If You do not agree or are not willing to be bound by the User Agreement and our Policies (defined below), please do not enter information as requested on the sign up page and click the “Accept” button and do not seek to obtain access to, view, visit, download or otherwise use the Platform (or any of its components/constituents) or any information or Services.</p>

<p>By impliedly or expressly accepting these Terms, You also accept and agree to be bound by applicable policies of the Company including the Privacy Policy as published on the Website (“Policies”) communicated to the Users by publication on the Platform and/or in writing.</p>
<h4>Amendment</h4>
<p>The Company reserves the right to modify the Platform and/or alter these Terms and/or Policies at any time and retains the right to deny access at any time including the termination of membership and deletion of the account, to anyone who the Company believes has violated the provisions of the User Agreement.</p>

<p>You are advised that any amendment to the User Agreement incorporated herein by reference will only be notified on the Website on publicly accessible links and You agree by accessing, browsing or using the Website that such publication shall immediately constitute sufficient notice for all purposes against You. The revised version/ terms shall be effective from the time that the Company publishes the same on the Website.</p>

<p>The Company shall not be liable to give any prior notice to the Users for any amendments to the Terms, which in the opinion of the Company, has the effect of diminishing the User’s existing rights or imposing additional obligations. You understand and hereby accept that the decision, whether any proposed amendments to the Terms and/or Policies have the effect of diminishing the User’s existing rights or imposing additional obligations on the User, shall be decided by the Company in its sole discretion.</p>
<h4>Definitions</h4>
<p>“Affiliate” shall mean any Person who directly or indirectly, controls, is controlled by, or is under the common control of the Company. The term “Control” is used in the sense of the possession by a Person or a group of Persons acting in concert, directly or indirectly, of the right to direct or cause the direction of the management and policies of another Person, whether through the board of directors or ownership of voting rights by such other Person, by the Articles of Association, contract or otherwise. A Person or a group of Persons acting in concert shall be deemed to be in control of a body corporate if such Person or group of Persons is in a position to appoint or appoints the majority of the directors of such body corporate.</p>

<p>“Authority” shall mean any union, national, state, local, or other governmental, statutory, administrative, judicial, regulatory or self-regulating authority, agency or instrumentality having jurisdiction over the relevant matter.</p>

<p>“Grievance Redressal Officer” means the grievance redressal officer appointed by the Company in accordance with applicable Law, from time to time.</p>

<p>“Information” means and shall include any confidential and/or personally identifiable information or other information provided to the Company or other Users of the Platform or at the time of registration with the Platform, or through any email/messaging feature and shall include without limitation Your name, sex, age, email address, phone number (if provided) or such other personal information.</p>

<p>“Internal Service Provider” shall mean reputable logistics or any back end service providers of the Platform appointed by the Company that will provide various services that the Company may require in order to run operations of the Platform for example to facilitate or outsource one or more aspects of the business, product and service operations provided on the Platform, including search technology, discussion boards, payments, affiliate and rewards programs, co-branded credit cards, maintenance services, database management etc.</p>

<p>“Law” shall mean all statutes, enactments, acts of legislature, laws, ordinances, rules, byelaws, regulations, notifications, guidelines, policies, directions, directives and orders of any government and or any Authority.</p>

<p>“Losses” shall include, without limitation, losses, liabilities, actions, suits, claims, proceedings, costs, damages, judgments, amounts paid in settlement and expenses etc.</p>

<p>“Pay Facility” means the automated electronic payment or collection and remittance facility provided by the Company to the Buyers to facilitate payments for paid services on the Website directly through banks or financial institution infrastructure or indirectly through payment gateway facility providers or through any such facility authorized by the Reserve Bank of India to provide enabling support facility for collection and remittance of payment.</p>

<p>“Person” shall mean and include any individual, legal entity, company, body corporate, partnership firm, association, limited liability partnership or proprietorship, whether incorporated or not.</p>

<p>“Policies” shall mean and include the privacy policy and any other policies of the Company as amended and provided on the Platform or communicated to the Users in any other way from time to time.</p>

<p>“Services” shall mean the services rendered by the Platform as may be specifically notified by the Company on the Website or by other means of communication from time to time, in accordance with the applicable Law, and currently includes providing a platform to create e-commerce online stores and sell their product online.</p>

<p>“Service Charges” means any charges charged to the User by the Company for the use of the Platform wholly or partially or for select services, as notified on the Platform from time to time.</p>

<p>“User(s)” shall mean a user of the Platform.</p>

<h4>Eligibility</h4>

<p>In order to use the Platform, You need to be 18 (eighteen) years of age or older. The Platform may only be used or accessed by such Persons who can enter into and perform legally binding contracts under Indian Contract Act, 1872.</p>

<p>The Company or the Platform shall not be liable in case of false information being given about the age by the User and the User and/or his natural or appointed guardian alone shall be liable for the consequences as per the law of the country to which the user belongs and as per applicable Indian law. If You are a natural person and You are a minor, Your natural or appointed guardian hereby acknowledges and agrees that (i) You have been permitted to sign up and use the Platform (ii) Your parent / guardian is responsible for monitoring and supervising Your use of the Platform.</p>

<p>The Company disclaims all liability arising out of such unauthorised use of the Platform and any third party liability arising out of Your use of the Platform if You are a minor.</p>

<h4>Registration and Creation of Profile</h4>

<p>A Merchant is required to register and create a profile by creating a user name and password and providing necessary details about the Merchant in order to be eligible to access the relevant Services.</p>

<p>You are solely responsible for maintaining secrecy and confidentiality of Your login details including Your user name, password and user code (if any).</p>

<p>The Merchant hereby agrees and acknowledges that the Platform will grant access to any person who has obtained Your username, password and code in the same manner as it would have granted access to You and You shall be responsible for all activities conducted under Your username, password and any code. The Merchant is responsible for maintaining the confidentiality of the Merchant’s account access information and password, if the Merchant is registered on the Platform. The Merchant shall be responsible for all usage of the Merchant’s account and password, whether or not authorized by the Merchant.</p>

<p>The Merchant shall immediately notify the Company of any actual or suspected unauthorized use of the Merchant’s account or password. Although the Company, its directors, shareholders, employees, associates, contractors or agents (“Personnel”) shall not be responsible in any manner for any Losses occurring from any breach of secrecy of Your username, password, user code or any unauthorized use of Your account, You may be liable for the Losses of the Company or such other parties as the case may be, due to any unauthorized use of Your account.</p>

<p>You agree that the sole purpose of registering on or using the Platform is to support individual sellers to get their own online store and connect with their own customers to get orders online, and You shall not use the Platform in any manner for any other purpose other than as mentioned above and for purposes which are not permitted under the applicable Law.</p>

<h4>User Information</h4>

<p>You agree to provide true, accurate, up to date and complete information while signing up on the Platform or for any other purpose when prompted or requested to do so on the Platform.</p>

<p>Certain information You provide on the Platform in Your profile may reveal, or allow others to identify different aspects of Your private life, and more generally about You. You are expressly and voluntarily accepting the terms of the User Agreement and supplying of all such information by You on the Platform, including all information deemed "personal" or “sensitive” by applicable Laws, is entirely voluntary on Your part.</p>

<p>For the use of our Services, You will be required to use certain devices, software, and data connections to use our Services, which we otherwise do not supply. For as long as You use our Services, You consent to downloading and installing updates to our Services, including automatically, downloading and installing such updates.</p>

<p>You are responsible for all carrier data plan and other fees and taxes associated with Your use of our Services. We may charge You for our Services, including applicable taxes. We do not provide refunds for our Services, except as required by law.</p>

<p>The Company shall not be responsible in any manner for the authenticity of the personal information or sensitive personal data or information supplied by the User to the Company or to any other person acting on behalf of the Company.</p>

<p>You are prohibited from misrepresenting Your identity and agree not to represent Yourself as another User or login/ register using the identity of any other Person. You are responsible to maintain and promptly update the information provided while signing up or verifying or for any other purpose on the Platform to ensure that the information provided by You is true, accurate, current and complete at all times.</p>

<p>If You provide any information that is untrue, inaccurate, not current or incomplete or the Company has reasonable grounds to believe that such information is untrue, inaccurate, not current or incomplete, or not in accordance with these Terms, the Company reserves the right to indefinitely suspend or terminate or block Your use or access to the Platform in any manner whatsoever. Should any other User or Person act upon such untrue, inaccurate, not current or incomplete information provided or verified by You, the Company, and its Personnel shall not be liable for any damages, Losses, direct, indirect, immediate or remote, interests or claims resulting from such information to You or to any third party. You hereby indemnify and agree to keep harmless the Company, and its Personnel in accordance with the Indemnity clause contained in these Terms.</p>

<p>Once You have signed up on the Platform, You agree and allow the Platform to get access to Your mobile device to find and keep track of mobile phone numbers of other Users of the Service, Your location, inbuilt storage or other storage on Your mobile device, access to internet, control vibration, accounts on Your mobile device and such other data or information downloaded, added, edited, stored, processed, used, deleted on or from Your mobile device.</p>
<h4>Electronic Communication</h4>
<p>You agree to keep Yourself updated with all data, information and communication pertaining to You made available on the Platform by the Company. You further agree that Your use of the Platform or provision of any data or information including any correspondence (by email or otherwise) to or by the Company is through electronic records and You consent to receiving communication from the Company via electronic documents including emails and/or SMS, which will be deemed adequate for service of notice/ electronic record.</p>

<p>You understand You may have to bear/pay any charges associated with any such access (including text messaging charges for messages from Your mobile device). Our communications to You may include communication that would inform Users about various features of our services. The Company may also send You promotional information unless You have opted out of receiving such information.</p>

<p>We may need to provide You with certain communications/notifications, such as service announcements and administrative messages. These communications are considered part of the Services and Your account, and You may not be able to opt-out from receiving them. If You added Your phone number to Your account and You later change or deactivate that phone number, You must update Your account information to help prevent us from communicating with anyone who acquires Your old number.</p>
<h4>Terms</h4>
<p>All commercial/contractual terms are offered by and agreed to between Buyers and Merchants alone with respect to products and services being offered by the Merchants. The commercial/contractual terms include without limitation price, applicable taxes, shipping costs, payment terms, date, period and mode of delivery, warranties related to products and services and after sales services related to products and services. The Company does not have any control or does not determine or advise or in any way involve itself in the offering or acceptance of such commercial/contractual terms between the Buyers and Merchants. The Company may, however, offer support services to Merchants in respect to Order fulfilment, mode of payment, payment collection and other ancillary services, pursuant to the understanding between the Company and the Merchants. The price of the product and services offered by the Merchant are determined by the Merchant itself and the Company has no role to play in such determination of price in any way whatsoever.</p>

<p>The Company does not make any representation or warranty as to the item-specifics (such as legal title, creditworthiness, identity, etc.) of any of the Merchants. You are advised to independently verify the bona fides of any particular Merchant that You choose to deal with on the Platform and use Your best judgment on that behalf. All Merchant offers and third-party offers are subject to respective party terms and conditions. The Company takes no responsibility for such offers.</p>

<p>The Company neither makes any representation or warranty as to specifics (such as quality, value, saleability, etc.) of the products or services proposed to be sold or offered to be sold or purchased on the Platform nor does implicitly or explicitly support or endorse the sale or purchase of any products or services on the Platform. The Company accepts no liability for any errors or omissions, whether on behalf of itself or third parties.</p>

<p>The Company is not responsible for any non-performance or breach of any contract entered into between Buyers and Merchants. The Merchant agrees to indemnify the Company for any and all Losses suffered by the Company due to Merchant’s use of the Platform and interactions with Buyers pursuant thereto. The Company cannot and does not guarantee that the concerned Buyers and Merchants will perform any transaction concluded on the Platform. The Company is not responsible for unsatisfactory or non-performance of services or damages or delays as a result of products which are out of stock, unavailable or back ordered.</p>

<p>The Company is operating an online marketplace and assumes the role of facilitator, and does not at any point of time during any transaction between Buyer and Merchant on the Platform come into or take possession of any of the products or services offered by Merchant. At no time shall the Company hold any right, title or interest over the products nor shall the Company have any obligations or liabilities in respect of such contract entered into between Buyer and Merchant.</p>

<p>The Company is only providing a platform for communication and it is agreed that the contract for sale of any of the products or services shall be a strictly bipartite contract between the Merchant and the Buyer. In case of complaints from the Buyer pertaining to efficacy, quality, or any other such issues, the Company shall notify the same to Merchant and shall also redirect the Buyer to the consumer call center of the Merchant. The Merchant shall be liable for redressing Buyer complaints. In the event You raise any complaint on any Merchant accessed using our Platform, we shall assist You to the best of our abilities by providing relevant information to You, such as details of the Merchant and the specific Order to which the complaint relates, to enable satisfactory resolution of the complaint.</p>
<h4>User Obligations</h4>
<p>You agree, undertake and confirm that Your use of Platform shall be strictly governed by the following binding principles:</p>

<p>You shall not host, display, upload, download, modify, publish, transmit, update or share any information which:</p>
<ul><li>belongs to another person and which You do not have any right to;</li>
<li>is grossly harmful, harassing, blasphemous, defamatory, obscene, pornographic, paedophilic, libellous, slanderous, criminally inciting or invasive of another's privacy, hateful, or racially, ethnically objectionable, disparaging, relating or encouraging money laundering or gambling, or otherwise unlawful in any manner whatsoever; or unlawfully threatening or unlawfully harassing including but not limited to "indecent representation of women" within the meaning of the Indecent Representation of Women (Prohibition) Act, 1986;</li>
<li>is misleading or misrepresentative in any way;</li>
<li>is patently offensive to the online community, such as sexually explicit content, or content that promotes obscenity, paedophilia, racism, bigotry, hatred or physical harm of any kind against any group or individual;</li>
<li>harasses or advocates harassment of another person;</li>
<li>involves the transmission of "junk mail", "chain letters", or unsolicited mass mailing or "spamming";</li>
<li>promotes illegal activities or conduct that is abusive, threatening, obscene, defamatory or libellous;</li>
<li>infringes upon or violates any third party's rights including, but not limited to, intellectual property rights, rights of privacy (including without limitation unauthorized disclosure of a person's name, email address, physical address or phone number) or rights of publicity;</li>
<li>promotes an illegal or unauthorized copy of another person's copyrighted work (see "copyright complaint" below for instructions on how to lodge a complaint about uploaded copyrighted material), such as providing pirated computer programs or links to them, providing information to circumvent manufacture-installed copy-protect devices, or providing pirated music or links to pirated music files;</li>
<li>contains restricted or password-only access pages, or hidden pages or images (those not linked to or from another accessible page);</li>
<li>provides material that exploits people in a sexual, violent or otherwise inappropriate manner or solicits personal information from anyone;</li>
<li>provides instructional information about illegal activities such as making or buying illegal weapons, violating someone's privacy, or providing or creating computer viruses;</li>
<li>contains video, photographs, or images of another person (with a minor or an adult);</li>
<li>tries to gain unauthorized access or exceeds the scope of authorized access to the Platform or to profiles, blogs, communities, account information, bulletins, friend request, or other areas of the Platform or solicits passwords or personal identifying information for commercial or unlawful purposes from other Users;</li>
<li>engages in commercial activities and/or sales without our prior written consent such as contests, sweepstakes, barter, advertising and pyramid schemes, or the buying or selling of products related to the Platform. Throughout these Terms, the Company’s prior written consent means a communication coming from the Company’s authorised representative, specifically in response to Your request, and expressly addressing and allowing the activity or conduct for which You seek authorization;</li>
<li>solicits gambling or engages in any gambling activity which is or could be construed as being illegal;</li>
<li>interferes with another user's use and enjoyment of the Platform or any third party's user and enjoyment of similar services;</li>
<li>refers to any website or URL that, in our sole discretion, contains material that is inappropriate for the Platform or any other website, contains content that would be prohibited or violates the letter or spirit of these Terms;</li>
<li>harm minors in any way;</li>
<li>infringes any patent, trademark, copyright or other intellectual property rights or third party's trade secrets or rights of publicity or privacy or shall not be fraudulent or involve the sale of counterfeit or stolen products;</li>
<li>violates any law for the time being in force;</li>
<li>deceives or misleads the addressee/users about the origin of such messages or communicates any information which is grossly offensive or menacing in nature;</li>
<li>impersonate another person;</li>
<li>contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer resource; or contains any trojan horses, worms, time bombs, cancelbots, easter eggs or other computer programming routines that may damage, detrimentally interfere with, diminish value of, surreptitiously intercept or expropriate any system, data or personal information;</li>
<li>threatens the unity, integrity, defence, security or sovereignty of India, friendly relations with foreign states, or public order or causes incitement to the commission of any criminal offence or prevents investigation of any offence or is insulting any other nation;</li>
<li>is false, inaccurate or misleading;</li>
<li>directly or indirectly, offers, attempts to offer, trades or attempts to trade in any item, the dealing of which is prohibited or restricted in any manner under the provisions of any applicable law, rule, regulation or guideline for the time being in force; or</li>
<li>creates liability for us or causes us to lose (in whole or in part) the services of our internet service provider or other suppliers.</li>

</ul>
<p>In case of any action, omission, transaction or attempted transaction which is violative of these Terms or applicable Laws comes to Your knowledge, You shall forthwith take all steps to inform the Company of such violation at <a href="mailto:support@simplisell.co">support@simplisell.co</a>.</p>

<p>You shall not use any "deep-link", "page-scrape", "robot", "spider" or other automatic device, program, algorithm or methodology, or any similar or equivalent manual process, to access, acquire, copy or monitor any portion of the Platform or any Content, or in any way reproduce or circumvent the navigational structure or presentation of the Platform or any Content, to obtain or attempt to obtain any materials, documents or information through any means not purposely made available through the Platform. We reserve our right to prohibit any such activity.</p>

<p>You shall not attempt to gain unauthorized access to any portion or feature of the Platform, or any other systems or networks connected to the Platform or to any server, computer, network, or to any of the services offered on or through the Platform, by hacking, "password mining" or any other illegitimate means.</p>

<p>You shall not probe, scan or test the vulnerability of the Platform or any network connected to the Platform nor breach the security or authentication measures on the Platform or any network connected to the Platform. You may not reverse look-up, trace or seek to trace any information on any other user of or visitor to Platform, or any other Buyer, including any account on the Platform not owned by You, to its source, or exploit the Platform or any service or information made available or offered by or through the Platform, in any way where the purpose is to reveal any information, including but not limited to personal identification or information, other than Your own information, as provided for by the Platform.</p>

<p>You may not use the Platform or any content on the Platform for any purpose that is unlawful or prohibited by these Terms, or to solicit the performance of any illegal activity or other activity that infringes the rights of the Company and/or others.</p>

<p>You shall at all times ensure full compliance with the applicable provisions, as amended from time to time, of (a) the Information Technology Act, 2000 and the rules thereunder; (b) all applicable Laws; and (c) international laws, foreign exchange laws, statutes, ordinances and regulations (including, but not limited to sales tax/VAT, income tax, octroi, service tax, central excise, custom duty, local levies) regarding Your use of our service and Your listing, purchase, solicitation of offers to purchase, and sale of products or services. You shall not engage in any transaction in an item or service, which is prohibited by the provisions of any applicable law including exchange control laws or regulations for the time being in force.</p>

<p>In order to allow us to use the information supplied by You, without violating Your rights or any laws, You agree to grant us a non-exclusive, worldwide, perpetual, irrevocable, royalty-free, sub-licensable (through multiple tiers) right to exercise the copyright, publicity, database rights or any other rights You have in Your information, in any media now known or not currently known, with respect to Your information. We will only use Your Information in accordance with these Terms and Policies applicable to use of the Platform.</p>

<p>From time to time, You shall be responsible for providing information relating to the products or services proposed to be sold by You. In this connection, You undertake that all such information shall be accurate in all respects. You shall not exaggerate or overemphasize the attributes of such products or services so as to mislead other Users in any manner.</p>

<p>You shall not engage in advertising to, or solicitation of, other Users of the Platform to buy or sell any products or services, including, but not limited to, products or services related to that being displayed on the Platform or related to us. You may not transmit any chain letters or unsolicited commercial or junk email to other Users via the Platform. It shall be a violation of these Terms to use any information obtained from the Platform in order to harass, abuse, or harm another person, or in order to contact, advertise to, solicit, or sell to another person other than us without our prior explicit consent. You understand that we have the right at all times to disclose any information (including the identity of the persons providing information or materials on the Platform) as necessary to satisfy any law, regulation or valid governmental request. This may include, without limitation, disclosure of the information in connection with investigation of alleged illegal activity or solicitation of illegal activity or in response to a lawful court order or subpoena. In addition, We can (and You hereby expressly authorize us to) disclose any information about You to law enforcement or other government officials, as we, in our sole discretion, believe necessary or appropriate in connection with the investigation and/or resolution of possible crimes, especially those that may involve personal injury.</p>

<p>We reserve the right, but have no obligation, to monitor the materials posted on the Platform. The Company shall have the right to remove or edit any content that in its sole discretion violates, or is alleged to violate, any applicable law or either the spirit or letter of these Terms. Notwithstanding this right, You remain solely responsible for the content of the materials You post on the platform and in Your private messages. Please be advised that such Content posted does not necessarily reflect the Company’s views. In no event shall the Company assume or have any responsibility or liability for any Content posted or for any claims, damages or Losses resulting from use of Content and/or appearance of Content on the Platform. You hereby represent and warrant that You have all necessary rights in and to all Content which You provide and all information it contains and that such Content shall not infringe any proprietary or other rights of third parties or contain any libellous, tortious, or otherwise unlawful information.</p>

<p>Your correspondence or business dealings with, or participation in promotions of, advertisers found on or through the Platform, including payment and delivery of related products or services, and any other terms, conditions, warranties or representations associated with such dealings, are solely between You and such advertiser. We shall not be responsible or liable for any loss or damage of any sort incurred as the result of any such dealings or as the result of the presence of such advertisers on the Platform.</p>

<p>It is possible that other Users (including unauthorized users or 'hackers') may post or transmit offensive or obscene materials on the Platform and that You may be involuntarily exposed to such offensive and obscene materials. It is also possible for others to obtain personal information about You due to Your use of the Platform, and that the recipient may use such information to harass or injure You. We do not approve of such unauthorized uses, but by using the Platform You acknowledge and agree that we are not responsible for the use of any personal information that You publicly disclose or share with others on the Platform. Please carefully select the type of information that You publicly disclose or share with others on the Platform.</p>

<p>The Company respects the intellectual property rights of others and expects Users of the Services to do the same. We reserve the right to remove Content alleged to be infringing without prior notice, at our sole discretion, and without liability to You. We will respond to notices of alleged infringement that comply with applicable Law and are properly provided to us. If You believe that Your Content has been copied in a way that constitutes copyright infringement, please report this at [●].</p>

<p>The Company may in future charge a fee or charges, as intimated from time to time on the Platform, for registration on the Platform as Users especially for maintaining an official account, for availing services provided by the Company through the Platform. Currently, the Company does not levy any charges/fees for registration, access or maintaining an account on the App. The Company reserves the right to introduce new services including any premium or paid services or modify or discontinue any existing services provided on the Platform. Changes to the Terms or any of the Policies of the Company shall be published on the Platform and such changes shall automatically become effective immediately after they are published on the Platform. You are requested to visit the Terms and various Policies links on the Platform often to keep abreast of any amendments.</p>

<p>You confirm that these Terms hereunder will not conflict with, result in a breach of or constitute a default (or any event that, with notice or lapse of time, or both, would constitute a default) or result in the acceleration of any obligation under any of the terms, conditions or provisions of any other agreement or instrument to which You are a party or by which You are bound or to which any of Your property or assets are subject, conflict with or violate any of the provisions of its charter documents, or violate any statute or any order, rule or regulation of any Authority that would materially and adversely affect the performance of Your duties hereunder. You have obtained any consent, approval, authorization of Authority required for the execution, delivery and performance of its respective obligations hereunder. If the Company suffers any Loss or damages or a claim is made by any Person against the Company or the Platform as a result of a breach or default or contravention on Your part of the Terms and the Policies, You agree to, forthwith upon delivery of notice by the Company, make good such Losses or damages or claim amounts to the Company all such Losses or damages.</p>

<p>You confirm that there is no action, suit or proceeding pending against You or to Your knowledge, threatened in any court or by or before any other Authority which would prohibit Your entering into or performing obligations under these Terms and the Policies.</p>

<p>You confirm that You shall not transfer Your account and shall not assign any rights and obligations under these Terms and Policies to any third party without the specific written permission of the Company.</p>
<h4>Action</h4>
<p>In case of any violation of these Terms and any Policies of the Company, the Company has the right to immediately terminate the access or usage rights of the User to the Platform without any notice and any such violative information that is displayed or submitted on the Platform may be removed immediately and completely and/or report to investigating authorities under applicable Law.</p>

<p>If the Company terminates Your access to the Platform, the Company may, in its sole and absolute discretion, remove and destroy any data and files stored by You on its servers and You agree and acknowledge that the Company or the Platform shall not be responsible and/or liable for removing or deleting such information.</p>
<h4>No Endorsement</h4>
<p>We neither endorse any Merchant or the products/services offered by them. In addition, although these Terms require You to provide accurate Information, we do not attempt to confirm, and do not confirm its purported identity. We will not be responsible for any damage or harm resulting from Your interactions with Merchants.</p>

<p>By using the Services, You agree that any legal remedy or liability that You seek to obtain for actions or omissions of Merchants or other third parties will be limited to a claim against the particular Merchant or other third parties who caused You harm and You agree not to attempt to impose liability on, or seek any legal remedy from us with respect to such actions or omissions.</p>
<h4>Disclaimers</h4>
<p>The Platform may be under constant upgrades, and some functions and features may not be fully operational.</p>

<p>Due to the vagaries that can occur in the electronic distribution of information and due to the limitations inherent in providing information obtained from multiple sources, there may be delays, omissions, or inaccuracies in the content provided on the platform or delay or errors in functionality of the Platform. As a result, we do not represent that the information posted is correct in every case.</p>

<p>We expressly disclaim all liabilities that may arise as a consequence of any unauthorized use of credit/ debit cards.</p>

<p>The Company disclaims all liability that may arise due to any violation of any applicable Laws including the law applicable to products and services offered by the Merchant.</p>

<p>While the materials provided on the Platform were prepared to provide accurate information regarding the subject discussed, the information contained in these materials is being made available with the understanding that we make no guarantees, representations or warranties whatsoever, whether expressed or implied, with respect to professional qualifications, expertise, quality of work or other information herein. Further, we do not, in any way, endorse any service offered or described herein. In no event shall we be liable to You or any third party for any decision made or action taken in reliance on such information.</p>

<p>The information provided hereunder is provided "as is". We and / or our employees make no warranty or representation regarding the timeliness, content, sequence, accuracy, effectiveness or completeness of any information or data furnished hereunder or that the information or data provided hereunder may be relied upon. Multiple responses may usually be made available from different sources and it is left to the judgement of Users based on their specific circumstances to use, adapt, modify or alter suggestions or use them in conjunction with any other sources they may have, thereby absolving us as well as our consultants, business associates, Affiliates, business partners and employees from any kind of professional liability.</p>

<p>We shall not be liable to You or anyone else for any Losses or injury arising out of or relating to the information provided on the platform. In no event will we or our employees, Affiliates, authors or agents be liable to You or any third party for any decision made or action taken by Your reliance on the content contained herein.</p>

<p>In no event will we be liable for any damages (including, without limitation, direct, indirect, incidental, special, consequential or exemplary damages, damages arising from personal injury/wrongful death, and damages resulting from lost profits, lost data or business interruption), resulting from any services provided by any third party or merchant accessed through the platform, whether based on warranty, contract, tort, or any other legal theory and whether or not we are advised of the possibility of such damages.</p>
<h4>Intellectual Property</h4>
<p>We are either the owner of intellectual property rights or have the non-exclusive, worldwide, perpetual, irrevocable, royalty free, sub-licensable (through multiple tiers) right to exercise the intellectual property, in the Platform, and in the material published on it including but not limited to user interface, layout format, order placing process flow and any content thereof.</p>

<p>You recognize that the Company is the registered owner of the word mark ‘Dukaan’ and the logo including but not limited to its variants (“IPR”) and You shall not directly or indirectly, attack or assist another in attacking the validity of, or Company’s or its Affiliates proprietary rights in, the licensed marks or any registrations thereof, or file any applications for the registration of the licensed marks or any names or logos derived from or confusingly similar to the licensed marks, any variation thereof, or any translation or transliteration thereof in another language, in respect of any products/services and in any territory throughout the world. If You become aware or acquire knowledge of any infringement of IPR You shall report the same at <a href="mailto:support@simplisell.co">support@simplisell.co</a> with all relevant information.</p>

<p>You may print off one copy, and may download extracts, of any page(s) from the Platform for Your personal reference and You may draw the attention of others within Your organisation to material available on the Platform.</p>

<p>You must not modify the paper or digital copies of any materials You have printed off or downloaded in any way, and You must not use any illustrations, photographs, video or audio sequences or any graphics separately from any accompanying text.</p>

<p>You must not use any part of the materials on the Platform for commercial purposes without obtaining a licence to do so from us or our licensors.</p>

<p>If You print off, copy or download any part of the Platform in breach of these Terms, Your right to use the Platform will cease immediately and You must, at our option, return or destroy any copies of the materials You have made.</p>
<h4>Payment</h4>
<p>For all the transactions between the Merchant and the Buyer, the Merchant agrees to pay an amount equal to 2% of the transaction value (“Transaction Fees”). The Merchant agrees that the Transaction Fees is subject to change and such change shall be published on the Platform from time to time.</p>

<p>The Company shall transfer the amount paid by the Buyer against an Order in the Merchant’s account, within 1 day from the Buyer being in receipt of the good/service pursuant to such an Order placed.</p>

<p>In order to enable Buyers to use paid services on the Website, the Company facilitates the provision of Pay Facility by a third-party service provider on the Website, at its sole discretion. The Pay Facility shall enable automated collection and remittance services using the facilities of various Indian banks, financial institutions, credit/ debit/ cash card brands, various third-party service providers and/or payment card industry issuers and through such other infrastructure and facilities as may be authorized by the Reserve Bank of India for collection, refund and remittance, as the case may be. The Payment Facility shall be availed in accordance with these Terms.</p>

<p>The Pay Facility shall support payments through credit/debit cards from the banks that are available while selecting the Pay Facility as the payment method or by cash. The Pay Facility shall support payments through UPI and mobile wallet payment options. However, payments may be added or removed or suspended through any one or more banks directly or through any payment gateway facilitators and such change shall come into effect upon the same being published on the Platform.</p>

<p>The Pay Facility shall be made available to the Buyers to make payment easier. In case wrong bank account details are used by the Buyer, Company shall not be responsible for loss of money, if any. In case of there being any technical failure, at the time of transaction and there is a problem in making payment, You could contact us at <a href="mailto:support@simplisell.co">support@simplisell.co</a>.</p>

<p>You agree and accept that the Company is neither acting as trustee nor acting in a fiduciary capacity with respect to the payment transaction by reason of providing the Pay Facility or any other method of payment to its Buyers.</p>

<p>While availing any of the payment method/s available on the Website, we will not be responsible or assume any liability, whatsoever in respect of any loss or damage arising directly or indirectly to You due to:</p>
<p>Lack of authorization for any transaction;</p>
<p>Exceeding the preset limit mutually agreed by You and between "Banks";</p>
<p>Any payment issues arising out of the transaction; or</p>
<p>Decline of transaction for any other reason.</p>

<p>All payments made against the purchases/services on the Website by You shall be compulsorily in Indian Rupees acceptable in the Republic of India. The Website shall not facilitate transaction with respect to any other form of currency with respect to the purchases made on the Website.</p>

<p>You have specifically authorized the Company or its service providers to collect, process, facilitate and remit payments electronically or through ‘cash on delivery’ to and from other Buyers in respect of transactions through Payment Facility. Your relationship with the Company is on a principal to principal basis and by accepting these Terms, You agree that the Company is an independent contractor for all purposes, and does not have control of or liability for the products or services that are listed on the Website that are paid for by using the Payment Facility. The Company does not guarantee the identity of any Buyer nor does it ensure that a Buyer or a Merchant will complete a transaction.</p>

<p>You understand, accept and agree that the Payment Facility provided by the Company is neither a banking nor financial service but is merely a facilitator providing an electronic, automated online electronic payment, receiving payment through ‘cash on delivery’, collection and remittance facility for the transactions on the Website using the existing authorized banking infrastructure and credit card payment gateway networks. Further, by providing the Payment Facility, the Company is neither acting as trustees nor acting in a fiduciary capacity with respect to the transaction or the transaction price.</p>

<p>Buyers acknowledge and agree that the Company acts as the Merchant's payment agent for the limited purpose of accepting payments from Buyers on behalf of the Merchant. Upon Your payment of amounts to us, which are due to the Merchant, Your payment obligation to the Merchant for such amounts is completed, and we are responsible for remitting such amounts to the Merchant. You shall not, under any circumstances whatsoever, make any payment directly to the Merchant for Order bookings made using the Website.</p>
<h4>Payment Facility for Buyers</h4>
<p>You, as a Buyer, understand that upon initiating a transaction You are entering into a legally binding and enforceable contract with the Merchant to purchase the products and /or services from the Merchant using the Payment Facility, and You shall pay the transaction price through Your issuing bank to the Merchant using Payment Facility.</p>

<p>You, as a Buyer, understand that upon initiating a transaction You are entering into a legally binding and enforceable contract with the Merchant to purchase the products and /or services from the Merchant using the Payment Facility, and You shall pay the transaction price through Your issuing bank to the Merchant using Payment Facility.</p>

<p>You, as a Buyer, understand that the Payment Facility may not be available in full or in part for certain category of products and/or services and/or transactions and hence You may not be entitled to a refund in respect of the transactions for those products and /or services.</p>

<p>Except for ‘cash on delivery’ transaction, refund, if any, shall be made at the same issuing bank from where transaction price was received, or through any other method available on the Website, as chosen by You.</p>

<p>For ‘cash on delivery’ transactions, refunds, if any, will be made via electronic payment transfers.</p>

<p>Refund shall be made in Indian Rupees only and shall be equivalent to the transaction price received in Indian Rupees.</p>

<p>For electronics payments, refund shall be made through payment facility using any other online banking / electronic funds transfer system approved by Reserve Bank India (RBI).</p>

<p>Refunds may be supported for select banks. Where a bank is not supported for processing refunds, You will be required to share alternate bank account details with us for processing the refund.</p>

<p>Refund shall be conditional and shall be with recourse available to the Company in case of any misuse by Buyer.</p>

<p>We may also request You for additional documents for verification.</p>

<p>The Company reserves the right to impose limits on the number of transactions or transaction price which the Company may receive from on an individual Valid Credit/Debit/ Cash Card / Valid Bank Account/ and such other infrastructure or any other financial instrument directly or indirectly through payment aggregator or through any such facility authorized by Reserve Bank of India to provide enabling support facility for collection and remittance of payment or by an individual Buyer during any time period, and reserves the right to refuse to process transactions exceeding such limit.</p>

<p>The Company reserves the right to refuse to process transactions by Buyers with a prior history of questionable charges including without limitation breach of any agreements by Buyer with the Company or breach/violation of any law or any charges imposed by issuing bank or breach of any Policy.</p>

<p>The Buyer and Merchant acknowledge that the Company will not be liable for any damages, interests or claims etc. resulting from not processing a transaction/transaction price or any delay in processing a transaction/transaction price which is beyond control of the Company.</p>
<h4>Compliance with Laws</h4>
<p>Buyer and Merchant shall comply with all the applicable Laws (including without limitation Foreign Exchange Management Act, 1999 and the rules made and notifications issued there under and the Exchange Control Manual as may be issued by Reserve Bank of India from time to time, Customs Act, Information and Technology Act, 2000 as amended by the Information Technology (Amendment) Act 2008, Prevention of Money Laundering Act, 2002 and the rules made there under, Foreign Contribution Regulation Act, 1976 and the rules made there under, Income Tax Act, 1961 and the rules made there under, Export Import Policy of government of India) applicable to them respectively for using the payment facility.</p>
<h4>Buyer's arrangement with Issuing Bank</h4>
<p>All Valid Credit / Debit/ Cash Card/ and other payment instruments are processed using a Credit Card payment gateway or appropriate payment system infrastructure and the same will also be governed by the terms and conditions agreed to between the Buyer and the respective Issuing Bank and payment instrument issuing company.</p>

<p>All Online Bank Transfers from Valid Bank Accounts are processed using the gateway provided by the respective Issuing Bank which support Payment Facility to provide these services to the Users. All such Online Bank Transfers on Payment Facility are also governed by the terms and conditions agreed to between Buyer and the respective Issuing Bank.</p>
<h4>Limitation of Liability</h4>
<p>In no event shall the Company or its suppliers, Affiliates, service providers and Internal Service Providers be liable for any direct, indirect, incidental, special, consequential or exemplary damages, including but not limited to, damages for loss of profits, goodwill, use, data or other intangible Losses arising (in any manner whatsoever) out of or in connection with the Platform, the Pay Facility, services provided by Internal Service Providers on behalf of the Platform or the Company or any other services.</p>

<p>The Company’s liability under all circumstances is limited to the amount of charges/ fees, if any, paid by You to the Company. The Company, its associates, Affiliates and Internal Service Providers and technology partners make no representations or warranties about the accuracy, reliability, completeness, and/or timeliness of any information or communication provided on or through the use of the Platform or that of the operation of the Platform or Pay Facility or that of any Internal Service Provider will be error free and/or uninterrupted. The Company assumes no liability whatsoever for any monetary or other damage suffered by You on account of: (a) the delay, failure, interruption, or corruption of any data or other information transmitted in connection with use of the Platform or Pay Facility; or (b) any delay, failure, interruption or errors in the operation of the Platform or Pay Facility or the Internal Service Provider.</p>
<h4>Indemnity for non-compliance or breach</h4>
<p>You shall indemnify and hold harmless the Company and the Company's parent, subsidiaries, Affiliates, Internal Service Provider and their respective officers, directors, agents, and employees, from and against any and all Losses, liabilities, actions, suits, claims, proceedings, costs, damages, judgments, amounts paid in settlement and expenses (including without limitation attorneys' fees and disbursements), made by any third party or penalty imposed due to or arising out of Your breach of the Terms or resulting from untrue, inaccurate, not current or incomplete information provided or verified by You.</p>
<h4>Consent and Privacy Policy</h4>
<p>By using the Platform and/or by providing Your Information, You consent to the collection and use of such Information disclosed by You on the Platform and on the Pay Facility (if applicable), by the Company. The personal information / data including but not limited to the information provided by You to the Platform /Pay Facility (if applicable) during the course of You being a registered User shall be retained in accordance with the Privacy Policy published on the Platform from time to time which is incorporated herein by reference and applicable Laws including but not limited to Information Technology Act, 2000 and rules there under. If You do not agree to Your information being transferred or used in this way, please do not use the Platform.</p>

<p>The Company views the protection of User’s privacy as a very important community principle. The Company clearly understands that You and the personal information provided by You is one of the most important assets to the Company. The Company stores and processes the information provided by You that are protected by physical as well as reasonable technological security measures and procedures in compliance with the applicable Law.</p>
<h4>Breach</h4>
<p>Without limiting other remedies that the Company may pursue, the Company may at its sole discretion take such action as it deems fit including but not limited to cancellation of the membership and deletion of the account , limit Your activity on the Platform, immediately remove Your information, or warn other Users of Your actions, forthwith temporarily/indefinitely suspend or terminate or block Your membership, and/or refuse to provide You with access to the Platform or initiate any legal action it may deem fit, particularly in the event: You breach any of the provisions of these Terms including the Policies, any of the documents, agreements between the Company and You in addition to these Terms, terms and conditions made thereunder which are incorporated therein by reference; any misuse of Your account or the Pay Facility (if utilised), in case the Company is unable to verify or authenticate any information provided by You, if the Company believes that Your actions may cause legal liability to the Company, other Users or Yourself.</p>

<p>No actions, omissions or decisions taken by the Company shall waive any rights or claims that the Company may have against the User. Any User that may have been suspended or blocked may not register or attempt to register with the Platform or use the Platform in any manner whatsoever until such time that such User is reinstated by the Company. Notwithstanding the above, if You breach the Terms or the Policies and other documents incorporated therein by reference or any other agreements entered into by the Company and You in addition to the Terms, the Company reserves the right to take strict legal action including but not limited to referral to the appropriate police or other authorities for initiating criminal or civil or other proceedings against You.</p>
<h4>Grievance Redressal Mechanism</h4>
<p>In case of any grievance, objection or complaint on Your part with respect to the Platform, other Users or the Company, including any complaints or enquiry about suspension, termination or blocking of Your membership or right to use the App, You should promptly raise such grievance or complaint with the designated Grievance Officer at <a href="mailto:support@simplisell.co">support@simplisell.co</a> and provide her with all necessary information and/or documents to enable the Company/Grievance Officer to try and resolve the issue.</p>

<p>The name and contact details of the Grievance Officer is published on the Platform as required under the provisions of the Information Technology Act, 2000 and the rules made thereunder.</p>
<h4>Confidentiality</h4>
<p>All communications between the Parties and all confidential information given to or received by You from the Company, and all information concerning the business transactions of the Company with any entity or person with whom it may or may not have a confidentiality obligation with regard to the matter in question, shall be kept confidential by You (whether or not such information or data has been marked as confidential) unless specifically permitted to the contrary in writing to the Company.</p>

<p>Further the Company may at any time disclose any confidential information on a confidential basis to any prospective and current investors, strategic or financial, partners or service providers other than other Users of the Platform.</p>

<p>This confidentiality obligation shall survive the termination of the agreement with and the User account of the concerned User.</p>
<h4>Governing Law and Jurisdiction</h4>
<p>The User Agreement and documents incorporated by reference shall be governed and construed in accordance with the laws of India. If any dispute arises between You and the Company during Your use of the Platform or the Pay Facility or any service incidental to the Platform or thereafter, in connection with the validity, interpretation, implementation or alleged breach of any provision of the Terms and/or any other agreement between the Company and You in addition to the User Agreement and any other documents incorporated therein by reference, the dispute shall be referred to the senior management of the Company for conciliation. If the dispute has not been resolved, then such a dispute shall be subject to the exclusive jurisdiction of the courts in Chennai, Tamilnadu and You hereby submit to the jurisdiction of such courts.</p>


								</div>

							</div>
						</div>
					</div>
				</section>
				<!-- end section -->
</main>
@endsection