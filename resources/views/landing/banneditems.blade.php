@extends('layouts.landing')
@section('content')
<!-- start main -->
<main role="main">
    <!-- Common styles
    ================================================== -->
    <link rel="stylesheet" href="{{ asset('landing/css/style.min.css') }}" type="text/css">

    <!-- Theme styles
    ================================================== -->
    <link rel="stylesheet" href="{{ asset('landing/css/color-7.min.css') }}" type="text/css">
<!-- start section -->
<section class="section">

					<div class="container">
						<div class="row">
							<div class="col-12">

								<div class="content-container">
								<div class="section-heading">
                                    <h2 class="__title">Banned Items</h2>
                        			<h6 class="__subtitle" style="color:black">SimpliSell’s List of Banned Products (Indicative List not exhaustive)</h6>
                                </div>	
									<ul>
                                    <li>Adult Products and Pornographic/ Obscene Materials (including child pornography) in any form (Print, audio/video, MMS, images, photographs, etc.) </li>
<li>Alcohol  </li>
<li>Animals, whether live or dead including its parts and products  </li>
<li>Any article/material/service which cannot be exhibited, advertised, made available, offered for sale at e-commerce platforms/ technology due to restrictions/conditions for sale of those articles / material/ service unless all those conditions are met pursuant to the Applicable Laws  </li>
<li>Any article/material/service which are prohibited by any law at any given time.  </li>
<li>Any item/material which may assist in performance of any illegal or unlawful activity  </li>
<li>Counterfeit Goods and goods/materials infringing any intellectual property rights </li>
<li>Currency, Negotiable Instruments, etc.;  </li>
<li>Endangered species of animals and plants, whether alive or dead  </li>
<li>Fire Arms, parts thereof and ammunitions, weapons, knives, sharpedged and other deadly weapons, and parts of, and machinery for manufacturing, arms, but does not include articles designed solely for domestic or agricultural uses such as a lathi or an ordinary walking stick and weapons incapable of being used otherwise than as toys or of being converted into serviceable weapons;  </li>
<li>Financial Services  </li>
<li>Grey market product  </li>
<li>Hazardous materials including but not limited to acid, fireworks, explosives, flammable adhesives, poison, hazardous chemical, oil-based paint and thinners (flammable liquids), industrial solvents, insecticides & pesticides, machinery (containing fuel), Fuel for camp stoves/lanterns/heating elements, infectious substances etc.  </li>
<li>Human remains or skeleton  </li>
<li>Invoices (including blank, pre-filled, or value added invoices or receipts), is strictly prohibited on the Site  </li>
<li>Liquefied Petroleum Gas cylinder  </li>
<li>Maps and literature where Indian external boundaries have been shown incorrectly  </li>
<li>Narcotic Drugs and Psychotropic Substances  </li>
<li>Government related items/equipment’s (like wireless with frequency used by Police, uniforms of Government officials including but not limited to Police/ Army, etc.)  </li>
<li>Prescription Medicines and Drugs  </li>
<li>Racially/ethnically/religiously offensive materials  </li>
<li>Radioactive Materials  </li>
<li>Reptile skins  </li>
<li>Sex Determination Kit </li> 
<li>Stocks and Securities  </li>
<li>Stolen Properties  </li>
<li>Unauthorized Copies of Intellectual Property  </li>
<li>Veterinary Drugs for animals  </li>
<li>Wildlife and Animal Products  </li>
<li>Any other sanctioned or prohibited items as per law.  </li>
<li>Passports, other government issued personal documents  </li>
<li>Any other items that are deemed unfit for listing by Indian Government </li>
<li>Any other items deemed unfit for listing by SimpliSell.co </li>

</ul>

								</div>

							</div>
						</div>
					</div>
				</section>
				<!-- end section -->
</main>
@endsection