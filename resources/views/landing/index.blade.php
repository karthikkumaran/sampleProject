@extends('layouts.landing')
@section('content')
@php 
$agent=new \Jenssegers\Agent\Agent();
@endphp
<style>
.custom-control-input {
    position: absolute;
    z-index: -1;
    opacity: 0;
}
input[type='checkbox'] {
    box-sizing: border-box;
    padding: 0;
}
.custom-switch .custom-control-label {
    height: 1.571rem;
    width: 3.1rem;
    padding: 0;
}
.custom-control-label {
    position: relative;
    margin-bottom: 0;
    vertical-align: top;
}
</style>
<!-- start start screen -->
<div id="home" class="start-screen start-screen--full-height start-screen--style-1">
    <div class="start-screen__bg-container">
        <div class="start-screen__bg" style="background-image: url({{ asset('landing/img/bg_1.png') }});background-position: center bottom;"></div>
    </div>

    <div class="start-screen__shapes  d-none d-lg-block">
        <img class="img-shape d-none d-xl-block" src="{{ asset('landing/img/blank.gif') }}" alt="simplisell" />

        <img class="img-shape" src="{{ asset('landing/img/shape_2.png') }}" alt="simplisell" />
    </div>

    <div class="start-screen__content-container">
        <div class="start-screen__content__item align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-10 col-lg-6 mt-4">
                         <h1><span>Create your <br>E-Commerce Store in 5 min <br>& Sell online</span></h1>

                        <p>
                        Setup your WhatsApp store instantly using SimpliSell and start selling online instantly 24X7
                        </p>

                        <div class="spacer py-2 py-sm-4"></div>

                        <div class="d-sm-table">
                            <div class="d-sm-table-cell  pb-5 pb-sm-0 pr-sm-8 pr-md-10">
                                <a class="custom-btn custom-btn--big custom-btn--style-1" href="{{ route('register') }}">Start your E-commerce Store</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end start screen -->

<!-- start main -->
<main role="main">
    <!-- Common styles
    ================================================== -->
    <link rel="stylesheet" href="{{ asset('landing/css/style.min.css') }}" type="text/css">

    <!-- Theme styles
    ================================================== -->
    <link rel="stylesheet" href="{{ asset('landing/css/color-7.min.css') }}" type="text/css">

    <!-- Load lazyLoad scripts
    ================================================== -->
    <script>
        (function(w, d){
            var m = d.getElementsByTagName('main')[0],
                s = d.createElement("script"),
                v = !("IntersectionObserver" in w) ? "8.17.0" : "12.0.0",
                o = {
                    elements_selector: ".lazy",
                    threshold: 500,
                    callback_enter: function (element) {

                    },
                    callback_load: function (element) {

                    },
                    callback_set: function (element) {

                        oTimeout = setTimeout(function ()
                        {
                            clearTimeout(oTimeout);

                            AOS.refresh();
                        }, 1000);
                    },
                    callback_error: function(element) {
                        element.src = "https://placeholdit.imgix.net/~text?txtsize=21&txt=Image%20not%20load&w=200&h=200";
                    }
                };
            s.type = 'text/javascript';
            s.async = false; // This includes the script as async. See the "recipes" section for more information about async loading of LazyLoad.
            s.src = "https://cdn.jsdelivr.net/npm/vanilla-lazyload@" + v + "/dist/lazyload.min.js";
            m.appendChild(s);
            // m.insertBefore(s, m.firstChild);
            w.lazyLoadOptions = o;
        }(window, document));
    </script>
     <!-- start section -->
     <section class="section section--no-pt">
        <div class="section-heading section-heading--center">
			<h6 class="__subtitle">Simple setup process</h6>
            <h2 class="__title">Simple 3 Step Process</h2>
        </div>

        <div class="spacer py-6"></div>
        <div class="container">
        <div class="row align-items-lg-center">
            <div class="col-12 col-lg-4 text-center">
                <h3>Step 1</h3>
                <h6>Create your account</h6><br>
                <img class="img-fluid lazy" width="289" height="585" src="{{asset('landing/img/blank.gif')}}" data-src="{{asset('landing/img/step_1.png')}}" data-srcset="{{asset('landing/img/step_1.png')}} 1x, {{asset('landing/img/step_1.png')}} 2x" alt="simplisell" />
            </div>
            <div class="col-12 col-lg-4 text-center">
                <h3>Step 2</h3>
                <h6>Upload your own products</h6><br>
                <img class="img-fluid lazy" width="289" height="585" src="{{asset('landing/img/blank.gif')}}" data-src="{{asset('landing/img/step_2.png')}}" data-srcset="{{asset('landing/img/step_2.png')}} 1x, {{asset('landing/img/step_2.png')}} 2x" alt="simplisell" />
            </div>
            <div class="col-12 col-lg-4 text-center">
                <h3>Step 3</h3>
                <h6>Start selling and collect <br>payment online</h6>
                <img class="img-fluid lazy" width="289" height="585" src="{{asset('landing/img/blank.gif')}}" data-src="{{asset('landing/img/step_3.png')}}" data-srcset="{{asset('landing/img/step_3.png')}} 1x, {{asset('landing/img/step_3.png')}} 2x" alt="simplisell" />
            </div>
        </div>
        </div>
    </section>

	<!-- start section -->
	<section class="section section--bg-img jarallax" data-speed="0.3" data-img-position="50% 70%" style="background-image: url({{asset('landing/img/base_bg_6.jpg')}});">
					<div class="container">
						<div class="section-heading section-heading--white section-heading--center">
							<h2 class="__title">Watch Demo Video</h2>
						</div>

						<div class="spacer py-6"></div>

						<div class="row justify-content-sm-center">
							<div class="col-12 col-md-10 col-lg-8">

								<!-- start video -->
								<div class="video-box video-box--s2">
									<figure class="__image">
										<img class="lazy" width="770" height="515" src="{{asset('landing/img/blank.gif')}}" data-src="{{asset('landing/img/video_bg.png')}}" alt="demo" />

										<a class="btn-play-link" href="https://www.youtube.com/watch?v=Yro_-JdwIdA&t=14s" data-fancybox="video-1">
											<span class="btn-play"></span>
										</a>
									</figure>
								</div>
								<!-- end video -->

							</div>
						</div>
					</div>

					<div class="shape">
						<svg xmlns="http://www.w3.org/2000/svg" width="1680" height="450" viewBox="0 0 1680 450" preserveAspectRatio="none" style="min-height: 180px">
							<path d="M0 3s561.937 57 842 55c286 0 484-18 839-58v450H-2z" style="mix-blend-mode:screen" fill="#fff" fill-rule="evenodd"/>
						</svg>
					</div>
				</section>
				<!-- end section -->

    <!-- start section -->
                 <section class="section section--no-pt">
					<div class="container">
						<div class="row flex-lg-row-reverse align-items-lg-center">
							<div class="col-12 col-lg-6">
								<div class="section-heading">
									<h6 class="__subtitle">Tell your customers</h6>

									<h2 class="__title">Promote your<br><span>E-commerce store in</span> <b style="color:#28C76F">WhatsApp</b></h2>
								</div>

								<div class="spacer py-4"></div>

								<div>
									<p>
                                    Your customers can order now 24x7 directly on your E-commerce and receive order updates and digital receipts on whatsApp.
									</p>

								</div>
							</div>

							<div class="spacer py-4 d-lg-none"></div>

							<div class="col-12 col-lg-6  text-center text-lg-left">

								<figure class="image-container js-tilt" data-tilt-speed="600" data-tilt-max="15" data-tilt-perspective="700">
									<img class="img-fluid lazy" width="547" height="469" src="{{asset('landing/img/blank.gif')}}" data-src="{{asset('landing/img/tell_your_customers_left.png')}}" alt="simplisell" />
								</figure>

							</div>
						</div>
					</div>
				</section>
				<!-- end section -->
   <!-- start section -->
             <section id="infoblock" class="section section--bg-img jarallax" data-speed="0.3" data-img-position="50% 50%" style="background-image: url({{asset('landing/img/base_bg.jpg')}});">
					<div class="container">
						<div class="row">
							<div class="col-12">

								<!-- start info block -->
								<div class="info-block info-block--s1">
									<div class="row align-items-md-center">
										<div class="col-12 col-md-6">
											<div class="section-heading section-heading--white">
												<h2 class="__title">Take SimpliSell <span>for a test drive</span></h2>
											</div>

											<div class="spacer py-4"></div>

											<div>
												<p>
													<a class="custom-btn custom-btn--medium custom-btn--style-4" href="{{ route('register') }}">&nbsp;&nbsp;Start Your Free Trial Now&nbsp;&nbsp;</a>
												</p>
											</div>
										</div>
									</div>
								</div>
								<!-- end info block -->

								<div class="spacer py-10 d-none d-md-block"></div>
								<div class="spacer py-4 d-none d-lg-block"></div>

							</div>
						</div>
					</div>

					<div class="shape">
						<svg xmlns="http://www.w3.org/2000/svg" width="1680" height="200" viewBox="0 0 1680 200" preserveAspectRatio="none" style="min-height: 200px">
							<path d="M0 3s561.937 57 842 55c286 0 484-18 839-58v450H-2z" style="mix-blend-mode:screen" fill="#fff" fill-rule="evenodd"/>
						</svg>
					</div>
				</section>
				<!-- end section -->
    <!-- start section -->
                 <section id="features" class="section section--no-pt">
                        <div class="section-heading section-heading--center">
							<h6 class="__subtitle">Features</h6>

							<h2 class="__title">Everything you need to run <br><span>your online store</span></h2>
						</div>

						<div class="spacer py-6"></div>
					<div class="container">
						<div class="row align-items-lg-center">
							<div class="col-12 col-lg-4 push-lg-4 text-center">
								<img class="img-fluid lazy" width="289" height="585" src="{{asset('landing/img/blank.gif')}}" data-src="{{asset('landing/img/feature.png')}}" data-srcset="{{asset('landing/img/feature.png')}} 1x, {{asset('landing/img/feature@2x.png')}} 2x" alt="simplisell" />
							</div>

							<div class="spacer py-5 d-lg-none"></div>

							<div class="col-12 col-lg-8">

								<!-- start feature -->
								<div class="feature feature--s1">
									<div class="__inner">
										<div class="row">
											<!-- start item -->
											<div class="col-12 col-md-6 pull-lg-6">
												<div class="__item  text-center text-md-right">
													<div class="row justify-content-sm-center flex-md-row-reverse">
														<div class="col-12 col-sm-10 col-md-12 col-lg-auto">
															<div class="mb-3 mb-lg-0">
																<i class="__ico">
																	<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g><g xmlns="http://www.w3.org/2000/svg"><path d="m135.758 119.209c4.142 0 7.5-3.358 7.5-7.5s-3.358-7.5-7.5-7.5h-16.549v-16.548c0-9.125 7.424-16.548 16.549-16.548h8.016c4.142 0 7.5-3.358 7.5-7.5s-3.358-7.5-7.5-7.5h-8.016c-17.396 0-31.549 14.152-31.549 31.548v16.548h-16.548c-4.142 0-7.5 3.358-7.5 7.5s3.358 7.5 7.5 7.5h16.548v56.629c0 4.142 3.358 7.5 7.5 7.5s7.5-3.358 7.5-7.5v-56.629z" fill="#735dad" data-original="#000000" style="" class=""/><path d="m399.774 272.79v-33.58c62.536-3.885 112.226-55.987 112.226-119.484 0-27.17-8.873-52.785-25.661-74.074-2.565-3.254-7.28-3.811-10.533-1.246-3.252 2.564-3.81 7.28-1.246 10.533 14.681 18.617 22.44 41.02 22.44 64.787 0 57.746-46.979 104.726-104.726 104.726s-104.726-46.98-104.726-104.726 46.98-104.726 104.726-104.726c21.549 0 42.244 6.492 59.847 18.773 3.398 2.37 8.072 1.539 10.442-1.859 2.37-3.397 1.538-8.072-1.859-10.442-20.133-14.047-43.796-21.472-68.43-21.472-63.497 0-115.599 49.69-119.484 112.226h-33.58c-3.885-62.536-55.987-112.226-119.484-112.226-66.018 0-119.726 53.708-119.726 119.726 0 63.498 49.69 115.599 112.226 119.484v33.58c-62.536 3.885-112.226 55.987-112.226 119.484 0 66.018 53.708 119.726 119.726 119.726 63.498 0 115.599-49.69 119.484-112.226h33.58c3.885 62.536 55.987 112.226 119.484 112.226 66.018 0 119.726-53.708 119.726-119.726 0-63.497-49.69-115.599-112.226-119.484zm-384.774-153.064c0-57.747 46.979-104.726 104.726-104.726s104.726 46.979 104.726 104.726-46.98 104.726-104.726 104.726-104.726-46.98-104.726-104.726zm104.726 377.274c-57.747 0-104.726-46.979-104.726-104.726s46.979-104.726 104.726-104.726c21.549 0 42.244 6.492 59.847 18.773 3.398 2.37 8.073 1.538 10.442-1.859 2.37-3.397 1.538-8.072-1.859-10.442-18.08-12.614-39.01-19.868-60.93-21.22v-33.59c60.055-3.731 108.253-51.93 111.984-111.984h33.58c3.731 60.055 51.93 108.253 111.984 111.984v33.58c-60.055 3.731-108.253 51.93-111.984 111.984h-33.592c-1.484-24.372-10.174-47.256-25.407-66.575-2.565-3.253-7.28-3.81-10.534-1.245-3.252 2.564-3.81 7.28-1.246 10.533 14.68 18.618 22.439 41.021 22.439 64.788.002 57.746-46.978 104.725-104.724 104.725zm272.548 0c-57.746 0-104.726-46.979-104.726-104.726s46.98-104.726 104.726-104.726 104.726 46.98 104.726 104.726-46.979 104.726-104.726 104.726z" fill="#735dad" data-original="#000000" style="" class=""/><path d="m376.801 73.903c-5.199-2.6-11.253-2.327-16.196.729-4.944 3.055-7.896 8.349-7.896 14.161v61.868c0 5.812 2.952 11.105 7.896 14.161 2.69 1.663 5.709 2.501 8.741 2.501 2.539 0 5.086-.588 7.455-1.773l61.868-30.934c5.676-2.838 9.202-8.543 9.202-14.889s-3.526-12.051-9.202-14.889zm55.16 47.296-61.869 30.934c-.678.339-1.245.148-1.602-.072s-.781-.642-.781-1.401v-61.868c0-.759.425-1.181.781-1.401.216-.134.51-.257.86-.257.226 0 .476.051.742.185l61.868 30.934c.604.302.91.797.91 1.473s-.306 1.17-.909 1.473z" fill="#735dad" data-original="#000000" style="" class=""/><path d="m151.791 336.677h-64.13c-12.976 0-23.532 10.557-23.532 23.532v64.129c0 12.976 10.557 23.532 23.532 23.532h64.129c12.976 0 23.532-10.557 23.532-23.532v-64.129c.001-12.975-10.556-23.532-23.531-23.532zm8.532 87.662c0 4.705-3.828 8.532-8.532 8.532h-64.13c-4.705 0-8.532-3.828-8.532-8.532v-64.129c0-4.705 3.828-8.532 8.532-8.532h64.129c4.705 0 8.532 3.828 8.532 8.532v64.129z" fill="#735dad" data-original="#000000" style="" class=""/><path d="m119.726 368.742c-12.976 0-23.532 10.557-23.532 23.533s10.557 23.532 23.532 23.532c12.976 0 23.533-10.557 23.533-23.532-.001-12.977-10.557-23.533-23.533-23.533zm0 32.065c-4.705 0-8.532-3.828-8.532-8.532 0-4.705 3.828-8.533 8.532-8.533 4.705 0 8.533 3.828 8.533 8.533-.001 4.704-3.828 8.532-8.533 8.532z" fill="#735dad" data-original="#000000" style="" class=""/><path d="m392.274 328.661c-35.077 0-63.613 28.537-63.613 63.613 0 11.496 3.063 22.625 8.895 32.432l-8.358 20.895c-1.114 2.786-.461 5.967 1.66 8.089 1.434 1.434 3.352 2.197 5.305 2.197.937 0 1.881-.175 2.784-.537l20.895-8.358c9.807 5.831 20.936 8.895 32.433 8.895 35.076 0 63.613-28.537 63.613-63.613-.001-35.076-28.537-63.613-63.614-63.613zm0 112.226c-9.83 0-19.308-2.931-27.41-8.476-1.266-.867-2.746-1.311-4.237-1.311-.941 0-1.886.177-2.785.537l-8.218 3.288 3.287-8.219c.93-2.323.639-4.957-.774-7.022-5.545-8.102-8.476-17.58-8.476-27.409 0-26.806 21.808-48.613 48.613-48.613s48.613 21.808 48.613 48.613-21.807 48.612-48.613 48.612z" fill="#735dad" data-original="#000000" style="" class=""/><path d="m403.976 403.976c-3.125 3.125-7.281 4.847-11.701 4.847s-8.576-1.722-11.702-4.847-4.847-7.281-4.847-11.701 1.721-8.576 4.847-11.702c2.929-2.929 2.929-7.678 0-10.606-2.929-2.929-7.678-2.929-10.606 0-5.959 5.958-9.24 13.881-9.24 22.308s3.281 16.349 9.24 22.308c5.958 5.959 13.881 9.241 22.308 9.241s16.349-3.282 22.308-9.241c2.929-2.929 2.929-7.678 0-10.606-2.93-2.931-7.679-2.93-10.607-.001z" fill="#735dad" data-original="#000000" style="" class=""/></g></g></svg>
																</i>
															</div>
														</div>

														<div class="col-12 col-sm-10 col-md-12 col-lg">
															<h4 class="__title">Sell on WhatsApp and Facebook</h4>

															<p>
                                                                Easily share catalogue link on WhatsApp, Facebook, Email or SMS to your Customers and they can simply shop with the click of a button.
															</p>
														</div>
													</div>
												</div>
											</div>
											<!-- end item -->

											<!-- start item -->
											<div class="col-12 col-md-6">
												<div class="__item  text-center text-md-left">
													<div class="row justify-content-sm-center">
														<div class="col-12 col-sm-10 col-md-12 col-lg-auto">
															<div class="mb-3 mb-lg-0">
																<i class="__ico">
																	<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g><path xmlns="http://www.w3.org/2000/svg" d="m376 16h-176a40.045 40.045 0 0 0 -40 40v152h-24a40.045 40.045 0 0 0 -40 40v100.687l-24 24-10.343-10.344a8 8 0 0 0 -11.314 0l-32 32a8 8 0 0 0 0 11.314l80 80a8 8 0 0 0 11.314 0l32-32a8 8 0 0 0 0-11.314l-2.343-2.343 20.686-20.686v4.686a40.045 40.045 0 0 0 40 40h176a40.045 40.045 0 0 0 40-40v-368a40.045 40.045 0 0 0 -40-40zm-272 452.687-68.686-68.687 20.686-20.687 68.686 68.687zm-20.686-84.687 26.343-26.343a8 8 0 0 0 2.343-5.657v-104a24.028 24.028 0 0 1 24-24h24v37.754l-22.06-5.515-3.88 15.522 18.292 4.573-14.009 14.009 11.314 11.314 40-40a14.628 14.628 0 0 1 20.686 20.687l-40 40a8 8 0 0 0 0 11.314 37.254 37.254 0 0 1 0 52.686l-42.343 42.342zm316.686 40a24.027 24.027 0 0 1 -24 24h-176a24.027 24.027 0 0 1 -24-24v-20.686l5.657-5.657a53.282 53.282 0 0 0 5.238-69.238l34.762-34.763a30.627 30.627 0 1 0 -43.314-43.313l-2.343 2.343v-196.686a24.027 24.027 0 0 1 24-24h176a24.027 24.027 0 0 1 24 24z" fill="#ff9cda" data-original="#000000" style="" class=""/><path xmlns="http://www.w3.org/2000/svg" d="m280 416h16v16h-16z" fill="#ff9cda" data-original="#000000" style="" class=""/><path xmlns="http://www.w3.org/2000/svg" d="m296 48h16v16h-16z" fill="#ff9cda" data-original="#000000" style="" class=""/><path xmlns="http://www.w3.org/2000/svg" d="m264 48h16v16h-16z" fill="#ff9cda" data-original="#000000" style="" class=""/><path xmlns="http://www.w3.org/2000/svg" d="m216 336h144v16h-144z" fill="#ff9cda" data-original="#000000" style="" class=""/><path xmlns="http://www.w3.org/2000/svg" d="m216 368h128v16h-128z" fill="#ff9cda" data-original="#000000" style="" class=""/><path xmlns="http://www.w3.org/2000/svg" d="m232 304h128v16h-128z" fill="#ff9cda" data-original="#000000" style="" class=""/><path xmlns="http://www.w3.org/2000/svg" d="m280 272h80v16h-80z" fill="#ff9cda" data-original="#000000" style="" class=""/><path xmlns="http://www.w3.org/2000/svg" d="m248 272h16v16h-16z" fill="#ff9cda" data-original="#000000" style="" class=""/><path xmlns="http://www.w3.org/2000/svg" d="m432 120h16v200h-16z" fill="#ff9cda" data-original="#000000" style="" class=""/><path xmlns="http://www.w3.org/2000/svg" d="m432 336h16v16h-16z" fill="#ff9cda" data-original="#000000" style="" class=""/><path xmlns="http://www.w3.org/2000/svg" d="m371.151 120.646-56-24a8 8 0 0 0 -8.808 1.7l-18.343 18.341-18.343-18.344a8 8 0 0 0 -8.808-1.7l-56 24a8 8 0 0 0 -4.61 9.294l8 32a8 8 0 0 0 9.7 5.821l22.061-5.512v69.754a8 8 0 0 0 8 8h80a8 8 0 0 0 8-8v-69.754l22.06 5.515a8 8 0 0 0 9.7-5.821l8-32a8 8 0 0 0 -4.609-9.294zm-16.972 29.654-24.239-6.06a8 8 0 0 0 -9.94 7.76v72h-64v-72a8 8 0 0 0 -9.94-7.761l-24.239 6.06-4.407-17.63 44.759-19.183 20.17 20.171a8 8 0 0 0 11.314 0l20.17-20.171 44.759 19.183z" fill="#ff9cda" data-original="#000000" style="" class=""/></g></svg>
																</i>
															</div>
														</div>

														<div class="col-12 col-sm-10 col-md-12 col-lg">
															<h4 class="__title">Catalogues</h4>

															<p>
                                                            Create as many catalogues as you like. You can create a special catalog for customer.
															</p>
														</div>
													</div>
												</div>
											</div>
											<!-- end item -->
										</div>

										<div class="spacer py-lg-3 py-xl-7 d-none d-lg-block"></div>

										<div class="row">
											<!-- start item -->
											<div class="col-12 col-md-6 pull-lg-6">
												<div class="__item  text-center text-md-right">
													<div class="row justify-content-sm-center flex-md-row-reverse">
														<div class="col-12 col-sm-10 col-md-12 col-lg-auto">
															<div class="mb-3 mb-lg-0">
																<i class="__ico">
																	<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" x="0" y="0" viewBox="0 0 300 300" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g><g xmlns="http://www.w3.org/2000/svg"><path d="m64 58c7.719 0 14-6.281 14-14s-6.281-14-14-14-14 6.281-14 14 6.281 14 14 14zm0-20c3.309 0 6 2.691 6 6s-2.691 6-6 6-6-2.691-6-6 2.691-6 6-6z" fill="#eb5b1c" data-original="#000000" style="" class=""/><circle cx="256" cy="78" r="5" fill="#eb5b1c" data-original="#000000" style="" class=""/><path d="m128.102 80.238c.637.375 1.336.555 2.027.555 1.371 0 2.703-.703 3.449-1.969 4.66-7.91 13.254-12.824 22.422-12.824 2.211 0 4-1.789 4-4s-1.789-4-4-4c-11.988 0-23.223 6.422-29.312 16.762-1.122 1.902-.489 4.355 1.414 5.476z" fill="#eb5b1c" data-original="#000000" style="" class=""/><path d="m114 230h-4c-8.824 0-16-7.176-16-16v-40h111c21.504 0 39-17.371 39-38.727 0-21.027-16.969-38.195-38.023-38.711.242-1.988.363-3.973.363-5.934 0-28.105-23.031-50.969-51.34-50.969-21.992 0-41.27 13.809-48.406 34.113-3.707-1.336-7.641-2.027-11.594-2.027-15.527 0-28.824 10.289-32.785 24.902-20.211 1.423-36.215 18.205-36.215 38.626 0 21.356 17.496 38.727 39 38.727h21v40c0 13.234 10.766 24 24 24h4c2.211 0 4-1.789 4-4s-1.789-4-4-4zm-49-64c-17.094 0-31-13.785-31-30.727s13.906-30.723 30.961-30.723c.09.008.395.02.484.02 1.953 0 3.621-1.41 3.945-3.336 2.098-12.449 12.872-21.488 25.61-21.488 4.262 0 8.496 1.051 12.242 3.039 1.086.574 2.375.617 3.5.121 1.125-.5 1.953-1.488 2.254-2.68 4.82-19.175 22.094-32.566 42.004-32.566 23.898 0 43.34 19.277 43.34 42.969 0 3.043-.352 6.16-1.043 9.266-.277 1.254.066 2.57.922 3.531.859.965 2.141 1.461 3.406 1.312 1.113-.117 2.234-.188 3.375-.188 17.094 0 31 13.781 31 30.723s-13.906 30.727-31 30.727h-111v-20.934c0-6.101 4.965-11.066 11.066-11.066h4.934l-6.399 4.801c-1.77 1.324-2.129 3.832-.801 5.598.785 1.051 1.984 1.602 3.203 1.602.836 0 1.676-.262 2.395-.801l16-12c1.008-.754 1.602-1.941 1.602-3.199s-.594-2.445-1.602-3.199l-16-12c-1.762-1.324-4.273-.969-5.598.801-1.328 1.766-.969 4.273.801 5.598l6.399 4.799h-4.934c-10.511 0-19.066 8.555-19.066 19.066v20.934z" fill="#eb5b1c" data-original="#000000" style="" class=""/><path d="m190 194h-64c-2.211 0-4 1.789-4 4v68c0 2.211 1.789 4 4 4h64c2.211 0 4-1.789 4-4v-68c0-2.211-1.789-4-4-4zm-4 8v14.001l-18.527 21.28-18.734-7.027c-1.422-.531-3.027-.211-4.141.828l-14.598 13.686v-42.768zm0 26.179v21.821h-49.887l12.141-11.383 19.008 7.129c1.566.586 3.324.141 4.422-1.121zm-56 33.821v-4h56v4z" fill="#eb5b1c" data-original="#000000" style="" class=""/><path d="m144 226c5.516 0 10-4.484 10-10s-4.484-10-10-10-10 4.484-10 10 4.484 10 10 10zm0-12c1.102 0 2 .898 2 2s-.898 2-2 2-2-.898-2-2 .898-2 2-2z" fill="#eb5b1c" data-original="#000000" style="" class=""/><path d="m270 194h-64c-2.211 0-4 1.789-4 4v68c0 2.211 1.789 4 4 4h64c2.211 0 4-1.789 4-4v-68c0-2.211-1.789-4-4-4zm-4 8v14.001l-18.527 21.28-18.734-7.027c-1.422-.531-3.027-.211-4.141.828l-14.598 13.686v-42.768zm0 26.179v21.821h-49.887l12.141-11.383 19.008 7.129c1.566.586 3.328.141 4.422-1.121zm-56 33.821v-4h56v4z" fill="#eb5b1c" data-original="#000000" style="" class=""/><path d="m224 226c5.516 0 10-4.484 10-10s-4.484-10-10-10-10 4.484-10 10 4.484 10 10 10zm0-12c1.102 0 2 .898 2 2s-.898 2-2 2-2-.898-2-2 .898-2 2-2z" fill="#eb5b1c" data-original="#000000" style="" class=""/></g></g></svg>
																</i>
															</div>
														</div>

														<div class="col-12 col-sm-10 col-md-12 col-lg">
															<h4 class="__title">Smart product upload</h4>

															<p>
                                                            Upload one or more product images in just one click and use our smart editor to quickly update the product details.
															</p>
														</div>
													</div>
												</div>
											</div>
											<!-- end item -->

											<!-- start item -->
											<div class="col-12 col-md-6">
												<div class="__item  text-center text-md-left">
													<div class="row justify-content-sm-center">
														<div class="col-12 col-sm-10 col-md-12 col-lg-auto">
															<div class="mb-3 mb-lg-0">
																<i class="__ico">
																	<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g><path xmlns="http://www.w3.org/2000/svg" d="m378.90625 394.292969h-210.046875c-4.171875 0-7.554687 3.386719-7.554687 7.558593 0 4.171876 3.382812 7.554688 7.554687 7.554688h210.042969c4.175781 0 7.558594-3.382812 7.558594-7.554688 0-4.171874-3.382813-7.558593-7.554688-7.558593zm0 0" fill="#b45bc4" data-original="#000000" style="" class=""/><path xmlns="http://www.w3.org/2000/svg" d="m378.90625 341.214844h-210.046875c-4.171875 0-7.554687 3.382812-7.554687 7.554687 0 4.175781 3.382812 7.558594 7.554687 7.558594h210.042969c4.175781 0 7.558594-3.382813 7.558594-7.558594 0-4.171875-3.382813-7.554687-7.554688-7.554687zm0 0" fill="#b45bc4" data-original="#000000" style="" class=""/><path xmlns="http://www.w3.org/2000/svg" d="m161.304688 295.660156c0 4.175782 3.382812 7.558594 7.554687 7.558594h133.660156c4.175781 0 7.558594-3.382812 7.558594-7.558594 0-4.171875-3.382813-7.554687-7.558594-7.554687h-133.660156c-4.171875 0-7.554687 3.382812-7.554687 7.554687zm0 0" fill="#b45bc4" data-original="#000000" style="" class=""/><path xmlns="http://www.w3.org/2000/svg" d="m458.664062 99.132812-13.625-14.789062v-52.894531c0-17.339844-14.148437-31.449219-31.542968-31.449219h-338.003906c-17.394532 0-31.542969 14.109375-31.542969 31.449219v59.664062h-13.820313c-16.613281 0-30.128906 13.523438-30.128906 30.144531v91.480469c0 16.621094 13.515625 30.144531 30.128906 30.144531h13.820313v210.800782c0 17.339844 14.148437 31.449218 31.542969 31.449218h17.265624c1.859376 15.121094 14.777344 26.867188 30.398438 26.867188h325.1875c16.910156 0 30.664062-13.738281 30.664062-30.625v-326.421875c0-26.714844-.804687-34.578125-20.34375-55.820313zm4.917969 40.269532h-56.609375c-8.574218 0-15.550781-6.957032-15.550781-15.503906l.050781-65.714844c9.9375.738281 12.589844 3.675781 21.300782 13.363281 1.445312 1.609375 3.023437 3.359375 4.777343 5.265625l14.445313 15.679688c.050781.054687.101562.113281.15625.167968l15.390625 16.707032c12 13.046874 15.191406 18.523437 16.039062 30.035156zm-404.519531-107.953125c0-9.007813 7.371094-16.335938 16.429688-16.335938h338.003906c9.058594 0 16.429687 7.328125 16.429687 16.335938v36.488281l-1.257812-1.367188c-1.714844-1.855468-3.246094-3.5625-4.652344-5.128906-12.421875-13.8125-17.851563-18.589844-40.085937-18.589844h-.003907-260.769531c-16.894531 0-30.636719 13.738282-30.636719 30.625v17.636719h-33.457031zm404.832031 449.925781c0 8.550781-6.976562 15.511719-15.550781 15.511719h-325.1875c-8.558594 0-15.523438-6.957031-15.523438-15.511719v-109.507812c0-4.171876-3.382812-7.554688-7.558593-7.554688-4.171875 0-7.554688 3.382812-7.554688 7.554688v98.152343h-17.027343c-9.058594 0-16.429688-7.328125-16.429688-16.332031v-210.804688h33.457031v94.382813c0 4.171875 3.382813 7.554687 7.554688 7.554687 4.175781 0 7.558593-3.382812 7.558593-7.554687v-94.382813h39.226563c4.171875 0 7.554687-3.382812 7.554687-7.558593 0-4.171875-3.382812-7.554688-7.554687-7.554688h-116.730469c-8.28125 0-15.015625-6.742187-15.015625-15.03125v-91.480469c0-8.289062 6.734375-15.03125 15.015625-15.03125h199.011719c8.292969 0 15.042969 6.742188 15.042969 15.03125v91.480469c0 8.289063-6.75 15.03125-15.042969 15.03125h-47.660156c-4.175781 0-7.558594 3.382813-7.558594 7.554688 0 4.175781 3.382813 7.558593 7.558594 7.558593h47.660156c16.628906 0 30.15625-13.523437 30.15625-30.144531v-91.480469c0-16.621093-13.527344-30.144531-30.15625-30.144531h-121.507813v-17.636719c0-8.554687 6.964844-15.511718 15.523438-15.511718h253.203125l-.050781 65.929687c0 16.886719 13.757812 30.625 30.664062 30.625h56.921875v.433594zm0 0" fill="#b45bc4" data-original="#000000" style="" class=""/><path xmlns="http://www.w3.org/2000/svg" d="m75.648438 134.636719h-18.101563c-2.222656 0-4.113281.777343-5.667969 2.335937-1.554687 1.554688-2.332031 3.472656-2.332031 5.757813v53.515625c0 2.222656.761719 4.09375 2.285156 5.617187 1.523438 1.527344 3.335938 2.289063 5.429688 2.289063 2.097656 0 3.90625-.761719 5.429687-2.289063 1.527344-1.523437 2.289063-3.332031 2.289063-5.425781v-15.519531h11.621093c7.050782 0 12.703126-2.109375 16.957032-6.332031 4.253906-4.222657 6.382812-9.761719 6.382812-16.617188 0-6.917969-2.253906-12.539062-6.761718-16.855469-4.511719-4.316406-10.355469-6.476562-17.53125-6.476562zm5.621093 29.996093c-1.84375 1.84375-4 2.761719-6.476562 2.761719h-9.8125v-18.949219h9.90625c2.605469 0 4.777343.9375 6.527343 2.808594 1.746094 1.875 2.617188 4.144532 2.617188 6.808594 0 2.539062-.917969 4.730469-2.761719 6.570312zm0 0" fill="#b45bc4" data-original="#000000" style="" class=""/><path xmlns="http://www.w3.org/2000/svg" d="m163.273438 152.777344c-2.253907-4.917969-5.414063-8.871094-9.476563-11.855469-5.652344-4.125-12.957031-6.1875-21.910156-6.1875h-15.910157c-2.226562 0-4.097656.792969-5.621093 2.378906-1.527344 1.589844-2.289063 3.398438-2.289063 5.429688v53.039062c0 2.285157.730469 4.15625 2.191406 5.617188 1.585938 1.398437 3.523438 2.09375 5.8125 2.09375h17.148438c9.269531 0 16.703125-2.507813 22.292969-7.523438 7.429687-6.789062 11.144531-15.773437 11.144531-26.945312 0-5.777344-1.128906-11.125-3.382812-16.046875zm-17.480469 30.996094c-3.304688 3.746093-7.683594 5.617187-13.148438 5.617187h-9.144531v-40.660156h8.289062c5.84375 0 10.464844 1.808593 13.863282 5.429687 3.394531 3.617188 5.09375 8.503906 5.09375 14.664063 0 6.222656-1.652344 11.203125-4.953125 14.949219zm0 0" fill="#b45bc4" data-original="#000000" style="" class=""/><path xmlns="http://www.w3.org/2000/svg" d="m184.402344 204.34375c2.15625 0 3.984375-.765625 5.476562-2.289062 1.492188-1.523438 2.238282-3.332032 2.238282-5.425782v-20.472656h14.671874c2.097657 0 3.84375-.679688 5.238282-2.046875 1.398437-1.363281 2.097656-3 2.097656-4.90625 0-1.964844-.714844-3.632813-2.144531-4.996094-1.429688-1.363281-3.160157-2.046875-5.191407-2.046875h-14.671874v-13.429687h17.339843c1.96875 0 3.621094-.695313 4.953125-2.09375 1.335938-1.394531 2-3.015625 2-4.855469 0-2.03125-.679687-3.730469-2.046875-5.09375-1.367187-1.367188-3-2.046875-4.90625-2.046875h-24.960937c-2.222656 0-4.097656.792969-5.621094 2.378906-1.523438 1.589844-2.285156 3.429688-2.285156 5.523438v54.085937c0 2.097656.792968 3.90625 2.382812 5.429688 1.585938 1.519531 3.394532 2.285156 5.429688 2.285156zm0 0" fill="#b45bc4" data-original="#000000" style="" class=""/></g></svg>
																</i>
															</div>
														</div>

														<div class="col-12 col-sm-10 col-md-12 col-lg">
															<h4 class="__title">Professional PDF</h4>

															<p>
                                                            Generate and share professional Catalog PDF for your corporate customers with a click of a button.
															</p>
														</div>
													</div>
												</div>
											</div>
											<!-- end item -->
										</div>

										<div class="spacer py-lg-3 py-xl-7 d-none d-lg-block"></div>

										<div class="row">
											<!-- start item -->
											<div class="col-12 col-md-6 pull-lg-6">
												<div class="__item  text-center text-md-right">
													<div class="row justify-content-sm-center flex-md-row-reverse">
														<div class="col-12 col-sm-10 col-md-12 col-lg-auto">
															<div class="mb-3 mb-lg-0">
																<i class="__ico">
																	<svg fill="#ffba33" width="50" height="55" viewBox="-51 0 512 512" xmlns="http://www.w3.org/2000/svg"><path d="M51.223 512.047h307.199c28.265-.027 51.172-22.934 51.203-51.2V102.446c-.031-28.265-22.938-51.172-51.203-51.199h-57.856a25.458 25.458 0 0 0-18.941-8.531h-26.309C251.172 18.059 229.824 0 204.824 0c-25.004 0-46.347 18.059-50.492 42.715h-26.309a25.466 25.466 0 0 0-18.945 8.531H51.223C22.957 51.273.05 74.18.023 102.446v358.402c.028 28.265 22.934 51.172 51.2 51.199zm307.199-128h-59.73c-9.426 0-17.067 7.64-17.067 17.066v59.735H59.758a8.534 8.534 0 0 1-8.535-8.535V110.98a8.534 8.534 0 0 1 8.535-8.535h42.664c0 14.14 11.465 25.602 25.601 25.602h153.602c14.137 0 25.598-11.461 25.598-25.602h42.668c4.71 0 8.53 3.82 8.53 8.535zm-12.067 17.066l-47.664 47.668v-47.668zM128.023 59.781h34.133a8.534 8.534 0 0 0 8.535-8.535c0-18.851 15.282-34.133 34.133-34.133 18.852 0 34.133 15.282 34.133 34.133a8.533 8.533 0 0 0 8.531 8.535h34.137c4.71 0 8.531 3.82 8.531 8.532v34.132a8.533 8.533 0 0 1-8.531 8.535H128.023a8.534 8.534 0 0 1-8.535-8.535V68.313a8.533 8.533 0 0 1 8.535-8.532zM17.09 102.445c0-18.851 15.281-34.132 34.133-34.132h51.199v17.066H59.758c-14.14 0-25.602 11.46-25.602 25.601v341.333c0 14.14 11.461 25.601 25.602 25.601H283.09a25.492 25.492 0 0 0 10.242-2.184c.16-.062.29-.164.441-.23a25.639 25.639 0 0 0 7.399-5.121l66.8-66.8a25.55 25.55 0 0 0 5.06-7.356c.085-.18.198-.325.265-.504a25.452 25.452 0 0 0 2.176-10.239v-274.5c0-14.14-11.461-25.601-25.602-25.601h-42.648V68.312h51.199c18.851 0 34.133 15.282 34.133 34.133v358.403c0 18.851-15.282 34.132-34.133 34.132h-307.2c-18.85 0-34.132-15.28-34.132-34.132zm0 0"/><path d="M162.156 93.914h85.332a8.534 8.534 0 1 0 0-17.066h-85.332a8.532 8.532 0 0 0-8.531 8.53 8.533 8.533 0 0 0 8.531 8.536zm-17.066 128c-4.711 0-8.535 3.82-8.535 8.531v8.535h-51.2v-51.199h25.602a8.533 8.533 0 0 0 8.531-8.535c0-4.71-3.82-8.531-8.531-8.531H85.355c-9.425 0-17.066 7.64-17.066 17.066v51.2c0 9.425 7.64 17.066 17.066 17.066h51.2c9.425 0 17.07-7.64 17.07-17.067v-8.535c0-4.71-3.82-8.53-8.535-8.53zm-8.535 68.266h-51.2c-9.425 0-17.066 7.64-17.066 17.066v51.2c0 9.425 7.64 17.066 17.066 17.066h51.2c9.425 0 17.07-7.64 17.07-17.067v-51.199c0-9.426-7.645-17.066-17.07-17.066zm-51.2 68.265v-51.199h51.2v51.2zm93.868-153.597h42.668c4.71 0 8.53-3.82 8.53-8.536 0-4.71-3.82-8.53-8.53-8.53h-42.668a8.532 8.532 0 0 0-8.532 8.53 8.533 8.533 0 0 0 8.532 8.536zm153.601-17.067h-76.8a8.533 8.533 0 1 0 0 17.067h76.8c4.711 0 8.531-3.82 8.531-8.536 0-4.71-3.82-8.53-8.53-8.53zM179.223 238.98h85.332a8.534 8.534 0 1 0 0-17.066h-85.332a8.532 8.532 0 0 0-8.532 8.531 8.533 8.533 0 0 0 8.532 8.535zm153.601-17.066h-34.133a8.533 8.533 0 1 0 0 17.066h34.133a8.533 8.533 0 0 0 8.531-8.535c0-4.71-3.82-8.53-8.53-8.53zM179.223 324.313h42.668c4.71 0 8.53-3.82 8.53-8.532a8.533 8.533 0 0 0-8.53-8.535h-42.668a8.533 8.533 0 0 0-8.532 8.535 8.532 8.532 0 0 0 8.532 8.531zm153.601-17.067h-76.8a8.534 8.534 0 1 0 0 17.067h76.8a8.532 8.532 0 0 0 8.531-8.532 8.533 8.533 0 0 0-8.53-8.535zm-68.269 34.133h-85.332a8.533 8.533 0 0 0-8.532 8.535 8.532 8.532 0 0 0 8.532 8.531h85.332a8.533 8.533 0 1 0 0-17.066zm68.269 0h-34.133a8.534 8.534 0 1 0 0 17.066h34.133a8.533 8.533 0 0 0 0-17.066zm0 0"/><path d="M155.219 157.223l-36.82 51.53-9.942-9.94a8.53 8.53 0 0 0-11.96.105 8.53 8.53 0 0 0-.106 11.96l17.066 17.067a8.527 8.527 0 0 0 6.031 2.5h.703a8.523 8.523 0 0 0 6.235-3.547l42.668-59.734a8.533 8.533 0 1 0-13.875-9.918zm0 0"/></svg>
																</i>
															</div>
														</div>

														<div class="col-12 col-sm-10 col-md-12 col-lg">
															<h4 class="__title">Simple order management</h4>

															<p>
                                                            Manage all your orders at one place and keep track of each of them.
															</p>
														</div>
													</div>
												</div>
											</div>
											<!-- end item -->

											<!-- start item -->
											<div class="col-12 col-md-6">
												<div class="__item  text-center text-md-left">
													<div class="row justify-content-sm-center">
														<div class="col-12 col-sm-10 col-md-12 col-lg-auto">
															<div class="mb-3 mb-lg-0">
																<i class="__ico">
																	<svg height="50" viewBox="0 0 64 64" width="50" xmlns="http://www.w3.org/2000/svg"><g id="POS"><path d="m51 38a5 5 0 0 0 10 0v-3h-10z" fill="#bddbff"/><path d="m21 7v16h20v-15a4.985 4.985 0 0 1 5-5h-21a4 4 0 0 0 -4 4z" fill="#bddbff"/><path d="m37 53h4v4h-4z" fill="#bddbff"/><path d="m37 45h4v4h-4z" fill="#bddbff"/><path d="m37 37h4v4h-4z" fill="#bddbff"/><path d="m29 53h4v4h-4z" fill="#bddbff"/><path d="m29 45h4v4h-4z" fill="#bddbff"/><path d="m29 37h4v4h-4z" fill="#bddbff"/><path d="m5 57h16a2.006 2.006 0 0 0 2-2v-22h18v-4h-36a2.006 2.006 0 0 0 -2 2v24a2.006 2.006 0 0 0 2 2zm14-4h-4v-4h4zm0-8h-4v-4h4zm-12-12h12v4h-12zm0 8h4v4h-4zm0 8h4v4h-4z" fill="#bddbff"/><g fill="#57a4ff"><path d="m61 34h-9v-26a6.006 6.006 0 0 0 -6-6h-21a5.006 5.006 0 0 0 -5 5v7.1a5.009 5.009 0 0 0 -4 4.9v9h-11a3 3 0 0 0 -3 3v24a3 3 0 0 0 3 3h11.1a5.009 5.009 0 0 0 4.9 4h20a5.006 5.006 0 0 0 5-5v-13h10a6.006 6.006 0 0 0 6-6v-3a1 1 0 0 0 -1-1zm-39-27a3 3 0 0 1 3-3h16.531a5.98 5.98 0 0 0 -1.531 4v14h-18zm-18 48v-24a1 1 0 0 1 1-1h16a1 1 0 0 1 1 1v24a1 1 0 0 1 -1 1h-16a1 1 0 0 1 -1-1zm19.816-25h16.184v2h-16v-1a2.966 2.966 0 0 0 -.184-1zm20.184 27a3 3 0 0 1 -3 3h-20a3 3 0 0 1 -2.816-2h2.816a3 3 0 0 0 3-3v-21h17a1 1 0 0 0 1-1v-4a1 1 0 0 0 -1-1h-23v-9a3 3 0 0 1 2-2.816v6.816a1 1 0 0 0 1 1h20a1 1 0 0 0 1-1v-6.816a3 3 0 0 1 2 2.816zm-2-42.9v-6.1a4 4 0 0 1 8 0v30a5.969 5.969 0 0 0 1.54 4h-5.54v-23a5.009 5.009 0 0 0 -4-4.9zm18 23.9a4 4 0 0 1 -8 0v-2h8z"/><path d="m33 36h-4a1 1 0 0 0 -1 1v4a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1v-4a1 1 0 0 0 -1-1zm-1 4h-2v-2h2z"/><path d="m41 36h-4a1 1 0 0 0 -1 1v4a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1v-4a1 1 0 0 0 -1-1zm-1 4h-2v-2h2z"/><path d="m33 44h-4a1 1 0 0 0 -1 1v4a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1v-4a1 1 0 0 0 -1-1zm-1 4h-2v-2h2z"/><path d="m41 44h-4a1 1 0 0 0 -1 1v4a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1v-4a1 1 0 0 0 -1-1zm-1 4h-2v-2h2z"/><path d="m33 52h-4a1 1 0 0 0 -1 1v4a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1v-4a1 1 0 0 0 -1-1zm-1 4h-2v-2h2z"/><path d="m41 52h-4a1 1 0 0 0 -1 1v4a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1v-4a1 1 0 0 0 -1-1zm-1 4h-2v-2h2z"/><path d="m24 6h6v2h-6z"/><path d="m32 6h6v2h-6z"/><path d="m24 10h6v2h-6z"/><path d="m32 10h6v2h-6z"/><path d="m24 14h6v2h-6z"/><path d="m32 14h6v2h-6z"/><path d="m24 18h6v2h-6z"/><path d="m32 18h6v2h-6z"/><path d="m19 32h-12a1 1 0 0 0 -1 1v4a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-4a1 1 0 0 0 -1-1zm-1 4h-10v-2h10z"/><path d="m11 40h-4a1 1 0 0 0 -1 1v4a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1v-4a1 1 0 0 0 -1-1zm-1 4h-2v-2h2z"/><path d="m19 40h-4a1 1 0 0 0 -1 1v4a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1v-4a1 1 0 0 0 -1-1zm-1 4h-2v-2h2z"/><path d="m19 48h-4a1 1 0 0 0 -1 1v4a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1v-4a1 1 0 0 0 -1-1zm-1 4h-2v-2h2z"/><path d="m11 48h-4a1 1 0 0 0 -1 1v4a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1v-4a1 1 0 0 0 -1-1zm-1 4h-2v-2h2z"/></g></g></svg>
																</i>
															</div>
														</div>

														<div class="col-12 col-sm-10 col-md-12 col-lg">
															<h4 class="__title">Point of Sale</h4>

															<p>
                                                            Now turn your online store into a PoS to work at your offline store and get all your orders in just one place.
															</p>
														</div>
													</div>
												</div>
											</div>
											<!-- end item -->
										</div>
									</div>
								</div>
								<!-- end feature -->

							</div>
						</div>
					</div>
				</section>
				<!-- end section -->
                <!-- start section -->
                <section class="section section--no-pt">
                       <div class="container">
						<div class="row align-items-lg-center">
							<div class="col-12 col-lg-4 push-lg-4 text-center">
								<img class="img-fluid lazy" width="289" height="585" src="{{asset('landing/img/blank.gif')}}" data-src="{{asset('landing/img/feature.png')}}" data-srcset="{{asset('landing/img/feature.png')}} 1x, {{asset('landing/img/feature@2x.png')}} 2x" alt="simplisell" />
							</div>

							<div class="spacer py-5 d-lg-none"></div>

							<div class="col-12 col-lg-8">

								<!-- start feature -->
								<div class="feature feature--s1">
									<div class="__inner">
										<div class="row">
											<!-- start item -->
											<div class="col-12 col-md-6 pull-lg-6">
												<div class="__item  text-center text-md-right">
													<div class="row justify-content-sm-center flex-md-row-reverse">
														<div class="col-12 col-sm-10 col-md-12 col-lg-auto">
															<div class="mb-3 mb-lg-0">
																<i class="__ico">
																	<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g><g xmlns="http://www.w3.org/2000/svg"><g><path d="m138 364.5c-37.818 1.57-37.789 55.444 0 57h78c37.818-1.57 37.789-55.444 0-57zm91.5 28.5c0 7.444-6.056 13.5-13.5 13.5h-78c-17.914-.743-17.9-26.264 0-27h78c7.444 0 13.5 6.056 13.5 13.5z" fill="#9281c0" data-original="#000000" style="" class=""/><path d="m462.369 79.5h-38.178c-9.931.344-9.923 14.66 0 15h38.178c6.689 0 12.131 5.441 12.131 12.131v200.738c0 6.689-5.441 12.131-12.131 12.131h-344.738c-6.689 0-12.131-5.441-12.131-12.131v-10.043h80.5c44.453-1.846 44.419-65.171 0-67h-80.5v-123.695c0-6.689 5.441-12.131 12.131-12.131h271.305c9.931-.344 9.923-14.66 0-15h-57.436v-32c0-26.191-21.309-47.5-47.5-47.5h-214c-26.191 0-47.5 21.309-47.5 47.5v43.039c.344 9.931 14.66 9.923 15 0v-43.039c0-17.921 14.579-32.5 32.5-32.5h214c17.921 0 32.5 14.579 32.5 32.5v32h-11v-32c0-11.855-9.645-21.5-21.5-21.5h-40.1c-5.528 0-10.455 3.509-12.263 8.741l-4.805 13.979h-99.666l-4.809-13.989c-1.804-5.222-6.73-8.73-12.259-8.73h-40.098c-11.855 0-21.5 9.645-21.5 21.5v417c0 11.855 9.645 21.5 21.5 21.5h214c11.855 0 21.5-9.645 21.5-21.5v-23.45c-.344-9.931-14.66-9.923-15 0v23.45c0 3.584-2.916 6.5-6.5 6.5h-214c-3.584 0-6.5-2.916-6.5-6.5v-417c0-3.584 2.916-6.5 6.5-6.5h38.652l4.809 13.988c1.803 5.223 6.729 8.731 12.259 8.731h102.56c5.529 0 10.456-3.509 12.263-8.741l4.805-13.979h38.652c3.584 0 6.5 2.916 6.5 6.5v32h-172.869c-14.96 0-27.131 12.171-27.131 27.131v200.738c0 14.96 12.171 27.131 27.131 27.131h172.869v71.573c.344 9.931 14.66 9.923 15 0v-71.573h11v130c0 17.921-14.579 32.5-32.5 32.5h-214c-17.921 0-32.5-14.579-32.5-32.5v-338.5c-.344-9.931-14.66-9.923-15 0v338.5c0 26.191 21.309 47.5 47.5 47.5h214c26.191 0 47.5-21.309 47.5-47.5v-130h130.869c14.96 0 27.131-12.171 27.131-27.131v-200.738c0-14.96-12.171-27.131-27.131-27.131zm-276.369 165.826c24.549 1.019 24.53 35.99 0 37h-80.5v-37z" fill="#9281c0" data-original="#000000" style="" class=""/><path d="m347.18 299.755c3.869 1.476 8.204-.468 9.679-4.339l3.569-9.372h24.542l3.529 9.351c1.458 3.914 5.891 5.823 9.665 4.368 3.875-1.462 5.831-5.789 4.368-9.665l-21.521-57.027c-.025-.069-.053-.138-.081-.207-1.365-3.315-4.562-5.456-8.147-5.456h-.012c-3.59.004-6.787 2.154-8.145 5.478-.023.056-.045.111-.066.167l-21.719 57.024c-1.475 3.87.469 8.203 4.339 9.678zm25.575-46.077 6.554 17.366h-13.168z" fill="#9281c0" data-original="#000000" style="" class=""/><path d="m415.666 265.425-.067 27.302c-.01 4.143 3.34 7.509 7.481 7.519h.019c4.134 0 7.49-3.346 7.5-7.481l.067-27.33 17.114-26.29c2.26-3.472 1.278-8.117-2.193-10.377-3.471-2.262-8.118-1.279-10.377 2.193l-12.048 18.507-12.185-18.662c-2.267-3.469-6.911-4.442-10.381-2.18-3.468 2.265-4.444 6.912-2.18 10.381z" fill="#9281c0" data-original="#000000" style="" class=""/><path d="m300.422 292.746c.344 9.931 14.66 9.923 15 0v-17.664c3.167-.016 6.598-.03 8.604-.03 32.031-1.314 32.016-46.338 0-47.645h-16.104c-4.038-.08-7.592 3.487-7.5 7.523zm23.604-50.339c4.955 0 9.142 4.04 9.142 8.822s-4.187 8.822-9.142 8.822c-1.993 0-5.383.014-8.533.03-.04-7.441-.026-10.009-.049-17.675h8.582z" fill="#9281c0" data-original="#000000" style="" class=""/><path d="m149 118.5c-7.444 0-13.5 6.056-13.5 13.5v40c0 7.444 6.056 13.5 13.5 13.5h62c7.444 0 13.5-6.056 13.5-13.5v-40c0-7.444-6.056-13.5-13.5-13.5zm60.5 52h-59v-37h59z" fill="#9281c0" data-original="#000000" style="" class=""/><path d="m273.904 144.069v12c.344 9.931 14.66 9.923 15 0v-12c-.343-9.931-14.66-9.923-15 0z" fill="#9281c0" data-original="#000000" style="" class=""/><path d="m405.106 163.569c4.143 0 7.5-3.357 7.5-7.5v-12c-.344-9.931-14.66-9.923-15 0v12c0 4.143 3.358 7.5 7.5 7.5z" fill="#9281c0" data-original="#000000" style="" class=""/><path d="m439.342 156.069v-12c-.344-9.931-14.66-9.923-15 0v12c.343 9.931 14.66 9.924 15 0z" fill="#9281c0" data-original="#000000" style="" class=""/><path d="m327.723 156.069v-12c-.344-9.931-14.66-9.923-15 0v12c.343 9.931 14.66 9.924 15 0z" fill="#9281c0" data-original="#000000" style="" class=""/><path d="m348.56 163.569c4.143 0 7.5-3.357 7.5-7.5v-12c-.344-9.931-14.66-9.923-15 0v12c0 4.143 3.357 7.5 7.5 7.5z" fill="#9281c0" data-original="#000000" style="" class=""/></g></g></g></svg>
																</i>
															</div>
														</div>

														<div class="col-12 col-sm-10 col-md-12 col-lg">
															<h4 class="__title">Payment Option</h4>

															<p>
                                                            Receive payments online from customers with multiple payment of options like COD, UPI and RazorPay.
															</p>
														</div>
													</div>
												</div>
											</div>
											<!-- end item -->

											<!-- start item -->
											<div class="col-12 col-md-6">
												<div class="__item  text-center text-md-left">
													<div class="row justify-content-sm-center">
														<div class="col-12 col-sm-10 col-md-12 col-lg-auto">
															<div class="mb-3 mb-lg-0">
																<i class="__ico">
																	<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" x="0" y="0" viewBox="0 0 58 60" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g><g xmlns="http://www.w3.org/2000/svg" id="Page-1" fill="none" fill-rule="evenodd"><g id="007---AR-Phone-Camera" fill="rgb(0,0,0)" fill-rule="nonzero"><path id="Shape" d="m1.819 31.469 9.181 6.331v18.2c0 2.209139 1.790861 4 4 4h28c2.209139 0 4-1.790861 4-4v-18.2l9.18-6.331c.8122713-.5599326 1.2973256-1.4834357 1.2973256-2.47s-.4850543-1.9100674-1.2973256-2.47l-9.18-6.329v-16.2c0-2.209139-1.790861-4-4-4h-28c-2.209139 0-4 1.790861-4 4v16.2l-9.181 6.33c-.8118505.5599942-1.29660053 1.4832471-1.29660053 2.4695s.48475003 1.9095058 1.29660053 2.4695zm45.181-8.841 8.045 5.549c.2704766.1866852.4319586.4943528.4319586.823s-.161482.6363148-.4319586.823l-8.045 5.548zm-10.309-20.628-1.283 1.368c-.3778598.40298537-.9055734.63173367-1.458.632h-9.9c-.5526257-.00070589-1.0804819-.22935917-1.459-.632l-1.282-1.368zm-23.691 19.249.391-.27c.3332225-.2301327.4947509-.638518.4091059-1.0343251-.085645-.395807-.401553-.7008843-.8001059-.7726749v-15.172c0-1.1045695.8954305-2 2-2h3.567l2.565 2.736c.7573847.80478557 1.8128722 1.26199467 2.918 1.264h9.9c1.105276-.00129287 2.1610336-.45861896 2.918-1.264l2.565-2.736h3.567c1.1045695 0 2 .8954305 2 2v15.172c-.3985529.0717906-.7144609.3768679-.8001059.7726749-.085645.3958071.0758834.8041924.4091059 1.0343251l.391.27v15.5l-15.433 10.645c-.3416849.2355021-.7933151.2355021-1.135 0l-15.432-10.643zm0 17.931 14.3 9.86c1.027.7025926 2.38.7025926 3.407 0l14.293-9.86v11.82c0 1.1045695-.8954305 2-2 2h-28c-1.1045695 0-2-.8954305-2-2zm0 15.264c.6053465.3599241 1.2957468.5518554 2 .556h28c.7042532-.0041446 1.3946535-.1960759 2-.556v1.556c0 1.1045695-.8954305 2-2 2h-28c-1.1045695 0-2-.8954305-2-2zm-10.045-26.267 8.045-5.551v12.746l-8.046-5.549c-.27047664-.1866852-.43195863-.4943528-.43195863-.823s.16148199-.6363148.43195863-.823z" fill="#f551a8" data-original="#000000" style=""/><path id="Shape" d="m22.421 14.539c.2022236-.0004134.3995701-.0621278.566-.177l1.919-1.324c.3040074-.1986905.4763932-.5465852.4503343-.9088273-.0260588-.3622421-.2464655-.6818802-.575782-.8350108-.3293166-.1531306-.7157684-.1156787-1.0095523.0978381l-1.918 1.323c-.3611392.2483217-.5184977.7028667-.3881892 1.1213221s.5179144.7032966.9561892.7026779z" fill="#f551a8" data-original="#000000" style=""/><path id="Shape" d="m18.014 15.362-1.914 1.324c-.4428738.3174601-.5500621.9310532-.2410473 1.3798604.3090148.4488073.9204676.5675983 1.3750473.2671396l1.919-1.324c.4428738-.3174601.5500621-.9310532.2410473-1.3798604-.3090148-.4488073-.9204676-.5675983-1.3750473-.2671396z" fill="#f551a8" data-original="#000000" style=""/><path id="Shape" d="m28.744 10.391.256-.176.256.176c.4542184.2968641 1.0623064.1773178 1.3703412-.2694005.3080348-.44671829.2036023-1.0575834-.2353412-1.3765995l-.824-.568c-.3414622-.23504294-.7925378-.23504294-1.134 0l-.824.568c-.4389435.3190161-.543376.92988121-.2353412 1.3765995.3080348.4467183.9161228.5662646 1.3703412.2694005z" fill="#f551a8" data-original="#000000" style=""/><path id="Shape" d="m35.014 14.362c.4545797.3004587 1.0660325.1816677 1.3750473-.2671396.3090148-.4488072.2018265-1.0624003-.2410473-1.3798604l-1.919-1.323c-.2931474-.2296705-.6896343-.2774197-1.0289278-.1239144-.3392936.1535052-.5651811.4828328-.5862034.8546419-.0210224.3718091.1663034.7245001.4861312.9152725z" fill="#f551a8" data-original="#000000" style=""/><path id="Shape" d="m40.771 18.333c.2942082.2027482.6742327.2331371.9969219.0797195.3226893-.1534177.5390191-.4673342.5675001-.8235001s-.1352138-.7004712-.429422-.9032194l-1.919-1.324c-.4545797-.3004587-1.0660325-.1816677-1.3750473.2671396-.3090148.4488072-.2018265 1.0624003.2410473 1.3798604z" fill="#f551a8" data-original="#000000" style=""/><path id="Shape" d="m29 35c.1874199.0000007.3710679-.0526682.53-.152l8-5c.2923815-.1827389.4700008-.5032096.47-.848v-9c.0000008-.3447904-.1776185-.6652611-.47-.848l-8-5c-.3242708-.2026698-.7357292-.2026698-1.06 0l-8 5c-.2923815.1827389-.4700008.5032096-.47.848v9c-.0000008.3447904.1776185.6652611.47.848l8 5c.1589321.0993318.3425801.1520007.53.152zm7-6.554-6 3.754v-6.645l6-3.75zm-7-12.267 6.113 3.821-6.113 3.821-6.113-3.821zm-1 16.021-6-3.75v-6.645l6 3.75z" fill="#f551a8" data-original="#000000" style=""/></g></g></g></svg>
																</i>
															</div>
														</div>
                                                        <div class="col-12 col-sm-10 col-md-12 col-lg">
															<h4 class="__title">Augmented Reality</h4>

															<p>
                                                            Your products can be see inside customers place, they can physically place the product in their space and experience before they buy. These experiences doesn't need any app to be installed all works on the browser.
															</p>
														</div>
													</div>
												</div>
											</div>
											<!-- end item -->
										</div>

										<div class="spacer py-lg-3 py-xl-7 d-none d-lg-block"></div>

										<div class="row">
											<!-- start item -->
											<div class="col-12 col-md-6 pull-lg-6">
												<div class="__item  text-center text-md-right">
													<div class="row justify-content-sm-center flex-md-row-reverse">
														<div class="col-12 col-sm-10 col-md-12 col-lg-auto">
															<div class="mb-3 mb-lg-0">
																<i class="__ico">
																	<svg fill="#ff83d1" width="50" height="50" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 424.038 424.038"><path d="M352.4 34.019c-32.4 0-58 26-58 58.4s26 58 58 58 58.4-26 58.4-58.4-26-58-58.4-58zm0 103.6c-25.2 0-45.6-20.4-45.6-45.6s20.8-45.2 45.6-45.2c25.2 0 45.6 20.4 45.6 45.6 0 25.2-20.4 45.2-45.6 45.2z"/><path d="M370.8 101.219c-.4-1.2-.4-2-1.2-3.2-.4-.8-.8-2-1.6-2.8-.8-.8-1.2-1.6-2-2.4s-1.6-1.6-2.8-2c-.8-.8-2-1.2-2.8-1.6-1.2-.4-2-1.2-3.2-1.6-1.2-.4-2.4-.8-3.2-1.2-.4 0-1.2-.4-1.6-.8-.4-.4-1.2-.4-1.6-.8-.4-.4-1.2-.4-1.6-.8l-1.2-1.2c-.4-.4-.8-.8-.8-1.6-.4-.4-.4-1.2-.4-1.6v-2c0-.4.4-1.2.4-1.6.4-.4.4-1.2.8-1.6l1.2-1.2c.4-.4 1.2-.8 2-.8.8-.4 1.2-.4 2-.4h2.4c.8 0 1.2 0 2 .4.8 0 1.6.4 2 .4.4 0 1.2.4 1.6.4.8 0 1.2.4 1.6.4l2 .8.4-2.4.8-6.4v-1.6l-1.6-.4c-.4 0-1.2-.4-1.6-.4-.4 0-.8-.4-1.6-.4-.4 0-1.2-.4-2-.4s-1.2-.4-2-.4h-.4v-7.6h-9.6v8h-.4c-1.2.4-2.4.8-3.6 1.6-1.2.4-2 1.2-2.8 2s-1.6 1.6-2.4 2.8c-.8.8-1.2 2-1.6 3.2l-1.2 3.6c-.4 1.2-.4 2.8-.4 4s0 2 .4 3.2c.4 1.2.4 2 1.2 3.2.4.8 1.2 2 1.6 2.8.8.8 1.2 1.6 2 2.4.8.8 1.6 1.6 2.8 2 .8.8 2 1.2 2.8 1.6 1.2.4 2.4 1.2 3.2 1.6 1.2.4 2.4.8 3.2 1.2.4 0 1.2.4 1.6.8.4.4 1.2.4 1.6.8.4.4.8.8 1.2.8l1.2 1.2c.4.4.4.8.8 1.6 0 .4.4 1.2.4 2v2c0 .4-.4 1.2-.8 1.6l-1.2 1.2c-.4.4-.8.8-1.6 1.2-.4.4-1.2.4-1.6.8-.4 0-1.2.4-2 .4h-5.2c-.8 0-1.6-.4-2-.4-.8 0-1.6-.4-2.4-.8-.8-.4-1.6-.4-2.4-.8-.4 0-.8-.4-1.2-.4-.4 0-.8-.4-1.2-.4l-2.4-1.2-.4 2.8-.8 7.2v1.2l1.2.4c1.6-.4 2.4 0 3.6.4.8.4 2 .8 2.8.8.8.4 2 .4 3.2.8 1.2.4 2 .4 3.2.4h.8v8h9.6v-8.4h.4c1.2-.4 2.4-.8 3.6-1.6 1.2-.4 2-1.2 3.2-2 .8-.8 1.6-1.6 2.4-2.8.8-.8 1.2-2 2-3.2l1.2-3.6c.4-1.2.4-2.4.4-4 0-1.2 0-2-.4-3.2zm-46.4 56.8c-2-2.8-6.4-3.2-9.2-1.2l-64.8 51.6-83.6-34.8c-2-.8-4.4-.4-6 .8l-89.6 64c-2.8 2-3.6 6-1.6 8.8 1.2 2 3.2 2.8 5.2 2.8 1.2 0 2.4-.4 3.6-1.2l86.8-62 84 34.8c2 .8 4.8.4 6.4-.8l67.6-53.6c2.8-2 3.2-6.4 1.2-9.2z"/><path d="M418 377.219h-29.6v-185.2c0-3.6-2.8-6.4-6.4-6.4h-58.8c-3.6 0-6.4 2.8-6.4 6.4v185.2h-21.6v-121.2c0-3.6-2.8-6.4-6.4-6.4h-59.2c-3.6 0-6.4 2.8-6.4 6.4v121.2h-21.6v-130.8c0-3.6-2.8-6.4-6.4-6.4h-58.8c-3.6 0-6.4 2.8-6.4 6.4v130.8h-21.6v-93.2c0-3.6-2.8-6.4-6.4-6.4H42.8c-3.6 0-6.4 2.8-6.4 6.4v93.2h-30c-3.6 0-6.4 2.8-6.4 6.4 0 3.6 2.8 6.4 6.4 6.4h411.2c3.6 0 6.4-2.8 6.4-6.4.4-3.6-2.4-6.4-6-6.4zm-322.8 0h-46v-86.8h46v86.8zm93.6 0h-46v-124.4h46v124.4zm93.2 0h-46v-114.8h46v114.8zm93.6 0h-46v-178.8h46v178.8z"/></svg>
																</i>
															</div>
														</div>

														<div class="col-12 col-sm-10 col-md-12 col-lg">
															<h4 class="__title">Reports & Analytics</h4>

															<p>
                                                            Take the guess work out of managing your business and make informed decision with our robust Reports and detailed Analytics.
															</p>
														</div>
													</div>
												</div>
											</div>
											<!-- end item -->

											<!-- start item -->
											<div class="col-12 col-md-6">
												<div class="__item  text-center text-md-left">
													<div class="row justify-content-sm-center">
														<div class="col-12 col-sm-10 col-md-12 col-lg-auto">
															<div class="mb-3 mb-lg-0">
																<i class="__ico">
																	<svg fill="#B45BC4" enable-background="new 0 0 512 512" height="50" viewBox="0 0 512 512" width="50" xmlns="http://www.w3.org/2000/svg"><g><path d="m429.726 162.149-11.226-4.7v-6.113c0-3.266-2.113-6.156-5.225-7.146l-51.269-16.319c-3.947-1.259-8.166.924-9.421 4.872-1.256 3.947.925 8.165 4.872 9.421l29.083 9.257-86.518 27.472c-3.948 1.254-6.132 5.47-4.878 9.418 1.12 2.589 4.498 6.367 9.418 4.878l98.939-31.416v140.178c-6.349 2.328-13.197 4.565-20.567 6.679-3.982 1.142-6.284 5.295-5.142 9.277 1.094 2.879 4.534 6.498 9.277 5.142 59.442-17.046 94.931-43.966 94.931-72.011 0-20.841-23.645-43.313-63.5-60.57v-16.757l5.469 2.29c47.095 19.425 73.031 46.074 73.031 75.037s-25.936 55.611-73.031 75.037c-44.988 18.556-104.641 28.776-167.969 28.776-43.613 0-86.003-4.907-122.964-14.184-.883-5.266-1.685-10.619-2.399-16.053 37.09 9.975 80.147 15.237 125.363 15.237 33.96 0 66.477-2.916 96.647-8.666 4.069-.775 6.739-4.702 5.963-8.771-.775-4.068-4.703-6.742-8.771-5.963-27.024 5.15-56.036 7.949-86.338 8.345v-19.296c0-4.142-3.358-7.5-7.5-7.5s-7.5 3.358-7.5 7.5v19.291c-43.519-.576-84.641-6.163-119.701-16.257-1.662-16.971-2.521-34.577-2.521-52.534 0-30.191 2.423-59.394 7.029-86.241l74.142 23.435c2.999.87 7.319-.124 9.412-4.891 1.248-3.949-.941-8.163-4.891-9.412l-75.843-23.972c.521-2.493 1.059-4.965 1.62-7.408l118.252-37.64 68.744 21.881c3.946 1.258 8.165-.925 9.421-4.872s-.925-8.165-4.872-9.421l-31.228-9.94c-10.344-32.888-23.688-58.91-39.704-77.343-17.154-19.741-37.318-30.176-58.312-30.176-23.355 0-45.474 12.894-63.964 37.288-2.502 3.301-1.854 8.006 1.447 10.508 3.301 2.503 8.005 1.855 10.507-1.447 15.545-20.509 33.53-31.349 52.01-31.349 31.99 0 60.872 31.469 80.343 86.893l-18.461-5.876c-19.978-54.406-44.597-65.957-61.882-65.957-22.605 0-45.206 23.926-62.007 65.645-17.259 42.854-26.764 99.781-26.764 160.295s9.505 117.441 26.764 160.295c16.801 41.718 39.401 65.645 62.007 65.645 18.648 0 48.073-26.095 61.884-63.957l17.638-5.614c-19.496 53.982-48.082 84.631-79.522 84.631-28.973 0-55.628-25.939-75.057-73.041-18.555-44.986-28.774-104.635-28.774-167.959s10.219-122.973 28.774-167.959c1.831-4.438 3.759-8.759 5.732-12.845 1.802-3.73.238-8.214-3.492-10.015-3.728-1.801-8.213-.238-10.015 3.492-2.1 4.349-4.15 8.94-6.092 13.648-9.287 22.516-16.558 48.498-21.608 76.665l-5.835 2.587c-53.966 21.925-83.682 53.694-83.682 89.465 0 35.437 29.232 67.01 82.311 88.903 1.756.724 3.558 1.44 5.377 2.148 5.049 32.384 12.962 62.192 23.438 87.591 21.896 53.085 53.477 82.32 88.923 82.32 40.318 0 75.684-38.328 97.316-105.296l108.944-34.677c7.292-2.321 12.192-9.023 12.192-16.676v-11.074c3.813-1.395 7.551-2.836 11.189-4.337 53.078-21.893 82.31-53.466 82.31-88.902s-29.232-67.01-82.274-88.889zm37.274 88.889c0 12.766-15.27 29.989-48.5 44.837v-88.934c29.003 13.698 48.5 31.158 48.5 44.097zm-384.78 39.343c-23.519-12.442-37.22-26.75-37.22-39.343 0-12.632 13.95-26.977 37.894-39.438-1.105 14.52-1.676 29.363-1.676 44.4 0 11.595.342 23.074 1.002 34.381zm-67.22-39.343c0-28.716 25.439-55.091 71.695-74.47-.822 5.774-1.553 11.624-2.193 17.541-35.186 16.249-54.502 36.381-54.502 56.929 0 20.442 18.962 40.503 53.504 56.742.516 5.774 1.119 11.491 1.806 17.144-45.363-19.322-70.31-45.489-70.31-73.886zm185.049-205.977c16.925 0 33.235 18.442 46.3 52.141l-104.232 33.177c2.907-10.288 6.192-20.015 9.839-29.07 13.973-34.696 32.401-56.248 48.093-56.248zm0 421.878c-15.692 0-34.12-21.553-48.093-56.248-3.385-8.404-6.453-17.395-9.201-26.867l29.143 9.277c2.994.875 7.325-.108 9.421-4.872 1.256-3.947-.925-8.165-4.872-9.421l-38.195-12.158c-.761-3.227-1.487-6.501-2.178-9.817 34.355 8.017 72.832 12.464 112.426 12.963v31.946l-38.815-12.355c-3.95-1.26-8.166.924-9.421 4.872-1.256 3.947.925 8.166 4.872 9.421l41.182 13.108c-12.74 31.284-36.474 50.151-46.269 50.151zm203.451-111.587c0 1.093-.7 2.051-1.742 2.383l-138.258 44.007v-31.944c50.958-.638 99.449-7.768 140-20.443z"/><path d="m197.17 212.273c-10.305 0-19.224 7.332-21.208 17.433-.798 4.065 1.85 8.007 5.914 8.805 4.062.793 8.006-1.85 8.805-5.914.606-3.084 3.334-5.323 6.489-5.323 3.646 0 6.613 2.967 6.613 6.613s-2.967 6.613-6.613 6.613c-4.142 0-7.5 3.358-7.5 7.5s3.358 7.5 7.5 7.5c3.646 0 6.613 2.967 6.613 6.613s-2.967 6.613-6.613 6.613c-2.489.046-6.544-2.514-6.613-6.613 0-4.142-3.358-7.5-7.5-7.5s-7.5 3.358-7.5 7.5c-.27 10.425 9.684 22.017 21.613 21.613 11.917 0 21.613-9.696 21.613-21.613 0-5.391-1.989-10.323-5.265-14.113 3.276-3.79 5.265-8.722 5.265-14.113.001-11.918-9.695-21.614-21.613-21.614z"/><path d="m265.292 226.714c3.298-2.478 3.978-7.158 1.513-10.472-2.472-3.324-7.171-4.014-10.494-1.542-1.639.468-22.174 19.659-22.819 44.632 0 12.93 10.52 23.449 23.45 23.449s23.45-10.519 23.45-23.449c0-12.907-10.482-23.41-23.38-23.448 4.104-5.911 8.143-9.065 8.28-9.17zm.099 32.619c0 4.659-3.791 8.449-8.45 8.449s-8.45-3.791-8.45-8.449c0-4.659 3.791-8.45 8.45-8.45s8.45 3.79 8.45 8.45z"/><path d="m341.943 260.753v-25.507c0-12.93-10.52-23.45-23.45-23.45s-23.449 10.52-23.449 23.45v25.507c0 12.93 10.519 23.45 23.449 23.45s23.45-10.519 23.45-23.45zm-31.899 0v-25.507c0-4.659 3.791-8.45 8.449-8.45 4.659 0 8.45 3.791 8.45 8.45v25.507c0 4.659-3.791 8.45-8.45 8.45s-8.449-3.79-8.449-8.45z"/><path d="m356.943 205.5c4.142 0 7.5-3.358 7.5-7.5v-1c0-4.142-3.358-7.5-7.5-7.5s-7.5 3.358-7.5 7.5v1c0 4.142 3.358 7.5 7.5 7.5z"/></g></svg>
																</i>
															</div>
														</div>

														<div class="col-12 col-sm-10 col-md-12 col-lg">
															<h4 class="__title">360 Degree View</h4>

															<p>
                                                            Your customers can view your products in any angle, they can rotate, zoom in/out to understand all the details of your product.
															</p>
														</div>
													</div>
												</div>
											</div>
											<!-- end item -->
										</div>

										<div class="spacer py-lg-3 py-xl-7 d-none d-lg-block"></div>

										<div class="row">
											<!-- start item -->
											<div class="col-12 col-md-6 pull-lg-6">
												<div class="__item  text-center text-md-right">
													<div class="row justify-content-sm-center flex-md-row-reverse">
														<div class="col-12 col-sm-10 col-md-12 col-lg-auto">
															<div class="mb-3 mb-lg-0">
																<i class="__ico">
																	<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" x="0" y="0" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g><g xmlns="http://www.w3.org/2000/svg" id="Outline"><path d="m484.25 239.741a24 24 0 0 0 -29.871-16.114l-214.6 64.207a8.016 8.016 0 0 0 -3.185 1.835l-26.072 24.5a8 8 0 0 0 4.1 13.711l35.243 6.156a8.019 8.019 0 0 0 3.67-.216l174.128-52.1.754 16.475-55.33 16.554 4.586 15.33 61.314-18.346a8 8 0 0 0 5.7-8.029l-1.223-26.704 24.673-7.382a24 24 0 0 0 16.113-29.872zm-233.49 78.21-17.622-3.078 13.036-12.251 36.512-10.922 4.586 15.328zm51.84-15.511-4.586-15.328 128.142-38.341.754 16.476zm165.7-52.023a7.952 7.952 0 0 1 -4.748 3.867l-20.841 6.235-.754-16.475 17.009-5.089a8 8 0 0 1 9.334 11.462z" fill="#ffba33" data-original="#000000" style="" class=""/><path d="m320 352h-136v-120a8 8 0 0 0 -8-8h-144a8 8 0 0 0 -8 8v256a8 8 0 0 0 8 8h288a8 8 0 0 0 8-8v-128a8 8 0 0 0 -8-8zm-88 16h32v32h-32zm-144-128h32v32h-32zm-48 0h32v40a8 8 0 0 0 8 8h48a8 8 0 0 0 8-8v-40h32v112h-128zm48 128h32v32h-32zm-48 0h32v40a8 8 0 0 0 8 8h48a8 8 0 0 0 8-8v-40h32v112h-128zm272 112h-128v-112h32v40a8 8 0 0 0 8 8h48a8 8 0 0 0 8-8v-40h32z" fill="#ffba33" data-original="#000000" style="" class=""/><path d="m280 448h16v16h-16z" fill="#ffba33" data-original="#000000" style="" class=""/><path d="m136 448h16v16h-16z" fill="#ffba33" data-original="#000000" style="" class=""/><path d="m136 320h16v16h-16z" fill="#ffba33" data-original="#000000" style="" class=""/><path d="m152 80a16.019 16.019 0 0 1 16-16h48v16h-40a8 8 0 0 0 -8 8v120h16v-112h192v128h16v-136a8 8 0 0 0 -8-8h-40v-16h48a16.019 16.019 0 0 1 16 16v136h16v-136a32.036 32.036 0 0 0 -32-32h-77.754l5.515-22.06a8 8 0 0 0 -7.761-9.94h-64a8 8 0 0 0 -7.761 9.94l5.515 22.06h-77.754a32.036 32.036 0 0 0 -32 32v128h16zm80-16h24a8 8 0 0 0 7.761-9.94l-5.515-22.06h43.508l-5.515 22.06a8 8 0 0 0 7.761 9.94h24v16h-96z" fill="#ffba33" data-original="#000000" style="" class=""/><path d="m408 408a16.019 16.019 0 0 1 -16 16h-48v16h48a32.036 32.036 0 0 0 32-32v-72h-16z" fill="#ffba33" data-original="#000000" style="" class=""/><path d="m392 400v-56h-16v48h-32v16h40a8 8 0 0 0 8-8z" fill="#ffba33" data-original="#000000" style="" class=""/><path d="m248 120a8 8 0 0 0 -8-8h-32a8 8 0 0 0 -8 8v32a8 8 0 0 0 8 8h32a8 8 0 0 0 8-8zm-16 24h-16v-16h16z" fill="#ffba33" data-original="#000000" style="" class=""/><path d="m240 184h-32a8 8 0 0 0 -8 8v32a8 8 0 0 0 8 8h32a8 8 0 0 0 8-8v-32a8 8 0 0 0 -8-8zm-8 32h-16v-16h16z" fill="#ffba33" data-original="#000000" style="" class=""/><path d="m264 112h16v16h-16z" fill="#ffba33" data-original="#000000" style="" class=""/><path d="m264 144h96v16h-96z" fill="#ffba33" data-original="#000000" style="" class=""/><path d="m296 112h64v16h-64z" fill="#ffba33" data-original="#000000" style="" class=""/><path d="m264 184h16v16h-16z" fill="#ffba33" data-original="#000000" style="" class=""/><path d="m264 216h96v16h-96z" fill="#ffba33" data-original="#000000" style="" class=""/><path d="m296 184h64v16h-64z" fill="#ffba33" data-original="#000000" style="" class=""/></g></g></svg>
																</i>
															</div>
														</div>

														<div class="col-12 col-sm-10 col-md-12 col-lg">
															<h4 class="__title">Inventory Management</h4>

															<p>
                                                            Reduce the change of human error. Inventory levels are updated automatically when an order is placed.
															</p>
														</div>
													</div>
												</div>
											</div>
											<!-- end item -->

											<!-- start item -->
											<div class="col-12 col-md-6">
												<div class="__item  text-center text-md-left">
													<div class="row justify-content-sm-center">
														<div class="col-12 col-sm-10 col-md-12 col-lg-auto">
															<div class="mb-3 mb-lg-0">
																<i class="__ico">
																    <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" x="0" y="0" viewBox="0 0 36 60" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g><g xmlns="http://www.w3.org/2000/svg" id="Page-1" fill="none" fill-rule="evenodd"><g id="001---Mobie-AR" fill="rgb(0,0,0)" fill-rule="nonzero"><path id="Shape" d="m4 60h28c2.209139 0 4-1.790861 4-4v-52c0-2.209139-1.790861-4-4-4h-28c-2.209139 0-4 1.790861-4 4v52c0 2.209139 1.790861 4 4 4zm21.692-58-1.282 1.368c-.378896.40269492-.907076.63133176-1.46.632h-9.9c-.5527743.00000995-1.0809024-.22876112-1.459-.632l-1.282-1.368zm-23.692 2c0-1.1045695.8954305-2 2-2h3.567l2.565 2.735c.7567936.80574999 1.8125738 1.2634477 2.918 1.265h9.9c1.1054015-.00208587 2.1611681-.45925919 2.919-1.264l2.564-2.736h3.567c1.1045695 0 2 .8954305 2 2v47c0 1.1045695-.8954305 2-2 2h-28c-1.1045695 0-2-.8954305-2-2zm0 50.444c.60534654.3599241 1.29574677.5518554 2 .556h28c.7042532-.0041446 1.3946535-.1960759 2-.556v1.556c0 1.1045695-.8954305 2-2 2h-28c-1.1045695 0-2-.8954305-2-2z" fill="#1463b0" data-original="#000000" style=""/><path id="Shape" d="m9 18v9c-.00026048.4171213.25841773.7905905.649.937l8 3c.2258574.0835314.4741426.0835314.7 0l8-3c.3913699-.1457526.6509487-.5193707.651-.937v-9c.0002605-.4171213-.2584177-.7905905-.649-.937l-8-3c-.2258574-.0835314-.4741426-.0835314-.7 0l-8 3c-.39136988.1457526-.65094875.5193707-.651.937zm16 8.307-6 2.25v-6.864l6-2.25zm-7-10.239 5.152 1.932-5.152 1.932-5.152-1.932zm-7 3.375 6 2.25v6.864l-6-2.25z" fill="#1463b0" data-original="#000000" style=""/><path id="Shape" d="m8 16c.55228475 0 1-.4477153 1-1v-2h2c.5522847 0 1-.4477153 1-1s-.4477153-1-1-1h-3c-.55228475 0-1 .4477153-1 1v3c0 .5522847.44771525 1 1 1z" fill="#1463b0" data-original="#000000" style=""/><path id="Shape" d="m25 13h2v2c0 .5522847.4477153 1 1 1s1-.4477153 1-1v-3c0-.5522847-.4477153-1-1-1h-3c-.5522847 0-1 .4477153-1 1s.4477153 1 1 1z" fill="#1463b0" data-original="#000000" style=""/><path id="Shape" d="m8 35h3c.5522847 0 1-.4477153 1-1s-.4477153-1-1-1h-2v-2c0-.5522847-.44771525-1-1-1s-1 .4477153-1 1v3c0 .5522847.44771525 1 1 1z" fill="#1463b0" data-original="#000000" style=""/><path id="Shape" d="m28 30c-.5522847 0-1 .4477153-1 1v2h-2c-.5522847 0-1 .4477153-1 1s.4477153 1 1 1h3c.5522847 0 1-.4477153 1-1v-3c0-.5522847-.4477153-1-1-1z" fill="#1463b0" data-original="#000000" style=""/><path id="Shape" d="m11 48h14c2.209139 0 4-1.790861 4-4s-1.790861-4-4-4h-10c-.5522847 0-1 .4477153-1 1s.4477153 1 1 1h10c1.1045695 0 2 .8954305 2 2s-.8954305 2-2 2h-14c-1.1045695 0-2-.8954305-2-2s.8954305-2 2-2c.5522847 0 1-.4477153 1-1s-.4477153-1-1-1c-2.209139 0-4 1.790861-4 4s1.790861 4 4 4z" fill="#1463b0" data-original="#000000" style=""/></g></g></g></svg>
                                                                </i>
															</div>
														</div>

														<div class="col-12 col-sm-10 col-md-12 col-lg">
															<h4 class="__title">Virtual Try On</h4>

															<p>
                                                            Allow your customers to virtually try the products on themselves and help them quickly make buying decision and confidently place the order.
															</p>
														</div>
													</div>
												</div>
											</div>
											<!-- end item -->
										</div>
									</div>
								</div>
								<!-- end feature -->

							</div>
						</div>
					</div>
				</section>
				<!-- end section -->

                <!-- start section -->
				<section id="pricing" class="section section--bg-img jarallax" data-speed="0.3" data-img-position="50% 50%" style="background-image: url({{asset('landing/img/base_bg.jpg')}});">
					<div class="container">
						<div class="section-heading section-heading--white section-heading--center">
						<h2 class="__title">Pricing & Plans </h2>
						<h6 class="__subtitle">Start your ecommerce website now, pick a plan later</h6>
							<h6 class="__subtitle">Try SimpliSell free for 21 days, no payment information required.</h6>
							

						</div>

						<div class="spacer py-6"></div>						<div class="col-12" style="text-align:center; vertical-align:middle;">
                <!-- <div class="custom-control custom-switch custom-switch-success">
                    <span class="switch-label mr-1"><b>Monthly</b></span>
                    <input type="checkbox" class="custom-control-input" id="plan_interval" name="plan_interval">
                    <label class="custom-control-label" for="plan_interval"></label>
                    <span class="switch-label"><b>Yearly</b></span>
                </div>
            </div> -->
            <!-- <div>
                <input type="checkbox" id="monthly_interval" name="monthly_interval" checked>&nbsp;
                <span class="switch-label mr-1" style="color:#fff;"><b>Monthly</b></span>&nbsp;&nbsp;
                <input type="checkbox" id="yearly_interval" name="yearly_interval">&nbsp;
                <span class="switch-label mr-1" style="color:#fff;"><b>Yearly</b></span>
            </div> -->

	

						<div class="row">
							<div class="col-12">

								<div class="content-container">
									<!-- start pricing table -->
									<div class="pricing-table pricing-table--s2">
										<div class="__inner">
											<div class="row justify-content-sm-center justify-content-md-start">
												<!-- start item -->
												<div class="col-12 col-sm-9 col-md-6 col-lg-3 d-sm-flex">
													<div class="__item __item--rounded __item--shadow" style="background-image: url({{asset('landing/img/pricing/bg_1.png')}});background-repeat: no-repeat;background-position: top left">
														<div class="__header">
															<div class="__title h4">Basic Plan</div>

															<div class="__price"><sup>₹</sup>350<sub>/mo</sub></div>
															<div class="__price s__price"><s><sup>₹</sup>650<sub>/mo</s></sub></div>
														</div>

														<div class="__body  pt-8 pb-9">
														{{--@if($agent->isMobile())
														<button class="my-2 pointer btn btn-info"  data-toggle="collapse" id="features" data-target="#demo">Show features &nbsp;
															<i class="fa fa-angle-double-down"></i>
															<!-- <i data-toggle="collapse" class="fa fa-angle-double-up"></i><i class="fa fa-angle-double-down"></i> -->
														</button>
														<div id="demo" class="collapse">
														@endif--}}
															<ul class="__desc-list check-list">
																<li><i class="ico-checked fontello-ok"></i>Number of Catalogue - 5</li>
																<li><i class="ico-checked fontello-ok"></i>Number of Products - 150</li>
																<li><i class="ico-checked fontello-ok"></i>Free Sub Domain</li>
																<li><i class="ico-checked fontello-ok"></i>Your Brand Logo</li>
																<li><i class="ico-checked fontello-ok"></i>Private Catalogue</li>
																<li><i class="ico-checked fontello-ok"></i>QR Code for the store and Catalog</li>
																<li><i class="ico-checked fontello-ok"></i>Store Analytics</li>
																<li><i class="ico-checked fontello-ok"></i>Email/Text Support</li>
																<li><i class="ico-unchecked fontello-minus"></i>Custom Domain Mapping</li>
																<li><i class="ico-unchecked fontello-minus"></i>Inventory</li>
																<li><i class="ico-unchecked fontello-minus"></i>Advanced Analytics</li>
																<li><i class="ico-unchecked fontello-minus"></i>Point of Sale</li>
																<li><i class="ico-unchecked fontello-minus"></i>Product Stories</li>
																<li><i class="ico-unchecked fontello-minus"></i>Video Catalog</li>
															</ul>
        														{{--@if($agent->isMobile())
														</div>
														@endif--}}
														</div>

														<div class="__footer">
															<a class="custom-btn custom-btn--medium custom-btn--style-2" href="{{ route('register') }}">&nbsp;&nbsp;Start Free Trial&nbsp;&nbsp;</a>
														</div>
													</div>
												</div>
												<!-- end item -->

												<!-- start item -->
												<div class="col-12 col-sm-9 col-md-6 col-lg-3 d-sm-flex">
													<div class="__item __item--rounded __item--shadow __item--active" style="background-image: url({{asset('landing/img/pricing/bg_2.png')}});background-repeat: no-repeat;background-position: top right">
														<div class="__header">
															<span class="__label">Best choice</span>

															<div class="__title h4">Silver Plan</div>

															<div class="__price"><sup>₹</sup>999<sub>/mo</sub></div>
															<div class="__price s__price"><s><sup>₹</sup>1,999<sub>/mo</s></sub></div>
														</div>

														<div class="__body  pt-8 pb-9">
															<ul class="__desc-list check-list">
                                                            <li><i class="ico-checked fontello-ok"></i>Number of Catalogue - 20</li>
																<li><i class="ico-checked fontello-ok"></i>Number of Products - 500</li>
																<li><i class="ico-checked fontello-ok"></i>Free Sub Domain</li>
																<li><i class="ico-checked fontello-ok"></i>Custom Domain Mapping</li>
																<li><i class="ico-checked fontello-ok"></i>Your Brand Logo</li>
																<li><i class="ico-checked fontello-ok"></i>Private Catalogue</li>
																<li><i class="ico-checked fontello-ok"></i>QR Code for the store and Catalog</li>
																<li><i class="ico-checked fontello-ok"></i>Inventory</li>
																<li><i class="ico-checked fontello-ok"></i>Store Analytics</li>
																<li><i class="ico-checked fontello-ok"></i>Product Stories</li>
																<li><i class="ico-checked fontello-ok"></i>Email/Text Support</li>
																<li><i class="ico-unchecked fontello-minus"></i>Point of Sale</li>
																<li><i class="ico-unchecked fontello-minus"></i>Video Catalog</li>
																<li><i class="ico-unchecked fontello-minus"></i>Advanced Analytics</li>

															</ul>
														</div>

														<div class="__footer">
															<a class="custom-btn custom-btn--medium custom-btn--style-2" href="{{ route('register') }}">&nbsp;&nbsp;Start Free Trial&nbsp;&nbsp;</a>
														</div>
													</div>
												</div>
												<!-- end item -->

												<!-- start item -->
												<div class="col-12 col-sm-9 col-md-6 col-lg-3 d-sm-flex">
													<div class="__item __item--rounded __item--shadow" style="background-image: url({{asset('landing/img/pricing/bg_3.png')}});background-repeat: no-repeat;background-position: top right">
														<div class="__header">
															<div class="__title h4">Gold Plan</div>

															<div class="__price"><sup>₹</sup>2,499<sub>/mo</sub></div>
															<div class="__price s__price"><s><sup>₹</sup>3,999<sub>/mo</s></sub></div>
														</div>

														<div class="__body  pt-8 pb-9">
															<ul class="__desc-list check-list">
                                                                <li><i class="ico-checked fontello-ok"></i>Number of Catalogue - 50</li>
																<li><i class="ico-checked fontello-ok"></i>Number of Products - 1000</li>
																<li><i class="ico-checked fontello-ok"></i>Free Sub Domain</li>
																<li><i class="ico-checked fontello-ok"></i>Custom Domain Mapping</li>
																<li><i class="ico-checked fontello-ok"></i>Your Brand Logo</li>
																<li><i class="ico-checked fontello-ok"></i>Private Catalogue</li>
																<li><i class="ico-checked fontello-ok"></i>QR Code for the store and Catalog</li>
																<li><i class="ico-checked fontello-ok"></i>Inventory</li>
																<li><i class="ico-checked fontello-ok"></i>Store Analytics</li>
																<li><i class="ico-checked fontello-ok"></i>Advanced Analytics</li>
																<li><i class="ico-checked fontello-ok"></i>Point of Sale</li>
																<li><i class="ico-checked fontello-ok"></i>Product Stories</li>
																<li><i class="ico-checked fontello-ok"></i>Video Catalog</li>
																<li><i class="ico-checked fontello-ok"></i>Email/Text Support</li>
															</ul>
														</div>

														<div class="__footer">
															<a class="custom-btn custom-btn--medium custom-btn--style-2" href="{{ route('register') }}">&nbsp;&nbsp;Start Free Trial&nbsp;&nbsp;</a>
														</div>
													</div>
												</div>
												<!-- end item -->
                                                <!-- start item -->
												<div class="col-12 col-sm-9 col-md-6 col-lg-3 d-sm-flex">
													<div class="__item __item--rounded __item--shadow" style="background-image: url({{asset('landing/img/pricing/bg_3.png')}});background-repeat: no-repeat;background-position: top right">
														<div class="__header">
															<div class="__title h4">Premium</div>
															<div class="__price" >Contact Us</div>
														</div>

														<div class="__body  pt-8 pb-9">
															<ul class="__desc-list check-list">
                                                                <li><i class="ico-checked fontello-ok"></i>Number of Catalogue - 100</li>
																<li><i class="ico-checked fontello-ok"></i>Number of Products - 2500</li>
																<li><i class="ico-checked fontello-ok"></i>Free Sub Domain</li>
																<li><i class="ico-checked fontello-ok"></i>Custom Domain Mapping</li>
																<li><i class="ico-checked fontello-ok"></i>Your Brand Logo</li>
																<li><i class="ico-checked fontello-ok"></i>Private Catalogue</li>
																<li><i class="ico-checked fontello-ok"></i>QR Code for the store and Catalog</li>
																<li><i class="ico-checked fontello-ok"></i>Inventory</li>
																<li><i class="ico-checked fontello-ok"></i>Store Analytics</li>
																<li><i class="ico-checked fontello-ok"></i>Advanced Analytics</li>
																<li><i class="ico-checked fontello-ok"></i>Point of Sale</li>
																<li><i class="ico-checked fontello-ok"></i>Product Stories</li>
																<li><i class="ico-checked fontello-ok"></i>Custom On boarding</li>
																<li><i class="ico-checked fontello-ok"></i>Email/Text Support</li>
																<li><i class="ico-checked fontello-ok"></i>Video Catalog</li>
																<li><i class="ico-checked fontello-ok"></i>VIP Support</li>
																<li><i class="ico-checked fontello-ok"></i>Native Android App</li>
																<li><i class="ico-checked fontello-ok"></i>Native iOS App</li>
																<li><i class="ico-checked fontello-ok"></i>Headless commerce</li>
															</ul>
														</div>

														<div class="__footer">
															<a class="custom-btn custom-btn--medium custom-btn--style-2" href="#contact">&nbsp;&nbsp;Contact Us&nbsp;&nbsp;</a>
														</div>
													</div>
												</div>
												<!-- end item -->
											</div>
										</div>
									</div>
									<!-- end pricing table -->
								</div>

							</div>
						</div>
					</div>

					<div class="shape">
						<svg xmlns="http://www.w3.org/2000/svg" width="1680" height="450" viewBox="0 0 1680 450" preserveAspectRatio="none" style="min-height: 300px">
							<path d="M0 3s561.937 57 842 55c286 0 484-18 839-58v450H-2z" style="mix-blend-mode:screen" fill="#fff" fill-rule="evenodd"/>
						</svg>
					</div>
				</section>
				<!-- end section -->
                <!-- start section -->
				<section id="infoblock" class="section section--bg-img jarallax" data-speed="0.3" data-img-position="50% 50%" style="background-image: url({{asset('landing/img/base_bg.jpg')}});">
					<div class="container">
						<div class="row">
							<div class="col-12">

								<!-- start info block -->
								<div class="info-block info-block--s1">
									<div class="row align-items-md-center">
										<div class="col-12 col-md-12">
											<div class="section-heading section-heading--white">
												<h2 class="__title">0% Commission for business of all sizes</h2>
                                                <h2 class="__title"><span>Own your own customers and get independent from online aggregators and their huge commission charges.</span></h2>
											</div>

											<div class="spacer py-4"></div>

											<div>
												<p>
													<a class="custom-btn custom-btn--medium custom-btn--style-4" href="{{ route('register') }}">&nbsp;&nbsp;Get your business online&nbsp;&nbsp;</a>
												</p>
											</div>
										</div>

									</div>
								</div>
								<!-- end info block -->

								<div class="spacer py-10 d-none d-md-block"></div>
								<div class="spacer py-4 d-none d-lg-block"></div>

							</div>
						</div>
					</div>

					<div class="shape">
						<svg xmlns="http://www.w3.org/2000/svg" width="1680" height="200" viewBox="0 0 1680 200" preserveAspectRatio="none" style="min-height: 200px">
							<path d="M0 3s561.937 57 842 55c286 0 484-18 839-58v450H-2z" style="mix-blend-mode:screen" fill="#fff" fill-rule="evenodd"/>
						</svg>
					</div>
				</section>
				<!-- end section -->

               <!-- start section -->
				<section class="section section--no-pt" id="faq">
					<div class="container">
						<div class="section-heading section-heading--center">
							<h6 class="__subtitle">FAQ</h6>

							<h2 class="__title">Frequently <span>Asked Questions</span></h2>

						</div>

						<div class="spacer py-6"></div>

						<div class="row">
							<div class="col-12">

								<div class="content-container">
									<!-- start FAQ -->
									<div class="faq">
										<div class="accordion-container">
											<!-- start item -->
											<div class="accordion-item">
												<div class="accordion-toggler">
													<h4 class="accordion-title">Why do I need a Whatsapp-integrated store?</h4>

													<i class="circled"></i>
												</div>

												<article class="accordion-content">
													<div class="accordion-content__inner">
														<p>
                                                        In the age of social media, if you are not promoting your products on Facebook and Whatsapp you are missing out. SimpliSell helps you to set up a Whatsapp-integrated shop, so you can sell products over whatsapp and other social media directly.
														</p>
													</div>
												</article>
											</div>
											<!-- end item -->

											<!-- start item -->
											<div class="accordion-item">
												<div class="accordion-toggler">
													<h4 class="accordion-title">I am not a business, can I use SimpliSell?</h4>

													<i class="circled"></i>
												</div>

												<article class="accordion-content">
													<div class="accordion-content__inner">
														<p>
                                                        Our goal is to enable everyone to go online, you can be an individual or a business, SimpliSell supports anyone with a product that they want to sell.
														</p>
													</div>
												</article>
											</div>
											<!-- end item -->

											<!-- start item -->
											<div class="accordion-item">
												<div class="accordion-toggler">
													<h4 class="accordion-title">What can I sell?</h4>

													<i class="circled"></i>
												</div>

												<article class="accordion-content">
													<div class="accordion-content__inner">
														<p>
                                                        The only limitation we put on what you can sell is that the products has to be legal. We have stores selling from apparel to handcrafts and many other categories. As long as someone wants to buy it and it is legal you can sell it!
														</p>
													</div>
												</article>
											</div>
											<!-- end item -->

											<!-- start item -->
											<div class="accordion-item">
												<div class="accordion-toggler">
													<h4 class="accordion-title">How do I take payments?</h4>

													<i class="circled"></i>
												</div>

												<article class="accordion-content">
													<div class="accordion-content__inner">
														<p>
                                                        SimpliSell offers integration with RazorPay payment gateway integration which allow you to take payments via different methods like UPI, Debit etc directly to your Bank account without any transaction charges to SimpliSell.
														</p>
													</div>
												</article>
											</div>
											<!-- end item -->
                                            <!-- start item -->
											<div class="accordion-item">
												<div class="accordion-toggler">
													<h4 class="accordion-title">Can I use my own domain name?</h4>

													<i class="circled"></i>
												</div>

												<article class="accordion-content">
													<div class="accordion-content__inner">
														<p>
                                                        Yes, you can connect your own personal domain with SimpliSell. Contact us with your domain name and our team will setup the domain for you including HTTPS.
														</p>
													</div>
												</article>
											</div>
											<!-- end item -->
                                            <!-- start item -->
											<div class="accordion-item">
												<div class="accordion-toggler">
													<h4 class="accordion-title">Do I need hosting?</h4>

													<i class="circled"></i>
												</div>

												<article class="accordion-content">
													<div class="accordion-content__inner">
														<p>
                                                        No. We provide everything that is required for your online store to host your data in a secure cloud hosting.
														</p>
													</div>
												</article>
											</div>
											<!-- end item -->
                                            <!-- start item -->
											<div class="accordion-item">
												<div class="accordion-toggler">
													<h4 class="accordion-title">Do you offer a free trial?</h4>

													<i class="circled"></i>
												</div>

												<article class="accordion-content">
													<div class="accordion-content__inner">
														<p>
                                                        Yes, we offer a 21 Days Free trial plan, you don't have to enter your card details. Just sign up and start using SimpliSell.
														</p>
													</div>
												</article>
											</div>
											<!-- end item -->
                                            <!-- start item -->
											<div class="accordion-item">
												<div class="accordion-toggler">
													<h4 class="accordion-title">How to I cancel my paid plan?</h4>

													<i class="circled"></i>
												</div>

												<article class="accordion-content">
													<div class="accordion-content__inner">
														<p>
                                                        You can cancel your subscription anytime in your account.
														</p>
													</div>
												</article>
											</div>
											<!-- end item -->
										</div>
									</div>
									<!-- end FAQ -->
								</div>

							</div>
						</div>
					</div>
				</section>
				<!-- end section -->
<!-- start section -->
<section id="contact" class="section section--bg-img jarallax" data-speed="0.3" data-img-position="50% 60%" style="background-image: url({{asset('landing/img/base_bg.jpg')}});">
					<div class="container">
						<div class="section-heading section-heading--white section-heading--center">
							<h2 class="__title">Get <span>in Touch</span></h2>

						
						</div>

						<div class="spacer py-6"></div>

						<div class="row justify-content-md-center">
							<div class="col-12 col-md-10 col-lg-8 col-xl-6">


								<!-- start company contacts -->
								<div class="company-contacts company-contacts--s2 text-white">
									<div class="__inner section-heading--center">
										<div class="__name h3">Have any <span>Questions?</span></div>

										<div class="row no-gutters align-items-baseline">
											<div class="col-12 col-md-auto section-heading--center">
												<a href="mailto:support@simplisell.co">support@simplisell.co</a>
											</div>
										</div>
									</div>
								</div>
								<!-- end company contacts -->

							</div>
						</div>
					</div>
				</section>
				<!-- end section -->

</main>
<!-- end main -->
@endsection