<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style>
        * { font-family: DejaVu Sans, sans-serif; }
        .table-borderless > tbody > tr > td,
        .table-borderless > tbody > tr > th,
        .table-borderless > tfoot > tr > td,
        .table-borderless > tfoot > tr > th,
        .table-borderless > thead > tr > td,
        .table-borderless > thead > tr > th 
        {
            border: 1px;
            border-color: white;
        }
    </style>
</head>
<body>
    @php
        $userSettings = $data['userSettings'];
        $product_qty = $data['product_qty'];
        $meta_data = $data['meta_data'];
        $product_notes = $data['product_notes'];
        $customer = $data['customerData'];
        $cmeta_data = json_decode($customer->meta_data, true);
        $subtotal = 0;
        $total_discount = 0;
        $serial_num = 0;
    @endphp
    <h3 style="padding:12px;">Order Confirmation:</h3>
    <p style="padding:12px;">
        Thanks for placing an order with us, our team will get in touch with you shortly to process the order. 
        Meanwhile if you want to reach us please feel free to reach us at {{$meta_data['settings']['emailid'] ?? ''}} or call us at 
        {{isset($meta_data['settings']['whatsapp_country_code']) ? '+'.$meta_data['settings']['whatsapp_country_code'].'-'.$meta_data['settings']['whatsapp'] : '+91-'.$meta_data['settings']['whatsapp']}}.
    </p>
    <table class="m-1 table table-borderless">  
        <tr>
            <td> 
                <p class=" font-weight-bold h4 text-capitalize text-secondary p-0 m-0">Store Details</p>
                    {{$meta_data['storefront']['storename'] ?? ''}}<br>
                    {{$meta_data['settings']['emailid'] ?? ''}}<br>
                    {{isset($meta_data['settings']['contactnumber_country_code']) ? '+'.$meta_data['settings']['contactnumber_country_code'].'-'.$meta_data['settings']['contactnumber'] : '+91'.'-'.$meta_data['settings']['contactnumber']}}<br>
            </td>
            <td>
                <p class=" font-weight-bold h4 text-secondary p-0 m-0">Order Details</p>
                <b> Date:</b>{{date('d-m-Y h:i:s A', strtotime($customer->created_at))}}<br>
                <b> Order #</b>{{$customer->order->id}}
                @if($customer->order->order_mode)
                    <br><b>Order mode:</b>{{$customer->order->order_mode=="Online-Storefront" ? "Online" : $customer->order->order_mode}}
                @endif
            </td>
        </tr> 
        <tr>
            <td>
                <p class="font-weight-bold bg-secondary text-white p-1 w-100">Billed to</p>
                {{ $customer->fname ?? "" }},<br>
                {{ $customer->address1 ?? "" }},
                @if ($customer->address2)
                    {{ $customer->address2}},<br>
                @else
                    <br>
                @endif
                {{ $customer->city ?? "" }} - {{ $customer->pincode ?? "" }},<br>
                {{ $customer->state ?? "" }}, {{ $customer->country ?? "" }}.<br>
                {{ $customer->mobileno ?? "" }}<br>
                {{ $customer->amobileno ?? "" }}
            </td>
            <td class="text-left text-bold-300">
                <p class="font-weight-bold bg-secondary text-white p-1 w-100">Ship to</p>
                {{ $customer->fname ?? "" }},<br>
                {{ $customer->address1 ?? "" }},
                @if ($customer->address2)
                    {{ $customer->address2}},<br>
                @else
                    <br>
                @endif
                {{ $customer->city ?? "" }} - {{ $customer->pincode ?? "" }},<br>
                {{ $customer->state ?? "" }}, {{ $customer->country ?? "" }}.<br>
                {{ $customer->mobileno ?? "" }}<br>
                {{ $customer->amobileno ?? "" }}
            </td>
        </tr>      
    </table>    
    <table class="table table-borderless ml-3">
        <tr class="table bg-secondary text-white">
            <th scope="col">S.No</th>
            <th scope="col">Product Name/Notes</th>
            <th scope="col">Discount</th>
            <th scope="col">Item Price</th>
            <th scope="col">Product Quantity</th>
            <th scope="col">Amount</th>
        </tr>
        @foreach($data['entity'] as $entity)
            @foreach($entity as $key => $card)
                @php 
                    $product = $card->product; 
                    if($product->discount != null)
                    {   
                        $discount_price = round(($product->price * ($product->discount/100)),2);
                        $discounted_price = round(($product->price - $discount_price),2);
                        $total_discount += round(($discount_price * $product_qty[$product->id]),2);
                    }
                    $subtotal += round(($product->price * $product_qty[$product->id]),2);
                    $items_total = $subtotal - $total_discount;
                    $serial_num ++;
                @endphp
                <tr class=" table-secondary text-center" >
                    <td style="text-align:right;">{{ $serial_num }}</td>
                    <td scope="row" style="text-align:left;">
                        {{$product->name}}
                        @if($product->sku)
                            <br><b>SKU:</b>{{" ".$product->sku}}
                            @endif
                        @if($product_notes[$product->id])
                            <br>{{$product_notes[$product->id]}}
                        @endif
                    </td>
                    <td style="text-align:right;">{{$product->discount ? number_format($product->discount,2).'%' : '00.00%'}}</td>
                    <td style="text-align:right;">{!! $product->price ?  $cmeta_data['currency']."".$product->price :$cmeta_data['currency'].' 00.00' !!}</td>
                    
                    <td style="text-align:right;">{{$product_qty[$product->id]}}</td>
                    <td style="text-align:right;">
                        {!! $cmeta_data['currency'] !!}   
                        @if($product->discount != null)
                            <span class="text-dark">{{number_format($discounted_price,2)}}</span><br>
                            &nbsp;<span class=""><s>{{$product->price}}</s></span>
                        @else
                            <span class="text-dark">{!! $product->price ?? '00.00' !!} </span>
                        @endif
                   </td>
                </tr>
            @endforeach
            @endforeach
            <tr>
                <td> <br> </td> <!--The br tag did what i was looking for -->
            </tr>
            <tr class="">
            <td colspan="4" class="font-weight-bold bg-secondary text-white mt-1">
                Special notes and instructions
            </td>
            <td>Items</td>
            <td class=" text-right">
                {!! $cmeta_data['currency']."".number_format($items_total,2) !!}
            </td>
        </tr>
        <tr>
            <td class="table-secondary" colspan="4">
                {{$customer->order->notes ? $customer->order->notes :  "" }}
            </td>
            <td>&nbsp;</td>
            <td class=" text-right">
                &nbsp;
                {{-- -{!! $cmeta_data['currency']."".number_format( $total_discounted_price,2) !!} --}}
            </td>
        </tr>
        @if(isset($cmeta_data['coupon_applied']))
        @if($cmeta_data['coupon_applied']==true)
        <tr>
        <td colspan="4">&nbsp;</td>
        <td>Coupon Discount</td>
        <td class=" text-right">
                -{!! $cmeta_data['currency']."".number_format($cmeta_data['coupon_discount_amount']?? 0 ,2) !!}</s>
        </td>
        </tr>
        @endif
        @endif
        
        @if(isset($cmeta_data['shipping_charges']) && $cmeta_data['shipping_charges'] > 0)
            <tr>
                <td colspan="4">&nbsp;</td>
                <td>Shipping</td>
                <td class=" text-right">
                    {!! $cmeta_data['currency']."".number_format($cmeta_data['shipping_charges'] ?? 0 ,2) !!}</s>
                </td>
            </tr>
        @endif
        @if(isset($cmeta_data['extra_charges_names']) && count($cmeta_data['extra_charges_names']) > 0)
            @foreach ($cmeta_data['extra_charges_names'] as $key => $extra_charge_name)
                <tr>
                    <td colspan="4">&nbsp;</td>
                    <td>{!! $extra_charge_name !!}</td>
                    <td class=" text-right">
                        {!! $cmeta_data['currency']."".number_format($cmeta_data['extra_charges_values'][$key] ?? 0 ,2) !!}</s>
                    </td>
                </tr>
            @endforeach
        @endif
        @if(isset($cmeta_data['tax_percentage']) && $cmeta_data['tax_percentage'] > 0)
            <tr>
                <td colspan="4">&nbsp;</td>
                <td>Tax ({{$cmeta_data['tax_percentage']}}% GST)</td>
                <td class=" text-right">
                    {!! $cmeta_data['currency']."".number_format($cmeta_data['tax_amount'] ?? 0 ,2) !!}</s>
                </td>
            </tr>
        @endif
        @php 
        if(isset($cmeta_data['coupon_applied']))
        {
        if($cmeta_data['coupon_applied']==true)
        {
            $items_total = (float)$items_total - (float)number_format($cmeta_data['coupon_discount_amount']?? 0 ,2);
        }
        }
        if(isset($cmeta_data['shipping_charges']) && $cmeta_data['shipping_charges'] > 0)
        {
            $items_total = (float)$items_total + (float)number_format($cmeta_data['shipping_charges'] ?? 0 ,2) ;
        }
       
        if(isset($cmeta_data['extra_charges_values']) && count($cmeta_data['extra_charges_values']) > 0)
        {
            foreach($cmeta_data['extra_charges_values'] as $key)
            {
                $items_total = (float)$items_total + (float)number_format($key ?? 0 ,2);
            }
        }
        if(isset($cmeta_data['tax_percentage']) && $cmeta_data['tax_percentage'] > 0)
        {
            $items_total = (float)$items_total + (float)number_format($cmeta_data['tax_amount'] ?? 0 ,2);
        }
        @endphp
        <tr>
            <td><b>Total</b></td>
            <td><b>{!!  $cmeta_data['currency']."".number_format($items_total,2)!!}</b></td>
        </tr>
        <tr>
            <td colspan="4" style="font-size: 20px" class=" m-0 p-0 font-weight-bolder text-secondary">
                <br>Thank you for your business!
                <p style="font-size: 10px" class="m-0 p-0">
                    Should you have any enquieries concerning this order receipt, please contact us.
                </p>
            </td>
        </tr>
    </table>
    <!-- <p class="ml-3"><b>Website: </b>{{App\Http\Controllers\Admin\CampaignsController::publicLink($userSettings->public_link,'store')}}</p> -->
</body>
</html>