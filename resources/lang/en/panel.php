<?php

return [
    'site_title' => 'SimpliSell',
    'site_tagline' => 'Create your catalogue using SimpleSell',
    'site_desc' => 'Create your E-Commerce Store in 5 min & Sell online',
];
