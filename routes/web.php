<?php


use Aws\Middleware;

$url_parameters = @explode(".", $_SERVER['HTTP_HOST']);

if (count($url_parameters) == 3) {
    $domain_pattern = '{subdomain}.{domain}.{tld}';
} else {
    $domain_pattern = '{domain}.{tld}';
}

// Route::domain('{subdomain}.' .env('SITE_URL', 'localhost'))->group(function () {
Route::domain('{subdomain}.' . env('SITE_URL', 'localhost'))->group(function () {
    Route::group(['namespace' => 'Admin'], function () {
        Route::get('/', ['uses' => 'UserSettingsController@storePublic'])->name('mystore.published');
        Route::group(['middleware' => ['shared', 'store_front_subscription_check']], function () {
            Route::get('/p/{shareable_link}', 'CampaignsController@sharePublic')->name('campaigns.published');
            Route::get('/t/{shareable_link}', 'CampaignsController@sharePublicCam')->name('campaigns.publishedTryOn');
            Route::get('/vd/{shareable_link}', ['uses' => 'CampaignsController@shareShowVideo',])->name('campaigns.showVideo');
            Route::get('/v/{shareable_link}', 'CampaignsController@sharePublicCamViewAR')->name('campaigns.publishedViewAR');
            Route::get('/vr/{shareable_link}', 'CampaignsController@sharePublicCamViewVR')->name('campaigns.publishedViewVR');
            Route::get('/d/{product_id}/{shareable_link}', 'CampaignsController@sharePublicCatalogDetails')->name('campaigns.publishedCatalogDetails');
            Route::get('/c/{shareable_link}', 'CampaignsController@sharePublicCheckout')->name('campaigns.publishedCheckout');
            Route::get('/pc/{shareable_link}/{order_id}', 'CampaignsController@paymentconfirmation')->name('campaigns.paymentConfirmation');
            Route::get('/customer/login', ['uses' => 'UserSettingsController@storeLogin'])->name('customer.storelogin');
            Route::get('/customer/register', ['uses' => 'UserSettingsController@storeRegister'])->name('customer.storeregister');
            Route::get('/customer/forgotpassword', ['uses' => 'UserSettingsController@storeforgotpassword'])->name('customer.storeforgotpassword');

            Route::get('/ec/{shareable_link}/{enquiry_id}', 'CampaignsController@paymentconfirmation');

            Route::get('/pay/{shareable_link}', 'CampaignsController@payment')->name('payment.upi');
            Route::get('/stories/{shareable_link}', 'CampaignsController@sharePublicStories')->name('campaigns.publishedStories');
            Route::get('/category/{category_id}/{shareable_link}', 'CampaignsController@sharePublicProductByCategory')->name('campaigns.publishedProductByCategory');
            Route::get('/products/{shareable_link}', 'CampaignsController@sharePublicAllProducts')->name('campaigns.publishedAllCatalog');
            Route::get('/store/about-us/{shareable_link}', ['uses' => 'CampaignsController@storeAboutUs'])->name('store.storeAboutUs');
            Route::get('/store/faq/{shareable_link}', ['uses' => 'CampaignsController@storeFaq'])->name('store.storeFaq');

            Route::get('/store/product-origin/{shareable_link}', ['uses' => 'CampaignsController@storeOrigin'])->name('store.storeOrigin');
            Route::get('/store/privacy-policy/{shareable_link}', ['uses' => 'CampaignsController@storePrivacyPolicy'])->name('store.storePrivacyPolicy');
            Route::get('/store/terms-and-conditions/{shareable_link}', ['uses' => 'CampaignsController@storeTermsAndConditions'])->name('store.storeTermsAndConditions');
            Route::get('/store/refund-and-return-policy/{shareable_link}', ['uses' => 'CampaignsController@storeRefundAndReturnPolicy'])->name('store.storeRefundAndReturnPolicy');
            Route::get('/store/contact-us/{shareable_link}', ['uses' => 'CampaignsController@storeContactUs'])->name('store.storeContactUs');
            Route::get('/store/shipping-policy/{shareable_link}', ['uses' => 'CampaignsController@storeShippingPolicy'])->name('store.storeShippingPolicy');
        });

        Route::group(['namespace' => 'Customer'], function () {
            Route::get('/profile/{shareable_link}', ['uses' => 'CustomerController@CustomerProfile'])->name('customer.profile');
            Route::get('/orders/{shareable_link}', ['uses' => 'CustomerController@CustomerOrders'])->name('customer.orders');
            Route::get('/orders/{id}/{shareable_link}', ['uses' => 'CustomerController@CustomerOrderDetails'])->name('customer.show.order_details');
            Route::get('/orderStatus/{id}/{shareable_link}', ['uses' => 'CustomerController@CustomerOrderStatus'])->name('customer.show.order_status');
            Route::post('/orders/cancel', 'CustomerController@CustomerOrderCancel')->name('customer.order.cancel');
        });

        // Route::get('/', ['uses' =>'CampaignsController@sharePublic']);
        Route::get('share/{uuid}', 'CapturesController@share')->name('captures.share');
        Route::post('/orders/orderplace', 'OrdersController@store')->name('orders.place');
        Route::post('/orders/payment', 'PaymentController@payment')->name('orders.payment');
        Route::post('/orders/storePayment', 'OrdersController@store_transaction')->name('transaction.store');
        Route::post('/payment/media', 'OrdersController@storeMedia')->name('payment.storeMedia');
        Route::post('/enquiry/store', 'EnquiryController@store')->name('enquiry.store');
        Route::post('/store/faq/contact-us/{shareable_link}', 'ContactUsController@store')->name('store.storeContactFaq');
        // Route::post('/email/orderplace', 'MailController@orderplace_email')->name('emails.orderplace');
        Route::get('orders/pdf/{orders}', 'PdfController@customer_export_order_pdf')->name('customer_export_order_pdf');
    });
});
// Match any other domains

Route::group(['domain' => $domain_pattern], function () {
    Route::group(['namespace' => 'Admin'], function () {
        Route::get('/', ['uses' => 'UserSettingsController@storePublic'])->name('mystore.published');
        // Route::get('/', ['uses' =>'CampaignsController@sharePublic']);
        Route::group(['middleware' => ['shared', 'store_front_subscription_check']], function () {
            Route::get('/p/{shareable_link}', 'CampaignsController@sharePublic')->name('campaigns.published');
            Route::get('/t/{shareable_link}', 'CampaignsController@sharePublicCam')->name('campaigns.publishedTryOn');
            Route::get('/v/{shareable_link}', 'CampaignsController@sharePublicCamViewAR')->name('campaigns.publishedViewAR');
            Route::get('/vr/{shareable_link}', 'CampaignsController@sharePublicCamViewVR')->name('campaigns.publishedViewVR');
            Route::get('/vd/{shareable_link}', ['uses' => 'CampaignsController@shareShowVideo',])->name('campaigns.showVideo');
            Route::get('/d/{product_id}/{shareable_link}', 'CampaignsController@sharePublicCatalogDetails')->name('campaigns.publishedCatalogDetails');
            Route::get('/c/{shareable_link}', 'CampaignsController@sharePublicCheckout')->name('campaigns.publishedCheckout');
            Route::get('/pc/{shareable_link}/{order_id}', 'CampaignsController@paymentconfirmation')->name('campaigns.paymentConfirmation');
            Route::get('/customer/login', ['uses' => 'UserSettingsController@storeLogin'])->name('customer.storelogin');
            Route::get('/customer/register', ['uses' => 'UserSettingsController@storeRegister'])->name('customer.storeregister');
            Route::get('/customer/forgotpassword', ['uses' => 'UserSettingsController@storeforgotpassword'])->name('customer.storeforgotpassword');

            Route::get('/ec/{shareable_link}/{enquiry_id}', 'CampaignsController@paymentconfirmation');

            Route::get('/pay/{shareable_link}', 'CampaignsController@payment')->name('payment.upi');
            Route::get('/stories/{shareable_link}', 'CampaignsController@sharePublicStories')->name('campaigns.publishedStories');
            Route::get('/products/{shareable_link}', 'CampaignsController@sharePublicAllProducts')->name('campaigns.publishedAllCatalog');
            Route::get('/category/{category_id}/{shareable_link}', 'CampaignsController@sharePublicProductByCategory')->name('campaigns.publishedProductByCategory');
            Route::get('/store/about-us/{shareable_link}', ['uses' => 'CampaignsController@storeAboutUs'])->name('store.storeAboutUs');
            Route::get('/store/faq/{shareable_link}', ['uses' => 'CampaignsController@storeFaq'])->name('store.storeFaq');

            Route::get('/store/product-origin/{shareable_link}', ['uses' => 'CampaignsController@storeOrigin'])->name('store.storeOrigin');
            Route::get('/store/privacy-policy/{shareable_link}', ['uses' => 'CampaignsController@storePrivacyPolicy'])->name('store.storePrivacyPolicy');
            Route::get('/store/terms-and-conditions/{shareable_link}', ['uses' => 'CampaignsController@storeTermsAndConditions'])->name('store.storeTermsAndConditions');
            Route::get('/store/refund-and-return-policy/{shareable_link}', ['uses' => 'CampaignsController@storeRefundAndReturnPolicy'])->name('store.storeRefundAndReturnPolicy');
            Route::get('/store/contact-us/{shareable_link}', ['uses' => 'CampaignsController@storeContactUs'])->name('store.storeContactUs');
            Route::get('/store/shipping-policy/{shareable_link}', ['uses' => 'CampaignsController@storeShippingPolicy'])->name('store.storeShippingPolicy');
        });

        Route::group(['namespace' => 'Customer'], function () {
            Route::get('/profile/{shareable_link}', ['uses' => 'CustomerController@CustomerProfile'])->name('customer.profile');
            Route::get('/orders/{shareable_link}', ['uses' => 'CustomerController@CustomerOrders'])->name('customer.orders');
            Route::get('/orders/{id}/{shareable_link}', ['uses' => 'CustomerController@CustomerOrderDetails'])->name('customer.show.order_details');
            Route::get('/orderStatus/{id}/{shareable_link}', ['uses' => 'CustomerController@CustomerOrderStatus'])->name('customer.show.order_status');
            Route::post('/orders/cancel', 'CustomerController@CustomerOrderCancel')->name('customer.order.cancel');

        });

        Route::get('share/{uuid}', 'CapturesController@share')->name('captures.share');
        Route::post('/orders/orderplace', 'OrdersController@store')->name('orders.place');
        Route::post('/orders/payment', 'PaymentController@payment')->name('orders.payment');
        Route::post('/orders/storePayment', 'OrdersController@store_transaction')->name('transaction.store');
        Route::post('/payment/media', 'OrdersController@storeMedia')->name('payment.storeMedia');
        Route::post('/enquiry/store', 'EnquiryController@store')->name('enquiry.store');
        Route::post('/store/faq/contact-us/{shareable_link}', 'ContactUsController@store')->name('store.storeContactFaq');
        // Route::post('/email/orderplace', 'MailController@orderplace_email')->name('emails.orderplace');
        Route::get('orders/pdf/{orders}', 'PdfController@customer_export_order_pdf')->name('customer_export_order_pdf');
    });
});

// Route::group([
//     'domain' => '{domain}',
// ], function () {
//     Route::group(['namespace' => 'Admin'], function () {
//         Route::get('/', ['uses' =>'CampaignsController@sharePublic']);
//         Route::get('/t/{shareable_link}', ['uses' =>'CampaignsController@sharePublicCam','middleware' => 'shared'])->name('campaigns.publishedTryOn');

//     });
// });

Route::group(['namespace' => 'Admin'], function () {
    Route::group(['middleware' => ['shared','store_front_subscription_check']], function () {
        Route::get('/p/{shareable_link}', ['uses' => 'CampaignsController@sharePublic'])->name('campaigns.published');
        Route::get('/s/{shareable_link}', ['uses' => 'UserSettingsController@storePublic'])->name('mystore.published');
        Route::get('/sd/{shareable_link}', ['uses' => 'UserSettingsController@storePublicVideo'])->name('mystore.publishedVideo');
        Route::get('/t/{shareable_link}', ['uses' => 'CampaignsController@sharePublicCam',])->name('campaigns.publishedTryOn');
        Route::get('/sv/{shareable_link}', ['uses' => 'CampaignsController@sharePublicVideo',])->name('campaigns.publishedVideo');
        Route::get('/vd/{shareable_link}', ['uses' => 'CampaignsController@shareShowVideo',])->name('campaigns.showVideo');
        Route::get('/v/{shareable_link}', 'CampaignsController@sharePublicCamViewAR')->name('campaigns.publishedViewAR');
        Route::get('/vr/{shareable_link}', 'CampaignsController@sharePublicCamViewVR')->name('campaigns.publishedViewVR');
        Route::get('/d/{product_id}/{shareable_link}', ['uses' => 'CampaignsController@sharePublicCatalogDetails'])->name('campaigns.publishedCatalogDetails');
        Route::get('/c/{shareable_link}', ['uses' => 'CampaignsController@sharePublicCheckout'])->name('campaigns.publishedCheckout');
        Route::get('/pc/{shareable_link}/{order_id}', ['uses' => 'CampaignsController@paymentconfirmation'])->name('campaigns.paymentConfirmation');
        Route::get('/s/{shareable_link}/customer/login', ['uses' => 'UserSettingsController@storeLogin'])->name('customer.storelogin');
        Route::get('/s/{shareable_link}/customer/register', ['uses' => 'UserSettingsController@storeRegister'])->name('customer.storeregister');
        Route::get('/s/{shareable_link}/customer/forgotpassword', ['uses' => 'UserSettingsController@storeforgotpassword'])->name('customer.storeforgotpassword');

        Route::get('/ec/{shareable_link}/{enquiry_id}', 'CampaignsController@paymentconfirmation');

        Route::get('/pay/{shareable_link}', ['uses' => 'CampaignsController@payment'])->name('payment.upi');
        Route::get('/stories/{shareable_link}', ['uses' => 'CampaignsController@sharePublicStories'])->name('campaigns.publishedStories');
        Route::get('/products/{shareable_link}', ['uses' => 'CampaignsController@sharePublicAllProducts'])->name('campaigns.publishedAllCatalog');
        Route::get('/category/{category_id}/{shareable_link}', ['uses' => 'CampaignsController@sharePublicProductByCategory'])->name('campaigns.publishedProductByCategory');
        Route::get('/store/about-us/{shareable_link}', ['uses' => 'CampaignsController@storeAboutUs'])->name('store.storeAboutUs');
        Route::get('/store/faq/{shareable_link}', ['uses' => 'CampaignsController@storeFaq'])->name('store.storeFaq');

        Route::get('/store/product-origin/{shareable_link}', ['uses' => 'CampaignsController@storeOrigin'])->name('store.storeOrigin');
        Route::get('/store/privacy-policy/{shareable_link}', ['uses' => 'CampaignsController@storePrivacyPolicy'])->name('store.storePrivacyPolicy');
        Route::get('/store/terms-and-conditions/{shareable_link}', ['uses' => 'CampaignsController@storeTermsAndConditions'])->name('store.storeTermsAndConditions');
        Route::get('/store/refund-and-return-policy/{shareable_link}', ['uses' => 'CampaignsController@storeRefundAndReturnPolicy'])->name('store.storeRefundAndReturnPolicy');
        Route::get('/store/contact-us/{shareable_link}', ['uses' => 'CampaignsController@storeContactUs'])->name('store.storeContactUs');
        Route::get('/store/shipping-policy/{shareable_link}', ['uses' => 'CampaignsController@storeShippingPolicy'])->name('store.storeShippingPolicy');
    });

    Route::group(['namespace' => 'Customer'], function () {
        Route::get('/profile/{shareable_link}', ['uses' => 'CustomerController@CustomerProfile'])->name('customer.profile');
        Route::get('/orders/{shareable_link}', ['uses' => 'CustomerController@CustomerOrders'])->name('customer.orders');
        Route::get('/orders/{id}/{shareable_link}', ['uses' => 'CustomerController@CustomerOrderDetails'])->name('customer.show.order_details');
        Route::get('/orderStatus/{id}/{shareable_link}', ['uses' => 'CustomerController@CustomerOrderStatus'])->name('customer.show.order_status');
        Route::post('forget-password', ['uses'=>'ForgotPasswordController@postEmail']);
        Route::post('update/profile', ['uses' => 'CustomerController@updateProfile'])->name('customer.update.profile');
        Route::post('update/address', ['uses' => 'CustomerController@updateAddress'])->name('customer.update.address');
        Route::post('change-password', ['uses' => 'CustomerController@ChangePassword'])->name('customer.change.password');
        Route::post('/customer_orders', ['uses' => 'CustomerController@CustomerOrders'])->name('customer.orders.ajax');
        Route::post('/orders/cancel', 'CustomerController@CustomerOrderCancel')->name('customer.order.cancel');

        Route::post('/logout-customer','LoginController@logout')->name('customer.logout');

    });


    Route::resource('captures','CapturesController');
    Route::get('share/{uuid}','CapturesController@share')->name('captures.share');
    Route::post('/orders/orderplace', 'OrdersController@store')->name('orders.place');
    Route::post('/orders/payment', 'PaymentController@payment')->name('orders.payment');
    Route::post('/orders/storePayment', 'OrdersController@store_transaction')->name('transaction.store');
    Route::post('/payment/media', 'OrdersController@storeMedia')->name('payment.storeMedia');
    Route::post('/enquiry/store', 'EnquiryController@store')->name('enquiry.store');
    Route::post('/store/faq/contact-us/{shareable_link}', 'ContactUsController@store')->name('store.storeContactFaq');
    // Route::post('/email/orderplace', 'MailController@orderplace_email')->name('emails.orderplace');
    Route::get('download/orders/pdf/{orders}', 'PdfController@customer_export_order_pdf')->name('customer_export_order_pdf');
    Route::get('catalog/ajaxProductData', 'CampaignsController@ajaxProductData')->name('admin.campaigns.ajaxProductData');
    Route::get('catalog/ajaxVideoProductData', 'CampaignsController@ajaxVideoProductData')->name('admin.campaigns.ajaxVideoProductData');
    Route::get('catalog/ajaxRelatedVideo', 'CampaignsController@ajaxRelatedVideo')->name('admin.campaigns.ajaxRelatedVideo');
    Route::get('catalog/ajaxRelatedVideoData', 'CampaignsController@ajaxRelatedVideoData')->name('admin.campaigns.ajaxRelatedVideoData');
    Route::get('catalog/ajaxVideoData', 'CampaignsController@ajaxVideoData')->name('admin.campaigns.ajaxVideoData');
    Route::get('category/ajaxCategoryProductData', 'CampaignsController@ajaxCategoryProductData')->name('admin.campaigns.ajaxCategoryProductData');
    Route::get('catalog/ajaxProductDetails', 'CampaignsController@ajaxProductDetails')->name('admin.campaigns.ajaxProductDetails');
    Route::get('catalog/ajaxProductVariantDetails', 'CampaignsController@ajaxProductVariantDetails')->name('admin.campaigns.ajaxProductVariantDetails');
    Route::get('catalog/ajaxCombinationalProductVariantDetails', 'CampaignsController@ajaxCombinationalProductVariantDetails')->name('admin.campaigns.ajaxCombinationalProductVariantDetails');
    Route::get('catalog/ajaxPosProductDetails', 'CampaignsController@ajaxPosProductDetails')->name('admin.campaigns.ajaxPosProductDetails');
    Route::get('catalog/ajaxPosProductVariantDetails', 'CampaignsController@ajaxPosProductVariantDetails')->name('admin.campaigns.ajaxPosProductVariantDetails');
    Route::get('catalog/ajaxPosCombinationalProductVariantDetails', 'CampaignsController@ajaxPosCombinationalProductVariantDetails')->name('admin.campaigns.ajaxPosCombinationalProductVariantDetails');

    //analytics
    Route::post('stats/events', 'Analytics\AnalyticsController@EventsAjax')->name('admin.stats.ajaxEvents');
    //ordersController
    Route::post('/apply_coupon','OrdersController@ApplyCouponCode')->name('coupon.apply');
    // Route::post('/sh/{product_id}/{shareable_link}','CampaignsController@sharelink')->name('shipping.price');
    Route::post('shipping/price','ShippingController@price_calculator')->name('shipping.price');
        // variant data
        Route::get('ajaxvariantdata', 'VariantsController@ajaxvariantdata')->name('variants.data');
        Route::get('ajaxproductdata', 'VariantsController@ajaxproductdata')->name('variants.product');

});


//customer login starts
Route::group([ 'as' => 'customer.', 'namespace' => 'Admin\Customer'], function () {

    Route::post('/loginCustomer','LoginController@customerLogin')->name('login');
    Route::post('/registerCustomer','RegisterController@createCustomer')->name('register');

    Route::get('forget-password', 'ForgotPasswordController@getEmail');
    Route::post('forget-password', 'ForgotPasswordController@postEmail');

    Route::get('reset-password/{token}', 'ResetPasswordController@getPassword');
    Route::post('reset-password', 'ResetPasswordController@updatePassword');

    Route::get('/loginCustomer/social/{provider}', 'LoginController@socialLogin_Redirect')->name('social.login');
    Route::get('/loginCustomer/{provider}/callback','LoginController@socialLogin_Callback');

});
//customer login ends

Route::get('/privacy', 'Landing\LandingController@privacyPolicy')->name('landing.privacyPolicy');
Route::get('/terms', 'Landing\LandingController@termsAndConditions')->name('landing.termsAndConditions');
Route::get('/banneditems', 'Landing\LandingController@bannedItems')->name('landing.bannedItems');

Route::get('/p/password/{shareable_link}', 'Shareable\ShareableLinkPasswordController@show');
Route::post('/p/password/{shareable_link}', 'Shareable\ShareableLinkPasswordController@store');


Route::get('/catalog/inactive', function () {
    return view('shareable.inactive');
});
Route::get('/catalog/expired', function () {
    return view('shareable.expired');
});
Route::get('/store/unavailabe', function () {
    return view('shareable.store_front_subscription_expired');
})->name('store_subscription_expired');

// Route::get('/shared1/{shareable_link}', ['middleware' => 'shared', function (ShareableLink $link) {
//     var_dump($link->shareable);
// }]);

Route::post('/otp/resend/', 'Auth\OtpController@resendOTP')->name('otp.resend');
Route::get('/otpvalidate', 'Auth\OtpController@index')->name('otpvalidate');

Route::get('/otp/generate', 'Telegram\TelegramBotController@generateOtp')->name('telegramBot.generateOtp');
Route::post('/telegram_bot_subscriber/delete', 'Telegram\TelegramBotController@deleteTelegramBotSuscriber')->name('telegramBot.deleteTelegramBotSuscriber');

Route::get('/login/social/{provider}', 'Auth\SocialController@redirect')->name('social.login');
Route::get('/login/{provider}/callback','Auth\SocialController@Callback');
// Route::redirect('/', '/login');
Route::get('/','Landing\LandingController@index');
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Route::get('/mytest', function () {
    return view('test');
});

Route::group(['prefix' => 'admin'], function () {
    Auth::routes();
});


Route::get('userVerification/{token}', 'UserVerificationController@approve')->name('userVerification');
Route::get('userVerification/otp/{token}', 'UserVerificationController@approveOTP')->name('userVerification_otp');
Route::post('user/register', 'Auth\RegisterController@store')->name('user_registration');



// Admin
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth', 'subscription_check']], function () {

    //notify
    //Route::get('/test','UserAlertsController@index')->name('notification');
    Route::post('/save-token', 'UserAlertsController@saveToken')->name('save-token');
    Route::post('/send-notification', 'NotificationController@sendNotification_test')->name('send.notification');

    Route::get('/dashboard', 'HomeController@index')->name('home');
    // Route::redirect('/dashboard', '/admin/stores');

    Route::redirect('/dashboard', '/admin/mystore');

    Route::get('/adminDashboard', 'DashboardController@index')->name('dashboard.index');

    Route::get('user-alerts/read', 'UserAlertsController@read');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users

    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');
    // Route::get('/users', 'App\Http\Controllers\UserController@index');

    Route::post('change-user-password/{user_id}', 'UsersController@changePassword')->name('users.changePassword');
    Route::get('accountSettings', 'UsersController@accountSettings')->name('users.accountSettings');
    Route::put('accountSettingsUpdate', 'UsersController@accountSettingsUpdate')->name('users.accountSettingsUpdate');


    // StoreUser
    Route::delete('storeUser/destroy', 'StoreUserController@massDestroy')->name('storeUser.massDestroy');
    Route::resource('storeUser', 'StoreUserController', ['except' => ['edit', 'show']]);
    Route::get('storeUser/{user_id}', 'StoreUserController@show')->name('storeUser.show');
    Route::get('storeUser/{user_id}/edit', 'StoreUserController@edit')->name('storeUser.edit');
    Route::post('storeUser/{user_id}/edit', 'StoreUserController@edit')->name('storeUser.edit');
    Route::post('change-password/{user_id}', 'StoreUserController@changePassword')->name('storeUser.changePassword');
    // Route::get('accountSettings', 'StoreUserController@accountSettings')->name('storeUser.accountSettings');
    Route::put('accountSettingsUpdate', 'StoreUserController@accountSettingsUpdate')->name('storeUser.accountSettingsUpdate');

    //Devices
    Route::resource('devices', 'DevicesController');
    Route::resource('userTranscations', 'UserTransactionsController');

    // Audit Logs
    Route::resource('audit-logs', 'AuditLogsController', ['except' => ['create', 'store', 'edit', 'update', 'destroy']]);

    // Product Categories
    Route::delete('product-categories/destroy', 'ProductCategoryController@massDestroy')->name('product-categories.massDestroy');
    Route::post('product-categories/media', 'ProductCategoryController@storeMedia')->name('product-categories.storeMedia');
    Route::post('product-categories/ckmedia', 'ProductCategoryController@storeCKEditorImages')->name('product-categories.storeCKEditorImages');
    Route::resource('product-categories', 'ProductCategoryController');

    Route::post('ajaxaddcategory', 'ProductCategoryController@ajaxAddCategory')->name('product-categories.ajaxAddCategory');

    // Product Tags
    Route::delete('product-tags/destroy', 'ProductTagController@massDestroy')->name('product-tags.massDestroy');
    Route::resource('product-tags', 'ProductTagController');

    // Product Attribute
    Route::get('product-attributes/add/{id}', 'ProductAttributeController@add')->name('productAttributes.add');
    Route::delete('product-attributes/delete/{id}', 'ProductAttributeController@AttributeDestroy')->name('productAttributes.attributedestroy');
    Route::resource('product-attributes', 'ProductAttributeController');

    //Subscriptions - Packages and Plans
    Route::get('packages/add/{id}/{plan_id}', 'PackageController@addFeature')->name('packages.addfeature');
    Route::get('packages/add/{id}/', 'PackageController@addPlanFeature')->name('packages.addPlanFeature');
    Route::delete('packages/remove_feature/{id}', 'PackageController@removeFeature')->name('packages.removeFeature');
    Route::get('packages/add_on/{id}/{user_id}', 'PackageAddOnController@addFeatureAddOn')->name('package_add_ons.addfeatureaddon');
    Route::delete('packages/remove_add_on/{id}', 'PackageAddOnController@removeAddOnFeature')->name('package_add_ons.removeAddOnFeature');

    Route::resource('packages', 'PackageController');
    Route::resource('package_add_ons', 'PackageAddOnController');
    Route::resource('plan_subscription_features', 'PlanSubscriptionFeaturesController');



    //User Payments
    Route::resource('user_payments', 'UserPaymentsController');

    //Pos
    Route::get('pos/ajaxProductData', 'PosController@ajaxProductsData')->name('pos.ajaxProductsData');
    Route::post('pos/placeOrder', 'PosController@placeOrder')->name('pos.order');
    Route::post('pos/update', 'PosController@update')->name('pos.update');
    Route::resource('pos', 'PosController',['except' => [ 'update' ]]);

    //shipping

    // Products
    Route::delete('products/destroy', 'ProductController@massDestroy')->name('products.massDestroy');
    Route::post('products/media', 'ProductController@storeMedia')->name('products.storeMedia');
    Route::post('products/log', 'ProductController@imagelog')->name('products.log');
    Route::post('products/sku', 'ProductController@productsku')->name('products.sku');
    Route::post('products/logshow', 'ProductController@imagelogshow')->name('products.logshow');
    Route::post('products/ckmedia', 'ProductController@storeCKEditorImages')->name('products.storeCKEditorImages');
    Route::resource('products', 'ProductController');
    Route::post('products/parse-csv-import', 'ProductController@parseCsvImports')->name('products.parseCsvImport');
    Route::post('products/process-csv-import', 'ProductController@processCsvImports')->name('products.processCsvImport');

    Route::get('ajaxData', 'ProductController@ajaxData')->name('products.ajaxData');
    Route::get('ajaxSelectProducts', 'ProductController@ajaxSelectProducts')->name('products.ajaxSelectProducts');
    Route::get('AddedajaxData', 'ProductController@AddedajaxData')->name('products.AddedajaxData');
    Route::get('myproducts', 'ProductController@product_list')->name('products.product_list');

    Route::post('products/updates', 'ProductController@updateProduct')->name('product_list.update');
    Route::post('products/removeimg', 'ProductController@removeImg')->name('products.removeimg');

    // Faq Categories
    Route::delete('faq-categories/destroy', 'FaqCategoryController@massDestroy')->name('faq-categories.massDestroy');
    Route::resource('faq-categories', 'FaqCategoryController');

    // Faq Questions
    Route::delete('faq-questions/destroy', 'FaqQuestionController@massDestroy')->name('faq-questions.massDestroy');
    Route::resource('faq-questions', 'FaqQuestionController');

    // Content Categories
    Route::delete('content-categories/destroy', 'ContentCategoryController@massDestroy')->name('content-categories.massDestroy');
    Route::resource('content-categories', 'ContentCategoryController');

    // Content Tags
    Route::delete('content-tags/destroy', 'ContentTagController@massDestroy')->name('content-tags.massDestroy');
    Route::resource('content-tags', 'ContentTagController');

    // Content Pages
    Route::delete('content-pages/destroy', 'ContentPageController@massDestroy')->name('content-pages.massDestroy');
    Route::post('content-pages/media', 'ContentPageController@storeMedia')->name('content-pages.storeMedia');
    Route::post('content-pages/ckmedia', 'ContentPageController@storeCKEditorImages')->name('content-pages.storeCKEditorImages');
    Route::resource('content-pages', 'ContentPageController');

    // User Alerts
    Route::delete('user-alerts/destroy', 'UserAlertsController@massDestroy')->name('user-alerts.massDestroy');
    Route::resource('user-alerts', 'UserAlertsController', ['except' => ['edit', 'update']]);

    // Teams
    Route::delete('teams/destroy', 'TeamController@massDestroy')->name('teams.massDestroy');
    Route::resource('teams', 'TeamController');

    // Arobjects
    Route::delete('arobjects/destroy', 'ArobjectController@massDestroy')->name('arobjects.massDestroy');
    Route::post('arobjects/media', 'ArobjectController@storeMedia')->name('arobjects.storeMedia');
    Route::post('arobjects/ckmedia', 'ArobjectController@storeCKEditorImages')->name('arobjects.storeCKEditorImages');
    Route::resource('arobjects', 'ArobjectController');

    Route::post('arobjects/objupload', 'ArobjectController@createorupdate')->name('arobjects.objupload');
    Route::post('arobjects/updates', 'ArobjectController@updateObject')->name('arobjects.updateObject');


    // Videos
    Route::delete('videos/destroy', 'VideosController@massDestroy')->name('videos.massDestroy');
    Route::post('videos/media', 'VideosController@storeMedia')->name('videos.storeMedia');
    Route::get('videos/updatevideoproduct', 'VideosController@updatevideoproduct')->name('videos.video_product');
    Route::post('videos/ckmedia', 'VideosController@storeCKEditorImages')->name('videos.storeCKEditorImages');
    Route::post('videos/removethumbnail', 'VideosController@removethumbnail')->name('videos.removethumbnail');
    Route::resource('videos', 'VideosController');

    Route::post('videos/updates', 'VideosController@updatevideoname')->name('video_list.update');


    // Campaigns
    Route::delete('stores/destroy', 'CampaignsController@massDestroy')->name('campaigns.massDestroy');
    // Route::resource('stores', 'CampaignsController', ['names' => 'campaigns']);
    Route::resource('campaigns', 'CampaignsController');
    Route::get('videocampaigns','CampaignsController@videoindex')->name('campaigns.videoindex');
    Route::get('catalog/ajaxEntityData', 'CampaignsController@ajaxEntityData')->name('campaigns.ajaxEntityData');
    Route::get('catalog/shareCatalog', 'CampaignsController@ajaxCatalogShareData')->name('campaigns.shareCatalogData');
    Route::get('catalog/ajaxVideoEntityData', 'CampaignsController@ajaxVideoEntityData')->name('campaigns.ajaxVideoEntityData');
    Route::get('catalog/ajaxVideoProductEntityData', 'CampaignsController@ajaxVideoProductEntityData')->name('campaigns.ajaxVideoProductEntityData');


    Route::post('catalog/createcard', 'CampaignsController@createEntityCard')->name('campaigns.createEntityCard');
    Route::post('catalog/createcardfromproducts', 'CampaignsController@createEntityCardFromProducts')->name('campaigns.createEntityCardFromProducts');
    Route::post('catalog/publish', 'CampaignsController@publish')->name('campaigns.publish');

    Route::post('catalog/update', 'CampaignsController@updateCampaign')->name('campaigns.update');
    Route::post('catalog/updateCatalogSettings', 'CampaignsController@updateCatalogSettings')->name('campaigns.updateCatalogSettings');

    Route::get('campaign/usage', 'CampaignsController@getSubscriptionFeatureUsage')->name('campaigns.getSubscriptionFeatureUsage');


    Route::post('storePublish', 'UserSettingsController@storePublish')->name('mystore.publish');
    Route::post('banner/removebannerimg', 'UserSettingsController@removebanerImg')->name('banner.removebannerimg');

    Route::group(['middleware' => 'shared'], function () {
        Route::get('preview/catalogue/{shareable_link}', ['uses' => 'CampaignsController@sharePreview'])->name('campaigns.preview');
        // Route::get('preview/{shareable_link}', ['uses' =>'UserSettingsController@storePreview','middleware' => 'shared'])->name('mystore.preview');

        Route::get('preview/tryon/{shareable_link}', ['uses' => 'CampaignsController@sharePreviewTryOn'])->name('campaigns.previewTryOn');
        Route::get('preview/viewAR/{shareable_link}', ['uses' => 'CampaignsController@sharePreviewViewAR'])->name('campaigns.previewViewAR');
        Route::get('preview/viewVR/{shareable_link}', ['uses' => 'CampaignsController@sharePreviewViewVR'])->name('campaigns.previewViewVR');
        Route::get('perview/details/{product_id}/{shareable_link}', ['uses' => 'CampaignsController@sharePreviewCatalogDetails'])->name('campaigns.previewCatalogDetails');
        Route::get('storePreview/{shareable_link}', 'UserSettingsController@storePreview')->name('mystore.preview');
    });




    Route::post('campaigns/catalogcreate', 'CampaignsController@CampCreate')->name('campaigns.catalogCreate');

    Route::get('campaigns/qrcode/{qrstring}', 'CampaignsController@previewQrCode')->name('campaigns.previewQrCode');

    Route::get('campaignOption/{campaign_id}', 'CampaignOptionController@index')->name('campaignOption.index');
    Route::post('optionsUpdate', 'CampaignOptionController@optionsUpdate')->name('campaignOption.optionsUpdate');

    Route::post('campaignOption/media', 'CampaignOptionController@storeMedia')->name('campaignOption.storeMedia');

    // Entitycards
    Route::delete('entitycards/destroy', 'EntitycardController@massDestroy')->name('entitycards.massDestroy');
    Route::resource('entitycards', 'EntitycardController');

    //Orders
    Route::POST('orders/generateinvoice', 'OrdersController@updateGenerateInvoice')->name('orders.updateGenerateInvoice');
    Route::POST('orders/status', 'OrdersController@updateOrderStatus')->name('orders.updateStatus');
    Route::POST('orders/shipping', 'OrdersController@shypliteOrderBook')->name('orders.shyplite');
    Route::POST('orders/tracking', 'OrdersController@shypliteOrderTrack')->name('track.shyplite');
    Route::delete('orders/destroy', 'OrdersController@massDestroy')->name('orders.massDestroy');
    Route::get('orders/list', 'OrdersController@ordersajaxadata')->name('orders.ajaxData');
    Route::post('orders/export','OrdersController@exportOrders')->name('orders.export');
    Route::resource('orders', 'OrdersController');

    //customers new route
    Route::get('customers', 'CustomersController@index')->name('customers.index');
    Route::get('customers/orders/{id}', 'CustomersController@CustomerInfo')->name('customers.details');
    Route::get('customers/orders/details/{id}', 'CustomersController@CustomerOrderDetails')->name('customers.orderDetails');


    //old cutomers route start
    // Route::get('customers', 'CustomersController@customerajaxData')->name('customers.index');
    // Route::get('customers/orders/{id}', 'CustomersController@customerdetails')->name('customers.details');
    Route::get('customers/orders/', 'CustomersController@displayorderlist')->name('customers.order');
    Route::get('customers/orders/list/{id}', 'CustomersController@displayorder')->name('customers.displayorder');
    Route::get('customer/ajaxData', 'CustomersController@customerajaxData')->name('customers.ajaxData');
    //old cutomers route end

    //transactions
    Route::get('transactions/ajaxData', 'TransactionController@ajaxData')->name('transactions.ajaxData');
    Route::resource('transactions', 'TransactionController');


    //Enquiries
    Route::POST('enquiry/status', 'EnquiryController@updateEnquiryStatus')->name('enquiry.updateStatus');
    Route::get('enquiry', 'EnquiryController@index')->name('enquiry.index');
    Route::get('enquiry/ajaxData', 'EnquiryController@enquiryAjaxData')->name('enquiry.ajaxData');
    Route::get('enquiry/list/{id}', 'EnquiryController@show')->name('enquiry.show');


    //coupons
    Route::get('coupons','CouponController@index')->name('coupons.index');

    //Referral
    Route::get('referrals','ReferralController@index')->name('referrals.index');

    //Variants
    Route::get('variants','VariantsController@index')->name('variants.index');

    //Product Variants
    Route::get('product-variants/{product_id}','ProductVariantsController@index')->name('productVariants.index');
    Route::get('product-variants/show/{product_id}','ProductVariantsController@show')->name('productVariants.show');
    Route::get('product-variants/edit/{product_id}','ProductVariantsController@edit')->name('productVariants.edit');
    Route::put('product-variants/update/{product_id}','ProductVariantsController@update')->name('productVariants.update');
    Route::get('product-variants/delete/{product_id}','ProductVariantsController@delete')->name('productVariants.destroy');
    Route::get('product-variants/add_variant/{variant_id}','ProductVariantsController@addVariant')->name('productVariants.addVariant');
    Route::post('product-variants/media', 'ProductVariantsController@storeMedia')->name('productVariants.storeMedia');
    Route::post('product-variants/ckmedia', 'ProductVariantsController@storeCKEditorImages')->name('productVariants.storeCKEditorImages');
    Route::post('product-variants/varremoveimg', 'ProductVariantsController@removeImg')->name('productVariants.varremoveimg');


    //Statistics
    Route::get('stats_index', 'Analytics\StatisticsController@index')->name('stats.index');
    Route::get('stats/columns', 'Analytics\StatisticsController@GetTableName')->name('stats.getTableName');
    Route::get('stats/data', 'Analytics\StatisticsController@StatsData')->name('stats.getStatsData');
    Route::get('stats/chart', 'Analytics\StatisticsController@statsChart')->name('stats.getStatChart');
    Route::get('stats/icustomers', 'Analytics\StatisticsController@customerStat')->name('stats.getCustomerstat');
    Route::get('stats/{sales?}', 'Analytics\StatisticsController@salesStat')->name('stats.getSalesstat');


    // Assets
    Route::delete('assets/destroy', 'AssetsController@massDestroy')->name('assets.massDestroy');
    Route::post('assets/media', 'AssetsController@storeMedia')->name('assets.storeMedia');
    Route::post('assets/ckmedia', 'AssetsController@storeCKEditorImages')->name('assets.storeCKEditorImages');
    Route::resource('assets', 'AssetsController');

    // Currencies
    Route::delete('currencies/destroy', 'CurrencyController@massDestroy')->name('currencies.massDestroy');
    Route::resource('currencies', 'CurrencyController');

    // Company Infos
    Route::delete('company-infos/destroy', 'CompanyInfoController@massDestroy')->name('company-infos.massDestroy');
    Route::resource('company-infos', 'CompanyInfoController');

    // Countries
    Route::delete('countries/destroy', 'CountriesController@massDestroy')->name('countries.massDestroy');
    Route::resource('countries', 'CountriesController');

    // States
    Route::delete('states/destroy', 'StateController@massDestroy')->name('states.massDestroy');
    Route::resource('states', 'StateController');

    // Cities
    Route::delete('cities/destroy', 'CityController@massDestroy')->name('cities.massDestroy');
    Route::resource('cities', 'CityController');

    // User Infos
    Route::delete('user-infos/destroy', 'UserInfoController@massDestroy')->name('user-infos.massDestroy');
    Route::resource('user-infos', 'UserInfoController');


    //mobileLogin
    Route::get('account/mobileLogin','MobileLoginController@index')->name('mobileLogin');

    // User Settings
    Route::get('userSettings/createExtraCharge/{extra_charge_id}', 'UserSettingsController@createExtraCharge')->name('userSettings.createExtraCharge');
    Route::get('userSettings', 'UserSettingsController@index')->name('userSettings.index');
    Route::post('userUpdate', 'UserSettingsController@update')->name('userSettings.update');
    Route::post('saveToken','UserSettingsController@saveToken')->name('notification.save');

    Route::post('userSettings/media', 'UserSettingsController@storeMedia')->name('userSettings.storeMedia');

    Route::group(['middleware' => 'shared'], function () {
        Route::get('catalog/pdf/{shareable_link}', 'PdfController@export_catalog_pdf')->name('export_catalog_pdf');
    });

    Route::get('pos/print_invoice/{orders}', 'PdfController@print_pos_invoice_pdf')->name('print_pos_invoice_pdf');
    Route::get('orders/pdf/{orders}', 'PdfController@export_order_pdf')->name('export_order_pdf');
    Route::get('labels/pdf/{orders}', 'PdfController@export_label_pdf')->name('export_label_pdf');
    Route::get('invoice/pdf/{orders}', 'PdfController@export_invoice_pdf')->name('export_invoice_pdf');
    Route::get('orders/print/{payment_id}', 'PdfController@print_user_payment_pdf')->name('print_user_payment_pdf');
    Route::get('user_payments/pdf/{payment_id}', 'PdfController@export_user_payment_pdf')->name('export_user_payment_pdf');



    Route::get('messenger', 'MessengerController@index')->name('messenger.index');
    Route::get('messenger/create', 'MessengerController@createTopic')->name('messenger.createTopic');
    Route::post('messenger', 'MessengerController@storeTopic')->name('messenger.storeTopic');
    Route::get('messenger/inbox', 'MessengerController@showInbox')->name('messenger.showInbox');
    Route::get('messenger/outbox', 'MessengerController@showOutbox')->name('messenger.showOutbox');
    Route::get('messenger/{topic}', 'MessengerController@showMessages')->name('messenger.showMessages');
    Route::delete('messenger/{topic}', 'MessengerController@destroyTopic')->name('messenger.destroyTopic');
    Route::post('messenger/{topic}/reply', 'MessengerController@replyToTopic')->name('messenger.reply');
    Route::get('messenger/{topic}/reply', 'MessengerController@showReply')->name('messenger.showReply');


    //maintanance mode
    Route::get('maintenance', 'MaintenanceController@index')->name('maintenance');

    Route::get('/clearcache', function () {
        Artisan::call('cache:clear');
        Artisan::call('route:clear');
        Artisan::call('config:clear');
        Artisan::call('view:clear');
        return "Cache is cleared";
    });

});



Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/subscription/getpaymentdetails', 'SubscriptionController@getPaymentDetails')->name('subscription.getPaymentDetails');
    Route::get('/subscription/myPlan', 'SubscriptionController@myPlan')->name('subscription.myPlan');
    Route::get('/subscription/new_user_show/{plan_id}', 'SubscriptionController@newUserShow')->name('subscription.newUserShow');
    Route::get('/subscription/show/{subscription}/{id}', 'SubscriptionController@show')->name('subscription.show');
    Route::post('/subscription', 'SubscriptionController@create')->name('subscription.create');
    Route::post('/subscription/upgrade', 'SubscriptionController@upgradePlan')->name('subscription.upgradePlan');
    Route::post('/subscription/downgrade', 'SubscriptionController@downgradePlan')->name('subscription.downgradePlan');
    Route::get('/subscription/renewSubscription', 'SubscriptionController@renewSubscription')->name('subscription.renewSubscription');
    Route::resource('subscriptions', 'SubscriptionController');
    Route::post('/adminSubscription/createSubscription', 'AdminSubscriptionController@createSubscription')->name('adminSubscription.createSubscription');
    Route::post('/adminSubscription/upgradeSubscription', 'AdminSubscriptionController@upgradeSubscription')->name('adminSubscription.upgradeSubscription');
    Route::post('/adminSubscription/downgradeSubscription', 'AdminSubscriptionController@downgradeSubscription')->name('adminSubscription.downgradeSubscription');
    Route::post('/adminSubscription/renewSubscription', 'AdminSubscriptionController@renewSubscription')->name('adminSubscription.renewSubscription');
    Route::get('/adminSubscription/userSubscription/{user_id}', 'AdminSubscriptionController@userSubscription')->name('adminSubscription.userSubscription');
    Route::post('/adminSubscription/assignSubscription', 'AdminSubscriptionController@assignSubscription')->name('adminSubscription.Subscription');
    Route::post('/adminSubscription/updateSubscription', 'AdminSubscriptionController@updateSubscription')->name('adminSubscription.updateSubscription');
    Route::get('/impersonate/{id}', 'UsersController@impersonate')->name('users.impersonate');
    Route::get('/stopimpersonate', 'UsersController@stopImpersonate')->name('users.stopimpersonate');
    Route::post('/adminSubscription/planData','AdminSubscriptionController@planData')->name('plan.data');
    Route::post('/adminSubscription/planDetails','AdminSubscriptionController@planDetails')->name('plan.details');
    Route::post('/adminSubscription/subscriptionPayment','AdminSubscriptionController@paymentDetails')->name('subscriptionPayment.data');

    Route::get('mystore', 'CampaignsController@storeIndex')->name('campaigns.storeIndex');
    Route::post('setDomain', 'UserSettingsController@set_domain')->name('userSettings.set_domain');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin'], function () {
    Route::get('mobileview/razorpay', 'PaymentController@razorpay_mobile')->name('payment.razorpay_mobile');
});

// in a routes file


// https://stackoverflow.com/questions/26783770/laravel-route-group-match-whole-domain
//https://stackoverflow.com/questions/37360369/laravel-cname-subdomain-routing
\PWA::routes();

