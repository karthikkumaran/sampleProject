<?php

// header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: HEAD, GET, POST, PUT, PATCH, DELETE, OPTIONS");
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method,Access-Control-Request-Headers, Authorization");
header('Content-Type: application/json');
// $method = $_SERVER['REQUEST_METHOD'];
$method = Request::method();
if (Request::isMethod('OPTIONS')) {
// header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method,Access-Control-Request-Headers, Authorization");
header("HTTP/1.1 200 OK");
die();
}

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin'], function () {

    //Register new user
    Route::post('register', 'AuthApiController@register');

    //Login existing user - get accesstoken
    Route::post('login', 'AuthApiController@login');

    Route::post('Device', 'DevicesApiController@checkDevice');

});

//public api

// Route::group(['prefix' => 'v1/public', 'as' => 'api.', 'namespace' => 'Api\V1\Frontend', 'middleware' => ['auth:api']], function () {
Route::group(['prefix' => 'v1/public', 'as' => 'api.', 'namespace' => 'Api\V1\Frontend'], function () {
    Route::get('products', 'ProductListApiController@index');
    Route::get('product-categories', 'ProductCategoryApiController@index');

});

Route::group(['prefix' => 'v1/user', 'as' => 'api.', 'namespace' => 'Api\V1\User', 'middleware' => ['auth:api']], function () {

    //product-Categories
    Route::post('product-categories/{id}', 'ProductCategoryApiController@update');

    Route::apiResource('product-categories', 'ProductCategoryApiController');

    //orders
    Route::apiResource('orders', 'OrdersApiController');
    Route::post('storePayment', 'OrdersApiController@store_transaction');

    //Products
    Route::apiResource('products', 'ProductApiController');
 
    //Customers
    Route::apiResource('customers', 'CustomerApiController');

    //Attributes
    Route::apiResource('attributes', 'AttributesApiController');

    //Campaigns
    //Route::post('campaigns', 'CampaignsApiController@update');

    Route::get('catalogues', 'CampaignsApiController@index');
    Route::get('catalogue/{id}', 'CampaignsApiController@catalogueByCard');
    Route::get('card/{id}', 'CampaignsApiController@getCardById');

    //EntityCardProducts
    Route::get('campaigns/products/{campaign_id}', 'CampaignsApiController@EntityCardProduct');

    Route::get('catalogue/{campaign_id}/products', 'CampaignsApiController@EntityCardProduct');

    //Product Enquiry
    Route::apiResource('enquiry', 'EnquiryApiController');
    Route::get('user-settings', 'EnquiryApiController@userSettings');
});
Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:api']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::apiResource('users', 'UsersApiController');

    // Product Categories
    Route::post('product-categories/media', 'ProductCategoryApiController@storeMedia')->name('product-categories.storeMedia');
    Route::apiResource('product-categories', 'ProductCategoryApiController');


    // Product Tags
    Route::apiResource('product-tags', 'ProductTagApiController');
    // Attributes
    Route::apiResource('attributes', 'AttributesApiController');

    // Products
    Route::post('products/media', 'ProductApiController@storeMedia')->name('products.storeMedia');
    Route::apiResource('products', 'ProductApiController');

    // Faq Categories
    Route::apiResource('faq-categories', 'FaqCategoryApiController');

    // Faq Questions
    Route::apiResource('faq-questions', 'FaqQuestionApiController');

    // Content Categories
    Route::apiResource('content-categories', 'ContentCategoryApiController');

    // Content Tags
    Route::apiResource('content-tags', 'ContentTagApiController');

    // Content Pages
    Route::post('content-pages/media', 'ContentPageApiController@storeMedia')->name('content-pages.storeMedia');
    Route::apiResource('content-pages', 'ContentPageApiController');

    // Teams
    Route::apiResource('teams', 'TeamApiController');

    // Arobjects
    Route::post('arobjects/media', 'ArobjectApiController@storeMedia')->name('arobjects.storeMedia');
    Route::apiResource('arobjects', 'ArobjectApiController');

    // Videos
    Route::post('videos/media', 'VideosApiController@storeMedia')->name('videos.storeMedia');
    Route::apiResource('videos', 'VideosApiController');

    // Campaigns
    Route::apiResource('campaigns', 'CampaignsApiController');

    Route::get('syncdata', 'SyncApiController@index');


    // Entitycards
    Route::apiResource('entitycards', 'EntitycardApiController');

    // Assets
    Route::post('assets/media', 'AssetsApiController@storeMedia')->name('assets.storeMedia');
    Route::apiResource('assets', 'AssetsApiController');

    // Currencies
    Route::apiResource('currencies', 'CurrencyApiController');

    // Company Infos
    Route::apiResource('company-infos', 'CompanyInfoApiController');

    // Countries
    Route::apiResource('countries', 'CountriesApiController');

    // States
    Route::apiResource('states', 'StateApiController');

    // Cities
    Route::apiResource('cities', 'CityApiController');

    // User Infos
    Route::apiResource('user-infos', 'UserInfoApiController');
    // Login

});
Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\User', 'middleware' => ['auth:api']], function () {
    // Attributes
    Route::apiResource('attributes', 'AttributesApiController');

    // Customers
    Route::apiResource('customers', 'CustomerApiController'); 

    Route::apiResource('user-infos', 'UserInfoApiController');

});

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Customer'], function () {
    Route::post('customer/register', 'CustomerAuthApiController@createCustomer');
    Route::post('customer/login', 'CustomerAuthApiController@customerLogin');
});

Route::group(['prefix' => 'v1/customer', 'as' => 'api.', 'namespace' => 'Api\V1\Customer', 'middleware' => ['auth:customer-api']], function () {
    Route::get('getDetail', 'CustomerApiController@getDetail');
    Route::put('updateProfile', 'CustomerApiController@updateProfile');
    Route::put('updateAddress', 'CustomerApiController@updateAddress');
    Route::apiResource('orders', 'OrdersApiController');
    Route::post('storePayment', 'OrdersApiController@store_transaction');
    Route::post('change-password', 'ChangePasswordApiController@changepassword');
});


/*-----------------------------------------------

API Version 2 Routes

--------------------------------------------------*/


Route::group(['prefix' => 'v2', 'as' => 'api.', 'namespace' => 'Api\V2\Admin'], function () {

    //Register new user
    Route::post('register', 'AuthApiController@register');

    //Login existing user - get accesstoken
    Route::post('login', 'AuthApiController@login');

    Route::post('Device', 'DevicesApiController@checkDevice');

});

// Route::group(['prefix' => 'v2/public', 'as' => 'api.', 'namespace' => 'Api\V2\Frontend', 'middleware' => ['auth:api']], function () {
Route::group(['prefix' => 'v2/public', 'as' => 'api.', 'namespace' => 'Api\V2\Frontend'], function () {
    Route::get('products', 'ProductListApiController@index');
    Route::get('product-categories', 'ProductCategoryApiController@index');

});

Route::group(['prefix' => 'v2/user', 'as' => 'api.', 'namespace' => 'Api\V2\User', 'middleware' => ['auth:api']], function () {

    //product-Categories
    Route::post('product-categories/{id}', 'ProductCategoryApiController@update');

    Route::apiResource('product-categories', 'ProductCategoryApiController');

    //Category Products
    Route::get('category-products', 'ProductApiController@category_products');

    Route::get('products_search', 'ProductApiController@products_search');

    //orders
    Route::apiResource('orders', 'OrdersApiController');
    Route::post('storePayment', 'OrdersApiController@store_transaction');

    //Products
    Route::apiResource('products', 'ProductApiController');
 
    //Customers
    Route::apiResource('customers', 'CustomerApiController');

    //Attributes
    Route::apiResource('attributes', 'AttributesApiController');

    //Store Settings

    Route::get('/store-setting', 'StoreInfoApiController@index');

    //Campaigns
    //Route::post('campaigns', 'CampaignsApiController@update');

    Route::get('catalogues', 'CampaignsApiController@index');
    Route::get('catalogue/{id}', 'CampaignsApiController@catalogueByCard');
    Route::get('card/{id}', 'CampaignsApiController@getCardById');

    //EntityCardProducts
    Route::get('campaigns/products/{campaign_id}', 'CampaignsApiController@EntityCardProduct');

    Route::get('catalogue/{campaign_id}/products', 'CampaignsApiController@EntityCardProduct');

    //Product Enquiry
    Route::apiResource('enquiry', 'EnquiryApiController');
    Route::get('user-settings', 'EnquiryApiController@userSettings');
});
Route::group(['prefix' => 'v2', 'as' => 'api.', 'namespace' => 'Api\V2\Admin', 'middleware' => ['auth:api']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::apiResource('users', 'UsersApiController');

    // Product Categories
    Route::post('product-categories/media', 'ProductCategoryApiController@storeMedia')->name('product-categories.storeMedia');
    Route::apiResource('product-categories', 'ProductCategoryApiController');


    // Product Tags
    Route::apiResource('product-tags', 'ProductTagApiController');
    // Attributes
    Route::apiResource('attributes', 'AttributesApiController');

    // Products
    Route::post('products/media', 'ProductApiController@storeMedia')->name('products.storeMedia');
    Route::apiResource('products', 'ProductApiController');

    // Faq Categories
    Route::apiResource('faq-categories', 'FaqCategoryApiController');

    // Faq Questions
    Route::apiResource('faq-questions', 'FaqQuestionApiController');

    // Content Categories
    Route::apiResource('content-categories', 'ContentCategoryApiController');

    // Content Tags
    Route::apiResource('content-tags', 'ContentTagApiController');

    // Content Pages
    Route::post('content-pages/media', 'ContentPageApiController@storeMedia')->name('content-pages.storeMedia');
    Route::apiResource('content-pages', 'ContentPageApiController');

    // Teams
    Route::apiResource('teams', 'TeamApiController');

    // Arobjects
    Route::post('arobjects/media', 'ArobjectApiController@storeMedia')->name('arobjects.storeMedia');
    Route::apiResource('arobjects', 'ArobjectApiController');

    // Videos
    Route::post('videos/media', 'VideosApiController@storeMedia')->name('videos.storeMedia');
    Route::apiResource('videos', 'VideosApiController');

    // Campaigns
    Route::apiResource('campaigns', 'CampaignsApiController');

    Route::get('syncdata', 'SyncApiController@index');


    // Entitycards
    Route::apiResource('entitycards', 'EntitycardApiController');

    // Assets
    Route::post('assets/media', 'AssetsApiController@storeMedia')->name('assets.storeMedia');
    Route::apiResource('assets', 'AssetsApiController');

    // Currencies
    Route::apiResource('currencies', 'CurrencyApiController');

    // Company Infos
    Route::apiResource('company-infos', 'CompanyInfoApiController');

    // Countries
    Route::apiResource('countries', 'CountriesApiController');

    // States
    Route::apiResource('states', 'StateApiController');

    // Cities
    Route::apiResource('cities', 'CityApiController');

    // User Infos
    Route::apiResource('user-infos', 'UserInfoApiController');
    // Login

});
Route::group(['prefix' => 'v2', 'as' => 'api.', 'namespace' => 'Api\V2\User', 'middleware' => ['auth:api']], function () {
    // Attributes
    Route::apiResource('attributes', 'AttributesApiController');

    // Customers
    Route::apiResource('customers', 'CustomerApiController'); 

    Route::apiResource('user-infos', 'UserInfoApiController');

});

Route::group(['prefix' => 'v2', 'as' => 'api.', 'namespace' => 'Api\V2\Customer'], function () {
    Route::post('customer/register', 'CustomerAuthApiController@createCustomer');
    Route::post('customer/login', 'CustomerAuthApiController@customerLogin');
});

Route::group(['prefix' => 'v2/customer', 'as' => 'api.', 'namespace' => 'Api\V2\Customer', 'middleware' => ['auth:customer-api']], function () {
    Route::get('getDetail', 'CustomerApiController@getDetail');
    Route::put('updateProfile', 'CustomerApiController@updateProfile');
    Route::put('updateAddress', 'CustomerApiController@updateAddress');
    Route::apiResource('orders', 'OrdersApiController');
    Route::post('storePayment', 'OrdersApiController@store_transaction');
    Route::get('order-detail', 'OrdersApiController@get_order_details');
    Route::post('change-password', 'ChangePasswordApiController@changepassword');
    Route::apiResource('enquiry', 'EnquiryApiController');
    
});
